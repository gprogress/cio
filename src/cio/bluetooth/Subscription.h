/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_SUBSCRIPTION_H
#define CIO_BLUETOOTH_SUBSCRIPTION_H

#include "Types.h"

#include "Attribute.h"
#include "Properties.h"

#include <chrono>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Subscription describes the detailed state of an asynchronous subscription to receive GATT value updates.
		 * It is primarily used as a utility class by Attribute Client subclasses and as a key for clients to manage their subscriptions.
		 */
		class CIO_BLUETOOTH_API Subscription
		{
			public:
				/**
				 * Construct an empty subscription.
				 */
				Subscription() noexcept;
							
				/**
				 * Constructs a subscription for the given attribute.
				 * 
				 * @param handle The attribute handle
				 */
				explicit Subscription(std::uint16_t handle) noexcept;

				/**
				 * Constructs a subscription for the given attribute.
				 *
				 * @param handle The attribute handle
				 * @param listener The listener callback
				 * @param interval The minimum polling interval
				 */
				Subscription(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval) noexcept;
								
				/**
				 * Copy construction.
				 *
				 * @param in The subcription to copy
				 */
				Subscription(const Subscription &in);
				
				/**
				 * Move construction.
				 *
				 * @param in The subcription to move
				 */
				Subscription(Subscription &&in) noexcept;
				
				/**
				 * Copy assignment.
				 *
				 * @param in The subcription to copy
				 * @return this subscription
				 */
				Subscription &operator=(const Subscription &in);
				
				/**
				 * Move assignment.
				 *
				 * @param in The subcription to move
				 * @return this subscription
				 */
				Subscription &operator=(Subscription &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				~Subscription() noexcept;
				
				/**
				 * Clears the subscription state.
				 */
				void clear() noexcept;

				/**
				 * Sets up a subscription using the given characteristic properties.
				 *
				 * @param value The handle to the attribute of the subscription
				 */
				void subscribe(std::uint16_t value);
				
				/**
				 * Gets the handle of the actual descriptor that the subscription receives.
				 * This is typically the characteristic value handle but it can be other descriptors.
				 *
				 * @return the subscription value handle
				 */
				std::uint16_t getHandle() const noexcept;
				
				/**
				 * Sets the receipt modes that this value subscription can process.
				 *
				 * @return the modes
				 */			
				void setModes(Properties modes) noexcept; 
				
				/**
				 * Gets the receipt modes that this value subscription can process.
				 *
				 * @return the modes
				 */
				Properties getModes() const noexcept;
				
				/**
				 * Gets the currently active receipt mode being used by the attribute client.
				 * 
				 * @return the receipt mode
				 */
				Property getCurrentMode() const noexcept;
				
				/**
				 * Sets the currently active receipt mode being used by the attribute client.
				 * 
				 * @param mode the receipt mode
				 */
				void setCurrentMode(Property mode) noexcept;
				
				/**
				 * Clears the subscription to the given characteristic.
				 * This clears all handles and receipt modes.
				 * The listener and poll interval are not modified.
				 */
				void unsubscribe() noexcept;
				
				/**
				 * Gets the polling interval requested by this subscription.
				 * Updates will only be processed at most as frequently as this interval.
				 * For subscriptions in polling read mode, this is used to guide clients for deciding how often to read.
				 * For subscriptions in notify or indicate mode, this is used as a rate limiter - but be careful since this can discard updates!
				 *
				 * @return The requested polling interval
				 */
				std::chrono::milliseconds getPollInterval() const noexcept;
				
				/**
				 * Sets the polling interval requested by this subscription.
				 * Updates will only be processed at most as frequently as this interval.
				 * For subscriptions in polling read mode, this is used to guide clients for deciding how often to read.
				 * For subscriptions in notify or indicate mode, this is used as a rate limiter - but be careful since this can discard updates!
				 *
				 * @param interval The requested polling interval
				 */
				void setPollInterval(std::chrono::milliseconds interval) noexcept;
				
				/**
				 * Gets the listener bound to the subscription.
				 *
				 * @return the listener
				 */
				const AttributeCallback &getListener() const noexcept;
				
				/**
				 * Sets the listener bound to the subscription.
				 *
				 * @param listener the listener bound to the subscription
				 */
				void setListener(AttributeCallback &&listener) noexcept;
				
				/**
				 * Sets the listener bound to the subscription.
				 *
				 * @param listener the listener bound to the subscription
				 */
				void setListener(const AttributeCallback &listener);

				/**
				 * Gets the last time that a notify event was processed for this subscription.
				 * If no notify event has been processed, this returns the subscription time.
				 *
				 * @return the time at which the last notify event occurred
				 */
				std::chrono::time_point<std::chrono::steady_clock> notified() const noexcept;
				
				/**
				 * Gets the time that the next notify event is due for this subscription.
				 * This is just the last notification time plus the poll interval.
				 *
				 * @return the time at which the next notify event is due
				 */
				std::chrono::time_point<std::chrono::steady_clock> scheduled() const noexcept;
				
				/**
				 * Gets the amount of time until the next update is scheduled.
				 * If the returned value is 0ms, the update is due immediately.
				 *
				 * @return the amount of time left until the next update
				 */
				std::chrono::milliseconds remaining() const noexcept;
		
				/**
				 * Notifies this subscription of the next update.
				 * This will check to make sure the timeout has elapsed and, if so, notify the listener of the update.
				 *
				 * @param buffer The update data
				 * @param length The update data length
				 * @return the status of the notify attempt
				 */
				State notify(const void *buffer, std::size_t length);
				
				/**
				 * Interprets a subscription in a Boolean context.
				 * It is considered true if the handle value is non-zero.
				 *
				 * @return whether the subscription existed
				 */
				explicit operator bool() const noexcept;
				
			private:
				/** Value handle of descriptor receiving updates - passed to listener */
				std::uint16_t mValue;

				/** Listener to receive updates */
				AttributeCallback mListener;	
				
				/** Minimum polling interval */
				std::chrono::milliseconds mPollInterval;
				
				/** Last time that a notification was received */
				std::chrono::time_point<std::chrono::steady_clock> mNotified;
				
				/** Known modes for receipt that are possible */
				Properties mModes;
				
				/** Current mode that attribute client is using for receipt */
				Property mCurrentMode;
				
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

