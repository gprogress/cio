/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_BUFFER_H
#define CIO_BUFFER_H

#include "Types.h"

#include "Character.h"
#include "Factory.h"
#include "FixedText.h"
#include "Exception.h"
#include "Newline.h"
#include "Order.h"
#include "Print.h"
#include "Resize.h"

#include <cstring>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Buffer class provides a high-performance read/write API outside of the Input / Output / Channel interface
	 * intended for optimized encoding and decoding of text and binary content. It is intended for use with the
	 * Input, Output, and Channel interfaces in situations where raw performance is of the utmost importance.
	 */
	class CIO_API Buffer
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct a new empty buffer with initial capacity of 0.
			* The position and limit are also initialized to 0.
			* The allocator is set to the default allocator for std::uint8_t and the buffer is marked with policy Resize::Any.
			*/
			Buffer() noexcept;

			/**
			 * Construct a buffer with the given capacity.
			 * The position is initialized to 0 and the limit is initialized to the capacity.
			 * Memory is allocated using the default allocator for std::uint8_t and the buffer is marked with
			 * policy Resize::Any.
			 *
			 * @param capacity The buffer capacity
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			explicit Buffer(std::size_t capacity);

			/**
			 * Construct a buffer with the given capacity.
			 * The position is initialized to 0 and the limit is initialized to the capacity.
			 * Memory is allocated using the given allocator if provided or the default
			 * allocator for std::uint8_t otherwise. The buffer is marked with
			 * policy Resize::Any.
			 *
			 * @param capacity The buffer capacity
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			Buffer(std::size_t capacity, Factory<std::uint8_t> allocator);

			/**
			 * Construct a buffer to non-intrusively use the given piece of memory.
			 * The capacity and limit are initialized to the provided capacity.
			 * The position is initialized to 0.
			 * The memory will not be destroyed when this buffer is destroyed or cleared.
			 * The buffer is marked with policy Resize::None.
			 *
			 * @param data The pointer to the memory to use
			 * @param capacity The number of bytes in the memory to use as the capacity
			 */
			Buffer(void *data, std::size_t capacity) noexcept;

			/**
			 * Construct a buffer to non-intrusively use the given static array.
			 * The capacity and limit are initialized to the provided array's size in bytes.
			 * The position is initialized to 0.
			 * The memory will not be destroyed when this buffer is destroyed or cleared.
			 * The buffer is marked with policy Resize::None.
			 *
			 * @tparam T The array element type
			 * @tparam N The array element count
			 * @param data The pointer to the memory to usey
			 */
			template <typename T, std::size_t N>
			inline Buffer(T (&data)[N]) :
				Buffer(static_cast<void *>(data), sizeof(T) *N)
			{
				// nothing more to do
			}

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared
			 * and the policy is marked as Resize::Any, otherwise the policy is marked as Resize::None.
			 * The capacity will be set to the given capacity.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 */
			Buffer(void *data, std::size_t capacity, Factory<std::uint8_t> metaclass) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared
			 * and the policy is marked as Resize::Any, otherwise the policy is marked as Resize::None.
			 * The capacity will be set to the given capacity.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 * @param limit The initial limit
			 * @param position The initial position
			 */
			Buffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator, std::size_t limit, std::size_t position) noexcept;

			/**
			 * Construct a copy of a buffer.
			 * If the input buffer is a non-owning reference to data, this buffer will reference the same data.
			 * Otherwise this buffer will duplicate the input data.
			 *
			 * @param in The buffer to copy
			 */
			Buffer(const Buffer &in);

			/**
			* Move buffer underlying data into a new instance of this class.
			*
			* @param in The device to move
			*/
			Buffer(Buffer &&in) noexcept;

			/**
			 * Copy a buffer.
			 *
			 * @param in The buffer to copy
			 * @return this buffer
			 */
			Buffer &operator=(const Buffer &in);

			/**
			* Move buffer underlying data into this object.
			*
			* @param in The buffer to move
			* @return this buffer
			*/
			Buffer &operator=(Buffer &&in) noexcept;

			/**
			* Destructor.
			*/
			~Buffer() noexcept;

			/**
			 * Sets the capacity, limit, and position to zero and deallocates the array.
			 */
			void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this class.
			 * 
			 * @return the metaclass
			 */
			const Metaclass &getMetaclass() const noexcept;

			// Baseline buffer API - direct manipulation of pointers and sizes
			
			/**
			 * Interprets the buffer in a Boolean context.
			 * It is considered true if it has bytes remaining between current position and limit.
			 *
			 * @return the input as a Boolean
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Gets the raw data array.
			 *
			 * @return the raw data array
			 */
			inline const std::uint8_t *data() const noexcept;

			/**
			* Gets the raw data array.
			*
			* @return the raw data array
			*/
			inline std::uint8_t *data() noexcept;

			/**
			* Gets the raw data array offset to the current position.
			*
			* @return the raw data array at the current position
			*/
			inline const std::uint8_t *current() const noexcept;

			/**
			* Gets the raw data array offset to the current position.
			*
			* @return the raw data array at the current position
			*/
			inline std::uint8_t *current() noexcept;

			/**
			 * Sets the position to zero and the limit to the capacity.
			 * Typically used to start a new read or write operation.
			 *
			 * @return this buffer for convenience
			 */
			inline Buffer &reset() noexcept;

			/**
			 * Sets the limit to the current position and the current position to zero.
			 * Typically used after data is written to the buffer to set up to read that data back.
			 *
			 * @return the buffer
			 */
			inline Buffer &flip() noexcept;
			
			/**
			 * Sets the limit to the current position and the current position to the given start.
			 * Typically used after data is written to the buffer to set up to read that data back,
			 * but starting at a known mark offset rather than at the beginning.
			 *
			 * @param start The new position after flipping
			 * @return the buffer
			 */
			inline Buffer &flip(std::size_t start) noexcept;
			
			/**
			 * Sets the position to the current limit and the limit to the current size.
			 * Typically used to append more data to the unused portion at the end of the buffer.
			 *
			 * @return the buffer
			 */
			inline Buffer &unflip() noexcept;
			
			/**
			 * Moves the data from the position to the limit to the beginning of the data array.
			 * The position is set to 0 and the limit is adjusted to subtract the prior position so 
			 * the result has the same remaining bytes.
			 *
			 * Typically used to remove unused data before the position to avoid extending the buffer.
			 *
			 * @return the number of bytes removed by this operation
			 */
			inline std::size_t compact() noexcept;

			/**
			 * Directly gets the memory position of the buffer.
			 * This is equivalent to calling getReadPosition() or getWritePosition() but returns std::size_t.
			 *
			 * @return the current buffer position
			 */
			inline std::size_t position() const noexcept;

			/**
			 * Directly sets the memory position of the buffer.
			 * This is equivalent to calling requestSeekToRead or requestSeekToWrite with Seek::Begin but operates directly
			 * on the std::size_t field and returns the position without a status.
			 *
			 * The position is clipped to the current limit.
			 *
			 * @param position The desired new position
			 * @return the actual position set
			 */
			inline std::size_t position(std::size_t position) noexcept;

			/**
			 * Directly advances the memory position of the buffer.
			 * This is equivalent to calling requestSeekToRead or requestSeekToWrite with Seek::Current but operates directly
			 * on the std::size_t field and returns the delta without a status.
			 *
			 * The position is clipped to the current limit.
			 * 
			 * @param bytes The number of bytes to skip
			 * @return the number of bytes actually advanced
			 */
			inline std::size_t skip(std::size_t bytes) noexcept;

			/**
			 * Directly gets the memory limit of the buffer.
			 *
			 * @return the current buffer limit
			 */
			inline std::size_t limit() const noexcept;

			/**
			 * Directly sets the memory limit of the buffer.
			 *
			 * The position is clipped to the current size.
			 *
			 * @param limit The desired new limit
			 * @return the actual position set
			 */
			inline std::size_t limit(std::size_t limit) noexcept;

			/**
			 * Sets both the position and limit in one call.
			 * They are both clipped to the size and the position is clipped to the limit.
			 *
			 * @param position The desired position
			 * @param limit The desired limit
			 */
			inline void reslice(std::size_t position, std::size_t limit) noexcept;

			/**
			 * Gets the number of bytes currently remaining between the position and the limit.
			 *
			 * @return the number of bytes available
			 */
			inline std::size_t remaining() const noexcept;

			/**
			 * Requests that the buffer has at least the given number of bytes available between the position and limit.
			 * If the current position and limit have at least the given number of bytes, the actual remaining is returned.
			 * Otherwise, if the limit is equal to size, the resize policy allows growth, and the allocator is set,
			 * this will allocate the minimum of either the needed bytes or the current size plus the growth percentage.
			 * 
			 * @param needed The required number of remaining bytes
			 * @return the actual number of remaining bytes
			 * @throw std::bad_alloc If the allocator failed to allocate the needed  memory
			 */
			std::size_t remaining(std::size_t needed);

			/**
			 * Ensures that the buffer has at least the given number of bytes available between the position and limit to write.
			 * If the current position and limit have at least the given number of bytes, the actual remaining is returned.
			 * Otherwise, if the limit is equal to size, the resize policy allows growth, and the allocator is set,
			 * this will allocate the minimum of either the needed bytes or the current size plus the growth percentage.
			 *
			 * @param needed The required number of writable bytes remaining
			 * @return the actual number of remaining bytes
			 * @throw Exception if the bytes could not be reserved due to the resize policy or lack of allocator
			 * @throw std::bad_alloc If the allocator failed to allocate the needed  memory
			 */
			std::size_t writable(std::size_t needed);

			/**
			 * Gets whether there are any bytes available between current position and the limit.
			 *
			 * @return whether there are available bytes between the position and the limit
			 */
			inline bool available() const noexcept;
			
			/**
			 * Gets the number of "unused" bytes in the buffer between the limit and the size.
			 *
			 * @return the number of unused bytes between the limit and size
			 */
			inline std::size_t unused() const noexcept;

			/**
			 * Ensures that the given number of "unused" bytes is in the buffer between the limit and the size.
			 * If the current state does not meet this requirement, the buffer will be compacted, then if necessary
			 * the buffer size will be extended.
			 *
			 * @param required The required number of unused bytes
			 * @return the number of unused bytes between the limit and size
			 */
			std::size_t unused(std::size_t required);

			/**
			 * Gets the allocator currently in use for this class.
			 * If the "null" allocator, the data is merely referenced rather than owned.
			 * 
			 * @return the allocator in use
			 */
			inline Factory<std::uint8_t> allocator() const noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * The data array will not be managed by this Buffer and will not be destroyed by it.
			 * The capacity will be set to the given capacity.
			 * The allocator will be set to nullptr.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 */
			void bind(void *data, std::size_t capacity) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 */
			void bind(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept;

			/**
			 * Sets up this buffer to allocate a new data array with the given capacity.
			 * The data array will be destroyed when this Buffer is destroyed or cleared.
			 * The default Metaclass allocator for std::uint8_t will be used.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param capacity The size of the data array in bytes
			 * @return the allocated data array
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::uint8_t *allocate(std::size_t capacity);

			/**
			 * Sets up this buffer to allocate a new data array with the given capacity.
			 * The data array will be destroyed when this Buffer is destroyed or cleared.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 * @return the allocated data array
			 */
			std::uint8_t *allocate(std::size_t capacity, Factory<std::uint8_t> allocator);

			/**
			 * Reallocates the existing data array to a different size.
			 * The existing allocator is used if one is set. Otherwise, the default metaclass for std::uint8_t is used.
			 * The existing position and limit will not be modified, except they will be clipped to the new size if lower.
			 * 
			 * @param capacity The desired size
			 * @return the allocated array
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::uint8_t *reallocate(std::size_t capacity);

			/**
			 * Trims the existing data array to the current limit.
			 * If an allocator is set, it will be used to reallocate the array.
			 * Otherwise the operation will fail and the current size is maintained.
			 * 
			 * @return the buffer size after trimming
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::size_t trim();
			
			/**
			 * Internalizes the data so that the buffer owns it.
			 * If there is no current allocator, the default allocator will be adopted
			 * and the current data will be copied. Otherwise nothing happens.
			 * 
			 * @return the internalized data
			 */
			std::uint8_t *internalize();

			/**
			 * Creates a copy of the given data bytes using the default metaclass for std::uint8_t as the allocator.
			 *
			 * @param data The data to duplicate
			 * @param length The number of bytes to duplicate
			 * @return the duplicated data
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::uint8_t *internalize(const void *data, std::size_t length);
						
			/**
			 * Creates a copy of the given data bytes using the given metaclass as an allocator.
			 *
			 * @param data The data to duplicate
			 * @param length The number of bytes to duplicate
			 * @return the duplicated data
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::uint8_t *internalize(const void *data, std::size_t length, Factory<std::uint8_t> allocator);

			/**
			 * Duplicates the current data using the default allocator and returns a buffer with the duplicate.
			 * 
			 * @return the duplicate buffer
			 */
			Buffer duplicate() const;

			/**
			 * Sets the current data array capacity of the buffer to be at least the reserved amount.
			 * If the current capacity is less than the requested amount, the data array will be reallocated to the new capacity.
			 * If the current limit is above the new capacity, it will be set to the new capacity.
			 * If the current offset position is above the new capacity, it will be set to the new capacity.
			 * 
			 * This method ignores the resize policy and the minimum growth percentage and will always try
			 * to perform the resize exactly as specified. It will only fail if no allocator is present or
			 * if the actual memory allocation fails.
			 *
			 * @param capacity The new capacity
			 * @return the actual capacity after this operation
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			std::size_t reserve(std::size_t capacity);

			/**
			 * Gets the maximum capacity of the buffer.
			 * If the buffer is growable, this is theoretically the memory size limit CIO_UNKNOWN_SIZE,
			 * although in practice allocations will fail at lower values.
			 * Otherwise it is the current size.
			 *
			 * @return the theoretical maximum capacity
			 */
			inline std::size_t capacity() const noexcept;

			/**
			 * Gets the current data array capacity of the buffer.
			 *
			 * @return the current array capacity
			 */
			inline std::size_t size() const noexcept;

			/**
			 * Gets whether the buffer is currently empty.
			 * This is true if size() is 0.
			 * 
			 * @return whether the buffer is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Sets the current data array capacity of the buffer to be at least the reserved amount.
			 * If the current capacity is less than the requested amount, the data array will be reallocated to the new capacity.
			 * If the current limit is above the new capacity, it will be set to the new capacity.
			 * If the current offset position is above the new capacity, it will be set to the new capacity.
			 *
			 * This method ignores the resize policy and the minimum growth percentage and will always try
			 * to perform the resize exactly as specified. It will only fail if no allocator is present or
			 * if the actual memory allocation fails.
			 * 
			 * @warning If the new size is below the current limit, all data after the new size will be lost
			 *
			 * @param capacity The new capacity
			 * @return the actual capacity after this operation
			 */
			std::size_t resize(std::size_t capacity);

			/**
			 * Requests to read into an output buffer the given number of bytes, up to the buffer limit.
			 * If fewer bytes are available before the buffer limit, the returned byte count will reflect that.
			 * The buffer position will be advanced by the number of bytes actually read.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @return the number of bytes actually read, with either Status::Success or Status::Underflow
			 */
			inline std::size_t requestRead(void *output, std::size_t length) noexcept;

			/**
			 * Reads into an output buffer the given number of bytes.
			 * If fewer bytes are available, this method will throw an exception with Reason::Underflow and the buffer will not be advanced.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @throw Exception If not enough data existed to read
			 */
			inline void read(void *output, std::size_t length);

			/**
			 * Requests to read into an output buffer the given number of bytes at the given absolute offset without modifying the buffer position.
			 * If fewer bytes are available before the buffer limit, the returned count will reflect that.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @param position The absolute byte offset to read from
			 * @return the number of bytes actually read
			 */
			inline std::size_t requestReadFromPosition(void *output, std::size_t length, std::size_t position) const noexcept;

			/**
			 * Reads into an output buffer the given number of bytes at the given absolute offset without modifying the buffer position.
			 * If fewer bytes are available, this method will throw an exception with Reason::Underflow.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @param position The absolute byte offset to read from
			 * @throw Exception If not enough data existed to read
			 */
			inline void readFromPosition(void *output, std::size_t length, std::size_t position) const;
			
			/**
			 * Gets a single byte from the current position.
			 *
			 * @return the byte
			 * @throw cio::Exception If the read could not be done
			 */
			inline std::uint8_t get();
			
			/**
			 * Gets a C++ primitive type from the current position in Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T get();
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBig();
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Little to Host byte order.
			 * 
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittle();

			/**
			 * Gets a C++ primitive type from the given position in Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from the given position, converting from Big to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBigFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from the given position, converting from Little to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittleFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position in Host byte order.
			 * This does not modify the current position.
			 *
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getFromOffset(std::size_t offset) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position, converting from Big to Host byte order.
			 * This does not modify the current position.
			 *
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBigFromOffset(std::size_t offset) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position, converting from Little to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittleFromOffset(std::size_t offset) const;

			/**
			 * Gets a C++ primitive type from the current position in host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void get(T &value);
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getBig(T &value);
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Little to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getLittle(T &value);
			
			/**
			 * Gets an array of C++ primitive type from the current position in host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getArray(T *data, std::size_t count);
			
			/**
			 * Gets an array of C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getBigArray(T *data, std::size_t count);
			
			/**
			 * Gets an array of C++ primitive type from the current position, converting from Little to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getLittleArray(T *data, std::size_t count);

			/**
			 * Get a referenced view of the text currently in the buffer up to (and including) a null terminator or the end of buffer.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 * 
			 * @warning Modifying the buffer content may invalidate the view.
			 *
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getText() noexcept;

			/**
			 * Gets a referenced view of the text currently in the buffer up to (and including) a null terminator, the end of buffer, or the given number of bytes.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 * 
			 * @warning Modifying the buffer content may invalidate the view.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getText(std::size_t bytes) noexcept;

			/**
			 * Gets a referenced view of the text currently in the buffer up to (and including) a null terminator, the end of the current line,
			 * the end of the buffer, or the given number of bytes.
			 * 
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 *
			 * @warning Modifying the buffer content may invalidate the view.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getTextLine(std::size_t bytes = SIZE_MAX) noexcept;

			/**
			 * Gets a referenced view of the text already processed in the buffer from position 0 to the current position.
			 * 
			 * @return the processed text view
			 */
			Text viewProcessedText() const noexcept;

			/**
			 * Gets a referenced view of the remaining text not processed in the buffer from the current position to the limit.
			 *
			 * @return the uprocessed text view
			 */
			Text viewRemainingText() const noexcept;

			/**
			 * Gets a fixed text of an exact number of bytes from the buffer starting at the current position.
			 * Exactly N bytes will be read regardless of null termination.
			 * 
			 * @tparam <N> The number of bytes of text to read
			 * @return the fixed text read
			 */
			template <std::size_t N>
			inline FixedText<N> getFixedText();

			/**
			 * Requests to write from an input buffer up to the given number of bytes, stopping at the buffer limit.
			 * If fewer bytes are available up to the buffer limit, the returned Progress will have Status::Overflow.
			 * The buffer position will be advanced by the number of bytes actually written.
			 *
			 * @param input The data buffer to write from
			 * @param length The number of bytes to write from the input
			 * @return the number of bytes actually written
			 */
			inline std::size_t requestWrite(const void *input, std::size_t length) noexcept;
			
			/**
			 * Write from an input buffer the given number of bytes.
			 * If fewer bytes are available to be written, an exception will be thrown and the position will not be advanced.
			 *
			 * @param input The data buffer to write from
			 * @param length The number of bytes to write from the input
			 * @throw cio::Exception If the write could not be done
			 */
			inline void write(const void *input, std::size_t length);
			
			/**
			 * Prints the given value as UTF-8 text to the buffer.
			 * This validates that the buffer has enough space remaining to actually print the value.
			 * 
			 * @tparam T The data type
			 * @param value The value to print
			 * @return the number of characters printed
			 * @throw Exception If not enough room was left in the buffer to print the value
			 */
			template <typename T>
			inline std::size_t putText(const T &value);

			/**
			 * Prints the given byte sequence as hexadecimal text digits to the buffer.
			 * 
			 * @param bytes The bytes to print
			 * @param count The number of bytes to print
			 * @return the number of characters written to the buffer
			 */
			std::size_t putByteText(const void *bytes, std::size_t count);

			/**
			 * Requests printing the given value as UTF-8 text to the buffer up to the buffer's current limit.
			 * This will return the actual number of characters that would needed to print the full value.
			 *
			 * @tparam T The data type
			 * @param value The value to print
			 * @return the number of bytes that would be needed to print the full value
			 */
			template <typename T>
			inline std::size_t requestPutText(const T &value) noexcept;

			/**
			 * Writes the given primitive to the buffer in the native host byte order.
			 *
			 * @tparam <T> The primitive type
			 * @param value The value to write
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void put(const T &value);
			
			/**
			 * Writes the given primitive to the buffer, converting from Host to Big byte order.
			 *
			 * @tparam <T> The primitive type
			 * @param value The value to write
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void putBig(const T &value);
			
			/**
			 * Writes the given primitive to the buffer, converting from Host to Little byte order.
			 *
			 * @tparam <T> The primitive type
			 * @param value The value to write
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void putLittle(const T &value);
			
			/**
			 * Repeatedly writes the given byte value to the specified number of bytes in the buffer, starting at the current position
			 * and continuing up to the limit. The current position is advanced by the number of bytes actually written.
			 *
			 * @param value The byte value to write
			 * @param count The number of times to repeatedly write the buffer value
			 * @return the actual number of times the byte value was written
			 */
			inline std::size_t requestSplat(std::uint8_t value, std::size_t count) noexcept;

			/**
			 * Repeatedly writes the given value to the buffer starting at the current position.
			 * If the needed number of bytes is not available, an exception is thrown and no bytes are written.
			 * The current position is advanced by the number of bytes actually written.
			 *
			 * @tparam <T> The value type
			 * @param value The value to write
			 * @param count The number of times to repeatedly write the buffer value
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void splat(T value, std::size_t count);
			
			/**
			 * Repeatedly writes the given value (converted from host to big endian) to the buffer starting at the current position.
			 * If the needed number of bytes is not available, an exception is thrown and no bytes are written.
			 * The current position is advanced by the number of bytes actually written.
			 *
			 * @tparam <T> The value type
			 * @param value The value to write
			 * @param count The number of times to repeatedly write the buffer value
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void splatBig(T value, std::size_t count);

			/**
			 * Repeatedly writes the given value (converted from host to little endian) in the buffer starting at the current position.
			 * If the needed number of bytes is not available, an exception is thrown and no bytes are written.
			 * The current position is advanced by the number of bytes actually written.
			 *
			 * @tparam <T> The value type
			 * @param value The value to write
			 * @param count The number of times to repeatedly write the buffer value
			 * @throw cio::Exception If the write could not be done
			 */
			template <typename T>
			inline void splatLittle(T value, std::size_t count);

			/**
			 * Copies bytes from the current position in the given input buffer to the current position in this buffer.
			 * At most the lesser of the input's remaining bytes and this buffer's bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param input The input buffer to copy from
			 * @return the number of bytes actually copied
			 */
			std::size_t receive(Buffer &input) noexcept;
			
			/**
			 * Copies bytes from the current position in the given input buffer to the current position in this buffer.
			 * At most the lesser of the input's remaining bytes, this buffer's bytes, and the provided maximum bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param input The input buffer to copy from
			 * @param maxLength The maximum number of bytes to copy from the buffer
			 * @return the number of bytes actually copied
			 */			
			std::size_t receive(Buffer &input, std::size_t maxLength) noexcept;

			/**
			 * Copies bytes from the current position in the given input buffer to the current position in this buffer.
			 * At most the lesser of the input's remaining bytes and this buffer's bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param input The input buffer to copy from
			 * @return the number of bytes actually copied
			 */
			std::size_t receive(ReadBuffer &input) noexcept;

			/**
			 * Copies bytes from the current position in the given input buffer to the current position in this buffer.
			 * At most the lesser of the input's remaining bytes, this buffer's bytes, and the provided maximum bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param input The input buffer to copy from
			 * @param maxLength The maximum number of bytes to copy from the buffer
			 * @return the number of bytes actually copied
			 */
			std::size_t receive(ReadBuffer &input, std::size_t maxLength) noexcept;

			/**
			 * Copies bytes from the current position in this buffer to the current position in the given output buffer.
			 * At most the lesser of the output's remaining bytes and this buffer's bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param output The buffer to copy to
			 * @return the number of bytes actually copied
			 */
			std::size_t send(Buffer &output) noexcept;
			
			/**
			 * Copies bytes from the current position in this buffer to the current position in the given output buffer.
			 * At most the lesser of the output's remaining bytes and this buffer's bytes will be copied, and the provided maximum bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param output The buffer to copy to
			 * @param maxLength The maximum number of bytes to copy to the buffer
			 * @return the number of bytes actually copied
			 */
			std::size_t send(Buffer &output, std::size_t maxLength) noexcept;
			
			/**
			 * Appends bytes from the given buffer to this buffer.
			 * This is equivalent to the following steps:
			 * <ol>
			 * <li>this->compact()</li>
			 * <li>this->reserve(this->size() + input.remaining())</li>
			 * <li>this->unflip()</li>
			 * <li>this->receive(input)</li>
			 * <li>this->flip()</li>
			 *
			 * @param input The buffer to read from
			 * @return this buffer
			 */
			Buffer &append(Buffer &input);
			
			/**
			 * Appends bytes from the given buffer to this buffer.
			 * This is equivalent to the following steps:
			 * <ol>
			 * <li>this->compact()</li>
			 * <li>this->reserve(maxLength)</li>
			 * <li>this->unflip()</li>
			 * <li>this->receive(input)</li>
			 * <li>this->flip()</li>
			 *
			 * @param input The buffer to read from
			 * @param maxLength The maximum number of bytes to read from the buffer
			 * @return this buffer
			 */
			Buffer &append(Buffer &input, std::size_t maxLength);

			/**
			 * Appends bytes from the given buffer to this buffer.
			 * This is equivalent to the following steps:
			 * <ol>
			 * <li>this->compact()</li>
			 * <li>this->reserve(this->size() + input.remaining())</li>
			 * <li>this->unflip()</li>
			 * <li>this->receive(input)</li>
			 * <li>this->flip()</li>
			 *
			 * @param input The buffer to read from
			 * @return this buffer
			 */
			Buffer &append(ReadBuffer &input);

			/**
			 * Appends bytes from the given buffer to this buffer.
			 * This is equivalent to the following steps:
			 * <ol>
			 * <li>this->compact()</li>
			 * <li>this->reserve(maxLength)</li>
			 * <li>this->unflip()</li>
			 * <li>this->receive(input)</li>
			 * <li>this->flip()</li>
			 *
			 * @param input The buffer to read from
			 * @param maxLength The maximum number of bytes to read from the buffer
			 * @return this buffer
			 */
			Buffer &append(ReadBuffer &input, std::size_t maxLength);

			/**
			 * Gets a read buffer that represents a read-only view of the current buffer.
			 * All fields have the same values, but the allocator on the returned read buffer will be null to indicate it's a view-only reference.
			 * Changes to the returned buffer will not affect this buffer.
			 * 
			 * @return the read-only view 
			 */
			ReadBuffer view() const noexcept;

			/**
			 * Gets a read buffer that represents a read-only view of the data already processed.
			 * The returned buffer will have position of 0, limit and capacity set to the position in this buffer,
			 * and point to position 0 of this buffer.
			 *
			 * Changes to the returned buffer will not affect this buffer.
			 *
			 * @return the read-only view of the data already processed
			 */
			ReadBuffer viewProcessed() const noexcept;

			/**
			 * Gets a read buffer that represents a read-only view of the currently available data.
			 * The returned buffer will have position of 0, limit and capacity set to the remaining bytes in this buffer,
			 * and point to the current position in this buffer.
			 * 
			 * Changes to the returned buffer will not affect this buffer.
			 * 
			 * @return the read-only view of the remaining data
			 */
			ReadBuffer viewRemaining() const noexcept;

			/**
			 * Gets the resize policy for the buffer.
			 * This affects whether attempting to write past the end of the buffer
			 * will attempt to allocate more memory.
			 * 
			 * @return the buffer resize policy
			 */
			Resize getResizePolicy() const noexcept;

			/**
			 * Sets the resize policy for the buffer.
			 * This affects whether attempting to write past the end of the buffer
			 * will attempt to allocate more memory.
			 *
			 * @return the buffer resize policy
			 */
			void setResizePolicy(Resize policy) noexcept;

			/**
			 * Gets the minimum percentage of current size to reserve when growing the buffer
			 * to accommodate new data. The default is 50% to balance performance for incremental
			 * growth with not using too much additional memory.
			 * 
			 * @return the minimum growth percentage when automatically resizing
			 */
			unsigned getGrowthPercentage() const noexcept;

			/**
			 * Gets the minimum percentage of current size to reserve when growing the buffer
			 * to accommodate new data. The default is 50% to balance performance for incremental
			 * growth with not using too much additional memory.
			 * 
			 * Setting to 0% will disable minimum growth and only allocate exactly as much memory
			 * as needed for each new data request. This is the most memory efficient but may be
			 * very slow for large numbers of small writes.
			 * 
			 * The maximum allowed value is 1000% for safety to avoid excessive memory use.
			 * Values higher than that will be capped to it.
			 *
			 * @param value the minimum growth percentage when automatically resizing
			 */
			void setGrowthPercentage(unsigned value) noexcept;

			/**
			 * Prints the Buffer itself to a returned UTF-8 string.
			 * This prints JSON elements for each class member, and dumps the entire buffer bytes to hexadecimal.
			 * 
			 * @return the printed string
			 */
			std::string print() const;

			/**
			 * Prints the Buffer itself to a UTF-8 text buffer.
			 * This prints JSON elements for each class member, and dumps the entire buffer bytes to hexadecimal.
			 *
			 * @param buffer The text buffer to print
			 * @param capacity The capacity of the text buffer
			 * @return the printed string
			 */
			std::size_t print(char *buffer, std::size_t capacity) const noexcept;

			/**
			 * Prints the Buffer to a UTF-8 markup writer.
			 * This prints elements for each class member.
			 * 
			 * @param writer The markup writer to print to
			 * @throw Exception If writing the markup fails
			 */
			void print(MarkupWriter &writer) const;

		private:
			/**
			 * The Read Buffer is allowed access for fast moves.
			 */
			friend class ReadBuffer;

			/** The metaclass for this class */
			static Class<Buffer> sMetaclass;
			
			/** The allocator, if any */
			Factory<std::uint8_t> mFactory;
			
			/** The actual data */
			std::uint8_t *mElements = nullptr;

			/** The allocated size of the data */
			std::size_t mSize = 0;

			/** The maximum valid position relative to the beginning of the chunk */
			std::size_t mLimit = 0;

			/** The current position relative to the beginning of the chunk */
			std::size_t mPosition = 0;

			/** Minimum growth percentage on resize */
			std::uint16_t mGrowthPercentage;

			/** The resize policy for buffer writes */
			Resize mResizePolicy;
	};

	/**
	 * Provides an overloaded operator<< print operator to print UTF-8 text values to a Buffer similar
	 * to a std::ostream.
	 * 
	 * @tparam T The value type being printed
	 * @param buffer The buffer to print to
	 * @param value The value to print
	 * @return the buffer after printing
	 * @throw Exception If not enough space was available to print the value
	 */
	template <typename T>
	inline Buffer &operator<<(Buffer &buffer, const T &value)
	{
		buffer.putText(value);
		return buffer;
	}

	/**
	 * Print a value using the buffer as the target for any value.
	 * 
	 * @tparam T The value type being printed
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @return the number of bytes needed to print
	 */
	template <typename T>
	inline std::size_t print(const T &value, Buffer &buffer) noexcept
	{
		return buffer.requestPutText(value);
	}
}

/* Inline implementation */

namespace cio
{
	// Buffer API - direct calls

	inline Buffer::operator bool() const noexcept
	{
		return mPosition < mLimit;
	}
	
	inline Buffer &Buffer::flip() noexcept
	{
		mLimit = mPosition;
		mPosition = 0;
		return *this;
	}
		
	inline Buffer &Buffer::flip(std::size_t start) noexcept
	{
		mLimit = mPosition;
		mPosition = std::min(mLimit, start);
		return *this;
	}
	
	inline Buffer &Buffer::unflip() noexcept
	{
		mPosition = mLimit;
		mLimit = mSize;
		return *this;
	}
	
	inline std::size_t Buffer::compact() noexcept
	{
		std::size_t prev = mPosition;
		std::memmove(mElements, mElements + mPosition, mLimit - mPosition);
		mLimit -= mPosition;
		mPosition = 0;
		return prev;
	}

	inline std::size_t Buffer::remaining() const noexcept
	{
		return mLimit - mPosition;
	}

	inline bool Buffer::available() const noexcept
	{
		return mPosition < mLimit;
	}

	inline std::size_t Buffer::unused() const noexcept
	{
		return mSize - mLimit;
	}

	inline Factory<std::uint8_t> Buffer::allocator() const noexcept
	{
		return mFactory;
	}

	inline void Buffer::reslice(std::size_t position, std::size_t limit) noexcept
	{
		mLimit = std::min(limit, mSize);
		mPosition = std::min(position, mLimit);
	}

	inline Buffer &Buffer::reset() noexcept
	{
		mPosition = 0;
		mLimit = mSize;
		return *this;
	}

	inline const std::uint8_t *Buffer::data() const noexcept
	{
		return mElements;
	}

	inline std::uint8_t *Buffer::data() noexcept
	{
		return mElements;
	}

	inline const std::uint8_t *Buffer::current() const noexcept
	{
		return mElements + mPosition;
	}

	inline std::uint8_t *Buffer::current() noexcept
	{
		return mElements + mPosition;
	}

	inline std::size_t Buffer::position() const noexcept
	{
		return mPosition;
	}

	inline std::size_t Buffer::position(std::size_t position) noexcept
	{
		mPosition = std::min(mLimit, position);
		return mPosition;
	}

	inline std::size_t Buffer::skip(std::size_t count) noexcept
	{
		std::size_t actual = std::min(mLimit - mPosition, count);
		mPosition += actual;
		return actual;
	}

	inline std::size_t Buffer::limit() const noexcept
	{
		return mLimit;
	}

	inline std::size_t Buffer::limit(std::size_t limit) noexcept
	{
		mLimit = std::min(limit, mSize);
		mPosition = std::min(mPosition, mLimit);
		return mLimit;
	}
	
	inline std::size_t Buffer::capacity() const noexcept
	{
		return cio::growable(mResizePolicy) ? SIZE_MAX : mSize;
	}

	inline std::size_t Buffer::size() const noexcept
	{
		return mSize;
	}

	inline bool Buffer::empty() const noexcept
	{
		return mSize == 0;
	}

	inline std::size_t Buffer::requestRead(void *output, std::size_t length) noexcept
	{
		std::size_t actual = 0;
		if (output)
		{
			actual = std::min(length, mLimit - mPosition);
			std::memcpy(output, mElements + mPosition, actual);
			mPosition += actual;
		}
		return actual;
	}
			
	inline void Buffer::read(void *output, std::size_t length)
	{
		if (!output || mLimit - mPosition < length)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(output, mElements + mPosition, length);
		mPosition += length;
	}

	inline std::size_t Buffer::requestReadFromPosition(void *output, std::size_t length, std::size_t position) const noexcept
	{
		std::size_t actual = 0;

		if (output && position <= mLimit)
		{
			actual = std::min(length, mLimit - position);
			std::memcpy(output, mElements + position, actual);
		}

		return actual;
	}

	inline void Buffer::readFromPosition(void *output, std::size_t length, std::size_t position) const
	{
		if (!output || mLimit - position < length)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}

		std::memcpy(output, mElements + position, length);
	}

	inline std::uint8_t Buffer::get()
	{
		if (mPosition >= mLimit)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		return mElements[mPosition++];
	}
						
	template <typename T>
	inline T Buffer::get()
	{
		T value;
		
		if (mLimit - mPosition < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(&value, mElements + mPosition, sizeof(T));
		mPosition += sizeof(T);
		
		return value;
	}
	
	template <typename T>
	inline T Buffer::getBig()
	{
		return Big::order(this->get<T>());
	}
	
	template <typename T>
	inline T Buffer::getLittle()
	{
		return Little::order(this->get<T>());
	}

	template <typename T>
	inline T Buffer::getFromPosition(std::size_t position) const
	{
		T value;

		if (mLimit - position < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}

		std::memcpy(&value, mElements + position, sizeof(T));

		return value;
	}

	template <typename T>
	inline T Buffer::getBigFromPosition(std::size_t position) const
	{
		return Big::order(this->getFromPosition<T>(position));
	}

	template <typename T>
	inline T Buffer::getLittleFromPosition(std::size_t position) const
	{
		return Little::order(this->getFromPosition<T>(position));
	}

	template <typename T>
	inline T Buffer::getFromOffset(std::size_t offset) const
	{
		return this->getFromPosition<T>(mPosition + offset);
	}

	template <typename T>
	inline T Buffer::getBigFromOffset(std::size_t offset) const
	{
		return Big::order(this->getFromPosition<T>(mPosition + offset));
	}

	template <typename T>
	inline T Buffer::getLittleFromOffset(std::size_t offset) const
	{
		return Little::order(this->getFromPosition<T>(mPosition + offset));
	}

	template <typename T>
	inline void Buffer::get(T &value)
	{
		if (mLimit - mPosition < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(&value, mElements + mPosition, sizeof(T));
		mPosition += sizeof(T);
	}
			
	template <typename T>
	inline void Buffer::getBig(T &value)
	{
		this->get(value);	
		Big::reorder(value);
	}
	
	template <typename T>
	inline void Buffer::getLittle(T &value)
	{
		this->get(value);	
		Little::reorder(value);
	}	
	
	template <typename T>
	inline void Buffer::getArray(T *data, std::size_t count)
	{
		std::size_t bytes = sizeof(T) * count;
		if (mLimit - mPosition < bytes)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(data, mElements + mPosition, bytes);
		mPosition += bytes;
	}
			
	template <typename T>
	inline void Buffer::getBigArray(T *data, std::size_t count)
	{
		this->getArray(data, count);	
		Big::reorder(data, count);
	}
	
	template <typename T>
	inline void Buffer::getLittleArray(T *data, std::size_t count)
	{
		this->getArray(data, count);	
		Little::reorder(data, count);
	}

	template <std::size_t N>
	inline FixedText<N> Buffer::getFixedText()
	{
		FixedText<N> text;
		if (mLimit - mPosition < N)
		{
			throw Exception(Action::Read, Reason::Overflow);
		}
		std::memcpy(text.data(), mElements + mPosition, N);
		mPosition += N;
		return text;
	}

	inline std::size_t Buffer::requestWrite(const void *input, std::size_t length) noexcept
	{
		std::size_t actual = 0;
		if (input)
		{
			actual = std::min(length, mLimit - mPosition);
			std::memcpy(mElements + mPosition, input, actual);
			mPosition += actual;
		}
		return actual;
	}

	inline void Buffer::write(const void *input, std::size_t length)
	{
		if (input)
		{
			this->writable(length);
			std::memcpy(mElements + mPosition, input, length);
			mPosition += length;
		}
	}

	template <typename T>
	inline std::size_t Buffer::putText(const T &value)
	{
		std::size_t required = cio::strlen(value);
		this->writable(required);

		std::size_t actual = cio::print(value, reinterpret_cast<char *>(mElements + mPosition), mLimit - mPosition);
		mPosition += actual;
		return actual;
	}

	template <typename T>
	inline std::size_t Buffer::requestPutText(const T &value) noexcept
	{
		std::size_t actual = cio::print(value, reinterpret_cast<char *>(mElements + mPosition), mLimit - mPosition);
		mPosition += std::min(actual, mLimit - mPosition);
		return actual;
	}

	template <typename T>
	inline void Buffer::put(const T &value)
	{	
		this->writable(sizeof(T));
		std::memcpy(mElements + mPosition, &value, sizeof(T));
		mPosition += sizeof(T);
	}
			
	template <typename T>
	inline void Buffer::putBig(const T &value)
	{
		this->put(Big::order(value));
	}
		
	template <typename T>
	inline void Buffer::putLittle(const T &value)
	{
		this->put(Little::order(value));
	}
	
	inline std::size_t Buffer::requestSplat(std::uint8_t value, std::size_t count) noexcept
	{
		std::size_t actual = std::min(count, mLimit - mPosition);
		std::memset(mElements + mPosition, value, actual);
		mPosition += actual;
		return actual;
	}

	template <typename T>
	inline void Buffer::splat(T value, std::size_t count)
	{
		this->writable(sizeof(T) * count);
		
		for (std::size_t i = 0; i < count; ++i)
		{
			std::memcpy(mElements + mPosition, &value, sizeof(T));
			mPosition += sizeof(T);
		}
	}

	// Specialize for 1 byte quantities

	template <>
	inline void Buffer::splat<unsigned char>(unsigned char value, std::size_t count)
	{
		this->writable(count);
		std::memset(mElements + mPosition, value, count);
		mPosition += count;
	}
	
	template <>
	inline void Buffer::splat<signed char>(signed char value, std::size_t count)
	{
		this->writable(count);
		std::memset(mElements + mPosition, value, count);
		mPosition += count;
	}

	template <>
	inline void Buffer::splat<char>(char value, std::size_t count)
	{
		this->writable(count);
		std::memset(mElements + mPosition, value, count);
		mPosition += count;
	}

	template <>
	inline void Buffer::splat<bool>(bool value, std::size_t count)
	{
		this->writable(count);
		std::memset(mElements + mPosition, value, count);
		mPosition += count;
	}

	template <typename T>
	inline void Buffer::splatBig(T value, std::size_t count)
	{
		this->splat(Big::order(value), count);
	}

	template <typename T>
	inline void Buffer::splatLittle(T value, std::size_t count)
	{
		this->splat(Little::order(value), count);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
