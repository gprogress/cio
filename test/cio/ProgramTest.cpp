/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ProgramTest.h"

#include <cio/Input.h>
#include <cio/Program.h>

namespace cio
{
	ProgramTest::ProgramTest():
		cio::test::Suite("cio::Program")
	{
		this->addTestCase("setArguments", &ProgramTest::setArgumentsTest);
		this->addTestCase("execution", &ProgramTest::testExecution);
	}

	ProgramTest::~ProgramTest() noexcept = default;

	void ProgramTest::setArgumentsTest()
	{
		std::vector<Text> args = { "--a", "b", "foo bar", "482" };

		// First version std::vector<Text>
		{
			Program program;

			// First version - using args

			program.setArguments(args);
			CIO_ASSERT_EQUAL(args.size(), program.getArguments().size());
			for (std::size_t i = 0; i < args.size(); ++i)
			{
				CIO_ASSERT_EQUAL(args[i], program.getArguments()[i]);
			}
		}

		// Second version int argc char ** argv
		{
			int argc = static_cast<int>(args.size());
			const char *argv[] = { args[0].c_str(), args[1].c_str(), args[2].c_str(), args[3].c_str() };

			Program program;
			program.setArguments(argc, argv);

			CIO_ASSERT_EQUAL(args.size(), program.getArguments().size());
			for (std::size_t i = 0; i < args.size(); ++i)
			{
				CIO_ASSERT_EQUAL(args[i], program.getArguments()[i]);
			}
		}

		// Third version - variadic printing with different types
		{
			Program program;
			program.setArguments("--a", 'b', "foo bar", 482);

			CIO_ASSERT_EQUAL(args.size(), program.getArguments().size());
			for (std::size_t i = 0; i < args.size(); ++i)
			{
				CIO_ASSERT_EQUAL(args[i], program.getArguments()[i]);
			}

			// Validate calling with no values clears it
			program.setArguments();
			CIO_ASSERT_EQUAL(0u, program.getArguments().size());
		}
	}

	void ProgramTest::testExecution()
	{
#if defined _WIN32
		const char *exe = "cmd";
		int argc = 3;
		const char *argv[] = { "/c", "echo", "TESTING" };
		const char *expected = "TESTING\r\n";
#else
		const char *exe = "sh";
		int argc = 2;
		const char *argv[] = { "-c", "echo TESTING" };
		const char *expected = "TESTING\n";
#endif

		Program program;
		program.setExecutable(exe);
		program.setArguments(argc, argv);
		program.start();

		Input &output = program.getProgramOutput();
		Text outputText = output.getText();

		CIO_ASSERT_EQUAL(expected, outputText);
	}
}