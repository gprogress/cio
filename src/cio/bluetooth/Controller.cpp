/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Controller.h"

#include "Peripheral.h"

#include <cio/DeviceAddress.h>
#include <cio/Exception.h>
#include <cio/Listener.h>
#include <cio/Class.h>
#include <cio/State.h>

namespace cio
{
	namespace bluetooth
	{
		Class<Controller> Controller::sMetaclass("cio::bluetooth::Controller");
		
		const Metaclass &Controller::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		Controller::Controller() noexcept
		{
			// nothing more to do
		}
			
		Controller::Controller(const Device &in) :
			mDevice(in)
		{
			// subclasses must perform connection step
		}	
		
		Controller::Controller(Controller &&in) noexcept :
			mDevice(in.mDevice)
		{
			// nothing more to do
		}
			
		Controller &Controller::operator=(Controller &&in) noexcept
		{
			mDevice = in.mDevice;	
			return *this;
		}
			
		Controller::~Controller() noexcept = default;
		
		const Metaclass &Controller::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void Controller::clear() noexcept
		{
			mDevice.clear();
		}

		Exception Controller::validate()
		{
			Exception valid;
#if defined __linux__
			valid.fail(Reason::Unsupported, "CIO was built without DBus support, make sure libdbus1-dev is installed and recompile CIO");
#elif defined _WIN32
			valid.fail(Reason::Unimplemented, "No backened implementation for Windows exists yet");
#elif defined __APPLE__
			valid.fail(Reason::Unimplemented, "No backened implementation for Mac OS targets exists yet");
#else
			valid.fail(Reason::Unimplemented, "No backened implementation for your platform exists yet");
#endif
			return valid;
		}
			
		DeviceAddress Controller::getAddress()
		{
			return this->readLocalAddress();	
		}
			
		std::string Controller::getName()
		{
			char tmp[64] = { };
			std::size_t actual = this->readLocalName(tmp, 64);
			return std::string(tmp, tmp + actual);
		}
						
		void Controller::setDevice(const Device &device) noexcept
		{
			if (mDevice != device)
			{
				this->close();
			}
			
			mDevice = device;
		}
		
		void Controller::open()
		{
			this->openCurrentDevice();
		}
		
		void Controller::open(const Device &device)
		{
			if  (device != mDevice)
			{
				this->close();
				mDevice = device;
			}
			
			this->openCurrentDevice();
		}
		
		const Device &Controller::openDefaultDevice()
		{
			this->open(this->findDefaultDevice());
			return mDevice;
		}
		
		void Controller::openWithoutDevice()
		{
			this->open(Device());
		}
		
		std::vector<Peripheral> Controller::discoverNearbyDevices(std::chrono::milliseconds timeout)
		{
			std::vector<Peripheral> result;
			
			auto capture = [&](const Peripheral &found)
			{
				result.emplace_back(found);
			};
			
			AdvertisementListener listener(capture);
			listener.setTimeout(timeout);
			this->discoverNearbyDevices(listener);
			
			return result;
		}
				
		State Controller::discoverNearbyDevices(AdvertisementListener &listener)
		{
			Progress<std::size_t> progress;
			if (this->isLowEnergy())
			{
				State scan = this->startLowEnergyScan();
				progress = this->listenForAdvertisements(listener);
				if (scan.succeeded())
				{
					this->stopLowEnergyScan();
				}
			}
			// TODO else do a traditional inquiry
			return progress;
		}
		
		void Controller::openCurrentDevice()
		{
			// base class does nothing
		}
					
		bool Controller::isOpen() const noexcept
		{
			return static_cast<bool>(mDevice);
		}			
						
		void Controller::close() noexcept
		{
			mDevice.clear();
		}
		
		bool Controller::isLowEnergy() const noexcept
		{
			return mDevice.isLowEnergy();
		}
		
		Device Controller::findDefaultDevice()
		{
			return Device();
		}
		
		std::vector<Device> Controller::discoverLocalDevices()
		{
			return std::vector<Device>();
		}
		
		DeviceAddress Controller::readLocalAddress()
		{
			return DeviceAddress();
		}
				
		std::size_t Controller::readLocalName(char *text, std::size_t length)
		{
			std::memset(text, 0, length);
			return 0;
		}
	
		State Controller::startLowEnergyScan()
		{
			return cio::fail(Action::Scan, Reason::Unsupported);
		}
		
		Progress<std::size_t> Controller::listenForAdvertisements(AdvertisementListener &listener) noexcept
		{
			listener.fail(Reason::Unsupported);
			return cio::fail(Action::Discover, Reason::Unsupported);
		}
				
		State Controller::stopLowEnergyScan()
		{
			return cio::fail(Action::Scan, Reason::Unsupported);
		}
		
		Peripheral Controller::requestPeripheralMetadata(const DeviceAddress &mac, Task &exec)
		{
			return Peripheral();
		}
		
		Peripheral Controller::findPeripheralByName(const char *name, std::size_t length, Task &exec)
		{
			return Peripheral();
		}

		State Controller::startConnection(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Connect, Reason::Unsupported);
		}
				
		State Controller::startDisconnection(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Disconnect, Reason::Unsupported);
		}
		
		State Controller::waitForConnectionState(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Inquire, Reason::Unsupported);
		}
		
		State Controller::connect(const Peripheral &peripheral)
		{
			Task task(std::chrono::milliseconds(20000));
			return this->connect(peripheral, task);
		}
				
		State Controller::connect(const Peripheral &peripheral, std::chrono::milliseconds timeout)
		{
			Task task(timeout);
			return this->connect(peripheral, task);
		}
				
		State Controller::connect(const Peripheral &peripheral, Task &exec)
		{
			State request = this->startConnection(peripheral, exec);
			
			if (request.started() && !request.completed())
			{
				request = this->waitForConnectionState(peripheral, exec);
			}
			
			return request;
		}
		
		State Controller::disconnect(const Peripheral &peripheral, Task &exec)
		{
			State request = this->startDisconnection(peripheral, exec);
			
			if (request.started() && !request.completed())
			{
				request = this->waitForConnectionState(peripheral, exec);
			}
			
			return request;
		}
				
		State Controller::requestConnectionState(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Inquire, Reason::Unsupported);
		}
		
		State Controller::startPairing(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Connect, Reason::Unsupported);		
		}
				
		State Controller::startUnpairing(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Disconnect, Reason::Unsupported);
		}
		
		State Controller::waitForPairingState(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Inquire, Reason::Unsupported);
		}
		
		State Controller::requestPairingState(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Inquire, Reason::Unsupported);
		}
		
		State Controller::pair(const cio::bluetooth::Peripheral &peripheral, cio::Task &exec)
		{
			State request = this->startPairing(peripheral, exec);
			
			if (request.started() && !request.completed())
			{
				request = this->waitForPairingState(peripheral, exec);
			}
			
			return request;
		}
		
		State Controller::unpair(const cio::bluetooth::Peripheral &peripheral, cio::Task &exec)
		{
			State request = this->startUnpairing(peripheral, exec);
			
			if (request.started() && !request.completed())
			{
				request = this->waitForPairingState(peripheral, exec);
			}
			
			return request;
		}

		std::uintptr_t Controller::getIndex() const noexcept
		{
			return mDevice.getIndex();
		}

		const Device &Controller::getDevice() const noexcept
		{
			return mDevice;
		}

		AddressType Controller::getAddressType() const noexcept
		{
			return mDevice.getAddressType();
		}

		void Controller::setAddressType(AddressType type) noexcept
		{
			mDevice.setAddressType(type);
		}

		Controller::operator bool() const noexcept
		{
			return this->isOpen();
		}
	}
}
