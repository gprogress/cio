/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ATTERROR_H
#define CIO_BLUETOOTH_ATTERROR_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The ATT error states are a list of specific errors that can occur for attribute queries.
		 */
		enum class ATTError : std::uint8_t
		{
			/** No error code was set */
			None =			0x00,
			
			/** The attribute handle is not found on the server */
			InvalidHandle =		0x01,
			
			/** The attribute value exists but is not readable */
			Unreadable =		0x02,
			
			/** The attribute value exists but is not writable */
			Unwritable =		0x03,
			
			/** The packet sent to the server was not properly formed */
			InvalidPacket =		0x04,
			
			/** The operation required authentication/bonding for security and this was not done */
			Unauthenticated =	0x05,
			
			/** The request was understood but the server explicitly did not implement how to handle it */
			UnsupportedRequest =	0x06,
			
			/** For a large attribute value, an offset past the end of the attribute value range was given */
			InvalidOffset =		0x07,
			
			/** The operation required authentication and the authenticated user is not allowed to perform it */
			Unauthorized =		0x08,
			
			/** Too many prepare write requests were performed and the BLE system ran out of internally resources */
			PrepQueueFull =		0x09,
			
			/** Read By Type request was made on a range that did not actually have any attributes of the given type */
			MissingAttribute =	0x0A,
			
			/** Read Blob Request was made on an attribute that wasn't long enough to need it */
			AttributeNotBlob =	0x0B,
			
			/** Encryption was needed but the given key was not large enough */
			WeakEncryptionKey =	0x0C,
			
			/** The client tried to write an attribute value that was too short or too long for the attribute */
			InvalidValueLength =	0x0D,
			
			/** "Unlikely" error is a catch-all for any errors the BLE spec didn't consider */
			Unlikely =		0x0E,
			
			/** Encryption was needed by the given encryption level was not strong enough */
			WeakEncryption =	0x0F,
			
			/** Read By Group Type specified a group type that wasn't actually a standard group type */
			UnsupportedGroupType =	0x10,
			
			/** The client or server host operating system ran out of system resources needed to perform the operation */
			InsufficientResources =	0x11
		};
		
		/**
		 * Gets the CIO Status that best represents the given ATT error code.
		 * 
		 * @param code the ATT error
		 * @return the CIO status
		 */
		CIO_BLUETOOTH_API Reason status(ATTError code) noexcept;

		/**
		 * Gets a human readable text representation of an ATT error code
		 *
		 * @param code The ATT error code
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(ATTError code) noexcept;

		/**
		 * Prints a human readable text representation of an ATT error code to a C++ stream.
		 *
		 * @param s The stream
		 * @param code The ATT error code
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, ATTError code);
	}
}

#endif

