/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_INPUT_H
#define CIO_INPUT_H

#include "Types.h"

#include "Length.h"
#include "ModeSet.h"
#include "Order.h"
#include "Path.h"
#include "Progress.h"
#include "Seek.h"

#include <chrono>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Input interface represents the base interface for streaming input from a resource over a transport layer.
	 * It is defined in terms of a core set of polymorphic virtual methods and then a larger set of helper methods
	 * for common input workflows.
	 * 
	 * The core API includes the following items:
	 * Resource API - metadata and core resource management inherited from Resource
	 * get methods - core interface for obtaining information about and capabilities of the input
	 * set methods - simplified API for modifying input metadata or state
	 * read methods - simplified API for reading binary data with optional byte order conversion
	 * request methods - core interface for reading, seeking, and setting parameters on the input which return a status result
	 */
	class CIO_API Input
	{
		public:
			/**
			 * Gets the metaclass for the Input class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Input() noexcept;

			/**
			 * Gets the metaclass for the runtime type of this object.
			 *
			 * @return the metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept;

			/**
			 * Clears the current state of the Innput. This should close any files, handles, or other resources
			 * and free any memory used, but should not remove the underlying storage or data if not exclusively
			 * owned by this Input.
			 */
			virtual void clear() noexcept;

			// Open methods

			/*
			 * Checks whether the input is currently open and usable in any way.
			 *
			 * @return whether the input is open
			 */
			virtual bool isOpen() const noexcept;

			/**
			 * Opens the resource using the given open modes.
			 * The given protocol factory is used to traverse parent resources and construct
			 * underlying transport and codec mechanisms if necessary.
			 *
			 * This is the fully general open method that subclasses must override.
			 * The base class simply returns the empty mode set.
			 *
			 * @param resource The resource to open
			 * @param modes The desired resource modes to use to open the resource
			 * @param factory The factory to use to construct underlying transport and codec protocols
			 * @return the actual resource modes that the resource could be opened with
			 * @throw cio::Exception If the resource could not be opened
			 */
			virtual cio::ModeSet openWithFactory(const cio::Path& resource, cio::ModeSet modes, ProtocolFactory* factory);

			/**
			 * Opens the input to work with the given resource using the default open modes for reading.
			 * If necessary, the default protocol factory is used for underlying resources.
			 *
			 * The base class implements it using the general openWithFactory method.
			 * Subclasses may (but are not required to) override it for performance.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resource could be opened with
			 * @throw cio::Exception If the resource could not be opened
			 */
			virtual cio::ModeSet openToRead(const cio::Path &resource);

			// Read Metadata - simplified methods for sizes and seeking

			/**
			 * Requests the metadata for the currently opened resource if possible to obtain.
			 * Only the fields that are known are returned.
			 *
			 * The base implementation constructs metadata using type(), size(), location(), and modes().
			 * Subclasses may override for performance if desired.
			 *
			 * @return all known metadata for the open channel
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Metadata metadata() const;

			/**
			 * Gets the most specific path location of the resource, if possible.
			 *
			 * The base implementation returns an empty path.
			 *
			 * @return the resource location of the open channel if known
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Path location() const;

			/**
			 * Gets the overall resource type if known.
			 *
			 * The base implementation returns Resource::Unknown.
			 *
			 * @return the resource type of the open channel if known
			 */
			virtual Resource type() const noexcept;

			/**
			 * Gets the current modes of the resource to the extent known.
			 *
			 * The base implementation returns an empty mode set.
			 *
			 * @return the modes of the open channel if known
			 */
			virtual ModeSet modes() const noexcept;

			/**
			 * Gets the total size in bytes of the resource providing the input if known.
			 * If unknown or potentially unbounded, the constant CIO_UNKNOWN_LENGTH is returned, which is
			 * also the largest possible std::uint64_t size value.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH. Subclasses should always override it.
			 *
			 * @return the total resource size of the open channel in bytes
			 */
			virtual Length size() const noexcept;

			/**
			 * Checks whether the resource is empty and contains zero bytes.
			 * 
			 * @return whether the resource is emtpy
			 */
			virtual bool empty() const noexcept;

			/**
			 * Gets the total number of bytes available to be read sequentially from the current position to the read limit.
			 * If unknown or potentially unbounded, the unbounded length is returned.
			 *
			 * The base class uses size() and getReadPosition() to calculate the value if both return known quantities.
			 * Subclass may override to provide a more efficient calculation.
			 *
			 * @return the total number of bytes that may be read before end of resource
			 */
			virtual Length readable() const noexcept;

			/**
			 * Gets the current sequential read position as a byte offset relative to the beginning of the resource, if known.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH.
			 *
			 * @return the current read position
			 */
			virtual Length getReadPosition() const noexcept;

			/**
			 * Calculates the absolute read position for the given relative position and Seek mode.
			 * This method does not modify the current read position or any other state.
			 * 
			 * Note that it just calculates the effective position, it does not validate whether the position
			 * is actually usable or valid in the resource.
			 * 
			 * The base implementation uses getReadPosition() and size() where necessary.
			 * By definition Seek::Begin returns the input.
			 * Seek::None returns 0 and Seek::Unknown returns CIO_UNKNOWN_LENGTH.
			 * Subclasses may override for peformance.
			 * 
			 * @param value The relative byte offset of interest
			 * @param mode The seek mode
			 * @return the absolute position of that relative value
			 */
			virtual Length calculateReadPosition(Length value, Seek mode) const noexcept;

			/**
			 * Gets whether the read position is directly seekable on this input and if so in which direction.
			 * Currently this also specifies whether requestReadFromPosition and associated methods may succeed,
			 * based on the requested position vs. the current position.
			 *
			 * If this returns Resize::None, the input is not seekable and all reads and discards must be done
			 * by sequentially processing input.
			 *
			 * If this returns Resize::Grow, the read position may be advanced forward but not backward.
			 * If this returns Resize::Shrink, the read position may be advanced backward but not forward.
			 * If this returns Resize::Any, the input is fully random access and any seek should succeed.
			 *
			 * The base class returns Resize::Unknown.
			 *
			 * @return the read seekability status
			 */
			virtual Resize isReadSeekable() const noexcept;

			/**
			 * Recommends the most efficient read block size and alignment for reading input from this input using requestRead
			 * or requestReadFromPosition. In theory, reads that are of this size and whose offset is a multiple of this
			 * size should perform optimally.
			 *
			 * The base class default is the memory alignment of the current architecture, typically either 4 or 8.
			 * For those who care, the status field indicates whether the value was defaulted (Reason::Missing)
			 * or actually determined from the underlying resource or system.
			 *
			 * @return the recommended alignment size for reads
			 */
			virtual Response<std::size_t> recommendReadAlignment() const noexcept;

			/**
			 * Sets the maximum read timeout for all read operations not otherwise specifying a timeout.
			 * Specifying a read timeout of 0 should, if possible, make the input non-blocking.
			 * To disable a previously set read timeout, use the maximum value of milliseconds.
			 *
			 * This is primarily useful for blocking communication links such as network sockets.
			 * The base class ignores the set read timeout and fails with Reason::Unsupported.
			 *
			 * @return the outome of setting the timeout
			 * @throw cio::Exception If the class can normally set a read timeout, but doing so failed
			 */ 
			virtual State setReadTimeout(std::chrono::milliseconds timeout);
			
			/**
			 * Gets the maximum read timeout for all read operations not otherwise specifying a timeout.
			 *
			 * The base class returns 0.
			 *
			 * @return the read timeout
			 */ 
			virtual std::chrono::milliseconds getReadTimeout() const;

			/**
			 * Sets the current sequential read position to a position relative to the specified Seek mode.
			 *
			 * The base implementation calls requestSeekToRead until it completes and throws if the seek position can't be set.
			 * This should always be sufficient, but subclasses can override to improve performance.
			 *
			 * @param pos The desired load position to seek to
			 * @param mode The seek mode
			 * @throw cio::Exception If the read position could not be set
			 */
			virtual void seekToRead(Length pos, Seek mode = Seek::Begin);

			// Read API - simplified methods to read or skip data

			/**
			 * Discards and skips the given number of bytes starting from the current sequential read position, up to the end of the input.
			 *
			 * The base implementation calls requestDiscard until it completes and throws if the requested bytes can be discarded.
			 * This should always be sufficient, but subclasses can override to improve performance.
			 *
			 * @param bytes The number of bytes to skip
			 * @throw cio::Exception If the given number of bytes could not be skipped
			 */
			virtual void discard(Length bytes);

			/**
			 * Reads data from this input into the given data buffer.
			 *
			 * The base implementation calls requestRead until it completes and throws if the buffer can't be filled.
			 * This should always be sufficient, but subclasses can override to improve performance.
			 *
			 * @param data The data buffer to read into
			 * @param length The number of bytes to read
			 * @throw cio::Exception If the requested number of bytes could not be read
			 */
			virtual void read(void *data, std::size_t length);

			/**
			 * Reads data from this input into the given data buffer starting from the given position relative to the seek mode.
			 *
			 * The base implementation calls requestReadFromPosition until it completes and throws if the buffer can't be filled.
			 * This should always be sufficient, but subclasses can override to improve performance.
			 *
			 * @param data The data buffer to read into
			 * @param length The number of bytes to read
			 * @param position The position to start reading from
			 * @param mode The seek mode
			 * @throw cio::Exception If the requested number of bytes could not be read
			 */
			virtual void readFromPosition(void *data, std::size_t length, Length position, Seek mode = Seek::Begin);
			
			/**
			 * Copies the given number of bytes remaining in the input to the specified output.
			 * The length can be specified as CIO_UNKNOWN_LENGTH to copy all remaining bytes.
			 *
			 * The base implementation sets up a small buffer and iterates using requestRead and requestWrite until no more input exists
			 * or the output reports it can't accept any more writes.
			 *
			 * Subclasses can override to improve performance.
			 *
			 * @param output the output to copy bytes to
			 * @param length The maximum number of bytes to copy
			 * @return the outcome of the copy operation
			 * @throw Exception If the copy could not be performed
			 */
			virtual Progress<Length> copyTo(Output &output, Length length = CIO_UNKNOWN_LENGTH);
			
			// Template API to read primitive data types
			// These are overlaid on the read API above
			
			/**
			 * Reads a single byte from the input.
			 *
			 * @return the value read from the input
			 * @throw cio::Exception If the byte could not be read
			 */
			inline std::uint8_t get();
			
			/**
			 * Reads machine-level numeric data from the input preserving the loaded byte order

			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @return the value read from the input
			 * @throw cio::Exception If not enough bytes could be read
			 */
			template <typename T>
			inline T get();

			/**
			 * Reads machine-level numeric data from the input, converted from Big to Host byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @return the value read from the input
			 * @throw cio::Exception If not enough bytes could be read from the input
			 */
			template <typename T>
			inline T getBig();
			
			/**
			 * Reads machine-level numeric data from the input, converted from Little to Host byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @return the value read from the input
			 * @throw cio::Exception If not enough bytes could be read from the input
			 */
			template <typename T>
			inline T getLittle();

			/**
			 * Reads machine-level numeric data from the input preserving the loaded byte order

			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param value The data value to be updated
			 * @throw cio::Exception If not enough bytes could be read
			 */
			template <typename T>
			inline void get(T &value);

			/**
			 * Reads machine-level numeric data from the input, converted from Big to Host byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param value The data value to be updated
			 * @throw cio::Exception If not enough bytes could be read from the input
			 */
			template <typename T>
			inline void getBig(T &value);
			
			/**
			 * Reads machine-level numeric data from the input, converted from Little to Host byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param value The data value to be updated
			 * @throw cio::Exception If not enough bytes could be read from the input
			 */
			template <typename T>
			inline void getLittle(T &value);
			
			/**
			 * Reads a machine-level primitive number array of count values from the Input.
			 * The data is read in host byte order.
			 *
			 * @tparam <T> The data type of the array
			 * @param data The array
			 * @param count The number of elements in the array
			 * @throw cio::Exception If the array could not be fully read
			 */
			template <typename T>
			inline void getArray(T *data, std::size_t count);

			/**
			 * Reads a machine-level primitive number array of count values from the Input.
			 * Each data element is converted from Big to Host byte order.
			 *
			 * @tparam <T> The data type of the array
			 * @param data The array
			 * @param count The number of elements in the array
			 * @throw cio::Exception If the array could not be fully read
			 */
			template <typename T>
			inline void getBigArray(T *data, std::size_t count);

			/**
			 * Reads a machine-level primitive number array of count values from the Input.
			 * Each data element is converted from Little to Host byte order.
			 *
			 * @tparam <T> The data type of the array
			 * @param data The array
			 * @param count The number of elements in the array
			 * @throw cio::Exception If the array could not be fully read
			 */
			template <typename T>
			inline void getLittleArray(T *data, std::size_t count);

			// Text retrieval - this is allowed to be virtual since base implementation cannot assume existence of backing buffer

			/**
			 * Gets the variable-length text from the input up to (and including) a null terminator, the end of input, or the given number of bytes.
			 * The input current read position will be advanced by the number of bytes actually processed.
			 * 
			 * The base implementation simply calls getText because no known buffer exists.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw Exception If the text could not be read from the input
			 * @throw std::bad_alloc If the text could not be copied
			 */
			virtual Text viewText(std::size_t bytes = SIZE_MAX);

			/**
			 * Gets the text currently in the buffer up to (and including) a null terminator, the end of buffer, or the given number of bytes.
			 * The text bytes will be internalized to the returned text buffer for safety.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 * 
			 * The base implementation reads the text character by character until either a null terminator is found, the end of input is reached,
			 * or the given number of bytes are successfully read. Subclasses may override for performance.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw Exception If the text could not be read from the input
			 * @throw std::bad_alloc If the text could not be copied
			 */
			virtual Text getText(std::size_t bytes = SIZE_MAX);

			/**
			 * Gets the text currently in the buffer up to (and including) a null terminator, the end of buffer, the end of the current text line,
			 * or the given number of bytes.
			 * 
			 * The text bytes will be internalized to the returned text buffer for safety.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 *
			 * The base implementation reads the text character by character until either a null terminator is found, the end of input is reached,
			 * a newline '\n' character is read, or the given number of bytes are successfully read. Subclasses may override for performance.
			 *
			 * If a newline character is found, it will be included in the returned line.
			 * 
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw Exception If the text could not be read from the input
			 * @throw std::bad_alloc If the text could not be copied
			 */
			virtual Text getTextLine(std::size_t bytes = SIZE_MAX);

			// Fixed text retrieval

			/**
			 * Gets a fixed text of an exact number of bytes from the buffer starting at the current position.
			 * Exactly N bytes will be read regardless of null termination.
			 *
			 * @tparam <N> The number of bytes of text to read
			 * @return the fixed text read
			 */
			template <std::size_t N>
			inline FixedText<N> getFixedText();

			// Core Read Request API - virtual non-throwing methods that may perform partial actions
			// All other Read methods can be implemented in terms of these

			/**
			 * Reads up to the given number of bytes into the provided data buffer.
			 * The read is performed from the current sequential read position up to the end of the input
			 * and is performed synchronously in the calling thread.
			 *
			 * The returned state object describes the actual number of bytes read and outcome.
			 * If the outcome includes Reason::Underflow, the end of input was reached during this operation.
			 * This is only a failure if no bytes were read at all.
			 *
			 * @warning This method is the lowest level synchronous sequential read method and as such may have surprising behavior.
			 * It is always allowed to return fewer bytes than requested - even 0 - and still consider that a success, and not every input has an end
			 * so reads may return Retry or timeout indefinitely.
			 * 
			 * The base implementation fails with Reason::Unsupported.
			 * Subclasses must override at least this method to support any kind of input reading.
			 *
			 * @param data The data buffer to read into
			 * @param length The maximum number of bytes to read into the buffer from the input
			 * @return a result describing the outcome and actual number of bytes read
			 */
			virtual Progress<std::size_t> requestRead(void *data, std::size_t length) noexcept;

			/**
			 * Reads up to the given number of bytes into the provided data buffer relative to the given seek position.
			 * The read is performed from the given position which may be absolute (Seek::Begin) or relative
			 * (Seek::Current or Seek::End). 
			 *
			 * The base implementation fails with Reason::Unsupported.
			 * Inputs that do support this method should override it.
			 *
			 * Note that most inputs that do support this method only directly support Seek::Begin and all other modes
			 * must consult metadata to calculate the correct abolute position.
			 *
			 * The returned result object describes the actual number of bytes read and the outcome state.
			 * If the read could not be executed because this stream is a sequential input, the Reason should be Unsupported.
			 * If the read tries to start before the beginning of the input, the failure Reason should be Invalid.
			 * If the read tries to start after the end of the input, the failure status Reason be Underflow.
			 * If the outcome includes Reason::Underflow, the end of input was reached during this operation.
			 * This is only a failure if no bytes were read at all.
			 * 
			 * @warning This method is the lowest level synchronous random read method and as such may have surprising behavior.
			 * It is always allowed to return fewer bytes than requested - even 0 - and still consider that a success.
			 * 
			 * @param data The data buffer to read into
			 * @param length The maximum number of bytes to read into the buffer from the input
			 * @param position The byte offset to read from relative to the seek mode
			 * @param mode The seek mode to use for the starting position
			 * @return a result describing the outcome and actual number of bytes read
			 */
			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t length, Length position, Seek mode = Seek::Begin) noexcept;

			/**
			 * Discards and skips the given number of bytes starting from the current sequential read position, up to the end of the input.
			 * The returned result object describes the actual number of bytes read and the outcome state.
			 * If the outcome includes Reason::Underflow, the end of input was reached during this operation.
			 * This is only a failure if no bytes were skipped at all.
			 * 
			 * The base implementation uses requestRead() to read and ignore input data to implement the effect of skipping bytes.
			 * Subclasses should override if they can provide a more efficient way to do it such as seeking.
			 *
			 * @warning This method is the lowest level synchronous discard method and as such may have surprising behavior.
			 * It is always allowed to discard fewer bytes than requested - even 0 - and still consider that a success.
			 *
			 * @param bytes The number of bytes to skip
			 * @return a result describing the outcome and actual number of bytes skipped
			 */
			virtual Progress<Length> requestDiscard(Length bytes) noexcept;
			
			/**
			 * Copies up to the given number of bytes remaining in the input to the specified output.
			 * The length can be specified as CIO_UNKNOWN_LENGTH to copy all remaining bytes.
			 * 
			 * The returned result object describes the actual number of bytes written to output and the outcome state.
			 * If the outcome includes Reason::Underflow, the end of input was reached during this operation.
			 * This is only a failure if no bytes were copied at all.
			 *
			 * The base implementation sets up a small buffer and iterates using requestRead and requestWrite until no more input exists
			 * or the output reports it can't accept any more writes.
			 *
			 * Subclasses can override to improve performance.
			 *
			 * @param output the output to copy bytes to
			 * @param length The maximum number of bytes to copy
			 * @return the outcome of the copy operation
			 */
			virtual Progress<Length> requestCopyTo(Output &output, Length length = CIO_UNKNOWN_LENGTH);
			
			/**
			 * Sets the current sequential read position to a position relative to the given Seek mode.
			 * The returned result includes the final read position if known, whether modified or not.
			 * 
			 * Failure reasons can include:
			 * Underflow - the seek position is before the beginning of the resource
			 * Overflow - the seek position is after the end of what can be set (typically the read limit or size)
			 * Unsupported - the resource doesn't support seeking
			 * Interrupted - the seek was not successful but trying again may succeed
			 * 
			 * All other results are failures not specific to seek.
			 * 
			 * The base implementation returns CIO_UNKNOWN_LENGTH with Reason::Unsupported.
			 * Subclasses should override if they support seeking.
			 * 
			 * @param pos The desired read position to seek to
			 * @return the resulting read position and outcome
			 */
			virtual Progress<Length> requestSeekToRead(Length pos, Seek mode = Seek::Begin) noexcept;

			/**
			 * Instructs the Input that a read buffer consisting of at least the specified number of bytes should be used where relevant.
			 * This is purely an advisory call that may not actually do anything depending on the input type and hardware.
			 * It is primarily useful for decompressors and other types of inputs that need to preload data to operate in chunks.
			 *
			 * If this method succeeds with a non-zero byte count, then the Input supports
			 * buffering and the preload() method is available to control when the buffer is refilled.
			 *
			 * The base class fails with Reason::Unsupported.
			 *
			 * @param bytes The requested number of bytes for the read buffer
			 * @return the status and actual number of bytes in the read buffer
			 */
			virtual Progress<std::size_t> requestReadBuffer(std::size_t bytes) noexcept;
			
			/**
			 * Advises the input that some sequence of future read requests will be performed at the given byte range.
			 * The base implementation does nothing and returns Status::Unsupported.
			 * Subclasses may override this to start prefetching data, resize internal buffers, inform the OS, or any other potentially useful
			 * behavior to improve performance is done.
			 *
			 * @param pos The intended read start position, relative to the seek mode
			 * @param mode The seek mode of the read position
			 * @param bytes The number of bytes in range that might be read
			 * @return the acknowledgment of what this advice did, if anything
			 */
			virtual State adviseUpcomingRead(Length pos, Seek mode, std::size_t bytes) noexcept;

			// Buffer convenience methods
			
			/**
			 * Requests to read from the input into the given buffer starting at its current position until its limit.
			 * Fewer bytes may be read and this is not necessarily an error.
			 * The buffer's position will be moved by the number of bytes actually read.
			 *
			 * @param buffer The buffer to fill
			 * @return the result showing the number of bytes requested, actually read, andoutcome
			 */
			Progress<std::size_t> requestFill(Buffer &buffer) noexcept;

			/**
			 * Requests to read from the input at its given position into the given buffer starting at its current position until its limit.
			 * Fewer bytes may be read and this is not necessarily an error.
			 * The buffer's position will be moved by the number of bytes actually read.
			 *
			 * @param buffer The buffer to fill
			 * @param offset The input offset to read from
			 * @param whence The seek mode to use
			 * @return the result showing the number of bytes requested, actually read, and outcome
			 */
			Progress<std::size_t> requestFill(Buffer &buffer, Length offset, Seek whence = Seek::Begin) noexcept;

			/**
			 * Read from the input into the given buffer starting at its current position until its limit.
			 * If not enough bytes could be read, this throws an exception.
			 * The buffer's position will be moved by the number of bytes actually read.
			 *
			 * @note Depending on when the input detects the error, some bytes may be read even if an exception is thrown.
			 * The buffer will be updated to reflect the partial read even in this case.
			 *
			 * @param buffer The buffer to fill
			 * @return the result showing the number of bytes requested, actually read, and outcome
			 * @throw cio::Exception If not enough bytes could be read
			 */
			Progress<std::size_t> fill(Buffer &buffer);

			/**
			 * Read from the input at its given position into the given buffer starting at its current position until its limit.
			 * If not enough bytes could be read, this throws an exception.
			 * The buffer's position will be moved by the number of bytes actually read.
			 *
			 * @note Depending on when the input detects the error, some bytes may be read even if an exception is thrown.
			 * The buffer will be updated to reflect the partial read even in this case.
			 *
			 * @param buffer The buffer to fill
			 * @param position The input offset to read from
			 * @param whence The seek mode to use
			 * @return the result showing the number of bytes requested, actually read, and outcome
			 * @throw cio::Exception If not enough bytes could be read
			 */
			Progress<std::size_t> fill(Buffer &buffer, Length position, Seek whence = Seek::Begin);

			/**
			 * Requests to read from the input to append to the current data in the buffer.
			 * The current position is saved, then the buffer is unflipped to read into the unused portion up to the size.
			 * After the read, the buffer is flipped to restore the original position and the new limit.
			 *
			 * This method exemplifies the typical "receive a packet" workflow.
			 *
			 * @param buffer The buffer to append to and update
			 * @return the status and number of bytes received
			 */
			Progress<std::size_t> append(Buffer &buffer) noexcept;
			
			/**
			 * Requests to read from input to receive data into the buffer.
			 * The buffer is compacted to make space available, then unflipped to read into the unused portion up to the size.
			 * After the read, the buffer is flipped to set the new limit based on data actually read.
			 *
			 * This method exemplifies the typical "prefill a buffer" workflow.
			 *
			 * @param buffer The buffer to receive into and update
			 * @return the status and number of bytes received
			 */
			Progress<std::size_t> receive(Buffer &buffer) noexcept;

			/**
			 * Interprets whether the channel is currently open when used in a Boolean context.
			 *
			 * @return whether the channel is currently open
			 */
			explicit operator bool() const noexcept;

		private:
			/** The metaclass for this class */
			static Class<Input> sMetaclass;
	};
}

/* Inline implementation */

namespace cio
{	
	inline std::uint8_t Input::get()
	{
		std::uint8_t value = 0;
		this->read(&value, 1);
		return value;
	}

	template <typename T>
	inline T Input::get()
	{
		T data;
		this->read(&data, sizeof(T));
		return data;
	}
	
	template <typename T>
	inline T Input::getBig()
	{
		T data;
		this->read(&data, sizeof(T));
		return Big::order(data);
	}

	template <typename T>
	inline T Input::getLittle()
	{
		T data;
		this->read(&data, sizeof(T));
		return Little::order(data);
	}

	template <typename T>
	inline void Input::get(T &data)
	{
		this->read(&data, sizeof(T));
	}

	template <typename T>
	inline void Input::getBig(T &data)
	{
		this->read(&data, sizeof(T));
		Big::reorder(data);
	}

	template <typename T>
	inline void Input::getLittle(T &data)
	{
		this->read(&data, sizeof(T));
		Little::reorder(data);
	}

	template <std::size_t N>
	inline FixedText<N> Input::getFixedText()
	{
		FixedText<N> text;
		this->read(text.data(), N);
		return text;
	}

	template <typename T>
	inline void Input::getArray(T *data, std::size_t count)
	{
		this->read(data, sizeof(Value<T>) * count);
	}

	template <typename T>
	inline void Input::getBigArray(T *data, std::size_t count)
	{
		this->read(data, sizeof(Value<T>) * count);
		Big::reorder(data, count);
	}
	
	template <typename T>
	inline void Input::getLittleArray(T *data, std::size_t count)
	{
		this->read(data, sizeof(Value<T>) * count);
		Little::reorder(data, count);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
