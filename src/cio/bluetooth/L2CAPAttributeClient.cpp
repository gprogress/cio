/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "L2CAPAttributeClient.h"

#include "Adapter.h"
#include "ATT.h"
#include "AttributeCodec.h"
#include "Characteristic.h"
#include "Device.h"
#include "Peripheral.h"
#include "Service.h"
#include "TypeDictionary.h"
#include "TypeId.h"

#include <cio/Exception.h>
#include <cio/Class.h>
#include <cio/State.h>

namespace cio
{
	namespace bluetooth
	{
		Class<L2CAPAttributeClient> L2CAPAttributeClient::sMetaclass("cio::bluetooth::L2CAPAttributeClient");

		AttributeCodec L2CAPAttributeClient::sDefaultCodec;
	
		const Metaclass &L2CAPAttributeClient::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
			
		L2CAPChannel L2CAPAttributeClient::createTransportLink(const Device &adapter, const Peripheral &peripheral)
		{
			L2CAPChannel transportLink;

			Adapter controller(adapter);
			transportLink.connectThroughDevice(controller.getAddress(), adapter.getAddressType(), DEFAULT_ATT_CID, DEFAULT_ATT_PSM);
			transportLink.connectToRemoteDevice(peripheral.getAddress(), peripheral.getAddressType(), DEFAULT_ATT_CID, DEFAULT_ATT_PSM);
			return transportLink;
		}
		
		L2CAPAttributeClient::L2CAPAttributeClient() noexcept :
			mClientPacketSize(DEFAULT_MAX_PACKET_SIZE),
			mServerPacketSize(DEFAULT_MAX_PACKET_SIZE),
			mResponseTimeout(1000),
			mRequestRetryCount(5),
			mCodec(&sDefaultCodec)
		{
			// nothing more to do
		}
		
		L2CAPAttributeClient::L2CAPAttributeClient(L2CAPChannel &&in) noexcept :
			mTransportLink(std::move(in)),
			mClientPacketSize(DEFAULT_MAX_PACKET_SIZE),
			mServerPacketSize(DEFAULT_MAX_PACKET_SIZE),
			mResponseTimeout(1000),
			mRequestRetryCount(5),		
			mCodec(&sDefaultCodec)
		{
			if (mTransportLink)
			{
				mTransmitBuffer.allocate(DEFAULT_MAX_PACKET_SIZE);
				mReceiveBuffer.allocate(DEFAULT_MAX_PACKET_SIZE);
				mTransportLink.setReadTimeout(mResponseTimeout);
			}
		}
			
		L2CAPAttributeClient::L2CAPAttributeClient(L2CAPAttributeClient &&in) noexcept :
			AttributeClient(std::move(in)),
			mTransportLink(std::move(in.mTransportLink)),
			mClientPacketSize(in.mClientPacketSize),
			mServerPacketSize(in.mServerPacketSize),
			mResponseTimeout(in.mResponseTimeout),
			mRequestRetryCount(in.mRequestRetryCount),
			mTransmitBuffer(std::move(in.mTransmitBuffer)),
			mReceiveBuffer(std::move(in.mReceiveBuffer)),
			mCodec(in.mCodec)
		{
			in.mClientPacketSize = DEFAULT_MAX_PACKET_SIZE;
			in.mServerPacketSize = DEFAULT_MAX_PACKET_SIZE;
			in.mCodec = &sDefaultCodec;
		}
			
		L2CAPAttributeClient &L2CAPAttributeClient::operator=(L2CAPAttributeClient &&in) noexcept
		{
			if (this != &in)
			{
				AttributeClient::operator=(std::move(in));
				
				mTransportLink = std::move(in.mTransportLink);
				
				mClientPacketSize = in.mClientPacketSize;
				in.mClientPacketSize = DEFAULT_MAX_PACKET_SIZE;
				
				mServerPacketSize = in.mServerPacketSize;
				in.mServerPacketSize = DEFAULT_MAX_PACKET_SIZE;
				
				mResponseTimeout = in.mResponseTimeout;
				mRequestRetryCount = in.mRequestRetryCount;
				
				mTransmitBuffer = std::move(in.mTransmitBuffer);
				mReceiveBuffer = std::move(in.mReceiveBuffer);
				
				mCodec = in.mCodec;
				in.mCodec = &sDefaultCodec;
			}
			
			return *this;
		}
			
		L2CAPAttributeClient::~L2CAPAttributeClient() noexcept = default;
		
		const Metaclass &L2CAPAttributeClient::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void L2CAPAttributeClient::clear() noexcept
		{
			this->disconnect();
		
			mCodec = &sDefaultCodec;
		
			mResponseTimeout = std::chrono::milliseconds(1000);
			mRequestRetryCount = 5;
					
			AttributeClient::clear();
		}
		
		void L2CAPAttributeClient::connect(const Peripheral &peripheral)
		{
			this->disconnect();
			
			this->openTransportLink(Device(), peripheral);
		}
					
		void L2CAPAttributeClient::connect(const Device &adapter, const Peripheral &peripheral)
		{
			this->disconnect();
			this->openTransportLink(adapter, peripheral);
		}
		
		bool L2CAPAttributeClient::isOpen() const noexcept
		{
			return mTransportLink.isOpen();
		}
		
		void L2CAPAttributeClient::disconnect() noexcept
		{
			mTransportLink.clear();
			mTransmitBuffer.clear();
			mReceiveBuffer.clear();
			mClientPacketSize = DEFAULT_MAX_PACKET_SIZE;
			mServerPacketSize = DEFAULT_MAX_PACKET_SIZE;
			
			this->clearAllSubscriptions();
		}
		
		L2CAPChannel &L2CAPAttributeClient::getTransportLink() noexcept
		{
			return mTransportLink;
		}
		
		void L2CAPAttributeClient::adoptTransportLink(L2CAPChannel &&in)
		{
			mTransportLink = std::move(in);
			if (mTransportLink)
			{
				mTransportLink.setReadTimeout(mResponseTimeout);
			
				std::size_t negotiated = std::min(mClientPacketSize, mServerPacketSize);
				mTransmitBuffer.resize(negotiated);
				mReceiveBuffer.resize(negotiated);
			}
		}
		
		L2CAPChannel &L2CAPAttributeClient::openTransportLink(const Device &adapter, const Peripheral &peripheral)
		{
			mTransportLink = L2CAPAttributeClient::createTransportLink(adapter, peripheral);
			mTransportLink.setReadTimeout(mResponseTimeout);
			
			std::size_t negotiated = std::min(mClientPacketSize, mServerPacketSize);
			mTransmitBuffer.resize(negotiated);
			mReceiveBuffer.resize(negotiated);
		
			return mTransportLink;
		}
				
		std::size_t L2CAPAttributeClient::getClientMaxPacketSize() const noexcept
		{
			return mClientPacketSize;
		}
				
		std::size_t L2CAPAttributeClient::getServerMaxPacketSize() const noexcept
		{
			return mServerPacketSize;
		}
		
		std::size_t L2CAPAttributeClient::negotiateMaxPacketSize(std::size_t requested)
		{
			std::unique_lock<std::mutex> lock(mMutex);
			
			mClientPacketSize = requested;
			
			// Do request with current (presumably default) packet size
			Buffer &request = this->requestTransmitBuffer();
			mCodec->encodeMaxPacketSizeRequest(mTransmitBuffer, requested);
			Buffer &response = this->requestResponse(request.flip());
			mServerPacketSize = mCodec->decodeMaxPacketSizeResponse(response);
			
			std::size_t negotiated = std::min(mClientPacketSize, mServerPacketSize);
			mTransmitBuffer.resize(negotiated);
			mReceiveBuffer.resize(negotiated);
			
			this->setMaxPacketSize(negotiated);
			
			return negotiated;
		}
		
		UniqueId L2CAPAttributeClient::getAttributeType(std::uint16_t handle)
		{
			UniqueId type;
			
			if (handle != 0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeFindInformationRequest(request, handle, handle);
				Buffer &response = this->requestResponse(request.flip());
				Progress<std::uint8_t> preamble = mCodec->decodeFindInformationPreamble(response);
				
				Attribute attribute;
				
				if (preamble.count == 4)
				{
					attribute = mCodec->decodeNextShortAttributeResponse(response);
				}
				else
				{
					attribute = mCodec->decodeNextAttributeResponse(response);
				}
				
				type = attribute.getType();
			}
			else
			{
				throw cio::Exception(Reason::Invalid, "Cannot get attribute for invalid handle 0x0000");
			}

			return type;
		}
		
		std::vector<Attribute> L2CAPAttributeClient::discoverAttributes(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Attribute> attributes;
			
			std::uint16_t previousEnd = std::max<std::uint16_t>(start, 0x0001u) - 1; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (previousEnd >= targetEnd)
			{
				std::swap(previousEnd, targetEnd);
			}
			
			Progress<std::size_t> result;
			
			do
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeFindInformationRequest(request, previousEnd + 1, targetEnd);
				Buffer &response = this->requestResponse(request.flip());
				
				std::size_t start = attributes.size();
				result += mCodec->decodeFindInformationResponse(response, attributes);
				for (std::size_t i = start; i < attributes.size(); ++i)
				{
					attributes[i].setClient(this);
				}
				
				if (!attributes.empty())
				{
					previousEnd = attributes.back().getHandle();
				}
				
			} while (!result.completed());

			return attributes;
		}
		
		std::uint16_t L2CAPAttributeClient::findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{			
			std::uint16_t targetStart = std::max<std::uint16_t>(start, 0x0001u) ; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (targetStart >= targetEnd)
			{
				std::swap(targetStart, targetEnd);
			}
		
			std::unique_lock<std::mutex> lock(mMutex);
			
			Buffer &request = this->requestTransmitBuffer();	
			mCodec->encodeReadByTypeRequest(request, targetStart, targetEnd);
			Buffer &response = this->requestResponse(request.flip());
			return mCodec->decodeReadByTypeResponse(response);
		}
		
		Service L2CAPAttributeClient::getService(std::uint16_t handle)
		{
			Service service;
			
			if (handle != 0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeDiscoverServicesRequest(request, handle, handle);
				Buffer &response = this->requestResponse(request.flip());

				Progress<std::uint8_t> preamble = mCodec->decodeDiscoverServicesPreamble(response);
				if (preamble.count == 6)
				{
					service = mCodec->decodeNextShortServiceDiscovery(response);
				}
				else if (preamble.count == 20)
				{
					service = mCodec->decodeNextServiceDiscovery(response);
				}
				else
				{
					throw cio::Exception(Reason::Missing, "No service existed at the requested handle");
				}
				
				service.setClient(this);
			}
			else
			{
				throw cio::Exception(Reason::Invalid, "Cannot get service for requested invalid handle 0x0000");
			}
			
			return service;
		}
				
		std::vector<Service> L2CAPAttributeClient::discoverPrimaryServices(std::uint16_t start, std::uint16_t end)
		{	
			std::vector<Service> services;
			
			std::uint16_t previousEnd = std::max<std::uint16_t>(start, 0x0001u) - 1; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (previousEnd >= targetEnd)
			{
				std::swap(previousEnd, targetEnd);
			}
			
			Progress<std::size_t> result;
			
			do
			{		
				sMetaclass.info() << "Searching for services in handle range: " << (previousEnd + 1) << " to " << targetEnd;
			
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();
				mCodec->encodeDiscoverServicesRequest(request, previousEnd + 1, targetEnd);
				Buffer &response = this->requestResponse(request.flip());
				std::size_t start = services.size();
				result += mCodec->decodeDiscoverServicesResponse(response, services);
				
				for (std::size_t i = start; i < services.size(); ++i)
				{
					Service &service = services[i];
					service.setClient(this);
					sMetaclass.info() << "Found service " << service.getServiceName() << " (" << service.getServiceType() << ") at handle range " << service.getHandle() << " to " << service.getEndHandle();
				}
				
				if (!services.empty())
				{
					previousEnd = services.back().getEndHandle();
				}
				
			} while (!result.completed());
			
			return services;
		}
		
		Service L2CAPAttributeClient::findPrimaryServiceInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end)
		{
			Service service;
			
			// TODO optimize
			std::vector<Service> services = this->discoverPrimaryServices(start, end);
			
			for (Service &candidate : services)
			{
				if (candidate.getServiceType() == id)
				{
					service = std::move(candidate);
					break;
				}
			}
			
			return service;
		}
		
		Characteristic L2CAPAttributeClient::getCharacteristic(std::uint16_t handle)
		{
			Characteristic c;
			
			if (handle != 0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeDiscoverCharacteristicsRequest(request, handle, handle);
				Buffer &response = this->requestResponse(request.flip());
				Progress<std::uint8_t> preamble = mCodec->decodeDiscoverCharacteristicsPreamble(response);
				
				if (preamble.count == 7)
				{
					c = mCodec->decodeNextShortCharacteristic(response);
				}
				else if (preamble.count == 21)
				{
					c = mCodec->decodeNextCharacteristic(response);
				}
				else
				{
					throw cio::Exception(Reason::Missing, "No characteristic at the given handle");
				}
				
				// WARNING: in general we do not know the characteristic end handles here, because you need to know service boundaries
				// and adjacent characteristics to discover that
				// So we leave the end handle 0x0000 to indicate it needs to be found

				c.setClient(this);
			}
			else
			{
				throw cio::Exception(Reason::Invalid, "Cannot get characteristic for invalid handle 0x0000");
			}
			
			return c;
		}
		
		std::vector<Characteristic> L2CAPAttributeClient::discoverCharacteristics(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Characteristic> characteristics;
			
			std::uint16_t previousEnd = std::max<std::uint16_t>(start, 0x0001u) - 1; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (previousEnd >= targetEnd)
			{
				std::swap(previousEnd, targetEnd);
			}
			
			Progress<std::size_t> result;
			
			do
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeDiscoverCharacteristicsRequest(request, previousEnd + 1, targetEnd);
				Buffer &response = this->requestResponse(request.flip());
				
				std::size_t start = characteristics.size();
				result += mCodec->decodeDiscoverCharacteristicsResponse(response, characteristics);
				for (std::size_t i = start; i < characteristics.size(); ++i)
				{
					characteristics[i].setClient(this);
				}
				
				// WARNING: in general we do not know the characteristic end handles here, because you need to know service boundaries
				// and adjacent characteristics to discover that
				// So we leave the end handle 0x0000 to indicate it needs to be found
				
				if (!characteristics.empty())
				{
					previousEnd = characteristics.back().getValueHandle();
				}
				
			} while (!result.completed());

			return characteristics;
		}
		
		Characteristic L2CAPAttributeClient::findCharacteristicInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end)
		{
			Characteristic c;
			
			// TODO optimize
			std::vector<Characteristic> chars = this->discoverCharacteristics(start, end);
			
			for (Characteristic &candidate : chars)
			{
				if (candidate.getValueType() == id)
				{
					c = std::move(candidate);
					break;
				}
			}
			
			return c;
		}		
		
		Descriptor L2CAPAttributeClient::getDescriptor(std::uint16_t handle)
		{
			Descriptor descriptor;
			
			if (handle != 0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeFindInformationRequest(request, handle, handle);
				Buffer &response = this->requestResponse(request.flip());
				Progress<std::uint8_t> preamble = mCodec->decodeFindInformationPreamble(response);
				
				Attribute attribute;
				
				if (preamble.count == 4)
				{
					attribute = mCodec->decodeNextShortAttributeResponse(response);
				}
				else
				{
					attribute = mCodec->decodeNextAttributeResponse(response);
				}
				
				if (attribute.getType().isGroupType())
				{
					throw cio::Exception(Reason::Invalid, "Attribute at specified handle is not a descriptor");
				}
				
				descriptor = attribute;
				descriptor.setClient(this);
			}
			else
			{
				throw cio::Exception(Reason::Invalid, "Cannot get attribute for invalid handle 0x0000");
			}

			return descriptor;
		}
		
		std::vector<Descriptor> L2CAPAttributeClient::discoverDescriptors(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Descriptor> descriptors;
			
			std::uint16_t previousEnd = std::max<std::uint16_t>(start, 0x0001u) - 1; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (previousEnd >= targetEnd)
			{
				std::swap(previousEnd, targetEnd);
			}
			
			Progress<std::size_t> result;
			
			do
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeFindInformationRequest(request, previousEnd + 1, targetEnd);
				Buffer &response = this->requestResponse(request.flip());
				
				Progress<std::uint8_t> preamble = mCodec->decodeFindInformationPreamble(response);
				
				previousEnd = targetEnd;
				
				if (preamble.count == 4)
				{
					while (response.remaining())
					{
						Attribute attribute = mCodec->decodeNextShortAttributeResponse(response);
						previousEnd = attribute.getHandle();
						
						if (!attribute.getType().isGroupType())
						{
							descriptors.emplace_back(std::move(attribute));
						}
					}
				}
				else
				{
					while (response.remaining())
					{
						Attribute attribute = mCodec->decodeNextAttributeResponse(response);
						previousEnd = attribute.getHandle();
						
						if (!attribute.getType().isGroupType())
						{
							descriptors.emplace_back(std::move(attribute));
						}
					}
				}
				
			} while (!result.completed());

			return descriptors;
		}
		
		Descriptor L2CAPAttributeClient::findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			Descriptor attribute;
			
			if (type.isGroupType())
			{
				throw cio::Exception(Reason::Invalid, "Specified type is not a descriptor type");
			}
			
			std::uint16_t targetStart = std::max<std::uint16_t>(start, 0x0001u) ; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (targetStart >= targetEnd)
			{
				std::swap(targetStart, targetEnd);
			}
		
			std::unique_lock<std::mutex> lock(mMutex);
			
			Buffer &request = this->requestTransmitBuffer();	
			mCodec->encodeReadByTypeRequest(request, targetStart, targetEnd);
			Buffer &response = this->requestResponse(request.flip());
			std::uint16_t handle = mCodec->decodeReadByTypeResponse(response);
			
			attribute.setHandle(handle);
			attribute.setType(type);
			attribute.setClient(this);
			
			return attribute;	
		}
		
		Progress<std::size_t> L2CAPAttributeClient::readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Read);
			std::lock_guard<std::mutex> lock(mMutex);
			std::size_t actual = 0;
			
			if (offset == 0)
			{
				Buffer &request = this->requestTransmitBuffer();
				mCodec->encodeReadRequest(request, handle);
				Buffer &response = this->requestResponse(request.flip());
				mCodec->decodeReadResponse(response);
			
				actual = response.remaining();
				listener(handle, response.current(), actual);
				result.update(actual);
				result.succeed();
			}

			return result;
		}
				
		Progress<std::size_t> L2CAPAttributeClient::readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end)
		{
			Progress<std::size_t> result(Action::Read);
			std::uint16_t previousEnd = std::max<std::uint16_t>(start, 0x0001u) - 1; // clamp start to be at least 1
			std::uint16_t targetEnd = std::max<std::uint16_t>(end, 0x0001u); // if the user passed an end of 0, force to 1
			
			// If the range was reversed, then fix that
			if (previousEnd >= targetEnd)
			{
				std::swap(previousEnd, targetEnd);
			}
			
			do
			{
				std::unique_lock<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();	
				mCodec->encodeReadByTypeRequest(request, type, previousEnd + 1, targetEnd);
				Buffer &response = this->requestResponse(request.flip());
				
				Progress<std::uint16_t> preamble = mCodec->decodeReadByTypePreamble(response);

				if (preamble.count > 0)
				{
					std::size_t itemLength = preamble.count;
					std::size_t itemCount = response.remaining() / itemLength;
					
					std::uint16_t handle = 0;
					
					for (std::size_t i = 0; i < itemCount; ++i)
					{
						handle = response.getLittle<std::uint16_t>();
						listener(handle, response.current(), itemLength);
						response.skip(itemLength);
						++result.count;
					}
					
					previousEnd = handle;
					
					// in the rare case where the last Progress handle is also the last possible handle
					// mark this operation as complete						
					if (handle == 0xFFFFu)
					{
						result.succeed();
					}
				}
				
			} while (!result.completed());
			
			return result;
		}
				
		Progress<std::size_t> L2CAPAttributeClient::writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Write);
			
			if (offset == 0)
			{	
				std::lock_guard<std::mutex> lock(mMutex);
				Buffer &request = this->requestTransmitBuffer();
				mCodec->encodeWriteRequest(request, handle, buffer, requested);
				Buffer &response = this->requestResponse(request.flip());
				mCodec->decodeWriteResponse(response);
				
				result.succeed();
			}
			
			return result;
		}
				
		Progress<std::size_t> L2CAPAttributeClient::sendValue(std::uint16_t handle, const void *buffer, std::size_t requested)
		{
			Progress<std::size_t> result;
						
			std::lock_guard<std::mutex> lock(mMutex);
			Buffer &command = this->requestTransmitBuffer();
			mCodec->encodeWriteCommand(command, handle, buffer, requested);
			this->transmitPacket(command.flip());
			
			return result;
		}		
		
		const Subscription *L2CAPAttributeClient::subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval)
		{
			const Subscription *sub = nullptr;
			
			return sub;
		}
				
		State L2CAPAttributeClient::executeSubscriptions(Task &exec) noexcept
		{
			State status(Action::Notify);
			
			status.fail(Reason::Unsupported);
			
			return status;
		}
				
		State L2CAPAttributeClient::unsubscribe(const Subscription *subscription)
		{
			State status(Action::Unsubscribe);
			
			status.fail(Reason::Unsupported);
			
			return status;
		}
				
		Progress<std::size_t> L2CAPAttributeClient::clearSubscriptions(std::uint16_t handle)
		{
			State status(Action::Unsubscribe);
			
			status.fail(Reason::Unsupported);
			
			return status;
		}
				
		Progress<std::size_t> L2CAPAttributeClient::clearAllSubscriptions()
		{
			Progress<std::size_t> removed;
			
			return removed;
		}
			
		std::unique_lock<std::mutex> L2CAPAttributeClient::lock()
		{
			return std::unique_lock<std::mutex>(mMutex);
		}
		
		std::chrono::milliseconds L2CAPAttributeClient::getResponseTimeout() const noexcept
		{
			return mResponseTimeout;
		}

		void L2CAPAttributeClient::setResponseTimeout(std::chrono::milliseconds timeout)
		{
			mResponseTimeout = timeout;
			if (mTransportLink)
			{
				mTransportLink.setReadTimeout(timeout);
			}
		}
				
		std::size_t L2CAPAttributeClient::getRequestRetryCount() const noexcept
		{
			return mRequestRetryCount;
		}

		void L2CAPAttributeClient::setRequestRetryCount(std::size_t count) noexcept
		{
			mRequestRetryCount = count;
		}
		
		Buffer &L2CAPAttributeClient::requestTransmitBuffer()
		{
			mTransmitBuffer.reset();
			return mTransmitBuffer;
		}
		
		Buffer &L2CAPAttributeClient::requestResponse(Buffer &request)
		{
			return this->requestResponse(request, this->requestReceiveBuffer());
		}
		
		Buffer &L2CAPAttributeClient::requestResponse(Buffer &request, Buffer &response)
		{	
			if (!request.available())
			{
				throw cio::Exception(Reason::Underflow, "Attempted to send empty BLE request packet");		
			}
			
			if (request.remaining () > this->getMaxPacketSize())
			{
				sMetaclass.info() << "Request packet size " << request.remaining() << " larger than negotiated MTU " << this->getMaxPacketSize();
				throw cio::Exception(Reason::Overflow, "Attempted to send BLE request larger than negotiated MTU");
			}

			for (std::size_t i = 0; i <= mRequestRetryCount; ++i)
			{
				Progress<std::size_t> sent = mTransportLink.requestWrite(request.current(), request.remaining());
				if (sent.failed())
				{
					throw cio::Exception(sent, "Could not send BLE request");
				}

				
				std::size_t mark = response.position();
				std::size_t end = response.limit();
				
				this->receiveResponse(response);
				if (response.available())
				{
					request.skip(request.remaining());
					break;
				}
				else
				{
					response.limit(end);
					response.position(mark);
				}
			}
			
			// We can only get here if the retry count is exceeded
			if (request.available())
			{
				throw cio::Exception(Reason::Unanswered, "No BLE response received for request");
			}
			
			return response;
		}
				
		void L2CAPAttributeClient::transmitPacket(Buffer &packet)
		{
			if (!packet.available())
			{
				throw cio::Exception(Reason::Underflow, "Attempted to send empty packet");
			}
			
			if (packet.remaining () > this->getMaxPacketSize())
			{
				sMetaclass.info() << "Packet size " << packet.remaining() << " larger than negotiated MTU " << this->getMaxPacketSize();
				throw cio::Exception(Reason::Overflow, "Attempted to send BLE packet larger than negotiated MTU");
			}
			
			// should throw on failure	
			mTransportLink.drain(packet);
		}
				
		Buffer &L2CAPAttributeClient::requestReceiveBuffer()
		{
			mReceiveBuffer.reset();
			return mReceiveBuffer;
		}
				
		Buffer &L2CAPAttributeClient::receiveResponse()
		{
			Progress<std::size_t> progress = mTransportLink.receive(mReceiveBuffer);
			
			if (progress.failed())
			{
				throw Exception(progress);
			}

			while (this->processNotification(mReceiveBuffer))
			{		
				progress = mTransportLink.receive(mReceiveBuffer);
				
				if (progress.failed())
				{
					throw Exception(progress);
				}
			}

			return mReceiveBuffer;
		}
				
		Buffer &L2CAPAttributeClient::receiveResponse(Buffer &packet)
		{
			Progress<std::size_t> progress = mTransportLink.receive(packet);
			
			if (progress.failed())
			{
				throw Exception(progress);
			}
			
			while (this->processNotification(packet))
			{
				progress = mTransportLink.receive(packet);
				
				if (progress.failed())
				{
					throw Exception(progress);
				}
			}
			
			return packet;
		}
							
		Buffer &L2CAPAttributeClient::receiveRawPacket()
		{
			Progress<std::size_t> progress = mTransportLink.receive(mReceiveBuffer);
			
			if (progress.failed())
			{
				throw Exception(progress);
			}
			
			return mReceiveBuffer;	
		}
				
		Buffer &L2CAPAttributeClient::receiveRawPacket(Buffer &packet)
		{
			Progress<std::size_t> progress =  mTransportLink.receive(mReceiveBuffer);
			
			if (progress.failed())
			{
				throw Exception(progress);
			}
			
			return packet;
		}
		
		bool L2CAPAttributeClient::processNotification(Buffer &packet)
		{
			bool valid = false;
			if (packet.remaining())
			{
				// peek at the first byte of the packet for the opcode
				ATT opcode = static_cast<ATT>(*packet.current());
				if (opcode == ATT::Notify)
				{
					sMetaclass.info() << "Received BLE notification";
					std::uint16_t handle = mCodec->decodeNotify(packet);
					mSubscriptions.notify(handle, packet.current(), packet.remaining());
					valid = true;
				}
				else if (opcode == ATT::Indicate)
				{
					sMetaclass.info() << "Received BLE indication";
					std::uint16_t handle = mCodec->decodeIndicateRequest(packet);
					mSubscriptions.notify(handle, packet.current(), packet.remaining());
					
					// bypass buffer to directly write an indicate confirmation response since it's just a single byte
					mTransportLink.put(ATT::Confirm);
					valid = true;
				}
			}
			
			return valid;
		}	
				
		AttributeCodec &L2CAPAttributeClient::getCodec() noexcept
		{
			return *mCodec;
		}
				
		const AttributeCodec &L2CAPAttributeClient::getCodec() const noexcept
		{
			return *mCodec;
		}		
		
		void L2CAPAttributeClient::setCodec(AttributeCodec &codec) noexcept
		{
			mCodec = &codec;
		}
	}
}
