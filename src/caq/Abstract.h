/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_ABSTRACT_H
#define CAQ_ABSTRACT_H

#include "Types.h"

namespace caq
{
	/**
	 * The Abstract accessor implements the base array accessor interface that is independent of any data type or implementation.
	 */
	class Abstract
	{
		public:
			/**
			 * Destructor.
			 */
			inline virtual ~Abstract() noexcept = default;

			/**
			 * Gets the number of array elements associated with this accessor.
			 * 
			 * @return The number of array elements
			 */
			virtual unsigned long long size() const noexcept = 0;

			/**
			 * Gets the number of components per array element associated with this accessor.
			 * 
			 * @return The number of components per array element
			 */
			virtual std::size_t components() const noexcept = 0;

			/**
			 * Interprets whether this accessor is usable.
			 * This should be used to indicate that the accessor is configured such that data is readable and (if appropriate) writable.
			 * 
			 * @return Whether the accessor is usable
			 */
			virtual explicit operator bool() const noexcept = 0;
	};
}

#endif
