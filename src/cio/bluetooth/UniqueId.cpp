/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "UniqueId.h"

#include "TypeId.h"

#include <cio/Parse.h>
#include <cio/Print.h>
#include <cio/Status.h>
#include <cio/Text.h>

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const std::uint64_t UniqueId::BLUETOOTH_UUID_HIGH(0x1000ull);
				
		const std::uint64_t UniqueId::BLUETOOTH_UUID_LOW(0x800000805F9B34FB);
			
		const UniqueId UniqueId::BLUETOOTH_BASE_UUID(UniqueId::BLUETOOTH_UUID_HIGH, UniqueId::BLUETOOTH_UUID_LOW);

		UniqueId::UniqueId() noexcept
		{
			// nothing more to do
		}
		
		UniqueId::UniqueId(std::uint64_t high, std::uint64_t low) :
			cio::UniqueId(high, low)
		{
			// nothing more to do
		}
			
		UniqueId::UniqueId(const cio::UniqueId &id) noexcept :
			cio::UniqueId(id)
		{
			// nothing more to do
		}
		
		UniqueId::UniqueId(std::uint16_t shortId) noexcept
		{
			this->setShortId(shortId);
		}
		
		UniqueId::UniqueId(TypeId type) noexcept
		{
			this->setBluetoothId(type);
		}
		
		UniqueId::UniqueId(const char *text) noexcept
		{
			this->parse(text);
		}
		
		UniqueId::UniqueId(const std::string &text) noexcept
		{
			this->parse(text);
		}
		
		UniqueId::UniqueId(const UniqueId &in) noexcept :
			cio::UniqueId(in)
		{
			// nothing more to do
		}
		
		UniqueId &UniqueId::operator=(const UniqueId &in) noexcept
		{
			cio::UniqueId::operator=(in);
			return *this;
		}
		
		UniqueId &UniqueId::operator=(const cio::UniqueId &in) noexcept
		{
			cio::UniqueId::operator=(in);
			return *this;
		}
		
		UniqueId &UniqueId::operator=(std::uint16_t shortId) noexcept
		{
			this->setShortId(shortId);
			return *this;
		}	
		
		UniqueId &UniqueId::operator=(TypeId shortId) noexcept
		{
			this->setBluetoothId(shortId);
			return *this;
		}
		
		UniqueId::~UniqueId() noexcept = default;
		
		bool UniqueId::isBluetoothId() const noexcept
		{
			return this->low() == UniqueId::BLUETOOTH_UUID_LOW && (this->high() & 0xFFFF0000FFFFFFFFull) == UniqueId::BLUETOOTH_UUID_HIGH;		
		}
		
		std::uint16_t UniqueId::getShortId() const noexcept
		{
			std::uint16_t shortId = 0;
			
			if (this->low() == UniqueId::BLUETOOTH_UUID_LOW)
			{
				std::uint64_t high = this->high();
				if ((high & 0xFFFF0000FFFFFFFFull) == UniqueId::BLUETOOTH_UUID_HIGH)
				{
					shortId = static_cast<std::uint16_t>(high >> 32);
				}
			}
			
			return shortId;
		}
		
		TypeId UniqueId::getBluetoothId() const noexcept
		{
			TypeId typeId = TypeId::None;
			
			if (this->low() == UniqueId::BLUETOOTH_UUID_LOW)
			{
				std::uint64_t high = this->high();
				if ((high & 0xFFFF0000FFFFFFFFull) == UniqueId::BLUETOOTH_UUID_HIGH)
				{
					typeId = static_cast<TypeId>(high >> 32);
				}
			}
			
			return typeId;
		}
		
		void UniqueId::setShortId(std::uint16_t shortId) noexcept
		{
			if (shortId != 0)
			{
				this->set(UniqueId::BLUETOOTH_UUID_HIGH | (static_cast<std::uint64_t>(shortId) << 32), UniqueId::BLUETOOTH_UUID_LOW);
			}
			else
			{
				this->clear();
			}
		}
		
		void UniqueId::setBluetoothId(TypeId type) noexcept
		{
			if (type != TypeId::None)
			{
				this->set(UniqueId::BLUETOOTH_UUID_HIGH | (static_cast<std::uint64_t>(type) << 32), UniqueId::BLUETOOTH_UUID_LOW);
			}
			else
			{
				this->clear();
			}		
		}
				
		bool UniqueId::isGroupType() const noexcept
		{
			TypeId type = this->getBluetoothId();
			return type == TypeId::PrimaryService || type == TypeId::SecondaryService || type == TypeId::Characteristic;
		}
		
		std::size_t UniqueId::print(char *buffer, std::size_t length) const noexcept
		{
			std::size_t needed = 0;
			std::uint16_t shortId = Big::order(this->getShortId());
			if (shortId != 0)
			{
				needed = cio::printBytes(&shortId, 2, buffer, length);
			}
			else
			{
				needed = cio::UniqueId::print(buffer, length);
			}
			return needed;
		}
				
		std::string UniqueId::print() const
		{
			char tmp[36];
			std::size_t actual = this->print(tmp, 36);
			return std::string(tmp, tmp + actual);
		}	
			
		TextParseStatus UniqueId::parse(const Text &text) noexcept
		{
			TextParseStatus parse;
			
			if (text)
			{
				TextParse<std::uint16_t> shortId = cio::parseUnsigned16(text);
				if (shortId.exact())
				{
					this->setShortId(shortId);
					parse = shortId;
				}
				else
				{
					parse = cio::UniqueId::parse(text);
				}
			}
			else
			{
				this->clear();
				parse.status = Validation::Missing;
			}
			
			return parse;
		}

		std::ostream &operator<<(std::ostream &stream, const UniqueId &value)
		{
			char tmp[37];
			std::size_t actual = value.print(tmp, 37);	
			return stream.write(tmp, actual);
		}
		
		UniqueId swapBytes(const UniqueId &value) noexcept
		{
			return UniqueId(cio::swapBytes(value.low()), cio::swapBytes(value.high()));
		}
	}
}
