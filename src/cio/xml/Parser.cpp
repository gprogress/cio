/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Parser.h"

#include <expat.h>
#include <cstring>

#include "Handler.h"
#include "TreeBuilder.h"

#include <cio/File.h>
#include <cio/Progress.h>
#include <cio/ModeSet.h>

namespace
{
	/**
	* The Safe Expat Parser frees the parser
	* if an exception gets thrown.
	*/
	class SafeExpatParser
	{
		public:
			SafeExpatParser()
			{
				mParser = XML_ParserCreate(0);
			}

			~SafeExpatParser()
			{
				XML_ParserFree(mParser);
			}

			operator XML_Parser()
			{
				return mParser;
			}

		private:
			XML_Parser mParser;
	};
}

namespace cio
{
	namespace xml
	{
		Parser::Parser() = default;

		Parser::~Parser() = default;

		void Parser::parseText(Text text, Handler &handler)
		{
			SafeExpatParser expatParser;
			std::pair<XML_Parser, Handler *> userData(expatParser, &handler);
			// todo set input
			XML_SetElementHandler(expatParser, &startElement, &endElement);
			XML_SetCharacterDataHandler(expatParser, &processText);
			XML_SetCommentHandler(expatParser, &processComment);
			XML_SetUserData(expatParser, &userData);
			handler.startDocument();
			XML_Status parseProgress = XML_Parse(expatParser, text.data(), (int)text.size(), false);

			if (parseProgress == XML_STATUS_ERROR)
			{
				XML_Error error = XML_GetErrorCode(expatParser);

				if (error != XML_ERROR_ABORTED)
				{
					std::string message("XML parse error: ");
					message += XML_ErrorString(XML_GetErrorCode(expatParser));
					throw std::runtime_error(message);
				}
			}
			else if (parseProgress == XML_STATUS_SUSPENDED)
			{
				XML_StopParser(expatParser, XML_FALSE);
			}
		}

		void Parser::parseFile(const cio::Path &path, Handler &handler, std::size_t bufferSize /* = 4096 */)
		{
			cio::File stream(path, cio::ModeSet::readonly());
			this->parse(stream, handler, bufferSize);
		}

		void Parser::parse(Input &input, Handler &handler, std::size_t bufferSize /* = 4096 */)
		{
			SafeExpatParser expatParser;
			std::pair<XML_Parser, Handler *> userData(expatParser, &handler);
			XML_SetElementHandler(expatParser, &startElement, &endElement);
			XML_SetCharacterDataHandler(expatParser, &processText);
			XML_SetCommentHandler(expatParser, &processComment);
			XML_SetUserData(expatParser, &userData);
			handler.startDocument();
			bool keepGoing = true;
			std::string errorText;

			while (keepGoing)
			{
				char *buffer = reinterpret_cast<char *>(XML_GetBuffer(expatParser, static_cast<int>(bufferSize)));

				if (buffer)
				{
					Progress<std::size_t> actualRead = input.requestRead(buffer, bufferSize);
					keepGoing = !actualRead.failed();
					XML_Status parseProgress = XML_ParseBuffer(expatParser, (int)actualRead.count, !keepGoing);

					if (parseProgress == XML_STATUS_ERROR)
					{
						XML_Error error = XML_GetErrorCode(expatParser);

						if (error != XML_ERROR_NO_ELEMENTS)
						{
							if (error == XML_ERROR_ABORTED)
							{
								keepGoing = false;
							}
							else
							{
								errorText = "XML parse error: ";
								errorText += XML_ErrorString(XML_GetErrorCode(expatParser));
								keepGoing = false;
							}
						}
					}
					else if (parseProgress == XML_STATUS_SUSPENDED)
					{
						XML_StopParser(expatParser, XML_FALSE);
						keepGoing = false;
					}
				}
				else
				{
					XML_ParsingStatus status;
					XML_GetParsingStatus(expatParser, &status);
					keepGoing = (status.parsing != XML_FINISHED && status.parsing != XML_SUSPENDED);
				}
			}

			handler.endDocument();

			if (!errorText.empty())
			{
				throw std::runtime_error(std::move(errorText));
			}
		}

		Node Parser::buildTreeFromText(Text text)
		{
			TreeBuilder builder;
			this->parseText(text, builder);
			return builder.take();
		}

		Node Parser::buildTreeFromFile(const cio::Path &path, std::size_t bufferSize /*= 4096*/)
		{
			TreeBuilder builder;
			this->parseFile(path, builder, bufferSize);
			return builder.take();
		}

		Node Parser::buildTree(Input &stream, std::size_t bufferSize /*= 4096*/)
		{
			TreeBuilder builder;
			this->parse(stream, builder, bufferSize);
			return builder.take();
		}

		void Parser::startElement(void *data, const char *el, const char **attributes)
		{
			std::pair<XML_Parser, Handler *> *userData = static_cast<std::pair<XML_Parser, Handler *> *>(data);

			try
			{
				userData->second->startElement(el);

				for (const char **attr = attributes; *attr != nullptr; attr += 2)
				{
					userData->second->processAttribute(attr[0], attr[1]);
				}
			}
			catch (...)
			{
				XML_StopParser(userData->first, XML_FALSE);
			}
		}

		void Parser::endElement(void *data, const char *el)
		{
			std::pair<XML_Parser, Handler *> *userData = static_cast<std::pair<XML_Parser, Handler *> *>(data);

			try
			{
				userData->second->endElement(el);
			}
			catch (...)
			{
				XML_StopParser(userData->first, XML_FALSE);
			}
		}

		void Parser::processText(void *data, const char *text, int len)
		{
			std::pair<XML_Parser, Handler *> *userData = static_cast<std::pair<XML_Parser, Handler *> *>(data);

			try
			{
				userData->second->processTextChunk(Text(text, len));
			}
			catch (...)
			{
				XML_StopParser(userData->first, XML_FALSE);
			}
		}

		void Parser::processComment(void *data, const char *text)
		{
			std::pair<XML_Parser, Handler *> *userData = static_cast<std::pair<XML_Parser, Handler *> *>(data);

			try
			{
				userData->second->processComment(text);
			}
			catch (...)
			{
				XML_StopParser(userData->first, XML_FALSE);
			}
		}
	}
}
