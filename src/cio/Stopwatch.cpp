﻿/*==============================================================================
 * Copyright 2022-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Stopwatch.h"
 
#include <cio/Print.h>

#include <ostream>
 
namespace cio
{
	std::size_t Stopwatch::print(char *buffer, std::size_t length) const noexcept
	{
		std::chrono::nanoseconds elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - this->value);
		
		long long ns = elapsed.count();
		bool prefixed = false;

		// Compute totals for each unit
		long long us = ns / 1000;
		long long ms = us / 1000;
		long long seconds = ms / 1000;
		long long minutes = seconds / 60;
		long long hours = minutes / 60;
		long long days = hours / 24;

		// Wrap units below days
		us %= 1000;
		ms %= 1000;
		seconds %= 60;
		minutes %= 60;
		hours %= 24;

		std::size_t needed = 0;
		std::size_t printed = 0;

		if (days > 0)
		{
			prefixed = true;

			needed += cio::print(days, buffer + printed, length - printed);
			printed = std::min(needed, length);

			needed += cio::print(" d", buffer + printed, length - printed);
			printed = std::min(needed, length);
		}

		if (hours > 0 || prefixed)
		{
			if (prefixed)
			{
				needed += cio::print(' ', buffer + printed, length - printed);
				printed = std::min(needed, length);
			}
			prefixed = true;

			needed += cio::print(hours, buffer + printed, length - printed);
			printed = std::min(needed, length);

			needed += cio::print(" h", buffer + printed, length - printed);
			printed = std::min(needed, length);
		}

		if (minutes > 0 || prefixed)
		{
			if (prefixed)
			{
				needed += cio::print(' ', buffer + printed, length - printed);
				printed = std::min(needed, length);
			}
			prefixed = true;

			needed += cio::print(minutes, buffer + printed, length - printed);
			printed = std::min(needed, length);

			needed += cio::print(" m", buffer + printed, length - printed);
			printed = std::min(needed, length);
		}

		if (seconds > 0 || prefixed)
		{
			if (prefixed)
			{
				needed += cio::print(' ', buffer + printed, length - printed);
				printed = std::min(needed, length);
			}

			needed += cio::print(seconds, buffer + printed, length - printed);
			printed = std::min(needed, length);

			// If we haven't used higher units, print fractions of seconds
			if (!prefixed)
			{
				if (seconds < 10ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(ms / 100ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}
				else if (seconds < 100ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(ms / 10ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}
			}

			needed += cio::print(" s", buffer + printed, length - printed);
			printed = std::min(needed, length);

			prefixed = true;
		}

		// Print smaller units if we don't have larger units
		if (!prefixed)
		{
			if (ms > 0ll)
			{
				needed += cio::print(ms, buffer, length);
				printed = std::min(needed, length);

				if (ms < 10ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(us / 100ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}
				else if (ms < 100ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(us / 10ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}

				needed += cio::print(" ms", buffer + printed, length - printed);
				printed = std::min(needed, length);
			}
			else if (us > 0ll)
			{
				needed += cio::print(us, buffer, length);
				printed = std::min(needed, length);

				if (us < 10ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(ns / 100ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}
				else if (us < 100ll)
				{
					needed += cio::print('.', buffer + printed, length - printed);
					printed = std::min(needed, length);

					needed += cio::print(ns / 10ll, buffer + printed, length - printed);
					printed = std::min(needed, length);
				}

				needed += cio::print(" us", buffer + printed, length - printed);
				printed = std::min(needed, length);
			}
			else
			{
				needed += cio::print(ns, buffer, length);
				printed = std::min(needed, length);

				needed += cio::print(" ns", buffer + printed, length - printed);
				printed = std::min(needed, length);
			}
		}

		if (printed < length)
		{
			std::memset(buffer + printed, 0, length - printed);
		}

		return needed;
	 }

	 std::string Stopwatch::str() const
	 {
		 char temp[128];
		 std::size_t length = this->print(temp, sizeof(temp));
		 return std::string(temp, temp + length);
	 }

	std::ostream &operator<<(std::ostream &stream, const Stopwatch &time)
	{
		char temp[128];
		std::size_t length = time.print(temp, sizeof(temp));
		return stream.write(temp, length);
	}
 }
 