/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_SEQUENCE_H
#define CAQ_SEQUENCE_H

#include "Types.h"

#include "Accessor.h"
#include "Reader.h"

namespace caq
{
	/**
	 * The Sequence accessor provides a procedurally-generated sequence based on a start and count.
	 * 
	 * @tparam T The component type
	 */
	template <typename T>
	class Sequence : public Reader<T>
	{
		public:
			/**
			 * Default constructor.
			 */
			inline Sequence() = default;

			/**
			 * Construct a sequence accessor given the data buffer and size.
			 * 
			 * @param start The first sequence index to return
			 * @param length The array element count
			 */
			inline explicit Sequence(T length) :
				mSize(length)
			{
				// nothing more to do
			}

			/**
			 * Destructor.
			 */
			inline virtual ~Sequence() noexcept override = default;

			// Override Abstract accessor methods

			/**
			 * Gets the number of array elements in this accessor.
			 * 
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return static_cast<unsigned long long>(mSize);
			}

			/**
			 * Gets the fixed number of element components in this accessor.
			 * 
			 * @return The number of components
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return 1u;
			}

			/**
			 * Interprets whether this accessor is usable.
			 * The Native accessor returns true if the data pointer is set and the size is nonzero.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mSize > 0;
			}

			// Override Reader<T> accessor methods

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The Native accessor cannot be further optimized, so it returns itself.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				return Accessor<T>(*this);
			}

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const noexcept override
			{
				return static_cast<T>(n);
			}

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 * 
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const noexcept override
			{
				*buffer = static_cast<T>(n);
				return buffer + 1;
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 * 
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const noexcept override
			{
				T value = static_cast<T>(noff);
				std::remove_const_t<T> *current = buffer;
				std::remove_const_t<T> *end = current + count;
				while (current < end)
				{
					*current++ = value++;
				}
				return end;
			}

			// Methods unique to Native<T, C>

		private:
			/** The number of sequence values */
			T mSize;
	};
}

#endif
