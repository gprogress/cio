/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "File.h"

#include "Class.h"
#include "ModeSet.h"
#include "Path.h"

#include <cerrno>
#include <system_error>

namespace cio
{
	Class<File> File::sMetaclass("cio::File");

	const Metaclass &File::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	File::File() noexcept
	{
		// nothing to do
	}

	File::File(std::uintptr_t handle)  noexcept :
		Device(handle)
	{
		// nothing to do
	}

	File::File(const Path &resource)
	{
		// TODO set up alternate protocol checking for various other devices that might be accessible
		this->openNativePath(resource.toNativeFile(), ModeSet::general());
	}

	File::File(const Path &resource, ModeSet modes)
	{
		// TODO set up alternate protocol checking for various other devices that might be accessible
		this->openNativePath(resource.toNativeFile(), modes);
	}

	File::File(const char *path)
	{
		this->openNativePath(path);
	}

	File::File(const std::string &path)
	{
		this->openNativePath(path);
	}

	File::File(const char *path, ModeSet modes)
	{
		this->openNativePath(path, modes);
	}

	File::File(const std::string &path, ModeSet modes)
	{
		this->openNativePath(path, modes);
	}

	File::File(File &&in) noexcept = default;

	File &File::operator=(File &&in) noexcept = default;

	File::~File() noexcept = default;
		
	const Metaclass &File::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	ModeSet File::openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory)
	{
		// TODO set up alternate protocol checking for various other devices that might be accessible
		return this->openNativePath(resource.toNativeFile(), modes);
	}
}
