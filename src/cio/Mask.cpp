/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Mask.h"

#include "Case.h"

#include <climits>
#include <ostream>

#if defined _MSC_VER
#include <intrin.h>
#endif

// ASCII characters

// Lowercase letter range

#define CIO_MASK_LOWER0 0ull
#define CIO_MASK_LOWER1 0x03FE000000000000ull

// Uppercase letter rage

#define CIO_MASK_UPPER0 0ull
#define CIO_MASK_UPPER1 0x000003FE00000000ull

#define CIO_MASK_ALPHA0 (CIO_MASK_LOWER0 | CIO_MASK_UPPER0)
#define CIO_MASK_ALPHA1 (CIO_MASK_LOWER1 | CIO_MASK_UPPER1)

// Spaces are nul (0), space, tab, newline, and carriage return
#define CIO_MASK_SPACE0 0x0000000100000261ull
#define CIO_MASK_SPACE1 0ull

#define CIO_MASK_DIGIT0 0x03FF000000000000ull
#define CIO_MASK_DIGIT1 0ull

#define CIO_MASK_ALNUM0 (CIO_MASK_ALPHA0 | CIO_MASK_DIGIT0)
#define CIO_MASK_ALNUM1 (CIO_MASK_ALPHA1 | CIO_MASK_DIGIT1)

// various printable symbols that are neither letters nor numbers
#define CIO_MASK_SYM0 0xFC00FFFE00000000ull
#define CIO_MASK_SYM1 0x780000001C000001ull

#define CIO_MASK_GLYPH0 (CIO_MASK_ALNUM0 | CIO_MASK_SYM0)
#define CIO_MASK_GLYPH1 (CIO_MASK_ALNUM1 | CIO_MASK_SYM1)

#define CIO_MASK_PRINT0 (CIO_MASK_GLYPH0 | CIO_MASK_SPACE0)
#define CIO_MASK_PRINT1 (CIO_MASK_GLYPH1 | CIO_MASK_SPACE1)

// underscore is just bit 95
#define CIO_MASK_US0 0ull
#define CIO_MASK_US1 (1ull << 31)

#define CIO_MASK_LABEL0 (CIO_MASK_ALNUM0 | CIO_MASK_US0)
#define CIO_MASK_LABEL1 (CIO_MASK_ALNUM1 | CIO_MASK_US1)



namespace cio
{
	Mask Mask::whitespace() noexcept
	{
		return Mask(CIO_MASK_SPACE0, CIO_MASK_SPACE1);
	}

	Mask Mask::uppercase() noexcept
	{
		return Mask(CIO_MASK_UPPER0, CIO_MASK_UPPER1);
	}

	Mask Mask::lowercase() noexcept
	{
		return Mask(CIO_MASK_LOWER0, CIO_MASK_LOWER1);
	}

	Mask Mask::alpha() noexcept
	{
		return Mask(CIO_MASK_ALPHA0, CIO_MASK_ALPHA1);
	}

	Mask Mask::digits() noexcept
	{
		return Mask(CIO_MASK_DIGIT0, CIO_MASK_DIGIT1);
	}

	Mask Mask::digits(unsigned base) noexcept
	{
		Mask mask;

		mask.set('0', static_cast<char>('0' + base - 1));

		if (base > 10)
		{
			mask.set('a', static_cast<char>('a' + base - 10));
			mask.set('A', static_cast<char>('a' + base - 10));
		}

		return mask;
	}

	Mask Mask::alphanumeric() noexcept
	{
		return Mask(CIO_MASK_ALNUM0, CIO_MASK_ALNUM1);
	}

	Mask Mask::symbols() noexcept
	{
		return Mask(CIO_MASK_SYM0, CIO_MASK_SYM1);
	}

	Mask Mask::glyphs() noexcept
	{
		return Mask(CIO_MASK_GLYPH0, CIO_MASK_GLYPH1);
	}

	Mask Mask::printable() noexcept
	{
		return Mask(CIO_MASK_PRINT0, CIO_MASK_PRINT1);
	}

	Mask Mask::ascii() noexcept
	{
		return Mask(ULLONG_MAX, ULLONG_MAX);
	}

	Mask Mask::unicode() noexcept
	{
		return Mask(0, 0, ULLONG_MAX, ULLONG_MAX);
	}

	Mask Mask::text() noexcept
	{
		return Mask(CIO_MASK_PRINT0, CIO_MASK_PRINT1, ULLONG_MAX, ULLONG_MAX);
	}

	Mask Mask::label() noexcept
	{
		return Mask(CIO_MASK_LABEL0, CIO_MASK_LABEL1);
	}

	Mask::Mask(unsigned long long bits64, unsigned long long bits128, unsigned long long bits192, unsigned long long bits256)
	{
		mEnabled[0] = bits64;
		mEnabled[1] = bits128;
		mEnabled[2] = bits192;
		mEnabled[3] = bits256;
	}

	Mask::Mask(char c) noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
		this->set(c);
	}

	Mask::Mask(char begin, char end) noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
		this->set(begin, end);
	}

	Mask::Mask(const char *any) noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
		this->set(any);
	}

	Mask::Mask(const char *any, std::size_t count) noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
		this->set(any, count);
	}

	bool Mask::empty() const noexcept
	{
		return !mEnabled[0] && !mEnabled[1] && !mEnabled[2] && !mEnabled[3];
	}
	
	std::size_t Mask::size() const noexcept
	{
#if defined _MSC_VER
		return __popcnt64(mEnabled[0]) + __popcnt64(mEnabled[1]) + __popcnt64(mEnabled[2]) + __popcnt64(mEnabled[3]);
#else
		return __builtin_popcount(mEnabled[0]) + __builtin_popcount(mEnabled[1]) + __builtin_popcount(mEnabled[2]) + __builtin_popcount(mEnabled[3]);
#endif
	}

	void Mask::set(char value) noexcept
	{
		std::uint64_t idx = static_cast<std::uint64_t>(value);
		mEnabled[idx >> 6] |= (1ull << (idx & 0x3F));
	}

	void Mask::set(char begin, char end) noexcept
	{
		for (char c = begin; c < end; ++c)
		{
			this->set(c);
		}
	}

	void Mask::set(const char *any) noexcept
	{
		if (any)
		{
			while (*any)
			{
				this->set(*any);
				++any;
			}
		}
	}

	void Mask::set(const char *any, std::size_t count) noexcept
	{
		if (any)
		{
			const char *end = any + count;
			while (any < end)
			{
				this->set(*any);
				++any;
			}
		}
	}

	void Mask::set(char value, bool enabled) noexcept
	{
		enabled ? this->set(value) : this->clear(value);
	}

	void Mask::clear() noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
	}

	void Mask::clear(char value) noexcept
	{
		std::uint64_t idx = static_cast<std::uint64_t>(value);
		mEnabled[idx >> 6] &= ~(1ull << (idx & 0x3F));
	}

	void Mask::clear(char begin, char end) noexcept
	{
		// TODO this can be more efficient
		for (char c = begin; c != end; ++c)
		{
			this->clear(c);
		}
	}

	void Mask::clear(const char *any) noexcept
	{
		if (any)
		{
			while (*any)
			{
				this->clear(*any);
				++any;
			}
		}
	}

	void Mask::clear(const char *any, std::size_t count) noexcept
	{
		if (any)
		{
			const char *end = any + count;
			while (any < end)
			{
				this->clear(*any);
				++any;
			}
		}
	}

	void Mask::setWhitespace() noexcept
	{
		mEnabled[0] |= CIO_MASK_SPACE0;
		mEnabled[1] |= CIO_MASK_SPACE1;
	}

	void Mask::setWhitespace(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_SPACE0) | (enabled * CIO_MASK_SPACE0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_SPACE1) | (enabled * CIO_MASK_SPACE1);
	}

	void Mask::clearWhitespace() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_SPACE0;
		mEnabled[1] &= ~CIO_MASK_SPACE1;
	}

	void Mask::setLowercase() noexcept
	{
		mEnabled[0] |= CIO_MASK_LOWER0;
		mEnabled[1] |= CIO_MASK_LOWER1;
	}

	void Mask::setLowercase(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_LOWER0) | (enabled * CIO_MASK_LOWER0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_LOWER1) | (enabled * CIO_MASK_LOWER1);
	}

	void Mask::clearLowercase() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_LOWER0;
		mEnabled[1] &= ~CIO_MASK_LOWER1;
	}

	void Mask::setUppercase() noexcept
	{
		mEnabled[0] |= CIO_MASK_UPPER0;
		mEnabled[1] |= CIO_MASK_UPPER1;
	}

	void Mask::setUppercase(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_UPPER0) | (enabled * CIO_MASK_UPPER0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_UPPER1) | (enabled * CIO_MASK_UPPER1);
	}

	void Mask::clearUppercase() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_UPPER0;
		mEnabled[1] &= ~CIO_MASK_UPPER1;
	}

	void Mask::setAlpha() noexcept
	{
		mEnabled[0] |= CIO_MASK_ALPHA0;
		mEnabled[1] |= CIO_MASK_ALPHA1;
	}

	void Mask::setAlpha(Case c) noexcept
	{
		switch (c)
		{
			case Case::Lower:
				this->setLowercase();
				break;

			case Case::Upper:
				this->setUppercase();
				break;

			default:
				this->setAlpha();
				break;
		}
	}

	void Mask::setAlpha(bool enabled) noexcept
	{
		mEnabled[0] |= CIO_MASK_ALPHA0;
		mEnabled[1] |= CIO_MASK_ALPHA1;
	}

	void Mask::setAlpha(Case c, bool enabled) noexcept
	{
		enabled ? this->setAlpha(c) : this->clearAlpha(c);
	}

	void Mask::clearAlpha() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_ALPHA0;
		mEnabled[1] &= ~CIO_MASK_ALPHA1;
	}

	void Mask::clearAlpha(Case c) noexcept
	{
		switch (c)
		{
			case Case::Lower:
				this->clearLowercase();
				break;

			case Case::Upper:
				this->clearUppercase();
				break;

			default:
				this->clearAlpha();
				break;
		}
	}

	void Mask::setDigits() noexcept
	{
		mEnabled[0] |= CIO_MASK_DIGIT0;
		mEnabled[1] |= CIO_MASK_DIGIT1;
	}

	void Mask::setDigits(unsigned base) noexcept
	{
		this->set('0', static_cast<char>('0' + base - 1));
		if (base > 10)
		{
			this->set('a', static_cast<char>('a' + base - 10));
			this->set('a', static_cast<char>('A' + base - 10));
		}
	}

	void Mask::setDigits(bool enabled) noexcept
	{
		enabled ? this->setDigits() : this->clearDigits();
	}

	void Mask::setDigits(unsigned base, bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_DIGIT0) | (enabled * CIO_MASK_DIGIT0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_DIGIT1) | (enabled * CIO_MASK_DIGIT1);
	}

	void Mask::clearDigits() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_DIGIT0;
		mEnabled[1] &= ~CIO_MASK_DIGIT1;
	}

	void Mask::clearDigits(unsigned base) noexcept
	{
		this->clear('0', static_cast<char>('0' + base - 1));
		if (base > 10)
		{
			this->clear('a', static_cast<char>('a' + base - 10));
			this->clear('a', static_cast<char>('A' + base - 10));
		}
	}

	void Mask::setAlphanumeric() noexcept
	{
		mEnabled[0] |= CIO_MASK_ALNUM0;
		mEnabled[1] |= CIO_MASK_ALNUM1;
	}

	void Mask::setAlphanumeric(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_ALNUM0) | (enabled * CIO_MASK_ALNUM0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_ALNUM1) | (enabled * CIO_MASK_ALNUM1);
	}

	void Mask::clearAlphanumeric() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_ALNUM0;
		mEnabled[1] &= ~CIO_MASK_ALNUM1;
	}

	void Mask::setSymbols() noexcept
	{
		mEnabled[0] |= CIO_MASK_SYM0;
		mEnabled[1] |= CIO_MASK_SYM1;
	}

	void Mask::setSymbols(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_SYM0) | (enabled * CIO_MASK_SYM0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_SYM1) | (enabled * CIO_MASK_SYM1);
	}

	void Mask::clearSymbols() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_SYM0;
		mEnabled[1] &= ~CIO_MASK_SYM1;
	}

	void Mask::setGlyphs() noexcept
	{
		mEnabled[0] |= CIO_MASK_GLYPH0;
		mEnabled[1] |= CIO_MASK_GLYPH1;
	}

	void Mask::setGlyphs(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_GLYPH0) | (enabled * CIO_MASK_GLYPH0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_GLYPH1) | (enabled * CIO_MASK_GLYPH1);
	}

	void Mask::clearGlyphs() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_GLYPH0;
		mEnabled[1] &= ~CIO_MASK_GLYPH1;
	}

	void Mask::setPrintable() noexcept
	{
		mEnabled[0] |= CIO_MASK_PRINT0;
		mEnabled[1] |= CIO_MASK_PRINT1;
	}

	void Mask::setPrintable(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_PRINT0) | (enabled * CIO_MASK_PRINT0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_PRINT1) | (enabled * CIO_MASK_PRINT1);
	}

	void Mask::clearPrintable() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_PRINT0;
		mEnabled[1] &= ~CIO_MASK_PRINT1;
	}

	void Mask::setAscii() noexcept
	{
		mEnabled[0] = ULLONG_MAX;
		mEnabled[1] = ULLONG_MAX;
	}

	void Mask::setAscii(bool enabled) noexcept
	{
		mEnabled[0] = enabled * ULLONG_MAX;
		mEnabled[1] = enabled * ULLONG_MAX;
	}

	void Mask::clearAscii() noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
	}

	void Mask::setUnicodePrefix() noexcept
	{
		mEnabled[2] = ULLONG_MAX;
		mEnabled[3] = ULLONG_MAX;
	}

	void Mask::setUnicodePrefix(bool enabled) noexcept
	{
		mEnabled[2] = enabled * ULLONG_MAX;
		mEnabled[3] = enabled * ULLONG_MAX;
	}

	void Mask::clearUnicodePrefix() noexcept
	{
		mEnabled[2] = 0;
		mEnabled[3] = 0;
	}

	void Mask::setText() noexcept
	{
		mEnabled[0] |= CIO_MASK_PRINT0;
		mEnabled[1] |= CIO_MASK_PRINT1;
		mEnabled[2] = ULLONG_MAX;
		mEnabled[3] = ULLONG_MAX;
	}

	void Mask::setText(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_PRINT0) | (enabled * CIO_MASK_PRINT0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_PRINT1) | (enabled * CIO_MASK_PRINT1);
		mEnabled[2] = enabled * ULLONG_MAX;
		mEnabled[3] = enabled * ULLONG_MAX;
	}

	void Mask::clearText() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_PRINT0;
		mEnabled[1] &= ~CIO_MASK_PRINT1;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
	}

	void Mask::setLabel() noexcept
	{
		mEnabled[0] |= CIO_MASK_LABEL0;
		mEnabled[1] |= CIO_MASK_LABEL1;
	}

	void Mask::setLabel(bool enabled) noexcept
	{
		mEnabled[0] = (mEnabled[0] & ~CIO_MASK_LABEL0) | (enabled * CIO_MASK_LABEL0);
		mEnabled[1] = (mEnabled[1] & ~CIO_MASK_LABEL1) | (enabled * CIO_MASK_LABEL1);
	}

	void Mask::clearLabel() noexcept
	{
		mEnabled[0] &= ~CIO_MASK_LABEL0;
		mEnabled[1] &= ~CIO_MASK_LABEL1;
	}

	Mask &Mask::operator|=(const Mask &right) noexcept
	{
		mEnabled[0] |= right.mEnabled[0];
		mEnabled[1] |= right.mEnabled[1];
		mEnabled[2] |= right.mEnabled[2];
		mEnabled[3] |= right.mEnabled[3];
		return *this;
	}

	Mask &Mask::operator&=(const Mask &right) noexcept
	{
		mEnabled[0] &= right.mEnabled[0];
		mEnabled[1] &= right.mEnabled[1];
		mEnabled[2] &= right.mEnabled[2];
		mEnabled[3] &= right.mEnabled[3];
		return *this;
	}

	Mask &Mask::operator^=(const Mask &right) noexcept
	{
		mEnabled[0] ^= right.mEnabled[0];
		mEnabled[1] ^= right.mEnabled[1];
		mEnabled[2] ^= right.mEnabled[2];
		mEnabled[3] ^= right.mEnabled[3];
		return *this;
	}

	Mask &Mask::invert() noexcept
	{
		mEnabled[0] = ~mEnabled[0];
		mEnabled[1] = ~mEnabled[1];
		mEnabled[2] = ~mEnabled[2];
		mEnabled[3] = ~mEnabled[3];
		return *this;
	}

	Mask operator~(const Mask &left) noexcept
	{
		return Mask(~left.mEnabled[0], ~left.mEnabled[1], ~left.mEnabled[2], ~left.mEnabled[3]);
	}

	Mask operator|(const Mask &left, const Mask &right) noexcept
	{
		return Mask(left.mEnabled[0] | right.mEnabled[0], left.mEnabled[1] | right.mEnabled[1], left.mEnabled[2] | right.mEnabled[2], left.mEnabled[3] | right.mEnabled[3]);
	}

	Mask operator&(const Mask &left, const Mask &right) noexcept
	{
		return Mask(left.mEnabled[0] & right.mEnabled[0], left.mEnabled[1] & right.mEnabled[1], left.mEnabled[2] & right.mEnabled[2], left.mEnabled[3] & right.mEnabled[3]);
	}

	Mask operator^(const Mask &left, const Mask &right) noexcept
	{
		return Mask(left.mEnabled[0] ^ right.mEnabled[0], left.mEnabled[1] ^ right.mEnabled[1], left.mEnabled[2] ^ right.mEnabled[2], left.mEnabled[3] ^ right.mEnabled[3]);
	}

	bool operator==(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) == 0;
	}

	bool operator!=(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) != 0;
	}

	bool operator<(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) < 0;
	}

	bool operator<=(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) <= 0;
	}

	bool operator>(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) > 0;
	}

	bool operator>=(const Mask &left, const Mask &right) noexcept
	{
		return std::memcmp(&left, &right, sizeof(Mask)) >= 0;
	}

	std::ostream &operator<<(std::ostream &stream, const Mask &mask)
	{
		return stream << std::hex << mask.enabled(0) << '-' << mask.enabled(1) << '-' << mask.enabled(2) << '-' << mask.enabled(3);
	}
}
