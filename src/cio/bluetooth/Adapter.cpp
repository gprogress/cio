/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Adapter.h"

#if defined CIO_BLUETOOTH_HAVE_DBUS
#include "DBusController.h"
#endif

#if defined CIO_BLUETOOTH_HAVE_HCI
#include "HCIController.h"
#endif

#include <cio/DeviceAddress.h>
#include <cio/Class.h>

namespace cio
{
	namespace bluetooth
	{
		Class<Adapter> Adapter::sMetaclass("cio::bluetooth::Adapter");
		
		const Metaclass *Adapter::getBestControllerType() noexcept
		{
#if defined CIO_BLUETOOTH_HAVE_DBUS
			return &DBusController::getDeclaredMetaclass();
#elif defined CIO_BLUETOOTH_HAVE_HCI
			return &HCIController::getDeclaredMetaclass();
#else	
			return &Controller::getDeclaredMetaclass();
#endif
		}
				
		std::unique_ptr<Controller> Adapter::allocateBestController()
		{
			void *memory = nullptr;
			const Metaclass *mc = Adapter::getBestControllerType();
			if (mc)
			{
				memory = mc->allocate(1);
				mc->construct(memory, 1);
			}
			return std::unique_ptr<Controller>(static_cast<Controller *>(memory));
		}
				
		Controller *Adapter::constructBestController(void *buffer, std::size_t capacity)
		{
			Controller *controller = nullptr;
			const Metaclass *mc = Adapter::getBestControllerType();
			if (mc && capacity >= mc->size())
			{
				mc->construct(buffer, 1);
				controller = static_cast<Controller *>(buffer);
			}
			return controller;
		}
				
		const Metaclass &Adapter::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		Adapter::Adapter() noexcept
		{
			// nothing more to do
		}
			
		Adapter::Adapter(const Device &in) :
			Controller(in)
		{
			this->setupImplementation();
		}	
		
		Adapter::Adapter(Adapter &&in) noexcept :
			Controller(std::move(in)),
			mController(std::move(in.mController))
		{
			// nothing more to do
		}
			
		Adapter &Adapter::operator=(Adapter &&in) noexcept
		{
			if (this != &in)
			{
				Controller::operator=(std::move(in));
				mController = std::move(in.mController);
			}
			
			return *this;
		}
			
		Adapter::~Adapter() noexcept = default;
		
		const Metaclass &Adapter::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void Adapter::clear() noexcept
		{
			Controller::clear();
			mController.reset();
		}

		Exception Adapter::validate()
		{
			return this->getOrCreateController()->validate();
		}
		
		void Adapter::openCurrentDevice()
		{
			this->setupImplementation();
		}
		
		bool Adapter::isOpen() const noexcept
		{
			return mController && mController->isOpen();
		}
		
		void Adapter::close() noexcept
		{
			if (mController)
			{
				mController->close();
			}
		}
		
		std::shared_ptr<Controller> Adapter::getOrCreateController()
		{
			if (!mController)
			{
				mController = Adapter::allocateBestController();
			}
			
			return mController;
		}
		
		std::shared_ptr<Controller> Adapter::getController() noexcept
		{
			return mController;
		}
				
		std::shared_ptr<const Controller> Adapter::getController() const noexcept
		{
			return mController;
		}
				
		void Adapter::setController(std::shared_ptr<Controller> controller) noexcept
		{
			mController = std::move(controller);
		}
						
		Device Adapter::findDefaultDevice()
		{
			// We can do this without a device set
			return this->setupImplementation().findDefaultDevice();
		}
					
		std::vector<Device> Adapter::discoverLocalDevices() 
		{
			// We can do this without a device set		
			return this->setupImplementation().discoverLocalDevices();
		}

		std::size_t Adapter::readLocalName(char *text, std::size_t length)
		{
			std::size_t count = 0;
			if (mController)
			{
				count = mController->readLocalName(text, length);
			}
			return count;		
		}
		
		DeviceAddress Adapter::readLocalAddress()
		{
			DeviceAddress address;
			if (mController)
			{
				address = mController->readLocalAddress();
			}
			return address;
		}

		State Adapter::startLowEnergyScan()
		{
			State status;
			if (mController)
			{
				status = mController->startLowEnergyScan();
			}
			return status;
		}
		
		Progress<std::size_t> Adapter::listenForAdvertisements(AdvertisementListener &listener) noexcept
		{
			Progress<std::size_t> result;
			if (mController)
			{
				result = mController->listenForAdvertisements(listener);
			}
		
			return result;
		}
		
		State Adapter::stopLowEnergyScan()
		{
			State status;
			if (mController)
			{
				status = mController->stopLowEnergyScan();
			}
			return status;
		}
		
		State Adapter::startConnection(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->startConnection(peripheral, exec);
			}
			return status;
		}
		
		State Adapter::startDisconnection(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->startDisconnection(peripheral, exec);
			}
			return status;
		}	
		
		State Adapter::waitForConnectionState(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->waitForConnectionState(peripheral, exec);
			}
			return status;
		}
				
		State Adapter::requestConnectionState(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->requestConnectionState(peripheral, exec);
			}
			return status;
		}
		
		State Adapter::startPairing(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->startPairing(peripheral, exec);
			}
			return status;
		}
		
		State Adapter::startUnpairing(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->startUnpairing(peripheral, exec);
			}
			return status;
		}
		
		State Adapter::waitForPairingState(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->waitForPairingState(peripheral, exec);
			}
			return status;
		}
				
		State Adapter::requestPairingState(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (mController)
			{
				status = mController->requestPairingState(peripheral, exec);
			}
			return status;
		}
		
		Controller &Adapter::setupImplementation()
		{
			if (!mController)
			{
				mController = Adapter::allocateBestController();
				mController->setDevice(this->getDevice());
				mController->openCurrentDevice();
			}
			else if (mController->getDevice() != this->getDevice())
			{
				mController->setDevice(this->getDevice());
				mController->openCurrentDevice();
			}
			return *mController;
		}
	}
}
