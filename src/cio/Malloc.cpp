/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Malloc.h"

#include "Metaclass.h"

#include <cstdlib>
#include <cstring>
#include <stdexcept>

namespace cio
{
	void *Malloc::allocate(std::size_t bytes)
	{
		void *data = nullptr;
		if (bytes)
		{
			data = ::malloc(bytes);
			if (!data)
			{
				throw std::bad_alloc();
			}
			::memset(data, 0, bytes);
		}
		return data;
	}

	std::nullptr_t Malloc::deallocate(void *data, std::size_t bytes) noexcept
	{
		if (data)
		{
			::free(data);
		}
		return nullptr;
	}

	void *Malloc::reallocate(void *data, std::size_t copied, std::size_t allocated)
	{
		void *result = ::realloc(data, allocated);

		if (!result && allocated)
		{
			throw std::bad_alloc();
		}

		if (copied < allocated)
		{
			::memset(static_cast<char *>(result) + copied, 0, allocated - copied);
		}

		return result;
	}

	void *Malloc::duplicate(const void *data, std::size_t bytes)
	{
		void *output = nullptr;
		if (bytes)
		{
			output = ::malloc(bytes);
			if (!output)
			{
				throw std::bad_alloc();
			}

			if (data)
			{
				::memcpy(output, data, bytes);
			}
			else
			{
				::memset(output, 0, bytes);
			}
		}
		return output;
	}

	void *Malloc::duplicate(const void *data, std::size_t copied, std::size_t allocated)
	{
		void *output = nullptr;
		if (allocated)
		{
			output = ::malloc(allocated);
			if (!output)
			{
				throw std::bad_alloc();
			}

			if (data)
			{
				if (copied)
				{
					::memcpy(output, data, copied);
				}

				if (copied < allocated)
				{
					::memset(static_cast<char *>(output) + copied, 0, allocated - copied);
				}
			}
			else
			{
				::memset(output, 0, allocated);
			}
		}
		return output;
	}

	void *Malloc::create(const Metaclass *type, std::size_t count)
	{
		void *result = nullptr;
		if (type && count)
		{
			result = ::malloc(type->size() * count);
			if (!result)
			{
				throw std::bad_alloc();
			}
			type->construct(result, count);
		}
		return result;
	}

	std::nullptr_t Malloc::destroy(const Metaclass *type, void *data, std::size_t count) noexcept
	{
		if (type && data && count)
		{
			type->destroy(data, count);
			::free(data);
		}
		return nullptr;
	}
	
	void *Malloc::copy(const Metaclass *type, const void *data, std::size_t count)
	{
		void *result = nullptr;

		if (type && count)
		{
			result = ::malloc(type->size() * count);
			if (!result)
			{
				throw std::bad_alloc();
			}
			type->initialize(data, count, result, count);
		}

		return result;
	}

	void *Malloc::copy(const Metaclass *type, const void *data, std::size_t copied, std::size_t allocated)
	{
		void *result = nullptr;

		if (type && allocated)
		{
			result = ::malloc(type->size() * allocated);
			if (!result)
			{
				throw std::bad_alloc();
			}
			type->initialize(data, copied, result, allocated);
		}

		return result;
	}

	void *Malloc::move(const Metaclass *type, void *data, std::size_t moved, std::size_t allocated)
	{
		void *result = nullptr;

		if (type && allocated)
		{
			result = ::malloc(type->size() * allocated);
			if (!result)
			{
				throw std::bad_alloc();
			}
			type->initialize(data, moved, result, allocated);
		}

		return result;
	}

	void *Malloc::resize(const Metaclass *type, void *data, std::size_t current, std::size_t desired)
	{
		void *result = nullptr;
		if (type)
		{
			if (current != desired)
			{
				if (!desired)
				{
					if (data && current)
					{
						type->destroy(data, current);
						::free(data);
					}
					result = nullptr;
				}
				else if (!data || !current)
				{
					result = ::malloc(type->size() * desired);
					if (!result)
					{
						throw std::bad_alloc();
					}
					type->construct(data, desired);
				}
				else
				{
					result = ::malloc(type->size() * desired);
					if (!result)
					{
						throw std::bad_alloc();
					}
					type->initialize(data, current, result, desired);
					::free(data);
				}
			}
			else
			{
				result = data;
			}
		}

		return result;
	}
}