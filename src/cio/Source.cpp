/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Source.h"

#include <cio/Print.h>
#include <ostream>

namespace cio
{
	std::string Source::print() const
	{
		std::string result;

		if (mFile)
		{
			result = mFile;
		}

		if (mLine >= 0)
		{
			if (!result.empty())
			{
				result += " line ";
			}
			else
			{
				result = "Line ";
			}

			char tmp[64] = { 0 };
			cio::print(mLine, tmp);
			result += tmp;
		}

		if (mColumn >= 0)
		{
			if (!result.empty())
			{
				result += " column ";
			}
			else
			{
				result = "Column ";
			}

			char tmp[64] = { 0 };
			cio::print(mColumn, tmp);
			result += tmp;
		}

		if (result.empty())
		{
			result = "Unknown location";
		}

		return result;
	}

	std::ostream &operator<<(std::ostream &s, const Source &source)
	{
		bool hasContent = false;

		if (source.getFile())
		{
			s << source.getFile();
			hasContent = true;
		}

		if (source.getLine() >= 0)
		{
			if (hasContent)
			{
				s << " line ";
			}
			else
			{
				s << "Line ";
			}

			s << source.getLine();
			hasContent = true;
		}

		if (source.getColumn() >= 0)
		{
			if (!hasContent)
			{
				s << " column ";
			}
			else
			{
				s << "Column ";
			}

			s << source.getColumn();
			hasContent = true;
		}

		if (!hasContent)
		{
			s << "Unknown location";
		}

		return s;
	}
}
