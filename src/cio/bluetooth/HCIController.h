/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_HCICONTROLLER_H
#define CIO_BLUETOOTH_HCICONTROLLER_H

#include "Types.h"

#include "Controller.h"

#include <cio/Buffer.h>
#include <cio/net/Channel.h>

#include <chrono>
#include <mutex>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO HCI Controller provides an implemention of the Controller interface for local adapters using native HCI sockets and packet processing.
		 * Most of the operations can be implemented as writing HCI command packets and then waiting for appropriate HCI events as a response.
		 * However, some operations can only be done a custom ioctl call on the socket itself or similarly platform-dependent techniques.
		 *
		 * @warning This class is currently a placeholder for proof of concept and likely will not work properly or even at all for any real world use case.
		 *
		 * @warning This class uses raw sockets and therefore requires either root/admin privileges or the appropriate setcap for the running application on Linux.
		 * DBusController is the recommended implementation to use on Linux. We've observed strange results trying to use HCI commands while BlueZ D-Bus daemon is also running.
		 */
		class CIO_BLUETOOTH_API HCIController : public Controller
		{
			public:							
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Makes an HCI socket.
				 *
				 * @return the HCI socket
				 */
				static cio::net::Socket makeSocket();
				
				/**
				 * Gets the address family for HCI sockets, which is Bluetooth.
				 *
				 * @return the address family
				 */
				static cio::net::AddressFamily family() noexcept;
				
				/**
				 * Gets the channel type for HCI sockets, which is ChannelType::Raw.
				 *
				 * @return the channel type
				 */
				static cio::net::ChannelType channel() noexcept;
				
				/**
				 * Gets the protocol family for HCI sockets.
				 *
				 * @return the protocol family
				 */
				static cio::net::ProtocolFamily protocol() noexcept;

				/**
				 * Construct an unconnected HCI Controller.
				 */
				HCIController() noexcept;
				
				/**
				 * Construct an HCI Controller and immediately connect it to the given local device.
				 *
				 * @param device The device
				 */
				explicit HCIController(const Device &device);
				
				/**
				 * Constructs an HCI Controller by adopting the state of a given client.
				 * 
				 * @param in The controller to move
				 */
				HCIController(HCIController &&in) noexcept;
				
				/**
				 * Moves an HCI Controller.
				 *
				 * @param in The controller to move
				 * @return this controller
				 */
				HCIController &operator=(HCIController &&in) noexcept;
				
				/**
				 * Destructor. Disconnects the socket.
				 */
				virtual ~HCIController() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the socket and device.
				 */				
				virtual void clear() noexcept override;
				
				// HCI network Channel
								
				cio::net::Channel &getChannel() noexcept;
				
				const cio::net::Channel &getChannel() const noexcept;
				
				void adoptChannel(cio::net::Channel &&in) noexcept;
				
				// Connection management
				
				virtual void openCurrentDevice() override;

				virtual void close() noexcept override;

				// Discovery API - does not require a device to be set
								
				virtual Device findDefaultDevice() noexcept override;
				
				/**
				 * Scans the local system to find all active Bluetooth devices.
				 *
				 * @return the list of known devices
				 * @throw cio::Exception If there was a failure in setting up the HCI connection to scan for local devices
				 * @throw std::bad_alloc If the memory for the list could not be allocated
				 */
				virtual std::vector<Device> discoverLocalDevices() override;
							
				/**
				 * Reads the local address from the currently open device.
				 * This returns the empty address if no device is open.
				 *
				 * @return the local device address
				 * @throw cio::Exception If the local device is open but a low-level hardware error occurred reading the address
				 */
				virtual DeviceAddress readLocalAddress() override;
				
				/**
				 * Reads the local device name from the currently open device.
				 * This prints the empty string if no device is open.
				 *
				 * @param text The buffer to store the device name into
				 * @param length the maximum number of text bytes available to store
				 * @return the total number of bytes needed for the name (which may be more or less than the buffer)
				 */
				virtual std::size_t readLocalName(char *text, std::size_t length) override;
	
				/**
				 * Uses the controller to start the Bluetooth Low Energy discovery process.
				 * This is a mode setting that enables the scanning mode.
				 * Scanning is inherently asynchronous so use listenForAdvertisements method to actually receive the scan results.
				 *
				 * This class writes the appropriate HCI commands to the socket to start low energy scanning.
				 * This method typically requires administraitive privileges on Linux either directly or via setcap to execute successfully.
				 *
				 * If scanning is already in progress, this method should do nothing and return State::Exists.
				 * 
				 * @param scanner The scanner to use for parameters settings
				 * @return the status which should either be Success if scanning wasstarted or Exists if scanning was already in progress
				 */
				virtual State startLowEnergyScan() override;
				
				/**
				 * Uses the controller to recieve advertisements for peripherals for an execution period.
				 * The receiver callback function is called once for each device advertisement until the execution's timer has elapsed or the execution
				 * has been asynchronously canceled by another thread.
				 *
				 * This class implements watching the HCI socket for relevant low-energy events and captures advertisements from them.
				 *
				 * The same peripheral may be discovered more than once if more than advertisement was seen during the execution time period.
				 * The result of this method is the number of advertisements recieved and the overall status, which should be Success if the full
				 * execution time elapsed, Abandoned if the scanning was canceled, or some other error if scanning was either not enabled or a hardware error occurred.
				 *
				 * The final result of the scan will also be stored into the executor itself to mark execution as complete, abandoned, etc.
				 * 
				 * @param receiver The callback function to receive each discovered peripheral advertisement
				 * @param exec The executor that controls the execution time and cancelation mechanism
				 * @return the result of the scanning in terms of number of advertisements and final status
				 */
				virtual Progress<std::size_t> listenForAdvertisements(AdvertisementListener &listener) noexcept override;
				
				/**
				 * Uses the controller to stop the Bluetooth Low Energy discovery process if it was enabled.
				 * This is a mode setting that disables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 * This method typically requires administraitive privileges on Linux either directly or via setcap to execute successfully.
				 * 
				 * @return the status which should either be Success if scanning was stopped or Missing if scanning was not enabled at all
				 */
				virtual State stopLowEnergyScan() override;
				
				/**
				 * Uses the current device controller to establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Exists.
				 *
				 * This class implements the operation using direct HCI commands and waits for an HCI connection completed event.
				 *
				 * @param peripheral The peripheral to connect to
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the state describing the progress of the connection
				 * @throw cio::Exception If the process of connecting failed
				 */ 			
				virtual State startConnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Disconnects the currently active connection to the given remote peripheral.
				 * If such a connection does not exist, this method should return State::Missing but not otherwise fail.
				 *
				 * This class implements the operation using direct HCI commands.
				 *
				 * @param peripheral The peripheral to disconnect from
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the state describing the progress of the disconnection
				 * @throw cio::Exception If no local device is selected or if the process of disconnecting failed
				 */
				virtual State startDisconnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Uses the current device controller to pair with the given remote peripheral for authentication.
				 * If the current device and peripheral are already paired, this should reuse the pairing and return State::Exists.
				 *
				 * This class implements the operation using direct HCI commands and then processes events accordingly.
				 *
				 * @note This method currently does not provide for advanced agent-based pairing to negotiate input/output capabilities.
				 * Future versions of CIO will enable that.
				 *
				 * @param peripheral The peripheral to pair with
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the state describing the progress of the pairing
				 * @throw cio::Exception If the process of pairing failed
				 */ 
				virtual State startPairing(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Unpairs the current device with the given peripheral.
				 * If the devices are not paired, this method should return State::Missing but not otherwise fail.
				 *
				 * This class implements the operation using direct HCI commands.
				 *
				 * @param peripheral The peripheral to unpair from
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the state describing the progress of the unpairing
				 * @throw cio::Exception If no local device is selected or if the process of unpairing failed
				 */
				virtual State startUnpairing(const Peripheral &peripheral, Task &exec) override;
			
				// High-level Device API
	
				// Low-level Buffer management - used to implement requests and responses
				
				/**
				 * Gets a lock on this controller for exclusive use.
				 * 
				 * @return the lock on the controller
				 */
				std::unique_lock<std::mutex> lock() const;
								
				/**
				 * Gets the maximum packet size for an HCI command or event.
				 *
				 * @return the maximum packet size
				 */
				std::size_t getMaxPacketSize() const noexcept;
				
				/**
				 * Gets the maximum time to wait on incoming packets for a response to a request.
				 * The default is 100ms if not otherwise set.
				 *
				 * @return the maximum response time
				 */
				std::chrono::milliseconds getResponseTimeout() const noexcept;
					
				/**
				 * Sets the maximum time to wait on incoming packets for a response to a request.
				 *
				 * @param timeout the maximum response time
				 */
				void setResponseTimeout(std::chrono::milliseconds timeout);
				
				/**
				 * Gets the maximum number of retry attempts to make a request without receiving a timely response
				 * before giving up. The default is 0 if not otherwise set.
				 *
				 * @return the maximum request retry count
				 */
				std::size_t getRequestRetryCount() const noexcept;
				
				/**
				 * Gets the maximum number of retry attempts to make a request without receiving a timely response
				 * before giving up. This can be set to 0 to disable retries to only make one attempt.
				 *
				 * @param count the maximum request retry count
				 */
				void setRequestRetryCount(std::size_t count) noexcept;
				
				/**
				 * Requests a write buffer from the controller.
				 * This should generally be done to reuse the same write buffer for efficiency.
				 * The buffer will be set up to start at position 0 and have a limit of the negotiated max packet size.
				 *
				 * @return the connection's transmit buffer
				 */
				Buffer &requestWriteBuffer();
				
				/** 
				 * Writes the given HCI request and returns a buffer containing the next appropriate response event.
				 * The request packet will be considered to be the portion of the buffer from the current position to the limit.
				 * This will send the request, then wait up to the current response timeout to receive that event.
				 * If no event is received in time, it will retry the request up to request retry count.
				 * If all retries are exhausted, an exception is thrown to indicate failure.
				 *
				 * This is the easiest way to do low-level synchronous request/response management but may not play
				 * well with unexpected asynchronous events.
				 * 
				 * @param request The packet with the encoded request
				 * @return the buffer with the encoded response event, or empty if no appropriate response is received
				 * @throw cio::Exception If no response was received
				 */
				Buffer &requestResponse(Buffer &request);
				
				/** 
				 * Writes the given HCI request and captures the event into the given buffer.
				 * The request packet will be considered to be the portion of the buffer from the current position to the limit.
				 * This will send the request, then wait up to the current response timeout to receive that event.
				 * If no response is received in time, it will retry the request up to request retry count.
				 * If all retries are exhausted, an exception is thrown to indicate failure.
				 * 
				 * @param request The packet with the encoded request
				 * @param response The buffer to store the encoded response packet
				 * @return the provided response buffer
				 * @throw cio::Exception If no response was received
				 */
				Buffer &requestResponse(Buffer &request, Buffer &response);
								
				/**
				 * Writes the given HCI request, command, or response packet without waiting for a response.
				 * The packet will be considered to be the portion of the buffer from the current position to the limit.
				 *
				 * Most users should only use this for command packets where no response is expected or for asynchronous
				 * request/response management.
				 * 
				 * @param packet The packet to transmit
				 */
				void command(Buffer &packet);
				
				/**
				 * Requests a read buffer from the controller.
				 * This should generally be done to reuse the same read buffer for efficiency.
				 * The buffer will be set up to start at position 0 and have a limit of the max packet size.
				 *
				 * @return the controller's read buffer
				 */
				Buffer &requestReadBuffer();
				
				/**
				 * Receives the next event from the controller in the connection's provided read buffer.
				 * This method will wait at most the response timeout to read the response.
				 * The buffer will be configured such that the current position to the limit contain the current event packet.
				 * Since this method is not aware of which request generated it, it will not retry if no response is received.
				 *
				 * @return the read buffer with the response packet
				 */
				Buffer &readEvent();
				
				/**
				 * Receives the next response from the server in your provided buffer.
				 * This method will wait at most the response timeout to recieve the response.
				 * The buffer will be configured such that the current position to the limit contain the current ATT packet.
				 * Since this method is not aware of which request generated it, it will not retry if no response is received.
				 *
				 * If any notify or indicate packets are received, it will reset the timeout
				 * and these will be handled by the connection's notify listener if set, otherwise they will be filtered out.
				 *
				 * @param packet the packet to receive into
				 * @return the provided packet, returned for convenience
				 */
				Buffer &readEvent(Buffer &packet);
				
				/**
				 * Gets the low-level native HCI handle.
				 *
				 * @return the native HCI handle
				 */
				int getHandle() const noexcept;
				
				/**
				 * Gets the underlying HCI codec used by this class.
				 *
				 * @return the HCI codec
				 */
				HCICodec &getCodec() noexcept;
				
				/**
				 * Gets the underlying HCI codec used by this class.
				 *
				 * @return the HCI codec
				 */
				const HCICodec &getCodec() const noexcept;
				
				/**
				 * Sets the underlying HCI codec used by this class.
				 *
				 * @return the HCI codec
				 */
				void setCodec(HCICodec &codec) noexcept;
				
				/**
				 * Rescans using the HCI ioctl to refresh the buffer of known local devices.
				 *
				 * @return the number of local devices found
				 */
				std::size_t refreshLocalDevices();
			
			private:
				/** The metaclass for this class */
				static Class<HCIController> sMetaclass;
				
				/** The default HCI packet codec to use */
				static HCICodec sDefaultCodec;
				
				void executeDeviceOpen();
				
				/** Mutex to guard multi-threaded access to this class */
				mutable std::mutex mMutex;

				/** HCI channel */
				mutable cio::net::Channel mChannel;
				
				/** Maximum timeout to wait for a response to a request */
				std::chrono::milliseconds mResponseTimeout;
				
				/** How many times to retry requests that did not receive a response */
				std::size_t mRequestRetryCount;
				
				/** How many low energy scans have been requested without being stopped */
				std::size_t mScanCount;
				
				/** Allocated buffer for ioctl to scan local devices - only allocated if needed */
				Buffer mDeviceBuffer;
				
				/** Allocated transmit buffer for use by this class and users */
				Buffer mWriteBuffer;
			
				/** Allocated receiver buffer for use by this class and users */
				Buffer mReadBuffer;
				
				/** The packet codec currently in use */
				HCICodec *mCodec;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

