/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_TREEBUILDER_H
#define CIO_XML_TREEBUILDER_H

#include "Handler.h"
#include "Node.h"

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		/**
		* XML Handler that builds an XML tree.
		*/
		class CIO_XML_API TreeBuilder : public Handler
		{
			public:
				static std::string trimWhitespace(Text text);

				/**
				* Constructor.
				*/
				TreeBuilder();

				/**
				* Destructor.
				*/
				virtual ~TreeBuilder() override;

				virtual void startDocument() override;

				virtual void endDocument() override;

				/**
				* Called when a new element is started.
				* @param element The element that just started
				* @param attrs The set of attributes for the element
				*/
				virtual void startElement(Text element) override;

				virtual void processAttribute(Text key, Text value) override;

				/**
				* Called when an element is ended.
				* @param element The element that just ended
				*/
				virtual void endElement(Text element) override;

				/**
				* Process a chunk of text.  The text might not be null-terminated.
				*
				* @param text The text to process
				*/
				virtual void processTextChunk(Text text) override;

				/**
				* Gets the parse tree.
				*
				* @return the parse tree
				*/
				Node &data();

				/**
				* Gets the parse tree.
				*
				* @return the parse tree
				*/
				const Node &data() const;

				/**
				* Takes the parse tree.
				*
				* @return the parse tree
				*/
				Node take();

			private:
				Node mRoot;
				std::vector<Node *> mStack;
				bool mRemoveWhiteSpace;
				bool mMergeText;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
