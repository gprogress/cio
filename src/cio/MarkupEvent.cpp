/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "MarkupEvent.h"

#include "Buffer.h"
#include "Print.h"

namespace cio
{
	std::size_t print(const MarkupEvent &e, char *buffer, std::size_t capacity) noexcept
	{
		Buffer printer(buffer, capacity);
		std::size_t needed = printer.requestPutText(e.markup);

		if (e.action != MarkupAction::Full)
		{
			needed += printer.requestPutText(' ');
			needed += printer.requestPutText(e.action);
		}

		if (e.key)
		{
			needed += printer.requestPutText(' ');
			needed += printer.requestPutText(e.key);
		}

		return needed;
	}

	std::ostream &operator<<(std::ostream &stream, const MarkupEvent &e)
	{
		stream << e.markup;
		if (e.action != MarkupAction::Full)
		{
			stream << ' ' << e.action;
		}

		if (e.key)
		{
			stream << ' ' << e.key;
		}

		return stream;
	}
}
