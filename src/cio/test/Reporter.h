/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_REPORTER_H
#define CIO_TEST_REPORTER_H

#include "Types.h"

#include <cio/Logger.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace test
	{
		/**
		 * The Reporter is a specialized kind of test logger to report testing faults and activities.
		 * In addition to normal logging capabilities, specific hookes exist for test actions such as starting, completing, failing, or skipping tests.
		 * Logging may also be chained to another logger to multiplex the logging output.
		 */
		class CIO_TEST_API Reporter : public Logger
		{
			public:
				/**
				 * Gets the metaclass for Reporter.
				 *
				 * @return the declared metaclass for this class
				 */
				const Metaclass &getDeclaredMetaclass();

				/**
				 * Construct the default reporter.
				 */
				Reporter();

				/**
				 * Construct the default reporter chained to the given logger for output.
				 *
				 * @param output The logger to use for output
				 */
				explicit Reporter(Logger *output);

				/**
				 * Destructor.
				 */
				virtual ~Reporter() noexcept override;

				/**
				 * Clears the state of this test reporter.
				 */
				virtual void clear() noexcept override;

				/**
				 * Gets the runtime metaclass for this object.
				 *
				 * @return the metaclass for this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Overrides general logging to also log to the chained logger if applicable.
				 *
				 * @param context The context of the logging message
				 * @param severity The severity of the logging message
				 * @param message The message to log
				 * @return whether the message was logged
				 */
				virtual bool log(const char *context, Severity severity, const char *message) override;

				/**
				 * Reports the start of the given test. This should only be done once per test before any actions (including setUp) are taken.
				 * The default implementation of this class is to log a message using the test path as the context, the message as "Starting", and the severity as Info.
				 *
				 * @param test the test that was started
				 */
				virtual void start(const Test &test);

				/**
				 * Reports the end of the given test as completing by normal execution. The severity indicates whether the test is considered as passed or failed.
				 * The default implementation of this class is to log a message using the test path as the context and the provided severity.
				 * If the severity is Error or higher, the message is "Failed" otherwise the test is "Passed".
				 *
				 * @param test the test to log as complete
				 * @param severity The severity result of the completed test
				 */
				virtual void complete(const Test &test, Severity severity);

				/**
				 * Reports the end of the given test as completing abnormally such as by throwing exception.
				 * The default implementation of this class is to log a message using the test path as the context and the provided severity, with the message as always being "Failed"
				 * followed by printing the fault's message and context.
				 *
				 * @param test the test to log as complete
				 * @param severity The severity result of the completed test
				 */
				virtual void fail(const Test &test, const Fault &fault);

				/**
				 * Reports the given test as having been skipped. This is used when a test is excluded by user requested, or when a parent Suite setUp method fails.
				 * The default implementation of this class is to log a message using the test path as the context and the provided severity, with the message being "Skipped".
				 */
				virtual void skip(const Test &test, Severity severity);

				/**
				 * Sets the logger to use for chained output.
				 * Any messages provided to this class's log() method is also forwarded to the given logger.
				 * The chained logger may have a different severity threshold which is checked separately from this class's.
				 *
				 * @param output The chained output logger to use
				 */
				void setOutput(Logger *output);

				/**
				 * Gets the logger currently used for chained output, if any.
				 * Any messages provided to this class's log() method is also forwarded to the given logger.
				 * The chained logger may have a different severity threshold which is checked separately from this class's.
				 *
				 * @return The chained output logger in use, or null if none
				 */
				Logger *getOutput();

			private:
				/** Metaclass for this class */
				static const Class<Reporter> sMetaclass;
			
				/** The chained output logger in use */
				Logger *mOutput;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
