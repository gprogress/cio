/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LIBRARY_H
#define CIO_LIBRARY_H

#include "Types.h"

#include "Path.h"
#include "Text.h"

#include <vector>

namespace cio
{
	/**
	 * The CIO Library class implements support for finding and loading dynamic shared libraries at runtime on
	 * platforms that support it, and to find functions and data pointers by name.
	 * 
	 * This can be used by higher level systems to implement dynamic discovery of libraries and plugin-based architectures.
	 */
	class CIO_API Library
	{
		public:
			/** Function pointer type for resolved symbols */
			using Symbol = void (*)();

			/**
			 * The filename prefix for dynamic libraries on this platform.
			 * Currently this is empty for Windows platforms and "lib" for others.
			 */
			static const char *FilePrefix;

			/**
			 * The file extension for dynamic libraries on this platform.
			 * Currently this is "dll" for Windows platforms and "so" for others.
			 */
			static const char *FileExtension;

			/**
			 * The name of the environment variable that specifies the dynamic library search path list.
			 * Currently this is "PATH" for Windows platforms and "LD_LIBRARY_PATH" for others.
			 */
			static const char *PathVariable;

			/**
			 * The delimiter character that separates entries in a library search path on this platform.
			 * Currently this is ';' for Windows platforms and ':' for others.
			 */
			static const char PathDelimiter;

			/**
			 * The list of hard-coded system search paths that are searched before any variable search paths.
			 * The list is delimited using PathDelimiter.
			 * Currently this is empty on Windows platforms and a specific list of system directories on others
			 * that do not typically show up LD_LIBRARY_PATH.
			 */
			static const char *SystemPaths;

			/**
			 * Gets the list of system library search paths.
			 * 
			 * @return the list of system library search paths
			 */
			static std::vector<Path> getSystemSearchPaths();

			/**
			 * Gets the list of user variable library search paths.
			 *
			 * @return the list of user library search paths
			 */
			static std::vector<Path> getUserSearchPaths();

			/**
			 * Gets the list of all library search paths.
			 * System library search paths are first, followed by user variable library search paths.
			 *
			 * @return the list of all library search paths
			 */
			static std::vector<Path> getSearchPaths();

			/**
			 * Parses a list of library search paths and appends the found paths to the givenlist.
			 * 
			 * @param delimitedList The list of library search paths delimited by PathDelimiter
			 * @param paths The path list to receive the parsed paths
			 */
			static void addSearchPaths(Text delimitedList, std::vector<Path> &paths);

			/**
			 * Checks whether the path matches the expected conventions for a library file on
			 * the current platform. This verifies the filename contains the library prefix and has the library file extension.
			 * 
			 * @warning This currently does not properly handle versioned .so libraries on Linux, only files or links
			 * with the simple .so file extension will match.
			 * 
			 * @param path The file path
			 * @return whether the path follows the convention for a library
			 */
			static bool matchesPath(const Path &path) noexcept;

			/**
			 * Gets a view of the portion of the path that represents the library name.
			 * 
			 * @warning This currently does not properly handle versioned .so libraries on Linux, only files or links
			 * with the simple .so file extension will match.
			 * 
			 * @param path The file path
			 * @return a non-owning view of the library name embedded in the path
			 */
			static Text name(const Path &path);

			/**
			 * Gets the best matching filename for the given search name.
			 *
			 * If provided a relative path, it will insert the library file prefix if it is not present, and
			 * set the extension to the library file extension. It will otherwise preserve the relative path structure.
			 *
			 * Note that this will not automatically append a debug suffix since these may vary by actual library, and on some
			 * platforms you can still open a release library from debug and vice-versa.
			 *
			 * If provided a complete path, the path is returned unmodified.
			 *
			 * @param name The library search name
			 * @return the best matching filename for the library search name
			 */
			static Path filename(Path name);

			/**
			 * Finds the given library in the default search path.
			 * The library name may be a complete path, in which case it is returned unmodified.
			 * Otherwise, the default library search path is used with any adjustments recommended by filename() to find the library.
			 * The default library search path will first check the system search path, followed by the user search path.
			 *
			 * @param name The library search name
			 * @return the best matching filename found for the library search name
			 */
			static Path find(const Path &name);

			/**
			 * Finds the given library in the given search path as text delimited by the native path separator.
			 * The library name may be a complete path, in which case it is returned unmodified.
			 * Otherwise, the given search path is used with any adjustments recommended by filename() to find the library.
			 *
			 * @param name The library search name
			 * @param search The search path delimited by the native path separator
			 * @return the best matching filename found for the library search name
			 */
			static Path find(const Path &name, Text search);

			/**
			 * Finds the given library in the given search path as text delimited by the native path separator.
			 * The library name may be a complete path, in which case it is returned unmodified.
			 * Otherwise, the given directory list is used with any adjustments recommended by filename() to find the library.
			 *
			 * @param name The library search name
			 * @param dirs The directories to search
			 * @return the best matching filename found for the library search name
			 */
			static Path find(const Path &name, const std::vector<Path> &dirs);

			/**
			 * Finds the given library in the given directory.
			 * The library name may be a complete path, in which case it is returned unmodified.
			 * Otherwise, the directory is searched for the file with any adjustments recommended by filename() to find the library.
			 *
			 * @param name The library search name
			 * @param directory The directory to search
			 * @return the best matching filename found for the library search name
			 */
			static Path find(const Path &name, const Path &directory);

			/**
			 * Opens the current application as a module.
			 * 
			 * @return the current application as a module
			 */
			static Library application();

			/**
			 * Finds the path for the library or module that contains the given address.
			 * 
			 * @param ptr The address of interest
			 * @return The path the library, module, or executable that contains the address
			 */
			static Path findLoadedAddress(void *ptr);

			/**
			 * Default constructor. Does not open anything.
			 */
			Library() noexcept;

			/**
			 * Constructs a library then opens it as if by calling open(name) to find it on the default search path.
			 * 
			 * @param name The library name to search
			 */
			explicit Library(const Path &name);

			/**
			 * Constructs a library then opens it as if by calling open(directory, name) to find it on the default search path.
			 *
			 * @param directory The directory path to search in
			 * @param name The library name to search
			 */
			Library(const Path &directory, const Path &name);

			/**
			 * Constructs a library with the given file path and native operating system handle.
			 * This can be used to adopt a library opened somewhere else.
			 * 
			 * @param path The full path to the library
			 * @param handle The operating system handle
			 */
			Library(Path path, std::uintptr_t handle) noexcept;

			/**
			 * Copy constructor.
			 * This requires reopening the same path as the input.
			 * 
			 * @param in The library to copy
			 */
			Library(const Library &in);

			/**
			 * Move constructor.
			 * 
			 * @param in The library to move
			 */
			Library(Library &&in) noexcept;

			/**
			 * Copy assignment.
			 * This requires reopening the same path as the input.
			 *
			 * @param in The library to copy
			 * @return this library
			 */
			Library &operator=(const Library &in);

			/**
			 * Move assignment.
			 *
			 * @param in The library to move
			 * @return this library
			 */
			Library &operator=(Library &&in) noexcept;

			/**
			 * Destructor.
			 */
			~Library() noexcept;

			/**
			 * Clears the currently open library.
			 * If this is the last reference to that library in this program, the operating system may unload
			 * the library from memory entirely. This will likely cause problems if you still have any pointers
			 * to functions or data in the library.
			 */
			void clear() noexcept;

			/**
			 * Opens this library to reference the current main application.
			 * This can be used to dynamically find symbols in the current program instead of a library.
			 */
			void openCurrentApplication();

			/**
			 * Opens the given path, discovering the best library to use for it in the default library search path.
			 * This is equivalent to calling openFile(find(path)).
			 * 
			 * For security reasons, this will only load libraries through the native filesystem file protocol, and will not
			 * automatically find libraries in the current directory or the application directory. You need to explicitly copy
			 * library files from other sources to the filesystem, and manually find libraries that aren't in the official search path.
			 * 
			 * @param path The library name or path to open
			 * @throw Exception if the library could not be opened
			 */
			void open(const Path &path);

			/**
			 * Opens the given path, discovering the best library to use for it in the given directory.
			 * This is equivalent to calling openFile(find(directory, path)).
			 *
			 * For security reasons, this will only load libraries through the native filesystem file protocol, and will not
			 * automatically find libraries in the current directory or the application directory. You need to explicitly copy
			 * library files from other sources to the filesystem, and manually find libraries that aren't in the official search path.
			 *
			 * @param path The library name or path to open
			 * @param directory The directory to search
			 * @throw Exception if the library could not be opened
			 */
			void open(const Path &path, const Path &directory);

			/**
			 * Opens the given path exactly as specified without any adjustments or searches.
			 * For security reasons, this will only load libraries through the native filesystem file protocol.
			 * 
			 * @param path The path to the library file to open
			 * @throw Exception if the library could not be opened
			 */
			void openFile(const Path &path);

			/**
			 * Gets the address of a function symbol in the open library given its name.
			 * Currently this can only handle undecorated C symbols, not decorated C++ symbols.
			 * 
			 * @param name The symbol name
			 * @return the pointer to the function or data for the symbol
			 * @throw Exception If the symbol could not be resolved
			 */
			Library::Symbol resolveFunction(Text name) const;

			/**
			 * Gets the address of a function symbol in the open library given its name and stores it
			 * to the given function pointer.
			 * 
			 * @tparam R The return type of the function
			 * @tparam A The argument types of the function
			 * @param name The symbol name
			 * @param function The function pointer to receive the resolved symbol
			 * @throw Exception If the symbol could not be resolved
			 */
			template <typename R, typename... A>
			inline void resolveFunction(Text name, R (*&function)(A...)) const
			{
				function = reinterpret_cast<R(*)(A...)>(this->resolveFunction(std::move(name)));
			}

			/**
			 * Gets the address of a funtion symbol in the open library given its name and stores it
			 * to the given function.
			 *
			 * @tparam F The function type
			 * @param name The symbol name
			 * @param function The function pointer to receive the resolved symbol
			 * @throw Exception If the symbol could not be resolved
			 */
			template <typename F>
			inline void resolveFunction(Text name, std::function<F> &function) const
			{
				function = reinterpret_cast<F *>(this->resolveFunction(std::move(name)));
			}

			/**
			 * Gets the address of a function symbol in the open library given its name.
			 * Currently this can only handle undecorated C symbols, not decorated C++ symbols.
			 * 
			 * This method does not throw on failure, and should be used if the symbol not existing
			 * is a valid outcome.
			 *
			 * @param name The symbol name
			 * @return the pointer to the function or data for the symbol, or null if not found
			 */
			Library::Symbol findFunction(Text name) const noexcept;

			/**
			 * Gets the address of a symbol in the open library given its name and stores it
			 * to the given function pointer.
			 * 
			 * This method does not throw on failure, and should be used if the symbol not existing
			 * is a valid outcome.
			 *
			 * @tparam R The return type of the function
			 * @tparam A The argument types of the function
			 * @param name The symbol name
			 * @param function The function pointer to receive the resolved symbol, or null if not found
			 */
			template <typename R, typename... A>
			inline void findFunction(Text name, R(*&function)(A...)) const
			{
				function = reinterpret_cast<R(*)(A...)>(this->findFunction(std::move(name)));
			}

			/**
			 * Gets the address of a symbol in the open library given its name and stores it
			 * to the given function.
			 *
			 * This method does not throw on failure, and should be used if the symbol not existing
			 * is a valid outcome.
			 *
			 * @tparam F The function type
			 * @param name The symbol name
			 * @param function The function pointer to receive the resolved symbol, or null if not found
			 */
			template <typename F>
			inline void findFunction(Text name, std::function<F> &function) const
			{
				function = reinterpret_cast<F *>(this->findFunction(std::move(name)));
			}

			/**
			 * Gets the final path actually used to open the library.
			 * This is after all link resolution.
			 * 
			 * @return the library path
			 */
			const Path &getPath() const noexcept;

			/**
			 * Gets the print length of the library path in UTF-8 characters.
			 * 
			 * @return the print length
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints the library path to the given UTF-8 text buffer.
			 * 
			 * @param buffer The text buffer
			 * @param capacity The capacity of the buffer
			 * @return the actual number of characters needed
			 */
			std::size_t print(char *buffer, std::size_t capacity) const noexcept;

			/**
			 * Prints the library path to a returned UTF-8 text string.
			 * 
			 * @return the library path
			 */
			Text print() const noexcept;

			/**
			 * Gets the underlying operating system handle for the library.
			 * 
			 * @return the handle
			 */
			std::uintptr_t getHandle() const noexcept;

			/**
			 * Takes ownership of the given handle away from the library.
			 * You are responsible for what happens after that.
			 * 
			 * @return the handle, which will no longer be managed by this library
			 */
			std::uintptr_t takeHandle() noexcept;

		private:
			/**
			 * Checks that the given path represents a library and returns it.
			 * If it is a link to a library, the actual resolved file path will be returned.
			 * If the path does not represent a library, the empty path is returned.
			 * 	
			 * The path is used exactly as formatted with no adjustments for platform prefix or suffix.
			 * 
			 * @param path The absolute path to check
			 * @return the found library path or empty if none
			 */
			static Path findExactMatch(Path path);

			/** Path used to open the library */
			Path mPath;

			/** Operating system handle of the library */
			std::uintptr_t mHandle;
	};

	/**
	 * Checks to see if two library instances are equal.
	 * This is true if they have the same path.
	 * 
	 * @param left The first library
	 * @param right The second library
	 * @return whether the libraries have the same path
	 */
	CIO_API bool operator==(const Library &left, const Library &right) noexcept;

	/**
	 * Checks to see if two library instances are not equal.
	 * This is true if they do not have the same path.
	 *
	 * @param left The first library
	 * @param right The second library
	 * @return whether the libraries have the same path
	 */
	CIO_API bool operator!=(const Library &left, const Library &right) noexcept;

	/**
	 * Checks to see if the first library sorts before the second.
	 * Sorting is done using path.
	 *
	 * @param left The first library
	 * @param right The second library
	 * @return whether the first library sorts before the second
	 */
	CIO_API bool operator<(const Library &left, const Library &right) noexcept;

	/**
	 * Checks to see if the first library sorts before or equal to the second.
	 * Sorting is done using path.
	 *
	 * @param left The first library
	 * @param right The second library
	 * @return whether the first library sorts before or equal to the second
	 */
	CIO_API bool operator<=(const Library &left, const Library &right) noexcept;

	/**
	 * Checks to see if the first library sorts after the second.
	 * Sorting is done using path.
	 *
	 * @param left The first library
	 * @param right The second library
	 * @return whether the first library sorts after the second
	 */
	CIO_API bool operator>(const Library &left, const Library &right) noexcept;

	/**
	 * Checks to see if the first library sorts after or equal to the second.
	 * Sorting is done using path.
	 *
	 * @param left The first library
	 * @param right The second library
	 * @return whether the first library sorts after or equal to the second
	 */
	CIO_API bool operator>=(const Library &left, const Library &right) noexcept;

}

#endif

