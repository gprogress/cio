/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_TYPES_H
#define CIO_ZIP_TYPES_H

#if defined _WIN32
#if defined CIO_ZIP_BUILD
#if defined CIO_ZIP_BUILD_SHARED
#define CIO_ZIP_ABI __declspec(dllexport)
#else
#define CIO_ZIP_ABI
#endif
#else
#define CIO_ZIP_ABI __declspec(dllimport)
#endif
#elif defined __ELF__
#define CIO_ZIP_ABI __attribute__ ((visibility ("default")))
#else
#define CIO_ZIP_ABI
#endif


namespace cio
{
	// Classes from other packages
	class Metaclass;
	template <typename> class Class;
	class Directory;
	class Input;
	class Output;

	/**
	 * The CIO zip namespace provides classes suitable for adapting inputs and outputs to work with general
	 * compression techniques such as Zip/Deflate and to provide views of Zip and 7-zip archive contents as a resource protocol.
	 */
	namespace zip
	{
		class Archive;
		class Deflator;
		class Inflator;
		class Program;
	}
}

#endif

