/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_METACLASS_H
#define CIO_METACLASS_H

#include "Types.h"

#include "Logger.h"

#include <algorithm>
#include <iosfwd>
#include <memory>
#include <stdexcept>
#include <typeinfo>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Metaclass object is used to provide a way to obtain basic reflection information about runtime polymorphic types
	 * such as their size, type info, alignment, and providing polymorphic construction, assignment, and destruction methods.
	 * It also serves a class logger for the represented class.
	 *
	 * The CIO library is instrumented to support Metaclass so polymorphic types can be dynamically cloned, moved, allocated,
	 * and used with factories and serializers. However, it is entirely optional and opt-in to any users of CIO beyond that, 
	 * and users can non-intrusively instrument non-CIO classes when they do need to do so.
	 *
	 * It does not assume or require classes to inherit from a common base class and can even work with primitives and C structures
	 * or custom constructors and destructors. It can also be used to set different default memory allocators for types when C++ new/delete
	 * are not the right choice (e.g. FILE structures from C should use std::fclose and have no default allocator).
	 *
	 * The Class<T> template subclasses Metaclass to automate the fields such as size, type, alignment, and the method implementations.
	 * Most users will be able to use that absent any exotic type requirements.
	 *
	 * The base Metaclass class itself implements supports for untyped binary sequences of however many bytes you want.
	 * The default Metaclass essentially is the metaclass for void and other 1-byte types except it does not have any name information.
	 *
	 * @note This class is intended to be constructed once for each class during static initialization and thus should not have order of construction
	 * dependencies between metaclasses or any other static data. It should be optimized for minimal compile-time storage without dynamic allocation,
	 * runtime-editable content, or multi-threading constructs that can deadlock system library loaders. The default Class implementation
	 * only requires a single C string constant to do its job.
	 */
	class CIO_API Metaclass : public Logger
	{
		public:
			/**
			 * Gets the declared metaclass for cio::Metaclass itself.
			 *
			 * @return the declared metaclass for cio::Metaclass
			 */
			const Metaclass &getDeclaredMetaclass() noexcept;
			
			/**
			 * Construct the default metaclass for untyped byte sequences.
			 * This defaults to no type info or name, with an size of 1 and alignment of 1.
			 */
			Metaclass() noexcept;
			
			/**
			 * Construct a metaclass with the given fully qualified name as a null-terminated C string.
			 * The name is referenced - not owned - by this class and is parsed to set up
			 * the class name and namespaces field. The size and alignment are initialized to the given size value.
			 *
			 * @param qname The qualified class name
			 * @param ti The type info for the class
			 * @param s The size of the class
			 */
			Metaclass(const char *qname, const std::type_info &ti, std::size_t s) noexcept;
			
			/**
			 * Construct a metaclass with the given fully qualified name as a null-terminated C string.
			 * The name is referenced - not owned - by this class and is parsed to set up
			 * the class name and namespaces field. The size and alignment are initialized to the given size and alignment values.
			 *
			 * @param qname The qualified class name
			 * @param ti The type info for the class
			 * @param s The size of the class
			 * @param a The alignment of the class
			 */
			Metaclass(const char *qname, const std::type_info &ti, std::size_t s, std::size_t a) noexcept;
			
			/**
			 * Destructor.
			 */
			virtual ~Metaclass() noexcept override;
			
			/**
			 * Gets the metaclass for this class.
			 * 
			 * @return the metaclass for this class
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;
			
			/**
			 * Clears the metaclass state.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the default allocator for this object.
			 * This base implementation returns the global new allocator.
			 * Subclasses may override if there is a reason to.
			 * 
			 * @return the default class allocator
			 */
			virtual Allocator *getDefaultAllocator() const noexcept;
				
			/**
			 * Gets the fully-qualified class name exactly as initialized.
			 * 
			 * @return the full class name
			 */
			inline const char *getFullName() const noexcept { return mFullName; }

			/** 
			 * Gets the C++ class name without namespaces or template parameters.
			 * 
			 * @return the C++ class name
			 */
			Text getName() const noexcept;
			
			/** 
			 * Gets the fully declared C++ class type name minus template parameter bindings.
			 *
			 * @return the fully-qualified name
			 */
			Text getQualifiedName() const noexcept;

			/** 
			 * Gets the C++ namespace hierarchy, in order of most general to most specific, with the "::" delimiter.
			 * 
			 * @return the namespace hierarchy
			 */
			Text getNamespace() const noexcept;

			/** 
			 * Gets the C++ std::type_info object for this class.
			 * 
			 * @return the C++ std::type_info
			 */
			inline const std::type_info *type() const noexcept { return mType; }

			/**
			 * Gets the size of this class in bytes.
			 * 
			 * @return the size of this class
			 */
			inline std::size_t size() const noexcept { return mSize; }

			/**
			 * Gets the alignment of this class in bytes.
			 * Normally this should be the same as the size.
			 *
			 * @return the alignment of this class
			 */
			inline std::size_t alignment() const noexcept { return mAlignment; }

			/**
			 * Gets the alignment of representing an array of this class using the given number of bytes.
			 * This returns the input value padded up to the next aligned unit.
			 *
			 * @param bytes The requested byte count
			 * @return the byte count after alignment
			 */
			inline std::size_t alignment(std::size_t bytes) const noexcept { return bytes + (bytes % mAlignment); }
			
			/**
			 * Performs the default memory allocation without construction suitable for count objects of this class.
			 * This uses the default allocator.
			 *
			 * @param count the number of objects (not bytes) to allocate
			 * @throw std::bad_alloc If the allocation failed
			 */
			void *allocate(std::size_t count) const;

			/**
			 * Performs the default memory deallocation without destruction for count objects of this class.
			 * This uses the default allocator.
			 *
			 * @param buffer The memory buffer representing count instances of this class
			 * @param count The number of objects to deallocate
			 */
			void deallocate(void *buffer, std::size_t count) const noexcept;

			/**
			 * Perform default construction of this class on a sufficiently large buffer.
			 *
			 * The base implementation uses std::memset to zero out the buffer which is fine
			 * for primitives and structures but probably not a good idea for most other classes.
			 *
			 * @param buffer The memory buffer to construct instances into
			 * @param count The number of objects to construct
			 */
			virtual bool construct(void *buffer, std::size_t count) const;

			/**
			 * Conduct copy construction of this class on a sufficiently large buffer.
			 * 
			 * @param input The input buffer to copy from, which must be initialized
			 * @param output The output buffer to copy to, which must be uninitialized
			 * @param count The number of elements to copy
			 * @return the bool indicating how the copy was performed
			 */
			virtual bool copy(const void *input, void *output, std::size_t count) const;

			/**
			 * Conduct move construction of this class on a sufficiently large buffer.
			 *
			 * @param input The input buffer to copy from, which msut be initialized
			 * @param output The output buffer to copy to, which must be uninitialized
			 * @param count The number of elements to move
			 * @return the bool indicating how the move was performed
			 */
			virtual bool move(void *input, void *output, std::size_t count) const noexcept;

			/**
			 * Conduct copy assignment of this class on a sufficiently large buffer.
			 *
			 * @param input The input buffer to copy from, which must be initialized
			 * @param output The output buffer to copy to, which must be initialized
			 * @param count The number of elements to copy
			 * @return the bool indicating how the copy was performed
			 */
			virtual bool assign(const void *input, void *output, std::size_t count) const;

			/**
			 * Conduct move assignment of this class on a sufficiently large buffer.
			 *
			 * @param input The input buffer to copy from, which msut be initialized
			 * @param output The output buffer to copy to, which must be initialized
			 * @param count The number of elements to move
			 * @return the bool indicating how the move was performed
			 */
			virtual bool transfer(void *input, void *output, std::size_t count) const noexcept;

			/**
			 * Conduct destruction of this class on a sufficiently large buffer.
			 *
			 * @param buffer The input buffer to conduct destruction, which msut be initialized
			 * @param count The number of elements to destroy
			 * @return the bool indicating how the destruction was performed
			 */
			virtual bool destroy(void *buffer, std::size_t count) const noexcept;

			/**
			 * Method to get the i'th array element of an input array buffer.
			 * This is a convenience method that applies a byte offset of the class size multiplied by i.
			 *
			 * @param buffer The buffer
			 * @param i The element index
			 * @return the pointer to the requested element
			 */
			inline void *element(void *buffer, std::size_t i) const;

			/**
			 * Method to get the i'th array element of an input array buffer.
			 * This is a convenience method that applies a byte offset of the class size multiplied by i.
			 *
			 * @param buffer The buffer
			 * @param i The element index
			 * @return the pointer to the requested element
			 */
			inline const void *element(const void *buffer, std::size_t i) const;

			/**
			 * Allocates enough memory for the represented class and then constructs it using the default constructor.
			 *
			 * @return the allocated instance of the class
			 */
			inline void *newInstance() const;

			/**
			 * Deconstructs an instance of the represented class then deallocates the memory used for it.
			 *
			 * @param buffer The buffer to delete
			 */
			inline void deleteInstance(void *buffer) const;

			/**
			 * Allocates enough memory for an array of length count for the represented class and then constructs each element using the default constructor.
			 *
			 * @param count The number of array elements to instantiate
			 * @return the allocated array of count instances
			 */
			inline void *newArray(std::size_t count) const;

			/**
			 * Deconstructs an array of instances of the represented class, then deallocates the memory used for them.
			 *
			 * @param buffer The array to delete
			 * @param count The number of class instances in the array
			 */
			inline void deleteArray(void *buffer, std::size_t count) const;

			/**
			 * Method to resize an existing buffer from an old size to a requested size.
			 * This is a convenience method that performs the following steps:
			 * <ol>
			 * <li>Determines if the old size is different from the new size, if not just returns the input</li>
			 * <li>If the input is null or the old size is 0, allocate a new buffer and construct objects into it</li>
			 * <li>If the input is non-null and the requested size is 0, destroy all objects then deallocate the array, returning null</li>
			 * <li>In all other cases, allocate a new buffer of the requested size, then use the move method to transfer the input buffer elements to the output buffer</li>
			 * <li>If there is no move method, then the copy method is used instead </li>
			 * <li>The input array is then finally destroyed and deallocated</li>
			 */
			void *resize(void *buffer, std::size_t old, std::size_t requested) const;
			
			/**
			 * Moves the given initialized source buffer to the given uninitialized target buffer.
			 * If the target is longer than the source, the excess elements are default constructed.
			 * The source is not modified other than the effects of the move on existing elements.
			 * 
			 * @param source The source buffer of initialized data elements
			 * @param length The number of elements in the source buffer to move
			 * @param target The target buffer of uninitialized data elements
			 * @param capacity The number of elemenst in the target buffer to construct
			 * @throw std::bad_alloc If move construction is not possible and copy construction results in memory allocations that fail
			 */
			void initialize(void *source, std::size_t length, void *target, std::size_t capacity) const;

			/**
			 * Copies the given initialized source buffer to the given uninitialized target buffer.
			 * If the target is longer than the source, the excess elements are default constructed.
			 *
			 * @param source The source buffer of initialized data elements
			 * @param length The number of elements in the source buffer to move
			 * @param target The target buffer of uninitialized data elements
			 * @param capacity The number of elemenst in the target buffer to construct
			 * @throw std::bad_alloc If copy construction results in memory allocations that fail
			 */
			void initialize(const void *source, std::size_t length, void *target, std::size_t capacity) const;

			/**
			 * Duplicates an existing array. This is a convenience method that delegates to the default allocator.
			 *
			 * @param buffer The buffer of objects to duplicate
			 * @param count The number of objects in the buffer
			 * @return the output buffer with all duplicated elements
			 */
			void *duplicate(const void *buffer, std::size_t count) const;

			/**
			 * Method to duplicate an existing buffer with, optionally, a different size.
			 * This is a convenience method that allocates a new buffer of the requested size, and then copies up to the minimum of the old size and requested size objects to it.
			 * Any extra objects on the duplicated buffer are default constructed.
			 * If the requested size is 0, nullptr is returned.
			 *
			 * @param buffer The buffer of objects to duplicate
			 * @param old The size of the input buffer
			 * @param requested The desired size of the output buffer
			 * @return the output buffer with all duplicated elements
			 */
			void *duplicate(const void *buffer, std::size_t old, std::size_t requested) const;
			
			/**
			 * Creates a new instance of this current metaclass's object, assuming without verification that is compatible with type T.
			 * 
			 * @tparam <T> The type of base class
			 * @return the created instance
			 */
			template <typename T>
			inline std::unique_ptr<T> create() const;

			/**
			 * Checks whether this metaclass is the declared metaclass of type T.
			 * This is true if this metaclass is exactly the same metaclass (pointer address equality) as the official one for T.
			 * 
			 * @tparam T The type of interest
			 * @return whether this is the metaclass for type T
			 */
			template <typename T>
			inline bool is() const noexcept;

		protected:
			/** Default allocator for this type - almost always New */
			Allocator *mAllocator;

			/** Size in bytes of one instance of the class described by the metaclass */
			std::size_t mSize;
			
			/** Minimum byte alignment for allocating instances of the class represented by the metaclass in memory */
			std::size_t mAlignment;
			
			/** C++ type info associated with the class represented by the metaclass */
			const std::type_info *mType;
		
			/** Fully qualified C++ name of the represented class as provided */
			const char *mFullName;
			
		private:
			/** Metaclass inception */
			static const Class<Metaclass> sMetaclass;
	};

	/**
	 * Checks to see if two metaclasses are equal.
	 * This is true if they have the same address or the same fully-qualified name.
	 * 
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the metaclasses are equal
	 */
	CIO_API bool operator==(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Checks to see if two metaclasses are not equal.
	 * This is false if they have the same address or the same fully-qualified name.
	 *
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the metaclasses are not equal
	 */
	CIO_API bool operator!=(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Checks to see if one metaclass sorts before another.
	 * The sort is performed lexicographically using fully-qualified name.
	 *
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the first metaclass sorts before the second one
	 */
	CIO_API bool operator<(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Checks to see if one metaclass sorts before or equal to another.
	 * The sort is performed lexicographically using fully-qualified name.
	 *
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the first metaclass sorts before or equal to the second one
	 */
	CIO_API bool operator<=(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Checks to see if one metaclass sorts after another.
	 * The sort is performed lexicographically using fully-qualified name.
	 *
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the first metaclass sorts after the second one
	 */
	CIO_API bool operator>(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Checks to see if one metaclass sorts after or equal to another.
	 * The sort is performed lexicographically using fully-qualified name.
	 *
	 * @param left The first metaclass
	 * @param right The second metaclass
	 * @return whether the first metaclass sorts after or equal to the second one
	 */
	CIO_API bool operator>=(const Metaclass &left, const Metaclass &right) noexcept;

	/**
	 * Gets the declared metaclass of a given type T.
	 * By default, this is the result of calling the static method T::getDeclaredMetaclass.
	 * However, for types defined without such a method, this template can be fully specialized to obtain the proper metaclass a different way.
	 *
	 * Specializations for getDeclaredMetaclass are built into CIO for all C/C++ primitive types.
	 *
	 * @tparam <T> The type of interest
	 * @return the declared metaclass for type T
	 */
	template <typename T>
	inline const Metaclass &getDeclaredMetaclass();

	/**
	 * Gets the dynamic metaclass of the given input object.
	 * The default template calls the getMetaclass() method on the input if it has one, which is the easiest way to implement this.
	 * However, for existing classes that do not use the CIO metaclass, alternate specializations or overloads can be provided.
	 * If no known way to detect the metaclass is found, this falls back to calling getDeclaredMetaclass<T> which will work for primitives, C structures,
	 * and non-polymorphic classes.
	 *
	 * @tparam<T> The formal static type of the input object
	 * @return the metaclass for the actual dynamic runtime type of the input object
	 */
	template <typename T>
	inline const Metaclass &getMetaclass(const T &input);

	/**
	* Gets the dynamic metaclass of the given input object referenced by a pointer.
	* In addition to the normal behavior of the getMetaclass method, this has additional special behavior for pointer types that can't normally be dereferenced.
	* If T is void, then the metaclass for void is returned.
	* If T is null, then the declared metaclass for the formal type T is returned.
	*
	* @tparam <T> The formal static type of the input object
	* @return the metaclass for the actual dynamic runtime type of the input object
	*/
	template <typename T>
	inline const Metaclass &getPointerMetaclass(const T *input);

	/**
	 * Allocates a new instance of an object with the same dynamic runtime type as the given input.
	 * The input is used to obtain the proper metaclass but is otherwise not used in creating the new object.
	 *
	 * @tparam <T> The formal static type of the input and allocated objects
	 * @param input the input object
	 * @return the allocated object
	 */
	template <typename T>
	inline std::unique_ptr<T> newInstance(const T &input);

	/**
	 * Allocates a new instance of an object with the same dynamic runtime type as the given input.
	 * The input is used to obtain the proper metaclass but is otherwise not used in creating the new object.
	 * If the input is null, the output will also be null.
	 *
	 * @tparam <T> The formal static type of the input and allocated objects
	 * @param input the input object
	 * @return the allocated object
	 */
	template <typename T>
	inline std::unique_ptr<T> newInstance(const T *input);

	/**
	 * Allocates a new instance of an object that is a deep copy of the given input including with the same dynamic runtime type.
	 * The input is used to obtain the proper metaclass and then becomes the input to the copy constructor.
	 *
	 * @tparam <T> The formal static type of the input and allocated objects
	 * @param input the input object
	 * @return the allocated object of the same dynamic type constructed by copy construction
	 */
	template <typename T>
	inline std::unique_ptr<T> clone(const T &input);

	/**
	 * Allocates a new instance of an object that is a deep copy of the given input including with the same dynamic runtime type.
	 * The input is used to obtain the proper metaclass and then becomes the input to the copy constructor.
	 *
	 * @tparam <T> The formal static type of the input and allocated objects
	 * @param input the input object
	 * @return the allocated object of the same dynamic type constructed by copy construction, or nullptr if input is nullptr
	 */
	template <typename T>
	inline std::unique_ptr<T> clonePointer(const T *input);

	/**
	 * Allocates a new instance of an object transferring the full runtime type content of the given input including with the same dynamic runtime type.
	 * The input is used to obtain the proper metaclass and then becomes the input to the move constructor.
	 *
	 * @tparam <T> The formal static type of the input and allocated objects
	 * @param input the input object
	 * @return the allocated object of the same dynamic type constructed by move construction
	 */
	template <typename T>
	inline std::unique_ptr<T> moveIntoNewInstance(T &&input);

	/**
	 * Prints the fully qualified class name to a C++ stream.
	 * 
	 * @param stream The stream
	 * @param mc The metaclass
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Metaclass &mc);
}

/* Inline implementation */

namespace cio
{
	inline void *Metaclass::element(void *buffer, std::size_t i) const
	{
		return static_cast<std::uint8_t *>(buffer) + i * mSize;
	}

	inline const void *Metaclass::element(const void *buffer, std::size_t i) const
	{
		return static_cast<const std::uint8_t *>(buffer) + i * mSize;
	}

	inline void *Metaclass::newInstance() const
	{
		void *buffer = this->allocate(1);
		this->construct(buffer, 1);
		return buffer;
	}

	inline void Metaclass::deleteInstance(void *buffer) const
	{
		this->destroy(buffer, 1);
		this->deallocate(buffer, 1);
	}

	inline void *Metaclass::newArray(std::size_t count) const
	{
		void *buffer = this->allocate(count);
		this->construct(buffer, count);
		return buffer;
	}

	inline void Metaclass::deleteArray(void *buffer, std::size_t count) const
	{
		this->destroy(buffer, count);
		this->deallocate(buffer, count);
	}

	template <typename T>
	inline std::unique_ptr<T> Metaclass::create() const
	{
		return std::unique_ptr<T>(static_cast<T *>(this->newInstance()));
	}

	template <typename T>
	inline bool Metaclass::is() const noexcept
	{
		return *this == cio::getDeclaredMetaclass<T>();
	}

	template <typename T>
	inline const Metaclass &getDeclaredMetaclass()
	{
		return T::getDeclaredMetaclass();
	}

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<void>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<bool>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<char>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<signed char>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<unsigned char>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<short>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<unsigned short>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<int>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<unsigned>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<long>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<unsigned long>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<long long>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<unsigned long long>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<float>();

	template<>
	CIO_API const Metaclass &getDeclaredMetaclass<double>();

	template <typename T>
	const Metaclass &getPointerMetaclass(const T *input)
	{
		return input ? getMetaclass(*input) : getDeclaredMetaclass<typename std::remove_cv<T>::type>();
	}

	template <>
	inline const Metaclass &getPointerMetaclass(const void *input)
	{
		return getDeclaredMetaclass<void>();
	}

	/**
	 * HasGetMetaclassType is a type traits detector template to determine if T has an invokable getMetaclass method.
	 * If it does, it inherits from std::true_type, otherwise it inherits from std::false_type.
	 *
	 * @tparam <T> The type of object being considered
	 */
	template <typename T, typename = Metaclass>
	struct HasGetMetaclassType : std::false_type {};

	/**
	 * HasGetMetaclassType is a type traits detector template to determine if T has an invokable getMetaclass method.
	 * If it does, it inherits from std::true_type, otherwise it inherits from std::false_type.
	 * This specialization uses the SFINAE rule to attempt to call the getMetaclass method at compile time to match the second template argument.
	 * If the method exists, return type is ignored and void is used instead, which matches the original defaulted template argument and thus
	 * makes this template specialization more "specialized" and causes this specialization to be used and inherits from std::true_type.
	 * Otherwise, the specialization is not used, and the general specialization inheriting from std::false_type is used.
	 *
	 * @tparam <T> The type of object being considered
	 */
	template <typename T>
	struct HasGetMetaclassType<T, typename std::decay<decltype(std::declval<T>().getMetaclass())>::type> : std::true_type {};

	/**
	 * Gets the metaclass from the input given that it has been detected as supporting getMetaclass method.
	 *
	 * @param input The input object
	 * @return the metaclass obtained by calling input.getMetaclass()
	 */
	template <typename T>
	inline const Metaclass &getMetaclass(const T &input, std::true_type)
	{
		return input.getMetaclass();
	}

	/**
	 * Gets the declared metaclass from the input given that it has been detected as not supporting getMetaclass method.
	 * This will trigger for all primitive types as well as any structs or classes not part of the CIO metaclass system.
	 *
	 * @param input The input object
	 * @return the metaclass obtained by calling getDeclaredMetaclass<T>()
	 */
	template <typename T>
	inline const Metaclass &getMetaclass(const T &input, std::false_type)
	{
		return getDeclaredMetaclass<T>();
	}

	template <typename T>
	inline const Metaclass &getMetaclass(const T &input)
	{
		return getMetaclass(input, HasGetMetaclassType<T>());
	}

	template <typename T>
	std::unique_ptr<T> newInstance(const T &input)
	{
		const Metaclass &metaclass = getMetaclass(input);
		return std::unique_ptr<T>(static_cast<T *>(metaclass.newInstance()));
	}

	template <typename T>
	std::unique_ptr<T> newInstance(const T *input)
	{
		return input ? newInstance(*input) : std::unique_ptr<T>();
	}

	template <typename T>
	std::unique_ptr<T> clone(const T &input)
	{
		const Metaclass &metaclass = getMetaclass(input);
		void *buffer = metaclass.allocate(1);
		metaclass.copy(&input, buffer, 1);
		return std::unique_ptr<T>(static_cast<T *>(buffer));
	}

	template <typename T>
	std::unique_ptr<T> clonePointer(const T *input)
	{
		return input ? clone(*input) : std::unique_ptr<T>();
	}

	template <typename T>
	std::unique_ptr<T> moveIntoNewInstance(T &&input)
	{
		const Metaclass &metaclass = getMetaclass(input);
		void *buffer = metaclass.allocate(1);
		metaclass.move(&input, buffer, 1);
		return std::unique_ptr<T>(static_cast<T *>(buffer));
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
