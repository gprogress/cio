/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PARSE_H
#define CIO_PARSE_H

#include "Types.h"

#include "ParseStatus.h"
#include "Type.h"

namespace cio
{
	/**
	 * The Parse template is a simple data structure for understanding the results of parsing a text section
	 * to capture underlying data.
	 *
	 * @tparam <T> The data type that was parsed
	 * @tparam <C> The character type of the input text, usually char
	 * @tparam <L> The length type of the input text, usually std::size_t
	 */
	template <typename T, typename C /* = char */, typename L /* = char */>
	class Parse : public ParseStatus<C, L>
	{
		public:
			/** The final parse result value */
			T value;
			
			/**
			 * Default constructor. Initializes parse status and state to empty results and calls value default constructor.
			 */
			inline Parse() noexcept;
			
			/**
			 * Construct from any parse type. The value is also copied.
			 *
			 * @tparam <U> The parse value type
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse type
			 */
			template <typename U, typename D, typename N>
			inline Parse(const Parse<U, D, N> &in) noexcept;
			
			/**
			 * Construct from any parse type. The value is also moved.
			 *
			 * @tparam <U> The parse value type
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse type
			 */
			template <typename U, typename D, typename N>
			inline Parse(Parse<U, D, N> &&in) noexcept;

			/**
			 * Construct from void parse type to ignore void value.
			 *
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse type
			 */
			template <typename D, typename N>
			inline Parse(const ParseStatus<D, N> &in) noexcept;
			
			/**
			 * Assign from any parse type. The value is also moved.
			 *
			 * @tparam <U> The parse value type
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse type
			 * @return this object
			 */
			template <typename U, typename D, typename N>
			inline Parse<T, C, L> &operator=(const Parse<U, D, N> &in) noexcept;

			/**
			 * Assign from void parse type to reset parse value.
			 *
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse type
			 * @return this object
			 */
			template <typename D, typename N>
			inline Parse<T, C, L> &operator=(const ParseStatus<D, N> &in) noexcept;

			/**
			 * Use the parsed value in its appropriate context.
			 *
			 * @return the parsed value
			 */
			inline operator T &() noexcept;

			/**
			 * Use the parsed value in its appropriate context.
			 *
			 * @return the parsed value
			 */
			inline operator const T &() const noexcept;
	};
	
	/**
	 * The Text Parse is a convenience alias to specialize the general Parse template to report
	 * only the parse results appropriate for a UTF-8 text string with an appropriate value.
	 */
	template <typename T>
	using TextParse = Parse<T, char, std::size_t>;

	/**
	 * Parses a single text character to interpret it as a "nibble" (the low 4 bits of a byte) from a hexadecimal digit.
	 *
	 * @param c The hexadecimal digit as a text character
	 * @return the nibble value from 0 to 15
	 */
	CIO_API std::uint8_t parseNibble(char c) noexcept;

	/**
	 * Parses a single text character to interpret it as a "nibble" (the low 4 bits of a byte) from a hexadecimal digit.
	 *
	 * @param c The hexadecimal digit as a text character
	 * @param byte The pointer of where to store the nibble value from 0 to 15
	 * @return whether the parsing was successful
	 */
	CIO_API bool parseNibble(char c, void *byte) noexcept;

	/**
	 * Parses two text characters to interpret it as a byte from two hexadecimal digit inputs.
	 *
	 * @param high The high nibble character
	 * @param low The low nibble character
	 * @return the parsed byte
	 */
	CIO_API std::uint8_t parseByte(char high, char low) noexcept;

	/**
	 * Parses two text characters to interpret it as a byte from two hexadecimal digit inputs.
	 *
	 * @param high The high nibble character
	 * @param low The low nibble character
	 * @param byte The pointer of where to store the parsed byte
	 * @return whether parsing was successful
	 */
	CIO_API bool parseByte(char high, char low, void *byte) noexcept;

	/**
	 * Parses a sequence of text to extract the binary representation from a hexadecimal encoding
	 * and store it in big-endian byte order in a buffer. This will result in the buffer containing
	 * the bytes in the same sequence as the text.
	 *
	 * @param input The input text
	 * @param output The buffer to store the binary output
	 * @param maxOutput The capacity of the output buffer
	 */
	CIO_API std::size_t parseBytes(const Text &input, void *output, std::size_t maxOutput);

	/**
	 * Perform the minimal checks to determine the primitive type a string would be if parsed.
	 * If the string is "true" or "false" this returns Boolean.
	 * If the string is a sequence of digits, optionally preceded with a "+" or "-", then it is an Integer.
	 * If the string starts with an Integer sequence and includes one decimal point followed by more digits and/or the character 'e' or 'E' followed by an integer, it is Real.
	 * The special strings "Infinity" and "NaN" (of any case) optionally preceded by "+" or "-" are also considered Real.
	 * If the string starts with "0x" and then is a sequence of hexadecimal digits, it is a Blob.
	 * Any other value, including the empty string or the null pointer, is Text.
	 *
	 * @param text The text to examine
	 * @return the native primitive type of the text if it were parsed
	 */
	CIO_API Type detectParsedType(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned character from text input, validating the results.
	 * This parses the text as an unsigned integer within the unsigned char range.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<unsigned char> &p) noexcept;

	/**
	 * Perform optimized parsing of a character from text input, validating the results.
	 * This overload parses a single UTF-8 text character.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<char> &p) noexcept;

	/**
	 * Perform optimized parsing of a signed character from text input, validating the results.
	 * This parses the text as a signed integer within the signed char range.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<signed char> &p) noexcept;

	/**
	 * Perform optimized parsing of an unsigned short from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<unsigned short> &p) noexcept;

	/**
	 * Perform optimized parsing of a short from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<short> &p) noexcept;

	/**
	 * Perform optimized parsing of an unsigned int from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<unsigned int> &p) noexcept;

	/**
	 * Perform optimized parsing of an int from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<int> &p) noexcept;

	/**
	 * Perform optimized parsing of an unsigned long from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<unsigned long> &p) noexcept;

	/**
	 * Perform optimized parsing of a long from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<long> &p) noexcept;

	/**
	 * Perform optimized parsing of a double from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<double> &p) noexcept;

	/**
	 * Perform optimized parsing of a long double from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<long double> &p) noexcept;

	/**
	 * Perform optimized parsing of a float from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<float> &p) noexcept;

	/**
	 * Perform optimized parsing of a string from text input, validating the results.
	 *
	 * @param text The text input
	 * @param p the reference to the parsed parameter that will be populated with the given primitive type and
	 * all validation parameters.
	 */
	CIO_API void parseType(const Text &text, Parse<std::string>& p) noexcept;

	/**
	 * Perform optimized parsing of a Boolean from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the Boolean value and all validation properties from the parse
	 */
	CIO_API TextParse<bool> parseBoolean(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned character from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the character value and all validation properties from the parse
	 */
	CIO_API TextParse<unsigned char> parseUnsignedChar(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a character from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the character value and all validation properties from the parse
	 */
	CIO_API TextParse<char> parseChar(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned short integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the short integer value and all validation properties from the parse
	 */
	CIO_API TextParse<unsigned short> parseUnsignedShort(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a short integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the short integer value and all validation properties from the parse
	 */
	CIO_API TextParse<short> parseShort(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<unsigned int> parseUnsignedInt(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<int> parseInt(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned long integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the long integer value and all validation properties from the parse
	 */
	CIO_API TextParse<unsigned long> parseUnsignedLong(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a long integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the long integer value and all validation properties from the parse
	 */
	CIO_API TextParse<long> parseLong(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a double from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the double value and all validation properties from the parse
	 */
	CIO_API TextParse<double> parseDouble(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a long double from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the long double value and all validation properties from the parse
	 */
	CIO_API TextParse<long double> parseLongDouble(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a float from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the float value and all validation properties from the parse
	 */
	CIO_API TextParse<float> parseFloat(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned 8-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::uint8_t> parseUnsigned8(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned 16-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::uint16_t> parseUnsigned16(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned 32-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::uint32_t> parseUnsigned32(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned 64-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::uint64_t> parseUnsigned64(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an unsigned integer from text input, validating the results.
	 * This returns the largest possible unsigned integer to account for all possible input.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<unsigned long long> parseUnsigned(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a signed 8-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::int8_t> parseInteger8(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a signed 16-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::int16_t> parseInteger16(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a signed 32-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::int32_t> parseInteger32(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of a signed 64-bit integer from text input, validating the results.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<std::int64_t> parseInteger64(const Text &text) noexcept;

	/**
	 * Perform optimized parsing of an signed integer from text input, validating the results.
	 * This returns the largest possible signed integer to account for all possible input.
	 *
	 * @param text The text input
	 * @return the parse result with the integer value and all validation properties from the parse
	 */
	CIO_API TextParse<long long> parseInteger(const Text &text) noexcept;
}

/* Inline implementation */

namespace cio
{
	template <typename T, typename C, typename L>
	inline Parse<T, C, L>::Parse() noexcept :
		value()
	{
		// nothing more to do
	}
	
	template <typename T, typename C, typename L>
	template <typename U, typename D, typename N>
	inline Parse<T, C, L>::Parse(const Parse<U, D, N> &in) noexcept :
		ParseStatus<C, L>(in),
		value(in.value)
	{
		// nothing more to do
	}
	
	template <typename T, typename C, typename L>
	template <typename U, typename D, typename N>
	inline Parse<T, C, L>::Parse(Parse<U, D, N> &&in) noexcept :
		ParseStatus<C, L>(in),
		value(std::move(in.value))
	{
		// nothing more to do
	}
	
	template <typename T, typename C, typename L>
	template <typename D, typename N>
	inline Parse<T, C, L>::Parse(const ParseStatus<D, N> &in) noexcept :
		ParseStatus<C, L>(in),
		value()
	{
		// nothing more to do
	}

	template <typename T, typename C, typename L>
	template <typename U, typename D, typename N>
	inline Parse<T, C, L> &Parse<T, C, L>::operator=(const Parse<U, D, N> &in) noexcept	
	{
		ParseStatus<C, L>::operator=(in);
		this->value = in.value;
		return *this;
	}

	template <typename T, typename C, typename L>
	template <typename D, typename N>
	inline Parse<T, C, L> &Parse<T, C, L>::operator=(const ParseStatus<D, N> &in) noexcept
	{
		ParseStatus<C, L>::operator=(in);
		this->value = T();
		return *this;
	}

	template <typename T, typename C, typename L>
	inline Parse<T, C, L>::operator T &() noexcept
	{
		return this->value;
	}

	template <typename T, typename C, typename L>
	inline Parse<T, C, L>::operator const T &() const noexcept
	{
		return this->value;
	}
}

#endif
