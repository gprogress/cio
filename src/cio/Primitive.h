/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PRIMITIVE_H
#define CIO_PRIMITIVE_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Primitive enumeration specifies standard numeric layouts for individual component values or sentinels for default or custom quantization.
	 */
	enum class Primitive : std::uint8_t
	{
		/** No data bits */
		None = 0x00,
		
		/** Packed bits for boolean data */
		Boolean = 0x01,

		/** Unsigned 4 bit integer */
		Unsigned4 = 0x03,

		/** Unsigned 8 bit integer */
		Unsigned8 = 0x04,
		
		/** Unsigned 16-bit integer */
		Unsigned16 = 0x05,
					
		/** Unsigned 32-bit integer */
		Unsigned32 = 0x06,
		
		/** Unsigned 64-bit integer */
		Unsigned64 = 0x07,
		
		/** Unsigned 128-bit integer */
		Unsigned128 = 0x08,

		/** Unsigned 256-bit integer */
		Unsigned256 = 0x09,

		/** Unspecified signed integer */
		Signed = 0x10,
		
		/** Signed 4 bit integer */
		Signed4 = 0x13,
		
		/** Signed 8 bit integer */
		Signed8 = 0x14,
		
		/** Signed 16-bit integer */
		Signed16 = 0x15,
		
		/** Signed 32-bit integer */
		Signed32 = 0x16,
		
		/** Signed 64-bit integer */
		Signed64 = 0x17,
		
		/** Signed 128-bit integer */
		Signed128 = 0x18,

		/** Signed 256-bit integer */
		Signed256 = 0x19,

		/** 8-bit "quarter" floating point */
		Float8 = 0x24,

		/** 16-bit "half" floating point */
		Float16 = 0x25,
		
		/** 32-bit "single" floating point */
		Float32 = 0x26,
		
		/** 64-bit "double" floating point */
		Float64 = 0x27,
		
		/** 128-bit "quad" floating point */
		Float128 = 0x28,
		
		/** UTF-8 text character */
		Char8 = 0x34,
		
		/** UTF-16 text character */
		Char16 = 0x35,
	
		/**  UCS-4 text character */
		Char32 = 0x36,
		
		/** General binary bit for blob data */
		Bit = 0x41,
		
		/** General binary nibble for blob data */
		Nibble = 0x43,
		
		/** General binary byte for blob data */
		Byte = 0x44,
		
		/** General binary 16-bit aligned words (short / half words) */
		Word16 = 0x45,
		
		/** General binary 32-bit aligned words */
		Word32 = 0x46,
		
		/** General binary 64-bit aligned words (long / double words) */
		Word64 = 0x47,
		
		/** General binary 128-bit aligned words (long long / quad words) */
		Word128 = 0x48,

		/** General binary 256-bit aligned words */
		Word256 = 0x49,

		/** Default representation for type */
		Default = 0x80,
		
		/** Custom quantization of data */
		Quantized = 0xF0,
		
		/** Variant class used for any type */
		Variant = 0xFF
	};
	
	/**
	 * Gets the general primitive that best matches the given component type.
	 *
	 * @param type The the component type
	 * @return the primitive
	 */
	CIO_API Type type(Primitive type) noexcept;
	
	/**
	 * Gets the number of bits per component for the given component type.
	 * Default and Quantized return 0.
	 *
	 * @return the number of bits per component
	 */
	inline std::size_t bits(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is a signed or unsigned integer type.
	 * For the purposes of this check, Boolean also passes.
	 *
	 * @param type The type
	 * @return whether the type is a signed or unsigned integer type
	 */
	inline bool isInteger(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is a signed integer type.
	 *
	 * @param type The type
	 * @return whether the type is a signed integer type
	 */
	inline bool isSignedInteger(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is an unsigned integer type.
	 * For the purposes of this check, Boolean also passes.
	 *
	 * @param type The type
	 * @return whether the type is an unsigned integer type
	 */
	inline bool isUnsignedInteger(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is floating point.
	 *
	 * @param type The type
	 * @return whether the type is floating point
	 */
	inline bool isFloat(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is character text.
	 *
	 * @param type The type
	 * @return whether the type is character text
	 */
	inline bool isText(Primitive type) noexcept;
	
	/**
	 * Checks whether the given component type is opaque binary data for blobs.
	 *
	 * @param type The type
	 * @return whether the type is binary blob data
	 */
	inline bool isBlob(Primitive type) noexcept;
	
	/**
	 * Gets the text label for a component type.
	 *
	 * @param type The component type
	 * @return the text label
	 */
	CIO_API const char *print(Primitive type);

	/**
	 * Prints the text label for a component type to a C++ stream.
	 *
	 * @param s The C++ stream
	 * @param type The type
	 * @return the stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Primitive type);
}

/* Inline implementation */

namespace cio
{
	inline std::size_t bits(Primitive type) noexcept
	{
		unsigned bits = 0;
		unsigned log = static_cast<unsigned>(type) & 0x07;
		
		if (log > 0)
		{
			bits = (1 << (log - 1u));
		}
		
		return bits;
	}
	

	inline bool isInteger(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) <= 0x10;
	}

	inline bool isSignedInteger(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) == 0x10;
	}

	inline bool isUnsignedInteger(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) == 0x00;
	}

	inline bool isFloat(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) == 0x20;
	}

	inline bool isText(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) == 0x30;
	}
	
	inline bool isBlob(Primitive type) noexcept
	{
		return (static_cast<std::size_t>(type) & 0xF0) == 0x40;
	}
}

#endif
