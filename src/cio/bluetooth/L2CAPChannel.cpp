/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "L2CAPChannel.h"

#include "AddressType.h"
#include "Device.h"
#include "HCICodec.h"

#include <cio/DeviceAddress.h>
#include <cio/Class.h>
#include <cio/ModeSet.h>

#include <cio/net/Address.h>
#include <cio/net/AddressFamily.h>
#include <cio/net/ChannelType.h>
#include <cio/net/ProtocolFamily.h>

#if defined CIO_USE_WINSOCK2
#include <WinSock2.h>
#include <ws2bth.h>

#elif defined CIO_USE_UNIX_SOCKETS
#include <sys/socket.h>

struct sockaddr_l2
{
	sa_family_t l2_family;

	std::uint16_t l2_psm;

	std::uint8_t l2_bdaddr[6];

	std::uint16_t l2_cid;

	std::uint8_t l2_bdaddr_type;
};

#ifndef AF_BLUETOOTH
#define AF_BLUETOOTH 31
#endif

#ifndef BTPROTO_L2CAP
#define BTPROTO_L2CAP 1
#endif

#else
#error "No socket implementation available to use for L2CAP"
#endif

namespace cio
{
	namespace bluetooth
	{
		Class<L2CAPChannel> L2CAPChannel::sMetaclass("cio::bluetooth::L2CAPChannel");
		
		Logger L2CAPChannel::sLogger("cio::bluetooth::L2CAPChannel");
		
		const Metaclass &L2CAPChannel::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

// Windows-specific implementation
#if defined CIO_USE_WINSOCK2
		cio::net::AddressFamily L2CAPChannel::family() noexcept
		{
			// AF_BLUETOOTH
			return cio::net::AddressFamily(AF_BTH);
		}

		cio::net::Address L2CAPChannel::makeAddress(const DeviceAddress &remote, AddressType remoteType, std::uint16_t cid, std::uint16_t psm)
		{
			SOCKADDR_BTH address = { };
			address.addressFamily = AF_BTH;
			HCICodec::toNativeAddress(remote, &address.btAddr);
			address.port = psm;

			return cio::net::Address(&address, sizeof(SOCKADDR_BTH));
		}
		cio::net::ProtocolFamily L2CAPChannel::protocol() noexcept
		{
			return cio::net::ProtocolFamily(BTHPROTO_L2CAP);
		}

// Unix-specific implementation
#elif defined CIO_USE_UNIX_SOCKETS

		cio::net::AddressFamily L2CAPChannel::family() noexcept
		{
			return cio::net::AddressFamily(AF_BLUETOOTH);
		}

		cio::net::ProtocolFamily L2CAPChannel::protocol() noexcept
		{
			return cio::net::ProtocolFamily(BTPROTO_L2CAP);
		}

		cio::net::Address L2CAPChannel::makeAddress(const DeviceAddress &remote, AddressType remoteType, std::uint16_t cid, std::uint16_t psm)
		{
			struct sockaddr_l2 address;
			std::memset(&address, 0, sizeof(address));
			address.l2_family = AF_BLUETOOTH;
			HCICodec::toNativeAddress(remote, &address.l2_bdaddr);

			address.l2_cid = Little::order(cid);
			address.l2_psm = Little::order(psm);
			
			address.l2_bdaddr_type = static_cast<std::uint8_t>(remoteType);		
			return cio::net::Address(&address, sizeof(struct sockaddr_l2));
		}
#endif

		cio::net::Socket L2CAPChannel::makeSocket()
		{
			return cio::net::Socket(family(), channel(), protocol());
		}

		cio::net::ChannelType L2CAPChannel::channel() noexcept
		{
			return cio::net::ChannelType::Sequential;
		}
				
		L2CAPChannel::L2CAPChannel() noexcept
		{
			// nothing more to do
		}
		
		L2CAPChannel::L2CAPChannel(L2CAPChannel &&in) noexcept :
			cio::net::Channel(std::move(in))
		{
			// nothing more to do
		}
		
		L2CAPChannel &L2CAPChannel::operator=(L2CAPChannel &&in) noexcept
		{
			cio::net::Channel::operator=(std::move(in));
			return *this;
		}
		
		L2CAPChannel::~L2CAPChannel() noexcept = default;
		
		const Metaclass &L2CAPChannel::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		cio::ModeSet L2CAPChannel::openWithFactory(const cio::Path &path, cio::ModeSet modes, cio::ProtocolFactory *factory)
		{
			cio::ModeSet actual;
			
			// TODO implement parsing and device setup
			
			return actual;
		}
		
		void L2CAPChannel::connectThroughDevice(const DeviceAddress &local, AddressType localType, std::uint16_t cid, std::uint16_t psm)
		{
			if (!mSocket)
			{
				mSocket = L2CAPChannel::makeSocket();
			}
			
			sLogger.info() << "Connecting via local device " << local << " (" << localType << ") with CID " << cid << ", PSM = " << psm;
			cio::net::Address address = L2CAPChannel::makeAddress(local, localType, cid, psm);
			
			mSocket.bind(address);
		}
		
		void L2CAPChannel::connectToRemoteDevice(const DeviceAddress &remote, AddressType remoteType, std::uint16_t cid, std::uint16_t psm)
		{
			if (!mSocket)
			{
				mSocket = L2CAPChannel::makeSocket();
			}
			
			sLogger.info() << "Connecting to remote device " << remote << " (" << remoteType << ") with CID " << cid << ", PSM = " << psm;
			cio::net::Address address = L2CAPChannel::makeAddress(remote, remoteType, cid, psm);
			mSocket.connect(address);
		}
	}
}
