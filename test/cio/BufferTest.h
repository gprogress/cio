
#ifndef ACSL_BUFFERTEST_H
#define ACSL_BUFFERTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	* Tests the basic functionality of a buffer.
	*/
	class BufferTest : public cio::test::Suite
	{
		public:
			BufferTest();
			
			virtual ~BufferTest() noexcept;

			void testConstruction();

			void testAssignment();

			void testWrite();

			void testPut();

			void testRead();

			void testGet();

			void testSetPosition();

			void testSetLimit();

			void testReallocate();

			void testReset();

			void testFlip();

			void testClear();

			void testCompact();
	};
}

#endif
