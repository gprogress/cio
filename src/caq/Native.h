/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_NATIVE_H
#define CAQ_NATIVE_H

#include "Types.h"

#include "Accessor.h"
#include "Reader.h"

namespace caq
{
	/**
	 * The Native accessor provides direct access to an aligned native datatype T via interleaved arrays with a fixed number of components
	 * backed by an underlying array of type U. The types must be directly copyable. Cast warnings are not suppressed.
	 * 
	 * @warning This class cannot be used if the data components are not aligned to the native memory unit of T and are not interleaved
	 * in a contiguous array.
	 * 
	 * @tparam T The component type
	 * @tparam U The underlying component type, which must be copyable to T
	 * @tparam C The number of components
	 */
	template <typename T, typename U, std::size_t C>
	class Native : public Reader<T>
	{
		public:
			/**
			 * Default constructor.
			 */
			inline Native() = default;

			/**
			 * Construct a native accessor given the data buffer and size.
			 * 
			 * @param data The data buffer
			 * @param length The array element count
			 */
			inline Native(U *data, std::size_t length) :
				mData(data),
				mSize(length)
			{
				// nothing more to do
			}

			/**
			 * Destructor.
			 */
			inline virtual ~Native() noexcept override = default;

			// Override Abstract accessor methods

			/**
			 * Gets the number of array elements in this accessor.
			 * 
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return mSize;
			}

			/**
			 * Gets the fixed number of element components in this accessor.
			 * 
			 * @return The number of components
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return C;
			}

			/**
			 * Interprets whether this accessor is usable.
			 * The Native accessor returns true if the data pointer is set and the size is nonzero.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mData != nullptr && mSize > 0;
			}

			// Override Reader<T> accessor methods

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The Native accessor cannot be further optimized, so it returns itself.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				return Accessor<T>(*this);
			}

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const noexcept override
			{
				return mData[n * C + c];
			}

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 * The Native implementation directly accesses the given array index element using the offset and stride.
			 *
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const noexcept override
			{
				const std::size_t start = n * C;
				std::copy(mData + start, mData + start + C, buffer);
				return buffer + C;
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 * The Native implementation directly accesses the each array index element using the offset and stride.
			 *
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const noexcept override
			{
				const std::size_t start = noff * C;
				const std::size_t end = (noff + count) * C;
				std::copy(mData + start, mData + end, buffer);
				return buffer + count * C;
			}

			// Methods unique to Native<T, C>

			/**
			 * Sets the data array and array index count.
			 * 
			 * @param data The data array
			 * @param size The number of array elements
			 */
			inline void bind(U *data, std::size_t size) noexcept
			{
				mData = data;
				mSize = size;
			}

			/**
			 * Gets access to the underlying data.
			 * 
			 * @return The underlying data
			 */
			inline U *data() noexcept
			{
				return mData;
			}

			/**
			 * Gets access to the underlying data.
			 *
			 * @return The underlying data
			 */
			const U *data() const noexcept
			{
				return mData;
			}

		private:
			/** The number of array index elements */
			std::size_t mSize = 0;

			/** The array data pointer */
			U *mData = nullptr;
	};

	/** 
	 * Convenience type for a native fixed accessor that doesn't need casting.
	 *
	 * @tparam T The component type
	 * @tparam C The component count
	 */
	template <typename T, std::size_t C>
	using NativeN = Native<T, T, C>;

	/**
	 * Convenience type for a native fixed single component array accessor that doesn't need casting.
	 *
	 * @tparam T The component type
	 * @tparam C The component count
	 */
	template <typename T>
	using ViewArray = Native<T, T, 1>;
}

#endif
