/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LISTENER_H
#define CIO_LISTENER_H

#include "Types.h"

#include "Task.h"

namespace cio
{
	/**
	 * The CIO Listener template extends the base CIO Task to implement a callback listener to receive one or more
	 * sets of arguments provided to a callback function.
	 *
	 * The callback is set on construction in the base listener class and cannot be modified after construction.
	 * Subclasses such as Receiver internally manage the callback to do more specific operations.
	 *
	 * @tparam <T> The callback argument types
	 */
	template <typename... T>
	class Listener : public Task
	{
		public:
			/**
			 * Construct a listener with no callback.
			 */
			Listener() noexcept;
			
			/**
			 * Construct a listener with the given moved callback.
			 *
			 * @param callback The callback to copy
			 */
			Listener(Callback<T...> &&callback) noexcept;
			
			/**
			 * Construct a listener with the given copied callback.
			 *
			 * @param callback The callback to copy
			 */
			Listener(const Callback<T...> &callback);
			
			/**
			 * Destructor.
			 */
			~Listener() noexcept;

			/**
			 * Attempts to invoke the callback with the given arguments and notify any interested listeners
			 * that progress has occurred.
			 *
			 * The task is locked for the during of the invocation.
			 *
			 * @param args The arguments to provide to the callback
			 */
			template <typename... A>
			State post(A... args) noexcept;
			
			/**
			 * Gets the callback currently in use.
			 *
			 * @return the callback
			 */
			const Callback<T...> &getCallback() const noexcept;
			
		protected:
			/** The callback to receive updates */
			Callback<T...> callback;
	};
}

/* Inline implementation */

namespace cio
{
	template <typename... T>
	inline Listener<T...>::Listener() noexcept = default;
	
	template <typename... T>	
	inline Listener<T...>::Listener(Callback<T...> &&callback) noexcept :
		callback(std::move(callback))
	{
		// nothing more to do
	}
	
	template <typename... T>			
	inline Listener<T...>::Listener(const Callback<T...> &callback) :
		callback(callback)
	{
		// nothing more to do
	}
			
	template <typename... T>
	inline Listener<T...>::~Listener() noexcept = default;
	
	template <typename... T>	
	template <typename... A>
	inline State Listener<T...>::post(A... args) noexcept
	{
		State status;
		try
		{
			if (this->callback)
			{
				this->callback(std::forward<A...>(args...));
				this->update(1u);
			}
			status.succeed();
		}
		catch (Exception &e)
		{
			status.fail(e.reason());
			this->fail(std::move(e));
		}
		catch (...)
		{
			this->fail(std::current_exception());
		}

		return status;
	}
	
	template <typename... T>
	inline const Callback<T...> &Listener<T...>::getCallback() const noexcept
	{
		return this->callback;
	}
}

#endif

