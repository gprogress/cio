/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DeviceAddressTest.h"

#include <cio/DeviceAddress.h>
#include <cio/test/Assert.h>

namespace cio
{
	DeviceAddressTest::DeviceAddressTest() :
		cio::test::Suite("cio/DeviceAddress")
	{		
		this->addTestCase("DeviceAddress", &DeviceAddressTest::testConstruction);
		this->addTestCase("operator=", &DeviceAddressTest::testAssignment);
		this->addTestCase("clear", &DeviceAddressTest::testClear);
		this->addTestCase("value", &DeviceAddressTest::testValue);
		this->addTestCase("setMac", &DeviceAddressTest::testSetMac);
		this->addTestCase("printMac",  &DeviceAddressTest::testPrintMac);
	}

	DeviceAddressTest::~DeviceAddressTest() noexcept = default;

	void DeviceAddressTest::testConstruction()
	{
		// Default construction
		{
			std::uint64_t expected = 0;
		
			DeviceAddress dev;
			CIO_ASSERT_EQUAL(expected, dev.value());
		}
		
		// Construct with 64-bit data value
		{
			std::uint64_t expected = 0xF0E1D2ABEF76C3B4ull;
			
			DeviceAddress dev(expected);
			CIO_ASSERT_EQUAL(expected, dev.value());
		}
		
		// Construct with text value for MAC
		{
			const char *text = "F0:E1:D2:C3:B4:95";
			std::uint64_t expected = 0xF0E1D20000C3B495ull;
			
			DeviceAddress dev(text);
			CIO_ASSERT_EQUAL(expected, dev.value());
		}
		
		// Construct with text value for EUI-64
		{
			const char *text = "F0:E1:D2:AB:EF:76:C3:B4";
			std::uint64_t expected = 0xF0E1D2ABEF76C3B4ull;
			
			DeviceAddress dev(text);
			CIO_ASSERT_EQUAL(expected, dev.value());
		}
		
		// Test copy construction
		{
			std::uint64_t expected = 0xF0E1D2ABEF76C3B4ull;
			DeviceAddress dev(expected);
			DeviceAddress copy(dev);
			
			CIO_ASSERT_EQUAL(expected, copy.value());
		}
	}

	void DeviceAddressTest::testAssignment()
	{
		// Test copy assignment
		{
			std::uint64_t expected = 0xF0E1D2ABEF76C3B4ull;
			DeviceAddress dev(expected);
			DeviceAddress copy;
			
			copy = dev;
			
			CIO_ASSERT_EQUAL(expected, copy.value());
		}
	}
	
	void DeviceAddressTest::testClear()
	{
		std::uint64_t expected = 0;
		DeviceAddress dev;
		
		dev.clear();
		CIO_ASSERT_EQUAL(expected, dev.value());
		
		std::uint64_t changed = 0xF0E1D2ABEF76C3B4ull;
		dev.value(changed);
		CIO_ASSERT_EQUAL(changed, dev.value());
		
		dev.clear();
		CIO_ASSERT_EQUAL(expected, dev.value());
	}
	
	void DeviceAddressTest::testValue()
	{
		DeviceAddress dev;
		
		std::uint64_t zero = 0u;
		CIO_ASSERT_EQUAL(zero, dev.value());
		CIO_ASSERT_EQUAL(0u, std::memcmp(&dev, &zero, 8u));
		
		std::uint64_t expected = 0xF0E1D2ABEF76C3B4ull;
		dev.value(expected);
		CIO_ASSERT_EQUAL(expected, dev.value());
		CIO_ASSERT_EQUAL(0u, std::memcmp(&dev, &expected, 8u));
		
		dev.value(0u);
		CIO_ASSERT_EQUAL(zero, dev.value());
		CIO_ASSERT_EQUAL(0u, std::memcmp(&dev, &zero, 8u));
	}
	
	void DeviceAddressTest::testSetMac()
	{
		DeviceAddress dev;
		
		std::uint64_t eui64 = 0xF0E1D20000C3B495ull;
		std::uint64_t mac = 0xF0E1D2C3B495;
		
		dev.setMac(mac);
		CIO_ASSERT_EQUAL(eui64, dev.value());
		
		// Do it with extra junk in front
		std::uint64_t junk = 0xBEEFF0E1D2C3B495;
		dev.setMac(mac);
		CIO_ASSERT_EQUAL(eui64, dev.value());
	}
	
	void DeviceAddressTest::testPrintMac()
	{
		// Print the empty MAC
		{
			DeviceAddress dev;
			char buffer[64] = { };
			std::string expected = "00:00:00:00:00:00";
			
			// Test printing to buffer
			CIO_ASSERT_EQUAL(17u, dev.printMac(buffer, 64));
			CIO_ASSERT_EQUAL(expected, buffer);
		}
	
		// Print a good MAC
		{
			DeviceAddress dev;
			char buffer[64] = { };
			std::string expected = "F0:E1:D2:C3:B4:95";
			
			std::uint64_t eui64 = 0xF0E1D20000C3B495ull;
			
			dev.value(eui64);
			CIO_ASSERT_EQUAL(eui64, dev.value());
			
			// Test printing to a good buffer
			CIO_ASSERT_EQUAL(17u, dev.printMac(buffer, 64));
			CIO_ASSERT_EQUAL(expected, buffer);
		}
		
		// Print an EUI-64
		{
			DeviceAddress dev;
			char buffer[64] = { };
			std::uint64_t notMac =  0xF0E1D2ABEF76C3B4ull;
			dev.value(notMac);
			CIO_ASSERT_EQUAL(0u, dev.printMac(buffer, 64));
		}
	}
}
