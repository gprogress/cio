/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_DEVICE_H
#define CIO_DEVICE_H

#include "Types.h"

#include "Channel.h"

#include <cio/Text.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The CIO Device class provides the lowest level access to the native device read/write API.
	* The exact range of devices depends on the host operating system, and this base class provides
	* the most basic operations without providing items that are specific to particular device types.
	* The resource open operations for this base class directly provide input resource URI paths to the native
	* operating system without interpretation.
	*
	* @note Most users will want to use a more structured subclass of this class such as File, Pipe, or Serial
	* to have resource and configuration parameters more appropriate to specific device types.
	*
	* @warning This device object may be unbuffered, and as such may also have various
	* platform-specific restrictions on how data must be written and read. It is intended
	* for full low-level control and performance rather than ease of use.
	*/
	class CIO_API Device : public Channel
	{
		public:
			/**
			* Gets the metaclass for the Channel class.
			*
			* @return the metaclass for this class
			*/
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Gets the sentinel handle for an unopened or invalid device.
			* This is platform specific.
			*
			* @return the invalid device handle
			*/
			static std::uintptr_t getInvalidHandle() noexcept;

			/**
			* Construct an instance not connected to any device.
			*/
			Device() noexcept;

			/**
			* Construct a native channel by adopting the given native handle.
			*
			* @param handle The native handle
			*/
			explicit Device(std::uintptr_t handle) noexcept;

			/**
			* Move a device connection into a new instance of this class.
			*
			* @param in The device to move
			*/
			Device(Device &&in) noexcept;

			/**
			* Move a device connection into this object.
			* The existing device connection is closed.
			*
			* @param in The device to move
			* @return this device
			*/
			Device &operator=(Device &&in) noexcept;

			/**
			* Close and tear down a device instance.
			*/
			virtual ~Device() noexcept override;

			/**
			* Gets the metaclass for the runtime type of this object.
			*
			* @return the metaclass
			*/
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			* Gets the raw device handle as used by the operating system.
			* This device retains ownership of the handle.
			*
			* @return the device handle
			*/
			std::uintptr_t getHandle() const noexcept;

			/**
			* Takes ownership of the raw device as used by the operating system.
			* This object instance will no longer reference the device handle and can no longer
			* be used to read or write the device.
			*
			* You must use operating system specific functions to close the returned handle,
			* and if it requires any additional cleanup beyond the usual, that's on you to deal with.
			*
			* @return the device handle
			*/
			std::uintptr_t takeHandle() noexcept;

			/**
			* Sets this object to take ownership of the given raw device handle as used by the operating system.
			* The previously open device, if any, is closed.
			* The object instance will point to the provided device and will close the handle on destruction.
			*
			* @param handle The device handle to adopt
			*/
			void adoptHandle(std::uintptr_t handle) noexcept;

			/**
			* Opens a device connection to the given path.
			* The path may be a file path or, depending on the platform,
			* a device path or special identifier.
			*
			* The device will be opened for read and write and created if missing.
			*
			* @param path The path to open
			* @throw Exception If the device could not be opened
			*/
			ModeSet openNativePath(Text path);

			/**
			* Opens a device connection to the given path.
			* The path may be a file path or, depending on the platform,
			* a device path or special identifier.
			*
			* The device will be opened with the desired modes.
			*
			* @param path The path to open
			* @param modes The desired modes to open the device with
			* @throw Exception If the device could not be opened
			*/
			ModeSet openNativePath(Text path, ModeSet modes);

			/**
			* Close the current device connection.
			*/
			virtual void clear() noexcept override;
				
			/**
			 * Checks whether the device is currently open.
			 * This is true as long as the native handle is valid.
			 *
			 * @return whether the device is open
			 */
			virtual bool isOpen() const noexcept override;

			virtual ModeSet openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory) override;

			virtual bool linkedReadWritePosition() const noexcept override;

			virtual Length size() const noexcept override;

			virtual Resize resizable() const noexcept override;
				
			virtual bool appendable() const noexcept override;

			virtual Progress<Length> requestResize(Length length) noexcept override;

			// Input API - polymorphic getters

			virtual Length readable() const noexcept override;

			virtual Length getReadPosition() const noexcept override;

			/**
				* Gets whether the read position is directly seekable on this input and if so in which direction.
				* 
				* For native devices, this depends on the underlying device model so the answer could be anything.
				* Typically for files and block devices it is Resize::Any and all other devices Resize::None.
				* 
				* @return the read seekability status
				*/
			virtual Resize isReadSeekable() const noexcept override;

			// Output API - polymorphic getters

			virtual Length writable() const noexcept override;

			virtual Length getWritePosition() const noexcept override;

			/**
				* Gets whether the write position is directly seekable on this output and if so in which direction.
				* 
				* For native devices, this depends on the underlying device model so the answer could be anything.
				* Typically for files and block devices it is Resize::Any and all other devices Resize::None.
				*
				* @return the write seekability status
				*/
			virtual Resize isWriteSeekable() const noexcept override;

			// Input API - polymorphic requests

			/**
			* Reads raw data from the device.
			* @param buffer The buffer to read into
			* @param length The number of bytes to read
			* @return the actual number of bytes read
			*/
			virtual Progress<std::size_t> requestRead(void *data, std::size_t length) noexcept override;

			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t length, Length position, Seek mode) noexcept override;

			virtual Progress<Length>  requestSeekToRead(Length position, Seek mode) noexcept override;

			virtual Progress<Length> requestDiscard(Length bytes) noexcept override;

			// Output API - polymorphic requests

			/**
			* Writes raw data to the device.
			*
			* @note Normally the device is unbuffered so the write will go straight to the device in one pass.
			* However, specific types of devices on specific platforms may be buffered, in which case you will need
			* to call commitWriteBuffer for the write to actually proceed.
			*
			* @param buffer The buffer to write from
			* @param length The number of bytes to write
			* @return the actual number of bytes written
			*/
			virtual Progress<std::size_t> requestWrite(const void *data, std::size_t bytes) noexcept override;

			virtual Progress<std::size_t> requestWriteToPosition(const void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			virtual Progress<Length> requestSeekToWrite(Length position, Seek mode) noexcept override;

			virtual Progress<Length> requestSkip(Length bytes) noexcept override;

			/**
			* If this device buffers writes, this will commit the write buffer to the physical device.
			*
			* On some platforms, this may have no effect since all writes are unbuffered.
			*
			* @param recursive Whether to flush recursively, which has no effect on this class
			* @return the result status and bytes flushed (if known)
			*/
			virtual Progress<Traversal> flush(Traversal recursive) noexcept override;

			/**
			* Reports whether writes of this device are buffered.
			*
			* @return are writes buffered?
			*/
			bool isWriteBuffered() const;

		protected:
			/**
				* Performs the actual OS-dependent disposal of the underlying device handle for clear() and destructor.
				*/
			void disposeHandle() noexcept;

			/** The system handle to the device */
			std::uintptr_t mHandle;
				
			/** The native file path to remove on close if this is not automatically handled by the operating system */
			Text mRemoveOnClose;

			/** Whether it is known or suspected that writes may be buffered on this device. */
			bool mWriteBuffered;
				
		private:
			/** The metaclass for this class */
			static Class<Device> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
