/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Packet.h"

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sPacketText[] = {
			"None",
			"Command",
			"Asynchronous Data",
			"Synchronous Data",
			"Event",
			// Discontinuity
			"Extended Command"
		};

		const char *print(Packet type) noexcept
		{
			const char *text = "Unknown";

			if (type == Packet::ExtendedCommand)
			{
				text = sPacketText[0x05];
			}
			else if (type <= Packet::Event)
			{
				text = sPacketText[static_cast<std::size_t>(type)];
			}

			return text;
		}

		std::ostream &operator<<(std::ostream &s, Packet type)
		{
			return s << print(type);
		}
	}

}
