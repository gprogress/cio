/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Program.h"

#include "Buffer.h"
#include "Filesystem.h"
#include "Pipe.h"
#include "Response.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>
#elif defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#include <sys/wait.h>
#include <thread>
#endif

#if defined _MSC_VER
#pragma warning(disable: 4996)
#endif

namespace cio
{
	const std::uintptr_t Program::InvalidHandle =
#if defined CIO_HAVE_WINDOWS_H
		reinterpret_cast<std::uintptr_t>(INVALID_HANDLE_VALUE);
#else
		static_cast<std::uintptr_t>(-1);
#endif

	const int Program::InvalidId = -1;

	const char *Program::PathVariable = "PATH";

#if defined CIO_HAVE_WINDOWS_H
	const char *Program::FileExtension = "exe";

	const char Program::PathDelimiter = ';';
#else
	const char *Program::FileExtension = "";

	const char Program::PathDelimiter = ':';
#endif

	int Program::getApplicationId() noexcept
	{
#if defined CIO_HAVE_WINDOWS_H
		return GetCurrentProcessId();
#else
		return ::getpid();
#endif
	}

	Text Program::getSearchPaths()
	{
		return std::getenv(Program::PathVariable);
	}

	std::vector<Path> Program::getSearchPathList()
	{
		std::vector<Path> paths;
		Text delimitedList = Program::getSearchPaths();

		std::pair<Text, Text> split = delimitedList.split(Program::PathDelimiter);
		while (split.first)
		{
			paths.emplace_back(std::move(split.first));
			split = split.second.split(Program::PathDelimiter);
		}
		return paths;
	}

	bool Program::matchesPath(const Path &path) noexcept
	{
		return path.matchesExtension(Program::FileExtension);
	}

	Path Program::find(const Path &name)
	{
		Path found;
		State findable = Program::findable(name);
		if (findable.succeeded())
		{
			Text delimitedList = Program::getSearchPaths();
			if (delimitedList)
			{
				std::pair<Text, Text> split = delimitedList.split(Program::PathDelimiter);
				found = Program::findInDirectory(name, split.first);

				while (!found && split.second)
				{
					split = split.second.split(Program::PathDelimiter);
					found = Program::findInDirectory(name, split.first);
				}
			}
		}
		else if (findable.reason == Reason::Exists)
		{
			found = name;
		}
				
		return found;
	}

	Path Program::find(const Path &name, const Path &dir)
	{
		Path found;
		State findable = Program::findable(name);
		if (findable.succeeded())
		{
			found = Program::findInDirectory(name, dir);
		}
		else if (findable.reason == Reason::Exists)
		{
			found = name;
		}
		return found;
	}

	Path Program::find(const Path &name, const std::vector<Path> &search)
	{
		Path found;
		State findable = Program::findable(name);
		if (findable.succeeded())
		{
			for (const Path &directory : search)
			{
				found = Program::findInDirectory(name, directory);
				if (found)
				{
					break;
				}
			}
		}
		else if (findable.reason == Reason::Exists)
		{
			found = name;
		}
		return found;
	}

	Path Program::find(const Path &name, Text delimitedList)
	{
		Path found;
		State findable = Program::findable(name);
		if (findable.succeeded() && delimitedList)
		{
			std::pair<Text, Text> split = delimitedList.split(Program::PathDelimiter);
			found = Program::findInDirectory(name, split.first);

			while (!found && split.second)
			{
				split = split.second.split(Program::PathDelimiter);
				found = Program::findInDirectory(name, split.first);
			}
		}
		else if (findable.reason == Reason::Exists)
		{
			found = name;
		}
		return found;
	}

	State Program::findable(const Path &name)
	{
		State state;

		if (!name.hasProtocol() || name.isFileProtocol())
		{
			cio::Filesystem fs;
			if (name.isComplete())
			{
				Resource type = fs.readType(name);

				// Resolve links
				if (type == Resource::Link)
				{
					Path resolved = fs.readLink(name, Traversal::Recursive);
					type = fs.readType(resolved);
				}

				// If the resource is an absolute path to a file on disk, no search is needed
				if (type == Resource::File)
				{
					state.fail(Reason::Exists);
				}
				// If the resource is an absolute path to something that doesn't exist, no search can be done
				else if (type == Resource::None)
				{
					state.fail(Reason::Missing);
				}
				// If the resource is an absolute path to something that isn't a file, no search can be done
				else
				{
					state.fail(Reason::Unexecutable);
				}
			}
			// Relative path is searchable
			else
			{
				state.succeed();
			}
		}
		else
		{
			// Refuse to search anything other than local filesystem
			state.fail(Reason::Refused);
		}

		return state;
	}

	Path Program::findInDirectory(const Path &name, const Path &directory)
	{
		Path candidate(directory, name);

		// Set conventional program extension if not on original input
		if (!candidate.hasExtension() && *Program::FileExtension)
		{
			candidate.setExtension(FileExtension);
		}

		// Check resource type of candidate path
		Filesystem fs;
		Resource type = fs.readType(candidate);

		// If it is a link, resolve it
		if (type == Resource::Link)
		{
			candidate = fs.readLink(candidate, Traversal::Recursive);
			type = fs.readType(candidate);
		}

		// If it's not a file we can't use it
		if (type != Resource::File)
		{
			candidate.clear();
		}

		return candidate;
	}

	Program Program::launch(cio::Path executable)
	{
		Program program;
		program.setExecutable(std::move(executable));
		program.setWorkingDirectory();
		program.start();
		return program;
	}

	Program::Program() noexcept :
		mProcessHandle(Program::InvalidHandle),
		mProcessId(Program::InvalidId)
	{
		// nothing more to do
	}

	Program::Program(Path exe) :
		mExecutable(std::move(exe)),
		mProcessHandle(Program::InvalidHandle),
		mProcessId(Program::InvalidId)
	{
		// nothing more to do
	}

	Program::Program(const Program &in) :
		mExecutable(in.mExecutable),
		mWorkingDir(in.mWorkingDir),
		mArguments(in.mArguments),
		mProcessHandle(Program::InvalidHandle),
		mProcessId(Program::InvalidId)
	{
		// nothing more to do
	}

	Program::Program(Program &&in) noexcept :
		mExecutable(std::move(in.mExecutable)),
		mWorkingDir(std::move(in.mWorkingDir)),
		mArguments(std::move(in.mArguments)),
		mProcessHandle(in.mProcessHandle),
		mProcessId(in.mProcessId),
		mProgramInput(std::move(in.mProgramInput)),
		mProgramOutput(std::move(in.mProgramOutput))
	{
		in.mProcessHandle = Program::InvalidHandle;
		in.mProcessId = Program::InvalidId;
	}

	Program &Program::operator=(const Program &in)
	{
		if (this != &in)
		{
			this->clear();
			mExecutable = in.mExecutable;
			mWorkingDir = in.mWorkingDir;
			mArguments = in.mArguments;
		}
		return *this;
	}

	Program &Program::operator=(Program &&in) noexcept
	{
		if (this != &in)
		{
			this->clear();
			mExecutable = std::move(in.mExecutable);
			mWorkingDir = std::move(in.mWorkingDir);
			mArguments = std::move(in.mArguments);
			mProcessHandle = in.mProcessHandle;
			mProcessHandle = in.mProcessHandle;
			mProcessId = in.mProcessId;
			mProgramInput = std::move(in.mProgramInput);
			mProgramOutput = std::move(in.mProgramOutput);

			in.mProcessHandle = Program::InvalidHandle;
			in.mProcessId = Program::InvalidId;
		}
		return *this;
	}

	Program::~Program() noexcept
	{
		this->clear();
	}

	void Program::clear() noexcept
	{
		this->terminate();

		mProgramInput.reset();
		mProgramOutput.reset();

#if defined CIO_HAVE_WINDOWS_H
		if (mProcessHandle != Program::InvalidHandle)
		{
			::CloseHandle(reinterpret_cast<HANDLE>(mProcessHandle));
		}
#endif

		mProcessHandle = Program::InvalidHandle;
		mProcessId = Program::InvalidId;

		mExecutable.clear();
		mWorkingDir.clear();
		mArguments.clear();
	}

	std::uintptr_t Program::detach() noexcept
	{
		std::uintptr_t handle = mProcessHandle;
		mProcessHandle = Program::InvalidHandle;
		return handle;
	}

	void Program::start()
	{
		if (mProcessHandle != Program::InvalidHandle)
		{
			throw Exception(Action::Execute, Reason::Exists);
		}

		// Create pipes needed to handle I/O
		cio::Pipe stdinWriter;
		cio::Pipe stdinReader = stdinWriter.createAnonymousPipe();

		cio::Pipe stdoutWriter;
		cio::Pipe stdoutReader = stdoutWriter.createAnonymousPipe();

		cio::Text exec;
		cio::Path found;

		if (mExecutable.isComplete())
		{
			exec = mExecutable.toNativeFile();

			if (mWorkingDir && !mWorkingDir.isComplete())
			{
				mWorkingDir.complete(mExecutable.getParent());
			}
		}
		else
		{
			found = Program::find(mExecutable);
			if (found.empty())
			{
				throw Exception(Action::Execute, "Could not find executable in search path");
			}
			exec = found.toNativeFile();

			if (mWorkingDir && !mWorkingDir.isComplete())
			{
				mWorkingDir.complete(found.getParent());
			}
		}

#if defined CIO_HAVE_WINDOWS_H
		cio::Text command = this->makeWindowsCommandLine();
		cio::Text working;
		if (mWorkingDir)
		{
			working = mWorkingDir.toNativeFile();
		}

		DWORD createFlags = 0;

		STARTUPINFO startup = { };
		startup.cb = sizeof(STARTUPINFO);
		startup.dwFlags = STARTF_USESTDHANDLES;
		startup.hStdInput = reinterpret_cast<HANDLE>(stdinReader.getHandle());
		startup.hStdOutput = reinterpret_cast<HANDLE>(stdoutWriter.getHandle());
		startup.hStdError = reinterpret_cast<HANDLE>(stdoutWriter.getHandle());

		SetHandleInformation(startup.hStdInput, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT);
		SetHandleInformation(startup.hStdOutput, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT);

		PROCESS_INFORMATION process = { };
		
		DWORD status = ::CreateProcess(exec.c_str(), const_cast<char *>(command.c_str()), nullptr, nullptr, true,
			createFlags, nullptr, working.c_str(), &startup, &process);

		if (status == 0)
		{
			Reason r = cio::errorWin32();
			throw Exception(Action::Execute, r);
		}

		mProcessHandle = reinterpret_cast<std::uintptr_t>(process.hProcess);
		::CloseHandle(process.hThread); // we don't actually need main thread handle
		mProcessId = process.dwProcessId;
#else
		pid_t pid = ::fork();
		if (pid == -1)
		{
			Reason error = cio::error();
			throw Exception(Action::Execute, error);
		}
		else if (pid == 0) //child process
		{
			if (!mWorkingDir.empty())
			{
				cio::Filesystem fs;
				fs.setCurrentDirectory(mWorkingDir);
			}

			// Set up handles to the pipes
			::dup2(static_cast<int>(stdinReader.getHandle()), 0);
			::dup2(static_cast<int>(stdoutWriter.getHandle()), 1);
			::dup2(static_cast<int>(stdoutWriter.getHandle()), 2);

			// Build argv

			std::vector<char *> argv;
			argv.reserve(mArguments.size() + 2);

			argv.emplace_back(const_cast<char *>(exec.c_str()));
			for (cio::Text &arg : mArguments)
			{
				arg.nullTerminate();
				argv.emplace_back(const_cast<char *>(arg.c_str()));
			}
			argv.emplace_back(nullptr);
			
			argv.data();

			// execve does not return on success, the actual program starts executing instead
			// on failure the forked program is in an unusable state
			int status = ::execv(exec.c_str(), argv.data());
			if (status != 0)
			{
				// abort does not return
				std::abort();
			}
		}
		else // parent process
		{
			mProcessId = pid;
			mProcessHandle = static_cast<std::uintptr_t>(pid);
		}
#endif

		// If we make this far without throwing, we can adopt the relevant input/output
		mProgramInput.reset(new Pipe(std::move(stdinWriter)));
		mProgramOutput.reset(new Pipe(std::move(stdoutReader)));
	}

	State Program::wait(std::chrono::milliseconds timeout) const
	{
		State state;

		if (mProcessHandle != Program::InvalidHandle)
		{
#if defined CIO_HAVE_WINDOWS_H
			DWORD dwTimeout = timeout.count() >= INFINITE ? INFINITE : static_cast<DWORD>(timeout.count());
			DWORD result = ::WaitForSingleObject(reinterpret_cast<HANDLE>(mProcessHandle), dwTimeout);
			if (result == WAIT_FAILED)
			{
				state.fail(cio::errorWin32());
			}
			else if (result == WAIT_TIMEOUT)
			{
				state.complete(Reason::Timeout);
			}
			else
			{
				state.succeed();
			}
#else
			// POSIX does not provide an easy way to do this
			int status = 0;

			// If we have infinite timeout, we can use the normal method
			if (timeout == std::chrono::milliseconds::max())
			{
				::waitpid(mProcessId, &status, 0);
				state.succeed();
			}
			// Otherwise we need to do a polling loop
			else
			{
				::waitpid(mProcessId, &status, WNOHANG);
				if (WIFEXITED(status))
				{
					state.succeed();
				}
				else if (timeout.count() == 0)
				{
					state.complete(Reason::Timeout);
				}
				else
				{
					auto mark = std::chrono::steady_clock::now();
					while (!state.completed())
					{
						std::this_thread::sleep_for(std::chrono::milliseconds(10));

						::waitpid(mProcessId, &status, WNOHANG);
						if (WIFEXITED(status))
						{
							state.succeed();
						}
						else
						{
							auto next = std::chrono::steady_clock::now();
							if (next - mark >= timeout)
							{
								state.complete(Reason::Timeout);
							}
						}
					}
				}
			}
#endif
		}
		else
		{
			state.fail(Reason::Missing);
		}

		return state;
	}

	State Program::status() const noexcept
	{
		State state;
		if (mProcessHandle != Program::InvalidHandle)
		{
#if defined CIO_HAVE_WINDOWS_H
			DWORD code = -1;
			::GetExitCodeProcess(reinterpret_cast<HANDLE>(mProcessHandle), &code);
			if (code == 259) // returned if the process is still running
			{
				state.start();
			}
			else if (code == 0)
			{
				state.succeed();
			}
			else
			{
				state.fail();
			}
#else
			int status = 0;
			pid_t exited = ::waitpid(mProcessId, &status, WNOHANG);
			if (exited == 0)
			{
				state.start(); // still running
			}
			else if (exited == mProcessId)
			{
				if (WIFEXITED(status))
				{
					int code = WEXITSTATUS(status);
					if (code == 0)
					{
						state.succeed();
					}
					else
					{
						state.fail();
					}
				}
				else
				{
					// exited abnormally
					state.abandon();
				}
			}
#endif
		}

		return state;
	}

	int Program::getExitCode() const
	{
		int result = -1;

		if (mProcessHandle != Program::InvalidHandle)
		{
#if defined CIO_HAVE_WINDOWS_H
			DWORD code = -1;

			::GetExitCodeProcess(reinterpret_cast<HANDLE>(mProcessHandle), &code);
			result = code;
#else
			int status = 0;
			::waitpid(mProcessId, &status, WNOHANG);
			result = WEXITSTATUS(status);
#endif
		}
		return result;
	}

	Response<int> Program::waitForExitCode(std::chrono::milliseconds timeout)
	{
		Response<int> code;

		State state = this->wait(timeout);
		if (state.succeeded())
		{
			code.receive(this->getExitCode());
		}
		else
		{
			code.fail(state.action, state.reason);
		}
		return code;
	}

	void Program::terminate()
	{
		if (mProcessHandle != Program::InvalidHandle)
		{
#if defined CIO_HAVE_WINDOWS_H

			::TerminateProcess(reinterpret_cast<HANDLE>(mProcessHandle), -1);
#else
			::kill(mProcessId, SIGKILL);
#endif
		}
	}

	const cio::Path &Program::getExecutable() const noexcept
	{
		return mExecutable;
	}

	void Program::setExecutable(cio::Path path)
	{
		mExecutable = std::move(path);
	}

	const cio::Path &Program::getWorkingDirectory() const noexcept
	{
		return mWorkingDir;
	}

	void Program::setWorkingDirectory()
	{
		mWorkingDir = ".";
	}

	void Program::setWorkingDirectory(cio::Path path)
	{
		mWorkingDir = std::move(path);
	}

	const std::vector<cio::Text> &Program::getArguments() const noexcept
	{
		return mArguments;
	}

	void Program::setArguments(std::vector<cio::Text> args)
	{
		mArguments = std::move(args);
	}

	void Program::setArguments(int argc, const char **argv)
	{
		mArguments.resize(argc);
		for (int i = 0; i < argc; ++i)
		{
			mArguments[i] = argv[i];
		}
	}

	cio::Text Program::makeWindowsCommandLine() const
	{
		cio::Buffer buffer;

		cio::Text exec = mExecutable.toWindowsFile();
		if (exec.contains(' '))
		{
			buffer << '"' << exec << '"';
		}
		else
		{
			buffer << exec;
		}

		for (const cio::Text &arg : mArguments)
		{
			buffer << ' ';
			if (arg.contains(' '))
			{
				buffer << '"' << arg << '"';
			}
			else
			{ 
				buffer << arg;
			}
		}

		return buffer.viewProcessedText().duplicate();
	}

	cio::Text Program::makeShellCommandLine() const
	{
		cio::Buffer buffer;

		cio::Text exec = mExecutable.toUnixFile();
		auto split = exec.split(' ');
		while (!split.second.empty())
		{
			buffer << split.first;
			buffer << "\\ ";
			split = split.second.split(' ');
		}
		buffer << split.first;

		for (const cio::Text &arg : mArguments)
		{
			buffer << ' ';
			split = arg.split(' ');
			while (!split.second.empty())
			{
				buffer << split.first;
				buffer << "\\ ";
				split = split.second.split(' ');
			}
			buffer << split.first;
		}
		
		return buffer.viewProcessedText().duplicate();
	}

	cio::Text Program::makeNativeCommandLine() const
	{
#if defined _WIN32
		return makeWindowsCommandLine();
#else
		return makeShellCommandLine();
#endif
	}

	std::uintptr_t Program::getProcessHandle() const noexcept
	{
		return mProcessHandle;
	}

	int Program::getProcessId() const noexcept
	{
		return mProcessId;
	}

	cio::Input &Program::getProgramOutput() noexcept
	{
		return *mProgramOutput;
	}

	cio::Output &Program::getProgramInput() noexcept
	{
		return *mProgramInput;
	}

	Program::operator bool() const noexcept
	{
		return mProcessHandle != Program::InvalidHandle;
	}
}