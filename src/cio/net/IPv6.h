/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_IPV6_H
#define CIO_NET_IPV6_H

#include "Types.h"

#include <cio/Validation.h>

#include <iosfwd>
#include <string>

namespace cio
{
	namespace net
	{
		/**
		 * The IPv6 class provides a runtime representation of IPv6 addresses in native runtime byte order.
		 *
		 * @note The byte order is native for ease of runtime operations using integer math. On little-endian host machines, it will
		 * need to be convert to "network" (big-endian) byte order to be used in native Address buffers - that is, high and low must be swapped
		 * and individually byte swapped. Using this class wth the Address class will swap automatically as needed.
		 */
		class CIO_NET_API IPv6
		{
			public:
				/**
				 * Gets the IPv6 address that represents the "any available address" for servers.
				 *
				 * @return the IPv6 "any" address
				 */
				static IPv6 any() noexcept;

				/**
				 * Gets the IPv6 address for localhost.
				 *
				 * @return the IPv6 "localhost" address
				 */
				static IPv6 localhost() noexcept;
				
				/**
				 * Checks whether IPv6 supports the given flow type.
				 * This is true for all types except Broadcast.
				 *
				 * @param flow The flow type
				 * @return whether the flow type is supported by IPv6
				 */
				static bool supports(Flow flow) noexcept;
				
				/**
				 * Formats a single short 16-bit value using the rules of IPv6 printing.
				 * This prints up to four hexadecimal digits big endian but omits leading zero nibbles.
				 * If the buffer is not large enough or is null, the returned count is how many bytes would have been needed.
				 *
				 * @param value The 16-bit value
				 * @param text The text buffer to print to
				 * @param length The length of the text buffer
				 * @return the number of bytes needed to fully print the value
				 */
				static std::size_t printShort(std::uint16_t value, char *text, std::size_t length) noexcept;

				/**
				 * Constructs the default IPv6 address of all zero bytes.
				 * This also happens to be the "any" address.
				 */
				inline IPv6() noexcept;

				/**
				 * Constructs the IPv6 address from two sets of 8 byte integers in native byte order.
				 *
				 * @param high The high 8 bytes
				 * @param low The low 8 bytes
				 */
				inline IPv6(std::uint64_t high, std::uint64_t low) noexcept;

				/**
				 * Parses the given null-terminated text to extract an IPv6 address.
				 *
				 * @param text The text to parse
				 * @return the validation results of the parse
				 */
				TextParseStatus parse(const char *text) noexcept;

				/**
				 * Parses the given text to extract an IPv6 address.
				 *
				 * @param text The text to parse
				 * @param length The number of bytes in the parsed text
				 * @return the validation results of the parse
				 */
				TextParseStatus parse(const char *text, std::size_t length) noexcept;

				/**
				 * Parses the given null-terminated text to extract an IPv6 address.
				 *
				 * @param text The text to parse
				 * @return the validation results of the parse
				 */
				TextParseStatus parse(const std::string &text) noexcept;

				/**
				 * Clears the IPv6 and restores it to all zero bytes.
				 * This also happens to be the "any" address.
				 */
				inline void clear() noexcept;

				/**
				 * Directly sets the IPv6 address from two sets of 8 byte integers in native byte order.
				 *
				 * @param high The high 8 bytes
				 * @param low The low 8 bytes
				 */
				inline void set(std::uint64_t high, std::uint64_t low) noexcept;

				/**
				 * Gets the high 8 bytes of the IP address.
				 *
				 * @return the high 8 bytes
				 */
				inline std::uint64_t high() const noexcept;

				/**
				 * Gets the low 8 bytes of the IP address.
				 *
				 * @return the low 8 bytes
				 */
				inline std::uint64_t low() const noexcept;

				/**
				 * Interprets the IPv6 in a Boolean context.
				 * It is considered true if the address is not all 8 bytes (the "any" address).
				 *
				 * @return whether the address is considered true
				 */
				inline explicit operator bool() const noexcept;

				/**
				 * Prints the IPv6 address to the given text buffer.
				 * The shortest form of the address will be printed, omitting leading zeroes and the first sequence of zero sections.
				 * If the text buffer is longer than the actual printed address, the remainder of the buffer is filled with null terminators.
				 *
				 * @param text The text buffer to print to
				 * @param length The length of the text buffer
				 * @return the number of bytes actually needed to print the address
				 */
				std::size_t print(char *text, std::size_t length) const noexcept;

				/**
				 * Prints the IPv6 address to a returned string.
				 * The shortest form of the address will be printed, omitting leading zeroes and the first sequence of zero sections.
				 *
				 * @return the printed address
				 * @throw std::bad_alloc If not enough memory could be allocated for the string
				 */
				std::string print() const;

			private:
				/** High 8 bytes of the address */
				std::uint64_t mHigh;

				/** Low 8 bytes of the address */
				std::uint64_t mLow;
		};

		/**
		 * Prints the IPv6 address to a C++ stream.
		 * The shortest form of the address will be printed, omitting leading zeroes and the first sequence of zero sections.
		 *
		 * @param stream The stream to print to
		 * @param ip The IPv6 address
		 * @return the stream
		 */
		CIO_API std::ostream &operator<<(std::ostream &stream, const IPv6 &ip);

		/**
		 * Checks to see if two IPv6 addresses are equal.
		 * This is true if they have exactly the same byte sequence.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether they are equal
		 */
		inline bool operator==(const IPv6 &left, const IPv6 &right);

		/**
		 * Checks to see if two IPv6 addresses are not equal.
		 * This is true if they have different byte sequences.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether they are not equal
		 */
		inline bool operator!=(const IPv6 &left, const IPv6 &right);

		/**
		 * Checks to see if an IPv6 address is less than another IPv6 address.
		 * This does an ordered comparison on the high bytes first, then the low bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is less than the second address
		 */
		inline bool operator<(const IPv6 &left, const IPv6 &right);

		/**
		 * Checks to see if an IPv6 address is less than or equal to another IPv6 address.
		 * This does an ordered comparison on the high bytes first, then the low bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is less than or equal to the second address
		 */
		inline bool operator<=(const IPv6 &left, const IPv6 &right);

		/**
		 * Checks to see if an IPv6 address is greater than another IPv6 address.
		 * This does an ordered comparison on the high bytes first, then the low bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is greater than the second address
		 */
		inline bool operator>(const IPv6 &left, const IPv6 &right);

		/**
		 * Checks to see if an IPv6 address is greater than or equal to another IPv6 address.
		 * This does an ordered comparison on the high bytes first, then the low bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is greater than or equal to the second address
		 */
		inline bool operator>=(const IPv6 &left, const IPv6 &right);
	}
}

/* Inline implementation */

namespace cio
{
	namespace net
	{
inline IPv6::IPv6() noexcept :
		mHigh(0),
			  mLow(0)
		{
			// nothing more to do
		}

inline IPv6::IPv6(std::uint64_t high, std::uint64_t low) noexcept :
		mHigh(high),
		mLow(low)
		{
			// nothing more to do
		}

		inline void IPv6::clear() noexcept
		{
			mHigh = 0;
			mLow = 0;
		}

		inline void IPv6::set(std::uint64_t high, std::uint64_t low) noexcept
		{
			mHigh = high;
			mLow = low;
		}

		inline std::uint64_t IPv6::high() const noexcept
		{
			return mHigh;
		}

		inline std::uint64_t IPv6::low() const noexcept
		{
			return mLow;
		}

		inline IPv6::operator bool() const noexcept
		{
			return mHigh != 0 || mLow != 0;
		}

		inline bool operator==(const IPv6 &left, const IPv6 &right)
		{
			return left.high() == right.high() && left.low() == right.low();
		}

		inline bool operator!=(const IPv6 &left, const IPv6 &right)
		{
			return left.high() != right.high() || left.low() != right.low();
		}

		inline bool operator<(const IPv6 &left, const IPv6 &right)
		{
			return left.high() < right.high() || (left.high() == right.high() && left.low() < right.low());
		}

		inline bool operator<=(const IPv6 &left, const IPv6 &right)
		{
			return left.high() < right.high() || (left.high() == right.high() && left.low() <= right.low());
		}

		inline bool operator>(const IPv6 &left, const IPv6 &right)
		{
			return left.high() > right.high() || (left.high() == right.high() && left.low() > right.low());
		}

		inline bool operator>=(const IPv6 &left, const IPv6 &right)
		{
			return left.high() > right.high() || (left.high() == right.high() && left.low() >= right.low());
		}
	}
}

#endif
