/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "PathFilter.h"

#include "Path.h"
#include "Text.h"

namespace cio
{
	std::regex PathFilter::createPathRegex(const std::string &filters)
	{
		return std::regex(PathFilter::createPathRegexText(filters), std::regex::optimize);
	}

	std::string PathFilter::createPathRegexText(const std::string &filters)
	{
		std::string text;
		std::string token;

		if (filters.empty())
		{
			// Special case - only match things without extensions
			text = "(.*/)?.+?.[^\\.]+$";
		}
		else for (char c : filters)
		{
			switch (c)
			{
			case ' ':
			{
				if (!text.empty())
				{
					text.push_back('|');
				}

				text.push_back('(');
				text.append(token);
				text.append(")");
				token.clear();
				break;
			}

			case '*':
			{
				token.append(".*");
				break;
			}

			case '.':
			{
				token.append("\\.");
				break;
			}

			default:
			{
				token.push_back(c);
				break;
			}
			}
		}

		if (!token.empty())
		{
			if (!text.empty())
			{
				text.push_back('|');
			}

			text.push_back('(');
			text.append(token);
			text.append(")");
		}

		return text;
	}

	PathFilter::PathFilter() noexcept
	{
		// nothing to do
	}

	PathFilter::PathFilter(const char *filters) :
		mFilters(filters),
		mFilterRegex(PathFilter::createPathRegex(mFilters))
	{
		// nothing to do
	}

	PathFilter::PathFilter(std::string filters) :
		mFilters(std::move(filters)),
		mFilterRegex(PathFilter::createPathRegex(mFilters))
	{
		// nothing to do
	}

	PathFilter::PathFilter(const PathFilter &in) = default;

	PathFilter::PathFilter(PathFilter &&in) noexcept = default;

	PathFilter &PathFilter::operator=(const PathFilter &in) = default;

	PathFilter &PathFilter::operator=(PathFilter &&in) noexcept = default;

	PathFilter::~PathFilter() noexcept = default;
	
	void PathFilter::clear() noexcept
	{
		mFilters.clear();
		mFilterRegex = std::regex();
	}

	const std::string &PathFilter::getFilters() const noexcept
	{
		return mFilters;
	}

	void PathFilter::setFilters(std::string filters)
	{
		mFilters = std::move(filters);
		mFilterRegex = PathFilter::createPathRegex(mFilters);
	}

	const std::regex &PathFilter::getFilterRegex() const noexcept
	{
		return mFilterRegex;
	}

	std::string PathFilter::createNameFilterText(const std::string &name) const
	{
		std::string nameFilter;
		if (!mFilters.empty())
		{
			nameFilter = name + " (" + mFilters + ")";
		}
		else
		{
			nameFilter = name;
		}
		return nameFilter;
	}

	std::string PathFilter::getDefaultFilter() const noexcept
	{
		return cio::Text::view(mFilters).splitBefore(' ');
	}

	cio::Path PathFilter::createDefaultPath(const cio::Path &parent, std::string name) const
	{
		std::string pattern = this->getDefaultFilter();
		std::pair<std::string, std::string> split = cio::Text::view(pattern).split('*');

		std::string filename(split.first + name + split.second);
		return cio::Path(parent, filename);
	}

	bool PathFilter::operator()(const cio::Path &path) const noexcept
	{
		return !mFilters.empty() ? std::regex_match(path.get(), mFilterRegex) : !path.hasExtension();
	}

	bool PathFilter::matches(const cio::Path &path) const noexcept
	{
		return !mFilters.empty() ? std::regex_match(path.get(), mFilterRegex) : !path.hasExtension();
	}

	const std::string &PathFilter::str() const noexcept
	{
		return mFilters;
	}

	PathFilter::operator bool() const noexcept
	{
		return !mFilters.empty();
	}
}
