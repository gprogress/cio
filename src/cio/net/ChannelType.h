/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_CHANNELTYPE_H
#define CIO_NET_CHANNELTYPE_H

#include "Types.h"

#include <cio/Parse.h>

#include <iosfwd>
#include <string>

namespace cio
{
	namespace net
	{
		/**
		* The CIO Net ChannelType enumeration describes the overall types of network connections available
		* from the perspective of stream vs. packet, reliable vs. unreliable, and ordered vs. unordered.
		*
		* This primarily is to encapsulate platform-level compile time differences in these definitions
		* since all of these types map to low level codes in the native socket library.
		*/
		enum class ChannelType : std::uint8_t
		{
			/** No network type */
			None,

			/** Reliable two-way stream based connection (e.g. TCP) */
			Stream,

			/** Unreliable datagram packet delivery (e.g. UDP) */
			Datagram,

			/** Reliable and ordered datagram delivery */
			Sequential,

			/** Low-level raw socket datagram delivery */
			Raw,

			/** Reliable but unordered datagram packet delivery */
			Reliable,

			/** A channel type that CIO does not recognize but may still be usable */
			Unknown
		};

		/**
		* Gets the native integer code for the given socket type as defined by the native socket library.
		* "None" and "Unknown" will return a value (typically 0) that is not a valid socket type in the native platform.
		*
		* @param type The socket type
		* @return the native integer code
		*/
		CIO_NET_API int toNativeSocketType(ChannelType type) noexcept;

		/**
		* Gets the socket type from the native integer code as defined by the native socket library.
		* If the code is the invalid native code (typically 0) then None will be returned.
		* If the code is unrecognzied, then Unknown will be returned.
		*
		* @param code The native integer code
		* @return the socket type
		*/
		CIO_NET_API ChannelType fromNativeSocketType(int code) noexcept;

		/**
		* Gets a text string representing the socket type.
		*
		* @param mode The socket type
		* @return the text string
		*/
		CIO_NET_API const char *print(ChannelType type) noexcept;

		/**
		* Parses the text string to get the type specified by it.
		*
		* @param text The text value
		* @return the parse results for the type
		*/
		CIO_NET_API  Parse<ChannelType> parseChannelType(const char *text) noexcept;

		/**
		* Parses the text string to get the type specified by it.
		*
		* @param text The text value
		* @param len The number of characters in the text
		* @return the parse results for the type
		*/
		CIO_NET_API  Parse<ChannelType> parseChannelType(const char *text, std::size_t len) noexcept;

		/**
		* Parses the text string to get the type specified by it.
		*
		* @param text The text value
		* @return the parse results for the type
		*/
		CIO_NET_API  Parse<ChannelType> parseChannelType(const std::string &text) noexcept;

		/**
		* Prints the type to a C++ stream.
		*
		* @param stream The stream
		* @param type The type
		* @return the stream
		*/
		CIO_NET_API  std::ostream &operator<<(std::ostream &stream, ChannelType type);

		/**
		* Parses the type from a C++ stream.
		*
		* @param stream The stream
		* @param type The type
		* @return the stream
		*/
		CIO_NET_API  std::istream &operator>>(std::istream &stream, ChannelType &type);
	}
}

#endif

