#ifndef CIO_BLUETOOTH_SUBSCRIPTIONS_H
#define CIO_BLUETOOTH_SUBSCRIPTIONS_H

#include "Types.h"

#include <chrono>
#include <map>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Subscriptions class is a utility class for managing a map of attribute subscriptions.
		 * The same attribute may share many subscriptions with different listeners and poll intervals.
		 * It is primarily used by Attribute Client to mediate user subscriptions with GATT operations.
		 * The class is purely a tracking mechanism and does not actually perform any GATT queries itself.
		 */
		class CIO_BLUETOOTH_API Subscriptions
		{
			public:
				/**
				 * Creates an empty subscriptions map
				 */
				Subscriptions() noexcept;
				
				/**
				 * Copy construct a subscriptions map.
				 * 
				 * @param in The subscriptions to copy
				 */
				Subscriptions(const Subscriptions &in);

				/**
				 * Move construct a subscriptions map.
				 *
				 * @param in The subscriptions to copy
				 */
				Subscriptions(Subscriptions &&in) noexcept;
				
				/**
				 * Copy assignment.
				 * 
				 * @param in The subscriptions to copy
				 * @return this subscriptions map
				 */
				Subscriptions &operator=(const Subscriptions &in);

				/**
				 * Move assignment.
				 *
				 * @param in The subscriptions to move
				 * @return this subscriptions map
				 */
				Subscriptions &operator=(Subscriptions &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				~Subscriptions() noexcept;
				
				/**
				 * Clears the subscriptions map.
				 */
				void clear() noexcept;
			
				/**
				 * Registers a new subscription to the given attribute handle.
				 * If any existing subscriptions for the same attribute are present, the new subscription will have
				 * the same receipt modes as the current ones and the returned State will be Exists.
				 * If this is the first subscription to the attribute, the returned satus will be Success.
				 * 
				 * The returned pointer is stable and will not be invalidated until the subscription is unsubscribed.
				 * The returned subscription parameters such as the listener callback, receipt modes, and poll interval can be modified.
				 * The value handle should not be modified since that will leave the subscriptions map inconsistent.
				 * 
				 * @param handle The attribute handle
				 * @return the subscription and whether it is the first subscription for the attribute (Success) or not (Exists)
				 */
				Response<Subscription *> subscribe(std::uint16_t handle);

				/**
				 * Registers a new subscription to the given attribute handle with subscription parameters.
				 * If any existing subscriptions for the same attribute are present, the new subscription will have
				 * the same receipt modes as the current ones and the returned State will be Exists.
				 * If this is the first subscription to the attribute, the returned satus will be Success.
				 *
				 * The returned pointer is stable and will not be invalidated until the subscription is unsubscribed.
				 * The returned subscription parameters such as listener callback, receipt modes, and poll interval can be modified.
				 * The value handle should not be modified since that will leave the subscriptions map inconsistent.
				 *
				 * @param handle The attribute handle
				 * @param listener The listener callback to receive subscription updates
				 * @param interval The minimum polling interval for subscription updates
				 * @return the subscription and whether it is the first subscription for the attribute (Success) or not (Exists)
				 */
				Response<Subscription *> subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval);
				
				/**
				 * Notifies all suscriptions to the given attribute that new data is available.
				 * 
				 * @param handle The attribute handle
				 * @param data The received data
				 * @param length The number of bytes in the received data
				 * @return whether no subscriptions were found (Missing) or subscriptions were updated (Success)
				 */
				State notify(std::uint16_t handle, const void *data, std::size_t length) noexcept;
				
				/**
				 * Unsubscribes the specific subscription given its stable pointer.
				 * This only removes the subcription from this map, and it is the caller's responsibility to perform the GATT
				 * operations to notify the peripheral device.
				 * 
				 * @param subscription The subscription to unsubscribe
				 * @return whether the subscription was the last subscription for the attribute (Success), more subscriptions remain (Exists),
				 * or the subscription was not found (Missing)
				 */
				State unsubscribe(const Subscription *subscription);
				
				/**
				 * Unsubscribes all subscriptions to the given attribute handle.
				 * This only removes the subcriptions from this map, and it is the caller's responsibility to perform the GATT
				 * operations to notify the peripheral device.
				 * 
				 * @param handle The attribute handle to unsubscribe all subscriptions
				 * @return the number of removed subscriptions and whether any were found (Success) or not (Missing)
				 */
				Progress<std::size_t> unsubscribe(std::uint16_t handle);

				/**
				 * Unsubscribes all subscriptions to the attribute handles.
				 * This only removes the subcriptions from this map, and it is the caller's responsibility to perform the GATT
				 * operations to notify the peripheral device.
				 *
				 * @return the number of removed subscriptions and whether any were found (Success) or not (Missing)
				 */
				Progress<std::size_t> unsubscribe() noexcept;
				
				/**
				 * Gets the first subscription to the given attribute handle, if any.
				 * This is primarily used to iteratively unsubscribe in combination with unsubscribe(std::uint16_t handle).
				 * 
				 * @param handle The attribute handle
				 * @return the first subscription to the given attribute, or nullptr if none exist
				 */
				Subscription *top(std::uint16_t handle) noexcept;

				/**
				 * Gets the first subscription to the any attribute handle, if any are present.
				 * This is primarily used to iteratively unsubscribe in combination with unsubscribe(std::uint16_t handle).
				 *
				 * @return the first subscription to any attribute, or nullptr if none exist
				 */
				Subscription *top() noexcept;
				
				/**
				 * Gets the next scheduled subscription to be updated, if any need to be updated within the given timeout.
				 * If no subscriptions exist or none require updates in the given timeout, nullptr is returned.
				 * 
				 * @param timeout The maximum timeout
				 * @return the next scheduled subscription to be updated
				 */
				Subscription *schedule(std::chrono::milliseconds timeout);
				
			private:
				/** The map of attribute handles to subscriptions */
				std::multimap<std::uint16_t, Subscription> mSubscriptions;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

