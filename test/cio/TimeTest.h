/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TIMETEST_H
#define CIO_TIMETEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the cio::Time class.
	 */
	class TimeTest : public cio::test::Suite
	{
		public:
		/**
		 * Constructor.
		 */
		TimeTest();

		/**
		 * Destructor.
		 */
		virtual ~TimeTest() noexcept override;

		/**
		 * Tests operator<<(std::ostream &, const Time &)
		 */
		void testPrintStream();

		/**
		 * Tests setting a specific date.
		 */
		void testSetDate();
	};
}

#endif

