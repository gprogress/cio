/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TEMPORARYFILETEST_H
#define CIO_TEMPORARYFILETEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	* The Temporary File Test class tests the functionality of the cio::TemporaryFile class.
	*/
	class TemporaryFileTest : public cio::test::Suite
	{
		public:
			/**
			* Construct a temporary file.
			*/
			TemporaryFileTest();

			/**
			* Destructor.
			*/
			virtual ~TemporaryFileTest() noexcept override;

			/**
			* Test creating temporary files and copying to
			* a permanant location.
			*/
			void createTempFileTest();
	};
}

#endif