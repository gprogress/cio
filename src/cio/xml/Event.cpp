/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Event.h"

namespace cio
{
	namespace xml
	{
		Event::Event() :
			mType(Type::None),
			mEventType(EventType::None)
		{
			// nothing more to do
		}

		Event::Event(Type dataType, EventType eventType) :
			mType(dataType),
			mEventType(eventType)
		{
			// nothing more to do
		}

		Event::Event(std::string value, Type dataType, EventType eventType) :
			mValue(std::move(value)),
			mType(dataType),
			mEventType(eventType)
		{
			// nothing more to do
		}

		Event::Event(Text value, Type dataType, EventType eventType) :
			mValue(value.str()),
			mType(dataType),
			mEventType(eventType)
		{
			// nothing more to do
		}

		Event::Event(const Event &in) = default;

Event::Event(Event &&in) noexcept :
		mValue(std::move(in.mValue)),
			   mType(in.mType),
			   mEventType(in.mEventType)
		{
			// nothing to do
		}

		Event &Event::operator=(const Event &in) = default;

		Event &Event::operator=(Event &&in) noexcept
		{
			if (this != &in)
			{
				mValue = std::move(in.mValue);
				mType = in.mType;
				mEventType = in.mEventType;
			}

			return *this;
		}

		Event::~Event() = default;

		void Event::appendValue(Text value)
		{
			mValue.insert(mValue.end(), value.begin(), value.end());
		}

		std::string Event::takeValue()
		{
			return std::move(mValue);
		}

		void Event::setValue(std::string value)
		{
			mValue = std::move(value);
		}
	}
}
