/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Logger.h"

#include "Class.h"
#include "Length.h"
#include "Print.h"
#include "Progress.h"
#include "Text.h"

#include <iostream>

namespace cio
{
	const Class<Logger> Logger::sMetaclass("cio::Logger");
	
	Logger Logger::sDefaultLogger("Global");

	Logger *Logger::sGlobal = &sDefaultLogger;

	const Metaclass &Logger::getDeclaredMetaclass()
	{
		return sMetaclass;
	}

	Logger *Logger::getGlobal()
	{
		return sGlobal;
	}

	void Logger::setGlobal(Logger *logger)
	{
		sGlobal = logger;
	}

	Logger::Logger() :
		mContext("Global"),
		mThreshold(Logger::Threshold::value)
	{
		// nothing more to do
	}

	Logger::Logger(const char *context) :
		mContext(context),
		mThreshold(Logger::Threshold::value)
	{
		// nothing more to do
	}
	
	Logger::Logger(const char *context, Severity severity) :
		mContext(context),
		mThreshold(severity)
	{
		// nothing more to do
	}
	
	Logger::Logger(const Metaclass &metaclass) :
		mContext(metaclass.getQualifiedName().c_str()),
		mThreshold(Logger::Threshold::value)
	{
		// nothing more to do
	}
	
	Logger::Logger(const Metaclass &metaclass, Severity severity) :
		mContext(metaclass.getQualifiedName().c_str()),
		mThreshold(severity)
	{
		// nothing more to do
	}

	Logger::~Logger() noexcept
	{
		// nothing more to do
	}

	void Logger::clear() noexcept
	{
		mContext = nullptr;
		mThreshold = Logger::Threshold::value;
	}

	const Metaclass &Logger::getMetaclass() const noexcept
	{
		return sMetaclass;
	}
	
	void Logger::setContext(const Metaclass &metaclass)
	{
		mContext = metaclass.getQualifiedName().c_str();
	}

	bool Logger::log(const char *context, Severity severity, const char *message)
	{
		std::clog << '[' << context << " - " << severity << "] " << message << std::endl;
		return true;
	}
	
	bool Logger::reportProgressUpdate(const char *context, Severity severity, const Progress<Length> &update)
	{
		char buffer[512] = "Progress ";
		std::size_t current = 9;
		
		current += update.count.print(buffer + current, 512 - current);
		
		if (update.total > 0 && update.total.known())
		{
			buffer[current++] = ' ';
			buffer[current++] = '/';
			buffer[current++] = ' ';
			
			current += update.total.print(buffer + current, 512 - current);
			
			std::uint64_t percent = std::min<std::uint64_t>(100u, (update.count.value * 100u) / update.total.value);
			buffer[current++] = ' ';
			current += cio::print(percent, buffer + current, 512 - current);
			buffer[current++] = '%';
		}
		
		buffer[current++] = 0;
		return this->log(context, severity, buffer);
	}
			
	bool Logger::reportTimeUpdate(const char *context, Severity severity, const Progress<std::chrono::milliseconds> &update)
	{
		char buffer[512] = "Elapsed ";
		std::size_t current = 8;
		
		// TODO pretty print time
		
		current += cio::print(update.count.count(), buffer + current, 512 - current); 
		
		if (update.total.count() > 0 && update.total < std::chrono::milliseconds::max())
		{
			buffer[current++] = ' ';
			buffer[current++] = '/';
			buffer[current++] = ' ';
			
			current += cio::print(update.total.count(), buffer + current, 512 - current);
			
			std::uint64_t percent = std::min<std::uint64_t>(100u, (update.count.count() * 100u) / update.total.count());
			buffer[current++] = ' ';
			current += cio::print(percent, buffer + current, 512 - current);
			buffer[current++] = '%';
		}
		
		buffer[current++] = 0;
		return this->log(context, severity, buffer);
	}
}
