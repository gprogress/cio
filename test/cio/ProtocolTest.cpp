/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ProtocolTest.h"

#include <cio/test/Assert.h>

#include <cio/Input.h>
#include <cio/Metadata.h>
#include <cio/ModeSet.h>
#include <cio/Protocol.h>
#include <cio/State.h>
#include <cio/Status.h>
#include <cio/Text.h>
#include <cio/Resource.h>

namespace cio
{
	ProtocolTest::ProtocolTest() :
		test::Suite("cio/Protocol")
	{
		CIO_TEST_METHOD(ProtocolTest, open);
		CIO_TEST_METHOD(ProtocolTest, next);
		CIO_TEST_METHOD(ProtocolTest, filename);
		CIO_TEST_METHOD(ProtocolTest, current);
		CIO_TEST_METHOD(ProtocolTest, traversable);
		CIO_TEST_METHOD(ProtocolTest, openCurrentInput);
		CIO_TEST_METHOD(ProtocolTest, openInput);
		CIO_TEST_METHOD(ProtocolTest, checkForSpecialDirectories);
		CIO_TEST_METHOD(ProtocolTest, checkForSpecialFilename);
		CIO_TEST_METHOD(ProtocolTest, special);
		CIO_TEST_METHOD(ProtocolTest, clear);
	}

	ProtocolTest::~ProtocolTest() noexcept = default;

	void ProtocolTest::openTest()
	{
		Protocol dir;
		CIO_ASSERT_EQUAL(ModeSet(), dir.open(Path()));
		CIO_ASSERT_EQUAL(ModeSet(), dir.open("gobbledegook"));
		CIO_ASSERT_EQUAL(ModeSet(), dir.open("file://valid/path/maybe"));
	}
	
	void ProtocolTest::currentTest()
	{
		Protocol dir;
		Metadata expected;

		CIO_ASSERT_EQUAL(expected, dir.current());

		dir.open("file://valid/path/maybe");

		CIO_ASSERT_EQUAL(expected, dir.current());

		dir.next();

		CIO_ASSERT_EQUAL(expected, dir.current());

		dir.clear();

		CIO_ASSERT_EQUAL(expected, dir.current());
	}


	void ProtocolTest::filenameTest()
	{
		Protocol dir;

		CIO_ASSERT_EQUAL("", dir.filename());

		dir.open("file://valid/path/maybe");

		CIO_ASSERT_EQUAL("", dir.filename());

		dir.next();

		CIO_ASSERT_EQUAL("", dir.filename());

		dir.clear();

		CIO_ASSERT_EQUAL("", dir.filename());
	}

	void ProtocolTest::nextTest()
	{
		State expected = fail(Action::Discover, Reason::Underflow);
	
		Protocol dir;
		CIO_ASSERT_EQUAL(expected, dir.next());
		CIO_ASSERT_EQUAL(expected, dir.next());

		dir.open("file://valid/path/maybe");

		CIO_ASSERT_EQUAL(expected, dir.next());
		CIO_ASSERT_EQUAL(expected, dir.next());

		dir.clear();

		CIO_ASSERT_EQUAL(expected, dir.next());
		CIO_ASSERT_EQUAL(expected, dir.next());
	}

	void ProtocolTest::traversableTest()
	{
		Protocol dir;

		CIO_ASSERT_EQUAL(false, dir.traversable());

		dir.open("foo");
		CIO_ASSERT_EQUAL(false, dir.traversable());

		dir.clear();
		CIO_ASSERT_EQUAL(false, dir.traversable());
	}

	void ProtocolTest::openCurrentInputTest()
	{
		Protocol dir;
		std::unique_ptr<Input> actual;
		
		actual = dir.openCurrentInput();
		CIO_ASSERT(!actual);

		dir.open("file://valid/path/maybe");

		actual = dir.openCurrentInput();
		CIO_ASSERT(!actual);

		dir.next();

		actual = dir.openCurrentInput();
		CIO_ASSERT(!actual);

		dir.clear();

		actual = dir.openCurrentInput();
		CIO_ASSERT(!actual);
	}
	
	void ProtocolTest::openInputTest()
	{
		Protocol dir;
		std::unique_ptr<Input> actual;
		
		actual = dir.openInput("");
		CIO_ASSERT(!actual);
		
		actual = dir.openInput("relative");
		CIO_ASSERT(!actual);

		dir.open("file://valid/path/maybe");

		actual = dir.openInput("");
		CIO_ASSERT(!actual);

		actual = dir.openInput("relative");
		CIO_ASSERT(!actual);

		dir.next();

		actual = dir.openInput("");
		CIO_ASSERT(!actual);

		actual = dir.openInput("relative");
		CIO_ASSERT(!actual);

		dir.clear();

		actual = dir.openInput("");
		CIO_ASSERT(!actual);

		actual = dir.openInput("relative");
		CIO_ASSERT(!actual);
	}
	
	void ProtocolTest::checkForSpecialDirectoriesTest()
	{
		CIO_ASSERT_EQUAL(Resource::None, Protocol::checkForSpecialDirectories(nullptr));
		CIO_ASSERT_EQUAL(Resource::None, Protocol::checkForSpecialDirectories(""));
		CIO_ASSERT_EQUAL(Resource::Directory, Protocol::checkForSpecialDirectories("."));
		CIO_ASSERT_EQUAL(Resource::Directory, Protocol::checkForSpecialDirectories(".."));

		CIO_ASSERT_EQUAL(Resource::None, Protocol::checkForSpecialDirectories("..."));
		CIO_ASSERT_EQUAL(Resource::None, Protocol::checkForSpecialDirectories("foo"));
		CIO_ASSERT_EQUAL(Resource::None, Protocol::checkForSpecialDirectories("PRN"));
	}

	void ProtocolTest::checkForSpecialFilenameTest()
	{
		Protocol dir;

		CIO_ASSERT_EQUAL(Resource::None, dir.checkForSpecialFilename(nullptr));
		CIO_ASSERT_EQUAL(Resource::None, dir.checkForSpecialFilename(""));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.checkForSpecialFilename("."));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.checkForSpecialFilename(".."));

		CIO_ASSERT_EQUAL(Resource::None, dir.checkForSpecialFilename("..."));
		CIO_ASSERT_EQUAL(Resource::None, dir.checkForSpecialFilename("foo"));
		CIO_ASSERT_EQUAL(Resource::None, dir.checkForSpecialFilename("PRN"));
	}

	void ProtocolTest::specialTest()
	{
		Protocol dir;

		// Check first set of overloads for null-terminated string
		CIO_ASSERT_EQUAL(Resource::None, dir.special(nullptr));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(""));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special("."));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special(".."));
		CIO_ASSERT_EQUAL(Resource::None, dir.special("..."));
		CIO_ASSERT_EQUAL(Resource::None, dir.special("foo"));
		CIO_ASSERT_EQUAL(Resource::None, dir.special("PRN"));

		// Check second set of overloads for std::string
		CIO_ASSERT_EQUAL(Resource::None, dir.special(std::string()));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special(std::string(".")));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special(std::string("..")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(std::string("...")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(std::string("foo")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(std::string("PRN")));

		// Check third set of overloads for Text
		CIO_ASSERT_EQUAL(Resource::None, dir.special(Text()));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special(Text(".")));
		CIO_ASSERT_EQUAL(Resource::Directory, dir.special(Text("..")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(Text("...")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(Text("foo")));
		CIO_ASSERT_EQUAL(Resource::None, dir.special(Text("PRN")));
	}

	void ProtocolTest::clearTest()
	{
		Protocol dir;

		dir.clear();
		dir.clear();

		dir.open("file://valid/path/maybe");

		dir.clear();
		dir.clear();
	}
}

