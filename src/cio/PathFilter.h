/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PATHFILTER_H
#define CIO_PATHFILTER_H

#include "Types.h"

#include <regex>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Path Filter class provides a method to match paths in terms of file extensions,
	 * protocols, and other text patterns in a rigorous way. Patterns are typically expressed
	 * as a space-delimited set of match patterns, typically file extensions encoded using wildcard '*' prefixes.
	 * For example, an image match pattern might look like 
	 *
	 * It is designed to be compatible with Qt path filters as used in the Qt widget system, especially QFileDialog, but is generally
	 * useful outside of GUI contexts.
	 */
	class CIO_API PathFilter
	{
		public:
			/**
			 * Creates a regular expression that can match the given path filters.
			 *
			 * @param filters The path filter text
			 * @return The regular expression for the current path filters
			 */
			static std::regex createPathRegex(const std::string &filters);
			
			/**
			 * Creates a regular expression text that can match the current path filters.
			 *
			 * @param filters The path filter text
			 * @return The regular expression text for the current path filters
			 */
			static std::string createPathRegexText(const std::string &filters);

			/**
			 * Default constructor.
			 */
			PathFilter() noexcept;

			/**
			 * Construct a path filter with the specified filter text pattern.
			 *
			 * @param filterText The filter text pattern
			 */
			PathFilter(const char *filterText);

			/**
			 * Construct a path filter with the specified filter text pattern.
			 * 
			 * @param filterText The filter text pattern
			 */
			PathFilter(std::string filterText);

			/**
			 * Copy constructor.
			 * 
			 * @param in The path filter to copy
			 */
			PathFilter(const PathFilter &in);

			/**
			 * Move constructor.
			 *
			 * @param in The path filter to move
			 */
			PathFilter(PathFilter &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The path filter to copy
			 * @return This path filter
			 */
			PathFilter &operator=(const PathFilter &in);

			/**
			 * Move assignment.
			 *
			 * @param in The path filter to move
			 * @return This path filter
			 */
			PathFilter &operator=(PathFilter &&in) noexcept;

			/**
			 * Destructor.
			 */
			~PathFilter() noexcept;

			/**
			 * Clears the path filter.
			 */
			void clear() noexcept;

			/**
			 * Gets the path filters specified as a space-delimited text string of matches.
			 * 
			 * @return The filters as specified by space-delimited text
			 */
			const std::string &getFilters() const noexcept;

			/**
			 * Sets the path filters by specifying a space-delimited text string of matches.
			 * The filters will also be compiled into an optimized regular expression matcher.
			 *
			 * @param filterText The filters as specified by space-delimited text
			 */
			void setFilters(std::string filterText);

			/**
			 * Gets the regular expression used to perform optimized path filter matching.
			 * 
			 * @return The regular expression for path filter matching
			 */
			const std::regex &getFilterRegex() const noexcept;

			/**
			 * Determines whether a path matches this filter.
			 * This operator enables the path filter to be used as a function-like object.
			 * 
			 * @param path The path to check
			 * @return Whether the path matches the filter
			 */
			bool operator()(const cio::Path &path) const noexcept;

			/**
			 * Determines whether a path matches this filter.
			 * 
			 * @param path The path to check
			 * @return Whether the path matches the filter
			 */
			bool matches(const cio::Path &path) const noexcept;

			/**
			 * Gets the default path filter.
			 * This is just the first path filter.
			 *
			 * @return The default path filter
			 */
			std::string getDefaultFilter() const noexcept;

			/**
			 * Creates a default path using the given logical name.
			 * This will consult the path filter to create a suitable name.
			 *
			 * @param parent The parent directory for the path
			 * @param name The logical name of the resource
			 * @return The path formatted using the default path filter
			 */
			cio::Path createDefaultPath(const cio::Path &parent, std::string name) const;
			
			/**
			 * Creates a name filter text using the format's label and path filters.
			 * This name filter is compatible with Qt file dialogs when transcoded to a QString, but may also be used independently of Qt.
			 *
			 * @param name The format or filter name to use
			 * @return The created name filter text
			 */
			std::string createNameFilterText(const std::string &name) const;

			/**
			 * Gets the path filters as a UTF-8 string.
			 * 
			 * @return The path filters
			 */
			const std::string &str() const noexcept;

			/**
			 * Checks to see if the path filter is set.
			 * 
			 * @return Whether the path filter is set
			 */
			explicit operator bool() const noexcept;

		private:
			/** Regex filter text */
			std::string mFilters;

			/** Optimized regex implementing filter */
			std::regex mFilterRegex;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
