#######################################################################################################################
# Copyright 2017-2024, Geometric Progress LLC
#
# This file is part of the Careful I/O Library (CIO).
#
# CIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CIO.  If not, see <https://www.gnu.org/licenses/>
#######################################################################################################################
# Minimum version is 3.1, but we prefer building with newest 3.29
# Syntax for ... is per CMake 3.12 to allow earlier versions to ignore the max
cmake_minimum_required(VERSION 3.1...3.29)

cmake_policy(SET CMP0054 NEW)

# Enable use of ROOT variables for find_package
# Note that CIO will still work fine without it, but this silences a warning
if(POLICY CMP0074)
	cmake_policy(SET CMP0074 NEW)
endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# If CMake install prefix is not set by cache or command line, note that for later
# It will be set by project() command so we can't check after that
if(NOT CMAKE_INSTALL_PREFIX)
	set(CMAKE_INSTALL_PREFIX_MISSING 1)
endif()

project(CIO LANGUAGES CXX)

set(CIO_MAJOR_VERSION 0)
set(CIO_MINOR_VERSION 8)
set(CIO_PATCH_VERSION 0)
set(CIO_TWEAK_VERSION 0)

set(CIO_ABI_VERSION "${CIO_MAJOR_VERSION}.${CIO_MINOR_VERSION}")
set(CIO_VERSION "${CIO_ABI_VERSION}.${CIO_PATCH_VERSION}")
set(CIO_FULL_VERSION "${CIO_API_VERSION}.${CIO_PATCH_VERSION}.${CIO_TWEAK_VERSION}")

set(CIO_COPYRIGHT "Copyright 2020-2024 Geometric Progress LLC")
set(CIO_HOMEPAGE "https://bitbucket.org/gprogress/cio")

message(STATUS "CIO Version: ${CIO_VERSION}")
message(STATUS "CIO ABI Version: ${CIO_ABI_VERSION}")
if(CMAKE_LIBRARY_ARCHITECTURE)
	message(STATUS "Architecture: ${CMAKE_LIBRARY_ARCHITECTURE}")
else()
	message(STATUS "Architecture: ${CMAKE_SYSTEM_PROCESSOR} (Default)")
endif()

# Pull in Unix standard installation dirs and associated variables
# This will notably set the following in a way that handles Linux differences:
# CMAKE_INSTALL_BINDIR - runtime destination
# CMAKE_INSTALL_LIBDIR - library and archive destination
# CMAKE_INSTALL_INCLUDEDIR - header include destination
# CMAKE_INSTALL_DATAROOTDIR - data root folder
# CMAKE_INSTALL_DOCDIR - doc root folder
include(GNUInstallDirs)

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release)
endif()

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

option(BUILD_SHARED_LIBS "Compile CIO as a shared library" ON)

set(CMAKE_DEBUG_POSTFIX "d" CACHE STRING "Filename postfix for debug builds")
if(CMAKE_DEBUG_POSTFIX)
	message(STATUS "Debug Build Suffix: ${CMAKE_DEBUG_POSTFIX}")
else()
	message(STATUS "Debug Build Suffix: (none)")
endif()

set(CMAKE_RELEASE_POSTFIX "" CACHE STRING "Filename postfix for release builds")
if(CMAKE_RELEASE_POSTFIX)
	message(STATUS "Release Build Suffix: ${CMAKE_RELEASE_POSTFIX}")
else()
	message(STATUS "Release Build Suffix: (none)")
endif()

include(CopyFoundRuntime)
set(CMAKE_STAGE_QUIET ON)
include(DefaultManifest OPTIONAL)

if(NOT WIN32)
	option(CMAKE_RELATIVE_RPATH "Whether to use relative rpaths or absolute rpaths (default is OFF because of setcap issues)" OFF)
	if(CMAKE_RELATIVE_RPATH)
		message(STATUS "RPATH: Relative using \$ORIGIN")
		file(RELATIVE_PATH CIO_PROGRAM_RELATIVE_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}" "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_PROGRAM_RPATH "\$ORIGIN/${CIO_PROGRAM_RELATIVE_RPATH}")
		set(CMAKE_LIBRARY_RPATH "\$ORIGIN")
	else()
		message(STATUS "RPATH: Absolute using ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_PROGRAM_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_LIBRARY_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
	endif()
endif()

# Install to default path in Windows
if(WIN32 AND CMAKE_INSTALL_PREFIX_MISSING)
	set(CMAKE_INSTALL_PREFIX "${MANIFEST_ROOT}/cio/${CIO_VERSION}" CACHE PATH "Installation path" FORCE)
endif()
message(STATUS "Install directory: ${CMAKE_INSTALL_PREFIX}")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")

add_subdirectory("src")

option(CIO_BUILD_TESTS "Whether to build the unit tests" ON)
if(CIO_BUILD_TESTS)
	add_subdirectory("test")
endif()

find_package(AStyle)
if(AStyle_FOUND)
	message(STATUS "AStyle formatting: available (build 'style' target)")
	ADD_ASTYLE_TARGET("style" "astyle.cfg"  "src/*.cpp,*.h" "test/*.cpp,*.h")
else()
	message(STATUS "AStyle formatting: disabled (AStyle not found)")
endif()

find_package(Doxygen)
if(Doxygen_FOUND)
	message(STATUS "Doxgyen HTML generation: available (build 'doc' target)")
	set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
	set(DOXYGEN_WARN_NO_PARAMDOC YES)
	doxygen_add_docs(doc "${CMAKE_CURRENT_SOURCE_DIR}/src")
	install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/html" DESTINATION "${CMAKE_INSTALL_DOCDIR}/${CIO_VERSION}" OPTIONAL)
else()
	message(STATUS "Doxygen HTML generation: disabled (Doxygen not found)")
endif()
