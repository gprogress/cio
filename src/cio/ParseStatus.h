/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PARSESTATUS_H
#define CIO_PARSESTATUS_H

#include "Types.h"

#include "Reason.h"
#include "Validation.h"

#include <cctype>
#include <ostream>

namespace cio
{
	/**
	 * The Parse Result template is a simple data structure for understanding the results of parsing a text section
	 * to capture underlying data. The length type and character type are specified by template parameters to support
	 * parsing not just UTF-8 text (C = char, L = std::size_t) but other text and binary formats.
	 * 
	 * The actual parsed value is not included, this is provided separately or via the derived Parse template class.
	 *
	 * @tparam <C> The character type of the input text, usually char
	 * @tparam <L> The length type of the input text, usually std::size_t
	 */
	template <typename C /* = char */, typename L /* = std::size_t */>
	class ParseStatus
	{
		public:
			/** The total number of elements parsed */
			L parsed = L(0);

			/** The element that caused parsing to terminate. For text, typically NUL (0) if parsing ended due to end of string */
			C terminator = C();

			/** The parse status of how confident the parsing results are */
			Validation status = Validation::None;

			/** 
			 * The reason why parsing stopped at the given position.
			 * 
			 * None = a logical stopping point (i.e. whitespace, end of text, output fully processed) was found
			 * Underflow = all input was processed but more input might be usable and change the result
			 * Unexpected = an element not expected by the parse format was encountered so parsing stopped early
			 */
			Reason reason = Reason::None;

			/** Construct empty parse results */
			ParseStatus() noexcept;

			/**
			 * Construct empty parse results with the given initial validation status.
			 *
			 * @param valid The validation status
			 */
			inline explicit ParseStatus(Validation valid) noexcept;

			/**
			 * Construct parse results with the given initial values.
			 *
			 * @param parsed The number of elements parsed
			 * @param terminator The parse terminator
			 * @param valid The validation status
			 * @param reason The reason for parse termination
			 */
			inline ParseStatus(L parsed, C terminator, Validation valid, Reason reason = Reason::None) noexcept;

			/**
			 * Construct from any parse type.
			 *
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse result to copy
			 */
			template <typename D, typename N>
			inline ParseStatus(const ParseStatus<D, N> &in) noexcept;

			/**
			 * Assign from any parse type.
			 *
			 * @tparam <N> The parse length type
			 * @tparam <D> The parse character type
			 * @param in The parse result to copy
			 * @return this object
			 */
			template <typename D, typename N>
			inline ParseStatus<C, L> &operator=(const ParseStatus<D, N> &in) noexcept;

			/**
			 * Checks to see if the parsed value is usable at all.
			 * This is true if the Validation is Exact, LosslessConversion, LossyConversion, or TooMuchPrecision
			 *
			 * @return whether the value is usable
			 */
			inline bool valid() const noexcept;

			/**
			 * Checks to see if the parsed value is an exact full match.
			 * This is true if the Validation is Exact and partial is false.
			 *
			 * @return whether the parse is exact
			 */
			inline bool exact() const noexcept;

			/**
			 * Checks to see if the parsed value is usable at all.
			 * This is true if the Validation is Exact, LosslessConversion, LossyConversion, or TooMuchPrecision
			 *
			 * @return whether the value is usable
			 */
			inline explicit operator bool() const noexcept;
	};
	
	/**
	 * The Text Parse Status is a convenience alias to specialize the general Parse Status template to report
	 * the parse status appropriate for a UTF-8 text string.
	 */
	using TextParseStatus = ParseStatus<char, std::size_t>;

	/**
	 * Checks whether two parse status results are equal.
	 *
	 * @param left The first status
	 * @param right The second status
	 * @return whether the status results are equal
	 */
	template <typename C, typename L, typename D, typename N>
	inline bool operator==(const ParseStatus<C, L> &left, const ParseStatus<D, N> &right) noexcept;

	/**
	 * Checks whether two parse status results are not equal.
	 *
	 * @param left The first status
	 * @param right The second status
	 * @return whether the status results are not equal
	 */
	template <typename C, typename L, typename D, typename N>
	inline bool operator!=(const ParseStatus<C, L> &left, const ParseStatus<D, N> &right) noexcept;

	/**
	 * Prints the parse status to a stream for display.
	 * 
	 * @param stream The straem
	 * @param in The status
	 */
	template <typename C, typename L>
	inline std::ostream &operator<<(std::ostream &stream, const ParseStatus<C, L> &in);
}

/* Inline implementation */

namespace cio
{
	template <typename C, typename L>
	inline ParseStatus<C, L>::ParseStatus() noexcept
	{
		// nothing more to do
	}

	template <typename C, typename L>
	inline ParseStatus<C, L>::ParseStatus(Validation valid) noexcept :
		status(valid)
	{
		// nothing more to do
	}

	template <typename C, typename L>
	inline ParseStatus<C, L>::ParseStatus(L parsed, C terminator, Validation valid, Reason reason) noexcept :
		parsed(parsed),
		terminator(terminator),
		status(valid),
		reason(reason)
	{
		// nothing more to do
	}

	template <typename C, typename L>
	template <typename D, typename N>
	inline ParseStatus<C, L>::ParseStatus(const ParseStatus<D, N> &in) noexcept :
		parsed(in.parsed),
		terminator(in.terminator),
		status(in.status),
		reason(in.reason)
	{
		// nothing more to do
	}

	template <typename C, typename L>
	template <typename D, typename N>
	inline ParseStatus<C, L> &ParseStatus<C, L>::operator=(const ParseStatus<D, N> &in) noexcept
	{
		this->parsed = in.parsed;
		this->terminator = in.terminator;
		this->status = in.status;
		this->reason = in.reason;
		return *this;
	}

	template <typename C, typename L>
	inline bool ParseStatus<C, L>::valid() const noexcept
	{
		return this->status > Validation::None && this->status <= Validation::TooMuchPrecision;
	}

	template <typename C, typename L>
	inline bool ParseStatus<C, L>::exact() const noexcept
	{
		return this->status == Validation::Exact;
	}

	template <typename C, typename L>
	inline ParseStatus<C, L>::operator bool() const noexcept
	{
		return this->status > Validation::None && this->status <= Validation::TooMuchPrecision;
	}

	template <typename C, typename L, typename D, typename N>
	inline bool operator==(const ParseStatus<C, L> &left, const ParseStatus<D, N> &right) noexcept
	{
		return left.parsed == right.parsed && left.terminator == right.terminator && left.status == right.status && left.reason == right.reason;
	}

	template <typename C, typename L, typename D, typename N>
	inline bool operator!=(const ParseStatus<C, L> &left, const ParseStatus<D, N> &right) noexcept
	{
		return !(left == right);
	}

	template <typename C, typename L>
	inline std::ostream &operator<<(std::ostream &stream, const ParseStatus<C, L> &in)
	{
		stream << "Parsed: " << static_cast<std::size_t>(in.parsed) << ", Status: " << in.status << ", Terminator: ";
		if (in.terminator > 0 && in.terminator < 128 && std::isprint(in.terminator))
		{
			stream << "'" << in.terminator << "'";
		}
		else
		{
			stream << static_cast<unsigned>(in.terminator);
		}
		return stream << ", Reason: " << in.reason;
	}
}

#endif
