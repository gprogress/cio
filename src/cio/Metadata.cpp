/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Metadata.h"

#include <iostream>

namespace cio
{
	Metadata::Metadata()  noexcept :
		mType(Resource::Unknown),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}

	Metadata::Metadata(const char *location) :
		mLocation(location),
		mType(Resource::Unknown),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}

	Metadata::Metadata(std::string location) noexcept :
		mLocation(std::move(location)),
		mType(Resource::Unknown),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}

	Metadata::Metadata(Path location)  noexcept :
		mLocation(std::move(location)),
		mType(Resource::Unknown),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}

	Metadata::Metadata(Path location, ModeSet openMode)  noexcept :
		mLocation(std::move(location)),
		mMode(openMode),
		mType(Resource::Unknown),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}

	Metadata::Metadata(Path location, ModeSet openMode, Resource type)  noexcept :
		mLocation(std::move(location)),
		mMode(openMode),
		mType(type),
		mLength(CIO_UNKNOWN_LENGTH),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}
	
	Metadata::Metadata(Path location, ModeSet openMode, Resource type, Length length) noexcept :
		mLocation(std::move(location)),
		mMode(openMode),
		mType(type),
		mLength(length),
		mPhysicalSize(CIO_UNKNOWN_LENGTH)
	{
		// nothing more to do
	}
			
	Metadata::Metadata(Path location, ModeSet openMode, Resource type, Length length, Length physical) noexcept :
		mLocation(std::move(location)),
		mMode(openMode),
		mType(type),
		mLength(length),
		mPhysicalSize(physical)
	{
		// nothing more to do
	}
	
	Metadata::Metadata(const Metadata &/*in*/) = default;

	Metadata::Metadata(Metadata &&in) noexcept = default;

	Metadata &Metadata::operator=(const Metadata &/* in */) = default;

	Metadata &Metadata::operator=(Metadata &&in) noexcept = default;

	Metadata::~Metadata()  noexcept = default;

	void Metadata::setLocation(Path location) noexcept
	{
		mLocation = std::move(location);
	}

	void Metadata::clearLocation() noexcept
	{
		mLocation.clear();
	}

	Metadata Metadata::getParent() const
	{
		return Metadata(Path(mLocation.getParent()), mMode, Resource::Directory);
	}

	void Metadata::clear() noexcept
	{
		mLocation.clear();
		mType = Resource::Unknown;
		mMode.clear();
		mLength.unbound();
		mPhysicalSize.unbound();
	}
	
	bool Metadata::validate(const Metadata &resource)
	{
		if (!resource.mLocation.empty() && resource.mType != Resource::None)
		{
			if (mType != Resource::None && mMode.count(Mode::Load) && resource.mMode.count(Mode::Load))
			{
				mLocation = resource.mLocation;
				mType = resource.mType;
				mMode.erase(Mode::Create);
				mMode &= resource.mMode;
			}
			else if (mType == Resource::None && mMode.count(Mode::Create) && resource.mMode.count(Mode::Create))
			{
				mLocation = resource.mLocation;
				mType = resource.mType;
				mMode.erase(Mode::Load);
				mMode &= resource.mMode;
			}
			else
			{
				this->clear();
			}
		}

		return !mLocation.empty();
	}

	bool operator==(const Metadata &left, const Metadata &right) noexcept
	{
		return left.getLocation() == right.getLocation();
	}

	bool operator!=(const Metadata &left, const Metadata &right) noexcept
	{
		return !(left == right);
	}

	bool operator<(const Metadata &left, const Metadata &right) noexcept
	{
		return left.getLocation() < right.getLocation();
	}

	std::ostream &operator<<(std::ostream &stream, const Metadata &resource)
	{
		stream << resource.getLocation();

		if (!resource.getMode().empty())
		{
			stream << ", Mode: " << resource.getMode();
		}
		
		if (resource.getType() != Resource::Unknown)
		{
			stream << ", Resource: " << resource.getType();
		}
		
		if (resource.getLength().known())
		{
			stream << ", Length: " << resource.getLength();	
		}
		
		if (resource.getPhysicalSize().known())
		{
			stream << ", Physical Size: " << resource.getPhysicalSize();
		}

		return stream;
	}
}
