/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "IPv4Test.h"

#include <cio/ParseStatus.h>

#include <cio/test/Assert.h>

namespace cio
{
	namespace net
	{
		IPv4Test::IPv4Test() :
			cio::test::Suite("cio/net/IPv4")
		{
			CIO_TEST_METHOD(IPv4Test, parse);
			CIO_TEST_METHOD(IPv4Test, print);
			CIO_TEST_METHOD(IPv4Test, set);
			CIO_TEST_METHOD(IPv4Test, clear);
		}

		IPv4Test::~IPv4Test() noexcept = default;

		void IPv4Test::parseTest()
		{
			IPv4 ip;

			// Validate that a normal address (in this case localhost) works as intended
			{
				TextParseStatus expectedValid(9u, 0, Validation::Exact, Reason::None);
				TextParseStatus valid = ip.parse("127.0.0.1");
				CIO_ASSERT_EQUAL(expectedValid, valid);
				CIO_ASSERT_EQUAL(0x7F000001, ip.get());
			}

			// Validate a local "any" address with a port functions as intended
			{
				TextParseStatus expectedValid(7u, ':', Validation::Exact, Reason::Unexpected);
				TextParseStatus valid = ip.parse("0.0.0.0:3000");
				CIO_ASSERT_EQUAL(expectedValid, valid);
				CIO_ASSERT_EQUAL(0, ip.get());
			}
		}

		void IPv4Test::printTest()
		{
			IPv4 ip = IPv4::localhost();
			std::string expected("127.0.0.1");
			char actual[16];
			CIO_ASSERT_EQUAL(9u, ip.print(actual, 16));
			CIO_ASSERT_EQUAL(expected, actual);
		}

		void IPv4Test::setTest()
		{
			IPv4 ip;
			CIO_ASSERT_EQUAL(0u, ip.get());
			ip.set(0x7F000001);
			CIO_ASSERT_EQUAL(0x7F000001, ip.get());
		}

		void IPv4Test::clearTest()
		{
			IPv4 ip;
			CIO_ASSERT_EQUAL(0u, ip.get());
			ip.clear();
			CIO_ASSERT_EQUAL(0u, ip.get());
			ip.parse("252.101.0.5");
		}
	}
}

