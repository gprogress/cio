/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Class.h"

#include "New.h"
#include "Text.h"

#include <cstring>
#include <new>
#include <ostream>

namespace cio
{
	namespace
	{
		Class<void> sVMetaclass("void");
		Class<bool> sBMetaclass("bool");
		Class<char> sCMetaclass("char");
		Class<signed char> sSCMetaclass("signed char");
		Class<unsigned char> sUCMetaclass("unsigned char");
		Class<short> sSMetaclass("short");
		Class<unsigned short> sUSMetaclass("unsigned short");
		Class<int> sIMetaclass("int");
		Class<unsigned> sUIMetaclass("unsigned int");
		Class<long> sLMetaclass("long");
		Class<unsigned long> sULMetaclass("unsigned long");
		Class<long long> sLLMetaclass("long long");
		Class<unsigned long long> sULLMetaclass("unsigned long long");
		Class<float> sFMetaclass("float");
		Class<double> sDMetaclass("double");
	}

	const Class<Metaclass> Metaclass::sMetaclass("cio::Metaclass");
	
	const Metaclass &Metaclass::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}
			
	Metaclass::Metaclass() noexcept :
		mFullName(nullptr),
		mAllocator(New::get()),
		mType(nullptr),
		mSize(1),
		mAlignment(1)
	{
		// nothing more to do
	}
			
	Metaclass::Metaclass(const char *qname, const std::type_info &ti, std::size_t s) noexcept :
		Logger(qname),
		mFullName(qname),
		mAllocator(New::get()),
		mType(&ti),
		mSize(s),
		mAlignment(s)
	{
		// nothing more to do
	}
			
	Metaclass::Metaclass(const char *qname, const std::type_info &ti, std::size_t s, std::size_t a) noexcept :
		Logger(qname),
		mFullName(qname),
		mAllocator(New::get()),
		mType(&ti),
		mSize(s),
		mAlignment(a)
	{
		// nothing more to do
	}
			
	Metaclass::~Metaclass() noexcept = default;
			
	const Metaclass &Metaclass::getMetaclass() const noexcept
	{
		return sMetaclass;
	}
	
	void Metaclass::clear() noexcept
	{
		mAllocator = New::get();
		mSize = 1;
		mAlignment = 1;
		mType = nullptr;
		mFullName = nullptr;
		Logger::clear();
	}

	Allocator *Metaclass::getDefaultAllocator() const noexcept
	{
		return mAllocator;
	}

	Text Metaclass::getName() const noexcept
	{
		// Skip leading prefix
		std::size_t start = 0;
		std::size_t len = std::strlen(mFullName);
		while (start < len && mFullName[start] == ':')
		{
			++start;
		}

		std::size_t ns = start;
		std::size_t tp = len;

		for (std::size_t i = 0; i < len; ++i)
		{
			if (mFullName[i] == ':')
			{
				ns = i + 1;
			}
			else if (mFullName[i] == '<')
			{
				if (tp == len)
				{
					tp = i;
				}
			}
		}

		return Text(mFullName + ns, tp - ns);
	}
	
	Text Metaclass::getQualifiedName() const noexcept
	{
		// Skip leading ::
		Text qname;
		std::size_t start = 0;
		std::size_t len = std::strlen(mFullName);
		while (start < len && mFullName[start] == ':')
		{
			++start;
		}

		qname.bind(mFullName + start, len - start);
		return qname;
	}

	Text Metaclass::getNamespace() const noexcept
	{
		// Skip leading prefix
		std::size_t start = 0;
		std::size_t len = std::strlen(mFullName);
		while (start < len && mFullName[start] == ':')
		{
			++start;
		}

		std::size_t ns = start;
		std::size_t tp = len;

		for (std::size_t i = 0; i < len; ++i)
		{
			if (mFullName[i] == ':')
			{
				ns = i + 1;
			}
			else if (mFullName[i] == '<')
			{
				if (tp == len)
				{
					tp = i;
				}
			}
		}

		return Text(mFullName + start, ns - start);
	}
			
	void *Metaclass::allocate(std::size_t count) const
	{
		return mAllocator->allocate(count * mSize);
	}

	void Metaclass::deallocate(void *buffer, std::size_t count) const noexcept
	{
		mAllocator->deallocate(buffer, count);
	}

	bool Metaclass::construct(void *buffer, std::size_t count) const
	{
		if (count > 0)
		{
			std::memset(buffer, 0, count * mSize);
		}
		
		return true;
	}

	bool Metaclass::copy(const void *input, void *output, std::size_t count) const
	{
		if (count > 0)
		{
			std::memmove(output, input, count * mSize);
		}
		
		return true;
	}

	bool Metaclass::move(void *input, void *output, std::size_t count) const noexcept
	{
		if (count > 0)
		{
			std::memmove(output, input, count * mSize);
		}
		
		return true;
	}

	bool Metaclass::assign(const void *input, void *output, std::size_t count) const
	{
		if (count > 0)
		{
			std::memmove(output, input, count * mSize);
		}
		
		return true;
	}

	bool Metaclass::transfer(void *input, void *output, std::size_t count) const noexcept
	{
		// Trivial move
		if (count > 0)
		{
			std::memmove(output, input, count * mSize);
		}
		
		return true;
	}

	bool Metaclass::destroy(void *buffer, std::size_t count) const noexcept
	{
		// trivial destructor
		return true;
	}
			
	void *Metaclass::resize(void *buffer, std::size_t old, std::size_t requested) const
	{
		void *result = nullptr;

		if (old == requested)
		{
			result = buffer;
		}
		else
		{
			if (requested > 0)
			{
				result = this->allocate(requested);
			}

			this->initialize(buffer, old, result, requested);

			if (old > 0)
			{
				this->destroy(buffer, old);
				this->deallocate(buffer, old);
			}
		}

		return result;
	}

	void Metaclass::initialize(void *source, std::size_t length, void *target, std::size_t capacity) const
	{
		std::size_t overlap = std::min(length, capacity);

		if (overlap > 0)
		{
			bool moved = this->move(source, target, overlap);
			if (!moved)
			{
				bool copied = this->copy(source, target, overlap);
				if (!copied)
				{
					bool constructed = this->construct(target, overlap);
					if (constructed)
					{
						bool transferred = this->transfer(source, target, overlap);
						if (!transferred)
						{
							bool assigned = this->assign(source, target, overlap);
							if (!assigned)
							{
								overlap = 0;
							}
						}
					}
				}
			}
		}

		if (overlap < capacity)
		{
			void *remainder = this->element(target, overlap);
			this->construct(remainder, capacity - overlap);
		}
	}

	void Metaclass::initialize(const void *source, std::size_t length, void *target, std::size_t capacity) const
	{
		std::size_t overlap = std::min(length, capacity);

		if (overlap > 0)
		{
			bool copied = this->copy(source, target, overlap);
			if (!copied)
			{
				bool constructed = this->construct(target, overlap);
				if (constructed)
				{
					bool assigned = this->assign(source, target, overlap);
					if (!assigned)
					{
						overlap = 0;
					}
				}
			}
		}

		if (overlap < capacity)
		{
			void *remainder = this->element(target, overlap);
			this->construct(remainder, capacity - overlap);
		}
	}

	void *Metaclass::duplicate(const void *buffer, std::size_t count) const
	{
		void *result = nullptr;

		if (count > 0)
		{
			result = this->allocate(count);

			bool copied = this->copy(buffer, result, count);
			
			if (!copied)
			{
				this->construct(result, count);
				this->assign(buffer, result, count);
			}
		}

		return result;
	}

	void *Metaclass::duplicate(const void *buffer, std::size_t old, std::size_t requested) const
	{
		void *result = nullptr;

		if (requested > 0)
		{
			result = this->allocate(requested);
			this->initialize(buffer, old, result, requested);
		}

		return result;
	}

	bool operator==(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left == &right || left.getQualifiedName() == right.getQualifiedName();
	}

	bool operator!=(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left != &right && left.getQualifiedName() != right.getQualifiedName();
	}

	bool operator<(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left != &right && left.getQualifiedName() < right.getQualifiedName();
	}

	bool operator<=(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left == &right || left.getQualifiedName() <= right.getQualifiedName();
	}

	bool operator>(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left != &right && left.getQualifiedName() > right.getQualifiedName();
	}

	bool operator>=(const Metaclass &left, const Metaclass &right) noexcept
	{
		return &left == &right || left.getQualifiedName() >= right.getQualifiedName();
	}

	template <>
	const Metaclass &getDeclaredMetaclass<void>()
	{
		return sVMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<bool>()
	{
		return sBMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<char>()
	{
		return sCMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<unsigned char>()
	{
		return sUCMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<signed char>()
	{
		return sSCMetaclass;
	}
	template <>
	const Metaclass &getDeclaredMetaclass<short>()
	{
		return sSMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<unsigned short>()
	{
		return sUSMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<int>()
	{
		return sIMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<unsigned>()
	{
		return sUIMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<long>()
	{
		return sLMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<unsigned long>()
	{
		return sULMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<long long>()
	{
		return sLLMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<unsigned long long>()
	{
		return sULLMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<float>()
	{
		return sFMetaclass;
	}

	template <>
	const Metaclass &getDeclaredMetaclass<double>()
	{
		return sDMetaclass;
	}

	std::ostream &operator<<(std::ostream &stream, const Metaclass &mc)
	{
		return stream << mc.getQualifiedName();
	}
}
