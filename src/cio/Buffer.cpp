/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Buffer.h"

#include "Input.h"
#include "Class.h"
#include "ModeSet.h"
#include "Newline.h"
#include "Output.h"
#include "JsonWriter.h"
#include "ReadBuffer.h"
#include "Resize.h"
#include "Seek.h"
#include "Text.h"

#include <algorithm>

namespace cio
{
	Class<Buffer> Buffer::sMetaclass("cio::Buffer");

	const Metaclass &Buffer::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}
		// Constructor implementations

	Buffer::Buffer() noexcept :	
		mFactory(nullptr),
		mElements(nullptr),
		mSize(0),
		mLimit(0),
		mPosition(0),
		mGrowthPercentage(50),
		mResizePolicy(Resize::Any)
	{
		// nothing more to do
	}

	Buffer::Buffer(std::size_t capacity) :
		mElements(mFactory.create(capacity)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0),
		mGrowthPercentage(50),
		mResizePolicy(Resize::Any)
	{
		// nothing more to do
	}
	
	Buffer::Buffer(std::size_t capacity, Factory<std::uint8_t> allocator) :
		mFactory(allocator),
		mElements(mFactory.create(capacity)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0),
		mGrowthPercentage(50),
		mResizePolicy(Resize::Any)
	{
		// nothing more to do
	}
	
	Buffer::Buffer(void *data, std::size_t capacity)  noexcept :
		mFactory(nullptr),	
		mElements(static_cast<std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0),
		mGrowthPercentage(50),
		mResizePolicy(Resize::None)
	{
		// nothing more to do
	}

	Buffer::Buffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept :
		mFactory(allocator),
		mElements(static_cast<std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0),
		mGrowthPercentage(50),
		mResizePolicy(allocator ? Resize::Any : Resize::None)
	{
		// nothing more to do
	}

	Buffer::Buffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator, std::size_t limit, std::size_t position) noexcept :
		mFactory(allocator),
		mElements(static_cast<std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(std::min(limit, mSize)),
		mPosition(std::min(position, mLimit)),
		mGrowthPercentage(50),
		mResizePolicy(allocator ? Resize::Any : Resize::None)
	{
		// nothing more to do
	}

	Buffer::Buffer(const Buffer &in) :
		mFactory(in.mFactory),	
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition),
		mGrowthPercentage(in.mGrowthPercentage),
		mResizePolicy(in.mResizePolicy)
	{
		if (mFactory)
		{
			mElements = mFactory.copy(mElements, mSize);
		}
	}	

	Buffer::Buffer(Buffer &&in) noexcept :
		mFactory(in.mFactory),	
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition),
		mGrowthPercentage(in.mGrowthPercentage),
		mResizePolicy(in.mResizePolicy)
	{
		in.mFactory = nullptr;
		in.mElements = nullptr;
		in.mSize = 0;
		in.mPosition = 0;
		in.mLimit = 0;
		in.mResizePolicy = Resize::None;
	}
	
	Buffer &Buffer::operator=(const Buffer &in)
	{
		if (this != &in)
		{
			this->clear();
			
			mFactory = nullptr;
			mElements = in.mElements;
			mSize = in.mSize;
			mPosition = in.mPosition;
			mLimit = in.mLimit;
			mGrowthPercentage = in.mGrowthPercentage;
			mResizePolicy = in.mResizePolicy;
		}

		return *this;
	}
	
	Buffer &Buffer::operator=(Buffer &&in) noexcept
	{
		if (this != &in)
		{
			mFactory = in.mFactory;
			mElements = in.mElements;
			mSize = in.mSize;
			mPosition = in.mPosition;
			mLimit = in.mLimit;
			mGrowthPercentage = in.mGrowthPercentage;
			mResizePolicy = in.mResizePolicy;

			in.mFactory = nullptr;
			in.mElements = nullptr;
			in.mSize = 0;
			in.mPosition = 0;
			in.mLimit = 0;
			in.mResizePolicy = Resize::None;
		}

		return *this;
	}

	Buffer::~Buffer() noexcept
	{
		mFactory.destroy(mElements, mSize);
	}
		
	void Buffer::clear() noexcept
	{
		mFactory.destroy(mElements, mSize);
		mFactory = nullptr;

		mElements = nullptr;
		mSize = 0;
		mPosition = 0;
		mLimit = 0;
		mGrowthPercentage = 50;
		mResizePolicy = Resize::Any;
	}

	const Metaclass &Buffer::getMetaclass() const noexcept
	{
		return sMetaclass;
	}
		
	void Buffer::bind(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept
	{
		this->clear();
		
		mElements = static_cast<std::uint8_t *>(data);
		mFactory = allocator;
		mSize = capacity;
		mLimit = mSize;
		mPosition = 0;
		mResizePolicy = allocator ? Resize::Any : Resize::None;
	}

	// Core Buffer API - direct calls

	std::uint8_t *Buffer::allocate(std::size_t capacity)
	{
		this->clear();

		mFactory = Factory<std::uint8_t>();

		if (capacity > 0)
		{
			mElements = mFactory.create(capacity);
			mSize = capacity;
			mLimit = mSize;
			mPosition = 0;
			mResizePolicy = Resize::Any;
		}

		return mElements;
	}

	std::uint8_t *Buffer::allocate(std::size_t capacity, Factory<std::uint8_t> allocator)
	{
		this->clear();

		mFactory = allocator;

		if (capacity > 0)
		{
			mElements = mFactory.create(capacity);
			mSize = capacity;
			mLimit = mSize;
			mPosition = 0;
			mResizePolicy = Resize::Any;
		}

		return mElements;
	}

	void Buffer::bind(void *data, std::size_t capacity) noexcept
	{
		this->clear();

		mElements = static_cast<std::uint8_t *>(data);
		mSize = capacity;
		mLimit = mSize;
		mPosition = 0;
		mResizePolicy = Resize::None;
	}
	
	std::size_t Buffer::reserve(std::size_t capacity)
	{
		if (capacity > mSize)
		{
			if (mFactory)
			{
				mElements = mFactory.resize(mElements, mSize, capacity);
				mSize = capacity;
			}
			else if (mSize == 0)
			{
				mFactory = Factory<std::uint8_t>();
				mElements = mFactory.create(capacity);
				mSize = capacity;
			}	
		}

		return mSize;
	}
	
	std::size_t Buffer::resize(std::size_t capacity)
	{
		if (capacity != mSize)
		{
			if (mFactory)
			{
				mElements = mFactory.resize(mElements, mSize, capacity);
				mSize = capacity;
			}
			else if (mSize == 0)
			{
				mFactory = Factory<std::uint8_t>();
				mElements = mFactory.create(capacity);
				mSize = capacity;
			}
		}
		
		return mSize;
	}

	std::uint8_t *Buffer::reallocate(std::size_t capacity)
	{
		if (capacity != mSize)
		{
			if (!mFactory)
			{
				if (mSize == 0)
				{
					mFactory = Factory<std::uint8_t>();
					mElements = mFactory.create(capacity);
					mSize = capacity;
				}
			}
			else
			{
				mElements = mFactory.resize(mElements, mSize, capacity);
				mSize = capacity;
				mLimit = std::min(capacity, mLimit);
				mPosition = std::min(capacity, mPosition);
			}
		}

		return mElements;
	}
		
	std::size_t Buffer::trim()
	{
		std::size_t result = mSize;
		if (mFactory)
		{
			if (mLimit == 0)
			{
				mElements = mFactory.destroy(mElements, mSize);
				mFactory = nullptr;
				mSize = 0;
			}
			else
			{
				mElements = mFactory.resize(mElements, mSize, mLimit);
				mSize = mLimit;
			}

			result = mLimit;
		}

		return result;
	}

	std::uint8_t *Buffer::internalize()
	{
		if (mElements && !mFactory)
		{
			mFactory = Factory<std::uint8_t>();
			mElements = mFactory.copy(mElements, mSize);
		}

		return mElements;
	}

	std::uint8_t *Buffer::internalize(const void *data, std::size_t capacity)
	{
		if (mElements != data || !mFactory)
		{
			this->clear();
		
			if (capacity > 0)
			{
				mFactory = Factory<std::uint8_t>();
				mElements = mFactory.copy(static_cast<const std::uint8_t *>(data), capacity);
				mSize = capacity;
				mPosition = 0;
				mLimit = capacity;
			}
		}
		
		return mElements;
	}

	std::uint8_t *Buffer::internalize(const void *data, std::size_t capacity, Factory<std::uint8_t> allocator)
	{
		if (mElements != data || !mFactory)
		{
			this->clear();
		
			if (capacity > 0)
			{
				mFactory = allocator;
				mElements = mFactory.copy(static_cast<const std::uint8_t *>(data), capacity);
				mSize = capacity;
				mPosition = 0;
				mLimit = capacity;
			}
		}
		
		return mElements;
	}

	std::size_t Buffer::unused(std::size_t required)
	{
		std::size_t current = mSize - mLimit;
		if (current < required)
		{
			if (mResizePolicy >= Resize::Grow)
			{
				std::size_t requested = std::max(mSize + (mSize * mGrowthPercentage) / 100u, mLimit + required);
				this->resize(requested);
			}
			current = mSize - mLimit;
		}
		return current;
	}

	Buffer Buffer::duplicate() const
	{
		Buffer buffer;

		if (mElements)
		{
			Factory<std::uint8_t> allocator;
			buffer.bind(allocator.copy(mElements, mSize), mSize, allocator);
			buffer.mPosition = mPosition;
			buffer.mLimit = mLimit;
		}

		return buffer;
	}

	std::size_t Buffer::remaining(std::size_t needed)
	{
		std::size_t current = mLimit - mPosition;
		if (current < needed)
		{
			if (mLimit >= mSize && mResizePolicy >= Resize::Grow)
			{
				std::size_t requested = std::max(mSize + (mSize * mGrowthPercentage) / 100u, mPosition + needed);
				mLimit = this->resize(requested);
			}

			current = mLimit - mPosition;
		}
		return current;
	}

	std::size_t Buffer::writable(std::size_t needed)
	{
		std::size_t available = this->remaining(needed);
		if (available < needed)
		{
			throw Exception(Action::Write, Reason::Overflow);
		}
		return available;
	}
	Text Buffer::getText() noexcept
	{
		const char *current = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = mLimit - mPosition;
		std::size_t end = cio::terminator(current, mLimit - mPosition);
		std::size_t length = end + (end < capacity); // include null terminator if present
		mPosition += length;
		return Text(current, length);
	}

	Text Buffer::getText(std::size_t bytes) noexcept
	{
		const char *current = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = std::min(bytes, mLimit - mPosition);
		std::size_t end = cio::terminator(current, capacity);
		std::size_t length = end + (end < capacity); // include null terminator if present
		mPosition += length;
		return Text(current, length);
	}

	Text Buffer::getTextLine(std::size_t bytes) noexcept
	{
		const char *text = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = std::min(bytes, mLimit - mPosition);
		std::size_t length = 0;

		while (length < capacity)
		{
			char c = text[length++];
			if (!c || c == '\n')
			{
				break;
			}
		}

		mPosition += length;
		return Text(text, length);
	}

	std::size_t Buffer::putByteText(const void *bytes, std::size_t count)
	{
		this->writable(count * 2);
		std::size_t printed = cio::printBytes(bytes, count, reinterpret_cast<char *>(mElements + mPosition), mLimit - mPosition);
		mPosition += printed;
		return printed;
	}

	Text Buffer::viewProcessedText() const noexcept
	{
		const char *data = reinterpret_cast<const char *>(mElements);
		return Text(data, mPosition);
	}

	Text Buffer::viewRemainingText() const noexcept
	{
		const char *data = reinterpret_cast<const char *>(mElements + mPosition);
		return Text(data, mLimit - mPosition);
	}

	std::size_t Buffer::receive(Buffer &input) noexcept
	{
		std::size_t needed = input.mLimit - input.mPosition;
		std::size_t available = this->remaining(needed);
		std::size_t toCopy = std::min(available, needed);

		std::memcpy(mElements + mPosition, input.mElements + input.mPosition, toCopy);
		mPosition += toCopy;
		input.mPosition += toCopy;
		return toCopy;
	}

	std::size_t Buffer::receive(Buffer &input, std::size_t maxLength) noexcept
	{
		std::size_t needed = std::min(input.mLimit - input.mPosition, maxLength);
		std::size_t available = this->remaining(needed);
		std::size_t toCopy = std::min(available, needed);

		std::memcpy(mElements + mPosition, input.mElements + input.mPosition, toCopy);
		mPosition += toCopy;
		input.mPosition += toCopy;
		return toCopy;
	}

	std::size_t Buffer::receive(ReadBuffer &input) noexcept
	{
		std::size_t needed = input.mLimit - input.mPosition;
		std::size_t available = this->remaining(needed);
		std::size_t toCopy = std::min(available, needed);

		std::memcpy(mElements + mPosition, input.mElements + input.mPosition, toCopy);
		mPosition += toCopy;
		input.mPosition += toCopy;
		return toCopy;
	}

	std::size_t Buffer::receive(ReadBuffer &input, std::size_t maxLength) noexcept
	{
		std::size_t needed = std::min(input.mLimit - input.mPosition, maxLength);
		std::size_t available = this->remaining(needed);
		std::size_t toCopy = std::min(available, needed); 

		std::memcpy(mElements + mPosition, input.mElements + input.mPosition, toCopy);
		mPosition += toCopy;
		input.mPosition += toCopy;
		return toCopy;
	}

	std::size_t Buffer::send(Buffer &output) noexcept
	{
		std::size_t needed = mLimit - mPosition;
		std::size_t available = output.remaining(needed);
		std::size_t toCopy = std::min(needed, available);

		std::memcpy(output.mElements + output.mPosition, mElements + mPosition, toCopy);
		mPosition += toCopy;
		output.mPosition += toCopy;
		return toCopy;
	}

	std::size_t Buffer::send(Buffer &output, std::size_t maxLength) noexcept
	{
		std::size_t needed = std::min(mLimit - mPosition, maxLength);
		std::size_t available = output.remaining(needed);
		std::size_t toCopy = std::min(needed, available); 
		
		std::memcpy(output.mElements + output.mPosition, mElements + mPosition, toCopy);
		mPosition += toCopy;
		output.mPosition += toCopy;
		return toCopy;
	}

	Buffer &Buffer::append(Buffer &input)
	{
		std::size_t mark = mLimit;
		mLimit = mSize;
		this->writable(mark + (input.mLimit - input.mPosition));
		mPosition = mark;
		this->unflip();
		this->receive(input);
		this->flip();
		return *this;
	}

	Buffer &Buffer::append(Buffer &input, std::size_t maxLength)
	{
		std::size_t mark = mLimit;
		mLimit = mSize;
		this->writable(mark + std::min(input.mLimit - input.mPosition, maxLength));
		mPosition = mark;
		this->unflip();
		this->receive(input);
		this->flip();
		return *this;
	}

	Buffer &Buffer::append(ReadBuffer &input)
	{
		std::size_t mark = mLimit;
		mLimit = mSize;
		this->writable(mark + (input.mLimit - input.mPosition));
		mPosition = mark;
		this->unflip();
		this->receive(input);
		this->flip();
		return *this;
	}

	Buffer &Buffer::append(ReadBuffer &input, std::size_t maxLength)
	{
		std::size_t mark = mLimit;
		mLimit = mSize;
		this->writable(mark + std::min(input.mLimit - input.mPosition, maxLength));
		mPosition = mark;
		this->unflip();
		this->receive(input);
		this->flip();
		return *this;
	}

	ReadBuffer Buffer::view() const noexcept
	{
		return ReadBuffer(mElements, mSize, nullptr, mLimit, mPosition);
	}

	ReadBuffer Buffer::viewProcessed() const noexcept
	{
		return ReadBuffer(mElements, mSize, nullptr, mPosition, 0);
	}

	ReadBuffer  Buffer::viewRemaining() const noexcept
	{
		return ReadBuffer(mElements + mPosition, mLimit - mPosition, nullptr, mLimit - mPosition, 0);
	}

	Resize Buffer::getResizePolicy() const noexcept
	{
		return mResizePolicy;
	}

	void Buffer::setResizePolicy(Resize policy) noexcept
	{
		mResizePolicy = policy;
	}

	unsigned Buffer::getGrowthPercentage() const noexcept
	{
		return mGrowthPercentage;
	}

	void Buffer::setGrowthPercentage(unsigned value) noexcept
	{
		mGrowthPercentage = std::min(1000u, value);
	}

	std::string Buffer::print() const
	{
		JsonWriter writer;
		this->print(writer);
		return writer.getWriteBuffer().viewProcessedText();
	}

	std::size_t Buffer::print(char *buffer, std::size_t capacity) const noexcept
	{
		std::size_t needed = SIZE_MAX;
		try
		{
			JsonWriter writer(buffer, capacity);
			this->print(writer);
			needed = writer.getWriteBuffer().position();
		}
		catch (...)
		{
			// printing failed, size is unknown
		}
		return needed;
	}

	void Buffer::print(MarkupWriter &writer) const
	{
		writer.writeSimpleNondefaultElement("position", mPosition);
		writer.writeSimpleNondefaultElement("limit", mLimit);
		writer.writeSimpleNondefaultElement("size", mSize);
		writer.writeSimpleNondefaultElement("resize", cio::print(mResizePolicy));
		writer.writeSimpleNondefaultElement("growth", mGrowthPercentage);
		if (mFactory)
		{
			// TODO add a print method to allocator
			// writer.writeSimpleElement("allocator", mFactory.metaclass->getName());
		}
		writer.startElement("elements");
		writer.writeValue(mElements, mSize);
		writer.endValue();
	}
}
