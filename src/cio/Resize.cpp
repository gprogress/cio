/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Resize.h"

#include <cio/Case.h>

#include <istream>
#include <ostream>

namespace cio
{
	namespace
	{
		const char *sResizeText[] =
		{
			"None",
			"Shrink",
			"Grow",
			"Any",
			"Unknown"
		};
	}

	const char *print(Resize value) noexcept
	{
		return sResizeText[static_cast<unsigned>(value)];
	}

	Resize parseResize(const char *text) noexcept
	{
		Resize value = Resize::Unknown;

		for (unsigned i = 0; i < sizeof(sResizeText) / sizeof(const char *); ++i)
		{
			if (CIO_STRCASECMP(sResizeText[i], text) == 0)
			{
				value = static_cast<Resize>(i);
				break;
			}
		}

		return value;
	}

	Resize parseResize(const std::string &text) noexcept
	{
		return parseResize(text.c_str());
	}

	std::ostream &operator<<(std::ostream &stream, Resize value)
	{
		return (stream << print(value));
	}

	std::istream &operator>>(std::istream &stream, Resize &value)
	{
		std::string chunk;
		stream >> chunk;
		value = parseResize(chunk);
		return stream;
	}
}
