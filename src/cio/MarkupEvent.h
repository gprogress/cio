/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MARKUPEVENT_H
#define CIO_MARKUPEVENT_H

#include "Types.h"

#include "Markup.h"
#include "MarkupAction.h"
#include "Variant.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Markup Event class represents a discrete event occurring during markup processing.
	 * For markup readers, this represents a single token being processed representing a logical markup action.
	 * For markup writers, this is primarily used to track the current stack of written hierarchy to know
	 * how to start and end encoded values.
	 */
	class CIO_API MarkupEvent
	{
		public:
			/**
			 * Markup event key name.
			 * For elements, attributes, and instructions this is the key name.
			 * For comments and values, this is the actual partial or full text value.
			 */
			Text key;

			/** The number of relevant values that have been processed at this level. */
			std::size_t count = 0;

			/** The maximum number of values that can be processed at this level. */
			std::size_t limit = SIZE_MAX;

			/** The markup item type for this event */
			Markup markup = Markup::None;

			/** The markup action this event represents */
			MarkupAction action = MarkupAction::None;

			/** The logical value type for the event, None to indicate unknown */
			Type type = Type::None;

			/**
			 * Construct an empty markup event.
			 */
			inline MarkupEvent() noexcept = default;

			/**
			 * Construct a markup event with unknown limit.
			 * 
			 * @param markup The markup type
			 * @param action The markup action type
			 */
			inline MarkupEvent(Markup markup, MarkupAction action) noexcept;

			/**
			 * Construct a markup event with unknown limit.
			 *
			 * @param markup The markup type
			 * @param action The markup action type
			 * @param type The data type
			 */
			inline MarkupEvent(Markup markup, MarkupAction action, Type type) noexcept;

			/**
			 * Construct a markup event with a limit.
			 *
			 * @param markup The markup type
			 * @param action The markup action type
			 * @param type The data type
			 * @param limit The item limit
			 */
			inline MarkupEvent(Markup markup, MarkupAction action, Type type, std::size_t limit) noexcept;

			/**
			 * Construct a markup event with the given data and unknown limit.
			 *
			 * @param key The key type
			 * @param markup The markup type
			 * @param action The markup action type
			 */
			inline MarkupEvent(Text key, Markup markup, MarkupAction action) noexcept;

			/**
			 * Copy constructor.
			 * 
			 * @param in The event to copy
			 */
			inline MarkupEvent(const MarkupEvent &in) = default;

			/**
			 * Move constructor.
			 *
			 * @param in The event to move
			 */
			inline MarkupEvent(MarkupEvent &&in) noexcept = default;

			/**
			 * Copy assignment.
			 *
			 * @param in The event to copy
			 * @return this event
			 */
			inline MarkupEvent &operator=(const MarkupEvent &in) = default;

			/**
			 * Move assignment.
			 *
			 * @param in The event to move
			 * @return this event
			 */
			inline MarkupEvent &operator=(MarkupEvent &&in) noexcept = default;

			/**
			 * Destructor.
			 */
			inline ~MarkupEvent() noexcept = default;

			/**
			 * Clears the markup event.
			 */
			inline void clear() noexcept;

			/**
			 * Checks to see if the event matches the given markup type and action type.
			 * 
			 * @param type The markup type
			 * @param action The action type
			 * @return whether this event matches the given
			 */
			inline bool matches(Markup type, MarkupAction action) const;

			/**
			 * Checks to see if this event is set.
			 * This is true if the markup type is not None.
			 * 
			 * @return whether the event is set
			 */
			inline explicit operator bool() const;
	};

	/**
	 * Checks to see if two markup events are equal.
	 * 
	 * @param left The first event
	 * @param right The second event
	 * @return whether the events are equal
	 */
	inline bool operator==(const MarkupEvent &left, const MarkupEvent &right) noexcept;

	/**
	 * Checks to see if two markup events are not equal.
	 *
	 * @param left The first event
	 * @param right The second event
	 * @return whether the events are not equal
	 */
	inline bool operator!=(const MarkupEvent &left, const MarkupEvent &right) noexcept;

	/**
	 * Prints the given markup event to a UTF-8 text buffer.
	 * 
	 * @param e The event type
	 * @param buffer The text buffer
	 * @param capacity The capacity of the text buffer
	 * @return the actual number of characters needed to print it
	 */
	CIO_API std::size_t print(const MarkupEvent &e, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the given markup event to a C++ stream.
	 * 
	 * @param stream The stream
	 * @param e The event type
	 * @return the stream after printing
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const MarkupEvent &e);
}

/* Inline implementation */

namespace cio
{
	inline  MarkupEvent::MarkupEvent(Markup markup, MarkupAction action) noexcept :
		markup(markup),
		action(action)
	{
		// nothing more to do
	}

	inline  MarkupEvent::MarkupEvent(Markup markup, MarkupAction action, Type type) noexcept :
		markup(markup),
		action(action),
		type(type)
	{
		// nothing more to do
	}

	inline  MarkupEvent::MarkupEvent(Markup markup, MarkupAction action, Type type, std::size_t limit) noexcept :
		markup(markup),
		action(action),
		type(type),
		limit(limit)
	{
		// nothing more to do
	}

	inline MarkupEvent::MarkupEvent(Text key, Markup markup, MarkupAction action) noexcept :
		key(std::move(key)),
		markup(markup),
		action(action)
	{
		// nothing more to do
	}

	inline void MarkupEvent::clear() noexcept
	{
		this->key.clear();
		this->markup = Markup::None;
		this->action = MarkupAction::None;
		this->count = 0;
		this->limit = SIZE_MAX;
		this->type = Type::Text;
	}

	inline bool MarkupEvent::matches(Markup markup, MarkupAction action) const
	{
		return this->markup == markup && this->action == action;
	}

	inline MarkupEvent::operator bool() const
	{
		return static_cast<bool>(this->markup);
	}

	inline bool operator==(const MarkupEvent &left, const MarkupEvent &right) noexcept
	{
		return left.markup == right.markup && left.action == right.action && left.count == right.count
			&& left.limit == right.limit && left.type == right.type && left.key == right.key;
	}

	inline bool operator!=(const MarkupEvent &left, const MarkupEvent &right) noexcept
	{
		return !(left == right);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
