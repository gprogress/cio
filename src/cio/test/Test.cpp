/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Test.h"

#include <cio/Class.h>
#include <cio/Severity.h>

namespace cio
{
	namespace test
	{
		const Class<Test> Test::sMetaclass("cio::test::Test");

		const Metaclass &Test::getDeclaredMetaclass()
		{
			return sMetaclass;
		}

		Test::Test() noexcept
		{
			// nothing to do
		}

		Test::Test(Text name) :
			mName(std::move(name))
		{
			// nothing more to do
		}


		Test::Test(const Test &in) :
			mName(in.mName)
		{
			// nothing more to do
		}

		Test::Test(Test &&in) noexcept :
			mName(std::move(in.mName))
		{
			// nothing more to do
		}

		Test &Test::operator=(const Test &in)
		{
			if (this != &in)
			{
				mName = in.mName;
			}

			return *this;
		}

		Test &Test::operator=(Test &&in) noexcept
		{
			if (this != &in)
			{
				mName = std::move(in.mName);
			}

			return *this;
		}

		Test::~Test() noexcept
		{
			// nothing to do
		}

		const Metaclass &Test::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Test::clear() noexcept
		{
			mName.clear();
		}

		Severity Test::setUp()
		{
			return Severity::None;
		}

		void Test::tearDown() noexcept
		{
			// nothing to do
		}

		Severity Test::execute(Reporter &reporter)
		{
			return Severity::None;
		}

		Severity Test::executeInContext(Test *, Reporter &reporter)
		{
			return this->execute(reporter);
		}

		Severity Test::executeInFullContext(Test **context, std::size_t depth, Reporter &reporter)
		{
			return this->executeInContext(context ? context[depth - 1] : nullptr, reporter);
		}

		Test *Test::find(const Text &uri) noexcept
		{
			return (!uri || mName == uri) ? this : nullptr;
		}

		const Text &Test::getName() const
		{
			return mName;
		}

		void Test::setName(Text name)
		{
			mName = std::move(name);
		}
	}
}
