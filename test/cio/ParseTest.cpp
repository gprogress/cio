/*=====================================================================================================================
 * Copyright 2025, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
=====================================================================================================================*/
#include "ParseTest.h"

#include <cio/Encoding.h>
#include <cio/Parse.h>
#include <cio/test/Assert.h>

namespace cio
{
	ParseTest::ParseTest() :
		cio::test::Suite("cio/Parse")
	{
		this->addTestCase("parseFloat", &ParseTest::parseFloatTest);
	}

	ParseTest::~ParseTest() noexcept = default;

	void ParseTest::parseFloatTest()
	{		
		std::vector<cio::Text> invalids = {"", ".", "a", "\t", " ", "f1.3", "\n.", "\0 5.1", "-", "+", "+.", "-."};
		for (const cio::Text &text : invalids)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(!result.valid(), text);
		}

		std::vector<cio::Text> nans = {" NAN", "nan ", "\tNan\t"};
		for (const cio::Text &text : nans)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.valid(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.exact(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, std::isnan(result.value), text);
		}

		std::vector<cio::Text> positiveInfs = {
			" INF",
			"inf ",
			"\tInf\t",
			" +INF",
			"+inf ",
			"\t+Inf\t",
		};
		for (const cio::Text &text : positiveInfs)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.valid(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.exact(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(std::numeric_limits<float>::infinity(), result.value, text);
		}

		std::vector<cio::Text> negativeInfs = {
			" -INF",
			"-inf ",
			"\t-Inf\t",
		};
		for (const cio::Text &text : negativeInfs)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.valid(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(true, result.exact(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(-std::numeric_limits<float>::infinity(), result.value, text);
		}
		
		std::vector<cio::Text> zeros = {
			"  0",
			"0.0",
			"\t000\t",
			"0.",
			".000 ",
			"\n.0a",
			"  +0",
			"+0.0",
			"\t+000\t",
			"+0.",
			"+.000 ",
			"\n+.0a",
			"  -0",
			"-0.0",
			"\t-000\t",
			"-0.",
			"-.000 ",
			"\n-.0a"};
		for (const cio::Text &text : zeros)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_EQUAL_FOR_CASE(0.0f, result.value, text);
			
		}

		std::vector<cio::Text> postiveRounds = {"13", " 13.0", "00013", "13.\n", "\t13.000\t", "13.", "+13", " +13.0", "+00013", "+13.\n", "\t+13.000\t", "+13."};
		for (const cio::Text &text : postiveRounds)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(13.0f, result.value, 1), text);
		}

		std::vector<cio::Text> negativeRounds = {"-13", " -13.0", "-00013", "-13.\n", "\t-13.000\t", "-13."};
		for (const cio::Text &text : negativeRounds)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(-13.0f, result.value, 1), text);
		}

		std::vector<cio::Text> positiveFractions = {"014.123000000", "\n14.123\t", "+014.123000000", "\n+14.123\t"};
		for (const cio::Text &text : positiveFractions)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(14.123f, result.value, 1), text);
		}

		std::vector<cio::Text> negativeFractions = {"-014.123000000", "\n-14.123\t"};
		for (const cio::Text &text : negativeFractions)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(-14.123f, result.value, 1), text);
		}

		std::vector<cio::Text> positiveScientificNotation = {
			" 10e4",
			"10E+04",
			"\t1e5",
			"1E+5",
			"0.1e6",
			".1E+6\n",
			".000001e+11",
			"100000E",
			"100000e+  ",
			"100000E+00",
			"10000000e-2"
		};
		for (const cio::Text &text : positiveScientificNotation)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(100000, result.value, 1), text);
		}

		std::vector<cio::Text> negativeScientificNotation = {
			" -370e4",
			"-370E+04",
			"\t-37e5",
			"-37E+5",
			"-3.7e6",
			"-3.7E+6\n",
			"-.000037e+11",
			"-3700000E",
			"-3700000e+  ",
			"-3700000E+00",
			"-370000000e-2"};
		for (const cio::Text &text : negativeScientificNotation)
		{
			TextParse<float> result = cio::parseFloat(text);
			CIO_ASSERT_FOR_CASE(result.exact(), text);
			CIO_ASSERT_FOR_CASE(Encoding::equalWithinULP(-3700000, result.value, 1), text);
		}
	}
}
