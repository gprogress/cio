/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LIBRARYFUNCTION_H
#define CIO_LIBRARYFUNCTION_H

#include "Types.h"

#include "Library.h"

namespace cio
{
	/**
	 * The CIO Library Function class provides syntactic sugar for easing working with dynamically resolved functions
	 * from loaded libraries.
	 * 
	 * @tparam R The return type of the function
	 * @tparam A The argument types of the function
	 */
	template <typename F>
	class LibraryFunction
	{
		public:
			/** The name of the function. */
			Text name;

			/** The resolved function */
			std::function<F> function;

			/**
			 * Default constructor.
			 * The name and resolved function pointer are not set.
			 */
			inline LibraryFunction() = default;

			/**
			 * Construct a library function with its symbol name.
			 * 
			 * @param name The symbol name
			 */
			inline explicit LibraryFunction(Text name) :
				name(std::move(name))
			{
				// nothing more to do
			}

			/**
			 * Construct a library function with its symbol name and resolves
			 * it using the given library.
			 *
			 * @param library The library to use to resolve the symbol name
			 * @param name The symbol name
			 * @throw Exception If symbol resolution failed
			 */
			inline explicit LibraryFunction(const Library &library, Text name) :
				name(std::move(name))
			{
				library.resolveFunction(this->name, this->function);
			}

			/**
			 * Clears the library function state.
			 */
			inline void clear() noexcept
			{
				this->name.clear();
				this->function = nullptr;
			}

			/**
			 * Resolves the library function using the given library.
			 * 
			 * @param library The library to use to resolve the symbol name
			 * @throw Exception If symbol resolution failed
			 */
			inline void resolve(const Library &library)
			{
				library.resolveFunction(this->name, this->function);
			}

			/**
			 * Resolves the library function using the given library.
			 * The given default symbol name is used if the library function does not yet have a symbol name.
			 *
			 * @param library The library to use to resolve the symbol name
			 * @param defaultName The default symbol name to use if the function symbol name was not set
			 * @throw Exception If symbol resolution failed
			 */
			inline void resolve(const Library &library, Text defaultName)
			{
				if (!this->name)
				{
					this->name = std::move(defaultName);
				}

				library.resolveFunction(this->name, this->function);
			}

			/**
			 * Resolves the library function using the given library.
			 * 
			 * This method does not throw on failure, and should be used if the symbol not existing
			 * is a valid outcome.
			 * 
			 * @param library The library to use to resolve the symbol name
			 * @return whether the symbol was found
			 */
			inline bool find(const Library &library)
			{
				library.findFunction(this->name, this->function);
				return this->function != nullptr;
			}

			/**
			 * Resolves the library function using the given library.
			 * The given default symbol name is used if the library function does not yet have a symbol name.
			 *
			 * @param library The library to use to resolve the symbol name
			 * @param defaultName The default symbol name to use if the function symbol name was not set
			 * @throw Exception If symbol resolution failed
			 */
			inline bool find(const Library &library, Text defaultName)
			{
				if (!this->name)
				{
					this->name = std::move(defaultName);
				}

				library.findFunction(this->name, this->function);
				return this->function != nullptr;
			}

			/**
			 * Invokes the resolved function.
			 * 
			 * @param args The arguments to the function
			 * @return the result of the function
			 */
			template <typename... A>
			auto operator()(A &&...args) const
			{
				return this->function(std::forward<A>(args)...);
			}
	};
}

#endif

