/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_PATHTEST_H
#define CIO_PATHTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the cio::Path class.
	 */
	class PathTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructor.
			 */
			PathTest();

			/**
			 * Destructor.
			 */
			virtual ~PathTest() noexcept override;
			
			/**
			 * Tests that Path::hasNativeRoot() works properly.
			 */
			void hasNativeRootTest();
			
			/**
			 * Tests that Path::hasWindowsRoot() works properly.
			 */
			void hasWindowsRootTest();
			
			/**
			 * Tests that Path::hasUnixRoot() works properly.
			 */
			void hasUnixRootTest();
			
			/**
			 * Tests that Path::fromNativeFile() works properly.
			 */
			void fromNativeFileTest();
			
			/**
			 * Tests that Path::fromWindowsFile() works properly.
			 */
			void fromWindowsFileTest();
			
			/**
			 * Tests that Path::fromUnixFile() works properly.
			 */
			void fromUnixFileTest();

			/**
			 * Tests that Path::getNativeSeparator() works properly.
			 */
			void getNativeSeparatorTest();

			/**
			 * Tests that Path::isNativeSeparator() works properly.
			 */
			void isNativeSeparatorTest();

			/**
			 * Tests that Path::isWindowsSeparator() works properly.
			 */
			void isWindowsSeparatorTest();

			/**
			 * Tests that Path::isUnixSeparator() works properly.
			 */
			void isUnixSeparatorTest();

			/**
			 * Tests that Path::validateNativeInputText() works properly.
			 */
			void validateNativeInputTextTest();

			/**
			 * Tests that Path::setRoot() works properly.
			 * Also tests Path::getRoot().
			 */
			void setRootTest();
			
			/**
			 * Tests that Path::setPath() works properly.
			 * Also tests Path::getPath().
			 */
			void setPathTest();
			
			/**
			 * Tests that Path::merge() works properly.
			 */
			void mergeTest();
			
			/**
			 * Tests that Path::getExtension() works properly.
			 */
			void getExtensionTest();
			
			/**
			 * Tests that Path::getQuery() works properly.
			 */
			void getQueryTest();
			
			/**
			 * Tests that Path::getParent() works properly.
			 */
			void getParentTest();
			
			/**
			 * Tests that Path::getFilename() works properly.
			 */
			void getFilenameTest();
			
			/**
			 * Tests that Path::getFilenameNoExtension() works properly.
			 */
			void getFilenameNoExtensionTest();
			
			/**
			 * Tests that Path::setProtocol() works properly.
			 * Also tests Path::getProtocol().
			 */
			void setProtocolTest();
			
			/**
			 * Tests that Path::matchesProtocol() works properly.
			 */
			void matchesProtocolTest();
			
			/**
			 * Tests that Path::isFileProtocol() works properly.
			 */
			void isFileProtocolTest();
			
			/**
			 * Tests that Path::matchesExtension() works properly.
			 */
			void matchesExtensionTest();
			
			/**
			 * Tests that Path::setExtension() works properly.
			 */
			void setExtensionTest();
			
			/**
			 * Tests that Path::removeExtension() works properly.
			 */
			void removeExtensionTest();

			/**
			 * Tests that Path::isComplete() works properly.
			 */
			void isRelativeTest();
			
			/**
			 * Tests that Path::isComplete() works properly.
			 */
			void isCompleteTest();

			/**
			 * Tests that Path::isRoot() works properly.
			 */
			void isRootTest();

			/**
			 * Tests that Path::complete() works properly.
			 */
			void completeTest();

			/**
			 * Tests that Path::makeCompletePath() works properly.
			 */
			void makeCompletePathTest();

			/**
			 * Tests that Path::makeRelativePath() works properly.
			 */
			void makeRelativePathTest();

			/**
			 * Tests that Path::makePathWithExtension() works properly.
			 */
			void makePathWithExtensionTest();

			/**
			 * Tests that Path::addChild() works properly.
			 */
			void addChildTest();

			/**
			 * Tests that Path::toNativeFile() works properly.
			 */
			void toNativeFileTest();

			/**
			 * Tests that Path::toWindowsFile() works properly.
			 */
			void toWindowsFileTest();

			/**
			 * Tests that Path::toUnixFile() works properly.
			 */
			void toUnixFileTest();

			/**
			 * Tests that Path::validateNativeInput() works properly.
			 */
			void validateNativeInputTest();

			/**
			 * Tests that Path::encode() works properly.
			 */
			void encodeTest();

			/**
			 * Tests that Path::decode works properly.
			 */
			void decodeTest();
	};
}

#endif

