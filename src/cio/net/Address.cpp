/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Address.h"

#include "AddressFamily.h"
#include "Flow.h"

#include <cio/Parse.h>
#include <cio/Print.h>

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#include <sys/types.h>
#endif

#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2

#if defined CIO_HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif

#if defined CIO_HAVE_WS2TCPIP_H
#include <WS2tcpip.h>
#endif
#endif

#include <istream>
#include <ostream>

namespace cio
{
	namespace net
	{
		Address::Address() noexcept :
			mData(nullptr),
			mLength(0)
		{
			std::memset(mBytes, 0, sizeof(mBytes));
		}

		Address::Address(const void *data, std::size_t len) :
			mData(nullptr),
			mLength(0)
		{
			this->set(data, len);
		}

		Address::Address(IPv4 ip, std::uint16_t port /* = 0 */) noexcept :
			mData(nullptr),
			mLength(0)
		{
			this->setIP4(ip, port);
		}

		Address::Address(IPv6 ip, std::uint16_t port /* = 0 */) noexcept :
			mData(nullptr),
			mLength(0)
		{
			this->setIP6(ip, port);
		}

		Address::Address(const Address &in) :
			mData(nullptr),
			mLength(0)
		{
			std::memset(mBytes, 0, sizeof(mBytes));

			if (in.mData && in.mLength > 0)
			{
				if (in.mLength <= sizeof(in.mBytes))
				{
					mData = mBytes;
				}
				else if (in.mData)
				{
					mData = new std::uint8_t[in.mLength];
				}

				mLength = in.mLength;
				std::memcpy(mData, in.mData, in.mLength);
			}
		}

		Address::Address(Address &&in) noexcept :
			mData(nullptr),
			mLength(0)
		{
			std::memset(mBytes, 0, sizeof(mBytes));

			if (in.mLength > 0)
			{
				if (in.mLength <= sizeof(in.mBytes))
				{
					mData = mBytes;
					std::memcpy(mBytes, in.mData, in.mLength);
				}
				else
				{
					mData = in.mData;
					in.mData = nullptr;
					in.mLength = 0;
				}

				mLength = in.mLength;
			}
		}

		Address &Address::operator=(const Address &in)
		{
			if (this != &in)
			{
				this->clear();

				if (in.mLength > 0)
				{
					if (in.mLength <= sizeof(in.mBytes))
					{
						mData = mBytes;
					}
					else
					{
						mData = new std::uint8_t[in.mLength];
					}

					mLength = in.mLength;
					std::memcpy(mData, in.mData, in.mLength);
				}
			}

			return *this;
		}

		Address &Address::operator=(Address &&in) noexcept
		{
			if (this != &in)
			{
				this->clear();

				if (in.mLength > 0)
				{
					if (in.mLength <= sizeof(in.mBytes))
					{
						mData = mBytes;
						std::memcpy(mBytes, in.mData, in.mLength);
					}
					else
					{
						mData = in.mData;
						in.mData = nullptr;
						in.mLength = 0;
					}

					mLength = in.mLength;
				}
			}

			return *this;
		}

		Address::~Address() noexcept
		{
			this->clear();
		}

		void Address::clear() noexcept
		{
			if (mLength > sizeof(mBytes))
			{
				delete [] mData;
			}

			mData = nullptr;
			mLength = 0;
			std::memset(mBytes, 0, sizeof(mBytes));
		}

		AddressFamily Address::getAddressFamily() const noexcept
		{
			int af = 0;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);
				af = sa->sa_family;
			}

			return AddressFamily(af);
		}

		std::uint16_t Address::getPort() const noexcept
		{
			std::int16_t port = 0;

			if (this->isIP())
			{
				// port is at the same offset for both IPv4 and IPv6
				const struct sockaddr_in *sip4 = reinterpret_cast<const struct sockaddr_in *>(mData);
				port = ntohs(sip4->sin_port);
			}

			return port;
		}

		bool Address::setPort(std::uint16_t port) noexcept
		{
			bool found = false;

			if (this->isIP())
			{
				// port is at the same offset for both IPv4 and IPv6
				struct sockaddr_in *sip4 = reinterpret_cast<struct sockaddr_in *>(mData);
				sip4->sin_port = htons(port);
				found = true;
			}

			return found;
		}

		bool Address::hasPort() const noexcept
		{
			bool found = false;

			if (this->isIP())
			{
				const struct sockaddr_in *sip4 = reinterpret_cast<const struct sockaddr_in *>(mData);
				found = (sip4->sin_port > 0);
			}

			return found;
		}

		void Address::setIP4(IPv4 ip, std::uint16_t port /*= 0*/) noexcept
		{
			this->clear();

			struct sockaddr_in *sip4 = reinterpret_cast<struct sockaddr_in *>(mBytes);
			sip4->sin_family = AF_INET;
			sip4->sin_port = htons(port);
			sip4->sin_addr.s_addr = htonl(ip.get());

			mData = mBytes;
			mLength = sizeof(struct sockaddr_in);
		}

		void Address::setIP6(IPv6 ip, std::uint16_t port /*= 0*/) noexcept
		{
			this->clear();

			struct sockaddr_in6 *sip6 = reinterpret_cast<struct sockaddr_in6 *>(mBytes);
			sip6->sin6_family = AF_INET6;
			sip6->sin6_port = htons(port);
			sip6->sin6_flowinfo = 0;

			std::uint64_t high = ip.high();
			std::uint64_t low = ip.low();

#if CIO_ENDIAN == CIO_LITTLE_ENDIAN
			std::uint64_t tmp = swapBytes(high);
			high = swapBytes(low);
			low = tmp;
#elif CIO_ENDIAN != CIO_BIG_ENDIAN

			if (detectRuntimeByteOrder() == ByteOrder::Little)
			{
				std::uint64_t tmp = swapBytes(high);
				high = swapBytes(low);
				low = tmp;
			}

#endif

			std::memcpy(&sip6->sin6_addr, &high, 8);
			std::memcpy(reinterpret_cast<std::uint8_t *>(&sip6->sin6_addr) + 8, &low, 8);

			mData = mBytes;
			mLength = sizeof(struct sockaddr_in6);
		}

		void Address::set(const void *data, std::size_t length)
		{
			if (data != mData)
			{
				this->clear();

				if (length <= sizeof(mBytes))
				{
					mData = mBytes;
				}
				else
				{
					mData = new std::uint8_t[length];
				}

				std::memcpy(mData, data, length);
				mLength = length;
			}
		}

		IPv4 Address::getIP4() const noexcept
		{
			IPv4 ip;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);

				if (sa->sa_family == AF_INET)
				{
					const struct sockaddr_in *sip4 = reinterpret_cast<const struct sockaddr_in *>(mData);
					ip.set(ntohl(sip4->sin_addr.s_addr));
				}
			}

			return ip;
		}

		IPv6 Address::getIP6() const noexcept
		{
			IPv6 ip;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);

				if (sa->sa_family == AF_INET6)
				{
					const struct sockaddr_in6 *sip6 = reinterpret_cast<const struct sockaddr_in6 *>(mData);
					std::uint64_t high = 0;
					std::uint64_t low = 0;
					std::memcpy(&high, &sip6->sin6_addr, 8);
					std::memcpy(&low, reinterpret_cast<const std::uint8_t *>(&sip6->sin6_addr) + 8, 8);
#if CIO_ENDIAN == CIO_LITTLE_ENDIAN
					ip.set(swapBytes(low), swapBytes(high));
#elif CIO_ENDIAN == CIO_BIG_ENDIAN
					ip.set(high, low);
#else

					if (detectRuntimeByteOrder() == ByteOrder::Little)
					{
						ip.set(swapBytes(low), swapBytes(high));
					}
					else
					{
						ip.set(high, low);
					}

#endif
				}
			}

			return ip;
		}

		unsigned Address::isIP() const noexcept
		{
			unsigned value = 0;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);

				if (sa->sa_family == AF_INET)
				{
					value = 4;
				}
				else if (sa->sa_family == AF_INET6)
				{
					value = 6;
				}
			}

			return value;
		}

		bool Address::isIP4() const noexcept
		{
			bool found = false;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);
				found = (sa->sa_family == AF_INET);
			}

			return found;
		}

		bool Address::isIP6() const noexcept
		{
			bool found = false;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);
				found = (sa->sa_family == AF_INET6);
			}

			return found;
		}

		unsigned Address::isAnyIP() const noexcept
		{
			unsigned value = 0;

			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);

				if (sa->sa_family == AF_INET)
				{
					const struct sockaddr_in *sip4 = reinterpret_cast<const struct sockaddr_in *>(mData);
					if (sip4->sin_addr.s_addr == 0)
					{
						value = 4;
					}
				}
				else if (sa->sa_family == AF_INET6)
				{
					const struct sockaddr_in6 *sip6 = reinterpret_cast<const struct sockaddr_in6 *>(mData);
					std::uint64_t high = 0;
					std::uint64_t low = 0;
					std::memcpy(&high, &sip6->sin6_addr, 8);
					std::memcpy(&low, reinterpret_cast<const std::uint8_t *>(&sip6->sin6_addr) + 8, 8);
					if (high == 0 && low == 0)
					{
						value = 6;
					}
				}
			}

			return value;
		}
		
		bool Address::isBroadcast() const noexcept
		{
			bool matched = false;
			if (mLength > 0)
			{
				const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(mData);

				if (sa->sa_family == AF_INET)
				{
					const struct sockaddr_in *sip4 = reinterpret_cast<const struct sockaddr_in *>(mData);
					if (sip4->sin_addr.s_addr == 0xFFFFFFFFu)
					{
						matched = true;
					}
				}
			}
			return matched;
		}
		
		bool Address::supports(Flow flow) const noexcept
		{
			bool answer = false;
			if (this->isIP4())
			{
				answer = IPv4::supports(flow);
			}
			else if (this->isIP6())
			{
				answer = IPv6::supports(flow);
			}
			else if (mLength > 0)
			{
				answer = (flow == Flow::Unicast);
			}
			return answer;
		}

		TextParseStatus Address::parse(const Text &text) noexcept
		{
			std::uint16_t port = 0;

			// Try parsing IPv4 first
			IPv4 candidate;
			TextParseStatus valid = candidate.parse(text);
			if (valid)
			{
				// If we stopped at a ':' see if we also have a port
				if (valid.terminator == ':')
				{
					std::size_t offset = valid.parsed + 1;
					port = parseUnsigned16(text.suffix(offset));
				}

				this->setIP4(candidate, port);
			}
			else
			{
				// Try parsing IPv6 next
				IPv6 candidate6;
				valid = candidate6.parse(text);
				if (valid)
				{
					// If we stopped at a ':' see if we also have a port
					if (valid.terminator == ':')
					{
						std::size_t offset = valid.parsed + 1;
						port = parseUnsigned16(text.suffix(offset));
					}

					this->setIP6(candidate6, port);
				}
			}

			return valid;
		}

		std::string Address::print() const
		{
			std::string text;

			if (this->isIP4())
			{
				char tmp[24];
				IPv4 ip = this->getIP4();
				std::size_t off = ip.print(tmp, 24);
				std::uint16_t port = this->getPort();

				if (port != 0)
				{
					tmp[off++] = ':';
					off += cio::print(port, tmp + off, 6);
				}

				text.assign(tmp, tmp + off);
			}
			else if (this->isIP6())
			{
				char tmp[58] = { '[' };
				IPv6 ip = this->getIP6();
				std::size_t off = 1 + ip.print(tmp + 1, 48);
				tmp[off++] = ']';
				std::uint16_t port = this->getPort();

				if (port != 0)
				{
					tmp[off++] = ':';
					off += cio::print(this->getPort(), tmp + off, 6);
				}

				text.assign(tmp, tmp + off);
			}
			else
			{
				text = cio::printBytes(mData, mLength);
			}

			return text;
		}

		void *Address::data() noexcept
		{
			return mData;
		}

		const void *Address::data() const noexcept
		{
			return mData;
		}

		std::size_t Address::size() const noexcept
		{
			return mLength;
		}

		bool Address::empty() const noexcept
		{
			return mLength == 0;
		}

		Address::operator bool() const noexcept
		{
			return mLength > 0;
		}

		std::ostream &operator<<(std::ostream &stream, const Address &in)
		{
			return stream << in.print();
		}
	}
}


