/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Case.h"

namespace cio
{
	const Enumerant<Case> Enumeration<Case>::values[] = {
		{ Case::Insensitive, "Insensitive", "Ignore case and use host default" },
		{ Case::Lower, "Lower", "Always use lower case"},
		{ Case::Upper, "Upper", "Always use upper case"},
		{ Case::Preserve, "Preserve", "Ignore case but preserve as provided" },
		{ Case::Sensitive, "Sensitive", "Consider case and use exactly as provided"}
	};

	const Enumeration<Case> Enumeration<Case>::instance;

	Enumeration<Case>::Enumeration() :
		Enumerants<Case>("cio::Case", values, Case::Preserve)
	{
		// nothing more to do
	}

	Enumeration<Case>::~Enumeration() noexcept = default;

}
