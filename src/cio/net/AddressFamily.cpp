/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "AddressFamily.h"

#include <cio/Case.h>

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2
#if defined CIO_HAVE_WS2TCPIP_H
#include <Ws2tcpip.h>
#endif
#endif

#include <ostream>

// Don't warn for use of std::strncpy
#if defined _MSC_VER
#pragma warning(disable: 4996)
#endif

namespace cio
{
	namespace net
	{
		AddressFamily AddressFamily::ipv4() noexcept
		{
			return AddressFamily(AF_INET);
		}

		AddressFamily AddressFamily::ipv6() noexcept
		{
			return AddressFamily(AF_INET6);
		}

		Validation AddressFamily::parse(const char *text) noexcept
		{
			Validation valid = Validation::Missing;

			if (text)
			{
				std::size_t len = std::strlen(text);
				valid = this->parse(text, len);
			}

			return valid;
		}

		Validation AddressFamily::parse(const char *text, std::size_t len) noexcept
		{
			Validation valid = Validation::Missing;

			if (text && len > 0)
			{
				if (CIO_STRNCASECMP(text, "IPv4", len) == 0)
				{
					mCode = AF_INET;
					valid = Validation::Exact;
				}
				else if (CIO_STRNCASECMP(text, "IPv6", len) == 0)
				{
					mCode = AF_INET6;
					valid = Validation::Exact;
				}
				else
				{
					// TODO add platform-specific protocols and/or look up services?
					mCode = 0;
					valid = Validation::InvalidValue;
				}
			}

			return valid;
		}

		Validation AddressFamily::parse(const std::string &text) noexcept
		{
			return this->parse(text.c_str(), text.size());
		}

		std::size_t AddressFamily::print(char *text, std::size_t len) const noexcept
		{
			std::size_t actual = 0;

			switch (mCode)
			{
				case AF_INET:
					actual = 4;

					if (text && len > 0)
					{
						std::strncpy(text, "IPv4", len - 1);
					}

					break;

				case AF_INET6:
					actual = 4;

					if (text && len > 0)
					{
						std::strncpy(text, "IPv6", len - 1);
					}

					break;

				default:
					// TODO add platform specific ones or lookup somehow?
					// nothing to do
					break;
			}

			if (actual < len)
			{
				std::memset(text + actual, 0, len - actual);
			}

			return actual;
		}

		std::string AddressFamily::print() const
		{
			char buffer[16];
			std::size_t actual = this->print(buffer, 16);
			return std::string(buffer, buffer + 16);
		}

		std::ostream &operator<<(std::ostream &stream, const AddressFamily &type)
		{
			char buffer[16];
			type.print(buffer, 16);
			return stream << buffer;
		}
	}
}
