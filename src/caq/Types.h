/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_TYPES_H
#define CAQ_TYPES_H

#include <cstdint>
#include <functional>
#include <type_traits>

/**
 * The Careful Array Quantizer (CAQ) library is an optimized header-only library
 * for encoding and decoding values to arrays and array-like objects.
 */
namespace caq
{
	class Abstract;
	template <typename> class Accessor;
	template <typename, typename, std::size_t> class Aligned;
	template <typename, typename, std::size_t > class Native;
	template <typename, typename> class Indexed;
	template <typename> class Reader;
	template <typename> class Sequence;
	template <typename, std::size_t> class Unaligned;
}

#endif