/*==============================================================================
 * Copyright 20202-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/

#include "TestSuite.h"

#include <cio/ModeSet.h>
#include <cio/Print.h>
#include <cio/test/Reporter.h>

#if defined CIO_NET_TEST

#include "net/IPv4Test.h"
#include "net/IPv6Test.h"

#include <cio/net/StreamChannel.h>

#include <cio/net/DatagramChannel.h>
#include <cio/net/Resolver.h>
#include <cio/net/StreamChannel.h>
#include <cio/net/StreamServer.h>

#endif

#if defined CIO_ZIP_TEST
#include <cio/zip/Archive.h>
#endif

#include <thread>
#include <iostream>

int main(int argc, char **argv)
{
	cio::test::Reporter logger;
	
	cio::TestSuite suite;
	
#if defined CIO_NET_TEST
	suite.addTestCase(cio::net::IPv4Test());
	suite.addTestCase(cio::net::IPv6Test());
#endif

	// See if we specified a test URI to only execute a single test
	cio::Text testUri;
	{
		int argnc = argc - 1;
		char **argn = argv + 1;

		while (argnc > 0)
		{
			if (argnc > 1 && std::strcmp(argn[0], "--test") == 0)
			{
				testUri = argn[1];
				argnc -= 2;
				argn += 2;
			}
			else
			{
				argnc -= 1;
				argn += 1;
			}
		}
	}

	cio::Severity result;
	cio::test::Test *test = suite.find(testUri);
	if (test)
	{
		result = test->execute(logger);
	}
	else
	{
		logger.critical() << "Test case " << testUri << " does not exist";
		result = cio::Severity::Critical;
	}

#if defined CIO_ZIP_TEST
	cio::Path zipPath;
	int argnc = argc - 1;
	char **argn = argv + 1;

	while (argnc > 0)
	{
		if (argnc > 1 && std::strcmp(argn[0], "--zip") == 0)
		{
			zipPath = cio::Path::fromNativeFile(argn[1]);

			argnc -= 2;
			argn += 2;
		}
		else
		{
			--argnc;
			++argn;
		}
	}

	if (!zipPath.empty())
	{
		std::cout << "Dumping Zip entries for " << zipPath << std::endl;

		cio::zip::Archive archive(zipPath);

		while (archive.traversable())
		{
			std::cout << archive.filename() << std::endl;
			archive.next();
		}
	}

#endif
	
#if defined CIO_NET_TEST
	cio::net::Resolver resolver;
	std::thread serverThread;
	cio::net::DatagramChannel udp;
	cio::net::DatagramChannel client;
	cio::net::StreamChannel tcpclient;
	cio::net::StreamServer tcpserver;
	cio::Path clientPath;
	bool doUdpClient = false;
	bool doTcpClient = false;
	auto server = [&]()
	{
		cio::net::Datagram datagram = udp.receive();
		std::cout << "Datagram sender was " << datagram.getSender() << std::endl;
		std::cout.write(reinterpret_cast<const char *>(datagram.getPacket().current()), datagram.getPacket().available());
		std::cout << std::endl;
		datagram.reply();
		datagram.getPacket() << "And your father smelled of elderberries";
		datagram.getPacket().flip();
		auto netResult = udp.send(datagram);
	};
	auto tcpServeThread = [&]()
	{
		cio::net::StreamChannel connection = tcpserver.waitForConnection();
		cio::Buffer buffer(1024);
		connection.requestFill(buffer);
		buffer.flip();
		std::cout.write(reinterpret_cast<const char *>(buffer.current()), buffer.available());
		std::cout << std::endl;
		buffer.reset();
		buffer << ("And your father smelled of elderberries");
		buffer.flip();
		connection.drain(buffer);
	};

	// Process net args
	{
		int argnc = argc - 1;
		char **argn = argv + 1;

		while (argnc > 0)
		{
			if (argnc > 1 && std::strcmp(argn[0], "--serve") == 0)
			{
				cio::Path path(argn[1]);
				cio::net::Service service = resolver.lookupService(path);

				if (service.getChannelType() == cio::net::ChannelType::Stream)
				{
					std::cout << "Running stream server test " << service << std::endl;
					tcpserver.listenToInterface(service);
					serverThread = std::thread(tcpServeThread);
				}
				else
				{
					std::cout << "Running datagram server test" << service << std::endl;
					udp.open(argn[1]);
					serverThread = std::thread(server);
				}

				argnc -= 2;
				argn += 2;
			}
			else if (argnc > 1 && std::strcmp(argn[0], "--send") == 0)
			{
				cio::Path path(argn[1]);
				cio::net::Service service = resolver.lookupService(path);

				if (service.getChannelType() == cio::net::ChannelType::Stream)
				{
					std::cout << "Running stream client test" << service << std::endl;
					clientPath = std::move(path);
					doTcpClient = true;
				}
				else
				{
					std::cout << "Running datagram client test" << service << std::endl;
					client.open(path);
					doUdpClient = true;
				}

				argnc -= 2;
				argn += 2;
			}
			else
			{
				--argnc;
				++argn;
			}
		}
	}

	if (doUdpClient)
	{
		// Server might be slow to start
		std::this_thread::sleep_for(std::chrono::seconds(1));
		client << "Your mother was a hamster";
		std::cout << "Client stored text" << std::endl;
		cio::net::Datagram datagram = client.receive();
		std::cout << "Datagram sender was " << datagram.getSender() << std::endl;
		std::cout.write(reinterpret_cast<const char *>(datagram.getPacket().current()), datagram.getPacket().available());
		std::cout << std::endl;
	}

	if (doTcpClient)
	{
		// Server might be slow to start
		std::this_thread::sleep_for(std::chrono::seconds(1));
		tcpclient.open(clientPath);
		tcpclient << "Your mother was a hamster";
		std::cout << "Client stored text" << std::endl;
		cio::Buffer buffer(1024);
		tcpclient.requestFill(buffer);
		buffer.flip();
		std::cout.write(reinterpret_cast<const char *>(buffer.current()), buffer.available());
		std::cout << std::endl;
	}

	if (serverThread.joinable())
	{
		serverThread.join();
	}
#endif

	return result >= cio::Severity::Error;
}
