/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/

#include "Device.h"

#include "Class.h"
#include "Exception.h"
#include "Filesystem.h"
#include "Path.h"
#include "Resize.h"
#include "Traversal.h"
#include "Seek.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>
#define CIO_INVALID_DEVICE reinterpret_cast<std::uintptr_t>(INVALID_HANDLE_VALUE)

#elif defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>


#define CIO_INVALID_DEVICE static_cast<std::uintptr_t>(-1)
#else

#error "No platform implementation for acsl::Device"

#endif

#include <cerrno>
#include <system_error>

namespace cio
{
	Class<Device> Device::sMetaclass("cio::Device");

	const Metaclass &Device::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	std::uintptr_t Device::getInvalidHandle() noexcept
	{
		return CIO_INVALID_DEVICE;
	}

	Device::Device() noexcept :
		mHandle(CIO_INVALID_DEVICE),
		mWriteBuffered(false)
	{
		// nothing to do
	}
		
	Device::Device(std::uintptr_t handle) noexcept :
		mHandle(handle),
		mWriteBuffered(false)
	{
		// nothing more to do
	}

	Device::Device(Device &&in) noexcept :
		cio::Channel(std::move(in)),
		mHandle(in.mHandle),
		mRemoveOnClose(std::move(in.mRemoveOnClose)),
		mWriteBuffered(in.mWriteBuffered)
	{
		in.mHandle = CIO_INVALID_DEVICE;
		in.mWriteBuffered = false;
	}

	Device &Device::operator=(Device &&in) noexcept
	{
		if (this != &in)
		{
			cio::Channel::operator=(std::move(in));
			this->clear();
			mHandle = in.mHandle;
			mRemoveOnClose = std::move(in.mRemoveOnClose);
			mWriteBuffered = in.mWriteBuffered;
				
			in.mHandle = CIO_INVALID_DEVICE;
			in.mWriteBuffered = false;
		}

		return *this;
	}

	Device::~Device()  noexcept
	{
		this->disposeHandle();
	}

	const Metaclass &Device::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Device::clear() noexcept
	{
		this->disposeHandle();
	}
		
	bool Device::isOpen() const noexcept
	{
		return mHandle != CIO_INVALID_DEVICE;
	}

	std::uintptr_t Device::getHandle() const noexcept
	{
		return mHandle;
	}

	std::uintptr_t Device::takeHandle() noexcept
	{
		auto handle = mHandle;
		mHandle = CIO_INVALID_DEVICE;
		mRemoveOnClose.clear();
		mWriteBuffered = false;
		return handle;
	}

	void Device::adoptHandle(std::uintptr_t handle) noexcept
	{
		this->clear();
		mHandle = handle;
		mWriteBuffered = false;
	}

	cio::ModeSet Device::openNativePath(Text path)
	{
		return this->openNativePath(path, cio::ModeSet::general());
	}

	cio::ModeSet Device::openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory)
	{
		return this->openNativePath(resource.toNativeFile(), modes);
	}

	bool Device::isWriteBuffered() const
	{
		return mWriteBuffered;
	}

	// Platform-specific code
#if defined CIO_HAVE_WINDOWS_H

	bool Device::linkedReadWritePosition() const noexcept
	{
		DWORD type = ::GetFileType(reinterpret_cast<HANDLE>(mHandle));
		return type == FILE_TYPE_DISK;
	}

	Length Device::size() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		LARGE_INTEGER value;
		BOOL success = ::GetFileSizeEx(reinterpret_cast<HANDLE>(mHandle), &value);

		if (success)
		{
			result = value.QuadPart;
		}
			
		return result;
	}
		
	Resize Device::resizable() const noexcept
	{
		DWORD type = ::GetFileType(reinterpret_cast<HANDLE>(mHandle));
		return type == FILE_TYPE_DISK ? Resize::Any : Resize::Grow;
	}
				
	bool Device::appendable() const noexcept
	{
		return true;
	}
		
	Progress<Length> Device::requestResize(Length length) noexcept
	{
		Progress<Length> result(Action::Resize, CIO_UNKNOWN_LENGTH);

		// Windows appears to only be able to resize to the current file pointer position
		// So let's first capture the current position
		LARGE_INTEGER offset;
		offset.QuadPart = 0;

		LARGE_INTEGER current;
		current.QuadPart = 0;

		BOOL knowPosition = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), offset, &current, FILE_CURRENT);
		if (knowPosition)
		{
			// If this succeeded, we have a regular file rather than a special device, so we can proceed to seek to the desired length
			LARGE_INTEGER target;
			target.QuadPart = length.value;

			// Move to target position...
			BOOL movedPosition = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), target, &current, FILE_BEGIN);
			if (movedPosition)
			{
				// Then set end of file
				BOOL resized = ::SetEndOfFile(reinterpret_cast<HANDLE>(mHandle));
				if (resized)
				{
					result.count = length;
					result.succeed();
				}
				else
				{
					int error = ::GetLastError();
					result.fail(cio::errorWin32(error));
				}

				// Try to move back to original position (shouldn't fail, and not much we can do if it does)
				::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), current, nullptr, FILE_BEGIN);
			}
		}
		else
		{
			result.fail(Reason::Unseekable);
		}

		return result;
	}

	cio::ModeSet Device::openNativePath(Text path, cio::ModeSet modes)
	{
		this->clear();
		cio::ModeSet actualModes = modes;
		DWORD access = 0;

		if (modes.count(cio::Mode::Read))
		{
			access |= GENERIC_READ;
		}

		if (modes.count(cio::Mode::Write))
		{
			access |= GENERIC_WRITE;
		}

		DWORD creation = 0;

		if (modes.count(cio::Mode::Load))
		{
			if (modes.count(cio::Mode::Create))
			{
				if (modes.count(cio::Mode::Replace))
				{
					creation = CREATE_ALWAYS;
				}
				else
				{
					creation = OPEN_ALWAYS;
				}
			}
			else if (modes.count(cio::Mode::Replace))
			{
				creation = TRUNCATE_EXISTING;
			}
			else
			{
				creation = OPEN_EXISTING;
			}
		}
		else if (modes.count(cio::Mode::Create))
		{
			creation = CREATE_NEW;
		}

		DWORD flags = 0;

		if (modes.count(cio::Mode::Temporary))
		{
			flags |= (FILE_FLAG_DELETE_ON_CLOSE | FILE_ATTRIBUTE_TEMPORARY);
		}

		if (modes.count(cio::Mode::Unbuffered))
		{
			flags |= FILE_FLAG_NO_BUFFERING;
		}

		if (modes.count(cio::Mode::Synchronous))
		{
			flags |= FILE_FLAG_WRITE_THROUGH;
		}

		HANDLE hComm;
		hComm = ::CreateFile(path.c_str(),
								access,
								FILE_SHARE_READ,
								0,
								creation,
								flags,
								0);
		// For some modes, the last error tells us what happened even if the function succeeded
		int error = ::GetLastError();

		if (hComm != INVALID_HANDLE_VALUE)
		{
			if (error == ERROR_ALREADY_EXISTS)
			{
				actualModes.erase(cio::Mode::Create);
			}
			else if (actualModes.count(cio::Mode::Create))
			{
				actualModes.erase(cio::Mode::Load);
			}
		}
		else
		{
			actualModes.clear();
			throw Exception(cio::errorWin32(error), "Could not open device");
		}

		mHandle = reinterpret_cast<std::uintptr_t>(hComm);
		mWriteBuffered = false;
		return actualModes;
	}

	void Device::disposeHandle() noexcept
	{
		if (mHandle != CIO_INVALID_DEVICE)
		{
			if (mRemoveOnClose)
			{
				Filesystem fs;
				fs.removeNativeFile(mRemoveOnClose.c_str());
				mRemoveOnClose.clear();
			}

			::CloseHandle(reinterpret_cast<HANDLE>(mHandle));
			mHandle = CIO_INVALID_DEVICE;
			mWriteBuffered = false;
		}
	}

	Length Device::readable() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		DWORD type = ::GetFileType(reinterpret_cast<HANDLE>(mHandle));
		if (type == FILE_TYPE_DISK)
		{
			LARGE_INTEGER length;
			BOOL knowLength = ::GetFileSizeEx(reinterpret_cast<HANDLE>(mHandle), &length);
			if (knowLength)
			{
				LARGE_INTEGER offset;
				offset.QuadPart = 0;

				LARGE_INTEGER current;
				current.QuadPart = 0;

				BOOL knowPosition = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), offset, &current, FILE_CURRENT);
				if (knowPosition)
				{
					result = length.QuadPart - current.QuadPart;
				}
			}
		}

		return result;
	}

	Length Device::writable() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		DWORD type = ::GetFileType(reinterpret_cast<HANDLE>(mHandle));
		if (type == FILE_TYPE_DISK)
		{
			LARGE_INTEGER length;
			BOOL knowLength = ::GetFileSizeEx(reinterpret_cast<HANDLE>(mHandle), &length);
			if (knowLength)
			{
				LARGE_INTEGER offset;
				offset.QuadPart = 0;

				LARGE_INTEGER current;
				current.QuadPart = 0;

				BOOL knowPosition = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), offset, &current, FILE_CURRENT);
				if (knowPosition)
				{
					result = length.QuadPart - current.QuadPart;
				}
			}
		}

		return result;
	}

	Progress<Length> Device::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Seek);

		LARGE_INTEGER offset;
		offset.QuadPart = bytes.value;

		LARGE_INTEGER current;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), offset, &current, FILE_CURRENT);
		if (success)
		{
			result.count = bytes.value;
			result.succeed();
		}
		else
		{
			result = cio::Channel::requestDiscard(bytes);
		}

		return result;
	}

	Progress<Length> Device::requestSkip(Length bytes) noexcept
	{
		Progress<Length> result(Action::Seek, 0, bytes);

		LARGE_INTEGER offset;
		offset.QuadPart = bytes.value;

		LARGE_INTEGER current;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), offset, &current, FILE_CURRENT);
		if (success)
		{
			result.count = bytes;
			result.succeed();
		}
		else
		{
			result = cio::Channel::requestSkip(bytes);
		}

		return result;
	}

	Progress<std::size_t> Device::requestRead(void *buffer, std::size_t length) noexcept
	{
		Progress<std::size_t> result(Action::Read);
		DWORD bytesRead = 0;
		DWORD bytesToRead = static_cast<DWORD>(std::min<std::size_t>(length, MAXDWORD));

		BOOL success = ::ReadFile(
			reinterpret_cast<HANDLE>(mHandle),
			buffer, bytesToRead,
			&bytesRead,
			nullptr);

		result.count = bytesRead;

		if (success)
		{
			if (bytesRead > 0)
			{
				result.succeed();
			}
			else
			{
				result.fail(Reason::Underflow);
			}
		}
		else
		{
			int error = ::GetLastError();
			result.fail(cio::errorWin32(error));
		}

		return result;
	}

	Progress<std::size_t> Device::requestReadFromPosition(void *buffer, std::size_t length, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Read, 0, length);
		DWORD bytesRead = 0;
		DWORD bytesToRead = static_cast<DWORD>(std::min<std::size_t>(length, MAXDWORD));

		Length actual = position;
		if (mode != Seek::Begin)
		{
			actual = this->calculateReadPosition(position, mode);
		}

		if (actual.known() && actual.forward())
		{
			OVERLAPPED offset;
			std::memset(&offset, 0, sizeof(offset));
			offset.Offset = static_cast<std::uint32_t>(actual.value);
			offset.OffsetHigh = static_cast<std::uint32_t>(actual.value >> 32);

			BOOL success = ::ReadFile(
				reinterpret_cast<HANDLE>(mHandle),
				buffer, bytesToRead,
				&bytesRead,
				&offset);

			result.count = bytesRead;

			if (success)
			{
				if (bytesRead > 0)
				{
					result.succeed();
				}
				else
				{
					result.fail(Reason::Underflow);
				}
			}
			else
			{
				int error = ::GetLastError();
				result.fail(cio::errorWin32(error));
			}
		}
		else
		{
			result.fail(Reason::Invalid);
		}

		return result;
	}

	Length Device::getReadPosition() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;

		LARGE_INTEGER param;
		param.QuadPart = 0;

		LARGE_INTEGER output;
		output.QuadPart = 0;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, FILE_CURRENT);
		if (success)
		{
			result = output.QuadPart;
		}

		return result;
	}

	Resize Device::isReadSeekable() const noexcept
	{
		Resize result = Resize::None;

		LARGE_INTEGER param;
		param.QuadPart = 0;

		LARGE_INTEGER output;
		output.QuadPart = 0;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, FILE_CURRENT);

		if (success)
		{
			result = Resize::Any;
		}

		return result;
	}

	Progress<Length> Device::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);

		DWORD method;
		LARGE_INTEGER param;

		switch (mode)
		{
			case Seek::None:
				method = FILE_BEGIN;
				param.QuadPart = 0;
				break;

			case Seek::Begin:
				method = FILE_BEGIN;
				param.QuadPart = position.value;
				break;

			case Seek::Current:
				method = FILE_CURRENT;
				param.QuadPart = position.value;
				break;

			case Seek::End:
				method = FILE_END;
				param.QuadPart = position.value;
				break;

			default:
				method = FILE_CURRENT;
				param.QuadPart = 0;
				break;
		}

		LARGE_INTEGER output;
		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, method);
		if (success)
		{
			result.count = output.QuadPart;
			result.succeed();
		}
		else
		{
			result.fail(Reason::Unsupported);
		}

		return result;
	}

	Progress<std::size_t> Device::requestWrite(const void *buffer, std::size_t length) noexcept
	{
		Progress<std::size_t> result(Action::Write, 0, length);
		DWORD bytesWritten = 0;
		DWORD bytesToWrite = static_cast<DWORD>(std::min<std::size_t>(length, MAXDWORD));

		bool success = ::WriteFile(
			reinterpret_cast<HANDLE>(mHandle),
			buffer, bytesToWrite,
			&bytesWritten,
			nullptr
		);

		result.count = bytesWritten;

		if (success)
		{
			result.succeed();
		}
		else
		{
			int error = ::GetLastError();
			result.fail(cio::errorWin32(error));
		}

		return result;
	}

	Progress<std::size_t> Device::requestWriteToPosition(const void *buffer, std::size_t length, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Write);
		DWORD bytesWritten = 0;
		DWORD bytesToWrite = static_cast<DWORD>(std::min<std::size_t>(length, MAXDWORD));

		Length actual = position;
		if (mode != Seek::Begin)
		{
			actual = this->calculateWritePosition(position, mode);
		}

		if (actual.known() && actual.forward())
		{
			OVERLAPPED offset;
			std::memset(&offset, 0, sizeof(offset));
			offset.Offset = static_cast<std::uint32_t>(actual.value);
			offset.OffsetHigh = static_cast<std::uint32_t>(actual.value >> 32);

			bool success = ::WriteFile(
				reinterpret_cast<HANDLE>(mHandle),
				buffer, bytesToWrite,
				&bytesWritten,
				&offset
			);

			result.count = bytesWritten;

			if (success)
			{
				result.succeed();
			}
			else
			{
				int error = ::GetLastError();
				result.fail(cio::errorWin32(error));
			}
		}
		else
		{
			result.fail(Reason::Invalid);
		}

		return result;
	}

	Length Device::getWritePosition() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;

		LARGE_INTEGER param;
		param.QuadPart = 0;

		LARGE_INTEGER output;
		output.QuadPart = 0;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, FILE_CURRENT);
		if (success)
		{
			result = output.QuadPart;
		}

		return result;
	}

	Resize Device::isWriteSeekable() const noexcept
	{
		Resize result = Resize::None;

		LARGE_INTEGER param;
		param.QuadPart = 0;

		LARGE_INTEGER output;
		output.QuadPart = 0;

		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, FILE_CURRENT);

		if (success)
		{
			result = Resize::Any;
		}

		return result;
	}

	Progress<Length> Device::requestSeekToWrite(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);

		LARGE_INTEGER param;
		DWORD method;

		switch (mode)
		{
			case Seek::None:
				method = FILE_BEGIN;
				param.QuadPart = 0;
				break;

			case Seek::Begin:
				method = FILE_BEGIN;
				param.QuadPart = position.value;
				break;

			case Seek::Current:
				method = FILE_CURRENT;
				param.QuadPart = position.value;
				break;

			case Seek::End:
				method = FILE_END;
				param.QuadPart = position.value;
				break;

			default:
				method = FILE_CURRENT;
				param.QuadPart = 0;
				break;
		}

		LARGE_INTEGER output;
		BOOL success = ::SetFilePointerEx(reinterpret_cast<HANDLE>(mHandle), param, &output, method);
		if (success)
		{
			result.count = output.QuadPart;
			result.succeed();
		}
		else
		{
			result.fail(Reason::Unsupported);
		}

		return result;
	}

	Progress<Traversal> Device::flush(Traversal depth) noexcept
	{
		Progress<Traversal> result(Action::Write, Traversal(), depth);
		
		if (depth)
		{
			// This only fails if the device is an unbuffered console, so it's a success either way, only difference in outcome is depth
			bool succeeded = ::FlushFileBuffers(reinterpret_cast<HANDLE>(mHandle));
			if (succeeded)
			{
					result.count = Traversal::Immediate;
			}
		}

		result.succeed();
		return result;
	}

#elif defined CIO_HAVE_UNISTD_H

	bool Device::linkedReadWritePosition() const noexcept
	{
		// Load/store position is linked for files that are seekable
		// So attempt a no-op seek and determine if it fails
		return ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR) >= 0;
	}
		
	Resize Device::resizable() const noexcept
	{
		off_t current = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		return current >= 0 ? Resize::Any : Resize::Grow;
	}
				
	bool Device::appendable() const noexcept
	{
		return true;
	}
		
	Length Device::size() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		struct stat value;
		int status = ::fstat(static_cast<int>(mHandle), &value);

		if (status == 0)
		{
			// stat::st_size is only set for regular files and symbolic links
			if (S_ISREG(value.st_mode) || S_ISLNK(value.st_mode))
			{
				result = value.st_size;
			}
			// anything else that's seekable (like raw devices) can be determined by seeking to the end
			else
			{
				// Save current position
				off_t current = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
				if (current >= 0)
				{
					// Get end
					off_t end = ::lseek(static_cast<int>(mHandle), 0, SEEK_END);
					if (end >= 0)
					{
						result = end - current;
					}

					// Seek back to original position
					::lseek(static_cast<int>(mHandle), current, SEEK_SET);
				}
			}
		}

		return result;
	}

	Progress<Length> Device::requestResize(Length length) noexcept
	{
		Progress<Length> result(Action::Resize, CIO_UNKNOWN_LENGTH);
		int success = ::ftruncate(static_cast<int>(mHandle), length.offset());
		if (success == 0)
		{
			result.count = length;
			result.succeed();
		}
		else
		{
			int error = errno;
			result.fail(cio::error(error));
		}

		return result;
	}

	cio::ModeSet Device::openNativePath(Text path, cio::ModeSet modes)
	{
		this->clear();
		cio::ModeSet actualModes = modes;
		int openMode = 0;

		if (modes.count(cio::Mode::Read))
		{
			if (modes.count(cio::Mode::Write))
			{
				openMode = O_RDWR;
			}
			else
			{
				openMode = O_RDONLY;
			}
		}
		else if (modes.count(cio::Mode::Write))
		{
			openMode = O_WRONLY;
		}

		int openFlags = O_NOCTTY;

		if (modes.count(cio::Mode::Unbuffered))
		{
			openFlags |= O_DIRECT;
		}

		if (modes.count(cio::Mode::Synchronous))
		{
			openFlags |= O_SYNC;
		}

		if (modes.count(cio::Mode::Create))
		{
			openFlags |= O_CREAT;

			if (!modes.count(cio::Mode::Load))
			{
				openFlags |= O_EXCL;
			}
		}

		if (modes.count(cio::Mode::Replace))
		{
			openFlags |= O_TRUNC;
		}

		int handle = ::open(path.c_str(), openMode | openFlags, S_IRWXU);

		if (handle == -1)
		{
			throw std::system_error(errno, std::system_category());
		}

		mHandle = static_cast<std::uintptr_t>(handle);
		mWriteBuffered = false;
		return actualModes;
	}

	void Device::disposeHandle() noexcept
	{
		if (mHandle != CIO_INVALID_DEVICE)
		{
			if (mRemoveOnClose)
			{
				cio::Filesystem fs;
				fs.removeNativeFile(mRemoveOnClose.c_str());
				mRemoveOnClose.clear();
			}

			::close(static_cast<int>(mHandle));
			mHandle = CIO_INVALID_DEVICE;
		}
	}

	Progress<std::size_t> Device::requestRead(void *buffer, std::size_t length) noexcept
	{
		Progress<std::size_t> result(Action::Read);

		ssize_t bytes = ::read(static_cast<int>(mHandle), buffer, length);

		if (bytes > 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else if (bytes == 0)
		{
			// EOF is reached
			result.fail(Reason::Underflow);
		}
		else
		{
			int error = errno;
			result.fail(cio::error(error));
		}

		return result;
	}

	Progress<std::size_t> Device::requestReadFromPosition(void *buffer, std::size_t length, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Read);

		Length actual = position;
		if (mode != Seek::Begin)
		{
			actual = this->calculateReadPosition(position, mode);
		}

		ssize_t bytes = ::pread(static_cast<int>(mHandle), buffer, length, actual.offset());

		if (bytes > 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else if (bytes == 0)
		{
			// technically a success but should only happen when length is 0
			result.complete();
		}
		else
		{
			int error = errno;
			result.fail(cio::error(error));
		}

		return result;
	}

	Length Device::readable() const noexcept
	{
		Length count = CIO_UNKNOWN_LENGTH;

		// First get current offset
		off_t current = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (current >= 0)
		{
			// Now get size
			struct stat metadata;
			int success = ::fstat(static_cast<int>(mHandle), &metadata);
			if (success == 0)
			{
				// stat::st_size is only set for regular files and symbolic links
				if (S_ISREG(metadata.st_mode) || S_ISLNK(metadata.st_mode))
				{
					count = static_cast<std::uint64_t>(metadata.st_size - current);
				}
				// anything else that's seekable (like raw devices) can be determined by seeking to the end
				else
				{
					// Get end
					off_t end = ::lseek(static_cast<int>(mHandle), 0, SEEK_END);
					if (end >= 0)
					{
						count = static_cast<std::uint64_t>(end - current);
					}

					// Seek back to original position
					::lseek(static_cast<int>(mHandle), current, SEEK_SET);
				}
			}
		}

		return count;
	}

	Length Device::getReadPosition() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		off_t value = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (value >= 0)
		{
			result = static_cast<std::int64_t>(value);
		}
		return result;
	}

	Resize Device::isReadSeekable() const noexcept
	{
		Resize result = Resize::None;
		off_t value = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (value >= 0)
		{
			result = Resize::Any;
		}
		return result;
	}

	Progress<Length> Device::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);
		int method;
		Length parameter;
		switch (mode)
		{
			case Seek::None:
				method = SEEK_SET;
				parameter = 0;
				break;
					
			case Seek::Begin:
				method = SEEK_SET;
				parameter = position;
				break;
					
			case Seek::Current:
				method = SEEK_CUR;
				parameter = position;
				break;
					
			case Seek::End:
				method = SEEK_END;
				parameter = position;
				break;
					
			default:
				method = SEEK_CUR;
				parameter = 0;
				break;
		}
			
		off_t value = ::lseek(static_cast<int>(mHandle), parameter.offset(), method);
		if (value >= 0)
		{
			result.count = static_cast<std::uint64_t>(value);
			result.succeed();
		}
		else
		{
			int error = errno;
			if (error == ESPIPE)
			{
				result.count = CIO_UNKNOWN_LENGTH;
				result.fail(Reason::Unsupported);
			}
			else if (error == EINVAL)
			{
				result.count = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
				result.fail(Reason::Overflow);
			}
			else
			{
				result.fail(cio::error(error));
			}
		}

		return result;
	}

	Progress<Length> Device::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Read);
		off_t current = ::lseek(static_cast<int>(mHandle), bytes.offset(), SEEK_CUR);
		if (current >= 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else
		{
			// device is not seekable, go for sequential discard from base class
			result = cio::Channel::requestDiscard(bytes);
		}
		return result;
	}

	Progress<std::size_t> Device::requestWrite(const void *buffer, std::size_t length) noexcept
	{
		Progress<std::size_t> result(Action::Write);

		ssize_t bytes = ::write(static_cast<int>(mHandle), buffer, length);

		if (bytes > 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else if (bytes == 0)
		{
			// technically a success but should only happen when length is 0
			result.complete();
		}
		else
		{
			int error = errno;
			result.fail(cio::error(error));
		}

		return result;
	}

	Progress<std::size_t> Device::requestWriteToPosition(const void *buffer, std::size_t length, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Write);
			
		Length actual = position;
		if (mode != Seek::Begin)
		{
			actual = this->calculateWritePosition(position, mode);
		}

		ssize_t bytes = ::pwrite(static_cast<int>(mHandle), buffer, length, actual.offset());

		if (bytes > 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else if (bytes == 0)
		{
			// technically a success but should only happen when length is 0
			result.complete();
		}
		else
		{
			int error = errno;
			result.fail(cio::error(error));
		}

		return result;
	}

	Length Device::writable() const noexcept
	{
		Length count = CIO_UNKNOWN_LENGTH;

		// First get current offset
		off_t current = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (current >= 0)
		{
			// Now get size
			struct stat metadata;
			int success = ::fstat(static_cast<int>(mHandle), &metadata);
			if (success == 0)
			{
				// stat::st_size is only set for regular files and symbolic links
				if (S_ISREG(metadata.st_mode) || S_ISLNK(metadata.st_mode))
				{
					count = static_cast<std::uint64_t>(metadata.st_size - current);
				}
				// anything else that's seekable (like raw devices) can be determined by seeking to the end
				else
				{
					// Get end
					off_t end = ::lseek(static_cast<int>(mHandle), 0, SEEK_END);
					if (end >= 0)
					{
						count = static_cast<std::uint64_t>(end - current);
					}

					// Seek back to original position
					::lseek(static_cast<int>(mHandle), current, SEEK_SET);
				}
			}
		}

		return count;
	}

	Length Device::getWritePosition() const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;
		off_t value = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (value >= 0)
		{
			result = static_cast<std::int64_t>(value);
		}
		return result;
	}

	Resize Device::isWriteSeekable() const noexcept
	{
		Resize result = Resize::None;
		off_t value = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
		if (value >= 0)
		{
			result = Resize::Any;
		}
		return result;
	}

	Progress<Length> Device::requestSeekToWrite(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);
		int method;
		Length parameter;
		switch (mode)
		{
			case Seek::None:
				method = SEEK_SET;
				parameter = 0;
				break;
					
			case Seek::Begin:
				method = SEEK_SET;
				parameter = position;
				break;
					
			case Seek::Current:
				method = SEEK_CUR;
				parameter = position;
				break;
					
			case Seek::End:
				method = SEEK_END;
				parameter = position;
				break;
					
			default:
				method = SEEK_CUR;
				parameter = 0;
				break;
		}
			
		off_t value = ::lseek(static_cast<int>(mHandle), parameter.offset(), method);
		if (value >= 0)
		{
			result.count = static_cast<std::int64_t>(value);
			result.succeed();
		}
		else
		{
			int error = errno;
			if (error == ESPIPE)
			{
				result.count = CIO_UNKNOWN_LENGTH;
				result.fail(Reason::Unsupported);
			}
			else if (error == EINVAL)
			{
				result.count = ::lseek(static_cast<int>(mHandle), 0, SEEK_CUR);
				result.fail(Reason::Overflow);
			}
			else
			{
				result.fail(cio::error(error));
			}
		}

		return result;
	}

	Progress<Length> Device::requestSkip(Length bytes) noexcept
	{
		Progress<Length> result(Action::Write);
		off_t current = ::lseek(static_cast<int>(mHandle), bytes.offset(), SEEK_CUR);
		if (current >= 0)
		{
			result.count = bytes;
			result.succeed();
		}
		else
		{
			// device is not seekable, go for sequential skip from base class
			result = cio::Channel::requestSkip(bytes);
		}
		return result;
	}

	Progress<Traversal> Device::flush(Traversal depth) noexcept
	{
		Progress<Traversal> result(Action::Write, Traversal(), depth);
		if (depth)
		{
			int success = ::fsync(static_cast<int>(mHandle));

			if (success == 0)
			{
				result.count = Traversal::Immediate;
				result.succeed();
			}
			else
			{
				result.fail(cio::error());
			}
		}
		else
		{
			result.complete();
		}
		
		return result;
	}

#else
#error "No cio::Device backend available on your platform, please implement one"
#endif
}
