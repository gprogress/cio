/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Resource.h"

#include <cio/Case.h>

#include <istream>
#include <ostream>

namespace cio
{
	namespace
	{
		const char *sResourceText[] =
		{
			"None",
			"Protocol",
			"File",
			"Directory",
			"Archive",
			"Device",
			"Link",
			"Network",
			"Memory",
			"Virtual",
			"Unknown"
		};
	}

	const char *print(Resource mode) noexcept
	{
		return sResourceText[static_cast<unsigned>(mode)];
	}

	Resource parseType(const char *text) noexcept
	{
		Resource mode = Resource::Unknown;

		for (unsigned i = 0; i < sizeof(sResourceText) / sizeof(const char *); ++i)
		{
			if (CIO_STRCASECMP(sResourceText[i], text) == 0)
			{
				mode = static_cast<Resource>(i);
				break;
			}
		}

		return mode;
	}

	Resource parseType(const std::string &text) noexcept
	{
		return parseType(text.c_str());
	}

	std::ostream &operator<<(std::ostream &stream, Resource mode)
	{
		return (stream << print(mode));
	}

	std::istream &operator>>(std::istream &stream, Resource &mode)
	{
		std::string chunk;
		stream >> chunk;
		mode = parseType(chunk);
		return stream;
	}
}
