/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEXTREADERTEST_H
#define CIO_TEXTREADERTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	* Tests the basic functionality of a text reader.
	*/
	class TextReaderTest : public cio::test::Suite
	{
		public:
			TextReaderTest();
			
			virtual ~TextReaderTest() noexcept;

			void testConstruction();

			void testAssignment();

			void testToken();

			void testTokenSkipWhitespace();

			void testReset();

			void testClear();
	};
}

#endif
