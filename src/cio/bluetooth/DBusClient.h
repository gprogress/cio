/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_DBUSCLIENT_H
#define CIO_BLUETOOTH_DBUSCLIENT_H

#include "Types.h"

#include <cio/Path.h>

#include <atomic>
#include <map>
#include <mutex>
#include <vector>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO D-Bus Client provides a common implementation for using the D-Bus system bus for performing BlueZ queries.
		 * It is not intended to be a general-purpose D-Bus implementation (D-Bus is more of a data model than just an I/O library)
		 * but provides the basic functionality needed by the D-Bus Controller and D-Bus Attribute Client.
		 */
		class CIO_BLUETOOTH_API DBusClient
		{
			public:				
				/** The D-Bus core domain */
				static const char *DBUS_DOMAIN;
			
				/** The core Object Management interface name */
				static const char *DBUS_OBJECTMANAGEMENT_IFACE;
			
				/** The core Properties interface name */
				static const char *DBUS_PROPERTIES_IFACE;
						
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Instructs D-Bus to enable multi-threading.
				 * This is automatically called once the first time a client is constructed, but
				 * can also be called manually before that if desired.
				 *
				 * Without this step, D-Bus cannot be used by more than one thread for any reason,
				 * and this tends to lead to odd errors in Release builds.
				 *
				 * @return whether D-Bus reported that multi-threading succeeded
				 */
				static bool enableThreads() noexcept;
				
				/**
				 * Construct an unconnected D-Bus client.
				 */
				DBusClient() noexcept;
				
				/**
				 * Construct an unconnected D-Bus client with the given domain.
				 *
				 * @param domain The domain
				 */
				explicit DBusClient(const char *domain) noexcept;
				
				/**
				 * Copies a D-Bus Client.
				 *
				 * @param in The client to copy
				 */
				DBusClient(const DBusClient &in);
				
				/**
				 * Constructs a Bluetooth D-Bus Client by adopting the state of a given client.
				 * 
				 * @param in The client to move
				 */
				DBusClient(DBusClient &&in) noexcept;
								
				/**
				 * Copies a D-Bus Client.
				 *
				 * @param in The client to move
				 * @return this client
				 */
				DBusClient &operator=(const DBusClient &in) noexcept;
				
				/**
				 * Moves a D-Bus Client.
				 *
				 * @param in The client to move
				 * @return this client
				 */
				DBusClient &operator=(DBusClient &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				~DBusClient() noexcept;
				
				/**
				 * Clears the client and unreferences the system bus connection.
				 */				
				void clear() noexcept;
				
				// Connection management
				
				/**
				 * Opens a connection to the system bus.
				 */
				void open();
				
				/**
				 * Opens a connection to the system bus and sets this client for the given domain.
				 *
				 * @param domain The domain
				 */
				void open(const char *domain);
				
				/**
				 * Checks whether the system bus is open and the domain is set.
				 *
				 * @return whether this client is opn
				 */
				bool isOpen() const noexcept;
				
				// D-Bus specific methods
				
				/**
				 * Gets the connection to the D-Bus system bus.
				 *
				 * @return the D-Bus system connection
				 */
				DBusConnection *getConnection() const noexcept;
				
				/**
				 * Gets the connection to the D-Bus system bus,
				 * creating a new connection if none exists.
				 *
				 * @return the D-Bus system connection
				 */
				DBusConnection *getOrCreateConnection();
				
				/**
				 * Sets the connection to the D-Bus system bus.
				 * This controller will take ownership of the connection.
				 *
				 * @param connection The D-Bus system connection to use
				 */
				void setConnection(DBusConnection *connection) noexcept;
				
				/**
				 * Sets the domain to use for all messages.
				 * For example for BlueZ this would be "org.bluez".
				 *
				 * @param domain The domain
				 */
				void setDomain(const char *domain) noexcept;
				
				/**
				 * Gets the domain used for all messages.
				 *
				 * @return the domain
				 */
				const char *getDomain() const noexcept;
				
				/**
				 * Uses the core DBus Object Management interface to obtain all objects matching the given interface
				 * at the root of the current domain.
				 *
				 * @param iface The interface to match
				 * @return the object paths of all objects that implement the given interface
				 */
				std::vector<cio::Path> findMatchingObjects(const char *iface) const;
				
				/**
				 * Uses the core DBus Object Management interface to obtain all objects matching any of the given interfaces
				 * at the root of the current domain. The objects are returned as a map by interface type.
				 * If an object matches multiple interfaces, it will be present multiple times.
				 *
				 * @param ifaces The interfaces to match
				 * @param count The number of interfaces to match
				 * @return the map of interfaces to object paths of all objects that implement the given interface
				 */
				std::map<std::string, std::vector<cio::Path>> findMatchingObjects(const char **ifaces, std::size_t count) const;
				
				/**
				 * Uses the core DBus Object Management interface to obtain all objects matching any of the given interfaces
				 * at the root of the current domain. The objects are provided to the given callback with both the interface
				 * and object path. This is the most efficient version of this method and all other versions are implemented in terms of it.
				 * If an object matches multiple interfaces, it will be provided to the receiver once for each interface.
				 *
				 * @param ifaces The interfaces to match
				 * @param count The number of interfaces to match
				 * @param receiver The function to provide each object matching one of the interfaces
				 */
				void findMatchingObjects(const char **ifaces, std::size_t count, const std::function<void (const char *object, const char *iface)> & receiver) const;
				
				/**
				 * Uses the core D-Bus Properties interface to obtain the first property value from an object.
				 *
				 * @warning This only handles a subset of value types as needed to support other methods.
				 *
				 * @param object The object path
				 * @param iface The property interface
				 * @param property The property name
				 * @return the property value
				 */
				Variant getProperty(const char *object, const char *iface, const char *property) const;
				
				/**
				 * Uses the core D-Bus Properties interface to obtain property values from an object.
				 *
				 * If the property is a scalar, the single value will be provided as a variant to the listener.
				 * If the property is an array, each element will be provided as a variant to the listener.
				 *
				 * @warning This only handles a subset of value types as needed to support other methods.
				 *
				 * @param object The object path
				 * @param iface The property interface
				 * @param property The property name
				 * @param receiver The listener to receive property values
				 * @return the number of property values processed
				 */
				std::size_t getProperty(const char *object, const char *iface, const char *property, const std::function<void (const char *property, const Variant &value)> &receiver) const;

				/**
				 * Calls a D-Bus method on the given interface of the given object that takes no parameters and returns no results.
				 *
				 * @param object The object path
				 * @param iface The interface name
				 * @param command The method name
				 * @return the status of the invocation
				 */
				State invokeCommand(const char *object, const char *iface, const char *command);
				
				/**
				 * Executes the steps necessary to do a direct GATT attribute read on a compatible D-Bus object interface.
				 * This can be used on either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1 interface.
				 * The actual invocation is somewhat nontrivial since it has to build out the option dictionary as well.
				 *
				 * @param object The path to the D-Bus object (either a characteristic or descriptor)
				 * @param iface The interface of the D-Bus object of relevance (either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1)
				 * @param handle The handle of the value being read, which should be the value handle (not the characteristic handle) or the descriptor handle
				 * @param listener The listener to receive the data that was read
				 * @param offset The initial read offset
				 * @return the number of bytes actually read and status thereof
				 */
				Progress<std::size_t> executeGattRead(const char *object, const char *iface, std::uint16_t handle, const AttributeCallback &listener, std::size_t offset) const;
				
				/**
				 * Executes the steps necessary to do a direct GATT attribute write with confirmation on a compatible D-Bus object interface.
				 * This can be used on either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1 interface.
				 * The actual invocation is somewhat nontrivial since it has to build out the option dictionary as well.
				 *
				 * @param object The path to the D-Bus object (either a characteristic or descriptor)
				 * @param iface The interface of the D-Bus object of relevance (either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1)
				 * @param buffer The buffer of data to write
				 * @param requested The number of bytes to write
				 * @param offset The initial write offset
				 * @return the number of bytes actually written and status thereof
				 */
				Progress<std::size_t> executeGattWrite(const char *object, const char *iface, const void *buffer, std::size_t requested, std::size_t offset);
				
				/**
				 * Executes the steps necessary to do a direct GATT attribute write without confirmation on a compatible D-Bus object interface.
				 * This can be used on either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1 interface.
				 * The actual invocation is somewhat nontrivial since it has to build out the option dictionary as well.
				 *
				 * @param object The path to the D-Bus object (either a characteristic or descriptor)
				 * @param iface The interface of the D-Bus object of relevance (either org.bluez.GattCharacteristic1 or org.bluez.GattDescriptor1)
				 * @param buffer The buffer of data to write
				 * @param requested The number of bytes to write
				 * @return the number of bytes submitted to write and status thereof
				 */
				Progress<std::size_t> executeGattSend(const char *object, const char *iface, const void *buffer, std::size_t requested);
				
				/**
				 * Listens to D-Bus for incoming signals that represent property updates.
				 * Each propert update is decoded - which is a very nontrivial operation - and then given to the receiver with information about
				 * the D-Bus object path, the relevant D-Bus interface, the property name, and the property binary value.
				 *
				 * @warning Currently this only works correctly with text and byte array properties, which is what we need for GATT support.
				 *
				 * @param receiver The receiver to get signal updates
				 * @param exec The executor to controll timeout and cancellation
				 * @return the number of property updates processed
				 */
				Progress<std::size_t> listenForProperties(const std::function<void (const char *object, const char *iface, const char *property, const void *value, std::size_t length)> &receiver, Task &exec) const;
				
				/**
				 * Checks whether the client is valid.
				 * This is true if the system bus is connected.
				 *
				 * @return whether this client is valid
				 */
				explicit operator bool() const noexcept;
				
				/**
				 * Sets up the D-Bus connection to listen for the given interface's signal for a given object in the current domain.
				 * Internally this compiles the parameters into a D-Bus match rule and then adds that to the bus.
				 *
				 * @param object The object path
				 * @param iface The interface
				 * @param name The signal name
				 */ 
				void subscribe(const char *object, const char *iface, const char *name);
				
				/**
				 * Sets up the D-Bus connection to listen for the given interface's signal for a given object in the current domain.
				 * Internally this compiles the parameters into a D-Bus match rule and then removes that from the bus.
				 *
				 * @param object The object path
				 * @param iface The interface
				 * @param name The signal name
				 */ 
				void unsubscribe(const char *object, const char *iface, const char *name);
				
				/**
				 * Prints a match rule text in the format D-Bus expects.
				 *
				 * @param object The object path
				 * @param iface The interface
				 * @param name The signal name
				 * @param rule The text buffer to print the rule to
				 * @param length The number of bytes available in the text buffer
				 * @return the actual number of bytes needed for the rule text
				 */
				std::size_t printMatchRule(const char *object, const char *iface, const char *name, char *rule, std::size_t length) noexcept;
				
				/**
				 * Convenience method to subscribe for the standard D-Bus property update interface.
				 * Internally this calls the more general subscribe with the correct parameters.
				 *
				 * @param object The object path
				 */
				void subscribePropertyUpdates(const char *object);
				
				/**
				 * Convenience method to unsubscribe from the standard D-Bus property update interface.
				 * Internally this calls the more general unsubscribe with the correct parameters.
				 *
				 * @param object The object path
				 */
				void unsubscribePropertyUpdates(const char *object);
				
			private:
				/** The metaclass for this class */
				static Class<DBusClient> sMetaclass;
				
				/** Atomic flag tracking whether multi-threading has been enabled */
				static std::atomic<unsigned> sThreaded;
				
				/** Mutex guarding D-Bus calls */
				static std::mutex sMutex;
				
				/** D-Bus system bus */
				DBusConnection *mSystemBus;
				
				/** The current domain */
				const char *mDomain;
		};
	}
}

#endif

