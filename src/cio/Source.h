/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_SOURCE_H
#define CIO_SOURCE_H

#include "Types.h"

#include <string>
#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Source class represents a resource location used for formatted text or binary locations.
	 * It is a lightweight class intended to be used for test frameworks and parsing.
	 */
	class CIO_API Source
	{
		public:
			/**
			 * Construct an empty source.
			 */
			inline Source();

			/**
			 * Construct a source that references the given URI.
			 * The line and column are initialized to -1.
			 *
			 * @param file The URI
			 */
			inline explicit Source(const char *file);
			
			/**
			 * Construct a source that references the given URI and line number.
			 * The column is initialized to -1.
			 *
			 * @param file The URI
			 * @param line The line number
			 */
			inline Source(const char *file, std::int64_t line);
			
			/**
			 * Construct a source that references the given URI, line, and column.
			 *
			 * @param file The URI
			 * @param line The line number
			 * @param column The column number
			 */
			inline Source(const char *file, std::int64_t line, std::int64_t column);

			/**
			 * Copy constructor for Source.
			 *
			 * @param in The source to copy
			 */
			inline Source(const Source &in) = default;
			
			/**
			 * Copy assignment for Source.
			 *
			 * @param in The source to copy
			 * @return this source
			 */
			inline Source &operator=(const Source &in) = default;

			/**
			 * Destructor.
			 */
			inline ~Source() = default;
			
			/**
			 * Clears the source information, sets line and column number to -1.
			 */
			inline void clear();

			/**
			 * Gets the resource URI.
			 *
			 * @return the resource URI
			 */
			inline const char *getFile() const;
			
			/**
			 * Sets the resource URI.
			 * It is referenced rather than copied or owned.
			 *
			 * @param file the resource URI
			 */
			inline void setFile(const char *file);

			/**
			 * Gets the line number.
			 *
			 * @return the line number.
			 */
			inline std::int64_t getLine() const;

			/*
			 * Sets the line number.
			 *
			 * @param line The line number
			 */
			inline void setLine(std::int64_t line);

			/*
			 * Increments the line number by one and resets
			 * the column number to zero.
			 */
			inline void advanceLine();

			/*
			 * Increments the column number by one.
			 */
			inline void advanceColumn();

			/**
			 * Gets the column number.
			 *
			 * @return the column number.
			 */
			inline std::int64_t getColumn() const;

			/**
			 * Sets the line number.
			 *
			 * @param column The column number.
			 */
			inline void setColumn(std::int64_t column);

			/**
			 * Prints a text representation of this source location.
			 *
			 * @return the source location
			 * @throw std::bad_alloc If memory allocation failed
			 */
			std::string print() const;

		private:
			/** The resource URI */
			const char *mFile;
			
			/** The line number */
			std::int64_t mLine;
			
			/** The column number */
			std::int64_t mColumn;
	};

	/**
	 * Prints the Source information to a C++ stream.
	 *
	 * @param s The stream
	 * @param source The source information
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, const Source &source);
}

/**
 * Macro to capture the C++ file and line number at the point it is used into a Source object.
 */
#define CIO_CURRENT_SOURCE() cio::Source(__FILE__, __LINE__)

/* Inline implementation */

namespace cio
{
	inline Source::Source() :
		mFile(nullptr),
		mLine(-1),
		mColumn(-1)
	{
		// nothing more to do
	}

	inline Source::Source(const char *file) :
		mFile(file),
		mLine(-1),
		mColumn(-1)
	{
		// nothing more to do
	}

	inline Source::Source(const char *file, std::int64_t line) :
		mFile(file),
		mLine(line),
		mColumn(-1)
	{
		// nothing more to do
	}

	inline Source::Source(const char *file, std::int64_t line, std::int64_t column) :
		mFile(file),
		mLine(line),
		mColumn(column)
	{
		// nothing more to do
	}

	inline void Source::clear()
	{
		mFile = nullptr;
		mLine = -1;
		mColumn = -1;
	}

	inline const char *Source::getFile() const
	{
		return mFile;
	}

	inline void Source::setFile(const char *file)
	{
		mFile = file;
	}

	inline std::int64_t Source::getLine() const
	{
		return mLine;
	}

	inline void Source::setLine(std::int64_t line)
	{
		mLine = line;
	}

	inline void Source::advanceLine()
	{
		++mLine;
		mColumn = 0;
	}

	inline void Source::advanceColumn()
	{
		++mColumn;
	}

	inline std::int64_t Source::getColumn() const
	{
		return mColumn;
	}

	inline void Source::setColumn(std::int64_t column)
	{
		mColumn = column;
	}
}

#endif
