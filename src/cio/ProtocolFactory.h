/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PROTOCOLFACTORY_H
#define CIO_PROTOCOLFACTORY_H

#include "Types.h"

#include "Metaclass.h"
#include "Protocol.h"
#include "Text.h"

#include <functional>
#include <map>
#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Protocol Factory is a class to obtain and use Protocol instances
	 * for particular resource URIs.
	 *
	 * The Protocol Factory itself also implements the Protocol interface to look up and create
	 * the proper protocol on each interface method, which may be convenient but is less efficient than
	 * obtaining the correct protocol first.
	 */
	class CIO_API ProtocolFactory : public Protocol
	{
		public:			
			/**
			 * Gets the declared metaclass for the Protocol Factory itself.
			 *
			 * @return the declared metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Gets access to a shared global default Protocol Factory.
			 * This factory is pre-populated with the protocols known to CIO at compile time,
			 * plus any additional protocols added to it by application code.
			 *
			 * @return the default global protocol factory
			 */
			inline static ProtocolFactory *getDefault() noexcept;

			/**
			 * Constructor for an empty protocol factory.
			 */
			ProtocolFactory() noexcept;

			/**
			 * Constructs a copy of a protocol factory.
			 *
			 * @param in The protocol factory to copy
			 */
			ProtocolFactory(const ProtocolFactory &in);

			/**
			 * Constructs a copy of a protocol factory by transferring the contents of an existing one.
			 *
			 * @param in The factory to move
			 */
			ProtocolFactory(ProtocolFactory &&in) noexcept;

			/**
			 * Copies a protocol factory.
			 *
			 * @param in The protocol factory to copy
			 * @return this protocol factory
			 */
			ProtocolFactory &operator=(const ProtocolFactory &in);

			/**
			 * Moves a protocol factory.
			 *
			 * @param in The protocol factory to move
			 * @return this protocol factory
			 */
			ProtocolFactory &operator=(ProtocolFactory &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~ProtocolFactory() noexcept override;

			/**
			 * Gets the runtime metaclass for this object.
			 *
			 * @return the runtime metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears the protocol factory of all protocols and metaclasses.
			 */
			virtual void clear() noexcept override;

			/**
			 * Creates the Protocol subclass instance best suited to handle the given URI path.
			 * This can be used to get a writable protocol even on a read-only protocol factory.
			 *
			 * @param path The URI of the resource of interest
			 * @param type The desired type of resource, which may modify which protocol is used
			 * @return the allocated Protocol subclass
			 * @throw std::bad_alloc If the memory could not be allocated
			 * @throw Exception If no protocol could handle the path or creating it failed
			 */
			virtual std::unique_ptr<Protocol> getProtocol(const Path &path, Resource type = Resource::Unknown) const;

			/**
			 * Creates the Protocol subclass instance best suited to handle the given URI path read-only.
			 *
			 * @param path The URI of the resource of interest
			 * @param type The desired type of resource, which may modify which protocol is used
			 * @return the allocated Protocol subclass
			 * @throw std::bad_alloc If the memory could not be allocated
			 * @throw Exception If no protocol could handle the path or creating it failed
			 */
			virtual std::unique_ptr<Protocol> readProtocol(const Path &path, Resource type = Resource::Unknown) const override;

			/**
			 * Creates the Protocol subclass instance best suited to handle the given URI path.
			 *
			 * @param path The URI of the resource of interest
			 * @return the allocated Protocol subclass
			 * @throw std::bad_alloc If the memory could not be allocated
			 * @throw Exception If no protocol could handle the path or creating it failed
			 */
			virtual std::unique_ptr<Protocol> openProtocol(const Path &path, Resource type = Resource::Unknown) override;

			/**
			 * Gets the metadata about the given resource.
			 * The metadata includes the modes and permissions, overall category, and content length.
			 * It also includes the canonical URI for the resource, following any links or redirects.
			 * If the resource does not actually exist, the returned metadata will reflect that.
			 *
			 * @param input the URI to the resource
			 * @return the metadata about the resource
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Metadata readMetadata(const Path &input) const override;

			/**
			 * Creates a resource using metadata as a template.
			 *
			 * @param resource The resource metadata describing the URI, permissions, and other parameters for the resource
			 * @param createParentResources Whether to create missing parent resources and if so whether to do it recursively
			 * @return the metadata describing the actually created resource
			 * @throw cio::Exception If any error (other than the resource already existing) prevented creating the resource
			 */
			virtual Metadata create(const Metadata &resource, Traversal createParentResources = Traversal::Recursive) override;

			/**
			 * Gets the overall resource type for a given resource.
			 * This is a subset of the metadata.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The resource path
			 * @return the overall resource type for the resource, or None if it does not exist
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Resource readType(const Path &path) const override;

			/**
			 * Gets the overall resource type for a given resource.
			 * This is a subset of the metadata.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The resource path
			 * @return the resource modes and permissions
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual ModeSet readModes(const Path &path, ModeSet desired = ModeSet::general()) const override;

			/**
			 * Gets the total content length for a given resource.
			 * This is a subset of the metadata.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The resource path
			 * @return the overall content length for the resource, or CIO_UNKNOWN_LENGTH if not known
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Length readSize(const Path &path) const override;

			/**
			 * Creates the CIO Protocol implementation most suitable for the given path, and then returns
			 * an opened directory suitable for scanning directory entries.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the directory
			 * @param modes The modes for the opened directory
			 * @return the protocol to use to read the directory
			 * @throw cio::Exception If the directory could not be opened for any reason
			 * @throw std::bad_alloc If memory allocation failed
			 */
			virtual std::unique_ptr<Protocol> readDirectory(const Path &path, ModeSet modes = ModeSet::readonly()) const override;

			/**
			 * Creates the CIO Protocol implementation most suitable for the given path, and then returns
			 * an opened directory suitable for scanning and modifying directory entries.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the directory
			 * @param modes The modes for the opened directory
			 * @param createParents WHether to create missing parent directories and if so to what depth
			 * @return the protocol to use to interact with the directory
			 * @throw cio::Exception If the directory could not be opened for any reason
			 * @throw std::bad_alloc If memory allocation failed
			 */
			virtual std::unique_ptr<Protocol> openDirectory(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::None) override;
			
			/**
			 * Creates an empty directory at the given path with the given modes, optionally also creating parent resources.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path of the directory to create
			 * @param modes The desired modes of the directory
			 * @param createParents Whether to create missing parent diretories and if so recursively
			 * @return the outcome of the operation
			 * @throw Exception If directory creation failed for any reason other than the directory already existing
			 */
			virtual State createDirectory(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
			
			/**
			 * Creates any missing parent resources for the given path, up to the given traversal depth.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path of the resource
			 * @param depth The number of parents to create
			 * @return the outcome of this operation
			 */
			virtual State createParents(const Path &path, Traversal depth = Traversal::Recursive) override;
			
			/**
			 * Examines the given resource as a directory to enumerate all of its children.
			 * This may consider only immediate children or recursively scan all children.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 * 
			 * @param path The path to the directory resource
			 * @param mode The mode describing whether to only capture immediate children or capture all children recursively
			 * @return the list of all discovered children
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented enumerating the children
			 * @throw std::bad_alloc If not enough memory was available to allocate the returned vector
			 */
			virtual std::vector<Path> listDirectory(const Path &path, Traversal mode = Traversal::Immediate) const override;

			/**
			 * Examines the given resource as a directory to enumerate all of its children.
			 * This may consider only immediate children or recursively scan all children.
			 * The given receiver callback is used to receive each child as it is discovered.
			 *
			 * The receiver can end traversal early by returning false or by throwing an exception.
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the directory resource
			 * @param mode The mode describing whether to only capture immediate children or capture all children recursively
			 * @param receiver The function callback to receive each discovered child and return whether to keep enumerating
			 * @return the total number of discovered children
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented enumerating the children
			 * @throw std::exception The receiver may throw any exception to indicate an error
			 */
			virtual std::size_t enumerateDirectory(const Path &path, Traversal mode, const std::function<bool(const Path &)> &receiver) const override;

			/**
			 * Removes and permanently erases (if possible) the resource at the given path.
			 * The Traversal parameter controls what happens if the resource is a directory with children.
			 * Traversal::None will only erase the resource if it has no children.
			 * Traversal::Immediate will erase direct children of the resource (but not their children if non-empty).
			 * Traversal::Recursive will recursively erase every child resource.
			 * 
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the resource to erase
			 * @param mode The mode controlling whether to erase non-empty directories and if so whether to erase only immediate children
			 * or all children recursively
			 * @return the total number of resources that were erased
			 * @throw cio::Exception If transport failures or other problems (other than the resource not existing) prevented erasure
			 */
			virtual Progress<std::uint64_t> erase(const Path &path, Traversal mode = Traversal::None) override;

			/**
			 * Create an Input subclass instantiation that can read the content for the resource at the given path.
			 * 
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open the input
			 * @return the Input class to use to read
			 * @throw cio::Exception If transport problems or lack of existence of the resource prevented creating an Input
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Input> openInput(const Path &path, ModeSet modes = ModeSet::readonly()) const override;

			/**
			 * Create an Output subclass instantiation that can write the content for the resource at the given path.
			 * The resource will be created if it does not already exist, but the existing content will be preserved if it is
			 * a persistent resource.
			 *
			 * Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			 * 
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open or create the channel
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the Output class to use to write
			 * @throw cio::Exception If transport problems or inability to open or create the resource prevented creating an Output
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Output> openOutput(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;

			/**
			 * Create an Channel subclass instantiation that can both read and write the content for the resource at the given path.
			 * The resource will be created if it does not already exist, but the existing content will be preserved if it is
			 * a persistent resource.
			 *
			 * Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			 * 
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open or create the channel
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the Channel class to use to both read and write
			 * @throw cio::Exception If transport problems or inability to open or create the resource prevented creating a Channel
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Channel> openChannel(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
			
			/**
			 * Creates an empty file using this protocol at the given path with the given modes, optionally also creating parent resources.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the file to create
			 * @param modes The desired modes for the file
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome of this operation, which should either be a success or a failure with Reason::Exists
			 * @throw Exception If creating the file failed for any reason other than it already existing
			 */
			virtual State createFile(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;

			/**
			 * Reads the target URI specified by the given path which is a link.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param path The path to the link
			 * @return the path to the resource the link references
			 * @throw Exception If the given path didn't exist or wasn't a link
			 */
			virtual Path readLink(const Path &path, Traversal depth = Traversal::Immediate) const override;
			
			/**
			 * Creates a link at the given link path that references the given target path.
			 *
			 * The Protocol Factory implementation looks up the best Protocol for the given path, and then delegates to that.
			 *
			 * @param link The path of the link to create
			 * @param target The path the link should point to
			 * @param modes The modes for the created link
			 * @param createParents Whether to create missing parent directories
			 * @return the outcome of the operation
			 * @throw Exception If the given link path already existed or the link couldn't be created
			 */
			virtual State createLink(const Path &link, const Path &target, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;

			/**
			 * Performs a direct copy from input to output path up to the specified traversal depth.
			 *
			 * The Protocol Factory implementation looks up the best Protocols for both the input and output.
			 * If the input and output use the same protocol, the operation will be delegated to that protocol.
			 * Otherwise, a generic algorithm will be used to perform the operation as a cross-protocol copy.
			 *
			 * @param input The input path
			 * @param output the output path
			 * @param depth The copy depth
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 */
			virtual Progress<std::uint64_t> copy(const Path &input, const Path &output, Traversal depth = Traversal::Recursive, Traversal createParents = Traversal::Recursive) override;
			
			/**
			 * Performs a direct move from input to output path.
			 *
			 * The Protocol Factory implementation looks up the best Protocols for both the input and output.
			 * If the input and output use the same protocol, the operation will be delegated to that protocol.
			 * Otherwise, a generic algorithm will be used to perform the operation as a cross-protocol move.
			 *
			 * By definition a move must be fully recursive, unlike a copy.
			 *
			 * @param input The input path
			 * @param output the output path
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 */
			virtual Progress<std::uint64_t> move(const Path &input, const Path &output, Traversal createParents = Traversal::Recursive) override;

		private:		
			/** Metaclass for this class */
			static Class<ProtocolFactory> sMetaclass;
			
			/** Global shared default protocol factory */
			static ProtocolFactory sDefault;

			/** Text that specifies file as default prefix */
			static const Text sFileDefaultPrefix;
	};
}

/* Inline implementation */

namespace cio
{
	inline ProtocolFactory *ProtocolFactory::getDefault() noexcept
	{
		return &sDefault;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

