/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_JSONWRITERTEST_H
#define CIO_JSONWRITERTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts basic testing of the CIO JSON Writer class.
	 */
	class JsonWriterTest : public cio::test::Suite
	{
		public:
			/**
			 * Construct the test.
			 */
			JsonWriterTest();

			/**
			 * Destructor.
			 */
			virtual ~JsonWriterTest() noexcept;

			/**
			 * The test for writeValue is implemented as a suite of tests,
			 * one for each method overload. This gets the suite.
			 * 
			 * @return the test suite implementing writeValue tests.
			 */
			cio::test::Suite makeWriteValueSubtest();

			/**
			 * Tests that the default construction of the writer works properly. 
			 */
			void testDefaultConstruction();

			/**
			 * Tests that startDocument works properly.
			 */
			void testStartDocument();

			/**
			 * Tests that endDocument works properly.
			 */
			void testEndDocument();

			/**
			 * Tests that startElement works properly.
			 */
			void testStartElement();

			/**
			 * Tests that endElement works properly.
			 */
			void testEndElement();

			/**
			 * Tests that writeValue(bool) works properly.
			 */
			void testWriteValueBool();

			/**
			 * Tests that writeValue(Text) works properly.
			 */
			void testWriteValueText();

			/**
			 * Tests that writeValue(int) works properly.
			 */
			void testWriteValueInt();

			/**
			 * Tests that writeValue(float) works properly.
			 */
			void testWriteValueFloat();

			/**
			 * Tests that writeValue(double) works properly.
			 */
			void testWriteValueDouble();

			/**
			 * Tests that startArray works properly.
			 */
			void testStartArray();

			/**
			 * Tests that endArray works properly.
			 */
			void testEndArray();
	};
}

#endif
