/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PRINTPROPERTIES_H
#define CIO_PRINTPROPERTIES_H

#include "Types.h"

#include "FixedText.h"

#include <string>

namespace cio
{
	/**
	 * The CIO Print Properties template is a traits class to non-intrusively determine basic properties
	 * on how to print a particular data type to a particular text character type.
	 *
	 * Currently this specifies minimum and maximum print length as compile-time constants.
	 * The general template assumes a minimum print length of 0 and a maximum print length of SIZE_MAX to indicate unknown.
	 * That should always work, but the template may be specialized for a particular value and character type to provide
	 * more specific information.
	 *
	 * It also specifies a String type to use for accumulating printed data.
	 * For types with a known and reasonably small maximum length (4096 bytes or less) this should be defined as a FixedText instance
	 * aligned upward to 4 or 8 byte boundary. For all other types, this should be defined as a mutable and resizable std::basic_string instance.
	 * In both cases, the String type should also accommodate a null terminating character for interoperability.
	 *
	 * This file specializes PrintProperties for built-in primitive C++ types and UTF-8 characters.
	 *
	 * @tparam T The value type
	 * @tparam C The text character type
	 */
	template <typename T, typename C>
	class PrintProperties
	{
		public:
			/** String type to use for printing. General case uses a resizable std::basic_string<C>. */
			using String = std::basic_string<C>;

			/** Minimum possible print length. 0 for general template. */
			static const std::size_t minimum = 0;

			/** Maximum possible print length. SIZE_MAX for general template to indicate unknown. */
			static const std::size_t maximum = SIZE_MAX;
	};

	/**
	 * The CIO Integer Print Properties template is a helper traits class to specify the Print Properties
	 * for built-in integer types, to accommodate that C++ integer types may have different byte sizes and therefore
	 * bounds on different platforms.
	 *
	 * The generic implementation uses an unbounded string.
	 * Specializations for particular combinations of byte size N, signedness S, and character type C should specify appropriate fixed sizes.
	 */
	template <std::size_t N, bool S, typename C>
	class IntegerPrintProperties
	{
		public:
			/** String type to use for printing. General case uses a resizable std::basic_string<C>. */
			using String = std::basic_string<C>;

			/** Minimum possible print length. 0 for general template. */
			static const std::size_t minimum = 0;

			/** Maximum possible print length. SIZE_MAX for general template to indicate unknown. */
			static const std::size_t maximum = SIZE_MAX;
	};

	/**
	 * Obtains the compile-time type of a buffer that can be used to print a type.
	 *
	 * @note If the print buffer size is unknown at compile time, this will generally result in a std::basic_string<C> being used.
	 *
	 * @tparam T The value type
	 * @tparam C The character type
	 */
	template <typename T, typename C>
	using PrintBufferType = typename PrintProperties<T, C>::String;

	/**
	 * Obtains the compile-time type of a buffer that can be used to print a type to UTF-8 text.
	 *
	 * @note If the print buffer size is unknown at compile time, this will generally result in a std::string being used.
	 *
	 * @tparam T The value type
	 */
	template <typename T>
	using PrintBuffer = PrintBufferType<T, char>;

	// Print Properties partial specializations to remove const and references
	// Pointers and array qualifiers are not removed since those inherently change the type

	/**
	 * CIO Print Properties partial specialization for const T to remove const.
	 *
	 * @tparam T The underlying non-const type
	 * @tparam C The character type
	 */
	template <typename T, typename C>
	class PrintProperties<const T, C> : public PrintProperties<T, C> {};

	/**
	 * CIO Print Properties partial specialization for T & to remove the reference.
	 *
	 * @tparam T The underlying non-reference type
	 * @tparam C The character type
	 */
	template <typename T, typename C>
	class PrintProperties<T &, C> : public PrintProperties<T, C> {};

	/**
	 * CIO Print Properties partial specialization for const T & to remove the const reference.
	 *
	 * @tparam T The underlying non-const non-reference type
	 * @tparam C The character type
	 */
	template <typename T, typename C>
	class PrintProperties<const T &, C> : public PrintProperties<T, C> {};

	// Full specializations for built-in C++ types and UTF-8 characters

	/**
	 * Specialization for void to indicate the empty string in all cases.
	 */
	template <>
	class PrintProperties<void, char>
	{
		public:
			/** String type to use for printing. Always the empty string. */
			using String = FixedText<0>;

			/** Minimum possible print length. 0 for general template. */
			static const std::size_t minimum = 0;

			/** Maximum possible print length. SIZE_MAX for general template to indicate unknown. */
			static const std::size_t maximum = SIZE_MAX;
	};

	/**
	 * Specialization for bool to handle that the possible results are "true" and "false".
	 */
	template <>
	class PrintProperties<bool, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<7>;

			/** Minimum possible print length. 4  for "true". */
			static const std::size_t minimum = 4;

			/** Maximum possible print length. 5 for "false". */
			static const std::size_t maximum = 5;
	};

	/**
	 * Specialization for char to specify it as a single UTF-8 character.
	 */
	template <>
	class PrintProperties<char, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<1>;

			/** Minimum possible print length. Always 1. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 1. */
			static const std::size_t maximum = 1;
	};

	// Partial specializations for unsigned integers to delegate to Integer Print Properties.

	/** 
	 * CIO Print Properties specialization for unsigned char to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<unsigned char, C> : public IntegerPrintProperties<sizeof(unsigned char), false, C> { };

	/**
	 * CIO Print Properties specialization for unsigned short to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<unsigned short, C> : public IntegerPrintProperties<sizeof(unsigned short), false, C> { };

	/**
	 * CIO Print Properties specialization for unsigned int to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<unsigned, C> : public IntegerPrintProperties<sizeof(unsigned), false, C> { };

	/**
	 * CIO Print Properties specialization for unsigned long to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<unsigned long, C> : public IntegerPrintProperties<sizeof(unsigned long), false, C> { };

	/**
	 * CIO Print Properties specialization for unsigned long to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<unsigned long long, C> : public IntegerPrintProperties<sizeof(unsigned long long), false, C> { };

	// Partial specializations for signed integers to delegate to Integer Print Properties.

	/**
	 * CIO Print Properties specialization for signed char to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<signed char, C> : public IntegerPrintProperties<sizeof(signed char), false, C> { };

	/**
	 * CIO Print Properties specialization for signed short to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<short, C> : public IntegerPrintProperties<sizeof(short), true, C> { };

	/**
	 * CIO Print Properties specialization for signed int to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<int, C> : public IntegerPrintProperties<sizeof(int), true, C> { };

	/**
	 * CIO Print Properties specialization for signed long to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<long, C> : public IntegerPrintProperties<sizeof(long), true, C> { };

	/**
	 * CIO Print Properties specialization for signed long long to delegate it to the appropriate IntegerPrintProperties.
	 *
	 * @tparam C The character type
	 */
	template <typename C>
	class PrintProperties<long long, C> : public IntegerPrintProperties<sizeof(long long), true, C> { };

	// Full specializations for floating point types

	/**
	 * CIO Print Properties specialization for float and UTF-8 characters to specify it as a fixed minimum and maximum length.
	 */
	template <>
	class PrintProperties<float, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<15>;

			/** Minimum possible print length. Always 3 for "0.0" and "NaN". */
			static const std::size_t minimum = 3;

			/** Maximum possible print length. Currently hard-coded to 31 as this can be hard to calculate. */
			static const std::size_t maximum = 15;
	};

	/**
	 * CIO Print Properties specialization and UTF-8 characters to specify it as a fixed minimum and maximum length.
	 */
	template <>
	class PrintProperties<double, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<31>;

			/** Minimum possible print length. Always 3 for "0.0" and "NaN". */
			static const std::size_t minimum = 3;

			/** Maximum possible print length. Currently hard-coded to 31 as this can be hard to calculate. */
			static const std::size_t maximum = 31;
	};

	// Full specializations for Integer Print Properties using built-in sizes

	/**
	 * CIO Integer Print Properties specialization for 1-byte unsigned integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<1u, false, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<3>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 3 since longest string is "255". */
			static const std::size_t maximum = 3;
	};

	/**
	 * CIO Integer Print Properties specialization for 2-byte unsigned integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<2u, false, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<7>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 5 since longest string is "65535". */
			static const std::size_t maximum = 5;
	};

	/**
	 * CIO Integer Print Properties specialization for 4-byte unsigned integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<4u, false, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<15>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 10 since longest string is roughly 9 billion. */
			static const std::size_t maximum = 10;
	};

	/**
	 * CIO Integer Print Properties specialization for 8-byte unsigned integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<8u, false, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<23>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 20 since longest string is roughly 1.8e19. */
			static const std::size_t maximum = 20;
	};

	/**
	 * CIO Integer Print Properties specialization for 1-byte signed integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<1, true, char>
	{
		public:
		/** String type to use for printing. */
		using String = FixedText<7>;

		/** Minimum possible print length. Always 1 since at least one digit is printed. */
		static const std::size_t minimum = 1;

		/** Maximum possible print length. Always 4 since longest string is "-128". */
		static const std::size_t maximum = 4;
	};

	/**
	 * CIO Integer Print Properties specialization for 2-byte signed integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<2u, true, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<7>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 6 since longest string is "-32768". */
			static const std::size_t maximum = 6;
	};

	/**
	 * CIO Integer Print Properties specialization for 4-byte signed integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<4u, true, char>
	{
		public:
			/** String type to use for printing. */
			using String = FixedText<15>;

			/** Minimum possible print length. Always 1 since at least one digit is printed. */
			static const std::size_t minimum = 1;

			/** Maximum possible print length. Always 11 since longest string is roughly negative 4 billion. */
			static const std::size_t maximum = 11;
	};

	/**
	 * CIO Integer Print Properties specialization for 8-byte signed integer printed to UTF-8.
	 */
	template <>
	class IntegerPrintProperties<8u, true, char>
	{
		public:
		/** String type to use for printing. */
		using String = FixedText<23>;

		/** Minimum possible print length. Always 1 since at least one digit is printed. */
		static const std::size_t minimum = 1;

		/** Maximum possible print length. Always 20 since longest string is roughly negative 9e18. */
		static const std::size_t maximum = 20;
	};
}

#endif
