/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Text.h"

#include "New.h"

#include "Case.h"
#include "CharacterView.h"
#include "Class.h"
#include "Parse.h"
#include "Print.h"
#include "Type.h"
#include "Validation.h"

#include <cctype>
#include <ostream>

namespace cio
{
	Class<Text> Text::sMetaclass("cio::Text");

	const char Text::sEmptyText(0);

	const char *Text::getEmptyText() noexcept
	{
		return &sEmptyText;
	}

	Text Text::fromUtf16(const char16_t *text, std::size_t count)
	{
		Text result;
		if (text && count && text[0])
		{
			std::size_t length = cio::strlen(text, count) + 1;
			char *buffer = result.allocate(length);
			Character::transcode(text, count, buffer, length);
		}
		return result;
	}

	Text Text::fromUnicode(const char32_t *text, std::size_t count)
	{
		Text result;
		if (text && count && text[0])
		{
			std::size_t length = cio::strlen(text, count) + 1;
			char *buffer = result.allocate(length);
			Character::transcode(text, count, buffer, length);
		}
		return result;
	}

	Text Text::duplicate(const char *text)
	{
		Text result(text);
		result.internalize();
		return result;
	}

	Text Text::duplicate(const char *text, std::size_t length)
	{
		Text result(text, length);
		result.internalize();
		return result;
	}

	Text Text::duplicate(const std::string &text)
	{
		Text result(text);
		result.internalize();
		return result;
	}

	std::string Text::trim(const char *text)
	{
		return cio::text(text).trim();
	}

	std::string Text::trim(const std::string &text)
	{
		return cio::text(text).trim();
	}

	std::string Text::trim(Text text) noexcept
	{
		return cio::text(text.mElements, text.mCapacity).trim();
	}

	Text Text::view(const char *s) noexcept
	{
		return Text(s);
	}

	Text Text::view(const std::string &s) noexcept
	{
		return s.empty() ? Text() : Text(s.data(), s.size() + 1);
	}

	Text Text::viewNullTerminated(const char *buffer, std::size_t len)
	{
		Text text(buffer, len);
		text.nullTerminate();
		return text;
	}

	Text::Text() noexcept :
		mFactory(nullptr)
	{
		// nothing more to do
	}

	Text::Text(const Text &in) :
		mElements(in.mElements),
		mCapacity(in.mCapacity),
		mFactory(nullptr)
	{
		// nothing more to do
	}

	Text::Text(Text &&in) noexcept :
		mElements(in.mElements),
		mCapacity(in.mCapacity),
		mFactory(std::move(in.mFactory))
	{
		in.mElements = nullptr;
		in.mCapacity = 0;
	}

	Text::Text(const char *cstr) noexcept :
		mElements(cstr),
		mCapacity(cstr ? std::strlen(cstr) + 1 : 0),
		mFactory(nullptr)
	{
		// nothing more to do
	}

	Text::Text(const std::string &text) noexcept :
		mElements(nullptr),
		mCapacity(text.size() + 1),
		mFactory(!text.empty())
	{
		mElements = text.empty() ? &sEmptyText : mFactory.copy(text.c_str(), text.size() + 1);
	}

	Text::Text(const char *str, std::size_t length) :
		mElements(str),
		mCapacity(length),
		mFactory(nullptr)
	{
		// nothing more to do
	}

	Text::Text(char *text, Factory<char> allocator) noexcept :
		mElements(text),
		mCapacity(text ? std::strlen(text) + 1 : 0),
		mFactory(allocator)
	{
		// nothing more to do
	}

	Text::Text(char *str, std::size_t length, Factory<char> allocator) noexcept :
		mElements(str),
		mCapacity(length),
		mFactory(allocator)
	{
		// nothing more to do
	}

	Text &Text::operator=(const Text &in)
	{
		if (this != &in)
		{
			this->clear();
			mElements = in.mElements;
			mCapacity = in.mCapacity;
		}
		return *this;
	}

	Text &Text::operator=(Text &&in) noexcept
	{
		if (this != &in)
		{
			mElements = in.mElements;
			mCapacity = in.mCapacity;
			mFactory = in.mFactory;

			in.mElements = nullptr;
			in.mCapacity = 0;
		}
		return *this;
	}

	bool Text::isNullTerminated() const noexcept
	{
		const char *current = mElements;
		const char *end = mElements + mCapacity;

		while (current < end && *current)
		{
			++current;
		}

		return current < end;
	}

	std::size_t Text::strlen() const noexcept
	{
		const char *current = mElements;
		const char *end = mElements + mCapacity;

		while (current < end && *current)
		{
			++current;
		}

		return current - mElements;
	}

	std::size_t Text::size() const noexcept
	{
		const char *current = mElements;
		const char *end = mElements + mCapacity;

		while (current < end && *current)
		{
			++current;
		}

		return current - mElements;
	}

	std::size_t Text::capacity() const noexcept
	{
		return mCapacity;
	}

	bool Text::empty() const noexcept
	{
		return mCapacity == 0 || !(*mElements);
	}

	bool Text::null() const noexcept
	{
		return mCapacity == 0;
	}

	const char *Text::data() const noexcept
	{
		return mElements;
	}

	const char *Text::c_str() const noexcept
	{
		return this->isNullTerminated() ? mElements : nullptr;
	}

	const char *Text::begin() const noexcept
	{
		return mElements;
	}

	const char *Text::end() const noexcept
	{
		return mElements + this->size();
	}

	const char *Text::limit() const noexcept
	{
		return mElements + mCapacity;
	}

	const char &Text::operator[](std::size_t offset) const noexcept
	{
		return mElements[offset];
	}

	Character Text::codepoint(std::size_t offset) const noexcept
	{
		return offset < mCapacity ? Character(mElements + offset, mCapacity - offset) : Character();
	}

	const char &Text::front() const noexcept
	{
		return *mElements;
	}

	const char &Text::back() const noexcept
	{
		return mElements[mCapacity - 1];
	}

	Text Text::view() const noexcept
	{
		return Text(mElements, mCapacity);
	}

	Text Text::substr(std::size_t off, std::size_t count) const noexcept
	{
		std::size_t limit = std::min(off, mCapacity);
		std::size_t actual = std::min(mCapacity - limit, count);
		return Text(mElements + limit, actual);
	}

	Text Text::suffix(std::size_t off) const noexcept
	{
		std::size_t limit = std::min(off, mCapacity);
		return Text(mElements + limit, mCapacity - limit);
	}

	Text Text::prefix(std::size_t prefix) const noexcept
	{
		return Text(mElements, std::min(prefix, mCapacity));
	}

	Text Text::splitBefore(char c) const noexcept
	{
		return this->prefix(this->find(c));
	}

	Text Text::splitBefore(const Text &t) const noexcept
	{
		return this->prefix(this->find(t));
	}

	Text Text::splitBeforeSuffix(char c) const noexcept
	{
		Text before;
		std::size_t end = this->strlen();
		if (end > 0 && mElements[end - 1] == c)
		{
			before.bind(mElements, end - 1);
		}
		return before;
	}

	Text Text::splitBeforeSuffix(const Text &t) const noexcept
	{
		Text before;
		std::size_t theirs = t.strlen();
		if (theirs > 0)
		{
			std::size_t ours = this->strlen();
			if (ours >= theirs && cio::equalText(mElements + (ours - theirs), t.mElements, theirs))
			{
				before.bind(mElements, ours - theirs);
			}
		}
		else
		{
			before.bind(mElements, mCapacity);
		}
		return before;
	}

	Text Text::splitAfter(char c) const noexcept
	{
		return this->suffix(std::min(this->find(c), mCapacity) + 1);
	}

	Text Text::splitAfter(const Text &t) const noexcept
	{
		return this->suffix(std::min(this->find(t), mCapacity) + t.strlen());
	}

	Text Text::splitAfterPrefix(char c) const noexcept
	{
		Text after;
		if (mCapacity && mElements[0] == c)
		{
			after.bind(mElements + 1, mCapacity - 1);
		}
		return after;
	}

	Text Text::splitAfterPrefix(const Text &t) const noexcept
	{
		Text after;
		std::size_t theirs = t.strlen();
		if (theirs > 0)
		{
			std::size_t ours = this->strlen();
			if (ours >= theirs && cio::equalText(mElements, t.mElements, theirs))
			{
				after.bind(mElements + theirs, ours - theirs);
			}
		}
		else
		{
			after.bind(mElements, mCapacity);
		}
		return after;
	}

	std::pair<Text, Text> Text::split(char c) const noexcept
	{
		std::size_t found = this->find(c);
		std::size_t split = std::min(found, mCapacity);
		std::size_t next = std::min(split + 1, mCapacity);

		return std::pair<Text, Text>(
			Text(mElements, split),
			Text(mElements + next, mCapacity - next));
	}

	std::pair<Text, Text> Text::split(const Text &t) const noexcept
	{
		std::size_t found = this->find(t);
		std::size_t split = std::min(found, mCapacity);
		std::size_t next = std::min(split + t.strlen(), mCapacity);

		return std::pair<Text, Text>(
			Text(mElements, split),
			Text(mElements + next, mCapacity - next));
	}

	Text::operator const char *() const noexcept
	{
		return this->c_str();
	}

	Text::operator bool() const noexcept
	{
		return mCapacity && *mElements;
	}

	bool SortTextCaseSensitive::operator()(const char *left, const char *right) const noexcept
	{
		return std::strcmp(left, right) <= 0;
	}

	bool SortTextCaseSensitive::operator()(const std::string &left, const std::string &right) const noexcept
	{
		return left < right;
	}

	bool SortTextCaseSensitive::operator()(const Text &left, const Text &right) const noexcept
	{
		return left < right;
	}

	bool SortTextCaseInsensitive::operator()(const char *left, const char *right) const noexcept
	{
		return CIO_STRCASECMP(left, right) < 0;
	}

	bool SortTextCaseInsensitive::operator()(const Text &left, const Text &right) const noexcept
	{
		return left.compareInsensitive(right) < 0;
	}

	SortTextCaseOptional::SortTextCaseOptional() noexcept :
		mMode(Case::Sensitive)
	{
		// nothing more to do
	}

	SortTextCaseOptional::SortTextCaseOptional(Case mode) noexcept :
		mMode(mode)
	{
		// nothing more to do
	}

	bool SortTextCaseOptional::operator()(const char *left, const char *right) const noexcept
	{
		return mMode == Case::Sensitive ? std::strcmp(left, right) < 0 : CIO_STRCASECMP(left, right) < 0;
	}

	bool SortTextCaseOptional::operator()(const Text &left, const Text &right) const noexcept
	{
		return left.compare(right, mMode) < 0;
	}

	Case SortTextCaseOptional::getCaseMode() const noexcept
	{
		return mMode;
	}

	void SortTextCaseOptional::setCaseMode(Case mode) noexcept
	{
		mMode = mode;
	}

	Text::~Text() noexcept
	{
		mFactory.destroy(const_cast<char *>(mElements), mCapacity);
	}

	void Text::clear() noexcept
	{
		mFactory.destroy(const_cast<char *>(mElements), mCapacity);
		mFactory = nullptr;
		mElements = nullptr;
		mCapacity = 0;
	}

	bool Text::nullTerminate()
	{
		bool nullTerminated = true;

		if (mElements)
		{
			nullTerminated = this->isNullTerminated();

			if (!nullTerminated)
			{
				if (mFactory)
				{
					mElements = mFactory.resize(const_cast<char *>(mElements), mCapacity, mCapacity + 1);
				}
				else
				{
					mFactory = New::get();
					mElements = mFactory.copy(mElements, mCapacity, mCapacity + 1);
				}

				++mCapacity;
			}
		}

		return !nullTerminated;
	}

	Text Text::viewNullTerminated() const
	{
		Text text(mElements, mCapacity);
		text.nullTerminate();
		return text;
	}

	void Text::bind(const char *text) noexcept
	{
		if (text != mElements)
		{
			this->clear();

			mElements = text;
			if (text)
			{
				mCapacity = std::strlen(text);
			}
		}
	}

	void Text::bind(const std::string &text) noexcept
	{
		if (text.c_str() != mElements)
		{
			this->clear();
			mElements = text.empty() ? &sEmptyText : text.c_str();
			mCapacity = text.size() + 1;
		}
	}

	void Text::bind(std::string &&text)
	{
		this->clear();

		mFactory = Factory<char>(!text.empty());
		mElements = text.empty() ? &sEmptyText : mFactory.copy(text.c_str(), text.size() + 1);
		mCapacity = text.size() + 1;
	}

	void Text::bind(const char *text, std::size_t length) noexcept
	{
		if (text != mElements)
		{
			this->clear();

			mElements = text;
			mCapacity = length;
		}
	}

	void Text::bind(char *text, std::size_t length, Factory<char> allocator) noexcept
	{
		if (text != mElements)
		{
			this->clear();

			mElements = text;
			mCapacity = length;
			mFactory = allocator;
		}
	}

	char *Text::allocate(std::size_t capacity)
	{
		this->clear();
		mFactory = Factory<char>();
		char *data = mFactory.create(capacity);
		mElements = data;
		mCapacity = capacity;
		return data;
	}

	Factory<char> Text::allocator() const noexcept
	{
		return mFactory;
	}

	bool Text::internalize()
	{
		bool done = false;

		if (!this->mFactory && mCapacity > 0)
		{
			if (*mElements)
			{
				mFactory = Factory<char>();
				std::size_t actual = this->strlen();

				mCapacity = actual + 1;
				mElements = mFactory.copy(mElements, actual, mCapacity);
			}
			else
			{
				mElements = &sEmptyText;
				mCapacity = 1;
			}

			done = true;
		}

		return done;
	}

	Text &Text::internalize(const char *text)
	{
		if (mElements != text)
		{
			this->clear();


			if (text)
			{
				if (*text)
				{
					mFactory = Factory<char>();
					std::size_t len = std::strlen(text) + 1;
					mElements = mFactory.copy(text, len);
					mCapacity = len;
				}
				else
				{
					mElements = &sEmptyText;
					mCapacity = 1;
				}
			}
		}
		else if (!mFactory)
		{
			this->internalize();
		}

		return *this;
	}

	Text &Text::internalize(const char *text, std::size_t length)
	{
		if (mElements != text)
		{
			this->clear();

			if (text && length > 0)
			{
				if (*text)
				{
					std::size_t actual = cio::getTerminator(text, length);
					std::size_t allocated = actual + 1;

					mFactory = Factory<char>();
					mElements = mFactory.copy(text, actual, allocated);
					mCapacity = allocated;
				}
				else
				{
					mElements = &sEmptyText;
					mCapacity = 1;
				}
			}
		}
		else if (!mFactory)
		{
			this->internalize();
		}

		return *this;
	}

	Text &Text::internalize(const std::string &text)
	{
		this->internalize(text.c_str(), text.size() + 1);
		return *this;
	}

	Text &Text::internalize(const Text &text)
	{
		return this->internalize(text.mElements, text.mCapacity);
	}

	Text &Text::internalize(Text &&text)
	{
		*this = std::move(text);
		this->internalize();
		return text;
	}

	Text &Text::merge(const Text &text)
	{
		if (mCapacity == 0)
		{
			this->internalize(text);
		}

		return *this;
	}

	Text &Text::merge(Text &&text)
	{
		if (mCapacity == 0)
		{
			this->internalize(std::move(text));
		}

		return *this;
	}

	Text Text::duplicate() const
	{
		Text text;
		text.internalize(*this);
		return text;
	}

	Text Text::trim() const noexcept
	{
		CharacterView<const char> trimmed = cio::text(mElements, mCapacity).trim();
		return Text(trimmed.data(), trimmed.values());
	}

	std::string Text::toLower() const
	{
		std::string output;
		CharacterView<const char> view(mElements, mCapacity);
		std::size_t needed = 0;

		for (Character c : view)
		{
			Character lc = c.toLowercase();
			needed += lc.strlen();
		}

		output.resize(needed);
		std::size_t current = 0;
		for (Character c : view)
		{
			Character lc = c.toLowercase();
			current += lc.print(&output[current], output.size() - current);
		}
		return output;
	}

	std::string Text::toUpper() const
	{
		std::string output;
		CharacterView<const char> view(mElements, mCapacity);
		std::size_t needed = 0;

		for (Character c : view)
		{
			Character lc = c.toUppercase();
			needed += lc.strlen();
		}

		output.resize(needed);
		std::size_t current = 0;
		for (Character c : view)
		{
			Character lc = c.toUppercase();
			current += lc.print(&output[current], output.size() - current);
		}
		return output;
	}

	std::string Text::toCase(Case c) const
	{
		return c == Case::Lower ? this->toLower() :
			(c == Case::Upper ? this->toUpper() : this->str());
	}

	Text Text::replace(char search, char replace) const
	{
		Text output = this->view();

		for (std::size_t i = 0; i < mCapacity; ++i)
		{
			if (mElements[i] == search)
			{
				Factory<char> allocator;
				char *modified = allocator.copy(mElements, i, mCapacity);
				modified[i] = replace;

				while (i < mCapacity)
				{
					if (mElements[i] == search)
					{
						modified[i] = replace;
					}
					else
					{
						modified[i] = mElements[i];
					}
					++i;
				}

				output.bind(modified, mCapacity, allocator);
			}
		}

		return output;
	}

	std::string Text::str() const
	{
		return std::string(mElements, this->strlen());
	}

	Text::operator std::string() const
	{
		return std::string(mElements, this->strlen());
	}

	Text operator+(const Text &left, const Text &right)
	{
		std::size_t llen = left.strlen();
		std::size_t rlen = right.strlen();
		std::size_t len = llen + rlen;

		Factory<char> allocator;
		char *tmp = allocator.copy(left.data(), llen, llen + rlen + 1);
		std::memcpy(tmp + llen, right.data(), rlen);
		tmp[llen + rlen] = 0;

		Text result;
		result.bind(tmp, llen + rlen + 1, allocator);
		return result;
	}

	Text &operator+=(Text &left, const Text &right)
	{
		std::size_t llen = left.strlen();
		std::size_t rlen = right.strlen();
		std::size_t len = llen + rlen;

		Factory<char> allocator;
		char *tmp = allocator.copy(left.data(), llen, llen + rlen + 1);
		std::memcpy(tmp + llen, right.data(), rlen);
		tmp[llen + rlen] = 0;

		left.bind(tmp, llen + rlen + 1, std::move(allocator));
		return left;
	}

	int Text::compare(const char *text) const noexcept
	{
		int cmp = 0;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			while (true)
			{
				// If left hit data end, cmp should be 0 if right is also at null terminator, and -1 otherwise
				if (l >= lend)
				{
					cmp = -static_cast<int>(*r != 0);
					break;
				}

				// get characters
				char lc = *l;
				char rc = *r;

				// mismatch, either actual mismatch or null terminator found
				// l < r = -1
				// l > r = +1
				if (lc < rc)
				{
					cmp = -1;
					break;
				}

				if (lc > rc)
				{
					cmp = 1;
					break;
				}

				// Both hit null terminator at same time
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
			}
		}
		else
		{
			// We are after the null string if we have any content
			cmp = static_cast<int>(mElements && mElements[0] != 0);
		}

		return cmp;
	}

	int Text::compare(const std::string &text) const noexcept
	{
		return this->compare(text.c_str());
	}

	int Text::compare(const Text &text) const noexcept
	{
		int cmp = 0;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		while (true)
		{
			// If left hit data end, cmp should be 0 if right is also at data end or null terminator, and -1 otherwise
			if (l >= lend)
			{
				cmp = -static_cast<int>((r < rend) && (*r != 0));
				break;
			}

			// If right hit data end, cmp should be 0 if left is at null terminator and +1 otherwise
			if (r >= rend)
			{
				cmp = static_cast<int>(*l != 0);
				break;
			}

			// get characters
			char lc = *l;
			char rc = *r;

			// mismatch, either actual mismatch or null terminator found
			// l < r = -1
			// l > r = +1
			if (lc < rc)
			{
				cmp = -1;
				break;
			}

			if (lc > rc)
			{
				cmp = 1;
				break;
			}

			// Both hit null terminator at same time
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;
		}

		return cmp;
	}

	int Text::compareInsensitive(const char *text) const noexcept
	{
		int cmp = 0;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			while (true)
			{
				// If left hit data end, cmp should be 0 if right is also at null terminator, and -1 otherwise
				if (l >= lend)
				{
					cmp = -static_cast<int>(*r != 0);
					break;
				}

				// Force to upper case for comparison
				char lc = std::toupper(*l);
				char rc = std::toupper(*r);

				// mismatch, either actual mismatch or null terminator found
				// l < r = -1
				// l > r = +1
				if (lc < rc)
				{
					cmp = -1;
					break;
				}

				if (lc > rc)
				{
					cmp = 1;
					break;
				}

				// Both hit null terminator at same time
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
			}
		}
		else
		{
			// We are after the null string if we have any content
			cmp = static_cast<int>(mElements && mElements[0] != 0);
		}

		return cmp;
	}

	int Text::compareInsensitive(const std::string &text) const noexcept
	{
		return this->compareInsensitive(text.c_str());
	}

	int Text::compareInsensitive(const Text &text) const noexcept
	{
		int cmp = 0;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		while (true)
		{
			// If left hit data end, cmp should be 0 if right is also at data end or null terminator, and -1 otherwise
			if (l >= lend)
			{
				cmp = -static_cast<int>((r < rend) && (*r != 0));
				break;
			}

			// If right hit data end, cmp should be 0 if left is at null terminator and +1 otherwise
			if (r >= rend)
			{
				cmp = static_cast<int>(*l != 0);
				break;
			}

			// Force to upper case for comparison
			char lc = std::toupper(*l);
			char rc = std::toupper(*r);

			// mismatch, either actual mismatch or null terminator found
			// l < r = -1
			// l > r = +1
			if (lc < rc)
			{
				cmp = -1;
				break;
			}

			if (lc > rc)
			{
				cmp = 1;
				break;
			}

			// Both hit null terminator at same time
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;
		}

		return cmp;
	}

	int Text::compare(const char *text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->compare(text) : this->compareInsensitive(text);
	}

	int Text::compare(const std::string &text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->compare(text) : this->compareInsensitive(text);
	}

	int Text::compare(const Text &text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->compare(text) : this->compareInsensitive(text);
	}

	int Text::comparePrefix(const char *text, std::size_t maxLength) const noexcept
	{
		int cmp = 0;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			std::size_t count = 0;

			while (count < maxLength)
			{
				// If left hit data end, cmp should be 0 if right is also at null terminator, and -1 otherwise
				if (l >= lend)
				{
					cmp = -static_cast<int>(*r != 0);
					break;
				}

				// get characters
				char lc = *l;
				char rc = *r;

				// mismatch, either actual mismatch or null terminator found
				// l < r = -1
				// l > r = +1
				if (lc < rc)
				{
					cmp = -1;
					break;
				}

				if (lc > rc)
				{
					cmp = 1;
					break;
				}

				// Both hit null terminator at same time
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
				++count;
			}
		}
		else
		{
			// We are after the null string if we have any content
			cmp = static_cast<int>(mElements && mElements[0] != 0);
		}

		return cmp;
	}

	int Text::comparePrefix(const std::string &text, std::size_t length) const noexcept
	{
		return this->comparePrefix(text.c_str(), length);
	}

	int Text::comparePrefix(const Text &text, std::size_t length) const noexcept
	{
		int cmp = 0;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		std::size_t count = 0;

		while (count < length)
		{
			// If left hit data end, cmp should be 0 if right is also at data end or null terminator, and -1 otherwise
			if (l >= lend)
			{
				cmp = -static_cast<int>((r < rend) && (*r != 0));
				break;
			}

			// If right hit data end, cmp should be 0 if left is at null terminator and +1 otherwise
			if (r >= rend)
			{
				cmp = static_cast<int>(*l != 0);
				break;
			}

			// get characters
			char lc = *l;
			char rc = *r;

			// mismatch, either actual mismatch or null terminator found
			// l < r = -1
			// l > r = +1
			if (lc < rc)
			{
				cmp = -1;
				break;
			}

			if (lc > rc)
			{
				cmp = 1;
				break;
			}

			// Both hit null terminator at same time
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;

			++count;
		}

		return cmp;
	}

	int Text::comparePrefixInsensitive(const char *text, std::size_t maxLength) const noexcept
	{
		int cmp = 0;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			std::size_t count = 0;

			while (count < maxLength)
			{
				// If left hit data end, cmp should be 0 if right is also at null terminator, and -1 otherwise
				if (l >= lend)
				{
					cmp = -static_cast<int>(*r != 0);
					break;
				}

				// force characters to upper case for comparison
				char lc = std::toupper(*l);
				char rc = std::toupper(*r);

				// mismatch, either actual mismatch or null terminator found
				// l < r = -1
				// l > r = +1
				if (lc < rc)
				{
					cmp = -1;
					break;
				}

				if (lc > rc)
				{
					cmp = 1;
					break;
				}

				// Both hit null terminator at same time
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
				++count;
			}
		}
		else
		{
			// We are after the null string if we have any content
			cmp = static_cast<int>(mElements && mElements[0] != 0);
		}

		return cmp;
	}

	int Text::comparePrefixInsensitive(const std::string &text, std::size_t length) const noexcept
	{
		return this->comparePrefixInsensitive(text.c_str(), length);
	}

	int Text::comparePrefixInsensitive(const Text &text, std::size_t length) const noexcept
	{
		int cmp = 0;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		std::size_t count = 0;

		while (count < length)
		{
			// If left hit data end, cmp should be 0 if right is also at data end or null terminator, and -1 otherwise
			if (l >= lend)
			{
				cmp = -static_cast<int>((r < rend) && (*r != 0));
				break;
			}

			// If right hit data end, cmp should be 0 if left is at null terminator and +1 otherwise
			if (r >= rend)
			{
				cmp = static_cast<int>(*l != 0);
				break;
			}

			// force characters to upper case for comparison
			char lc = std::toupper(*l);
			char rc = std::toupper(*r);

			// mismatch, either actual mismatch or null terminator found
			// l < r = -1
			// l > r = +1
			if (lc < rc)
			{
				cmp = -1;
				break;
			}

			if (lc > rc)
			{
				cmp = 1;
				break;
			}

			// Both hit null terminator at same time
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;

			++count;
		}

		return cmp;
	}

	int Text::comparePrefix(const char *text, std::size_t maxLength, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->comparePrefix(text, maxLength) : this->comparePrefixInsensitive(text, maxLength);
	}

	int Text::comparePrefix(const std::string &text, std::size_t maxLength, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->comparePrefix(text, maxLength) : this->comparePrefixInsensitive(text, maxLength);
	}

	int Text::comparePrefix(const Text &text, std::size_t maxLength, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->comparePrefix(text, maxLength) : this->comparePrefixInsensitive(text, maxLength);
	}

	bool Text::equal(const char *text) const noexcept
	{
		bool matched = true;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			while (matched)
			{
				// If left hit data end, match should be true if right is also at null terminator, and false otherwise
				if (l >= lend)
				{
					matched = (*r == 0);
					break;
				}

				// get characters
				char lc = *l;
				char rc = *r;
				matched = (lc == rc);

				// Break on null terminator
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
			}
		}
		else
		{
			matched = !mElements || (mElements[0] == 0);
		}

		return matched;
	}

	bool Text::equal(const std::string &text) const noexcept
	{
		return this->equal(text.c_str());
	}

	bool Text::equal(const Text &text) const noexcept
	{
		bool matched = true;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		while (matched)
		{
			// If left hit data end, match should be true if right is also at data end or null terminator, and false otherwise
			if (l >= lend)
			{
				matched = (r >= rend || (*r == 0));
				break;
			}

			// If right hit data end, match should be true if left is at null terminator and false otherwise
			if (r >= rend)
			{
				matched = (*l == 0);
				break;
			}

			// get characters
			char lc = *l;
			char rc = *r;
			matched = (lc == rc);

			// Break on null terminator
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;
		}

		return matched;
	}

	bool Text::equal(const char16_t *text, std::size_t length) const noexcept
	{
		return cio::text(mElements, mCapacity) == cio::text(text, length);
	}

	bool Text::equal(const char32_t *text, std::size_t length) const noexcept
	{
		return cio::text(mElements, mCapacity) == cio::text(text, length);
	}

	bool Text::equalInsensitive(const char *text) const noexcept
	{
		bool matched = true;

		if (text)
		{
			const char *l = mElements;
			const char *r = text;

			const char *lend = mElements + mCapacity;

			while (matched)
			{
				// If left hit data end, match should be true if right is also at null terminator, and false otherwise
				if (l >= lend)
				{
					matched = (*r == 0);
					break;
				}

				// force characters to upper case for comparison
				char lc = std::toupper(*l);
				char rc = std::toupper(*r);
				matched = (lc == rc);

				// Break on null terminator
				if (lc == 0)
				{
					break;
				}

				++l;
				++r;
			}
		}
		else
		{
			matched = !mElements || (mElements[0] == 0);
		}

		return matched;
	}

	bool Text::equalInsensitive(const std::string &text) const noexcept
	{
		return this->equalInsensitive(text.c_str());
	}

	bool Text::equalInsensitive(const Text &text) const noexcept
	{
		bool matched = true;

		const char *l = mElements;
		const char *r = text.mElements;

		const char *lend = mElements + mCapacity;
		const char *rend = text.mElements + text.mCapacity;

		while (matched)
		{
			// If left hit data end, match should be true if right is also at data end or null terminator, and false otherwise
			if (l >= lend)
			{
				matched = (r >= rend || (*r == 0));
				break;
			}

			// If right hit data end, match should be true if left is at null terminator and false otherwise
			if (r >= rend)
			{
				matched = (*l == 0);
				break;
			}

			// force characters to upper case for comparison
			char lc = std::toupper(*l);
			char rc = std::toupper(*r);
			matched = (lc == rc);

			// Break on null terminator
			if (lc == 0)
			{
				break;
			}

			++l;
			++r;
		}

		return matched;
	}

	bool Text::equal(const char *text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->equal(text) : this->equalInsensitive(text);
	}

	bool Text::equal(const std::string &text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->equal(text.c_str()) : this->equalInsensitive(text.c_str());
	}

	bool Text::equal(const Text &text, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->equal(text) : this->equalInsensitive(text);
	}

	std::size_t Text::find(char c, std::size_t off) const noexcept
	{
		std::size_t found = std::string::npos;
		std::size_t limit = this->strlen();

		for (std::size_t i = off; i < limit; ++i)
		{
			if (mElements[i] == c)
			{
				found = i;
				break;
			}
		}

		return found;
	}

	std::size_t Text::find(const Text &text, std::size_t off) const noexcept
	{
		std::size_t found = std::string::npos;
		std::size_t length = text.strlen();
		std::size_t ours = this->strlen();
		std::size_t i = off;
		while (i + length <= ours)
		{
			if (std::memcmp(mElements + i, text.mElements, length) == 0)
			{
				found = i;
				break;
			}

			++i;
		}
		return found;
	}

	std::size_t Text::rfind(char c, std::size_t off) const noexcept
	{
		std::size_t found = std::string::npos;
		std::size_t limit = std::min(off, this->strlen());

		for (std::size_t i = limit; i > 0; --i)
		{
			if (mElements[i - 1] == c)
			{
				found = i - 1;
				break;
			}
		}

		return found;
	}

	bool Text::contains(char c) const noexcept
	{
		bool found = false;
		const char *current = mElements;
		const char *end = mElements + mCapacity;

		while (current < end && *current && !found)
		{
			found = (*current == c);
			++current;
		}

		return found;
	}

	bool Text::containsInsensitive(char c) const noexcept
	{
		bool found = false;
		const char cu = std::toupper(c);
		const char *current = mElements;
		const char *end = mElements + mCapacity;

		while (current < end && *current && !found)
		{
			found = (std::toupper(*current) == cu);
			++current;
		}

		return found;
	}

	bool Text::contains(char c, Case mode) const noexcept
	{
		return mode == Case::Sensitive ? this->contains(c) : this->containsInsensitive(c);
	}

	Text Text::trimPrefix(char c, std::size_t count) const noexcept
	{
		std::size_t current = 0;

		while (count > 0 && current < mCapacity && mElements[current] == c)
		{
			++current;
			--count;
		}

		return Text(mElements + current, mCapacity - current);
	}

	Text Text::trimSuffix(char c, std::size_t count) const noexcept
	{
		std::size_t end = this->strlen();
		std::size_t current = end;

		while (count > 0 && current > 0 && mElements[current - 1] == c)
		{
			--current;
			--count;
		}

		return Text(mElements, current);
	}

	bool Text::startsWith(char prefix) const noexcept
	{
		return mCapacity > 0 && mElements[0] == prefix;
	}

	bool Text::startsWith(const Text &prefix) const noexcept
	{
		std::size_t theirs = prefix.strlen();
		bool matches = (theirs == 0);
		if (theirs > 0)
		{
			std::size_t ours = this->strlen();
			matches = (ours >= theirs) && cio::equalTextInsensitive(mElements, prefix.mElements, theirs);
		}
		return matches;
	}

	bool Text::endsWith(char suffix) const noexcept
	{
		std::size_t end = this->strlen();
		return (end > 0 && mElements[end - 1] == suffix);
	}

	bool Text::endsWith(const Text &suffix) const noexcept
	{
		std::size_t theirs = suffix.strlen();
		bool matches = (theirs == 0);
		if (theirs > 0)
		{
			std::size_t ours = this->strlen();
			matches = (ours >= theirs && cio::equalText(mElements + (ours - theirs), suffix.mElements, theirs));
		}
		return matches;
	}

	const Text &Text::print() const noexcept
	{
		return *this;
	}

	std::size_t Text::print(char *buffer, std::size_t capacity) const noexcept
	{
		std::size_t length = this->strlen();
		cio::reprint(mElements, length, buffer, capacity);
		return length;
	}

	const Text &Text::swapBytes() const noexcept
	{
		return *this;
	}

	Text &Text::applyByteSwap() noexcept
	{
		return *this;
	}

	std::ostream &operator<<(std::ostream &s, const Text &text)
	{
		std::size_t len = text.strlen();
		return s.write(text.data(), len);
	}

	bool operator==(const Text &left, const Text &right) noexcept
	{
		return left.equal(right);
	}

	bool operator==(const char *left, const Text &right) noexcept
	{
		return right.equal(left);
	}

	bool operator==(const Text &left, const char *right) noexcept
	{
		return left.equal(right);
	}

	bool operator==(const std::string &left, const Text &right) noexcept
	{
		return right.equal(left);
	}

	bool operator==(const Text &left, const std::string &right) noexcept
	{
		return left.equal(right);
	}

	bool operator!=(const Text &left, const Text &right) noexcept
	{
		return !left.equal(right);
	}
	bool operator!=(const char *left, const Text &right) noexcept
	{
		return !right.equal(left);
	}

	bool operator!=(const Text &left, const char *right) noexcept
	{
		return !left.equal(right);
	}

	bool operator!=(const std::string &left, const Text &right) noexcept
	{
		return !right.equal(left);
	}

	bool operator!=(const Text &left, const std::string &right) noexcept
	{
		return !left.equal(right);
	}

	bool operator<(const Text &left, const Text &right) noexcept
	{
		return left.compare(right) < 0;
	}

	bool operator<=(const Text &left, const Text &right) noexcept
	{
		return left.compare(right) <= 0;
	}

	bool operator>(const Text &left, const Text &right) noexcept
	{
		return left.compare(right) > 0;
	}

	bool operator>=(const Text &left, const Text &right) noexcept
	{
		return left.compare(right) >= 0;
	}
}
