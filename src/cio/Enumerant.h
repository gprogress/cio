/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ENUMERANT_H
#define CIO_ENUMERANT_H

#include "Types.h"

#include "Metatypes.h"

namespace cio
{
	/**
	 * The Enumerant<E> is a data structure for associating a C++ enum or enum class value with a text label and a human-readable description.
	 * It is primarily used to implement backend tables for compile-time enumeration metadata.
	 *
	 * @tparam E The enum class type
	 */
	template <typename E>
	class Enumerant
	{
		public:
			/** The actual enum value */
			const E value;

			/** The label for the enum as presented in code */
			const char *label;

			/** The human readable description for the enum */
			const char *description;

			/** The text value of the enum to print if different from the label */
			const char *text;

			/**
			 * Gets the printed UTF-8 text length of the enumerant.
			 * 
			 * @return the UTF-8 text length
			 */
			inline std::size_t strlen() const noexcept
			{
				return label ? std::strlen(this->print()) : 0;
			}

			/**
			 * Prints the enumeration to a UTF-8 string.
			 * This returns the text if set, or the label if not.
			 * 
			 * @return the printed text
			 */
			inline const char *print() const noexcept
			{
				return this->text ? this->text : this->label;
			}

			/**
			 * Prints the enumeration to a UTF-8 text buffer.
			 * This prints the text if set, or the label if not.
			 *
			 * @param buffer The text buffer
			 * @param capacity The characters available in the text buffer
			 * @return the number of characters needed for the text
			 */
			inline std::size_t print(char *buffer, std::size_t capacity) const noexcept
			{
				const char *text = this->print(value);
				std::size_t length = std::strlen(text);
				cio::reprint(text, length, buffer, capacity);
				return length;
			}
	};
}

#endif
