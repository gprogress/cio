/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Status.h"

#include "Enumeration.h"

#include <istream>
#include <ostream>

namespace cio
{
	const Enumerant<Status> sStatusEntries[] =
	{
		{ Status::None, "None", "No operation was requested" },
		{ Status::Requested, "Requested", "The operation has been requested but has not yet started" },
		{ Status::Submitted, "Submitted", "The operation has been submitted for execution but not yet started" },
		{ Status::Started, "Started", "The operation has been started" },
		{ Status::Updated, "Updated", "The operation has made a progress update" },
		{ Status::Resumed, "Resumed", "The operation has been resumed" },
		{ Status::Interrupted, "Interrupted", "The operation was interrupted" },
		{ Status::Paused, "Paused", "The operation was paused by an external request" },
		{ Status::Canceled, "Canceled", "The operation has been canceled but not yet completed execution" },
		{ Status::Completed, "Completed", "The operation completed execution" },
	};

	const char *print(Status status) noexcept
	{
		return sStatusEntries[static_cast<unsigned>(status)].label;
	}
	
	const char *getDescription(Status status) noexcept
	{
		return sStatusEntries[static_cast<unsigned>(status)].description;
	}

	std::ostream &operator<<(std::ostream &s, Status status)
	{
		return s << sStatusEntries[static_cast<unsigned>(status)].label;
	}
}
