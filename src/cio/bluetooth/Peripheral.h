/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_PERIPHERAL_H
#define CIO_BLUETOOTH_PERIPHERAL_H

#include "Types.h"

#include <cio/DeviceAddress.h>

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Peripheral class provides a summary representation of a single peripheral device
		 * advertising report. It is primarily geared toward low energy (BLE) peripheral advertisements but can represent
		 * classic advertisements as well.
		 *
		 * Peripherals will always have the address and address type of the remote device.
		 * Additional peripheral parameters are generally only present for peripherals created from BLE advertisements and it's
		 * up to the remote device as to what is actually included.
		 */
		class CIO_BLUETOOTH_API Peripheral
		{
			public:
				/**
				 * Construct an empty peripheral.
				 */
				Peripheral() noexcept;
				
				/**
				 * Constructs a default peripheral for the given peripheral address.
				 * The address type will default to Public and all other settings are initialized
				 * to a reasonable default.
				 *
				 * This method also enables implicit casting from DeviceAddress to Peripheral.
				 *
				 * @param address The peripheral address
				 */
				Peripheral(const DeviceAddress &address) noexcept;
				
				/**
				 * Constructs a default peripheral for the given peripheral address and type.
				 *
				 * @param address The peripheral address
				 * @param type The peripheral address type
				 */
				Peripheral(const DeviceAddress &address, AddressType type) noexcept;
				
				/**
				 * Construct a copy of an peripheral.
				 *
				 * @param in The peripheral to copy
				 * @throw std::bad_alloc If the copy required allocating memory and that failed
				 */
				Peripheral(const Peripheral &in);
				
				/**
				 * Copy an peripheral.
				 *
				 * @param in The peripheral to copy
				 * @return this peripheral
				 * @throw std::bad_alloc If the copy required allocating memory and that failed
				 */
				Peripheral &operator=(const Peripheral &in);
				
				/**
				 * Destructor.
				 */
				~Peripheral() noexcept;
				
				/**
				 * Clears all state and restores this to an empty peripheral.
				 */
				void clear() noexcept;
				
				/**
				 * Gets the remote device address.
				 *
				 * @return the remote device address
				 */
				const DeviceAddress &getAddress() const noexcept;
				
				/**
				 * Sets the remote device address.
				 *
				 * @param address The address
				 */
				void setAddress(const DeviceAddress &address) noexcept;
				
				/**
				 * Gets the remote address type.
				 * If Classic, this peripheral represents a BR/EDR "Classic" scan response.
				 * If Public or Random, this peripheral represents a BLE peripheral packet.
				 *
				 * @return the remote address type
				 */
				AddressType getAddressType() const noexcept;
				
				/**
				 * Sets the remote address type.
				 * If Classic, this peripheral represents a BR/EDR "Classic" scan response.
				 * If Public or Random, this peripheral represents a BLE peripheral packet.
				 *
				 * @param type the remote address type
				 */
				void setAddressType(AddressType type) noexcept;
				
				/**
				 * Checks whether this peripheral has a remote device name set.
				 *
				 * @return whether the device name was set
				 */
				bool hasName() const noexcept;
				
				/**
				 * Checks whether this peripheral has a remote device name set.
				 *
				 * @return whether the device name was set
				 */
				const char *getShortName() const noexcept;
				
				/**
				 * Sets the short name of the remote device.
				 * At most 7 characters will be copied from the text up to the null terminator.
				 *
				 * @param text The short name to set
				 */
				void setShortName(const char *text) noexcept;
				
				/**
				 * Sets the short name of the remote device.
				 * At most 7 characters will be copied from the text up to given input length.
				 *
				 * @param text The short name to set
				 * @param length The number of bytes in the short name
				 */
				void setShortName(const char *text, std::size_t length) noexcept;
				
				/**
				 * Gets the signal strength for the peripheral in dBm.
				 * This is typically a negative value with a strong signal being more than -50 dBm
				 * and a usable signal being more than -80 dBm.
				 *
				 * If the peripheral is not detectable, the lowest possible value (-32768) is returned.
				 *
				 * @return the signal strength
				 */
				std::int16_t getSignalStrength() const noexcept;
				
				/**
				 * Sets the signal strength for the peripheral in dBm.
				 * This is typically a negative value with a strong signal being more than -50 dBm
				 * and a usable signal being more than -80 dBm.
				 *
				 * @param strength the signal strength
				 */
				void setSignalStrength(std::int16_t strength) noexcept;
				
				/**
				 * Clears the signal strength by setting it to the lowest possible value (-32768).
				 */
				void clearSignalStrength() noexcept;
				
				/**
				 * Gets the minimum preferred connection interval for the peripheral device.
				 * This is in the native format of the advertisements, which is a 16-bit value in units of 1.25 ms.
				 * The valid range for this value is 0x0006 to 0x0C80, with 0xFFFF indicating that there is no specific minimum.
				 * This value is usually dictated by battery life constraints.
				 * The default is 0xFFFF to indicate no minimum was set.
				 *
				 * @return the preferred minimum connection interval
				 */
				std::uint16_t getMinConnectionInterval() const noexcept;
				
				/**
				 * Sets the minimum preferred connection interval for the peripheral device.
				 * This is in the native format of the advertisements, which is a 16-bit value in units of 1.25 ms.
				 * The valid range for this value is 0x0006 to 0x0C80, with 0xFFFF indicating that there is no specific minimum.
				 * This method does not enforce the valid range.
				 *
				 * @param value the preferred minimum connection interval in units of 1.25 ms
				 */
				void setMinConnectionInterval(std::uint16_t value) noexcept;
				
				/**
				 * Gets the maximum preferred connection interval for the peripheral device.
				 * This is in the native format of the advertisements, which is a 16-bit value in units of 1.25 ms.
				 * The valid range for this value is 0x0006 to 0x0C80, with 0xFFFF indicating that there is no specific minimum.
				 * This value is usually dictated by available hardware buffer sizes on the device.
				 * The default is 0xFFFF to indicate no maximum was set.
				 *
				 * @return the preferred maximum connection interval
				 */
				std::uint16_t getMaxConnectionInterval() const noexcept;
				
				/**
				 * Sets the maximum preferred connection interval for the peripheral device.
				 * This is in the native format of the advertisements, which is a 16-bit value in units of 1.25 ms.
				 * The valid range for this value is 0x0006 to 0x0C80, with 0xFFFF indicating that there is no specific maximum.
				 * This method does not enforce the valid range.
				 *
				 * @param value the preferred maximum connection interval in units of 1.25 ms
				 */
				void setMaxConnectionInterval(std::uint16_t value) noexcept;
				
				/**
				 * Gets the bitset of flags from a BLE peripheral.
				 *
				 * @return the BLE peripheral flags
				 */
				std::uint8_t getLowEnergyFlags() const noexcept;
				
				/**
				 * Sets the bitset of flags for a BLE peripheral.
				 *
				 * @param value the BLE peripheral flags
				 */
				void setLowEnergyFlags(std::uint8_t value) noexcept;
				
				/**
				 * Gets whether this peripheral has enabled BLE limited discoverable mode.
				 * This is part of the BLE peripheral flags bitset.
				 * The default is off.
				 *
				 * @return whether this peripheral has enabled BLE limited discoverable mode
				 */
				bool getLimitedDiscoverableMode() const noexcept;
				
				/**
				 * Sets whether this peripheral has enabled BLE limited discoverable mode.
				 * This is part of the BLE peripheral flags bitset.
				 *
				 * @param value whether this peripheral should enable BLE limited discoverable mode
				 */				
				void setLimitedDiscoverableMode(bool value) noexcept;
				
				/**
				 * Gets whether this peripheral has enabled BLE general discoverable mode.
				 * This is part of the BLE peripheral flags bitset.
				 * The default is off.
				 *
				 * @return whether this peripheral has enabled BLE general discoverable mode
				 */
				bool getGeneralDiscoverableMode() const noexcept;
				
				/**
				 * Sets whether this peripheral has enabled BLE general discoverable mode.
				 * This is part of the BLE peripheral flags bitset.
				 *
				 * @param value whether this peripheral should enable BLE general discoverable mode
				 */	
				void setGeneralDiscoverableMode(bool value) noexcept;
				
				/**
				 * Gets whether this peripheral has enabled classic BR/EDR support.
				 * This is always true if the address type is Classic but can also be true for BLE modes.
				 * This is part of the BLE peripheral flags bitset.
				 * The default is on.
				 *
				 * @return whether this peripheral has enabled BR/EDR "Classic" support
				 */
				bool getClassicSupported() const noexcept;
				
				/**
				 * Sets whether this peripheral has enabled classic BR/EDR support.
				 * The value is ignored if the address type is Classic since Classic BR/EDR is always supported in that case.
				 * This is part of the BLE peripheral flags bitset.
				 *
				 * @param value whether this peripheral has enabled BR/EDR "Classic" support
				 */
				void setClassicSupported(bool value) noexcept;
				
				/**
				 * Gets whether this peripheral has marked that the host is capable of simultaneous Classic and Low Energy modes.
				 * This is part of the BLE peripheral flags bitset.
				 *
				 * @return whether the device is capable of simultaneous BR/EDR and LE modes
				 */
				bool getSimultaneousCapable() const noexcept;
				
				/**
				 * Sets whether this peripheral has marked that the host is capable of simultaneous Classic and Low Energy modes.
				 * This is part of the BLE peripheral flags bitset.
				 *
				 * @param value whether the device is capable of simultaneous BR/EDR and LE modes
				 */
				void setSimultaneousCapable(bool value) noexcept;
				
				/**
				 * Checks whether this peripheral is currently set to a low energy address mode.
				 *
				 * @return whether peripheral is low energy
				 */
				bool isLowEnergy() const noexcept;
				
				/**
				 * Prints this peripheral to the given buffer.
				 * This prints the remote device address if one was set.
				 *
				 * @param buffer The text buffer to print into
				 * @param length The length of the text buffer
				 * @return the actual length that would be needed to fully print the value
				 */
				std::size_t print(char *buffer, std::size_t length) const noexcept;
				
				/**
				 * Interprets the peripheral in a Boolean context.
				 * It is considered true if the device address is valid.
				 *
				 * @return whether the device address is valid
				 */
				explicit operator bool() const noexcept;
				
			private:
				/** The remote device address */
				DeviceAddress mAddress;
				
				/** The short name of the remote device */
				char mShortName[8];
				
				/** The most recent RSSI value */
				std::int16_t mSignalStrength;
				
				/** The preferred minimum connection interval in units of 1.25 ms */
				std::uint16_t mMinConnectionInterval;
				
				/** The preferred maximum connection interval in units of 1.25 ms */
				std::uint16_t mMaxConnectionInterval;
								
				/** The remote device address type */
				AddressType mAddressType;
				
				/** The BLE peripheral flags bitset */
				std::uint8_t mLowEnergyFlags;
		};
		
		/**
		 * Checks to see if the two peripherals are identical.
		 * This checks both the address and all settings.
		 *
		 * @param left The first peripheral
		 * @param right The second peripheral
		 * @return whether the peripherals are identical
		 */
		CIO_BLUETOOTH_API bool operator==(const Peripheral &left, const Peripheral &right) noexcept;
		
		/**
		 * Checks to see if the two peripherals are not identical.
		 * This checks both the address and all settings.
		 *
		 * @param left The first peripheral
		 * @param right The second peripheral
		 * @return whether the peripherals have any differences
		 */
		CIO_BLUETOOTH_API bool operator!=(const Peripheral &left, const Peripheral &right) noexcept;
		
		/**
		 * Prints a peripheral to a C++ stream.
		 *
		 * @param stream The stream
		 * @param p The peripheral
		 * @return the stream
		 * @throw std::exception If the stream throws an exception
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, const Peripheral &p);
	}
}

#endif

