/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PRINT_H
#define CIO_PRINT_H

#include "Types.h"

#include "Case.h"
#include "Character.h"
#include "Metatypes.h"
#include "Order.h"
#include "PrintProperties.h"

#include <algorithm>
#include <string>
#include <sstream>

/**
 * @file The CIO Print.h file provides common utility methods for efficiently printing values to UTF-8 text,
 * minimizing memory allocation and overhead where possible.
 */

namespace cio
{
	// Generic methods - these define the core API, which may then be overloaded for specific types

	/**
	 * Calculates the exact printing length of the specified value for UTF-8 text without any trailing null-terminator.
	 * This should be done as efficiently as possible without actually printing anything.
	 * 
	 * The return value of this method must be convertible to std::size_t.
	 * If the actual number of bytes to print is constant, this should be returned as std::integral_constant<std::size_t, N> instead.
	 * The return value for this method should give the same answer as print(value, nullptr, 0).
	 * 
	 * This method attempts to call value.strlen() and return the result.
	 * If this fails to compile because the type is not a class or does not have a compatible method,
	 * this method will not be used and other overloads (if available) will be selected.
	 *
	 * @tparam T The value type
	 * @param value The value to print
	 * @return The number of UTF-8 characters needed to print the value, not counting any null terminator
	 */
	template <typename T>
	inline auto strlen(const T &value) noexcept -> decltype(value.strlen())
	{
		return value.strlen();
	}

	/**
	 * Prints the specified value to a UTF-8 text buffer with a maximum capacity.
	 * 
	 * The actual number of bytes to print the value will be calculated and returned,
	 * even if the buffer is not large enough or is null. This can be used to incrementally resize
	 * buffers. The return value for this method should give the same answer as strlen(value).
	 * 
	 * The return value of this method must be convertible to std::size_t.
	 * If the actual number of bytes to print is constant, this should be returned as std::integral_constant<std::size_t, N> instead.
	 * 
	 * If the buffer is larger than needed, a null terminator byte will be written as a courtesy.
	 * 
	 * This method attempts to call value.print(buffer, capacity) and return the result.
	 * If this fails to compile because the type is not a class or does not have a compatible method,
	 * this method will not be used and other overloads (if available) will be selected.
	 * 
	 * This file adds overloads for primitive and other common types.
	 * Other overloads may be added non-intrusively in the same space as the object you want to print.
	 *
	 * @tparam T The value type
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of UTF-8 characters that is needed to print the value
	 */
	template <typename T>
	inline auto print(const T &value, char *buffer, std::size_t capacity) noexcept -> decltype(value.print(buffer, capacity))
	{
		return value.print(buffer, capacity);
	}

	/**
	 * Prints the specified value to be returned as a UTF-8 text object.
	 * The object must be convertible to C++ std::string.
	 * 
	 * If possible, the returned text should avoid memory allocations - text constants should
	 * be returned as const char * and printed fixed-length text should be returned as cio::FixedText.
	 * 
	 * The general implementation attempts to call value.print() and return the result,
	 * which allows the value to decide how to best implemet it. If this fails to compile, this
	 * overload is not considered and other print overloads (if available) may be used.
	 *
	 * @tparam T The value type
	 * @param value The value to print
	 * @return The printed text
	 * @throw std::bad_alloc If memory allocation was needed but failed
	 */
	template <typename T>
	inline auto print(const T &value) -> decltype(value.print())
	{
		return value.print();
	}

	/**
	 * Prints the lowest decimal digit from the given value.
	 * 
	 * @param value The value to print
	 * @return The character from '0' to '9'
	 */
	constexpr char printDigit(unsigned value) noexcept
	{
		return (value % 10u) + '0';
	}

	/**
	 * Prints the lowest hexadecimal digit from the given value to the default letter case.
	 * This is also the low 4 bits (nibble) of a byte.
	 * The default case is uppercase.
	 *
	 * @param value The value to print
	 * @return The character from '0' to '9' or 'A' to 'F'>
	 */
	constexpr char printHexDigit(unsigned value) noexcept
	{
		const unsigned nibble = value & 0xFu;
		return (nibble < 10u) ? (nibble + '0') : (nibble - 10u + 'A');
	}
	
	/**
	 * Prints the lowest hexadecimal digit from the given value using uppercase digits.
	 * This is also the low 4 bits (nibble) of a byte.
	 *
	 * @param value The integer containing the 4-bit value
	 * @param c The case
	 * @return The hexadecimal character representing the low 4 bits
	 */
	constexpr char printHexDigit(unsigned value, Uppercase c) noexcept
	{
		const unsigned nibble = value & 0xFu;
		return (nibble < 10u) ? (nibble + '0') : (nibble - 10u + 'A');
	}

	/**
	 * Prints the lowest hexadecimal digit from the given value using lowercase digits.
	 * This is also the low 4 bits (nibble) of a byte.
	 *
	 * @param value The integer containing the 4-bit value
	 * @param c The case
	 * @return The hexadecimal character representing the low 4 bits
	 */
	constexpr char printHexDigit(unsigned value, Lowercase c) noexcept
	{
		const unsigned nibble = value & 0xFu;
		return (nibble < 10u) ? (nibble + '0') : (nibble - 10u + 'a');
	}
	
	/**
	 * Prints the lowest hexadecimal digit from the given value using the given letter case for digits.
	 * This is also the low 4 bits (nibble) of a byte.
	 * If the case is not explicitly set to Uppercase or Lowercase, the default of Uppercase is used.
	 *
	 * @param value The integer containing the 4-bit value
	 * @param c The case
	 * @return The hexadecimal character representing the low 4 bits
	 */
	constexpr char printHexDigit(unsigned value, Case c) noexcept
	{
		const unsigned nibble = value & 0xFu;
		return (nibble < 10u) ? (nibble + '0') : (nibble - 10u + (c == Case::Lower ? 'a' : 'A'));
	}

	/**
	 * Prints an 8-bit byte value as two hexadecimal digits to the returned text buffer.
	 * The digits are printed using the default upper case.
	 *
	 * @param value The byte to print
	 * @return The printed text
	 */
	inline FixedText<2> printByte(unsigned value) noexcept
	{
		FixedText<2> text;
		text[0] = printHexDigit(value >> 4);
		text[1] = printHexDigit(value);
		return text;
	}
	
	/**
	 * Prints an 8-bit byte value as two hexadecimal digits to the given text buffer.
	 * The buffer must have at least two bytes available.
	 * The digits are printed using the default case, which is currently uppercase.
	 *
	 * @param value The byte to print
	 * @param text The text buffer to print the two digits to
	 * @return The pointer to the text buffer after the two printed digits
	 */
	inline char *printByte(unsigned value, char *text) noexcept
	{
		text[0] = printHexDigit(value >> 4);
		text[1] = printHexDigit(value);
		return text + 2;
	}

	/**
	 * Prints an 8-bit byte value as two uppercase hexadecimal digits to the given text buffer.
	 * The buffer must have at least two bytes available.
	 *
	 * @param value The byte to print
	 * @param text The text buffer to print the two digits to
	 * @param c The case
	 * @return The pointer to the text buffer after the two printed digits
	 */
	inline char *printByte(unsigned value, char *text, Uppercase c) noexcept
	{
		text[0] = printHexDigit(value >> 4, c);
		text[1] = printHexDigit(value, c);
		return text + 2;
	}
		
	/**
	 * Prints an 8-bit byte value as two lowercase hexadecimal digits to the given text buffer.
	 * The buffer must have at least two bytes available.
	 *
	 * @param value The byte to print
	 * @param text The text buffer to print the two digits to
	 * @param c The case
	 * @return The pointer to the text buffer after the two printed digits
	 */
	inline char *printByte(unsigned value, char *text, Lowercase c) noexcept
	{
		text[0] = printHexDigit(value >> 4, c);
		text[1] = printHexDigit(value, c);
		return text + 2;
	}
	
	/**
	 * Prints an 8-bit byte value as two hexadecimal digits using the specified case to the given text buffer.
	 * The buffer must have at least two bytes available.
	 * If the case is not specifically upper or lower, the default case (currently upper) is used.
	 *
	 * @param value The byte to print
	 * @param text The text buffer to print the two digits to
	 * @param c The case
	 * @return The pointer to the text buffer after the two printed digits
	 */
	inline char *printByte(unsigned value, char *text, Case c) noexcept
	{
		text[0] = printHexDigit(value >> 4, c);
		text[1] = printHexDigit(value, c);
		return text + 2;
	}

	/**
	 * Prints a sequence of bytes in native order as hexadecimal digits into a returned text string.
	 *
	 * @param bytes The input byte buffer
	 * @param len The number of input bytes
	 * @return The printed hexadecimal text representing the byte sequence
	 * @throw std::bad_alloc If memory could not be allocated for the output string
	 */
	CIO_API std::string printBytes(const void *bytes, std::size_t len);

	/**
	 * Prints a sequence of bytes in native order as hexadecimal digits into a provided text buffer.
	 * If the text buffer is not large enough to represent the full sequence, the function returns how many bytes would have been needed
	 * and the result is truncated to the available text length.
	 *
	 * @param bytes The input byte buffer
	 * @param len The number of input bytes
	 * @param text The output text buffer
	 * @param textlen The number of output text bytes available for printing
	 * @return The number of text bytes actually needed to print the full byte sequence
	 */
	CIO_API std::size_t printBytes(const void *bytes, std::size_t len, char *text, std::size_t textlen) noexcept;

	/**
	 * Prints a sequence of bytes in reverse order as hexadecimal digits into a returned text string.
	 *
	 * @param bytes The input byte buffer
	 * @param len The number of input bytes
	 * @return The printed hexadecimal text representing the reversed byte sequence
	 * @throw std::bad_alloc If memory could not be allocated for the output string
	 */
	CIO_API std::string printSwappedBytes(const void *bytes, std::size_t len);

	/**
	 * Prints a sequence of bytes in reverse order as hexadecimal digits into a provided text buffer.
	 * If the text buffer is not large enough to represent the full sequence, the function returns how many bytes would have been needed
	 * and the result is truncated to the available text length.
	 *
	 * @param bytes The input byte buffer
	 * @param len The number of input bytes
	 * @param text The output text buffer
	 * @param textlen The number of output text bytes available for printing
	 * @return The number of text bytes actually needed to print the full reversed byte sequence
	 */
	CIO_API std::size_t printSwappedBytes(const void *bytes, std::size_t len, char *text, std::size_t textlen) noexcept;

	/**
	 * Trims unnecessarily trailing zeros from a printed floating point text buffer.
	 * This will ensure that no trailing zeros are present after the decimal point, except a single zero
	 * if the printed value is an integer (ignoring any exponent section).
	 * 
	 * @param buffer The printed floating point text
	 * @param length The length of the printed text
	 * @return The adjusted size of the printed text, which will either be the same or reduced
	 */
	CIO_API std::size_t trimFloatZeros(char *buffer, std::size_t length) noexcept;

	/**
	 * Prints a floating point number special value such as NaN, or +/- Infinity to a UTF-8 buffer.
	 * If the float is not a special value, this does nothing and returns 0 bytes printed.
	 *
	 * @param value The floating point value
	 * @param buffer The UTF-8 buffer to receive printed text
	 * @param length The number of characters in the buffer
	 * @return The number of characters that is actually needed to print the value, or 0 if it is not a special value
	 */
	CIO_API std::size_t printSpecialFloat(float value, char *buffer, std::size_t length) noexcept;

	/**
	 * Prints a floating point number special value such as NaN, or +/- Infinity to a UTF-8 buffer.
	 * If the float is not a special value, this does nothing and returns 0 bytes printed.
	 *
	 * @param value The floating point value
	 * @param buffer The UTF-8 buffer to receive printed text
	 * @param length The number of characters in the buffer
	 * @return The number of characters that is actually needed to print the value, or 0 if it is not a special value
	 */
	CIO_API std::size_t printSpecialFloat(double value, char *buffer, std::size_t length) noexcept;

	// Overloads for built-in types for strlen
	// Character (char, char16_t, char32_t) overloads are in Character.h

	/**
	 * Directly computes the UTF-8 text length of a C++ UTF-8 string.
	 * This delegates to value.size().
	 *
	 * @param value The text
	 * @return The UTF-8 text length of the text
	 */
	inline std::size_t strlen(const std::string &value) noexcept
	{
		return value.size();
	}

	/**
	 * Directly computes the UTF-8 text length of a boolean value.
	 * This is either 4 for "true" or 5 for "false".
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	constexpr std::size_t strlen(bool value) noexcept
	{
		return value ? 4 : 5;
	}

	/**
	 * Directly computes the UTF-8 text length of an unsigned char value.
	 * Unlike with traditional C/C++ printing, it is always formatted as an integer.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(unsigned char value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an signed char value.
	 * Unlike with traditional C/C++ printing, it is always formatted as an integer.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(signed char value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an unsigned short value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(unsigned short value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an signed short value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(short value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an unsigned int value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(unsigned value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an signed int value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(int value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an unsigned long value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(unsigned long value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an signed long value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(long value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an unsigned long long value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(unsigned long long value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of an signed long long value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(long long value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of a float value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(float value) noexcept;

	/**
	 * Directly computes the UTF-8 text length of a double value.
	 *
	 * @param value The value
	 * @return The UTF-8 text length of the value
	 */
	CIO_API std::size_t strlen(double value) noexcept;

	// Overloads for built-in types for print to buffer

	/**
	 * Prints the specified text value to a buffer with a maximum capacity.
	 * This just determines the C string length and copies it.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	inline std::size_t print(const char *value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = value ? std::strlen(value) : 0;
		reprint(value, needed, buffer, capacity);
		return needed;
	}

	/**
	 * Prints the specified text value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	inline std::size_t print(const std::string &value, char *buffer, std::size_t capacity) noexcept
	{
		reprint(value.c_str(), value.size(), buffer, capacity);
		return value.size();
	}

	/**
	 * Prints the specified Boolean value to a buffer with a maximum capacity.
	 * This prints either "true" or "false".
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	inline std::size_t print(bool value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = (value ? 4 : 5);
		reprint(value ? "true" : "false", needed, buffer, capacity);
		return needed;
	}

	/**
	 * Prints the specified unsigned char value to a buffer with a maximum capacity.
	 * Unlike with traditional C++ printing, this always treats unsigned char as an integer.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(unsigned char value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified signed char value to a buffer with a maximum capacity.
	 * Unlike with traditional C++ printing, this always treats signed char as an integer.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(signed char value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified unsigned short value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(unsigned short value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified signed short value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(short value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified unsigned int value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(unsigned value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified signed int value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(int value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified unsigned long value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(unsigned long value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified signed long value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(long value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified unsigned long long value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(unsigned long long value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified signed long long value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(long long value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified float value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(float value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the specified double value to a buffer with a maximum capacity.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value
	 */
	CIO_API std::size_t print(double value, char *buffer, std::size_t capacity) noexcept;

	// Overloads for built-in types to print to returned string

	/**
	 * Prints a Boolean value to a returned UTF-8 string.
	 * This always returns a static string literal of either "true" or "false."
	 *
	 * @param value The value
	 * @return The UTF-8 string
	 */
	inline const char *print(bool value) noexcept
	{
		return value ? "true" : "false";
	}

	/**
	 * Prints a UTF-8 C-style null terminated string to a returned UTF-8 string.
	 * This just returns the input string.
	 *
	 * @param value The text
	 * @return The input text
	 */
	inline const char *print(const char *value) noexcept
	{
		return value;
	}

	/**
	 * Prints a UTF-8 C++ string to a returned UTF-8 string.
	 * This just returns a reference to the input string.
	 *
	 * @param value The text
	 * @return The input text
	 */
	inline const std::string &print(const std::string &value) noexcept
	{
		return value;
	}

	/**
	 * Prints a UTF-8 C++ temporary string to a returned UTF-8 string.
	 * This just moves the input to the output.
	 *
	 * @param value The text
	 * @return The moved input
	 */
	inline std::string print(std::string &&value) noexcept
	{
		return std::move(value);
	}

	/**
	 * Prints a UTF-8 character to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is always 1 but the value depends on the character.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	inline FixedText<1> print(char c) noexcept
	{
		return FixedText<1>(c);
	}

	/**
	 * Prints an unsigned integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 * Unlike with traditional C++ printing, this always treats unsigned char as an integer.
	 * 
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<unsigned char> print(unsigned char value) noexcept;

	/**
	 * Prints an unsigned integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<unsigned short> print(unsigned short value) noexcept;

	/**
	 * Prints an unsigned integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<unsigned> print(unsigned value) noexcept;

	/**
	 * Prints an unsigned integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<unsigned long> print(unsigned long value) noexcept;

	/**
	 * Prints an unsigned integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<unsigned long long> print(unsigned long long value) noexcept;

	/**
	 * Prints an signed integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 * Unlike with traditional C++ printing, this always treats unsigned char as an integer.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<signed char> print(signed char value) noexcept;

	/**
	 * Prints an signed integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<short> print(short value) noexcept;

	/**
	 * Prints an signed integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<int> print(int value) noexcept;

	/**
	 * Prints an signed integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<long> print(long value) noexcept;

	/**
	 * Prints an signed integer value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<long long> print(long long value) noexcept;

	/**
	 * Prints a 32-bit floating point value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<float> print(float value) noexcept;

	/**
	 * Prints a 64-bit floating point value to returned UTF-8 text.
	 * This is returned as a fixed text buffer since the length is unknown but bounded.
	 *
	 * @param value The value to print
	 * @return The printed value
	 */
	CIO_API PrintBuffer<double> print(double value) noexcept;


	// Print overloads to target different output buffer types - these need to be last to see our overloads

	/**
	 * Prints a value to a fixed-size UTF-8 text array.
	 * The value is truncated to the array length.
	 * This delegates to the regular print method.
	 *
	 * @tparam T The value type
	 * @tparam N The array length
	 * @param value The value
	 * @param buffer The array to print to
	 * @return The number of bytes needed to actually print the value
	 */
	template <typename T, std::size_t N>
	inline auto print(const T &value, char(&buffer)[N]) noexcept -> decltype(print(value, buffer, std::integral_constant<std::size_t, N>()))
	{
		return print(value, buffer, std::integral_constant<std::size_t, N>());
	}

	/**
	 * Prints a value to a UTF-8 Fixed Text buffer.
	 * The generic implementation delegates to printing to the underlying character buffer with capacity N.
	 *
	 * @tparam T The value type
	 * @tparam N The size of the fixed text
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @return The number of characters actually needed to print
	 */
	template <typename T, std::size_t N>
	inline auto print(const T &value, FixedText<N> &buffer) noexcept -> decltype(print(value, buffer.data(), N))
	{
		return print(value, buffer.data(), std::integral_constant<std::size_t, N>());
	}

	/**
	 * Prints a value to a UTF-8 C++ string.
	 * This calls print(value) and assigns the results to the buffer.
	 *
	 * @tparam T The value type
	 * @tparam N The size of the fixed text
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @return The number of characters actually printed
	 */
	template <typename T>
	inline auto print(const T &value, std::string &buffer) noexcept -> decltype(strlen(print(value)))
	{
		auto printed = print(value);
		auto length = strlen(printed);
		buffer = std::move(printed);
		return length;
	}
}

#endif
