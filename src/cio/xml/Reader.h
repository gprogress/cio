/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_READER_H
#define CIO_XML_READER_H

#include "Types.h"

#include "Parser.h"
#include "ReaderHandler.h"

#include <cio/Input.h>
#include <cio/Path.h>

#include <thread>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		class CIO_XML_API Reader
		{
			public:
				static std::size_t getDefaultBufferSize();

				Reader();

				explicit Reader(std::unique_ptr<Input> stream);

				~Reader();

				void setInputBufferSize(std::size_t bytes);

				void setEventQueueSize(std::size_t count);

				void openFile(const cio::Path &path);

				void openStream(std::unique_ptr<Input> stream);

				void openStreamRef(Input &stream);

				void openText(std::string text);

				void openText(Text text);

				/**
				 * Reads and takes the next XML parse event regardless what of type it is.
				 *
				 * @return the next XML event
				 */
				Event readNextEvent();

				/**
				 * Reads the starting element of the next element after the current element ends.
				 * Within the current element, all content including attributes, text, and child elements are skipped.
				 * The returned event is the start of the next element.
				 *
				 * If the end of document is reached first - due to element open/close mismatches or the current element being the root element -
				 * then no event is returned.
				 *
				 * @return the next start element after the current element ends, or none if the document ran out of elements
				 */
				Event readNextElementStart();

				/**
				 * Reads the next attribute key and value within the currently open element tag.
				 * The pair is empty if not currently processing attributes.
				 *
				 * @return the pair of key and value for the next attribute, or none if not processing attributes right now
				 */
				std::pair<Event, Event> readNextAttribute();

				/**
				 * Skip all remaining attributes for the current element start tag.
				 * Has no effect if not currently inside an element start tag.
				 */
				void finishAttributes();

				/**
				 * Reads the next child of the current element regardless of what it is.
				 * This will skip any attributes still present, but return either the next text chunk, child element start, or any other direct child of the current element.
				 * If the current element end is reached, it is not removed from the event stream and no event is returned.
				 *
				 * @return the next element child, or none if the current element end is reached
				 */
				Event readNextChild();

				/**
				 * Reads the next child of the current element that is itself an element start.
				 * This will skip any attributes still present and any text chunks to reach the next child element start.
				 * If the current element end is reached, it is not removed from the event stream and no event is returned.
				 *
				 * @return the next element child element start, or none if the current element end is reached
				 */
				Event readNextChildElement();

				/**
				 * Reads the next child of the current element if it is a text chunk.
				 * This will skip any attributes still present to obtain the next chunk of freeform text.
				 * If the current element end or any non-text child elements are reached, they are not removed from the event stream and no event is returned.
				 *
				 * The parser may split longer text into multiple chunks and this method only returns the first chunk.
				 *
				 * @return the next text chunk, or none if the current element end is reached or a non-text child element is found
				 */
				Event readTextChunk();

				/**
				 * Reads and consolidates the next children of the current element if they are consecutive text chunks.
				 * This will skip any attributes still present to obtain the next chunk of freeform text.
				 * If the current element end or any non-text child elements are reached, they are not removed from the event stream and no event is returned.
				 *
				 * The parser may split longer text into multiple chunks and this method will consolidate all adjacent text chunks into a single text event.
				 *
				 * @return the next consolidated text, or none if the current element end is reached or a non-text child element is found
				 */
				Event readText();

				/**
				 * Skips all remaining content for the current element and processes and removes the element end marker from the event stream.
				 * This effectively goes up one level to make the parent element the current element.
				 */
				void finishCurrentElement();

				/**
				 * Closes the parser and ends parsing.
				 */
				void close();

				explicit operator bool() const;

			private:
				static const std::size_t sDefaultBufferSize;

				Parser mParser;
				ReaderHandler mHandler;
				std::unique_ptr<Input> mStream;
				std::string mOwnedText;
				std::thread mHandlerThread;
				std::size_t mBufferSize;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
