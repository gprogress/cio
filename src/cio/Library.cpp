#include "Library.h"

#include "Exception.h"
#include "Filesystem.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>
#elif defined CIO_HAVE_DLFCN_H
#include <dlfcn.h>
#else
#error "No dynamic library backend on this platform"
#endif

// Disable spurious warning on getenv
#if defined _MSC_VER
#pragma warning(disable: 4996)
#endif

namespace cio
{
	// Platform-independent code

	std::vector<Path> Library::getUserSearchPaths()
	{
		std::vector<Path> results;
		addSearchPaths(std::getenv(PathVariable), results);
		return results;
	}

	std::vector<Path> Library::getSystemSearchPaths()
	{
		std::vector<Path> results;
		addSearchPaths(SystemPaths, results);
		return results;
	}

	std::vector<Path> Library::getSearchPaths()
	{
		std::vector<Path> results;
		addSearchPaths(SystemPaths, results);
		addSearchPaths(std::getenv(PathVariable), results);
		return results;
	}

	void Library::addSearchPaths(Text delimitedList, std::vector<Path> &paths)
	{
		std::pair<Text, Text> split = delimitedList.split(PathDelimiter);
		while (split.first)
		{
			paths.emplace_back(std::move(split.first));
			split = split.second.split(PathDelimiter);
		}
	}

	bool Library::matchesPath(const Path &path) noexcept
	{
		return path.getFilename().startsWith(FilePrefix) && path.matchesExtension(FileExtension);
	}

	Text Library::name(const Path &path)
	{
		Text name;
		if (path.matchesExtension(FileExtension))
		{
			name = path.getFilenameNoExtension().splitAfterPrefix(FilePrefix);
		}
		return name;
	}

	Path Library::filename(Path name)
	{
		Path result = std::move(name);
		if (!result.isComplete())
		{
			Text local = result.getFilename();
			if (!local.startsWith(FilePrefix))
			{
				result.setFilename(FilePrefix + local);
			}
			result.setExtension(FileExtension);
		}

		return result;
	}

	Path Library::find(const Path &name)
	{
		Path found;

		if (name.isComplete())
		{
			found = Library::findExactMatch(name);
		}
		else
		{
			Path sanitized = Library::filename(name);
			Filesystem fs;

			// First check system search path
			Text system = Library::SystemPaths;
			if (system)
			{
				std::pair<Text, Text> split = system.split(Library::PathDelimiter);
				found = Library::findExactMatch(split.first / sanitized);
				while (!found && split.second)
				{
					split = split.second.split(Library::PathDelimiter);
					found = Library::findExactMatch(split.first / sanitized);
				}
			}

			if (!found)
			{
				Text user = std::getenv(Library::PathVariable);
				if (user)
				{
					std::pair<Text, Text> split = user.split(Library::PathDelimiter);
					found = Library::findExactMatch(split.first / sanitized);
					while (!found && split.second)
					{
						split = split.second.split(Library::PathDelimiter);
						found = Library::findExactMatch(split.first / sanitized);
					}
				}
			}
		}
		return found;
	}

	Path Library::find(const Path &name, Text search)
	{
		Path found;

		if (!name.hasProtocol() || name.isFileProtocol())
		{
			// If the name is complete, only use that path and nothing else
			if (name.isComplete())
			{
				found = Library::findExactMatch(name);
			}
			else
			{
				Path sanitized = Library::filename(name);
				std::pair<Text, Text> split = search.split(Library::PathDelimiter);
				found = Library::findExactMatch(split.first / sanitized);
				while (!found && split.second)
				{
					split = split.second.split(Library::PathDelimiter);
					found = Library::findExactMatch(split.first / sanitized);
				}
			}
		}

		return found;
	}

	Path Library::find(const Path &name, const std::vector<Path> &dirs)
	{
		Path found;

		if (!name.hasProtocol() || name.isFileProtocol())
		{
			// If the name is complete, only use that path and nothing else
			if (name.isComplete())
			{
				found = Library::findExactMatch(name);
			}
			else
			{
				Path sanitized = Library::filename(name);
				for (const Path &dir : dirs)
				{
					found = Library::findExactMatch(dir / sanitized);
					if (found)
					{
						break;
					}
				}
			}
		}

		return found;
	}

	Path Library::find(const Path &name, const Path &directory)
	{
		Path found;

		if (!name.hasProtocol() || name.isFileProtocol())
		{
			// If the name is complete, only use that path and nothing else
			if (name.isComplete())
			{
				found = Library::findExactMatch(name);
			}
			else
			{
				found = Library::findExactMatch(directory/ Library::filename(name));
			}
		}

		return found;
	}

	Path Library::findExactMatch(Path candidate)
	{
		Path found;
		Filesystem fs;
		Resource type = fs.readType(candidate);

		if (type == Resource::Link)
		{
			candidate = fs.readLink(candidate, Traversal::Recursive);
			type = fs.readType(candidate);
		}

		if (type == Resource::File)
		{
			found = std::move(candidate);
		}

		return found;
	}

	Library Library::application()
	{
		Library lib;
		lib.openCurrentApplication();
		return lib;
	}

	Path Library::findLoadedAddress(void *ptr)
	{
		Path location;
#if defined _WIN32
		HMODULE handle;
		bool success = ::GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, reinterpret_cast<LPCSTR>(ptr), &handle);
		if (success)
		{
			char text[512];
			DWORD len = ::GetModuleFileName(handle, text, 512);
			if (len)
			{
				location = std::string(text, text + len);
			}
			::FreeLibrary(handle);
		}
#else
		Dl_info info = { };
		int success = ::dladdr(ptr, &info);
		if (success)
		{
			location = info.dli_fname;
		}
#endif
		return location;
	}

	Library::Library() noexcept :
		mHandle(0)
	{
		// nothing more to do
	}

	Library::Library(const Path &name) :
		mHandle(0)
	{
		this->open(name);
	}

	Library::Library(const Path &directory, const Path &name) :
		mHandle(0)
	{
		this->open(name, directory);
	}

	Library::Library(Path path, std::uintptr_t handle) noexcept :
		mPath(std::move(path)),
		mHandle(handle)
	{
		// nothing more to do
	}

	Library::Library(const Library &in) :
		mHandle(0)
	{
		if (in.mPath)
		{
			this->openFile(in.mPath);
		}
	}

	Library::Library(Library &&in) noexcept :
		mPath(std::move(in.mPath)),
		mHandle(in.mHandle)
	{
		in.mHandle = 0;
	}

	Library &Library::operator=(const Library &in)
	{
		if (this != &in)
		{
			this->clear();
			if (in.mPath)
			{
				this->openFile(in.mPath);
			}
		}
		return *this;
	}

	Library &Library::operator=(Library &&in) noexcept
	{
		if (this != &in)
		{
			mPath = std::move(in.mPath);
			mHandle = in.mHandle;
			in.mHandle = 0;
		}
		return *this;
	}

	Library::~Library() noexcept
	{
		this->clear();
	}

	void Library::open(const Path &path)
	{
		Path resolved = this->find(path);
		if (resolved.empty())
		{
			throw Exception(Action::Discover, Reason::Missing);
		}

		this->openFile(resolved);
	}

	void Library::open(const Path &path, const Path &directory)
	{
		Path resolved = this->find(path, directory);
		if (resolved.empty())
		{
			throw Exception(Action::Discover, Reason::Missing);
		}

		this->openFile(resolved);
	}

	std::size_t Library::strlen() const noexcept
	{
		return mPath.strlen();
	}

	std::size_t Library::print(char *buffer, std::size_t capacity) const noexcept
	{
		return mPath.print(buffer, capacity);
	}

	Text Library::print() const noexcept
	{
		return mPath.c_str();
	}

	const Path &Library::getPath() const noexcept
	{
		return mPath;
	}

	std::uintptr_t Library::getHandle() const noexcept
	{
		return mHandle;
	}

	std::uintptr_t Library::takeHandle() noexcept
	{
		std::uintptr_t handle = mHandle;
		mHandle = 0;
		return handle;
	}

	bool operator==(const Library &left, const Library &right) noexcept
	{
		return left.getPath() == right.getPath();
	}

	bool operator!=(const Library &left, const Library &right) noexcept
	{
		return left.getPath() != right.getPath();
	}

	bool operator<(const Library &left, const Library &right) noexcept
	{
		return left.getPath() < right.getPath();
	}

	bool operator<=(const Library &left, const Library &right) noexcept
	{
		return left.getPath() <= right.getPath();
	}

	bool operator>(const Library &left, const Library &right) noexcept
	{
		return left.getPath() > right.getPath();
	}

	bool operator>=(const Library &left, const Library &right) noexcept
	{
		return left.getPath() >= right.getPath();
	}

	// Platform-dependent code

#if defined CIO_HAVE_WINDOWS_H

#if defined _MSC_VER
	const char *Library::FilePrefix = "";
#else
	const char *Library::FilePrefix = "lib";
#endif

	const char *Library::FileExtension = "dll";

	const char *Library::PathVariable = "PATH";

	const char *Library::SystemPaths = "";

	const char Library::PathDelimiter = ';';

	void Library::clear() noexcept
	{
		if (mHandle)
		{
			HMODULE handle = reinterpret_cast<HMODULE>(mHandle);
			::FreeLibrary(handle);
			mHandle = 0;
		}
	}

	void Library::openCurrentApplication()
	{
		this->clear();

		HMODULE handle = ::GetModuleHandle(nullptr);
		mHandle = reinterpret_cast<std::uintptr_t>(handle);
		
		Filesystem fs;
		mPath = fs.getApplicationFile();
	}

	void Library::openFile(const Path &path)
	{
		this->clear();

		if (!path.isFileProtocol())
		{
			throw Exception(Action::Open, Reason::Blocked, "Can only load local files as dynamic libraries");
		}

		Text filename = path.toNativeFile();
		HMODULE handle = ::LoadLibraryEx(filename.c_str(), nullptr, 0);
		if (!handle)
		{
			Reason error = errorWin32();
			throw Exception(Action::Open, error);
		}
		mHandle = reinterpret_cast<std::uintptr_t>(handle);
		mPath = path;
	}

	Library::Symbol Library::resolveFunction(Text name) const
	{
		if (!mHandle)
		{
			throw Exception(Action::Discover, Reason::Unopened);
		}

		HMODULE handle = reinterpret_cast<HMODULE>(mHandle);
		name.nullTerminate();
		
		FARPROC proc = ::GetProcAddress(handle, name.c_str());
		if (!proc)
		{
			Reason error = errorWin32();
			throw Exception(Action::Discover, error);
		}
		return reinterpret_cast<Library::Symbol>(proc);
	}

	Library::Symbol Library::findFunction(Text name) const noexcept
	{
		FARPROC proc = nullptr;
		if (mHandle)
		{
			HMODULE handle = reinterpret_cast<HMODULE>(mHandle);
			name.nullTerminate();
			proc = ::GetProcAddress(handle, name.c_str());
		}
		return reinterpret_cast<Library::Symbol>(proc);
	}

#elif CIO_HAVE_DLFCN_H
	const char *Library::FilePrefix = "lib";

	const char *Library::FileExtension = "so";

	const char *Library::PathVariable = "LD_LIBRARY_PATH";

	// TODO need to deal with architecture suffixes
	const char *Library::SystemPaths = "/lib:/usr/lib:/usr/local/lib";

	const char Library::PathDelimiter = ':';

	void Library::clear() noexcept
	{
		if (mHandle)
		{
			void *handle = reinterpret_cast<void *>(mHandle);
			::dlclose(handle);
			mHandle = 0;
		}
	}

	void Library::openCurrentApplication()
	{
		this->clear();

		void *handle = ::dlopen(nullptr, RTLD_NOW);
		mHandle = reinterpret_cast<std::uintptr_t>(handle);

		Filesystem fs;
		mPath = fs.getApplicationFile();
	}

	void Library::openFile(const Path &path)
	{
		if (!path.isFileProtocol())
		{
			throw Exception(Action::Open, Reason::Blocked, "Can only load local files as dynamic libraries");
		}

		Text filename = path.toNativeFile();
		void *handle = ::dlopen(filename.c_str(), RTLD_NOW);
		if (!handle)
		{
			Reason error = cio::error();
			throw Exception(Action::Open, error);
		}
		mHandle = reinterpret_cast<std::uintptr_t>(handle);
		mPath = path;
	}

	Library::Symbol Library::resolveFunction(Text name) const
	{
		if (!mHandle)
		{
			throw Exception(Action::Discover, Reason::Unopened);
		}

		void *handle = reinterpret_cast<void *>(mHandle);
		name.nullTerminate();

		void *symbol = ::dlsym(handle, name.c_str());
		if (!symbol)
		{
			Reason error = cio::error();
			throw Exception(Action::Discover, error);
		}
		return reinterpret_cast<Library::Symbol>(symbol);
	}


	Library::Symbol Library::findFunction(Text name) const noexcept
	{
		void *symbol = nullptr;
		if (mHandle)
		{
			void *handle = reinterpret_cast<void *>(mHandle);
			name.nullTerminate();
			symbol = ::dlsym(handle, name.c_str());
		}
		return reinterpret_cast<Library::Symbol>(symbol);
	}

#endif
}