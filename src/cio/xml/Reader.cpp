/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Reader.h"

namespace cio
{
	namespace xml
	{
		const std::size_t Reader::sDefaultBufferSize = 4096;

		std::size_t Reader::getDefaultBufferSize()
		{
			return sDefaultBufferSize;
		}

		Reader::Reader() :
			mBufferSize(sDefaultBufferSize)
		{
			// do nothing
		}

		Reader::Reader(std::unique_ptr<Input> stream) :
			mBufferSize(sDefaultBufferSize),
			mStream(std::move(stream))
		{
			mHandlerThread = std::thread(&Parser::parse, &mParser, std::ref(*mStream), std::ref(mHandler), mBufferSize);
		}

		Reader::~Reader()
		{
			if (mHandlerThread.joinable())
			{
				mHandler.cancel();
				mHandlerThread.join();
			}
		}

		void Reader::setInputBufferSize(std::size_t bytes)
		{
			mBufferSize = bytes;
		}

		void Reader::setEventQueueSize(std::size_t count)
		{
			mHandler.setEventQueueSize(count);
		}

		void Reader::openFile(const cio::Path &path)
		{
			this->close();
			mHandlerThread = std::thread(&Parser::parseFile, &mParser, path, std::ref(mHandler), mBufferSize);
		}

		void Reader::openStream(std::unique_ptr<Input> stream)
		{
			this->close();
			mStream = std::move(stream);
			mHandlerThread = std::thread(&Parser::parse, &mParser, std::ref(*mStream), std::ref(mHandler), mBufferSize);
		}

		void Reader::openStreamRef(Input &stream)
		{
			this->close();
			mHandlerThread = std::thread(&Parser::parse, &mParser, std::ref(stream), std::ref(mHandler), mBufferSize);
		}

		void Reader::openText(std::string text)
		{
			this->close();
			mOwnedText = std::move(text);
			mHandlerThread = std::thread(&Parser::parseText, &mParser, Text(mOwnedText), std::ref(mHandler));
		}

		void Reader::openText(Text textRef)
		{
			this->close();
			mHandlerThread = std::thread(&Parser::parseText, &mParser, textRef, std::ref(mHandler));
		}

		Event Reader::readNextEvent()
		{
			return mHandler.takeNextEvent();
		}

		Event Reader::readNextElementStart()
		{
			return mHandler.skipToTakeNextElementStart();
		}

		std::pair<Event, Event> Reader::readNextAttribute()
		{
			std::pair<Event, Event> values;
			values.first = mHandler.takeNextEvent(Type::Attribute);

			if (values.first)
			{
				values.second = mHandler.takeNextEvent(Type::Attribute);
			}

			return values;
		}

		void Reader::finishAttributes()
		{
			mHandler.skipCurrentAttributes();
		}

		Event Reader::readNextChild()
		{
			return mHandler.skipToNextChild();
		}

		Event Reader::readNextChildElement()
		{
			return mHandler.skipToNextChildElement();
		}

		Event Reader::readTextChunk()
		{
			mHandler.skipCurrentAttributes();
			return mHandler.takeNextEvent(Type::Text);
		}

		Event Reader::readText()
		{
			mHandler.skipCurrentAttributes();
			Event result = mHandler.takeNextEvent(Type::Text);

			if (result)
			{
				Event nextText = mHandler.takeNextEvent(Type::Text);

				while (nextText)
				{
					result.appendValue(nextText.getValue());
					nextText = mHandler.takeNextEvent(Type::Text);
				}
			}

			return result;
		}

		void Reader::finishCurrentElement()
		{
			mHandler.skipCurrentElement();
		}

		void Reader::close()
		{
			if (mHandlerThread.joinable())
			{
				mHandler.cancel();
				mHandlerThread.join();
			}

			mStream.reset();
			mOwnedText.clear();
		}

		Reader::operator bool() const
		{
			return static_cast<bool>(mHandler);
		}
	}
}
