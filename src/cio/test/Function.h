/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_FUNCTION_H
#define CIO_TEST_FUNCTIOn_H

#include "Types.h"

#include "Test.h"

#include <cio/Class.h>

namespace cio
{
	namespace test
	{
		/**
		 * The Function class provides the basic fixture infrastructure for unit testing by a function or static method.
		 * It is a single C++ static method or function that is executed during the class's execute method.
		 */
		template <typename F /* = void (*)()*/>
		class Function : public Test
		{
			public:
				/**
				 * Gets the declared metaclass for Test.
				 *
				 * @return the metaclass for Test
				 */
				static const Metaclass &getDeclaredMetaclass();

				Function() = default;

				/**
				 * Construct a test labeled with the given resource path and executor.
				 * The test short name is taken from the leaf filename of the path.
				 *
				 * @param path The resource path for the test
				 * @param executor The test method that will run during execute()
				 */
				Function(cio::ResourcePath path, F method);

				/**
				 * Construct a test labeled with the given resource path and executor.
				 * The test short name is taken from the leaf filename of the path.
				 *
				 * @param path The resource path for the test
				 * @param executor The test method that will run during execute()
				 */
				Function(const cio::ResourcePath &parentPath, const char *name, F method);

				/**
				 * Construct a copy of a test.
				 *
				 * @param in The test to copy
				 */
				explicit Function(const Function<F> &in) = default;

				/**
				 * Construct a test by transferring the input of a given test.
				 *
				 * @param in The test to move
				 */
				Function(Function<F> &&in) noexcept;

				/**
				 * Assign a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Function<F> &operator=(const Function<F> &in) = default;

				/**
				 * Transfer the contents of a test into this one.
				 *
				 * @param in The test to move
				 */
				Function<F> &operator=(Function<F> &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Function() noexcept override = default;

				/**
				 * Gets the metaclass for the runtime type of this Test class or subclass.
				 * This must be overridden by subclasses to enable polymorphic cloning and transfers.
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears all content of the test including executors and test cases.
				 */
				virtual void clear() noexcept override;

				/**
				 * Conducts standalone execution of the test using the given logger.
				 * This class constructs a default instance of T and then uses that as the parent context.
				 *
				 * @param logger The logger to use for notification of test start, complete, and failure events
				 * @return the highest severity of any test fault during the test conducted
				 */
				virtual Severity execute(Reporter &logger) override;

			private:
				static const Class<Function<F>> sMetaclass;

				F mFunction;
		};
	}
}

#include "Assert.h"

/* Inline implementation */
namespace cio
{
	namespace test
	{
		template <typename F>
		const Class<Function<F>> Function<F>::sMetaclass("cio/test", "Function");

		template <typename F>
		const Metaclass &Function<F>::getDeclaredMetaclass()
		{
			return sMetaclass;
		}

		template <typename F>
		Function<F>::Function(cio::ResourcePath path, F method) :
			Test(std::move(path)),
			mFunction(std::move(method))
		{
			// nothing more to do
		}

		template <typename F>
		Function<F>::Function(const cio::ResourcePath &parentPath, const char *name, F method) :
			Test(parentPath / name),
			mFunction(std::move(method))
		{
			// nothing more to do
		}


		template <typename F>
Function<F>::Function(Function<F> &&in) noexcept :
		Test(std::move(in)),
			 mFunction(std::move(in.mFunction))
		{
			// nothing more to do
		}

		template <typename F>
		Function<F> &Function<F>::operator=(Function<F> &&in) noexcept
		{
			if (&this != in)
			{
				Test::operator=(std::move(in));
				mFunction = std::move(in.mFunction);
			}
		}

		template <typename F>
		const Metaclass &Function<F>::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		template <typename F>
		void Function<F>::clear() noexcept
		{
			mFunction = F();
			Test::clear();
		}

		template <typename F>
		Severity Function<F>::execute(Reporter &logger)
		{
			Severity severity = Severity::Info;
			logger.start(*this);

			try
			{
				mFunction();
			}
			catch (const Fault &fault)
			{
				logger.fail(*this, fault);
				severity = fault.getSeverity();
			}
			catch (const std::exception &e)
			{
				logger.fail(*this, Fault(e.what(), __FILE__, __LINE__, Severity::Critical));
				severity = Severity::Critical;
			}
			catch (...)
			{
				logger.fail(*this, Fault("Unexpected exception", __FILE__, LINE__, Severity::Critical));
				severity = Severity::Critical;
			}

			logger.complete(*this, severity);
		}
	}
}

#endif
