/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_STATETEST_H
#define CIO_STATETEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	* Tests the basic functionality of the State class.
	*/
	class StateTest : public cio::test::Suite
	{
		public:
			StateTest();
			
			virtual ~StateTest() noexcept override;

			void testConstruction();
			
			void testAssignment();

			void testClear();
			
			void testPrint();
			
			void testPrintOperator();
	};
}

#endif
