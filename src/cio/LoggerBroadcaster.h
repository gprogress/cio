/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LOGGERBROADCASTER_H
#define CIO_LOGGERBROADCASTER_H

#include "Types.h"

#include "Logger.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The LoggerBroadcaster extends the base Logger class to accept logging messages and rebroadcast them to a sequence of other Loggers.
	 * This is primarily useful for implementing multi-output logging such as to log to both the console, a file, a database, or some other output together.
	 */
	class CIO_API LoggerBroadcaster : public Logger
	{
		public:

			/**
			 * Gets the metaclass for Logger.
			 *
			 * @return the metaclass for Logger
			 */
			static const Metaclass &getDeclaredMetaclass();

			/**
			 * Construct a logger with the default severity threshold and context.
			 * For debug builds, this is Severity::None.
			 * For release builds, this is Severity::Error.
			 * The default context is "Global".
			 */
			LoggerBroadcaster();

			/**
			 * Construct a logger with the default severity threshold.
			 * For debug builds, this is Severity::None.
			 * For release builds, this is Severity::Error.
			 *
			 * @param context The logger context name, which will referenced (not owned) by the logger
			 */
			explicit LoggerBroadcaster(const char *context);

			/**
			 * Construct a logger.
			 *
			 * @param context The logger context name, which will referenced (not owned) by the logger
			 * @param severity The severity threshold
			 */
			LoggerBroadcaster(const char *context, Severity severity);

			/**
			 * Destructor.
			 */
			virtual ~LoggerBroadcaster() noexcept;

			/**
			 * Clears all state attached to this logger.
			 * In the base class, this resets the severity threshold to the default and clears the output list.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this object.
			 *
			 * @return the metaclass for this object
			 */
			virtual const Metaclass &getMetaclass() const noexcept;

			/**
			 * Logs a complete message to a logger with a context, severity, and message payload.
			 * The context may be different than this logger's context to use it with chained loggers, inheritance hierarchies, etc.
			 * In the LoggerBroadcaster class, this method calls in sequence the log() message on each of its output loggers with the same parameters.
			 * Each output logger is separately checked to see if the severity level is enabled so the message may only go through for some of the outputs.
			 *
			 * If no outputs are defined, this will instead call the default Logger log() method.
			 *
			 * @param context The context (such as class name, test name, etc.)
			 * @param severity The severity of the message
			 * @param message The full message to log
			 * @return whether the message was logged to any output
			 */
			virtual bool log(const char *context, Severity severity, const char *message);

			/**
			 * Adds an output logger to this broadcaster.
			 *
			 * @param output the output logger to add
			 */
			void addOutput(Logger *output);

			/**
			 * Removes an output logger from this broadcaster.
			 *
			 * @param output the output logger to remove
			 */
			void removeOutput(Logger *output);

			/**
			 * Gets the list of output loggers.
			 *
			 * @return the output loggers
			 */
			std::vector<Logger *> &getOutputs();

			/**
			 * Gets the read-only list of output loggers.
			 *
			 * @return the output loggers
			 */
			const std::vector<Logger *> &getOutputs() const;

			/**
			 * Clears the list of output loggers.
			 */
			void clearOutputs();

		private:
			/** The metaclass for this class */
			static const Class<LoggerBroadcaster> sMetaclass;
		
			/**  The list of output loggers. */
			std::vector<Logger *> mOutputs;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
