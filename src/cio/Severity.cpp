/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Severity.h"

#include <iostream>

namespace cio
{
	namespace
	{
		const char *sSeverityText[] =
		{
			"None",
			"Debug",
			"Info",
			"Warning",
			"Error",
			"Critical",
			"Fatal"
		};
	}

	const char *print(Severity severity) noexcept
	{
		return sSeverityText[static_cast<unsigned>(severity)];
	}

	std::ostream &operator<<(std::ostream &s, Severity severity)
	{
		return s << sSeverityText[static_cast<unsigned>(severity)];
	}
}
