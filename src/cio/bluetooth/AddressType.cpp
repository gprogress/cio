/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "AddressType.h"

#include <algorithm>
#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sAddressTypeText[] = {
			"Classic",
			"Public",
			"Random",
			"None"	
		};
		
		const char *print(AddressType type) noexcept
		{
			std::size_t idx = static_cast<std::size_t>(type);
			return sAddressTypeText[std::min<std::size_t>(idx, 3u)];
		}			
		
		std::ostream &operator<<(std::ostream &stream, AddressType type)
		{
			std::size_t idx = static_cast<std::size_t>(type);
			return stream << sAddressTypeText[std::min<std::size_t>(idx, 3u)];
		}
	}
}

