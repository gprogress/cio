/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MARKUPACTION_H
#define CIO_MARKUPACTION_H

#include "Types.h"

#include <iosfwd>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The CIO Markup Action enumeration describes the actions that can generate events for particular Markup element types
	 * in the generic markup framework.
	 */
	enum class MarkupAction : std::uint8_t
	{
		/** No markup action */
		None = 0,

		/** Markup element is being started */
		Start = 1,

		/** Markup element is being ended */
		End = 2,

		/** Markup element is both started and ended in the same event */
		Full = 3,

		/** Markup element is being continued */
		Continue = 4
	};

	/**
	 * Gets the markup action label.
	 *
	 * @param type The markup action type
	 * @return the label for the markup action
	 */
	CIO_API const char *print(MarkupAction type) noexcept;

	/**
	 * Prints the markup action label.
	 *
	 * @param type The markup action type
	 * @param buffer The buffer to print to
	 * @param capacity The number of characters available in the buffer
	 * @return the number of bytes needed to print the markup type
	 */
	CIO_API std::size_t print(MarkupAction type, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the markup action label to a C++ stream.
	 *
	 * @param stream The stream to print to
	 * @param type The markup action type
	 * @return the stream after printing
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, MarkupAction type);
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
