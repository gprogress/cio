/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Descriptor.h"

#include "AttributeClient.h"

#include <cio/Class.h>

#include <algorithm>

namespace cio
{
	namespace bluetooth
	{
		Class<Descriptor> Descriptor::sMetaclass("cio::bluetooth::Descriptor");
	
		Descriptor::Descriptor() noexcept
		{
			// nothing more to do
		}
		
		Descriptor::Descriptor(std::uint16_t handle) noexcept :
			Attribute(handle)
		{
			// nothing more to do
		}
		
		Descriptor::Descriptor(const UniqueId &type) noexcept :
			Attribute(type)
		{
			// nothing more to do
		}
				
		Descriptor::Descriptor(const UniqueId &type, std::uint16_t handle) noexcept :
			Attribute(type, handle)
		{
			// nothing more to do
		}
	
		Descriptor::Descriptor(const UniqueId &type, std::uint16_t handle, AttributeClient *client) noexcept :
			Attribute(type, handle, client)
		{
			// nothing more to do
		}
		
		Descriptor::Descriptor(const Attribute &in) :
			Attribute(in)
		{
			// nothing more to do
		}
		
		Descriptor::Descriptor(Attribute &&in) noexcept :
			Attribute(std::move(in))
		{
			// nothing more to do
		}
		
		Descriptor::Descriptor(const Descriptor &in) = default;
			
		Descriptor::Descriptor(Descriptor &&in) noexcept :
			Attribute(std::move(in))
		{
			// nothing more to do
		}
		
		Descriptor &Descriptor::operator=(const Attribute &in)
		{
			Attribute::operator=(in);
			return *this;
		}
		
		Descriptor &Descriptor::operator=(Attribute &&in) noexcept
		{
			Attribute::operator=(std::move(in));
			return *this;
		}
				
		Descriptor &Descriptor::operator=(const Descriptor &in) = default;
				
		Descriptor &Descriptor::operator=(Descriptor &&in) noexcept
		{
			Attribute::operator=(std::move(in));
			return *this;
		}

		Descriptor::~Descriptor() noexcept = default;
						
		void Descriptor::validate()
		{
			if (mStartHandle)
			{
				if (mEndHandle)
				{
					mStartHandle = std::min(mStartHandle, mEndHandle);
				}
				
				mEndHandle = mStartHandle;
			}
			else
			{
				mStartHandle = mEndHandle;
			}
			
			Attribute::validate();
		}
		
		std::size_t Descriptor::read(const AttributeCallback &listener) const
		{
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "Descriptor does not have associated Bluetooth Low Energy Client");
			}
			
			Progress<std::size_t> read = mClient->readValue(mStartHandle, listener, 0);
			return read.count;
		}
		
		
		std::size_t Descriptor::read(void *data, std::size_t length) const
		{
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "Descriptor does not have associated Bluetooth Low Energy Client");
			}
			
			Progress<std::size_t> read = mClient->getValue(mStartHandle, data, length, 0);
			return read.count;
		}
		
		std::size_t Descriptor::write(const void *data, std::size_t length)
		{
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "Descriptor does not have associated Bluetooth Low Energy Client");
			}
			
			Progress<std::size_t> written = mClient->writeValue(mStartHandle, data, length, 0);
			return written.count;
		}
		
		std::size_t Descriptor::send(const void *data, std::size_t length)
		{
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "Descriptor does not have associated Bluetooth Low Energy Client");
			}
			
			Progress<std::size_t> written = mClient->sendValue(mStartHandle, data, length);
			return written.count;
		}
		
		template <>
		std::string Descriptor::read<std::string>() const
		{
			char tmp[512];
			std::size_t actual = this->read(tmp, 512);
			return std::string(tmp, tmp + actual);
		}
		
		template <>
		cio::Text Descriptor::read<cio::Text>() const
		{
			char tmp[512];
			std::size_t actual = this->read(tmp, 512);
			
			cio::Text text(tmp, actual, nullptr);
			text.internalize();
			return text;
		}
		
		template <>
		void Descriptor::write<std::string>(const std::string &value)
		{
			std::size_t actual = this->write(value.c_str(), value.size());
			if (actual != value.size())
			{
				throw cio::Exception(Reason::Overflow, "Could not write text attribute data");
			}
		}
					
		template <>
		CIO_BLUETOOTH_API void Descriptor::write<Text>(const Text &value)
		{
			std::size_t actual = this->write(value.data(), value.strlen());
			if (actual != value.size())
			{
				throw cio::Exception(Reason::Overflow, "Could not write text attribute data");
			}
		}
		
		template <>
		void Descriptor::send<std::string>(const std::string &value)
		{
			std::size_t actual = this->send(value.c_str(), value.size());
			if (actual != value.size())
			{
				throw cio::Exception(Reason::Overflow, "Could not write text attribute data");
			}
		}
					
		template <>
		CIO_BLUETOOTH_API void Descriptor::send<Text>(const Text &value)
		{
			std::size_t actual = this->send(value.data(), value.strlen());
			if (actual != value.size())
			{
				throw cio::Exception(Reason::Overflow, "Could not write text attribute data");
			}
		}	
	}
}

