/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Reason.h"

#include <cerrno>

#include "Enumerant.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>
#endif

namespace cio
{
	const Enumerant<Reason> Enumeration<Reason>::values[] =
	{
		{ Reason::None, "None", "No reason is specified" },
		{ Reason::Unknown, "Unknown", "The reason is unknown" },
		{ Reason::User, "User", "The application user or their agent made a request" }, 
		
		{ Reason::Interrupted, "Interrupted", "The operation was interrupted" },
		{ Reason::Reconnected, "Reconnected", "The operation was interrupted by a reconnection event" },
		{ Reason::Timeout, "Timeout", "Waiting for the operation timed out" },
		{ Reason::Canceled, "Canceled", "The operation was canceled" },

		{ Reason::Busy, "Busy", "The resource is currently busy" },
		{ Reason::Hardware, "Hardware", "The resource reported a hardware failure" },
		{ Reason::Unopened, "Unopened", "The operation failed because the resource was never set up" },
		{ Reason::Underflow, "Underflow", "Not enough data was available" },
		{ Reason::Overflow, "Overflow", "Too much data was available" },
		{ Reason::Exhausted, "Exhausted", "The operation ran out of system resources" },
		{ Reason::Unanswered, "Unanswered", "No response was received for the request" },
		{ Reason::Infinite, "Infinite", "The operation resulted in an infinite loop of redirection" },
							
		{ Reason::Invalid, "Invalid", "The operation was provided invalid parameters" },
		{ Reason::Unimplemented, "Unimplemented", "The operation is possible but the code to do so is not yet implemented" },
		{ Reason::Misaligned, "Misaligned", "The operation was provided a misaligned or wrong data length" },
		{ Reason::Unparsable, "Unparsable", "The underlying protocol used for the operation received malformed data it could not understand" },
		{ Reason::Unexpected, "Unexpected", "The underlying protocol used for the operation received an unexpected response" },
		{ Reason::Incompatible, "Incompatible", "The underlying protocol used for the operation uses an incompatible version" },

		{ Reason::Exists, "Exists", "The resource already exists" },
		{ Reason::Missing, "Missing", "The resource does not exist" },
		{ Reason::Unplugged, "Unplugged", "The operation failed because a local device was physically unplugged" },
		{ Reason::Connected, "Connected", "The operation failed because the resource is already connected" },
		{ Reason::Disconnected, "Disconnected", "The operation failed because the resource disconnected" },
		{ Reason::Unreachable, "Unreachable", "The operation failed because the desired resource is on an unreachable network" },
		
		{ Reason::Unsupported, "Unsupported", "The operation is unsupported" },
		{ Reason::Unusable, "Unusable", "The resource exists but is not usable for the type of operation requested" },
		{ Reason::Unreadable, "Unreadable", "The resource is not readable" },
		{ Reason::Unwritable, "Unwritable", "The resource is not writable" },
		{ Reason::Unremovable, "Unremovable", "The resource is not capable of being removed" },
		{ Reason::Uncreatable, "Uncreatable", "The resource is not capable of being created" },
		{ Reason::Unseekable, "Unseekable", "The resource is not capable of seeking or random access" },
		{ Reason::Unexecutable, "Unexecutable", "The resource is not a program that can be executed" },

		{ Reason::Refused, "Refused", "The operation was refused" },
		{ Reason::Blocked, "Blocked", "The operation was blocked by the recipient's policy" },
		{ Reason::Uncredentialed, "Uncredentialed", "The operation requires authentication to perform" },
		{ Reason::Unauthorized, "Unauthorized", "The operation is not authorized for the authenticated user" },
		{ Reason::Insecure, "Insecure", "The operation was refused due to not using secure enough encryption" },
		{ Reason::Quota, "Quota", "The operation was refused due to the authenticated user reaching a defined quota" }
	};
	
	const Enumeration<Reason> Enumeration<Reason>::instance;

	Enumeration<Reason>::Enumeration() :
		Enumerants<Reason>("cio::Reason", values, Reason::Missing, false)
	{
		// nothing more to do
	}

	Enumeration<Reason>::~Enumeration() noexcept = default;

	std::ostream &operator<<(std::ostream &stream, Reason reason)
	{
		return stream << print(reason);
	}

	Reason error() noexcept
	{
		return cio::error(errno);
	}

	Reason error(int error) noexcept
	{
		Reason reason;
		
		switch (error)
		{
			case 0:
				reason = Reason::None;
				break;
				
			case EACCES:
			case EPERM:
				reason = Reason::Unauthorized;
				break;
			
			case ECONNREFUSED:
				reason = Reason::Blocked;
				break;
				
			case EAGAIN:
				reason = Reason::Interrupted;
				break;
			
			case ETIMEDOUT:
#if defined EWOULDBLOCK && EWOULDBLOCK != EAGAIN
			case EWOULDBLOCK:
#endif
				reason = Reason::Timeout;
				break;
				
			case EBADF:
			case ENOTSOCK:
				reason = Reason::Unopened;
				break;
				
			case EADDRNOTAVAIL:
			case EBUSY:
				reason = Reason::Busy;
				break;
			
			case ECONNRESET:
				reason = Reason::Reconnected;
				break;

// not available on all platforms
#if defined EDQUOT
			case EDQUOT:
				reason = Reason::Quota;
				break;
#endif

			case EFBIG:
			case EMSGSIZE:
			case ENAMETOOLONG:
			case ENOSPC:
			case ERANGE:
				reason = Reason::Overflow;
				break;
			
			case EADDRINUSE:
			case EISCONN:
				reason = Reason::Connected;
				break;
					
			case EEXIST:
			case ENOTEMPTY:
				reason = Reason::Exists;
				break;
					
			case EINTR:
				reason = Reason::Interrupted;
				break;
				
			case EINVAL:
				reason = Reason::Invalid;
				break;

			case ELOOP:
				reason = Reason::Infinite;
				break;

			case EMFILE:
			case ENFILE:
			case ENOBUFS:
			case ENOMEM:
				reason = Reason::Exhausted;
				break;
				
			case ENETUNREACH:
				reason = Reason::Unreachable;
				break;
				
			case ENOENT:
				reason = Reason::Missing;
				break;
#if defined EISDIR
			// This code is Linux specific
			case EISDIR:
#endif
			case ENOTDIR:
			case ESPIPE:
				reason = Reason::Unusable;
				break;
				
			case EROFS:
				reason = Reason::Unwritable;
				break;
			
			case EPIPE:
				reason = Reason::Disconnected;
				break;
			
			case EAFNOSUPPORT:
			case EPROTONOSUPPORT:
			case EXDEV:
				reason = Reason::Unsupported;
				break;		
						
			default:
				reason = Reason::Unknown;
				break;
		}
		
		return reason;
	}
	
	Reason errorWin32() noexcept
	{
#if defined CIO_HAVE_WINDOWS_H
		return cio::errorWin32(::GetLastError());
#else
		return Reason::Unsupported;
#endif
	}
	
	Reason errorWin32(int error) noexcept
	{
		Reason reason;
		
		switch (error)
		{
			case 0:
				reason = Reason::None;
				break;
				
#if defined CIO_HAVE_WINDOWS_H
			case ERROR_ACCESS_DENIED:
				reason = Reason::Unauthorized;
				break;

			case ERROR_ALREADY_EXISTS:
			case ERROR_FILE_EXISTS:
				reason = Reason::Exists;
				break;
				
			case ERROR_BROKEN_PIPE:
			case ERROR_NO_MORE_FILES:
				reason = Reason::Underflow;
				break;

			case ERROR_NOT_FOUND:
			case ERROR_FILE_NOT_FOUND:
			case ERROR_PATH_NOT_FOUND:
				reason = Reason::Missing;
				break;

			case ERROR_DIRECTORY:
				reason = Reason::Unusable;
				break;
				
			case ERROR_INVALID_NAME:
				reason = Reason::Unparsable;
				break;

			case ERROR_NOT_SAME_DEVICE:
				reason = Reason::Unsupported;
				break;
#endif
				
			default:
				reason = Reason::Unknown;
				break;
		}
		
		return reason;
	}
}
