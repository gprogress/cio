/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CHARACTERITERATOR_H
#define CIO_CHARACTERITERATOR_H

#include "Types.h"

#include "Character.h"
#include "Parse.h"

#include <iterator>

namespace cio
{
	/**
	 * The Character Iterator provides a sequential forward iterator view of Unicode text backed
	 * by a standard text representation identified by the template parameter. The text
	 * may be const or non-const but will not be modified by any iterator methods.
	 * 
	 * Any character type may be used as long as it supports the methods
	 * std::size_t cio::terminator(const C *) and Character(const C *, std::size_t).
	 * 
	 * <ul>
	 * <li>char = UTF-8 text</li>
	 * <li>char16_t = UTF-16 text</li>
	 * <li>char32_t = UCS-4 native Unicode text</li>
	 * </ul>
	 * 
	 * @tparam C The character encoding type
	 */
	template <typename C>
	class CharacterIterator
	{
		public:
			/** The iterator value type is always Character */
			using value_type = Character;

			/** The iterator difference type */
			using difference_type = std::ptrdiff_t;

			/** The iteratory category type for algorithms */
			using iterator_category = std::forward_iterator_tag;

			/** The character iterator doesn't support pointers */
			using pointer = void;

			/** The character iterator doesn't support references */
			using reference = void;

			/**
			 * Construct the empty text iterator.
			 */
			inline CharacterIterator() noexcept
			{
				// nothing to do
			}

			/**
			 * Construct a text iterator for the given null-terminated text.
			 * The text size is used to determine the limit and the first character is decoded.
			 * 
			 * @param text The text to iterate on
			 */
			inline explicit CharacterIterator(C *text) noexcept :
				mCurrent(text),
				mLimit(text ? cio::terminator(text) : 0)
			{
				if (mCurrent)
				{
					mDecoded = Character::decode(mCurrent, mLimit);
				}
			}

			/**
			 * Construct a text iterator for the given text.
			 * The specified length sets the limit and the first character is decoded.
			 *
			 * @param text The text to iterate on
			 * @param length The maximum number of array elements to iterate through
			 */
			inline CharacterIterator(C *text, std::size_t length) noexcept :
				mCurrent(text),
				mLimit(length)
			{
				if (mCurrent)
				{
					mDecoded = Character::decode(mCurrent, mLimit);
				}
			}

			/**
			 * Gets the current character.
			 * 
			 * @return The current character
			 */
			inline Character operator*() const noexcept
			{
				return mDecoded.value;
			}

			/**
			 * Calls a method through the current character.
			 *
			 * @return The current character
			 */
			inline const Character *operator->() const noexcept
			{
				return &mDecoded.value;
			}

			/**
			 * Preincrement - advances to the next character and returns the iterator.
			 * 
			 * @return This iterator
			 */
			inline CharacterIterator &operator++() noexcept
			{
				mCurrent += mDecoded.parsed;
				mLimit -= mDecoded.parsed;
				mDecoded = Character::decode(mCurrent, mLimit);
				return *this;
			}

			/**
			 * Postincrement - advances to the next character and returns the iterator prior to doing that.
			 *
			 * @param tag The tag to select this overload
			 * @return This iterator
			 */
			inline CharacterIterator operator++(int tag) noexcept
			{
				CharacterIterator old(*this);
				++(*this);
				return old;
			}

			/**
			 * Get the end iterator implied by the text limit.
			 * 
			 * @return The implied end iterator
			 */
			inline CharacterIterator end() const noexcept
			{
				return CharacterIterator(mCurrent + mLimit, 0);
			}

			/**
			 * Gets the underlying data array.
			 * 
			 * @return The data array
			 */
			inline C *data() noexcept
			{
				return mCurrent;
			}

			/**
			 * Gets the underlying data array.
			 *
			 * @return The data array
			 */
			inline const C *data() const noexcept
			{
				return mCurrent;
			}

			/**
			 * Gets whether the current iterator is valid.
			 * This is true if the iterator has been initialized, not yet reached its limit,
			 * and has not prematurely ended due to invalid character sequence.
			 * 
			 * @return Whether the iterator is valid
			 */
			inline explicit operator bool() const noexcept
			{
				return static_cast<bool>(mDecoded.value);
			}

		private:
			/** The current UTF-8 byte position */
			C *mCurrent = nullptr;

			/** The maximum length of the UTF-8 byte sequence */
			std::size_t mLimit = 0;

			/** The decoded character at the current position */
			Parse<Character, std::remove_const_t<C>> mDecoded;
	};

	/**
	 * Checks to see if two text iterators are equal.
	 * This is true if they point to the same byte or have both ended.
	 * 
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the iterators are equal
	 */
	template <typename C>
	inline bool operator==(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return left.data() == right.data() || (!left && !right);
	}

	/**
	 * Checks to see if two text iterators are not equal.
	 * This is true if they point to the different bytes and at least one has not ended.
	 *
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the iterators are not equal
	 */
	template <typename C>
	inline bool operator!=(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return left.data() != right.data() && (left || right);
	}

	/**
	 * Checks to see if one text iterator is before another.
	 * This is true if the second iterator has not ended and the first either has ended or points
	 * to an earlier data byte than the second.
	 *
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the first iterator is before the second
	 */
	template <typename C>
	inline bool operator<(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return right && (!left || left.data() < right.data());
	}

	/**
	 * Checks to see if one text iterator is before or equal to another.
	 * This is true if the first iterator has ended or both iterators are valid and
	 * the first points to data byte before or equal to the second.
	 *
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the first iterator is before or equal to the second
	 */
	template <typename C>
	inline bool operator<=(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return !left || (right && left.data() <= right.data());
	}

	/**
	 * Checks to see if one text iterator is after another.
	 * This is true if the first iterator has not ended and the second either has ended or points
	 * to an earlier data byte than the first.
	 *
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the first iterator is before the second
	 */
	template <typename C>
	inline bool operator>(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return left && (!right || left.data() > right.data());
	}

	/**
	 * Checks to see if one text iterator is after or equal to another.
	 * This is true if the second iterator has ended or both iterators are valid and
	 * the second points to data byte before or equal to the first.
	 *
	 * @tparam C The character encoding type
	 * @param left The first iterator
	 * @param right The second iterator
	 * @return Whether the first iterator is after or equal to the second
	 */
	template <typename C>
	inline bool operator>=(const CharacterIterator<C> &left, const CharacterIterator<C> &right) noexcept
	{
		return !right || (left && left.data() >= right.data());
	}
}

#endif
