/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "IPv6Test.h"

#include <cio/ParseStatus.h>

#include <cio/test/Assert.h>

namespace cio
{
	namespace net
	{
		IPv6Test::IPv6Test() :
			cio::test::Suite("cio/net/IPv6")
		{
			CIO_TEST_METHOD(IPv6Test, parse);
			CIO_TEST_METHOD(IPv6Test, print);
			CIO_TEST_METHOD(IPv6Test, set);
			CIO_TEST_METHOD(IPv6Test, clear);
		}

		IPv6Test::~IPv6Test() noexcept = default;

		void IPv6Test::parseTest()
		{
			IPv6 ip;

			// Test empty / any address
			IPv6 expected;
			{
				TextParseStatus expectedValid(2u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("::"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test version with zeros explicitly spelled out
			{
				TextParseStatus expectedValid(15u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("0:0:0:0:0:0:0:0"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test version with [] around it
			{
				TextParseStatus expectedValid(4u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("[::]"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test version with mismatched [
			{
				TextParseStatus expectedValid(3u, 0, Validation::TooShort, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("[::"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test version with mismatched ]
			{
				TextParseStatus expectedValid(2u, ']', Validation::Exact, Reason::Unexpected);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("::]"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test localhost
			expected = IPv6::localhost();
			{
				TextParseStatus expectedValid(3u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("::1"));
				CIO_ASSERT_EQUAL(expected, ip);
			}
			// Test unshortened form of localhost
			{
				TextParseStatus expectedValid(15u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("0:0:0:0:0:0:0:1"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// test IP with low section entirely 0
			expected.set(0x123456789ABCDEF0ull, 0ull);
			{
				TextParseStatus expectedValid(21u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("1234:5678:9abc:def0::"));
				CIO_ASSERT_EQUAL(expected, ip);
			}
			// Test an IP with all sections set
			expected.set(0xDEADBEEFABCD1024ull, 0x5544EFFF01234567ull);
			{
				TextParseStatus expectedValid(38u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("dead:beef:abcd:1024:5544:efff:123:4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test uppercase
			{
				TextParseStatus expectedValid(38u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("DEAD:BEEF:ABCD:1024:5544:EFFF:123:4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			//Test a silly variant that mixes case and has an unnecessary leading zero
			{
				TextParseStatus expectedValid(39u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("dEaD:BeEf:aBcD:1024:5544:EffF:0123:4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test an IP with a zero sequence in the high section
			expected.set(0xDEADBEEF00000000ull, 0x5544EFFF01234567ull);
			{
				TextParseStatus expectedValid(29u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("DEAD:BEEF::5544:EFFF:123:4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test an IP with a zero sequence in the low section
			expected.set(0xDEADBEEF12345678ull, 0x5544000000004567ull);
			{
				TextParseStatus expectedValid(30u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("DEAD:BEEF:1234:5678:5544::4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test an IP with a zero sequence that crosses high and low sections
			expected.set(0xDEADBEEF12340000ull, 0x0000000000004567ull);
			{
				TextParseStatus expectedValid(20u, 0, Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("DEAD:BEEF:1234::4567"));
				CIO_ASSERT_EQUAL(expected, ip);
			}

			// Test an IP with [] and a port
			{
				TextParseStatus expectedValid(22u, ':', Validation::Exact, Reason::None);
				CIO_ASSERT_EQUAL(expectedValid, ip.parse("[DEAD:BEEF:1234::4567]:3000"));
				CIO_ASSERT_EQUAL(expected, ip);
			}
		}

		void IPv6Test::printTest()
		{
			IPv6 ip;
			std::string expected("::");
			char actual[48];
			// All zeros
			CIO_ASSERT_EQUAL(2u, ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// High section is all zeroes, low section is nonzero
			ip = IPv6::localhost();
			expected = "::1";
			CIO_ASSERT_EQUAL(3u, ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// Leading zeroes
			ip.set(0x0000000000F344ull, 0xDEADBEEF02345678ull);
			expected = "::f344:dead:beef:234:5678";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// Zeroes in the high section
			ip.set(0xDEAD0000FEEDBEEFull, 0x12340078ABCDEFFFull);
			expected = "dead::feed:beef:1234:78:abcd:efff";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// Trailing zeros
			ip.set(0xDEBE0101ABCD1234ull, 0ull);
			expected = "debe:101:abcd:1234::";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// multiple sets of zeros, first is longer
			ip.set(0xDEAD00000000BEEFull, 0x12340000ABCDEFFFull);
			expected = "dead::beef:1234:0:abcd:efff";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// multiple sets of zeros, second is longer but standard still says pick first
			ip.set(0xDEAD0000FEEDBEEFull, 0x00000000ABCDEFFFull);
			expected = "dead::feed:beef:0:0:abcd:efff";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
			// multiple sets of zeros of same length, standard says pick first
			ip.set(0xDEAD0000FEEDBEEFull, 0x00001234ABCDEFFFull);
			expected = "dead::feed:beef:0:1234:abcd:efff";
			CIO_ASSERT_EQUAL(expected.size(), ip.print(actual, 48));
			CIO_ASSERT_EQUAL(expected, actual);
		}

		void IPv6Test::setTest()
		{
			IPv6 ip;
			CIO_ASSERT_EQUAL(0u, ip.high());
			CIO_ASSERT_EQUAL(0u, ip.low());
			ip.set(124371u, 1239071u);
			CIO_ASSERT_EQUAL(124371u, ip.high());
			CIO_ASSERT_EQUAL(1239071u, ip.low());
		}

		void IPv6Test::clearTest()
		{
			IPv6 ip;
			CIO_ASSERT_EQUAL(0u, ip.high());
			CIO_ASSERT_EQUAL(0u, ip.low());
			ip.clear();
			CIO_ASSERT_EQUAL(0u, ip.high());
			CIO_ASSERT_EQUAL(0u, ip.low());
			ip.set(0xDEADBEEFu, 0x12345678u);
			CIO_ASSERT_EQUAL(0xDEADBEEFu, ip.high());
			CIO_ASSERT_EQUAL(0x12345678u, ip.low());
			ip.clear();
			CIO_ASSERT_EQUAL(0u, ip.high());
			CIO_ASSERT_EQUAL(0u, ip.low());
		}
	}
}

