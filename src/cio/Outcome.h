/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_OUTCOME_H
#define CIO_OUTCOME_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The Outcome enumeration describes the possible outcomes involved
	 * in executing tasks relevant to operating on resources.
	 */
	enum class Outcome : std::uint8_t
	{
		// Default state

		/** No outcome occurred */
		None,
		
		/** The action completed without assessment of success or failure */
		Completed,
		
		/** The action completed successfully */
		Succeeded,

		/** The action completed unsuccessfully due to some failure condition */
		Failed,
		
		/** The action was abandoned due to timeout or cancellation */ 
		Abandoned
	};
	
	/**
	 * Checks whether a outcome is considered a failure.
	 * This is for all states equal to or greater than Failed.
	 *
	 * @param outcome The outcome of interest
	 * @return whether the outcome indicates a failure
	 */
	inline bool failed(Outcome outcome) noexcept; 
	
	/**
	 * Checks whether a outcome has succeeded.
	 * For the purposes of this check, Completed is considered a success.
	 *
	 * @param outcome The outcome
	 * @return whether the operation has suceeded
	 */
	inline bool succeeded(Outcome outcome) noexcept;

	/**
	 * Gets the text value of the Outcome value.
	 *
	 * @param outcome The outcome value
	 * @return the text value
	 */
	CIO_API const char *print(Outcome outcome) noexcept;

	/**
	 * Gets a detailed description of the outcome value.
	 *
	 * @param outcome The outcome value
	 * @return the description text
	 */
	CIO_API const char *getDescription(Outcome outcome) noexcept;

	/**
	 * Prints the text value of the Outcome value to a C++ standard output stream.
	 *
	 * @param s The stream
	 * @param value The outcome value
	 * @return the stream after printing
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Outcome outcome);
}

/* Inline implementation */

namespace cio
{
	inline bool failed(Outcome outcome) noexcept
	{
		return outcome >= Outcome::Failed;	
	}

	inline bool succeeded(Outcome outcome) noexcept
	{
		return outcome >= Outcome::Completed && outcome < Outcome::Failed;
	}
}

#endif
