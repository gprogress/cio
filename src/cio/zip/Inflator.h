/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_INFLATOR_H
#define CIO_ZIP_INFLATOR_H

#include "Types.h"

#include <cio/Input.h>

#include <cio/Buffer.h>
#include <cio/Metadata.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace zip
	{
		/**
		* The Inflator is an Input overlay that decompresses data from another Input using the Deflate algorithm
		* common to Zip and Gzip files.
		* 
		* For the various open methods, a window bits parameter may be specified to control memory use and to
		* specify which exact format the input is in - Gzip, Deflate, or raw input as provided by Zip files.
		* Aside from choosing the format, the low nibble of the absolute value of the window bits is also
		* the base 2 logarithm of the window size, and must be equal to or greater than the value used
		* to compress the file or else decompression will fail. On modern systems there is typically no
		* reason not to use the largest possible size of 15.
		* 
		* Window bits details:
		* <ul>
		* <li>Value between 8 and 15 is used to specify normal deflate init, 15 is recommended</li>
		* <li>Value between 24 and 31 is used to specify Gzip deflate init, 31 is recommended</li>
		* <li>Value between -8 and -15 is used to specify raw deflate as used by Zip, -15 is recommended</li>
		* <li>Value of 0 is used to autodetect if possible</li>
		* <li>Value outside of these ranges is clamped to nearest valid value</li>
		* </ul>
		* 
		* If the window bits is specified as 0, this will attempt to guess the right setting based
		* on input metadata (-15 for a .zip file, 31 for a .gz file, 15 otherwise).
		*/
		class CIO_ZIP_ABI Inflator : public Input
		{
			public:
				/**
				 * Gets the declared metaclass for the Inflator class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Validates the window bits parameter to be in range of valid values.
				 * Values from 1 to 7 are clamped to 8.
				 * Values from 16 to 23 are clamped to 24.
				 * Values above 31 are clamped to 31.
				 * Values from -1 to -7 are clamped to -8.
				 * Values below -15 are clamped to -15
				 * Value of 0 is returned as-is.
				 * 
				 * @param value The input window bits value
				 * @return the validated window bits value
				 */
				static int validateWindowBits(int value) noexcept;

				/**
				 * Attempts to detect the window bits parameter from inspecting the given input.
				 * 
				 * The following items are checked:
				 * <ul>
				 * <li>If the input is a RelativeInput backed by a file with .zip extension, -15 is returned</li>
				 * <li>If the input is backed by a file with a .gz extension, 31 is returned</li>
				 * <li>Default value reeturned is 15 otherwise<?li>
				 * </ul>
				 * 
				 * Only input metadata should be considered, the input itself will not be read or modified.
				 * 
				 * @return the autodetect window bits
				 * @throw Exception If getting metadata from the input failed
				 */
				static int detectWindowBits(Input &input);

				/**
				 * Construct an unopened Inflator.
				 */
				Inflator() noexcept;
				
				/**
				 * Associates this Inflator with the given Input that provides compressed data.
				 *
				 * @param input The input to use to obtain compressed data.
				 * @param windowBits The window bits to provide to the inflator
				 */
				explicit Inflator(std::shared_ptr<Input> input, int windowBits = 0);
				
				/**
				 * Associates this Inflator with the given Input that provides compressed data and the given metadata.
				 *
				 * @param input The input to use to obtain compressed data.
				 * @param metadata The metadata to use to provide URI and uncompressed size
				 * @param windowBits The window bits to provide to the inflator
				 */
				Inflator(std::shared_ptr<Input> input, Metadata metadata, int windowBits = 0);
				
				/**
				 * Construct an inflator by transferring the contents of an existing inflator.
				 *
				 * @param in The inflator to move
				 */
				Inflator(Inflator &&in) noexcept;

				/**
				 * Moves an inflator.
				 *
				 * @param in The inflator to move
				 * @return this inflator
				 */
				Inflator &operator=(Inflator &&in) noexcept;

				/**
				* Destructor.
				*/
				virtual ~Inflator() noexcept override;

				/**
				 * Gets the runtime metaclass for this object.
				 *
				 * @return the metaclass for this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears all Zlib and buffer state and disassociates the underlying Input.
				 */
				virtual void clear() noexcept override;
							
				// Overall resource metadata

				/**
				 * Gets the metadata for this inflator. This returns the cached metadata.
				 *
				 * @return the metadata
				 * @throw std::bad_alloc If memory allocations failed for any operation
				 */
				virtual Metadata metadata() const override;

				/**
				 * Gets the path URI set on this inflactor. This returns the path from the cached metadata.
				 * 
				 * @return the resource location
				 * @throw std::bad_alloc If memory allocations failed for any operation
				 */
				virtual Path location() const override;

				/**
				 * Gets the overall resource type. This returns the type from the cached metadata, which should be Resource::File.
				 * 
				 * @return the resource type
				 */
				virtual Resource type() const noexcept override;

				/**
				 * Gets the current modes of the resource. This returns the modes from the cached metadata, which should be ModeSet::readonly().
				 * 
				 * @return the modes
				 */
				virtual ModeSet modes() const noexcept override;

				/**
				 * Gets the total logical size of the resource if known.
				 * This returns the logical size from the cached metadata, which is typically only known if the inflator
				 * was constructed from an Archive or similar resource that has the appropriate metadata.
				 *
				 * @return the total logical resource size
				 */
				virtual Length size() const noexcept override;
			
				/**
				 * Checks whether the Inflator is open.
				 * This is true if the inflator has underlying Input and that input is also open.
				 *
				 * @return whether the inflator is open
				 */
				virtual bool isOpen() const noexcept override;

				/**
				 * Opens the resource using the given open modes.
				 * This method will open the underlying resource with the factory, then set up Inflator decompression on it.
				 * This method will do nothing if the output modes do not include Read.
				 * 
				 * The following possibilities are available:
				 * <ul>
				 * <li>A single file with .gz extension - assumed Gzip format, uses factory to open file</li>
				 * <li>A jar: URI that specifies an entry - opens a Zip archive and then reads that entry</li>
				 * <li>Any other input is assumed to represent a single file compressed with raw deflate, uses factory to open file</li>
				 * </ul>
				 * 
				 * @param resource The resource to open
				 * @param modes The desired resource modes to use to open the resource
				 * @param factory The factory to use to construct underlying transport and codec protocols
				 * @return the actual resource modes that the resource could be opened with
				 * @throw cio::Exception If the resource could not be opened
				 */
				virtual cio::ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;
				
				/**
				 * Gets the current sequential read position as a byte offset relative to the beginning of the resource.
				 * This returns the tracked read position.
				 *
				 * @return the current read position
				 */
				virtual Length getReadPosition() const noexcept override;
			
				/**
				 * Loads data from the underlying Input, transparently decompressing it using the Deflate algorithm.
				 * Compressed data is preloaded into an internal compressed data buffer and then decompressed to the provided buffer.
				 *
				 * @param data The data buffer to load into
				 * @param length The number of bytes to load into the data buffer
				 * @return the result describing the status of the load operation and number of bytes actually put into the load buffer
				 */
				virtual Progress<std::size_t> requestRead(void *data, std::size_t length) noexcept override;

				/**
				 * Requests that the internal compressed data buffer have the given size.
				 * Larger buffers are more efficient since they require fewer requests to the underlying Input.
				 *
				 * @param bytes The number of bytes in the load buffer
				 * @return the status describing whether this request could be done and the actual compressed data buffer size
				 */
				virtual Progress<std::size_t> requestReadBuffer(std::size_t bytes) noexcept override;

				/**
				 * Attempts to preload the given number of compressed data bytes.
				 *
				 * @param bytes The number of bytes to preload
				 * @return the status and numer of actual preloaded bytes
				 */
				virtual Progress<std::size_t> preload(std::size_t bytes) noexcept;
				
				/**
				 * Gets the underlying input stream that compressed data is read from.
				 *
				 * @return the underlying input
				 */
				std::shared_ptr<Input> getInput() noexcept;

				/**
				 * Gets the underlying input stream that compressed data is read from.
				 *
				 * @return the underlying input
				 */
				std::shared_ptr<const Input> getInput() const noexcept;

				/**
				 * Takes ownership of the referenced Input from the Inflator.
				 * This can be used to embed compressed data inside of a stream then reclaim ownership of the stream to write uncompressed data or switch codecs.
				 *
				 * @warning The input buffer used for this class may have unused data remaining that will be lost by this operation
				 *
				 * @return the underlying input
				 */
				std::shared_ptr<Input> takeInput();
				
				/**
				 * Sets the tracked metadata for this inflator.
				 *
				 * @param metadata the tracked metadata
				 */
				void setMetadata(Metadata metadata) noexcept;
				
				/**
				 * Sets the tracked metadata for this inflator.
				 *
				 * @param path The URI for the input
				 * @param uncompressed The logical uncompressed length if known
				 */
				void setMetadata(Path path, Length uncompressed) noexcept;
				
				/**
				 * Associates this Inflator with the given Input that provides compressed data.
				 * 
				 * @param input The input to use to obtain compressed data
				 * @param windowBits The window bits for the inflate initialization, or 0 to autodetect
				 */
				void openWithInput(std::shared_ptr<Input> input, int windowBits = 0);

				/**
				 * Associates this Inflator with the given Input that provides compressed data.
				 * The Input will not be owned by this Inflator.
				 *
				 * @param input The input to use to obtain compressed data
				 * @param windowBits The window bits for the inflate initialization, or 0 to autodetect
				 */
				void openWithUnownedInput(Input *input, int windowBits = 0);

				/**
				 * Refills the compressed data buffer from the underlying Input.
				 *
				 * @return the result describing the refill status and how many compressed bytes were preloaded
				 */
				Progress<std::size_t> refill();

				/**
				 * Gets the window bits used to open this inflator.
				 *
				 * @return the window bits actually used to open this inflator
				 */
				int getWindowBits() const noexcept;

				/**
				 * Returns whether the inflator was opened in Gzip mode.
				 * This is true if the window bits is between 24 and 31.
				 * 
				 * @return whether the inflator is processing a Gzip file
				 */

				bool isGzip() const noexcept;

				/**
				 * Returns whether the inflator was opened in Deflate framed mode.
				 * This is true if the window bits is between 8 and 15.
				 *
				 * @return whether the inflator is processing a ramed Deflate stream
				 */
				bool isDeflate() const noexcept;

				/**
				 * Returns whether the inflator was opened in raw entry mode
				 * as typically used in .zip archives.
				 * 
				 * This is true if the window bits is between -8 and -15.
				 *
				 * @return whether the inflator is processing a raw entry such as in a Zip file
				 */
				bool isRawEntry() const noexcept;

			private:
				/** The metaclass for this class */
				static Class<Inflator> sMetaclass;
				
				/**
				 * Configures the Zlib and buffer state for a new Input.
				 * 
				 * @param windowBits The window bits setting
				 */
				void configureNewInput(int windowBits = 0);

				/**
				 * Clears all Zlib state and the compressed data buffer and dissassociates the underlying Input.
				 */
				void dispose();

				/** The underlying Input source for compressed data. */
				std::shared_ptr<Input> mSource;

				/** The Zlib decoder handle. */
				void *mDecoder;

				/** The buffer storing compressed data prior to decompression. */
				Buffer mLoadBuffer;

				/** Metadata containing uncompressed size (if known) and intended URI */
				Metadata mMetadata;
								
				/** The number of uncompressed bytes read since starting decompression */
				Length mPosition;

				/** The window bits to use for inflate */
				int mWindowBits;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
