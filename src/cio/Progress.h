/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PROGRESS_H
#define CIO_PROGRESS_H

#include "Types.h"

#include "State.h"
#include "Metatypes.h"

#include <algorithm>

namespace cio
{
	/**
	 * The Progress template provides a measure of actual object count processed vs. total object count processed.
	 * It extends the base State status description and operations to also describe this count.
	 *
	 * @tparam <T> The size type for the number of objects/bytes, typically std::size_t for memory operations and
	 * std::uint64_t for general Input or Output operations
	 */
	template <typename T>
	class Progress : public State
	{
		public:
			/** The number of objects processed */
			T count;
			
			/** The total number of objects  */
			T total;

			/**
			 * Construct an empty Progress object with a count of 0 Progress and a status of None.
			 */
			inline Progress() noexcept;
			
			/**
			 * Construct a Progress object with the given status.
			 *
			 * @param s The status
			 */	
			inline Progress(Status s) noexcept;

			/**
			 * Construct a Progress object with the given action and status.
			 *
			 * @param a The action
			 * @param s The status
			 */
			inline Progress(Action a, Status s) noexcept;

			/**
			 * Construct a Progress object from an State object.
			 *
			 * @param e The state
			 */			
			inline Progress(State e) noexcept;
			
			/**
			 * Construct a Progress object with the given value as both count and total.
			 *
			 * @param count The count
			 */
			inline Progress(T count) noexcept;
			
			/**
			 * Construct a Progress object with the given values for count and total.
			 *
			 * @param count The count 
			 * @param total The total
			 */
			inline Progress(T count, T total) noexcept;
			
			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param a The action
			 * @param count The count
			 * @param e The state
			 */
			inline Progress(Action a, T count) noexcept;

			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param a The action
			 * @param s The status
			 * @param count The count
			 * @param e The state
			 */
			inline Progress(Action a, Status s, T count) noexcept;

			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param a The action
			 * @param count The count 
			 * @param total The total
			 * @param e The state
			 */
			inline Progress(Action a, T count, T total) noexcept;

			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param a The action
			 * @param s The status
			 * @param count The count
			 * @param total The total
			 * @param e The state
			 */
			inline Progress(Action a, Status s, T count, T total) noexcept;

			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param e The state
			 * @param count The count
			 */
			inline Progress(State e, T count) noexcept;
			
			/**
			 * Construct a Progress object with the given values.
			 *
			 * @param e The state
			 * @param count The count 
			 * @param total The total
			 */
			inline Progress(State e, T count, T total) noexcept;
			
			/**
			 * Construct a copy of a Progress object.
			 *
			 * @param in The Progress object to copy
			 */
			inline Progress(const Progress<T> &in);

			/**
			 * Construct a copy of a Progress object.
			 *
			 * @tparam <U> The other Progress object's numeric type
			 * @param in The Progress object to copy
			 */
			template <typename U>
			inline Progress(const Progress<U> &in);

			/**
			 * Copy a Progress object.
			 *
			 * @param in The Progress object to copy
			 * @return this Progress object
			 */
			inline Progress<T> &operator=(const Progress<T> &in);

			/**
			 * Copy a Progress object.
			 *
			 * @tparam <U> The other Progress object's numeric type
			 * @param in The Progress object to copy
			 * @return this Progress object
			 */
			template <typename U>
			inline Progress<T> &operator=(const Progress<U> &in);
			
			/**
			 * Casts a Progress object to a different value type.
			 *
			 * @tparam <U> The desired value type
			 * @return the converted Progress object
			 */
			template <typename U>
			inline explicit operator Progress<U>() const noexcept;

			/**
			 * Clears the Progress object to 0 objects processsed and a status of None.
			 */
			inline void clear() noexcept;
			
			// Pull in State::start() so we don't shadow it
			using State::start;
			
			/**
			 * Starts a task with the given max progress amount.
			 * The current progress is set to 0.
			 *
			 * @param maxProgress The initial maximum progress
			 */
			inline void start(T maxProgress) noexcept;
			
			/**
			 * Updates to add the given progress amount ot the current value.
			 *
			 * @param progress The current progress to add
			 */
			inline void update(T progress) noexcept;
			
			/**
			 * Updates to add the given progress amounts to the current value and max value.
			 *
			 * @param progress The current progress to add
			 * @param maxProgress The maximum progress to add
			 */
			inline void update(T progress, T maxProgress) noexcept;

			/**
			 * Gets the ratio of count to total progress.
			 * This will normally be a value between 0.0 and 1.0.
			 * 
			 * @return the progress ratio
			 */
			inline double ratio() const noexcept;

			/**
			 * Gets the percentage of total progress as an integer.
			 * This will be a value between 0 and 100.
			 *
			 * @return the progress percent
			 */
			inline unsigned percent() const noexcept;
	};

	/**
	 * Combine two Progress objects  by adding the count of count objects and taking the max of the total objects.
	 *
	 * @tparam <T> The count type of the first Progress object
	 * @tparam <U> The count type of the second Progress object
	 *
	 * @param left The Progress object to update
	 * @param right The Progress to add into the count Progress object
	 * @return the updated Progress object with the sum of the objects count count and max of total count
	 */
	template <typename T, typename U>
	inline Progress<T> &operator+=(Progress<T> &left, const Progress<U> &right) noexcept;

	/**
	 * Combine two Progress objects by adding the count of count objects and taking the max of the total objects.
	 *
	 * @tparam <T> The count type of the first Progress object
	 * @tparam <U> The count type of the second Progress object
	 *
	 * @param left The first Progress object to include in the total
	 * @param right The second Progress object to include in the total
	 * @return the Progress object with the sum of the objects count count and max of total count
	 */
	template <typename T, typename U>
	inline Progress<Sum<T, U>> operator+(Progress<T> left, Progress<U> right) noexcept;
	
	/**
	 * Construct a Progress object from the current count, which is also used for the max total.
	 * The status is initialized to None.
	 *
	 * @tparam <T> The value type
	 * @param value the value
	 * @return the progress object
	 */
	template <typename T>
	inline Progress<T> progress(T value) noexcept;
	
	/**
	 * Construct a Progress object from the current and total value.
	 * The status is initialized to None.
	 *
	 * @tparam <T> The count type
	 * @tparam <U> The total type
	 * @param count The count
	 * @param total The total
	 * @return the progress object
	 */
	template <typename T, typename U>
	inline Progress<std::common_type_t<T, U>> progress(T count, U total) noexcept;
	
	/**
	 * Construct a Progress object from status and the current count, which is also used for the max total.
	 *
	 * @tparam <T> The value type
	 * @param e The state
	 * @param value the value
	 * @return the progress object
	 */
	template <typename T>
	inline Progress<T> progress(State e, T value) noexcept;
	
	/**
	 * Construct a Progress object from the status and current and total value.
	 *
	 * @tparam <T> The count type
	 * @tparam <U> The total type
	 * @param e The state
	 * @param count The count
	 * @param total The total
	 * @return the progress object
	 */	
	template <typename T, typename U>
	inline Progress<std::common_type_t<T, U>> progress(State e, T count, U total) noexcept;
}

/* Inline implementation */

namespace cio
{
	template <typename T>
	inline Progress<T>::Progress() noexcept :
		count(),
		total()
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(Status s) noexcept :
		State(s),
		count(),
		total()
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(Action a, Status s) noexcept :
		State(a, s),
		count(),
		total()
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(State e) noexcept :
		State(e),
		count(),
		total()
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Progress<T>::Progress(T count) noexcept :
		count(count),
		total(count)
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Progress<T>::Progress(T count, T total) noexcept :
		count(count),
		total(total)
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Progress<T>::Progress(Action a, T count) noexcept :
		State(a),
		count(count),
		total(count)
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(Action a, Status s, T count) noexcept :
		State(a, s),
		count(count),
		total(count)
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(Action a, T count, T total) noexcept :
		State(a),
		count(count),
		total(total)
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(Action a, Status s, T count, T total) noexcept :
		State(a, s),
		count(count),
		total(total)
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T>::Progress(State e, T count) noexcept :
		State(e),
		count(count),
		total(count)
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Progress<T>::Progress(State e, T count, T total) noexcept :
		State(e),
		count(count),
		total(total)
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Progress<T>::Progress(const Progress<T> &in) = default;

	template <typename T>
	template <typename U>
	inline Progress<T>::Progress(const Progress<U> &in) :
		State(in),
		count(in.count),
		total(in.total)
	{
		// nothing more to do
	}

	template <typename T>
	inline Progress<T> &Progress<T>::operator=(const Progress<T> &in) = default;

	template <typename T>
	template <typename U>
	inline Progress<T> &Progress<T>::operator=(const Progress<U> &in)
	{
		State::operator=(in);
		this->count = in.count;
		this->total = in.total;
		return *this;
	}

	template <typename T>
	template <typename U>
	inline Progress<T>::operator Progress<U>() const noexcept
	{
		return Progress<U>(static_cast<U>(this->count), static_cast<U>(this->total), static_cast<const State &>(*this));
	}
			
	template <typename T>
	inline void Progress<T>::clear() noexcept
	{
		State::clear();
		this->count = T();
		this->total = T();
	}
	
	template <typename T>
	inline void Progress<T>::start(T maxProgress) noexcept
	{
		State::start();
		this->count = T();
		this->total = maxProgress;
	}

	template <typename T>			
	inline void Progress<T>::update(T progress) noexcept
	{
		this->count += progress;
	}

	template <typename T>	
	inline void Progress<T>::update(T progress, T maxProgress) noexcept
	{
		this->count += progress;
		this->total += maxProgress;
	}

	template <typename T>
	inline double Progress<T>::ratio() const noexcept
	{
		return static_cast<double>(this->count) / static_cast<double>(this->total);
	}

	template <typename T>
	inline unsigned Progress<T>::percent() const noexcept
	{
		auto t = (this->count * 100u) / ((this->total != T()) ? this->total : T(1));
		return (t <= T(0)) ? 0u : ((t >= T(100)) ? 100u : static_cast<unsigned>(t));
	}
			
	template <typename T, typename U>
	inline Progress<T> &operator+=(Progress<T> &left, const Progress<U> &right) noexcept
	{
		left.count += right.count;
		left.total = std::max<T>(left.total, right.total);
		left.set(right);
		return left;
	}

	template <typename T, typename U>
	inline Progress<Sum<T, U>> operator+(Progress<T> left, Progress<U> right) noexcept
	{
		return Progress<Sum<T, U>>(left.count + right.count, std::max<Sum<T, U>>(left.total, right.total), right);
	}
	
	template <typename T>
	inline Progress<T> progress(T value) noexcept
	{
		return Progress<T>(value);
	}
	
	template <typename T, typename U>
	inline Progress<std::common_type_t<T, U>> progress(T count, U total) noexcept
	{
		return Progress<std::common_type_t<T, U>>(count, total);
	}
	
	template <typename T>
	inline Progress<T> progress(State e, T value) noexcept
	{
		return Progress<T>(value, e);
	}
	
	template <typename T, typename U>
	inline Progress<std::common_type_t<T, U>> progress(State e, T count, U total) noexcept
	{
		return Progress<std::common_type_t<T, U>>(count, total, e);
	}	
}



#endif
