/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NEWLINE_H
#define CIO_NEWLINE_H

#include "Types.h"

#include <iosfwd>
#include <utility>

namespace cio
{
	/**
	 * The Newline enumeration specifies the style of newlines used in text files.
	 */
	enum class Newline : std::uint8_t
	{
		/** No newlines should be used */
		None,

		/** Unix newlines simply consist of the newline character */
		Unix,

		/** Apple newlines consist solely of the carriage return character */
		Apple,

		/** Windows newlines consist of the character sequence newline + carriage return */
		Windows,

		/** Native platform newline style */
		Native,

		/** Sentinel to indicate preserving the input newline style regardless of what it is */
		Preserve
	};

	/**
	 * Specialize CIO Print Properties for newlines to show the minimum and maximum values.
	 */
	template <>
	class PrintProperties<Newline, char>
	{
		public:
			/** Largest newline is 2 */
			using String = FixedText<2>;

			/** Minimum length is 0 for None */
			static const std::size_t minimum = 0;

			/** Maximumlength is 2 for Windows */
			static const std::size_t maximum = 2;
	};

	/**
	 * Gets the native newline style that this library was compiled with.
	 * This will return either Unix, Windows, or Apple.
	 *
	 * @return the native newline value
	 */
	CIO_API Newline getNativeNewline() noexcept;

	/**
	 * Gets the platform newline style that best matches the requested style.
	 * If the requested style is Native or Preserve, the actual plaform native style will be returned.
	 * Otherwise, the requested style is returned unchanged.
	 *
	 * @param style The requested newline
	 * @return the native newline value
	 */
	CIO_API Newline getNativeNewline(Newline style) noexcept;

	/**
	 * Gets the character sequence that a particular newline style represents.
	 * For the purposes of this method, Preserve is equivalent to Native and None is the empty string.
	 */
	CIO_API const char *print(Newline style) noexcept;

	/**
	 * Prints a newline in the given style to a UTF-8 text buffer.
	 * This will print 0 to 2 characters depending on the style and platform.
	 * Preserve is treated as equivalent to Native for this method, and None does nothing.
	 * 
	 * @param style The newline style
	 * @param buffer The buffer to print to
	 * @param capacity The buffer capacity
	 * @return the actual number of characters needed
	 */
	CIO_API std::size_t print(Newline style, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Gets the number of characters needed for the given native newline style.
	 * This is 0 for None, 1 for Unix and Apple, and 2 for Windows.
	 * Preserve and Native will use the value for the host platform.
	 * 
	 * @param style The newline style
	 * @return the number of characters needed for the style
	 */
	CIO_API std::size_t strlen(Newline style) noexcept;

	/**
	 * Converts all newlines of all possible styles from the input buffer to the desired newline style in the output buffer.
	 * This can result in the output being either smaller or larger than the input.
	 *
	 * If the output style is Newline::None, this method will remove all newlines entirely.
	 * If the output style is Newline::Preserve, this method effectivly is just a byte copier.
	 *
	 * Null terminator bytes (binary 0x00) in the input are treated as the end of input, even if encountered before the end of the input length.
	 *
	 * The result of this method is a pair of sizes. The first represents how many input bytes were processed. The second represents the number of output bytes were generated.
	 *
	 * If the converted output is larger than the available buffer output, the input bytes processed will be less than the input length.
	 * In the case of Newline::Windows, partial newlines (\r without \n) will not be written, either both will be written or neither.
	 *
	 * @param input The input text buffer
	 * @param inputLen The input text buffer length
	 * @param output The output text buffer
	 * @param outputLen The output text buffer length
	 * @param outputStyle The desired newline style for the output
	 * @return a pair of the number of input and output bytes that were processed
	 */
	CIO_API std::pair<std::size_t, std::size_t> convertNewlines(const char *input, std::size_t inputLen, char *output, std::size_t outputLen, Newline outputStyle);

	/**
	 * Gets the text value for a Newline value.
	 * This is the name of the enumerant itself, not its actual character sequence.
	 *
	 * @param style The newline style
	 * @return the text label of the newline style
	 */
	CIO_API const char *label(Newline style) noexcept;

	/**
	 * Prints the text value for a Newline value to a C++ standard output stream.
	 * This prints the newline text, not the enumerant label.
	 *
	 * @param s The stream
	 * @param style The newline style
	 * @return the stream after printing
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Newline style);
}

#endif