/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_PROGRAMTEST_H
#define CIO_PROGRAMTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the cio::Program class.
	 */
	class ProgramTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructs the test.
			 */
			ProgramTest();

			/**
			 * Destructor.
			 */
			virtual ~ProgramTest() noexcept override;

			/**
			 * Tests running a simple executable (the native platform shell) with a known output.
			 * This validates that an executable can be located by name, launched with arguments, and 
			 * its output can be read.
			 */
			void testExecution();

			/**
			 * Tests that the setArguments methods work properly.
			 */
			void setArgumentsTest();
	};
}

#endif