/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Parse.h"

#include "Case.h"
#include "Text.h"

#include <cctype>
#include <cfloat>
#include <climits>
#include <cmath>

namespace cio
{
	std::uint8_t parseNibble(char c) noexcept
	{
		std::uint8_t nibble = 0;

		if (c >= '0' && c <= '9')
		{
			nibble = static_cast<std::uint8_t>((c - '0'));
		}
		else if (c >= 'a' && c <= 'f')
		{
			nibble = static_cast<std::uint8_t>((c - 'a') + 10);
		}
		else if (c >= 'A' && c <= 'F')
		{
			nibble = static_cast<std::uint8_t>((c - 'A') + 10);
		}

		return nibble;
	}

	bool parseNibble(char c, void *nibble) noexcept
	{
		bool valid = false;
		std::uint8_t parsed = 0;

		if (c >= '0' && c <= '9')
		{
			parsed = static_cast<std::uint8_t>((c - '0'));
			valid = true;
		}
		else if (c >= 'a' && c <= 'f')
		{
			parsed = static_cast<std::uint8_t>((c - 'a') + 10);
			valid = true;
		}
		else if (c >= 'A' && c <= 'F')
		{
			parsed = static_cast<std::uint8_t>((c - 'A') + 10);
			valid = true;
		}

		*static_cast<std::uint8_t *>(nibble) = parsed;

		return valid;
	}

	std::uint8_t parseByte(char high, char low) noexcept
	{
		return static_cast<std::uint8_t>((parseNibble(high) << 4) | parseNibble(low));
	}

	bool parseByte(char high, char low, void *byte) noexcept
	{
		std::uint8_t highNibble = 0, lowNibble = 0;
		bool valid = parseNibble(high, &highNibble) && parseNibble(low, &lowNibble);
		*static_cast<std::uint8_t *>(byte) = static_cast<std::uint8_t>((highNibble << 4) | lowNibble);
		return valid;
	}

	std::size_t parseBytes(const Text &input, void *output, std::size_t maxOutput)
	{
		std::size_t count = 0;
		
		if (input && output)
		{
			std::uint8_t* outCur = static_cast<std::uint8_t*>(output);
			std::uint8_t* outEnd = outCur + maxOutput;
			
			const char *inCur = input.begin();
			const char *inEnd = input.end();
			
			while (outCur < outEnd && (inCur + 1) < inEnd && inCur[0] != 0 && inCur[1] != 0)
			{
				if (parseByte(inCur[0], inCur[1], outCur))
				{
					inCur += 2;
					++outCur;
				}
				else
				{
					break;
				}
			}
			
			count = outCur - static_cast<std::uint8_t*>(output);
		}
		
		return count;
	}

	Type detectParsedType(const Text &text) noexcept
	{
		Type result = Type::None;

		if (text)
		{
			result = Type::Text;

			std::size_t length = text.strlen();
			const char *cur = text.begin();
			const char *end = cur + length;

			while (*(end - 1) == 0 && length > 0)
			{
				--end;
				--length;
			}

			if (length == 4 && CIO_STRNCASECMP(cur, "true", 4) == 0)
			{
				result = Type::Boolean;
			}
			else if (length == 5 && CIO_STRNCASECMP(cur, "false", 5) == 0)
			{
				result = Type::Boolean;
			}
			else if (length >= 2 && cur[0] == '0' && cur[1] == 'x')
			{
				cur += 2;
				result = Type::Blob;

				while (cur < end)
				{
					if (!std::isxdigit(*cur))
					{
						result = Type::None;
						break;
					}
				}
			}
			else
			{
				bool hasDecimal = false;
				bool hasExponent = false;
				bool hasExponentValue = false;

				// Skip any leading negative or positive, those don't change the outcome
				if (*cur == '-')
				{
					++cur;
				}
				else if (*cur == '+')
				{
					++cur;
				}

				// Special cases for special real values
				if (end - cur == 8 && CIO_STRNCASECMP(cur, "infinity", 8) == 0)
				{
					result = Type::Real;
				}
				else if (cur - cur == 3 && CIO_STRNCASECMP(cur, "nan", 3) == 0)
				{
					result = Type::Real;
				}
				else
				{
					// start with default assumption of integer unless proven otherwise
					result = Type::Integer;

					while (cur < end)
					{
						// digits are always cool
						if (!std::isdigit(*cur))
						{
							// Hit a decimal point
							if (*cur == '.')
							{
								// First decimal is cool, but only if we haven't already done an exponent
								if (!hasDecimal && !hasExponent)
								{
									// Transition to Real and mark that we saw a decimal point
									result = Type::Real;
									hasDecimal = true;
								}
								// More than one decimal is not cool, or if we already processed an exponent, this is just general text
								else
								{
									result = Type::Text;
									break;
								}
							}
							// Hit a possible exponent delimiter
							else if (*cur == 'e' || *cur == 'E')
							{
								// If we don't have an exponent already and have at least one more byte left...
								if (!hasExponent && (end - cur) > 1)
								{
									// Skip a positive or negative symbol
									if (cur[1] == '-' || cur[1] == '+')
									{
										++cur;
									}

									// if we have at least one digit after the exponent, we're cool
									if (end - cur > 1 && std::isdigit(cur[1]))
									{
										// Transition to Real
										result = Type::Real;
									}
									else
									{
										// Otherwise we're just general text
										result = Type::Text;
										break;
									}
								}
								else
								{
									// multiple exponent symbols or ending on one is just general text
									result = Type::Text;
									break;
								}
							}
							else
							{
								// general text character not processed, this is just text
								result = Type::Text;
								break;
							}
						}

						++cur;
					}
				}
			}
		}

		return result;
	}

	template <typename T, typename U>
	Parse<T> parseGeneral(const Text &text, U max, bool valUnsigned) noexcept
	{
		Parse<T> parse;

		if (text)
		{
			const char *current = text.begin();
			std::size_t length = text.capacity();

			// Skip leading whitespace
			while (parse.parsed < length && std::isspace(*current))
			{
				++parse.parsed;
				++current;
			}

			bool negative = false;
			bool decimalParsed = false;
			int decimalPlaces = 0;

			// Check for sign
			if (parse.parsed < length)
			{
				// Leading - means negative, this is a hard fail for unsigned
				if (*current == '-' && valUnsigned)
				{
					parse.status = Validation::BelowMinimum;
					parse.terminator = '-';
				}
				else
				{
					// Skip a single leading +, but only if it's not at end of input
					if ((*current == '+' || *current == '-') && parse.parsed + 1 < length)
					{
						negative = (*current == '-') ? true : false;
						++parse.parsed;
						++current;
					}

					// Skip a leading decimal, start incrementing decimal places for every digit after
					if (*current == '.')
					{
						decimalParsed = true;
						++parse.parsed;
						++current;
					}

					// We have at least one valid digit, parse results are valid from here on out
					if ((*current >= '0' && *current <= '9') || *current == '.')
					{						
						std::size_t value = static_cast<std::size_t>(*current) - static_cast<std::size_t>('0');
						++parse.parsed;
						++current;
						if (decimalParsed)
							++decimalPlaces;

						while (parse.parsed < length && ((*current >= '0' && *current <= '9') || *current == '.'))
						{
							std::size_t next = 0;
							if (*current == '.')
							{
								//Ran into another decimal, invalid input
								if (decimalParsed)
								{
									parse.status = Validation::InvalidValue;
									break;
								}
								decimalParsed = true;
								next = value;
							}
							else
							{
								next = value * static_cast <std::size_t>(10) + static_cast<std::size_t>(*current) - static_cast<std::size_t>('0');
								if (decimalParsed)
									++decimalPlaces;
							}

							// If we exceed the maximum or if we overflow and wrap around, this is a fail
							if ((!negative && next > max) || next < value)
							{
								parse.status = Validation::AboveMaximum;
								break;
							}
							else if (negative && ((next-1) > max))
							{
								parse.status = Validation::BelowMinimum;
								break;
							}
							else
							{
								value = next;
								++parse.parsed;
								++current;
							}
						}

						U retValue = static_cast<U>(value);
						
						if (parse.parsed + 1 < length && (*current == 'e' || *current == 'E'))
						{
							++parse.parsed;
							++current;

							int sign = 1;
							if (*current == '+')
							{
								++parse.parsed;
								++current;
							}
							else if (*current == '-')
							{
								++parse.parsed;
								++current;
								sign = -1;
							}

							std::size_t d = 0;

							while (parse.parsed < length && (*current >= '0' && *current <= '9'))
							{
								d = d * static_cast<std::size_t>(10) + static_cast<std::size_t>(*current) - static_cast<std::size_t>('0');
								++parse.parsed;
								++current;
							}

							decimalPlaces -= (sign * d);
						}

						if (decimalPlaces != 0)
						{
							U val = static_cast<U>(pow(10.0, -decimalPlaces));
							retValue = retValue * val;
						}

						if (negative)
							retValue = retValue * (-1);

						// Parse terminated due to end of text, but more might be valid
						if (parse.parsed == length)
						{
							parse.value = static_cast<T>(retValue);
							parse.status = Validation::Exact;
							parse.terminator = 0;
							parse.reason = Reason::Underflow;
						}
						// Parse terminated due to whitespace or end of text, great
						else if (*current == 0 || std::isspace(*current))
						{
							parse.value = static_cast<T>(retValue);
							parse.status = Validation::Exact;
							parse.terminator = *current;
						}
						// No validation errors occurred, parse ended early but got something
						else if (parse.status == Validation::None)
						{
							parse.value = retValue;
							parse.status = Validation::Exact;
							parse.terminator = *current;
						}
						else
						{
							parse.reason = Reason::Unexpected;
						}
					}
					//Check for NaN and Infinity
					else
					{
						const char *mark = current;
						while (parse.parsed < length && std::isalpha(*current))
						{
							++parse.parsed;
							++current;
						}

						std::size_t length = current - mark;

						if (length == 3 && cio::equalTextInsensitive(mark, "nan", 3))
						{
							parse.value = negative ? static_cast<T>(-NAN) : static_cast<T>(NAN);
							parse.status = Validation::Exact;
						}
						else if ((length == 8 && cio::equalTextInsensitive(mark, "infinity", 8)) ||
							(length == 3 && cio::equalTextInsensitive(mark, "inf", 3)))
						{
							parse.value = negative ? static_cast<T>(-INFINITY) : static_cast<T>(INFINITY);
							parse.status = Validation::Exact;
						}
						else
						{
							parse.status = Validation::InvalidValue;
						}

						if (parse.parsed == length)
						{
							parse.reason = Reason::Underflow;
						}
						else if (parse.status != Validation::None)
						{
							parse.reason = Reason::Unexpected;
						}
					}
				}
			}
		}

		return parse;
	}

	Parse<std::string> parseGeneralText(const Text &text) noexcept
	{
		Parse<std::string> parse;

		if (text)
		{
			const char *current = text.begin();
			std::size_t length = text.strlen();

			// Skip leading whitespace
			while (parse.parsed < length && std::isspace(*current))
			{
				++parse.parsed;
				++current;
			}

			while (parse.parsed < length && !std::isspace(*current))
			{
				parse.value.push_back(*current);
				++parse.parsed;
				++current;
			}

			// Parse terminated due to end of text, success but more may be available
			if (parse.parsed == length)
			{
				parse.status = Validation::Exact;
				parse.terminator = 0;
				parse.reason = Reason::Underflow;
			}
			// Parse terminated due to whitespace or nul, great
			else
			{
				parse.status = Validation::Exact;
				parse.terminator = *current;
			}
		}
		return parse;
	}

	void parseType(const Text &text, Parse<unsigned char> &p) noexcept
	{
		// Unsigned char represented by digits
		p = parseGeneral<unsigned char>(text, 255u, true);
	}

	void parseType(const Text &text, Parse<char>& p) noexcept
	{
		p = parseChar(text);
	}

	void parseType(const Text &text, Parse<signed char> &p) noexcept
	{
		p = parseChar(text);
	}

	void parseType(const Text &text, Parse<unsigned short>& p) noexcept
	{
		p = parseGeneral<unsigned short>(text, USHRT_MAX, true);
	}
	
	void parseType(const Text &text, Parse<short>& p) noexcept
	{
		p = parseGeneral<short>(text, SHRT_MAX, false);
	}
	
	void parseType(const Text &text, Parse<unsigned int>& p) noexcept
	{
		p = parseGeneral<unsigned int>(text, UINT_MAX, true);
	}
	
	void parseType(const Text &text, Parse<int>& p) noexcept
	{
		p = parseGeneral<int>(text, INT_MAX, false);
	}
	
	void parseType(const Text &text, Parse<unsigned long>& p) noexcept
	{
		p = parseGeneral<unsigned long>(text, ULONG_MAX, true);
	}
	
	void parseType(const Text &text, Parse<long>& p) noexcept
	{
		p = parseGeneral<long>(text, LONG_MAX, false);
	}
	
	void parseType(const Text &text, Parse<double>& p) noexcept
	{
		p = parseGeneral<double>(text, DBL_MAX, false);
	}
	
	void parseType(const Text &text, Parse<long double>& p) noexcept
	{
		p = parseGeneral<long double>(text, LDBL_MAX, false);
	}
	
	void parseType(const Text &text, Parse<float>& p) noexcept
	{
		p = parseGeneral<float>(text, FLT_MAX, false);
	}
	
	void parseType(const Text &text, Parse<std::string>& p) noexcept
	{
		p = parseGeneralText(text);
	}

	TextParse<bool> parseBoolean(const Text &text) noexcept
	{
		TextParse<bool> result;
		std::size_t length = text.strlen();
		std::size_t limit = 0;
		while (limit < length && text[limit] && !std::isspace(text[limit]))
		{
			++limit;
		}
		
		cio::Text tmp = text.prefix(limit);
		if (tmp.equalInsensitive("true") || tmp.equalInsensitive("yes") || tmp.equalInsensitive("on") || tmp.equalInsensitive("1"))
		{
			result.value = true;
			result.status = Validation::Exact;
			result.terminator = (limit < length) ? text[limit] : 0;
			result.parsed = limit;
		}
		else if (tmp.equalInsensitive("false") || tmp.equalInsensitive("no") || tmp.equalInsensitive("off") || tmp.equalInsensitive("0"))
		{
			result.value = false;
			result.status = Validation::Exact;
			result.terminator = (limit < length) ? text[limit] : 0;
			result.parsed = limit;
		}
		else
		{
			TextParse<double> number = cio::parseDouble(text);
			if (number.valid())
			{
				if (std::isnan(number.value) || number.value == 0.0)
				{
					result.value = false;
					result.status = Validation::LosslessConversion;
					result.terminator = number.terminator;
					result.parsed = number.parsed;
				}
				else if (number.value == 1.0)
				{
					result.value = true;
					result.status = Validation::LosslessConversion;
					result.terminator = number.terminator;
					result.parsed = number.parsed;
				}
				else
				{
					result.value = true;
					result.status = Validation::LossyConversion;
					result.terminator = number.terminator;
					result.parsed = number.parsed;
				}
			}
			else
			{
				result.value = false;
				result.status = Validation::InvalidValue;
				result.terminator = (length > 0) ? text[0] : 0;
				result.parsed = 0;
			}
		}

		return result;
	}

	TextParse<unsigned char> parseUnsignedChar(const Text &text) noexcept
	{
		return parseGeneral<unsigned char>(text, 255u, true);
	}

	TextParse<char> parseChar(const Text &text) noexcept
	{
		TextParse<char> p;

		if (text)
		{
			p.value = text[0];
			p.parsed = 1;
			p.status = Validation::Exact;

			if (text.size() > 1)
			{
				p.terminator = text[1];
			}
			else
			{
				p.terminator = 0;
			}
		}
		return p;
	}

	TextParse<unsigned short> parseUnsignedShort(const Text &text) noexcept
	{
		return parseGeneral<unsigned short>(text, 65535u, true);
	}

	TextParse<short> parseShort(const Text &text) noexcept
	{
		return parseGeneral<short>(text, 65535u, false);
	}

	TextParse<unsigned int> parseUnsignedInt(const Text &text) noexcept
	{
		return parseGeneral<unsigned int>(text, 65535u, true);
	}

	TextParse<int> parseInt(const Text &text) noexcept
	{
		return parseGeneral<int>(text, 65535u, false);
	}

	TextParse<unsigned long> parseUnsignedLong(const Text &text) noexcept
	{
		return parseGeneral<unsigned long>(text, ULONG_MAX, true);
	}

	TextParse<long> parseLong(const Text &text) noexcept
	{
		return parseGeneral<long>(text, LONG_MAX, false);
	}

	TextParse<float> parseFloat(const Text &text) noexcept
	{
		return parseGeneral<float>(text, FLT_MAX, false);
	}

	TextParse<double> parseDouble(const Text &text) noexcept
	{
		return parseGeneral<double>(text, DBL_MAX, false);
	}

	TextParse<long double> parseLongDouble(const Text &text) noexcept
	{
		return parseGeneral<long double>(text, LDBL_MAX, false);
	}

	TextParse<std::uint8_t> parseUnsigned8(const Text &text) noexcept
	{
		return parseGeneral<std::uint8_t>(text, 255u, true);
	}

	TextParse<std::uint16_t> parseUnsigned16(const Text &text) noexcept
	{
		return parseGeneral<std::uint16_t>(text, 65535u, true);
	}

	TextParse<std::uint32_t> parseUnsigned32(const Text &text) noexcept
	{
		return parseGeneral<std::uint32_t>(text, UINT32_MAX, true);
	}

	TextParse<std::uint64_t> parseUnsigned64(const Text &text) noexcept
	{
		return parseGeneral<std::uint64_t>(text, UINT64_MAX, true);
	}

	TextParse<unsigned long long> parseUnsigned(const Text &text) noexcept
	{
		return parseGeneral<unsigned long long>(text, ULLONG_MAX, true);
	}

	TextParse<std::int8_t> parseInteger8(const Text &text) noexcept
	{
		return parseGeneral<std::int8_t>(text, 255u, false);
	}

	TextParse<std::int16_t> parseInteger16(const Text &text) noexcept
	{
		return parseGeneral<std::int16_t>(text, 65535u, false);
	}

	TextParse<std::int32_t> parseInteger32(const Text &text) noexcept
	{
		return parseGeneral<std::int32_t>(text, UINT32_MAX, false);
	}

	TextParse<std::int64_t> parseInteger64(const Text &text) noexcept
	{
		return parseGeneral<std::int64_t>(text, UINT64_MAX, false);
	}

	TextParse<long long> parseInteger(const Text &text) noexcept
	{
		return parseGeneral<long long>(text, ULLONG_MAX, false);
	}
}
