/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_CHANNEL_H
#define CIO_NET_CHANNEL_H

#include "Types.h"

#include "Socket.h"

#include <cio/Channel.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace net
	{
		/**
		 * The CIO Network Channel is a general implementation of the CIO Channel interface using network sockets.
		 * This base class abstracts the socket management and provides a baseline implementation that can provide "good enough"
		 * support for any socket-based networking, including socket types not explicitly supoprted by CIO.
		 *
		 * This base implementation provides reasonable implementations of load and store and interfaces to get and set the underlying socket.
		 *
		 * Most users, however, will want to use more specific subclasses such as StreamChannel or DatagramChannel that tailor behavior
		 * more specifically to the type of socket being used, and provide implementations of openForModes.
		 */
		class CIO_NET_API Channel : public cio::Channel
		{
			public:
				/**
				 * Gets the declared metaclass for cio::net::Channel.
				 *
				 * @return the metaclass
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct a channel that is not yet connected to a socket.
				 */
				Channel() noexcept;

				/**
				 * Construct a channel that adopts the given socket.
				 *
				 * @param socket The socket to adopt
				 */
				explicit Channel(Socket &&socket) noexcept;

				/**
				 * Construct a channel from the moved contents of the given channel.
				 *
				 * @param in The channel to move
				 */
				Channel(Channel &&in) noexcept;

				/**
				 * Assign the moved contents of the given channel to this one.
				 *
				 * @param in The channel to move
				 * @return this channel
				 */
				Channel &operator=(Channel &&in) noexcept;

				/**
				 * Destructor. Closes the socket if open.
				 */
				virtual ~Channel() noexcept override;

				/**
				 * Gets the runtime metaclass of this object.
				 *
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears the Channel. This closes and disconnects the socket.
				 */
				virtual void clear() noexcept override;
				
				/**
				 * Checks whether the channel is currently open.
				 * True if the underlying socket has been allocated.
				 *
				 * @return whether the channel is open
				 */
				virtual bool isOpen() const noexcept override;

				/**
				 * Loads data from the next incoming datagram or stream chunk.
				 * Any information on the remote sender of the data will be ignored.
				 *
				 * @warning For non-stream sockets, depending on the protocol and configuration this may lose data
				 * in the next datagram if it has more data than is allocated for the buffer
				 *
				 * @note In some cases, 0 bytes may be received due to a zero-length datagram being valid in the protocol.
				 * However, for stream-based sockets, 0 bytes received usually means end of stream.
				 * Finally, for non-blocking connections, 0 bytes can also occur if no data is available yet.
				 * Sorting these issues out is the responsibility of subclasses; this base class just reports what the socket says.
				 *
				 * @param buffer The buffer to load into
				 * @param length The number of bytes to load
				 * @return the result describing the success of the operation and number of bytes actually received
				 */
				virtual Progress<std::size_t> requestRead(void *buffer, std::size_t length) noexcept override;

				/**
				 * Stores data by sending it to the default connected recipient.
				 * For non-stream sockets, the entire buffer will be sent as one datagram if possible.
				 *
				 * @note Zero length buffers may actually be sent since these are valid datagrams in some configurations.
				 * Some protocols may also support "jumbograms" which are a single logical datagram much larger than the
				 * physical datagram size. Sorting out these issues is the responsibility of subclasses; this base class just
				 * sends the data straight to the socket.
				 *
				 * @param buffer The buffer to store from
				 * @param length The number of bytes to store
				 * @return the result describing the success of the operation and number of bytes actually sent
				 */
				virtual Progress<std::size_t> requestWrite(const void *buffer, std::size_t length) noexcept override;

				/**
				 * Gets the maximum datagram size that can be atomically sent or received by this socket.
				 * The default base implementation uses 65535 for safety.
				 * Most protocols use less due to overhead.
				 *
				 * This does not consider jumbograms or other ways of sending a logical datagram over
				 * multiple physical datagrams, and is purely advisory for stream protocols.
				 *
				 * @return the max datagram size
				 */
				virtual std::size_t getMaxDatagramSize() const noexcept;

				// Pull base class receve(Buffer &) method into scope
				using Input::receive;

				/**
				 * Receives the entirety of the next datagram.
				 * The recipient address will reflect the local interface this was received on.
				 * The sender address will reflect the remote origin of the datagram.
				 * The packet buffer position and limit will reflect the datagram contents read.
				 *
				 * @note For non-stream protocols, zero-length datagrams are entirely valid and still considered a success.
				 * For stream protocols, a zero-length datagram can only occur when either zero bytes are requested or when
				 * the end of stream is reached and the stream is therefore disconnected.
				 *
				 * @warning This method allocates a new buffer and is not very efficient for handling more than one datagram.
				 * Consider setting up your own datagram allocated to the max buffer size and passing it to receive.
				 *
				 * @return the entire next datagram
				 * @throw std::bad_alloc If the memory for the datagram buffer could not be allocated
				 */
				virtual Datagram receive();

				/**
				 * Receives some or all of the next datagram.
				 *
				 * The recipient address will reflect the local interface this was received on.
				 * The sender address will reflect the remote origin of the datagram.
				 * The packet buffer position and limit will reflect the datagram contents read.
				 *
				 * @warning If not enough space was left in the packet buffer, this can lose datagram contents
				 * for non-stream sockets.
				 *
				 * @note For non-stream protocols, zero-length datagrams are entirely valid and still considered a success.
				 * For stream protocols, a zero-length datagram can only occur when either zero bytes are requested or when
				 * the end of stream is reached and the stream is therefore disconnected.
				 *
				 * @param datagram The datagram to populate
				 * @return the result that was also set on the datagram
				 */
				virtual Progress<std::size_t> receive(Datagram &datagram) noexcept;

				/**
				 * Sends an entire datagram to the specified recipient if possible.
				 * The packet buffer's position and limit are used to select what data to send as the datagram.
				 *
				 * @param datagram The datagram to send
				 * @return the result of the send operation
				 */
				virtual Progress<std::size_t> send(Datagram &datagram) noexcept;

				/**
				 * Adopts the given preconfigured socket.
				 *
				 * @param socket The socket to adopt
				 */
				inline void adopt(Socket &&socket) noexcept;

				/**
				 * Releases the current socket from this Channel to the caller.
				 * The Channel will effectively be closed, but the socket itself will remain active.
				 *
				 * @return the released socket
				 */
				inline Socket take() noexcept;

				/**
				 * Gets access to the underlying socket.
				 *
				 * @return the socket
				 */
				inline Socket &get() noexcept;

				/**
				 * Sets the receive timeout option on the socket if possible.
				 * 
				 * @pre the underlying socket must be allocated
				 *
				 * @param timeout The receive timeout in milliseconds
				 * @return the outcome of this request
				 */
				virtual State setReadTimeout(std::chrono::milliseconds timeout) override;
				
				/**
				 * Gets the receive timeout option currently set on the socket.
				 * If this returns 0, no timeout was set.
				 *
				 * @pre the underlying socket must be allocated
				 *
				 * @return the receive timeout
				 */
				virtual std::chrono::milliseconds getReadTimeout() const override;
				
				/**
				 * Sets the send timeout option on the socket if possible.
				 *
				 * @pre the underlying socket must be allocated
				 *
				 * @param timeout The receive timeout in milliseconds
				 * @throw cio::Exception If the timeout could not be set
				 */
				void setWriteTimeout(std::chrono::milliseconds timeout);
				
				/**
				 * Gets the send timeout option currently set on the socket.
				 * If this returns 0, no timeout was set.
				 *
				 * @pre the underlying socket must be allocated
				 *
				 * @return the send timeout
				 */
				std::chrono::milliseconds getWriteTimeout() const;
				
			protected:
				/** The underlying socket used for communications */
				Socket mSocket;
				
			private:
				/** The metaclass for this class */
				static Class<Channel> sMetaclass;
		};
	}
}

namespace cio
{
	namespace net
	{
		inline void Channel::adopt(Socket &&socket) noexcept
		{
			mSocket = std::move(socket);
		}

		inline Socket Channel::take() noexcept
		{
			return std::move(mSocket);
		}

		inline Socket &Channel::get() noexcept
		{
			return mSocket;
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif


