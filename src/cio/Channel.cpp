/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Channel.h"

#include "Class.h"
#include "Exception.h"
#include "Metadata.h"
#include "ModeSet.h"
#include "Path.h"
#include "ProtocolFactory.h"
#include "Resize.h"

namespace cio
{
	Class<Channel> Channel::sMetaclass("Channel");

	const Metaclass &Channel::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Channel::~Channel() noexcept = default;

	const Metaclass &Channel::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	bool Channel::isOpen() const noexcept
	{
		return false;
	}

	ModeSet Channel::open(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Load | Mode::Read | Mode::Write, ProtocolFactory::getDefault());
	}

	ModeSet Channel::create(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Create | Mode::Read | Mode::Write, ProtocolFactory::getDefault());
	}

	ModeSet Channel::openOrCreate(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Load | Mode::Create | Mode::Read | Mode::Write, ProtocolFactory::getDefault());
	}

	ModeSet Channel::openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory)
	{
		return ModeSet();
	}

	void Channel::clear() noexcept
	{
		// nothing to do in base class
	}

	Metadata Channel::metadata() const
	{
		return Metadata(this->location(), this->modes(), this->type(), this->size());
	}

	Path Channel::location() const
	{
		return Path();
	}

	Resource Channel::type() const noexcept
	{
		return Resource::Unknown;
	}

	ModeSet Channel::modes() const noexcept
	{
		return ModeSet();
	}

	Length Channel::size() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	bool Channel::empty() const noexcept
	{
		return this->size() == 0;
	}

	bool Channel::linkedReadWritePosition() const noexcept
	{
		return false;
	}

	Channel::operator bool() const noexcept
	{
		return this->isOpen();
	}
}

