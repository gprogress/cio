/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MEMORYCHANNEL_H
#define CIO_MEMORYCHANNEL_H

#include "Types.h"

#include "Buffer.h"
#include "Channel.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	* The Memory Channel adapts a Buffer memory byte array to support the Channel read/write interface.
	*/
	class CIO_API MemoryChannel : public Channel
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 * 
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Construct an empty memory channel.
			 */
			MemoryChannel() noexcept;

			/**
			 * Copy constructor.
			 * 
			 * @param in The memory channel to copy 
			 */
			MemoryChannel(const MemoryChannel &in);

			/**
			 * Move constructor.
			 * 
			 * @param in The memory channel to move
			 */
			MemoryChannel(MemoryChannel &&in) noexcept;

			/**
			 * Construct and open a memory channel to use the given memory buffer.
			 * The buffer will not own the data and will not be resizable.
			 * 
			 * @param data The buffer data
			 * @param length The buffer length in bytes
			 */
			MemoryChannel(void *data, std::size_t length) noexcept;

			/**
			 * Copy assignment.
			 * 
			 * @param in The memory channel to copy 
			 * @return this buffer
			 */
			MemoryChannel &operator=(const MemoryChannel &in);

			/**
			 * Move assignment.
			 * 
			 * @param in The memory channel to move
			 * @return this buffer
			 */
			MemoryChannel &operator=(MemoryChannel &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~MemoryChannel() noexcept override;

			/**
			 * Gets the metaclass for this object.
			 * 
			 * @return the metaclass for this object 
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Sets the buffer to back this memory channel.
			 * 
			 * @param buffer The buffer to use 
			 */
			void bind(Buffer buffer);

			/**
			 * Sets the buffer to back this memory channel.
			 * The buffer will not own the given memory, and will not be resizable.
			 *
			 * @param data The buffer data
			 * @param length The length of the buffer in bytes
			  */
			void bind(void *data, std::size_t length);

			/**
			 * Removes and returns the current buffer in use.
			 * 
			 * @return the removed buffer
			*/
			Buffer unbind();

			/**
			 * Gets direct access to the underlying current buffer.
			 * Modifications to the buffer will be reflected in the channel.
			 * 
			 * @return the buffer
			 */
			Buffer &buffer() noexcept;

			/**
			 * Gets direct access to the underlying current buffer.
			 *
			 * @return the buffer
			 */
			const Buffer &buffer() const noexcept;

			/**
			 * Clears the memory channel and buffer.
			 */
			virtual void clear() noexcept override;
				
			/**
			* Checks whether the channel is open.
			* This is true if the underlying memory buffer has data.
			*
			* @return whether the channel is open
			*/
			virtual bool isOpen() const noexcept override;
				
			/**
			 * Gets the resize policy of the buffer backing this channel.
			 * 
			 * @return the resize policy 
			 */
			virtual Resize resizable() const noexcept override;
				
			/**
			 * Gets whether the buffer's resize policy supports growing by appending data.
			 * 
			 * @return whether the buffer is appendable
			 */
			virtual bool appendable() const noexcept override;

			/**
			 * Gets the current size of the memory buffer backing this channel.
			 * 
			 * @return the current buffer size 
			 */
			virtual Length size() const noexcept override;

			/**
			 * Checks whether the resource is empty and contains zero bytes.
			 *
			 * @return whether the resource is emtpy
			 */
			virtual bool empty() const noexcept override;

			/**
			 * Gets the theoretical maximum size of the resource.
			 * If the underlying buffer can grow, this is SIZE_MAX. Otherwise it is the buffer size.
			 *
			 * @return the maximum size
			 */
			virtual Length capacity() const noexcept override;

			/**
			 * Attempts to resize the underlying memory buffer.
			 * This is only successful if the buffer's growth policy supports it.
			 * 
			 * @param desired The desired size
			 * @return the actual size and status after this operation
			*/
			virtual Progress<Length> requestResize(Length desired) noexcept override;

			/**
			 * Indicates that the buffer has a shared read and write position.
			 * 
			 * @return true to indicate that read and write positions are linked
			*/
			virtual bool linkedReadWritePosition() const noexcept override;

			/**
			 * Gets the number of readable bytes remaining in the buffer.
			 * 
			 * @return the number of writable bytes 
			 */
			virtual Length readable() const noexcept override;

			/**
			 * Attempts to read the buffer data using a direct memory copy.
			 * 
			 * @param data The data buffer to read into
			 * @param bytes The number of bytes to read
			 * @return the actual number of bytes read and the status of the operation
			 */
			virtual Progress<std::size_t> requestRead(void *data, std::size_t bytes) noexcept override;

			/**
			 * Attempts to read the buffer data at a given position using a direct memory copy.
			 * 
			 * @param data The data buffer to read into
			 * @param bytes The number of bytes to read
			 * @param position The requested start position of the read
			 * @param mode The seek mode for the start position
			 * @return the actual number of bytes read and the status of the operation
			 */
			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			/**
			 * Gets the current read position in the buffer.
			 * 
			 * @return the current read position
			 */
			virtual Length getReadPosition() const noexcept override;

			/**
			 * Sets the current read position in the buffer.
			 * 
			 * @param position The new read position 
			 * @param mode The seek mode for the read position
			 * @return the actual read position and status after this operation
			*/
			virtual Progress<Length> requestSeekToRead(Length position, Seek mode) noexcept override;

			/**
			 * Skips the given number of bytes in the buffer.
			 * 
			 * @param bytes The number of bytes to skip
			 * @return the actual number of bytes skipped and the status of this operation
			 */
			virtual Progress<Length> requestDiscard(Length bytes) noexcept override;

			/**
			 * Gets the number of writable bytes remaining without triggering a resize.
			 * 
			 * @return the number of remaining writable bytes 
			 */
			virtual Length writable() const noexcept override;

			/**
			 * Attempts to write the given data to the buffer by a direct memory copy.
			 * 
			 * @param data The data buffer to write from
			 * @param bytes  The number of bytes to write
			 * @return the actual number of bytes written and the status of this operation
			 */
			virtual Progress<std::size_t> requestWrite(const void *data, std::size_t bytes) noexcept override;

			/**
			 * Attempts to write the given data to the buffer at the given position by a direct memory copy.
			 * 
			 * @param data The data buffer to write from
			 * @param bytes  The number of bytes to write
			 * @param position The start position of the write
			 * @param mode The seek mode for the start position
			 * @return the actual number of bytes written and the status of this operation
			*/
			virtual Progress<std::size_t> requestWriteToPosition(const void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			/**
			 * Gets the current write position in the buffer.
			 * 
			 * @return the current write position 
			 */
			virtual Length getWritePosition() const noexcept override;

			/**
			 * Sets the current write position in the buffer.
			 * 
			 * @param position The write position to set
			 * @param mode The seek mode for the write position
			 * @return The actual write position and status after this operation
			*/
			virtual Progress<Length> requestSeekToWrite(Length position, Seek mode) noexcept override;

			/**
			 * Skips the given number of bytes in the buffer without modifying them.
			 * 
			 * @param bytes The number of bytes to skip
			 * @return the number of bytes actually skipped and the status of this operation
			*/
			virtual Progress<Length> requestSkip(Length bytes) noexcept override;

			/**
			 * Writes the given byte the specified number of times to the buffer.
			 * 
			 * @param value The byte to write 
			 * @param count The number of times to write the byte
			 * @return the actual bytes written and status of this operation
			 */
			virtual Progress<Length> requestSplat(std::uint8_t value, Length count) noexcept override;

			/**
			 * Implements output flushing to show that the operation is synchronous so flush doesn't do anything.
			 * 
			 * @param traversal The flush depth
			 * @return the number of levels flushed, which is always 0
			*/
			virtual Progress<Traversal> flush(Traversal traversal) noexcept override;

			/**
			 * @copydoc Input::getText
			 *
			 * This override directly implements searching the memory buffer for the text end.
			 */
			virtual Text getText(std::size_t bytes = SIZE_MAX) override;

			/**
			 * @copydoc Input::getTextLine
			 *
			 * This override directly implements searching the memory buffer for the text line end.
			 */
			virtual Text getTextLine(std::size_t bytes = SIZE_MAX) override;

		private:
			/** The metaclass for this class */
			static Class<MemoryChannel> sMetaclass;

			/** The memory buffer backing this channel */
			Buffer mBuffer;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
