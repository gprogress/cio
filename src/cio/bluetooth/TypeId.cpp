/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TypeId.h"

#include <cio/Text.h>

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		struct TypeIdEntry
		{
			TypeId id;
			const char *const name;
		};
		
		// Sorted in ID order
		const TypeIdEntry sTypeIds[] = {
			{ TypeId::None , "None" },
			{ TypeId::GAP, "Generic Access" },
			{ TypeId::GATT, "Generic Attribute" },
			{ TypeId::PrimaryService, "Primary Service" },
			{ TypeId::SecondaryService, "Secondary Service" },
			{ TypeId::Include, "Include" },
			{ TypeId::Characteristic, "Characteristic" },
			{ TypeId::CharacteristicExtendedProperties, "Characteristic Extended Properties" },
			{ TypeId::CharacteristicUserDescription, "Characteristic User Description" },
			{ TypeId::ClientCharacteristicConfiguration, "Client Characteristic Configuration" },
			{ TypeId::ServerCharacteristicConfiguration, "Server Characteristic Configuration" },
			{ TypeId::CharacteristicFormat, "Characteristic Presentation Format" },
			{ TypeId::CharacteristicAggregateFormat, "Characteristic Aggregate Formate" },
			{ TypeId::ValidRange, "Valid Range" },
			{ TypeId::DeviceName, "Device Name" },
			{ TypeId::Appearance, "Appearance" },
			{ TypeId::ServiceChanged, "Service Changed" },
			{ TypeId::CentralAddressResolution, "Central Address Resolution" }
		};
		
		bool parse(const Text &text, TypeId &id) noexcept
		{
			// Currently not optimized, just do a search through the array
			
			bool found = false;
			
			for (const TypeIdEntry &entry : sTypeIds)
			{
				if (entry.name == text)
				{
					id = entry.id;
					found = true;
					break;
				}
			}
			
			if (!found)
			{
				id = TypeId::None;
			}
			
			return found;
		}
		
		const char *print(TypeId id) noexcept
		{
			std::size_t low = 0;
			std::size_t high = sizeof(sTypeIds) / sizeof(TypeIdEntry);
			
			const char *result = "Unknown";
			
			while (low < high)
			{
				std::size_t mid = (low + high) / 2;
				const TypeIdEntry &current = sTypeIds[mid];
				if (current.id == id)
				{
					result = current.name;
					break;
				}
				else if (current.id < id)
				{
					low = mid + 1;
				}
				else if (current.id > id)
				{
					high = mid;
				}
			}
			
			return result;
		}
		
		std::ostream &operator<<(std::ostream &stream, TypeId id)
		{
			return stream << print(id);
		}
	}
}

