/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Token.h"

namespace cio
{
	// Constructor implementations
	Token::Token() noexcept :
		mPartial(false),
		mIsWhitespace(false)
	{
		// nothing more to do
	}

	Token::Token(const Source& src, const Text& txt) noexcept :
		mSource(src),
		mPartial(false),
		mIsWhitespace(false)
	{
	}

	Token::Token(const Token &in) noexcept:
		mText(in.mText),
		mSource(in.mSource),
		mPartial(in.mPartial),
		mIsWhitespace(in.mIsWhitespace)
	{
		// nothing more to do
	}	

	Token::Token(Token&& in) noexcept :
		mText(std::move(in.mText)),
		mSource(std::move(in.mSource)),
		mPartial(in.mPartial),
		mIsWhitespace(in.mIsWhitespace)
	{
		// nothing more to do
	}
	
	Token &Token::operator=(const Token &in)
	{
		if (this != &in)
		{
			mText = in.mText;
			mSource = in.mSource;
			mPartial = in.mPartial;
			mIsWhitespace = in.mIsWhitespace;
		}

		return *this;
	}
	
	Token &Token::operator=(Token &&in) noexcept
	{
		if (this != &in)
		{
			mText = std::move(in.mText);
			mSource = std::move(in.mSource);
			mPartial = in.mPartial;
			mIsWhitespace = in.mIsWhitespace;
		}
		return *this;
	}

	void Token::setSource(const Source& src) noexcept
	{
		mSource = src;
	}

	void Token::setText(const Text& txt) noexcept
	{
		mText = txt;
	}

	Type Token::type() const noexcept
	{
		return detectParsedType(mText);
	}

	void Token::referenceText(const char* text, std::size_t len) noexcept
	{
		mText = Text(text, len);
	}
}
