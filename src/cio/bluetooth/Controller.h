/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_CONTROLLER_H
#define CIO_BLUETOOTH_CONTROLLER_H

#include "Types.h"

#include "Device.h"

#include <cio/Buffer.h>
#include <cio/net/Channel.h>

#include <chrono>
#include <mutex>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Controller provides the core interface for managing local Bluetooth adapters.
		 * This can be implemented by one of several underlying technologies including HCI raw sockets with manual packet processing, D-Bus on Linux, and so on.
		 * 
		 * Most users will want to use the Adapter or Connection classes to set up and obtain the best implementation your current platform.
		 * This class is intended to provide the minimal API needed to implement all higher-level queries in a technology-agnostic way.
		 *
		 * The base class provides a placeholder implementation that allows you to get and set a device, but not much beyond that.
		 */
		class CIO_BLUETOOTH_API Controller
		{
			public:							
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct an unconnected Controller.
				 */
				Controller() noexcept;
				
				/**
				 * Construct a Controller for the given Device.
				 * The base class sets the device.
				 * Subclasses should also open it.
				 *
				 * @param device The device
				 */
				explicit Controller(const Device &device);
				
				/**
				 * Constructs a Bluetooth Low Energy Client by adopting the state of a given client.
				 * 
				 * @param in The controller to move
				 */
				Controller(Controller &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Controller.
				 *
				 * @param in The controller to move
				 * @return this controller
				 */
				Controller &operator=(Controller &&in) noexcept;
				
				/**
				 * Destructor. Disconnects the socket.
				 */
				virtual ~Controller() noexcept;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept;
				
				/**
				 * Clears the socket and device.
				 */				
				virtual void clear() noexcept;

				/**
				 * Performs validation checks to determine if the Bluetooth controller is usable and if not, attempt to determine why.
				 * The returned Exception object contains the success state, and if marked as a failure, a general reason why and a specific error message.
				 * 
				 * On Linux platforms, the base implementation should only be called if the DBus implementation was not built, so it indicates failure with
				 * reason Unsupported and a message indicating DBus support was missing.
				 * On all other platforms, the base implementation indicates failure with reason Unimplemented since we do not yet have any non-Linux backend.
				 * 
				 * @return an exception object indicating whether Bluetooth support is usable and, if not, why not
				 */
				virtual Exception validate();
							
				// Basic device identification - controlled by user
							
				/**
				 * Gets the local device associated with this controller.
				 *
				 * @return the local device
				 */
				const Device &getDevice() const noexcept;
				
				/**
				 * Gets the native device index of the local device currently associated with this controller.
				 * The interpretation of this index is platform-dependent.
				 *
				 * @return the native device index
				 */
				std::uintptr_t getIndex() const noexcept;
				
				/**
				 * Gets the address type of the local device.
				 *
				 * @return the device address type
				 */
				AddressType getAddressType() const noexcept;
				
				/**
				 * Sets the address type of the local device.
				 * 
				 * @note This should be possible to autodetect, but CIO does not yet do so.
				 *
				 * @param type the device address type
				 */
				void setAddressType(AddressType type) noexcept;
								
				/**
				 * Sets the device to be associated with this controller.
				 * This closes the currently open device but does not actually open the given device.
				 *
				 * @param device The device to assocaite
				 */
				void setDevice(const Device &device) noexcept;
							
				/**
				 * Gets the address of the currently associated device.
				 * The device must be open for this to work.
				 *
				 * @return the local device address.
				 * @throw cio::Exception If the device wasn't open or if reading the address failed
				 */				
				DeviceAddress getAddress();
				
				/**
				 * Gets the human-readable name for the local device.
				 *
				 * @return the human readable name
				 * @throw std::bad_alloc If the memory for the name could not be allocated
				 * @throw cio::Exception If the device wasn't open or if reading the address failed
				 */
				std::string getName();
		
				/**
				 * Opens the controller using the currently associated device.
				 * If no device is associated, the controller is opened without a device for general discovery.
				 *
				 * @throw cio::Exception If opening the controller failed
				 */				
				void open();
				
				/**
				 * Opens the controller using the specified device.
				 * If the device is empty, the controller is opened without a device for general discovery.
				 *
				 * @param device The device to open
				 * @throw cio::Exception If opening the controller failed
				 */	
				void open(const Device &device);
				
				/**
				 * Opens the controller using the default device for the system.
				 * If no local devices are available, this method fails.
				 *
				 * @param device The device to open
				 * @throw cio::Exception If opening the controller failed or no local devices existed
				 * @return the device that was opened
				 */
				const Device &openDefaultDevice();
				
				/**
				 * Opens the controller without associating a device to be used for general discovery.
				 *
				 * @param device The device to open
				 * @throw cio::Exception If opening the controller
				 */
				void openWithoutDevice();
				
				/**
				 * Checks whether the currently open device is a low energy device.
				 *
				 * @return whether the device is low energy
				 */
				bool isLowEnergy() const noexcept;
				
				/**
				 * Conducts a scanning pass to discover advertisements for nearby devices.
				 * If the same device is discovered multiple times, the most recent advertisement will be used.
				 * This scan cannot be canceled and will run until the timeout elapses or the local device itself is no longer available.
				 * 
				 * Internally this checks to see whether this is a low-energy device or not.
				 * If yes, then a low energy scan is started and an advertisement listener is constructed and used to process
				 * discovered advertisements. The low energy scan is then completed at the end of the timeout.
				 *
				 * Currently this cannot perform classic BR/EDR inquiries for non-low-energy scans but that will be added in future versions.
				 *
				 * @param timeout The amount of time to scan
				 * @return discovered devices
				 */
				std::vector<Peripheral> discoverNearbyDevices(std::chrono::milliseconds timeout);
				
				/**
				 * Conducts a scanning pass to discover advertisements for nearby devices using the given listener.
				 * If the same device is discovered multiple times, each instance may be provided separately to the listener.
				 * The listener specifies the task parameters such as timeout and cancelation mechanism.
				 * 
				 * Internally this checks to see whether this is a low-energy device or not.
				 * If yes, then a low energy scan is started and the advertisement listener is used to process
				 * discovered advertisements. The low energy scan is then completed at the end of the timeout.
				 *
				 * Currently this cannot perform classic BR/EDR inquiries for non-low-energy scans but that will be added in future versions.
				 *
				 * @param listener The listener to receive advertisements and control task parameters
				 * @return the final State of the scan (Success for completed, Abandoned if canceled, or some failure mode if the scan failed)
				 */
				State discoverNearbyDevices(AdvertisementListener &listener);

				// Connection management
				
				/**
				 * Opens the device that is currently associated with this controller.
				 * This should be used to set up communication with the device itself in whatever way is necessary for the platform.
				 *
				 * If no device is currently associated, this should open the controller in a general mode suitable for device discovery
				 * that supports findDefaultDevice() and discoverLocalDevices().
				 */
				virtual void openCurrentDevice();
				
				/**
				 * Checks whether a local device is currently open.
				 *
				 * @return whether a local device is currently open
				 */
				virtual bool isOpen() const noexcept;
				
				/**
				 * Closes the currently open local device without otherwise clearing the controller state.
				 * The device remains associated with this controller but will no longer support queries.
				 */
				virtual void close() noexcept;
				
				// Local Device discovery
				
				/**
				 * Discovers the default device for the system.
				 * Typically this is device index 0 but it's entirely up to the native system platform.
				 * This method may be used even if the controller was not opened to a specific device.
				 *
				 * @return the default device, or none if not found
				 */
				virtual Device findDefaultDevice();
							
				/**
				 * Scans the local system to find all active Bluetooth devices.
				 * This method may be used even if the controller was not opened to a specific device.
				 *
				 * @return the list of known devices
				 * @throw cio::Exception If there was a failure in setting up the HCI connection to scan for local devices
				 * @throw std::bad_alloc If the memory for the list could not be allocated
				 */
				virtual std::vector<Device> discoverLocalDevices();
				
				/**
				 * Reads the local address from the currently open device.
				 * This returns the empty address if no device is open.
				 *
				 * @return the local device address
				 * @throw cio::Exception If the local device is open but a low-level hardware error occurred reading the address
				 */
				virtual DeviceAddress readLocalAddress();
				
				/**
				 * Reads the local device name from the currently open device.
				 * This prints the empty string if no device is open.
				 *
				 * @param text The buffer to store the device name into
				 * @param length the maximum number of text bytes available to store
				 * @return the total number of bytes needed for the name (which may be more or less than the buffer)
				 */
				virtual std::size_t readLocalName(char *text, std::size_t length);
			
				// Peripheral discovery - advertisements
				
				/**
				 * Uses the controller to start the Bluetooth Low Energy discovery process.
				 * This is a mode setting that enables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 * Scanning is inherently asynchronous so use listenForAdvertisements method to actually receive the scan results.
				 *
				 * Some controller implementations may provide additional control over the scanning parameters.
				 *
				 * If scanning is already in progress, this method should do nothing and return State::Exists.
				 * 
				 * @return the State which should either be Success if scanning was started or Exists if scanning was already in progress
				 */
				virtual State startLowEnergyScan();
				
				/**
				 * Uses the controller to recieve advertisements for peripherals for an execution period.
				 * The receiver callback function is called once for each device advertisement until the execution's timer has elapsed or the execution
				 * has been asynchronously canceled by another thread.
				 *
				 * The same peripheral may be discovered more than once if more than advertisement was seen during the execution time period.
				 * The result of this method is the number of advertisements recieved and the overall State, which should be Success if the full
				 * execution time elapsed, Abandoned if the scanning was canceled, or some other error if scanning was either not enabled or a hardware error occurred.
				 *
				 * The final result of the scan will also be stored into the listener task itself to mark execution as complete, abandoned, etc.
				 * 
				 * @param listener the advertisement listener to specify task parameters and receive peripherals
				 * @return the result of the scanning in terms of number of advertisements and final State
				 */
				virtual Progress<std::size_t> listenForAdvertisements(AdvertisementListener &listener) noexcept;
				
				/**
				 * Uses the controller to stop the Bluetooth Low Energy discovery process if it was enabled.
				 * This is a mode setting that disables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 * 
				 * @return the State which should either be Success if scanning was stopped or Missing if scanning was not enabled at all
				 */
				virtual State stopLowEnergyScan();
				
				// Peripheral Direct Requests
				
				/**
				 * Gets the peripheral metadata given the peripheral's MAC device address.
				 *
				 * @param mac The device address of the peripheral
				 * @param exec The task controlling timeout and cancellation
				 * @return the peripheral information if known
				 */
				virtual Peripheral requestPeripheralMetadata(const DeviceAddress &mac, Task &exec);
				
				/**
				 * Finds a peripheral by name and returns its metadata if found.
				 *
				 * @param name The name of the peripheral
				 * @param length The number of bytes in the peripheral name
				 * @param exec The task controlling timeout and cancellation
				 * @return the peripheral information if known
				 */
				virtual Peripheral findPeripheralByName(const char *name, std::size_t length, Task &exec);
				
				// Peripheral connection and disconnection
				
				/**
				 * Uses the current device controller to asynchronously establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Connected.
				 *
				 * This method may (and likely will) return before a connection is actually established.
				 * Bluetooth connection methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * Note that it is not an error if the peripheral is not currently visible to the device.
				 * It is not unusual for Bluetooth devices to appear and disappear depending on local conditions.
				 *
				 * @param peripheral The peripheral to connect to
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Connected if the peripheral was already connected, State::Requested if the request was made
				 */ 
				virtual State startConnection(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Uses the current device controller to asynchronously disconnect a connection to the given remote peripheral.
				 * If the peripheral is known but no such connection already exists, this method should return State::Disconnected.
				 * If the peripheral itself is not known, then this method should return State::Missing.
				 *
				 * This method may (and likely will) return before the connection is actually disconnected.
				 * Bluetooth connection methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * @param peripheral The peripheral to disconnect from
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Missing if no such peripheral is known, State::Disconnected if the peripheral is already disconnected, or State::Requested if the request was made
				 */ 
				virtual State startDisconnection(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Waits for the next connection event from the device relevant to the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the State of the connection (Connected for connected, Disconnected for disconnection, Timeout if this method timed out, error state for failure to connect)
				 */
				virtual State waitForConnectionState(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Requests the current connection state for the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the State of the connection (Connected for connected, Disconnected for exists but not connected, Missing if no such peripheral is known)
				 */
				virtual State requestConnectionState(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Uses the current device controller to establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Exists.
				 *
				 * This method is equivalent to connecting with the default timeout of 20 seconds.
				 *
				 * @param peripheral The peripheral to connect to
				 * @return State::Success upon completion or State::Connected if the peripheral was already connected
				 * @throw cio::Exception If the process of connecting failed
				 */ 			
				State connect(const Peripheral &peripheral);
				
				/**
				 * Uses the current device controller to establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Exists.
				 *
				 * This method uses the given timeout to construct the task needed to execute the connection.
				 *
				 * @param peripheral The peripheral to connect to
				 * @param timeout The maximum amount of time to attempt to connect
				 * @return State::Success upon completion or State::Connected if the peripheral was already connected
				 * @throw cio::Exception If the process of connecting failed
				 */ 				
				State connect(const Peripheral &peripheral, std::chrono::milliseconds timeout);
				
				/**
				 * Uses the current device controller to establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Exists.
				 *
				 * This method uses the asynchronous startConnection then waits until the connection is actually completed.
				 * The given task is used to manage the timeout and cancellation of this operation.
				 *
				 * @param peripheral The peripheral to connect to
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Success upon completion or State::Exists if the peripheral was already connected
				 * @throw cio::Exception If the process of connecting failed
				 */ 
				State connect(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Disconnects the currently active connection to the given remote peripheral.
				 * If such a connection does not exist, this method should return State::Missing but not otherwise fail.
				 *
				 * This method uses the asynchronous startDisconnection then waits until the disconnection is actually completed.
				 * The given task is used to manage the timeout and cancellation of this operation.
				 *
				 * @param peripheral The peripheral to disconnect from
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Success upon completion or State::Missing if the peripheral was not actually connected
				 * @throw cio::Exception If no local device is selected or if the process of disconnecting failed
				 */
				State disconnect(const Peripheral &peripheral, Task &exec);
				
				// Peripheral pairing and unpairing
				
				/**
				 * Uses the current device controller to asynchronously pair with the given remote peripheral for authentication.
				 * If the current device and peripheral are already paired, this should reuse the pairing and return State::Exists.
				 *
				 * This method may (and likely will) return before the devices are actually paired.
				 * Bluetooth pairing methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * @note This method currently does not provide for advanced agent-based pairing to negotiate input/output capabilities.
				 * Future versions of CIO will enable that.
				 *
				 * @param peripheral The peripheral to pair with
				 * @return State::Success upon completion or State::Exists if the peripheral was already paired
				 * @throw cio::Exception If the process of pairing failed
				 */ 
				virtual State startPairing(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Unpairs the current device with the given peripheral.
				 * If the devices are not paired, this method should return State::Missing but not otherwise fail.
				 *
				 * @param peripheral The peripheral to unpair from
				 * @return State::Success upon completion or State::Missing if the peripheral was not actually paired
				 * @throw cio::Exception If no local device is selected or if the process of unpairing failed
				 */
				virtual State startUnpairing(const Peripheral &peripheral, Task &exec);
					
				/**
				 * Waits for the next pairing event from the device relevant to the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the State of the connection (Connected for paired, Disconnected for unpaired, Timeout if this method timed out, error state for failure to pair)
				 */
				virtual State waitForPairingState(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Requests the current pairing state for the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the State of the pairing (Connected for paired, Disconnected for exists but not paired, Missing if no such peripheral is known)
				 */
				virtual State requestPairingState(const Peripheral &peripheral, Task &exec);
				
				State pair(const Peripheral &peripheral, Task &exec);
				
				State unpair(const Peripheral &peripheral, Task &exec);
				
				/**
				 * Interprets the device in a Boolean context.
				 * It is considered true if the device is a valid device index.
				 *
				 * @return whether the device is valid
				 */
				explicit operator bool() const noexcept;
			
			private:
				/** The metaclass for this class */
				static Class<Controller> sMetaclass;

				/** The device */
				Device mDevice;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

