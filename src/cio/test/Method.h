/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_METHOD_H
#define CIO_TEST_METHOD_H

#include "Types.h"

#include "Test.h"

#include <cio/Class.h>

namespace cio
{
	namespace test
	{
		/**
		 * The Test Method class provides the basic fixture infrastructure for unit testing via a method on another test class.
		 * It is a single C++ method that is executed during the class's execute method using the parent test.
		 *
		 * For test hierarchies, the Test class also manages test cases, which are sub-tests that can be run while the parent test is active.
		 * Test cases must be written so they can run in multiple threads.
		 *
		 * Tests can be named and/or given resource paths to form a name hierarchy. This is not used for the testing itself, but can
		 * be used by the Runner or other classes to selectively enable or disable tests.
		 */
		template <typename T, typename F /* = void (T::*)() */>
		class Method : public Test
		{
			public:
				/**
				 * Gets the declared metaclass for Test.
				 *
				 * @return the metaclass for Test
				 */
				inline static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct an empty Method test.
				 * It will not be executable until a method is bound to it.
				 */
				inline Method() noexcept;

				/**
				 * Construct a test labeled with the given resource path and executor.
				 * The test short name is taken from the leaf filename of the path.
				 *
				 * @param path The resource path for the test
				 * @param executor The test method that will run during execute()
				 */
				Method(Text path, F method);

				/**
				 * Construct a test labeled with the given resource path and executor.
				 * The test short name is taken from the leaf filename of the path.
				 *
				 * @param path The resource path for the test
				 * @param executor The test method that will run during execute()
				 */
				Method(Text parentPath, const char *name, F method);

				/**
				 * Construct a copy of a test.
				 *
				 * @param in The test to copy
				 */
				explicit Method(const Method<T, F> &in) = default;

				/**
				 * Construct a test by transferring the input of a given test.
				 *
				 * @param in The test to move
				 */
				Method(Method<T, F> &&in) noexcept;

				/**
				 * Assign a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Method<T, F> &operator=(const Method<T, F> &in) = default;

				/**
				 * Transfer the contents of a test into this one.
				 *
				 * @param in The test to move
				 */
				Method<T, F> &operator=(Method<T, F> &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Method() noexcept override = default;

				/**
				 * Gets the metaclass for the runtime type of this Test class or subclass.
				 * This must be overridden by subclasses to enable polymorphic cloning and transfers.
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears all content of the test including executors and test cases.
				 */
				virtual void clear() noexcept override;

				/**
				 * Conducts standalone execution of the test using the given logger.
				 * This class constructs a default instance of T and then uses that as the parent context.
				 *
				 * @param logger The logger to use for notification of test start, complete, and failure events
				 * @return the highest severity of any test fault during the test conducted
				 */
				virtual Severity execute(Reporter &logger) override;

				/**
				 * Conducts execution of the test using the given context and logger.
				 * If the provided context has a public base class of T, then it is used to invoke the method.
				 * Otherwise, a default instance of T is constructed to use for that.
				 *
				 * @param context The context to use possibly for invocation
				 * @param logger The logger to use for notification of test start, complete, and failure events
				 * @return the highest severity of any test fault during the test conducted
				 */
				virtual Severity executeInContext(Test *context, Reporter &logger) override;

				/**
				 * Conducts execution of the test using the given context and logger.
				 * If any test in the provided context has a public base class of T, then it is used to invoke the method.
				 * Otherwise, a default instance of T is constructed to use for that.
				 * If more than one test in the context has a public T, the most specific (rightmost) test will be used.
				 *
				 * @param context The list of contexts to consider
				 * @param depth The number of contexts
				 * @param logger The logger to use for notification of test start, complete, and failure events
				 * @return the highest severity of any test fault during the test conducted
				 */
				virtual Severity executeInFullContext(Test **context, std::size_t depth, Reporter &logger) override;

				/**
				 * Invokes the test method on the given object and properly catches and logs all faults and exceptions.
				 *
				 * @param object The object to invoke the test method on
				 * @param logger The logger to use to report information
				 * @return the highest severity of any faults
				 */
				Severity invoke(T *object, Reporter &logger) noexcept;

				/**
				 * Binds the given method to this test.
				 */
				void bind(F method);

			private:
				static const Class<Method<T, F>> sMetaclass;

				F mMethod;
		};
	}
}

#include "Assert.h"
#include "Reporter.h"

/* Inline implementation */
namespace cio
{
	namespace test
	{
		template <typename T, typename F>
		const Class<Method<T, F>> Method<T, F>::sMetaclass("cio::test::Method");

		template <typename T, typename F>
		inline const Metaclass &Method<T, F>::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		template <typename T, typename F>
		inline Method<T, F>::Method() noexcept :
			mMethod()
		{
			// nothing more to do
		}

		template <typename T, typename F>
		Method<T, F>::Method(Text path, F method) :
			Test(std::move(path)),
			mMethod(std::move(method))
		{
			// nothing more to do
		}

		template <typename T, typename F>
		Method<T, F>::Method(Text parentPath, const char *name, F method) :
			Test(parentPath + "::" + name),
			mMethod(std::move(method))
		{
			// nothing more to do
		}

		template <typename T, typename F>
		Method<T, F>::Method(Method<T, F> &&in) noexcept :
			Test(std::move(in)),
			mMethod(std::move(in.mMethod))
		{
			// nothing more to do
		}

		template <typename T, typename F>
		Method<T, F> &Method<T, F>::operator=(Method<T, F> &&in) noexcept
		{
			if (this != &in)
			{
				Test::operator=(std::move(in));
				mMethod = std::move(in.mMethod);
			}

			return *this;
		}

		template <typename T, typename F>
		const Metaclass &Method<T, F>::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		template <typename T, typename F>
		void Method<T, F>::clear() noexcept
		{
			mMethod = F();
			Test::clear();
		}

		template <typename T, typename F>
		Severity Method<T, F>::execute(Reporter &logger)
		{
			T context;
			return this->invoke(&context, logger);
		}

		template <typename T, typename F>
		Severity Method<T, F>::executeInContext(Test *context, Reporter &logger)
		{
			Severity result = Severity::None;
			T *derived = dynamic_cast<T *>(context);

			if (derived)
			{
				result = this->invoke(derived, logger);
			}
			else
			{
				result = this->execute(logger);
			}

			return result;
		}

		template <typename T, typename F>
		Severity Method<T, F>::executeInFullContext(Test **context, std::size_t depth, Reporter &logger)
		{
			Severity result = Severity::None;
			std::size_t current = depth;
			T *derived = nullptr;

			while (current > 0 && !derived)
			{
				derived = dynamic_cast<T *>(context[current - 1]);
				--current;
			}

			if (derived)
			{
				result = this->invoke(derived, logger);
			}
			else
			{
				result = this->execute(logger);
			}

			return result;
		}

		template <typename T, typename F>
		Severity Method<T, F>::invoke(T *object, Reporter &logger) noexcept
		{
			Severity severity = Severity::Info;
			logger.start(*this);

			try
			{
				severity = std::max(severity, object->setUp());
				(object->*mMethod)();
			}
			catch (const Fault &fault)
			{
				logger.fail(*this, fault);
				severity = std::max(severity, fault.getSeverity());
			}
			catch (const std::exception &e)
			{
				logger.fail(*this, Fault(e.what(), CIO_CURRENT_SOURCE(), Severity::Critical));
				severity = Severity::Critical;
			}
			catch (...)
			{
				logger.fail(*this, Fault("Unexpected exception", CIO_CURRENT_SOURCE(), Severity::Critical));
				severity = Severity::Critical;
			}

			object->tearDown();
			logger.complete(*this, severity);
			return severity;
		}

		template <typename T, typename F>
		inline void Method<T, F>::bind(F method)
		{
			mMethod = std::move(method);
		}
	}
}

#endif
