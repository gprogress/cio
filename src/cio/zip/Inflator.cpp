/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/

#include "Inflator.h"

#include "Archive.h"
#include "Protocol.h"

#include <cio/Class.h>
#include <cio/ModeSet.h>
#include <cio/ProtocolFactory.h>
#include <cio/Resize.h>
#include <cio/Resource.h>

#include <zlib.h>

namespace cio
{
	namespace zip
	{
		Class<Inflator> Inflator::sMetaclass("cio::zip::Inflator");

		const Metaclass &Inflator::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		int Inflator::validateWindowBits(int value) noexcept
		{
			int result = value;

			if (value < -15)
			{
				value = -15;
			}
			else if (value > -8 && value < 0)
			{
				value = -8;
			}
			else if (value > 0 && value < 8)
			{
				value = 8;
			}
			else if (value > 15 && value < 24)
			{
				value = 24;
			}
			else if (value > 31)
			{
				value = 31;
			}
			
			return value;
		}

		int Inflator::detectWindowBits(Input &input)
		{
			int windowBits = 15; // deflate to regular Deflate

			Metadata md = input.metadata();
			std::pair<Path, Path> archiveEntry = cio::zip::Protocol::splitArchiveEntry(md.getLocation());
			if (!archiveEntry.first.empty())
			{
				if (archiveEntry.second.empty()) // processing a single file
				{
					if (md.getLocation().matchesExtension("gz"))
					{
						windowBits = 31;
					}
				}
				else // processing an archive, assume raw entry
				{
					windowBits = -15;
				}
			}

			return windowBits;
		}

		Inflator::Inflator() noexcept :
			mDecoder(nullptr),
			mPosition(0),
			mWindowBits(0)
		{
			// nothing more to do
		}

		Inflator::Inflator(std::shared_ptr<Input> input, int windowBits) :
			mDecoder(nullptr),
			mPosition(0),
			mWindowBits(0)
		{
			this->openWithInput(std::move(input), windowBits);
		}
		
		Inflator::Inflator(std::shared_ptr<Input> input, Metadata metadata, int windowBits) :
			mDecoder(nullptr),
			mPosition(0),
			mWindowBits(0)
		{
			// TODO check metadata

			this->openWithInput(std::move(input), windowBits); // clears metadata
			mMetadata = std::move(metadata);
		}
		
		// VS 2017 will not let us default this method
		Inflator::Inflator(Inflator &&in) noexcept :
			cio::Input(std::move(in)),
			mSource(std::move(in.mSource)),
			mDecoder(in.mDecoder),
			mLoadBuffer(std::move(in.mLoadBuffer)),
			mMetadata(std::move(in.mMetadata)),
			mPosition(in.mPosition),
			mWindowBits(in.mWindowBits)
		{
			in.mDecoder = nullptr;
		}

		// VS 2017 will not let us default this method
		Inflator &Inflator::operator=(Inflator &&in) noexcept
		{
			if (this != &in)
			{
				this->dispose();
				cio::Input::operator=(std::move(in));
				mSource = std::move(in.mSource);
				mDecoder = in.mDecoder;
				in.mDecoder = nullptr;
				mLoadBuffer = std::move(in.mLoadBuffer);
				mMetadata = std::move(in.mMetadata);
				mPosition = in.mPosition;
				mWindowBits = in.mWindowBits;
			}

			return *this;
		}

		Inflator::~Inflator() noexcept
		{
			this->dispose();
		}
		
		bool Inflator::isOpen() const noexcept
		{
			return mSource && mSource->isOpen();
		}

		const Metaclass &Inflator::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		Metadata Inflator::metadata() const
		{
			return mMetadata;
		}

		Path Inflator::location() const
		{
			return mMetadata.getLocation();
		}

		Resource Inflator::type() const noexcept
		{
			return mMetadata.getType();
		}

		ModeSet Inflator::modes() const noexcept
		{
			return mMetadata.getMode();
		}

		Length Inflator::size() const noexcept
		{
			return mMetadata.getLength();
		}
			
		cio::ModeSet Inflator::openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory)
		{
			this->dispose();

			cio::ModeSet actual;

			if (modes.count(Mode::Read) && factory)
			{
				std::pair<Path, Path> archiveEntry = cio::zip::Protocol::splitArchiveEntry(resource);
				std::shared_ptr<Input> input;

				if (archiveEntry.first.empty())
				{
					throw Exception(Reason::Invalid, "No resource specified to open");
				}
				
				int windowBits = 15;

				if (archiveEntry.second.empty())
				{
					input = factory->openInput(archiveEntry.first, modes);
					
					if (archiveEntry.first.matchesExtension("gz"))
					{
						windowBits = 31;
					}
				}
				else
				{
					std::shared_ptr<Input> zipInput = factory->openInput(archiveEntry.first, modes);

					Archive archive;
					archive.openZipInput(zipInput);
					archive.seekToEntry(archiveEntry.second);

					RelativeInput record = archive.getCurrentFileRecord();
					input = archive.openFileRecordInput(record);

					windowBits = -15;
				}

				this->openWithInput(input, windowBits);
				actual = mSource->modes();
			}

			return actual;
		}

		void Inflator::openWithInput(std::shared_ptr<Input> input, int windowBits)
		{
			this->dispose();
			mSource = std::move(input);
			this->configureNewInput(windowBits);
		}

		void Inflator::openWithUnownedInput(Input *input, int windowBits)
		{
			this->dispose();
			mSource = cio::not_shared_ptr(input);
			this->configureNewInput(windowBits);
		}

		std::shared_ptr<Input> Inflator::getInput() noexcept
		{
			return mSource;
		}

		std::shared_ptr<const Input> Inflator::getInput() const noexcept
		{
			return mSource;
		}

		std::shared_ptr<Input> Inflator::takeInput()
		{
			return std::move(mSource);
		}

		void Inflator::configureNewInput(int windowBits)
		{
			if (mLoadBuffer.size() == 0)
			{
				mLoadBuffer.allocate(4096);
			}
			
			mLoadBuffer.limit(0);
			z_stream *decoder = new z_stream();
			mDecoder = decoder;
			decoder->zalloc = 0;
			decoder->zfree = 0;
			decoder->opaque = 0;
			decoder->next_in = 0;
			decoder->avail_in = 0;

			// Detect window bits
			int validated = Inflator::validateWindowBits(windowBits);
			if (validated == 0)
			{
				validated = Inflator::detectWindowBits(*mSource);
			}
			mWindowBits = validated;

			// Let's gooooo
			int result = inflateInit2(decoder, mWindowBits);

			if (result != Z_OK)
			{
				switch (result)
				{
					case Z_MEM_ERROR:
						throw Exception(Reason::Exhausted, decoder->msg);

					case Z_STREAM_ERROR:
						throw Exception(Reason::Invalid, decoder->msg);

					case Z_VERSION_ERROR:
						throw Exception(Reason::Incompatible, decoder->msg);

					default:
						throw Exception(Reason::Unknown, decoder->msg);
				}
			}
			
			// Metadata will always be of type File and read only
			mMetadata.setType(Resource::File);
			mMetadata.setMode(ModeSet::readonly());
		}
		
		void Inflator::setMetadata(Metadata metadata) noexcept
		{
			mMetadata = std::move(metadata);
		}
				
		void Inflator::setMetadata(Path path, Length uncompressed) noexcept
		{
			mMetadata.setLocation(std::move(path));
			mMetadata.setLength(uncompressed);
		}
		
		Progress<std::size_t> Inflator::refill()
		{
			Progress<std::size_t> result(Action::Read);

			if (mSource)
			{
				result = mSource->receive(mLoadBuffer);
			}

			return result;
		}
				
		Length Inflator::getReadPosition() const noexcept
		{
			return mPosition;
		}
				
		Progress<std::size_t> Inflator::requestRead(void *buffer, size_t bufferLen) noexcept
		{
			Progress<std::size_t> result(Action::Read, 0, bufferLen);
			Progress<std::size_t> fillProgress;

			if (mSource)
			{
				while (!result.failed() && result.count < bufferLen)
				{
					if (!mLoadBuffer.available())
					{
						fillProgress = this->refill();
					}

					if (fillProgress.failed())
					{
						result.fail(fillProgress.reason);
					}
					else if (mLoadBuffer.available())
					{
						z_stream *decoder = static_cast<z_stream *>(mDecoder);
						Bytef *bufferBytes = static_cast<Bytef *>(mLoadBuffer.current());
						decoder->next_in = bufferBytes;
						decoder->avail_in = static_cast<unsigned>(mLoadBuffer.remaining());
						decoder->next_out = reinterpret_cast<Bytef *>(buffer) + result.count;
						decoder->avail_out = static_cast<unsigned>(bufferLen - result.count);
						int zProgress = inflate(decoder, Z_SYNC_FLUSH);
						result.count = (bufferLen - decoder->avail_out);
						mPosition += result.count;
						mLoadBuffer.skip(decoder->next_in - bufferBytes);

						switch (zProgress)
						{
							case Z_OK:
								result.succeed();
								break;

							case Z_STREAM_END:
								if (result.count > 0)
								{
									result.succeed(Reason::Underflow);
								}
								else
								{
									result.fail(Reason::Underflow);
								}
								break;

							case Z_DATA_ERROR:
								result.fail(Reason::Unparsable);
								sMetaclass.error() << "Zlib failed to inflate: " << decoder->msg;
								break;

							case Z_STREAM_ERROR:
								result.fail(Reason::Invalid);
								sMetaclass.error() << "Zlib failed to inflate: " << decoder->msg;
								break;

							case Z_MEM_ERROR:
								result.fail(Reason::Exhausted);
								sMetaclass.error() << "Zlib failed to inflate: " << decoder->msg;
								break;

							case Z_BUF_ERROR:
								result.complete(Reason::Overflow);
								break;

							default:
								result.fail(Reason::Unknown);
								sMetaclass.error() << "Zlib failed to inflate: " << decoder->msg;
								break;
						}
					}
				}
			}

			return result;
		}

		Progress<std::size_t> Inflator::requestReadBuffer(std::size_t bytes) noexcept
		{
			Progress<std::size_t> result;

			if (mSource)
			{
				mLoadBuffer.compact();
				mLoadBuffer.flip();

				if (mLoadBuffer.remaining() < bytes)
				{
					mLoadBuffer.reallocate(bytes);
					result.count = bytes;
					result.succeed();
				}
				else
				{
					result.count = mLoadBuffer.trim();
					result.succeed(Reason::Overflow);
				}
			}
			else
			{
				mLoadBuffer.reallocate(bytes);
				result.count = bytes;
				result.succeed();
			}

			return result;
		}

		Progress<std::size_t> Inflator::preload(std::size_t bytes) noexcept
		{
			Progress<std::size_t> result;

			if (mSource)
			{
				if (mLoadBuffer.remaining() < bytes)
				{
					this->refill();

					if (mLoadBuffer.remaining() < bytes)
					{
						result.succeed(Reason::Underflow);
					}
					else
					{
						result.succeed();
					}
				}
				else
				{
					result.succeed();
				}

				result.count = mLoadBuffer.remaining();
			}

			return result;
		}

		void Inflator::clear() noexcept
		{
			this->dispose();
			Input::clear();
		}

		void Inflator::dispose()
		{
			if (mDecoder)
			{
				z_stream *decoder = static_cast<z_stream *>(mDecoder);
				inflateEnd(decoder);
				delete decoder;
				mDecoder = nullptr;
			}

			mSource.reset();
			mMetadata.clear();
			mPosition = 0;
		}

		int Inflator::getWindowBits() const noexcept
		{
			return mWindowBits;
		}

		bool Inflator::isGzip() const noexcept
		{
			return mWindowBits >= 24 && mWindowBits <= 31;
		}

		bool Inflator::isDeflate() const noexcept
		{
			return mWindowBits >= 8 && mWindowBits <= 15;
		}

		bool Inflator::isRawEntry() const noexcept
		{
			return mWindowBits >= -15 && mWindowBits <= -8;
		}
	}
}
