/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_PROPERTIES_H
#define CIO_BLUETOOTH_PROPERTIES_H

#include "Types.h"

#include <iosfwd>
#include <string>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Properties class represents a set of enabled characteristic properties
		 * encoded in its native BLE representation of an 8-bit unsigned integer.
		 */
		class CIO_BLUETOOTH_API Properties
		{
			public:
				/**
				 * Construct the Properties with no properties enabled.
				 */
				Properties() noexcept;
				
				/**
				 * Construct the Properties with the given binary value.
				 *
				 * @param value The value
				 */
				explicit Properties(std::uint8_t value) noexcept;
				
				/**
				 * Construct the properties initialized with the given property enabled.
				 *
				 * @param value The property to enable
				 */
				Properties(Property value) noexcept;
				
				/**
				 * Gets whether the given property is enabled.
				 *
				 * @param value The property
				 * @return whether the property was enabled
				 */
				bool get(Property value) const noexcept;
				
				/**
				 * Sets whether the given property is enabled.
				 *
				 * @param value The property
				 * @param value whether the property should be enabled
				 */
				void set(Property value, bool enabled = true) noexcept;
				
				/**
				 * Clears the properties so none are enabled.
				 */
				void clear() noexcept;
				
				/**
				 * Gets the binary value representing the properties.
				 * 
				 * @return the binary value
				 */
				std::uint8_t getValue() const noexcept;
				
				/**
				 * Sets the binary value representing the properties.
				 * 
				 * @param value the binary value
				 */
				void setValue(std::uint8_t value) noexcept;
				
				/**
				 * Prints a text description of enabled properties to the given text buffer.
				 * If multiple properties are enabled, then they will be separated by commas and spaces.
				 *
				 * @param buffer The buffer to print to
				 * @param length The maximum text buffer length
				 * @return the actual size that would be needed to print all of the properties
				 */
				std::size_t print(char *buffer, std::size_t length) const noexcept;
				
				/**
				 * Prints a text description of enabled properties returned as a string.
				 * If multiple properties are enabled, then they will be separated by commas and spaces.
				 *
				 * @return the printed properties
				 */
				std::string print() const;
				
			private:
				std::uint8_t mBits;
		};
		
		/**
		 * Prints a text description of enabled properties to a C++ stream.
		 * If multiple properties are enabled, then they will be separated by commas and spaces.
		 *
		 * @param stream The stream
		 * @param value The properties
		 * @return the stream after printing
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, const Properties &value);
	}
}

#endif

