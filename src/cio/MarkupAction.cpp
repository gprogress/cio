/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "MarkupAction.h"

#include "Print.h"

namespace cio
{
	const char *sMarkupActionText[] = {
		"None",
		"Start",
		"End",
		"Full",
		"Continue"
	};

	const char *print(MarkupAction type) noexcept
	{
		const char *text = "Unknown";
		std::size_t idx = static_cast<std::size_t>(type);
		if (idx < sizeof(sMarkupActionText) / sizeof(const char *))
		{
			text = sMarkupActionText[idx];
;		}
		return text;
	}

	std::size_t print(MarkupAction type, char *buffer, std::size_t capacity) noexcept
	{
		const char *text = print(type);
		std::size_t needed = std::strlen(text);
		reprint(text, needed, buffer, capacity);
		return needed;
	}

	std::ostream &operator<<(std::ostream &stream, MarkupAction type)
	{
		return stream << print(type);
	}
}
