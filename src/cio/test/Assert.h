/************************************************************************
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_ASSERT_H
#define CIO_TEST_ASSERT_H

#include "Types.h"

#include "Fault.h"

#include <cio/Severity.h>

#include <cstring>
#include <sstream>

namespace cio
{
	namespace test
	{
		/**
		 * Asserts that the given value is considered true when cast to a Boolean,
		 * constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 *
		 * @tparam <T> The type of expression being evaluated
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 * @param predicate The predicate to assess in Boolean context
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the predicate is false
		 */
		template <typename T, typename... A>
		inline void assertTrue(T &&predicate, A &&...args);
		
		/**
		 * Asserts that the given value is considered true when cast to a Boolean,
		 * constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 * An additional message is used to construct the Fault for information.
		 *
		 * @tparam <T> The type of expression being evaluated
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 * @param predicate The predicate to assess in Boolean context
		 * @param message The message to pass to the Fault if thrown
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the predicate is false
		 */
		template <typename T, typename... A>
		inline void assertTrueWithMessage(T &&predicate, const std::string &message, A &&...args);

		/**
		 * Asserts that two values are equal using operator==, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 *
		 * @tparam <T> The type of the first object
		 * @tparam <U> The type of the second object
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param expected The first object representing the expected value
		 * @param actual The second object representing the actual value
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename T, typename U, typename... A>
		inline void assertEqual(T &&expected, U &&actual, A &&...args);

		/**
		 * Asserts that two values are equal using operator==, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 *
		 * @tparam <T> The type of the first object
		 * @tparam <U> The type of the second object
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param expected The first object representing the expected value
		 * @param actual The second object representing the actual value
		 * @param caseString The case string to print if Fault is thrown
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename T, typename U, typename... A>
		inline void assertEqualForCase(T &&expected, U &&actual, const std::string &caseString, A &&...args);

		/**
		 * Asserts that two values are not equal using operator==, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 *
		 * @tparam <T> The type of the first object
		 * @tparam <U> The type of the second object
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param unexpected The first object representing the unexpected value to reject
		 * @param actual The second object representing the actual value
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename T, typename U, typename... A>
		inline void assertDifferent(T &&unexpected, U &&actual, A &&...args);

		/**
		 * Asserts that two null-terminated C string are equal text, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 * This overload handles assertEqual in a more intuitive way if both arguments are C-style strings.
		 *
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param expected The first object representing the expected value
		 * @param actual The second object representing the actual value
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename... A>
		inline void assertEqual(const char *expected, const char *actual, A &&...args);
		
		/**
		 * Asserts that two null-terminated C string are equal text, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 * This overload handles assertEqual in a more intuitive way if both arguments are C-style strings.
		 *
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param expected The first object representing the expected value
		 * @param actual The second object representing the actual value
		 * @param caseString The case string to print if Fault is thrown
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename... A>
		inline void assertEqualForCase(const char *expected, const char *actual, const std::string &caseString, A &&...args);

		/**
		 * Asserts that two values are equal using operator==, constructing and throwing a cio::test::Fault with the given arguments otherwise.
		 * An additional message is passed to the Fault if it is thrown.
		 *
		 * @tparam <T> The type of the first object
		 * @tparam <U> The type of the second object
		 * @tparam <A> The type of arguments to pass to a constructed Fault
		 *
		 * @param expected The first object representing the expected value
		 * @param actual The second object representing the actual value
		 * @param message The message to pass to the Fault if thrown
		 * @param args The arguments to pass to the Fault if thrown
		 * @throw cio::test::Fault If the expected value is not equal to the actual value
		 */
		template <typename T, typename U, typename... A>
		inline void assertEqualWithMessage(T &&expected, U &&actual, const char *message, A &&...args);
		
		/**
		 * Asserts that two byte sequences are identical. This verifies that they have the same length and the same sequence of bytes.
		 * If not, a cio::test::Fault is thrown indicating the failure with an explanatory message.
		 *
		 * @param expected The pointer to the first "expected" byte sequence
		 * @param expectedLength The number of bytes in the first "expected" byte sequence
		 * @param actual The pointer to the second "actual" byte sequence
		 * @param actualLength The number of bytes in the second "actual" byte sequence
		 * @param source The source file and line information for the point of call of this assertion
		 * @throw cio::test::Fault If the expected byte sequence length or content is not equal to the actual byte sequence length and content
		 */
		CIO_TEST_API void assertBytesEqual(const void *expected, std::size_t expectedLength, const void *actual, std::size_t actualLength, const Source &source);
		
		/**
		 * Asserts that two metadata instances are equal for all fields that can be known at compile time.
		 * This validates that the location, modes, type, and logical size are equal. Physical size is platform and runtime dependent (normally) so that is not checked.
		 * This is used for a more detailed check than the normal operator== which only checks location path.
		 *
		 * @param expected The expected metadata
		 * @param actual The actual metadata
		 * @param source The source file and line information for the point of call of this assertion
		 * @throw cio::test::Fault If the expected metadata does not equal the actual metadata
		 */
		CIO_TEST_API void assertMetadataEqual(const Metadata &expected, const Metadata &actual, const Source &source);
	}
}

#ifndef CIO_ASSERT
/**
 * The CIO_ASSERT macro is used to make a Boolean assertion interpreting a predicate as either true or false.
 * The macro calls the corresponding cio::test::assertTrueWithMessage while capturing the source file and line context of the point of assertion.
 * 
 * @param PREDICATE The C++ expression to interpret as a Boolean predicate
 * @throw cio::test::Fault If the predicate is false
 */
#define CIO_ASSERT(PREDICATE) cio::test::assertTrueWithMessage((PREDICATE), "Assertion failed: " #PREDICATE, CIO_CURRENT_SOURCE(), cio::Severity::Critical)
#endif

#ifndef CIO_ASSERT_FOR_CASE
/**
 * The CIO_ASSERT macro is used to make a Boolean assertion interpreting a predicate as either true or false.
 * The macro calls the corresponding cio::test::assertTrueWithMessage while capturing the source file and line context of the point of assertion.
 *
 * @param PREDICATE The C++ expression to interpret as a Boolean predicate
 * @param CASE The case string to print if Fault is thrown
 * @throw cio::test::Fault If the predicate is false
 */
#define CIO_ASSERT_FOR_CASE(PREDICATE, CASE) cio::test::assertTrueWithMessage((PREDICATE), std::string("Assertion failed: " #PREDICATE " For case: ") + CASE, CIO_CURRENT_SOURCE(), cio::Severity::Critical)
#endif

#ifndef CIO_ASSERT_EQUAL
/**
 * The CIO_ASSERT_EQUAL macro is used to make an equality assertion assuming two values are equal.
 * The macro calls the corresponding cio::test::assertEqual while capturing the source file and line context of the point of assertion.
 * 
 * @param EXPECTED The first "expected" value
 * @param ACTUAL The second "actual" value
 * @throw cio::test::Fault If the expected value is not equal to the actual value
 */
#define CIO_ASSERT_EQUAL(EXPECTED, ACTUAL) cio::test::assertEqual((EXPECTED), (ACTUAL), CIO_CURRENT_SOURCE(), cio::Severity::Critical)
#endif

#ifndef CIO_ASSERT_EQUAL_FOR_CASE
/**
 * The CIO_ASSERT_EQUAL macro is used to make an equality assertion assuming two values are equal.
 * The macro calls the corresponding cio::test::assertEqual while capturing the source file and line context of the point of assertion.
 *
 * @param EXPECTED The first "expected" value
 * @param ACTUAL The second "actual" value
 * @param CASE The case string to print if Fault is thrown
 * @throw cio::test::Fault If the expected value is not equal to the actual value
 */
#define CIO_ASSERT_EQUAL_FOR_CASE(EXPECTED, ACTUAL, CASE) cio::test::assertEqualForCase((EXPECTED), (ACTUAL), CASE, CIO_CURRENT_SOURCE(), cio::Severity::Critical)
#endif

#ifndef CIO_ASSERT_DIFFERENT
 /**
  * The CIO_ASSERT_DIFFERENT macro is used to make an equality assertion assuming two values are not equal.
  * The macro calls the corresponding cio::test::assertDifferent while capturing the source file and line context of the point of assertion.
  *
  * @param UNEXPECTED The first "unexpected" value
  * @param ACTUAL The second "actual" value
  * @throw cio::test::Fault If the value if equal to the unexpected value
  */
#define CIO_ASSERT_DIFFERENT(UNEXPECTED, ACTUAL) cio::test::assertDifferent((UNEXPECTED), (ACTUAL), CIO_CURRENT_SOURCE(), cio::Severity::Critical)
#endif

#ifndef CIO_ASSERT_THROW
/**
 * The CIO_ASSERT_THROW macro is used to validate that an expression throws a specific exception type.
 * It executes the expression inside of a try/catch and validates that the expected exception was thrown.
 * If no exception or a different exception type is thrown, a cio::test::Fault is thrown to indicate the failure while
 * capturing the source file and line context of the point of assertion.
 * 
 * @param EXPR The expression to execute
 * @param EX The expected exception type to be thrown by the expression
 * @throw cio::test::Fault If no exception or an unexpected exception type was thrown
 */
#define CIO_ASSERT_THROW(EXPR, EX) { bool thrown = false; \
	try { \
		EXPR; \
	} \
	catch (const EX &) { thrown = true; }  \
	catch (...) { \
		cio::test::Fault("Throw assertion failed: expected " #EX " but a different exception was thrown", CIO_CURRENT_SOURCE(), cio::Severity::Critical); \
	} \
	if (!thrown) throw cio::test::Fault("Throw assertion failed: expected " #EX " but none thrown", CIO_CURRENT_SOURCE(), cio::Severity::Critical); }

#endif

#ifndef CIO_ASSERT_BYTES_EQUAL
/**
 * The CIO_ASSERT_BYTES_EQUAL validates that two byte sequences are identical. This verifies that they have the same length and the same sequence of bytes.
 * If not, a cio::test::Fault is thrown indicating the failure with an explanatory message while capturing the source file and line context of the point of assertion.
 *
 * @param EXPECTED_BYTES The pointer to the first "expected" byte sequence
 * @param EXPECTED_LENGTH The number of bytes in the first "expected" byte sequence
 * @param ACTUAL_BYTES The pointer to the second "actual" byte sequence
 * @param ACTUAL_LENGTH The number of bytes in the second "actual" byte sequence
 * @throw cio::test::Fault If the expected byte sequence length or content is not equal to the actual byte sequence length and content
 */
#define CIO_ASSERT_BYTES_EQUAL(EXPECTED_BYTES, EXPECTED_LENGTH, ACTUAL_BYTES, ACTUAL_LENGTH) cio::test::assertBytesEqual(EXPECTED_BYTES, EXPECTED_LENGTH, ACTUAL_BYTES, ACTUAL_LENGTH, CIO_CURRENT_SOURCE())
#endif

#ifndef CIO_ASSERT_METADATA_EQUAL
/**
 * The CIO_ASSERT_METADATA_EQUAL that two metadata instances are equal for all fields that can be known at compile time.
 * This validates that the location, modes, type, and logical size are equal. Physical size is platform and runtime dependent (normally) so that is not checked.
 * This is used for a more detailed check than the normal operator== which only checks location path.
 *
 * @param EXPECTED The expected metadata
 * @param ACTUAL The actual metadata
 * @throw cio::test::Fault If the expected metadata does not equal the actual metadata
 */
#define CIO_ASSERT_METADATA_EQUAL(EXPECTED, ACTUAL) cio::test::assertMetadataEqual((EXPECTED), (ACTUAL), CIO_CURRENT_SOURCE())
#endif

/* Inline implementation */

namespace cio
{
	namespace test
	{
		template <typename T, typename... A>
		inline void assertTrue(T &&predicate, A &&...args)
		{
			if (!predicate)
			{
				throw Fault("Assertion failed", std::forward<A>(args)...);
			}
		}

		template <typename T, typename... A>
		inline void assertTrueWithMessage(T &&predicate, const std::string &message, A &&...args)
		{
			if (!predicate)
			{
				throw Fault(message, std::forward<A>(args)...);
			}
		}

		template <typename T, typename U, typename... A>
		inline void assertEqual(T &&expected, U &&actual, A &&...args)
		{
			if (!(expected == actual))
			{
				std::ostringstream message;
				message << "Equality Assertion failed: expected " << expected << ", actual " << actual;
				throw Fault(message.str(), std::forward<A>(args)...);
			}
		}

		template <typename T, typename U, typename... A>
		inline void assertEqualForCase(T &&expected, U &&actual, const std::string &caseString, A &&...args)
		{
			if (!(expected == actual))
			{
				std::ostringstream message;
				message << "Equality Assertion failed: expected " << expected << ", actual " << actual << " For case: " << caseString;
				throw Fault(message.str(), std::forward<A>(args)...);
			}
		}

		template <typename T, typename U, typename... A>
		inline void assertDifferent(T &&unexpected, U &&actual, A &&...args)
		{
			if (unexpected == actual)
			{
				std::ostringstream message;
				message << "Assertion failed: actual " << actual << " matched rejected value " << unexpected;
				throw Fault(message.str(), std::forward<A>(args)...);
			}
		}

		template <typename... A>
		inline void assertEqual(const char *expected, const char *actual, A &&...args)
		{
			if (std::strcmp(expected, actual) != 0)
			{
				std::ostringstream message;
				message << "Equality Assertion failed: expected " << expected << ", actual " << actual;
				throw Fault(message.str(), std::forward<A>(args)...);
			}
		}

		template <typename... A>
		inline void assertEqualForCase(const char *expected, const char *actual, const std::string &caseString, A &&...args)
		{
			if (std::strcmp(expected, actual) != 0)
			{
				std::ostringstream message;
				message << "Equality Assertion failed: expected " << expected << ", actual " << actual << " For case: " << caseString;
				throw Fault(message.str(), std::forward<A>(args)...);
			}
		}

		template <typename T, typename U, typename... A>
		inline void assertEqualWithMessage(T &&expected, U &&actual, const char *message, A &&...args)
		{
			if (!(expected == actual))
			{
				throw Fault(message, std::forward<A>(args)...);
			}
		}
	}
}

#endif
