/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_STREAMSERVER_H
#define CIO_NET_STREAMSERVER_H

#include "Types.h"

#include "Socket.h"
#include "StreamChannel.h"

namespace cio
{
	namespace net
	{
		/**
		 * The Stream Server provides a listener for incoming stream-based network connections such as for TCP/IP
		 * and then hands off Stream Channels for each connection once received.
		 */
		class CIO_NET_API StreamServer
		{
			public:
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for the class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct a server not bound to any socket.
				 */
				StreamServer() noexcept;

				/**
				 * Construct a server using the given socket.
				 *
				 * @param in The socket to use
				 */
				StreamServer(Socket &&in) noexcept;

				/**
				 * Construct a server by adopting the socket from the given server.
				 *
				 * @param in The server to move
				 */
				StreamServer(StreamServer &&in) noexcept;

				/**
				 * Transfer a server by adopting the socket from the given server.
				 *
				 * @param in The server to move
				 * @return this server
				 */
				StreamServer &operator=(StreamServer &&in) noexcept;

				/**
				 * Destructor. Closes the socket if open.
				 */
				virtual ~StreamServer() noexcept;

				/**
				 * Gets the metaclass for this runtime object.
				 *
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept;

				/**
				 * Closes the socket and clears the server state.
				 */
				virtual void clear() noexcept;

				/**
				 * Waits until a new stream connection is available, and then returns it.
				 *
				 * @return the next available stream connection
				 * @throw cio::Exception If a network or socket failure prevented receiving connections
				 */
				StreamChannel waitForConnection();

				/**
				 * Binds this Stream Server to listen on the interface(s) that best match the given URL.
				 *
				 * The protocol (if specified) is used to determine the port based on the OS service database.
				 * The hostname can be the address of an interface or a remote address.
				 * If the hostname is a local interface, that interface will be used.
				 * If the hostname is a remote host, the interface that is the default route to that host will be used.
				 *
				 * @param iface The URI describing the interface to listen to
				 * @return the State of the bind request
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToInterface(const cio::Path &iface);

				/**
				 * Binds this Stream Server to listen on the interface(s) that best match the given service.
				 *
				 * @param iface The service describing the interface to listen to
				 * @return the State of the bind request
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToInterface(const Service &service);

				/**
				 * Binds this Stream Server to listen on all local TCP/IP addresses and interfaces on the given port.
				 *
				 * @param port the port to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToAllInterfaces(std::int16_t port);

				/**
				 * Binds this Stream Server to listen on the given local address and port.
				 *
				 * @param address The address to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToInterface(const Address &address);

				/**
				 * Binds this Stream Server to listen on the given local IPv4 address and port.
				 *
				 * @param ip The IP address to listen to
				 * @param port The port to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToInterface(const IPv4 &ip, std::int16_t port);

				/**
				 * Binds this Stream Server to listen on the given local IPv6 address and port.
				 *
				 * @param ip The IP address to listen to
				 * @param port The port to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket setup, binding, or listening failed
				 */
				State listenToInterface(const IPv6 &ip, std::int16_t port);

				/**
				 * Adopts the given socket to use for this server.
				 * The existing socket, if any, is closed.
				 *
				 * @param socket The socket to adopt
				 */
				void adopt(Socket &&socket) noexcept;

				/**
				 * Releases the current socket in use for this server to the caller.
				 * The server will no longer reference the given socket
				 *
				 * @return the released socket
				 */
				Socket take() noexcept;

				/**
				 * Gets access to the underlying socket used by this server.
				 *
				 * @return the socket
				 */
				Socket &get() noexcept;

			private:
				/** The underlying socket for binding and listening */
				Socket mSocket;
		};
	}
}

#endif
