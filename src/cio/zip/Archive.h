/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_ARCHIVE_H
#define CIO_ZIP_ARCHIVE_H

#include "Types.h"

#include <cio/Buffer.h>
#include <cio/Protocol.h>
#include <cio/RelativeInput.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace zip
	{
		/**
		 * The CIO Zip Archive implements representing a Zip or Zip64 archive file as a directory of its contents.
		 * By nature of the Zip format, the input used for the zip file must support seeking.
		 */
		class CIO_ZIP_ABI Archive : public cio::Protocol
		{
			public:			
				/** 4-byte signature for "End of Central Directory" marker. */
				static const std::uint32_t EOCD_SIGNATURE;

				/** 4-byte signature for "End of Central Directory 64" marker. */
				static const std::uint32_t EOCD64_SIGNATURE;

				/** 4-byte signature for "End of Central Directory 64 Locator" marker. */
				static const std::uint32_t EOCD64_LOC_SIGNATURE;

				/** 4-byte signature for a "Central Directory File Header" marker. */
				static const std::uint32_t CDENTRY_SIGNATURE;
				
				/** 4-byte signature for a local file record marker */
				static const std::uint32_t LOCAL_FILE_SIGNATURE;

				/** 2-byte signature for "Zip64 Extra Data" */
				static const std::uint16_t ZIP64_EXTRA_SIGNATURE;

				/**
				 * Gets the metaclass for the Input class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Decodes a Zip Central File Directory record from an input into a buffer.
				 * This is mainly used to implement next() and construction, but can be used directly.
				 * The buffer will contain all bytes from the entry, including the variable length text
				 * and any Zip64 extra fields.
				 *
				 * @note The buffer size may be increased if necessary to hold the full record.
				 *
				 * @param input The input positioned at the central directory record start
				 * @param entry The buffer to receive the entry bytes
				 * @throw cio::Exception If reading the input failed
				 * @throw std::bad_alloc If the buffer needed to be resized but memory allocation failed
				 */
				static void readZipEntry(Input &input, Buffer &entry);

				/**
				 * Decodes the internal file or directory metadata from a buffer representing a Zip Central File Directory record.
				 * The buffer is not modified as only getLittleFromPositon or temporary view-only sub-buffers are used.
				 * 
				 * @param entry The buffer representing the central file directory record
				 * @return the metadata for the represented file
				 */
				static Metadata decodeEntryMetadata(const Buffer &entry);

				/**
				 * Checks whether the given central directory record matches the given relative filename.
				 * 
				 * @param entry The buffer representing the central file directory record
				 * @param filename The filename of interest
				 */
				static bool isMatchingEntry(const Buffer &entry, const Text &filename);

				/**
				 * Default constructor.
				 */
				Archive() noexcept;
				
				/**
				 * Constructs a zip archive using the given URI and default protocol factory.
				 * 
				 * @param path The file to open
				 */
				explicit Archive(const Path &path);

				/**
				 * Copy constructor.
				 * Both archives will share the same input and will start at the same entry,
				 * but can seek independently.
				 *
				 * @param in The archive to copy
				 */
				Archive(const Archive &in);
				
				/**
				 * Move constructor.
				 *
				 * @param in The archive to move
				 */
				Archive(Archive &&in) noexcept;
				
				/**
				 * Copy assignment.
				 * Both archives will share the same input and will start at the same entry,
				 * but can seek independently.
				 *
				 * @param in The archive to copy
				 * @return this archive
				 */
				Archive &operator=(const Archive &in);
				
				/**
				 * Move assignment.
				 *
				 * @param in The archive to move
				 * @return this archive
				 */
				Archive &operator=(Archive &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Archive() noexcept override;

				/**
				 * Gets the metaclass for the runtime type of this object.
				 *
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the current zip input.
				 */
				virtual void clear() noexcept override;
				
				/**
				 * Gets the number of directory entries in the zip central directory.
				 * This is cached so it is returned immediately.
				 *
				 * @return the current number of directory entries
				 */
				virtual Length size() const noexcept override;
				
				/**
				 * Gets the nmaximum umber of directory entries in the zip central directory.
				 * For Zip64, this is essentially unbounded so this method returns CIO_UNKNOWN_LENGTH.
				 *
				 * @return the maximum number of directory entries
				 */
				virtual Length capacity() const noexcept override;
				
				/**
				 * Opens the Archive. This uses the factory to open the underlying file, then set up this class. 
				 * 
				 * This will scan for the EOD record, then reposition to get the first directory entry.
				 *
				 * @param resource The resource to open
				 * @param modes The open modes
				 * @param factory The protocol factory to use
				 * @return the actual resource modes that the resource could be opened with
				 * @throw Exception If the directory could not be opened for any reason
				 */
				virtual ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;

				/**
				 * Gets the metadata about the given resource.
				 * The metadata includes the modes and permissions, overall category, and content length.
				 * It also includes the canonical URI for the resource, following any links or redirects.
				 * If the resource does not actually exist, the returned metadata will reflect that.
				 *
				 * @param input the URI to the resource
				 * @return the metadata about the resource
				 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
				 */
				virtual Metadata readMetadata(const Path &input) const override;

				/**
				 * Gets the current raw filename entry as reported by the underlying transport layer.
				 * The entry is not necessarily null-terminated and may only be valid until the next call to next() or clear().
				 * 
				 * The base implementation acts as a permanently empty directory and will always return the empty string.
				 *
				 * @return the current filename entry
				 */
				virtual Text filename() const override;

				/**
				 * Gets the metadata of the current entry as reported by the Zip Central File Directory data.
				 * This includes a copy of the filename entry plus the logical (uncompressed) and physical (compressed) size.
				 *
				 * @return the metadata of the current entry
				 * @throw std::bad_alloc If the memory for the metadata could not be allocated
				 */
				virtual Metadata current() const override;

				/**
				 * Advances to the next directory entry if possible.
				 * The returned Status will either be Success if there is another entry or Underflow if no more entries exist.
				 * If a transport error or other failure to read the directory itself occurs, an Exception is thrown with the information
				 * about the error.
				 * 
				 * The base implementation acts as a permanently empty directory and will always return Status::Underflow.
				 *
				 * @return Success if another entry was read, Underflow if no more entries exist
				 * @throw Exception If a transport error occurred that prevented reading the directory
				 */
				virtual State next() override;

				/**
				 * Seeks back to the first directory entry.
				 * This returns the central directory to its first entry.
				 *
				 * @return the oucome of rewinding to the first entry, which is either successful or fails with Reason::Underflow if no entries are present
				 * @throw Exception If a transport error occurred that prevented reading the directory
				 */
				virtual State rewind() override;

				/**
				 * Seeks the directory to the given entry described as a relative path.
				 *
				 * This directory scans the central directory until the record is found.
				 *
				 * @param relative The relative path to find
				 * @return the outcome of this operation, which is either successful or fails with Reason::Missing if the entry is not present
				 * @throw Exception If seeking failed for any reason other than the entry not existing
				 * */
				virtual State seekToEntry(const Path &relative) override;

				/**
				 * Create an Input subclass instantiation that can read the content for the current directory entry.
				 *
				 * This class will follow the current record to read the local file record, and then set up a Relative Input to read the actual
				 * data. If the entry is compressed using Deflate, the returned Input will be a cio::zip::Inflator to transparently decompress it.
				 *
				 * @param path The path to the resource to read
				 * @param modes The modes to open the input
				 * @return the Input to use to read
				 * @throw cio::Exception If transport problems or lack of existence of the resource prevented creating an Input
				 * @throw std::bad_alloc If allocating the new Input failed
				 */
				virtual std::unique_ptr<Input> openCurrentInput(ModeSet modes = ModeSet::readonly()) const override;

				/**
				 * Create an Input subclass instantiation that can read the content for the resource at the given path.
				 * For Directory instances, this should be able to handle relative paths to the directory itself.
				 *
				 * This class will scan the zip central directory to find the right record for this filename if it can, and if so
				 * set up the input the same way as openCurrentInput().
				 * 
				 * @param path The path to the resource to read
				 * @param modes The modes to open the input
				 * @return the Input to use to read
				 * @throw cio::Exception If transport problems or lack of existence of the resource prevented creating an Input
				 * @throw std::bad_alloc If allocating the new Input failed
				 */
				virtual std::unique_ptr<Input> openInput(const cio::Path &path, ModeSet modes = ModeSet::readonly()) const override;

				/**
				 * Checks whether the directory is currently open and has a directory entry to process.
				 * 
				 * @return whether the directory has an entry available
				 */
				virtual bool traversable() const noexcept override;
				
				// Zip specific methods
				
				/**
				 * Gets direct access to the underlying Input providing the zip file content.
				 *
				 * @return the underlying zip input
				 */
				std::shared_ptr<Input> getZipInput() noexcept;
				
				/**
				 * Gets direct access to the underlying Input providing the zip file content.
				 *
				 * @return the underlying zip input
				 */
				std::shared_ptr<const Input> getZipInput() const noexcept;
				
				/**
				 * Opens the given Input as the underlying zip file.
				 * This will clear the current state, then set the given input and scan to seek to the first input.
				 *
				 * @param input The input to set
				 */
				void openZipInput(std::shared_ptr<Input> input);

				/**
				 * Gets an input view of the central directory as a whole.
				 * This view is independent of the current central directory view used for directory iteration,
				 * but still tied to the same underlying zip file input.
				 *
				 * @return a view of the central directory
				 */
				RelativeInput getCentralDirectory() const;
				
				/**
				 * Gets an input view of the local file record pointed to by the current central directory entry.
				 * Note that this file record will include the header information as well as the full zipped file content.
				 * 
				 * @return a view of the current file record
				 */
				RelativeInput getCurrentFileRecord() const;
				
				/**
				 * Gets an input view of the local file record pointed to by the given central directory entry.
				 * Note that this file record will include the header information as well as the full zipped file content.
				 * 
				 * @param entry The buffer containing the intended central directory entry
				 * @return a view of that file's record
				 */
				RelativeInput getFileRecord(const Buffer &entry) const;
				
				/**
				 * Finds a given relative path in the central directory, then uses it to create an input view of the local file record.
		 		 * Note that this file record will include the header information as well as the full zipped file content.
		 		 *
		 		 * @param path The relative path to find
		 		 * @return a view of that file's record
		 		 */
				RelativeInput findFileRecord(const Path &path) const;
				
				/**
				 * Gets an input representing the actual zipped file contents given a local file record input.
				 * This method will decode the local file header and then decide how to set up the decompressor based on
				 * the compression method. The returned input will still eventually point back to the current zip file input.
				 *
				 * @param record the local file record
				 * @param modes The open modes for the input
				 * @return the input to decompress the file content
				 */
				std::unique_ptr<Input> openFileRecordInput(RelativeInput record, ModeSet modes = ModeSet::readonly()) const;

				/**
				 * Reads the metadata for a file entry in the central directory.
				 * 
				 * @param relativePath The entry relative path
				 * @return The metadata
				 */
				Metadata readRelativeMetadata(const cio::Path &relativePath) const;

				/**
				 * Reads the metadata for a file entry central directory record.
				 *
				 * @param entry The buffer containing the record bytes
				 * @return The metadata
				 */
				Metadata readRecordMetadata(const Buffer &entry) const;

			private:
				/** The metaclass for this class */
				static Class<Archive> sMetaclass;
				
				/** 
				 * Scans to find the central directory record.
				 * 
				 * @throw cio::Exception If the central directory could not be read or found
				 */
				void findCentralDirectory();

				/**
				 * Sets up this archive from an EOCD64 record.
				 * 
				 * @param record The buffer containing the EOCD64 record
				 */
				void setupFromEocd64(Buffer &record);
				
				/** The underlying input providing zip file content */
				std::shared_ptr<Input> mZipInput;

				/** The relative input describing the central directory */
				RelativeInput mCentralDirectory;

				/** State and progress of entry iteration */
				cio::Progress<std::uint64_t> mEntryProgress;

				/** The current directory entry */
				Buffer mCurrentEntry;

				/** Whether this file is a Zip64 file */
				bool mZip64;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
