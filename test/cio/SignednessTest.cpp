/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "SignednessTest.h"

#include <cio/test/Assert.h>

#include <cio/Signedness.h>

#include <sstream>

namespace cio
{
	SignednessTest::SignednessTest() :
		cio::test::Suite("cio/Signedness")
	{
		this->addTestCase("signedness<T>()", &SignednessTest::testSignedness);
		this->addTestCase("print(Signedness)", &SignednessTest::testPrint);
		this->addTestCase("operator<<(std::ostream &, Signedness)", &SignednessTest::testPrintOperator);		
	}

	SignednessTest::~SignednessTest() noexcept = default;
	
	void SignednessTest::testSignedness()
	{
		CIO_ASSERT_EQUAL(Signedness::None, signedness<void>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<bool>());
		CIO_ASSERT_EQUAL(std::is_signed<char>::value ? Signedness::Negation : Signedness::None, signedness<char>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<unsigned char>());				
		CIO_ASSERT_EQUAL(Signedness::Negation, signedness<signed char>());
		CIO_ASSERT_EQUAL(std::is_signed<wchar_t>::value ? Signedness::Negation : Signedness::None, signedness<wchar_t>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<unsigned short>());				
		CIO_ASSERT_EQUAL(Signedness::Negation, signedness<short>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<unsigned>());				
		CIO_ASSERT_EQUAL(Signedness::Negation, signedness<int>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<unsigned long>());				
		CIO_ASSERT_EQUAL(Signedness::Negation, signedness<long>());
		CIO_ASSERT_EQUAL(Signedness::None, signedness<unsigned long long>());				
		CIO_ASSERT_EQUAL(Signedness::Negation, signedness<long long>());

		CIO_ASSERT_EQUAL(Signedness::Prefix, signedness<float>());				
		CIO_ASSERT_EQUAL(Signedness::Prefix, signedness<double>());
		CIO_ASSERT_EQUAL(Signedness::Prefix, signedness<long double>());
	}

	void SignednessTest::testPrint()
	{
		CIO_ASSERT_EQUAL("None", print(Signedness::None));
		CIO_ASSERT_EQUAL("Bitwise", print(Signedness::Bitwise));
		CIO_ASSERT_EQUAL("Negation", print(Signedness::Negation));
		CIO_ASSERT_EQUAL("Prefix", print(Signedness::Prefix));
		CIO_ASSERT_EQUAL("Unknown", print(static_cast<Signedness>(42)));	
	}
			
	void SignednessTest::testPrintOperator()
	{
		// None
		{
			std::ostringstream s;
			s << Signedness::None;
			CIO_ASSERT_EQUAL(print(Signedness::None), s.str());
		}
		
		// Bitwise
		{
			std::ostringstream s;
			s << Signedness::Bitwise;
			CIO_ASSERT_EQUAL(print(Signedness::Bitwise), s.str());
		}
		
		// Negation
		{
			std::ostringstream s;
			s << Signedness::Negation;
			CIO_ASSERT_EQUAL(print(Signedness::Negation), s.str());
		}
		
		// Prefix
		{
			std::ostringstream s;
			s << Signedness::Prefix;
			CIO_ASSERT_EQUAL(print(Signedness::Prefix), s.str());
		}
		
		// Invalid value
		{	
			std::ostringstream s;
			s << static_cast<Signedness>(42);
			CIO_ASSERT_EQUAL(print(static_cast<Signedness>(42)), s.str());
		}
	}
}

