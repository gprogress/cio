/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ATTError.h"

#include <cio/Reason.h>

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sAttErrorText[] = {
			"None",
			"Invalid Handle",
			"Unreadable",
			"Unwritable",
			"Invalid Packet",
			"Unauthenticated",
			"Unsupported Request",
			"Invalid Offset",
			"Unauthorized",
			"Prepare Write Queue Full",
			"Missing Attribute",
			"Attribute Isn't Blob",
			"Insufficient Encryption Key",
			"Invalid Value Length",
			"Unlikely",
			"Insufficient Security Level",
			"Unsupported Group Type",
			"Insufficient Resources"
		};
	
		const char *print(ATTError code) noexcept
		{
			const char *text = "Unknown";
			if (code <= ATTError::InsufficientResources)
			{
				text = sAttErrorText[static_cast<std::size_t>(code)];
			}
			return text;
		}
		
		std::ostream &operator<<(std::ostream &s, ATTError code)
		{
			return s << print(code);
		}

		Reason status(ATTError code) noexcept
		{
			Reason status;
		
			switch (code)
			{
				case ATTError::None:
					status = Reason::None;
					break;
				
				case ATTError::InvalidHandle:
					status = Reason::Missing;
					break;
				
				case ATTError::Unreadable:
					status = Reason::Unreadable;
					break;
				
				case ATTError::Unwritable:
					status = Reason::Unwritable;
					break;
				
				case ATTError::InvalidPacket:
					status = Reason::Unparsable;
					break;
				
				case ATTError::Unauthenticated:
					status = Reason::Uncredentialed;
					break;
				
				case ATTError::UnsupportedRequest:
					status = Reason::Unsupported;
					break;
				
				case ATTError::InvalidOffset:
					status = Reason::Overflow;
					break;
				
				case ATTError::Unauthorized:
					status = Reason::Unauthorized;
					break;
				
				case ATTError::PrepQueueFull:
					status = Reason::Exhausted;
					break;
				
				case ATTError::MissingAttribute:
					status = Reason::Missing;
					break;
					
				case ATTError::AttributeNotBlob:
					status = Reason::Unseekable;
					break;
					
				case ATTError::WeakEncryptionKey:
					status = Reason::Insecure;
					break;
				
				case ATTError::InvalidValueLength:
					status = Reason::Misaligned;
					break;
				
				case ATTError::Unlikely:
					status = Reason::Unknown;
					break;
				
				case ATTError::WeakEncryption:
					status = Reason::Insecure;
					break;
				
				case ATTError::UnsupportedGroupType:
					status = Reason::Invalid;
					break;
				
				case ATTError::InsufficientResources:
					status = Reason::Exhausted;
					break;
				
				default:
					status = Reason::Unknown;
					break;				
			}
			
			return status;
		}
		
	}
}

