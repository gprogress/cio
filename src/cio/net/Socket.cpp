/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Socket.h"

#include "Service.h"

#include <cio/Logger.h>
#include <cio/Class.h>

#if defined CIO_USE_UNIX_SOCKETS

#if defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#endif

#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#include <sys/types.h>
#endif

#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

/** Integer type for actual socket handles */
using cio_socket_t = int;

/** Integer type of buffer length parameters for socket calls */
using cio_socket_size_t = size_t;

/** Casts SocketId to native socket integer */
#define CIO_TO_SOCKET(X) static_cast<int>(X)

/** Casts native socket integer to SocketId */
#define CIO_FROM_SOCKET(X) static_cast<std::uintptr_t>(X)

/** Constant for invalid SocketId */
#define CIO_INVALID_SOCKET static_cast<std::uintptr_t>(-1)

/** Closes a socket, has different names on different platforms */
#define CIO_CLOSE_SOCKET(X) ::close(X)

#define CIO_MAX_SOCKET_BACKLOG SOMAXCONN

#endif

#if defined CIO_USE_WINSOCK2

#ifndef NOMINMAX
#define NOMINMAX
#endif

#if defined CIO_HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif

#if defined CIO_HAVE_WS2TCPIP_H
#include <WS2tcpip.h>
#endif

#include <atomic>
#include <mutex>

/** Integer type for actual socket handles */
using cio_socket_t = SOCKET;

/** Integer type of buffer length parameters for socket calls */
using cio_socket_size_t = int;

/** Casts SocketId to native socket integer */
#define CIO_TO_SOCKET(X) static_cast<SOCKET>(X)

/** Casts native socket integer to SocketId */
#define CIO_FROM_SOCKET(X) static_cast<std::uintptr_t>(X)

/** Constant for invalid SocketId */
#define CIO_INVALID_SOCKET INVALID_SOCKET

/** Closes a socket, has different names on different platforms */
#define CIO_CLOSE_SOCKET(X) ::closesocket(X)

#define CIO_MAX_SOCKET_BACKLOG SOMAXCONN

#endif

#include <algorithm>
#include <cerrno>

namespace cio
{
	namespace net
	{
		Logger Socket::sLogger("cio::net::Socket");
	
#if defined CIO_USE_WINSOCK2
		State Socket::initialize()
		{
			State e(Action::Prepare);
			
			WORD wVersionRequested;
			WSADATA wsaData;
			int err;
			/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
			wVersionRequested = MAKEWORD(2, 2);
			err = ::WSAStartup(wVersionRequested, &wsaData);

			if (err != 0)
			{
				e.fail();
				throw std::runtime_error("Failed to initialize Windows Socket library");
			}

			if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2)
			{
				WSACleanup();
				e.fail(Reason::Unsupported);
				throw std::runtime_error("Windows Socket library is incompatible version");
			}
			
			e.succeed();

			return e;
		}

		void Socket::deinitialize() noexcept
		{
			::WSACleanup();
		}

		int Socket::getLastNativeError() noexcept
		{
			return ::WSAGetLastError();
		}

		Reason Socket::fromNativeError(int error) noexcept
		{
			Reason reason;

			switch (error)
			{
				case 0:
					reason = Reason::None;
					break;

				case WSAEALREADY:
				case WSAEINPROGRESS:
					reason = Reason::Busy;
					break;

				case WSAEWOULDBLOCK:
				case WSAETIMEDOUT:
					reason = Reason::Timeout;
					break;

				case WSAENETRESET:
					reason = Reason::Reconnected;
					break;

				case WSAENOTCONN:
				case WSAEDESTADDRREQ:
					reason = Reason::Unopened;
					break;

				case WSAEISCONN:
					reason = Reason::Connected;
					break;

				case WSAENETDOWN:
				case WSAESHUTDOWN:
				case WSAECONNABORTED:
				case WSAECONNRESET:
					reason = Reason::Disconnected;
					break;

				case WSAEADDRINUSE:
				case WSAENOTEMPTY:
					reason = Reason::Exists;
					break;

				case WSAENETUNREACH:
					reason = Reason::Unreachable;
					break;

				case WSAEHOSTUNREACH:
				case WSAEHOSTDOWN:
					reason = Reason::Missing;
					break;

				case WSAEMSGSIZE:
					reason = Reason::Overflow;
					break;

				case WSAEINTR:
					reason = Reason::Interrupted;
					break;
				
				case WSAEACCES:
				case WSAECONNREFUSED:
					reason = Reason::Refused;
					break;

				case WSAEFAULT:
				case WSAEBADF:
				case WSAENOTSOCK:
				case WSAEINVAL:
				case WSAENOPROTOOPT:
				case WSAELOOP:
				case WSAENAMETOOLONG:
				case WSAEADDRNOTAVAIL:
					reason = Reason::Invalid;
					break;

				case WSAEOPNOTSUPP:
				case WSAEAFNOSUPPORT:
				case WSAEINVALIDPROVIDER:
				case WSAEPROTONOSUPPORT:
				case WSAEPROTOTYPE:
				case WSAESOCKTNOSUPPORT:
					reason = Reason::Unsupported;
					break;

				case WSAEMFILE:
				case WSAENOBUFS:
				case WSAETOOMANYREFS:
				case WSAEPROCLIM:
				case WSAEUSERS:
					reason = Reason::Exhausted;
					break;

				default:
					reason = Reason::Unknown;
					break;
			}

			return reason;
		}

		void Socket::setReceiveTimeout(std::chrono::milliseconds timeout)
		{
			DWORD opt;
			if (timeout.count() == 0)
			{
				opt = 1;
			}
			else if (timeout == std::chrono::milliseconds::max())
			{
				opt = 0;
			}
			else
			{
				opt = static_cast<DWORD>(timeout.count());
			}

			int status = ::setsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char *>(&opt), sizeof(opt));
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not set socket receive timeout");
			}
		}

		std::chrono::milliseconds Socket::getReceiveTimeout() const
		{
			DWORD value = 0;
			socklen_t timelen = 0;

			int status = ::getsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char *>(&value), &timelen);
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not get socket receive timeout");
			}

			return value != 0 ? std::chrono::milliseconds(value) : std::chrono::milliseconds::max();
		}

		void Socket::setSendTimeout(std::chrono::milliseconds timeout)
		{
			DWORD opt;
			if (timeout.count() == 0)
			{
				opt = 1;
			}
			else if (timeout == std::chrono::milliseconds::max())
			{
				opt = 0;
			}
			else
			{
				opt = static_cast<DWORD>(timeout.count());
			}

			int status = ::setsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const char *>(&opt), sizeof(opt));
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not set socket receive timeout");
			}
		}

		std::chrono::milliseconds Socket::getSendTimeout() const
		{
			DWORD value = 0;
			socklen_t timelen = 0;

			int status = ::getsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<char *>(&value), &timelen);
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not get socket receive timeout");
			}

			return value != 0 ? std::chrono::milliseconds(value) : std::chrono::milliseconds::max();
		}

#else
		State Socket::initialize()
		{
			return cio::complete(Action::Prepare);
		}

		void Socket::deinitialize() noexcept
		{
			// nothing to do
		}

		int Socket::getLastNativeError() noexcept
		{
			return errno;
		}

		Reason Socket::fromNativeError(int error) noexcept
		{
			return cio::error(error);
		}

		void Socket::setReceiveTimeout(std::chrono::milliseconds timeout)
		{
			struct timeval timeval;

			if (timeout.count() == 0)
			{
				timeval.tv_sec = 0;
				timeval.tv_usec = 1;
			}
			else if (timeout == std::chrono::milliseconds::max())
			{
				timeval.tv_sec = 0;
				timeval.tv_usec = 0;
			}
			else
			{
				timeval.tv_sec = static_cast<long>(timeout.count() / 1000);
				timeval.tv_usec = static_cast<long>(timeout.count() % 1000) * 1000;
			}

			int status = ::setsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&timeval), sizeof(timeval));
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not set socket receive timeout");
			}
		}

		std::chrono::milliseconds Socket::getReceiveTimeout() const
		{
			struct timeval timeval;
			timeval.tv_sec = 0;
			timeval.tv_usec = 0;

			socklen_t timelen = 0;

			int status = ::getsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char*>(&timeval), &timelen);
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not get socket receive timeout");
			}

			return (timeval.tv_sec > 0 || timeval.tv_usec > 0) ? std::chrono::milliseconds(timeval.tv_sec * 1000 + timeval.tv_usec / 1000) : std::chrono::milliseconds::max();
		}

		void Socket::setSendTimeout(std::chrono::milliseconds timeout)
		{
			struct timeval timeval;

			if (timeout.count() == 0)
			{
				timeval.tv_sec = 0;
				timeval.tv_usec = 1;
			}
			else if (timeout == std::chrono::milliseconds::max())
			{
				timeval.tv_sec = 0;
				timeval.tv_usec = 0;
			}
			else
			{
				timeval.tv_sec = static_cast<long>(timeout.count() / 1000);
				timeval.tv_usec = static_cast<long>(timeout.count() % 1000) * 1000;
			}

			int status = ::setsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const char*>(&timeval), sizeof(timeval));
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not set socket send timeout");
			}
		}

		std::chrono::milliseconds Socket::getSendTimeout() const
		{
			struct timeval timeval;
			timeval.tv_sec = 0;
			timeval.tv_usec = 0;

			socklen_t timelen = 0;

			int status = ::getsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<char*>(&timeval), &timelen);
			if (status < 0)
			{
				throw cio::Exception(Socket::mapLastNativeError(), "Could not get socket receive timeout");
			}

			return (timeval.tv_sec > 0 || timeval.tv_usec > 0) ? std::chrono::milliseconds(timeval.tv_sec * 1000 + timeval.tv_usec / 1000) : std::chrono::milliseconds::max();
		}
#endif

		Reason Socket::mapLastNativeError() noexcept
		{
			return Socket::fromNativeError(Socket::getLastNativeError());
		}

		SocketId Socket::invalid() noexcept
		{
			return CIO_INVALID_SOCKET;
		}

		Socket::Socket() noexcept :
			mSocketId(CIO_INVALID_SOCKET)
		{
			// nothing more to do
		}

		Socket::Socket(SocketId socketId) noexcept :
			mSocketId(socketId)
		{
			// nothing more to do
		}
		
		Socket::Socket(AddressFamily af, ChannelType type) :
			mSocketId(CIO_INVALID_SOCKET)
		{
			this->allocate(af, type, ProtocolFamily());
		}
		
		Socket::Socket(AddressFamily af, ChannelType type, ProtocolFamily pf) :
			mSocketId(CIO_INVALID_SOCKET)
		{
			this->allocate(af, type, pf);
		}
		
		Socket::Socket(AddressFamily af, ChannelType type, ProtocolFamily pf, Flow flow) :
			mSocketId(CIO_INVALID_SOCKET)
		{
			this->allocate(af, type, pf, flow);
		}
		
		Socket::Socket(const Service &service) :
			mSocketId(CIO_INVALID_SOCKET)
		{
			this->allocate(service);
		}
		
		Socket::Socket(Socket &&in) noexcept :
			mSocketId(in.mSocketId)
		{
			in.mSocketId = CIO_INVALID_SOCKET;
		}

		Socket &Socket::operator=(Socket &&in) noexcept
		{
			if (this != &in)
			{
				this->clear();
				mSocketId = in.mSocketId;
				in.mSocketId = CIO_INVALID_SOCKET;
			}

			return *this;
		}

		Socket::~Socket() noexcept
		{
			this->clear();
		}

		void Socket::clear() noexcept
		{
			if (mSocketId != CIO_INVALID_SOCKET)
			{
				CIO_CLOSE_SOCKET(mSocketId);
				mSocketId = CIO_INVALID_SOCKET;
				Socket::deinitialize();
			}
		}

		SocketId Socket::take() noexcept
		{
			SocketId socketId = mSocketId;
			mSocketId = CIO_INVALID_SOCKET;
			return socketId;
		}

		void Socket::adopt(SocketId socketId) noexcept
		{
			if (socketId != mSocketId)
			{
				this->clear();
				mSocketId = socketId;
			}
		}

		State Socket::allocate(AddressFamily af, ChannelType type)
		{
			return this->allocate(af, type, ProtocolFamily());
		}

		State Socket::allocate(AddressFamily af, ChannelType type, ProtocolFamily pf)
		{
			Socket::initialize();
			this->clear();
			mSocketId = CIO_FROM_SOCKET(::socket(af.get(), toNativeSocketType(type), pf.get()));
			
			State e(Action::Create);
			
			if (mSocketId == CIO_INVALID_SOCKET)
			{
				e.fail(Socket::mapLastNativeError());
				throw Exception(e, "Failed to create a socket");
			}
			else
			{
				e.succeed();
			}

			return e;
		}

		State Socket::allocate(AddressFamily af, ChannelType type, ProtocolFamily pf, Flow flow)
		{
			State state = this->allocate(af, type, pf);
			
			if (state.succeeded())
			{
				bool broadcast = (flow == Flow::Broadcast);
				this->setBroadcastEnabled(broadcast);
			}
			
			return state;
		}

		State Socket::allocate(const Service &service)
		{
			return this->allocate(service.getAddressFamily(), service.getChannelType(), service.getProtocolFamily(), service.getFlow());
		}

		State Socket::connect(const void *address, std::size_t addrlen)
		{
			State e(Action::Connect);

			int value = CIO_FROM_SOCKET(::connect(CIO_TO_SOCKET(mSocketId),
				reinterpret_cast<const struct sockaddr *>(address), static_cast<socklen_t>(addrlen)));

			if (value < 0)
			{
				e.fail(Socket::mapLastNativeError());
				sLogger.error() << "Failed to connect to remote address: " << e.reason;
				throw Exception(e, "Failed to connect to a remote address");
			}
			else
			{
				e.succeed();
			}

			return e;
		}

		State Socket::connect(const Address &address)
		{
			return this->connect(address.data(), address.size());
		}

		State Socket::connect(const Service& service)
		{
			if (service.getFlow() == Flow::Broadcast)
			{
				this->setBroadcastEnabled(true);
			}

			return this->connect(service.getAddress());
		}

		Socket Socket::accept()
		{
			State e(Action::Discover);
		
			std::uint8_t addrbuf[128];
			struct sockaddr *sa = reinterpret_cast<struct sockaddr *>(addrbuf);
			socklen_t slen = sizeof(addrbuf);
			SocketId acceptedId = CIO_FROM_SOCKET(::accept(CIO_TO_SOCKET(mSocketId), sa, &slen));

			if (acceptedId == CIO_INVALID_SOCKET)
			{
				e.fail(Socket::mapLastNativeError());
				throw Exception(e, "Failed to accept an incoming connection");
			}
			else
			{
				e.succeed();
			}

			return Socket(acceptedId);
		}

		Socket Socket::accept(void *address, std::size_t &addrlen)
		{
			struct sockaddr *sa = reinterpret_cast<struct sockaddr *>(address);
			socklen_t slen = static_cast<socklen_t>(addrlen);
			SocketId socketId = CIO_FROM_SOCKET(::accept(CIO_TO_SOCKET(mSocketId), sa, &slen));
			addrlen = slen;

			if (socketId == CIO_INVALID_SOCKET)
			{
				Reason reason = Socket::mapLastNativeError();
				throw Exception(reason, "Failed to accept an incoming connection");
			}

			return Socket(socketId);
		}

		Socket Socket::accept(Address &address)
		{
			std::uint8_t addrbuf[128];
			struct sockaddr *sa = reinterpret_cast<struct sockaddr *>(addrbuf);
			socklen_t slen = sizeof(addrbuf);
			SocketId acceptedId = CIO_FROM_SOCKET(::accept(CIO_TO_SOCKET(mSocketId), sa, &slen));

			if (acceptedId == CIO_INVALID_SOCKET)
			{
				Reason reason = Socket::mapLastNativeError();
				throw Exception(reason, "Failed to accept an incoming connection");
			}

			address.set(sa, slen);
			return Socket(acceptedId);
		}

		State Socket::bind(const void *address, std::size_t addrlen)
		{
			State e(Action::Open);
			const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(address);
			int result = ::bind(CIO_TO_SOCKET(mSocketId), sa, static_cast<socklen_t>(addrlen));

			if (result < 0)
			{
				e.fail(Socket::mapLastNativeError());
				
				Logger logger("cio::net::Socket");
				logger.error() << "Failed to bind to a local address: " << e.reason;
				
				throw Exception(e, "Failed to bind to a local address");
			}
			else
			{
				e.succeed();
			}

			return e;
		}

		State Socket::bind(const Address &address)
		{
			return this->bind(address.data(), address.size());
		}

		State Socket::bind(const Service &service)
		{
			if (service.getFlow() == Flow::Broadcast)
			{
				this->setBroadcastEnabled(true);
			}

			return this->bind(service.getAddress());
		}

		State Socket::listen()
		{
			State e(Action::Scan);
			int result = ::listen(CIO_TO_SOCKET(mSocketId), CIO_MAX_SOCKET_BACKLOG);

			if (result < 0)
			{
				e.fail(Socket::mapLastNativeError());
				throw Exception(e, "Failed to configure socket to listen on bound address");
			}
			else
			{
				e.succeed();
			}

			return e;
		}

		State Socket::listen(std::size_t backlog)
		{
			State e(Action::Scan);
		
			int actualBacklog = static_cast<int>(backlog);
			int result = ::listen(CIO_TO_SOCKET(mSocketId), actualBacklog);

			if (result < 0)
			{
				e.fail(Socket::mapLastNativeError());
				throw Exception(e, "Failed to configure socket to listen on bound address");
			}
			else
			{
				e.succeed();
			}

			return e;
		}

		Progress<std::size_t> Socket::receive(void *buffer, std::size_t length, int flags) noexcept
		{
			Progress<std::size_t> result(Action::Read);

			auto actual = ::recv(CIO_TO_SOCKET(mSocketId), reinterpret_cast<char *>(buffer),
								 static_cast<cio_socket_size_t>(length), flags);

			if (actual < 0)
			{
				result.fail(Socket::mapLastNativeError());
			}
			else if (static_cast<std::size_t>(actual) <= length)
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed();
			}
			else
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed(Reason::Overflow);
			}

			return result;
		}

		Progress<std::size_t> Socket::receiveFrom(void *buffer, std::size_t length, void *address, std::size_t &addrlen, int flags) noexcept
		{
			Progress<std::size_t> result(Action::Read);

			struct sockaddr *sa = reinterpret_cast<struct sockaddr *>(address);
			socklen_t slen = static_cast<socklen_t>(addrlen);
			auto actual = ::recvfrom(CIO_TO_SOCKET(mSocketId), reinterpret_cast<char *>(buffer),
									 static_cast<cio_socket_size_t>(length), flags, sa, &slen);
			addrlen = slen;

			if (actual < 0)
			{
				result.fail(Socket::mapLastNativeError());
			}
			else if (static_cast<std::size_t>(actual) <= length)
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed();
			}
			else
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed(Reason::Overflow);
			}

			return result;
		}

		Progress<std::size_t> Socket::receiveFrom(Datagram &datagram, int flags /* = 0 */) noexcept
		{
			Buffer &packet = datagram.getPacket();
			std::size_t remaining = packet.remaining();

			std::uint8_t addrbuf[128];
			std::size_t addrlen = sizeof(addrbuf);

			Progress<std::size_t> result = this->receiveFrom(packet.current(), remaining, addrbuf, addrlen, flags);
			packet.skip(result.count);
			datagram.setProgress(result);

			Address address;
			address.set(addrbuf, addrlen);
			datagram.setSender(std::move(address));
			return result;
		}

		Progress<std::size_t> Socket::receive(Buffer &buffer, int flags) noexcept
		{
			std::size_t remaining = buffer.remaining();
			Progress<std::size_t> result = this->receive(buffer.current(), remaining, flags);
			buffer.skip(std::min(result.count, remaining));
			return result;
		}

		Progress<std::size_t> Socket::send(const void *buffer, std::size_t length, int flags) noexcept
		{
			Progress<std::size_t> result(Action::Write);

			auto actual = ::send(CIO_TO_SOCKET(mSocketId), reinterpret_cast<const char *>(buffer),
								 static_cast<cio_socket_size_t>(length), flags);

			if (actual < 0)
			{
				result.fail(Socket::mapLastNativeError());
			}
			else
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed();
			}

			return result;
		}

		Progress<std::size_t> Socket::send(Buffer &buffer, int flags) noexcept
		{
			std::size_t remaining = buffer.remaining();
			Progress<std::size_t> result = this->send(buffer.current(), remaining, flags);
			buffer.skip(std::min(result.count, remaining));
			return result;
		}

		Progress<std::size_t> Socket::sendTo(const void *buffer, std::size_t length, const void *address, std::size_t addrlen, int flags) noexcept
		{
			Progress<std::size_t> result(Action::Write);

			const struct sockaddr *sa = reinterpret_cast<const struct sockaddr *>(address);
			socklen_t slen = static_cast<socklen_t>(addrlen);

			auto actual = ::sendto(CIO_TO_SOCKET(mSocketId), reinterpret_cast<const char *>(buffer),
								   static_cast<cio_socket_size_t>(length), flags, sa, slen);

			if (actual < 0)
			{
				result.fail(Socket::mapLastNativeError());
			}
			else
			{
				result.count = static_cast<std::size_t>(actual);
				result.succeed();
			}

			return result;
		}

		Progress<std::size_t> Socket::sendTo(Datagram &datagram, int flags /* = 0 */) noexcept
		{
			Progress<std::size_t> result;

			Buffer &packet = datagram.getPacket();
			const Address &recipient = datagram.getRecipient();

			result = this->sendTo(packet.current(), packet.remaining(), recipient.data(), recipient.size(), flags);
			packet.skip(result.count);
			datagram.setProgress(result);

			return result;
		}

		void Socket::setBroadcastEnabled(bool value)
		{
			int enabled = value;
			int result = ::setsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char *>(&enabled), sizeof(enabled));
			if (result != 0)
			{
				throw Exception(Socket::mapLastNativeError());
			}
		}

		bool Socket::getBroadcastEnabled() const
		{
			int enabled = 0;
			socklen_t length = 0;
			int result = ::getsockopt(CIO_TO_SOCKET(mSocketId), SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char *>(&enabled), &length);
			if (result != 0)
			{
				throw Exception(Socket::mapLastNativeError());
			}
			return enabled != 0;
		}

		State Socket::setGeneralOption(int layer, int option, const void *data, std::size_t length) noexcept
		{
			State e(Action::Metadata);
			int result = ::setsockopt(CIO_TO_SOCKET(mSocketId), layer, option, reinterpret_cast<const char *>(data), static_cast<socklen_t>(length));
			
			if (result == 0)
			{
				e.succeed();
			}
			else
			{
				e.fail(Socket::mapLastNativeError());
			}
			
			return e;
		}
				
		Progress<std::size_t> Socket::getGeneralOption(int layer, int option, void *data, std::size_t maxLength) const noexcept
		{
			Progress<std::size_t> e(Action::Metadata);
		
			socklen_t actualLength = 0;
			int result = ::getsockopt(CIO_TO_SOCKET(mSocketId), layer, option, reinterpret_cast<char *>(data), &actualLength);
			if (result == 0)
			{
				e.count = actualLength;
				e.succeed();
			}
			else
			{
				e.fail(Socket::mapLastNativeError());
			}
			
			return e;
		}

		Socket::operator bool() const noexcept
		{
			return mSocketId != CIO_INVALID_SOCKET;
		}
	}
}


