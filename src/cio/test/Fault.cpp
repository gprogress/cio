/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Fault.h"

#include <cio/Severity.h>

namespace cio
{
	namespace test
	{
		Fault::Fault() noexcept :
			mSeverity(Severity::None)
		{
			// nothing more to do
		}

		Fault::Fault(const char *message) :
			mMessage(message),
			mSeverity(Severity::None)
		{
			// nothing more to do
		}

		Fault::Fault(std::string message) :
			mMessage(std::move(message)),
			mSeverity(Severity::None)
		{
			// nothing more to do
		}

		Fault::Fault(const char *message, const Source &source, Severity severity) :
			mMessage(message),
			mSource(source),
			mSeverity(severity)
		{
			// nothing more to do
		}

		Fault::Fault(std::string message, const Source &source, Severity severity) :
			mMessage(std::move(message)),
			mSource(source),
			mSeverity(severity)
		{
			// nothing more to do
		}

		Fault::Fault(const Fault &in) = default;

		Fault::Fault(Fault &&in) noexcept :
			mMessage(std::move(in.mMessage)),
			 mSource(std::move(in.mSource)),
			 mSeverity(in.mSeverity)
		{
			// nothing more to do
		}

		Fault &Fault::operator=(const Fault &in) = default;

		Fault &Fault::operator=(Fault &&in) noexcept
		{
			mMessage = std::move(in.mMessage);
			mSource = std::move(in.mSource);
			mSeverity = in.mSeverity;
			return *this;
		}

		Fault::~Fault() noexcept = default;

		void Fault::clear() noexcept
		{
			mMessage.clear();
			mSource.clear();
			mSeverity = Severity::None;
		}

		const char *Fault::what() const noexcept
		{
			return mMessage.c_str();
		}

		const std::string &Fault::getMessage() const
		{
			return mMessage;
		}

		void Fault::setMessage(const char *message)
		{
			mMessage = message;
		}

		void Fault::setMessage(std::string message)
		{
			mMessage = std::move(message);
		}

		const Source &Fault::getSource() const
		{
			return mSource;
		}

		void Fault::setSource(const Source &source)
		{
			mSource = source;
		}

		Severity Fault::getSeverity() const
		{
			return mSeverity;
		}

		void Fault::setSeverity(Severity severity)
		{
			mSeverity = severity;
		}

		bool Fault::failed() const
		{
			return mSeverity >= Severity::Error;
		}

		bool Fault::passed() const
		{
			return mSeverity < Severity::Error;
		}

		Fault::operator bool() const
		{
			return mSeverity >= Severity::Error;
		}
	}
}
