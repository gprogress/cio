/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Archive.h"

#include "Inflator.h"
#include "Protocol.h"

#include <cio/Buffer.h>
#include <cio/Class.h>
#include <cio/ModeSet.h>
#include <cio/ProtocolFactory.h>
#include <cio/ReadBuffer.h>
#include <cio/Seek.h>

#include <cio/Print.h>

namespace cio
{
	namespace zip
	{
		const std::uint32_t Archive::EOCD_SIGNATURE = 0x06054b50;
	
		const std::uint32_t Archive::EOCD64_SIGNATURE = 0x06064b50;

		const std::uint32_t Archive::EOCD64_LOC_SIGNATURE = 0x07064b50;

		const std::uint32_t Archive::CDENTRY_SIGNATURE = 0x02014b50;

		const std::uint32_t Archive::LOCAL_FILE_SIGNATURE = 0x04034b50;

		const std::uint16_t Archive::ZIP64_EXTRA_SIGNATURE = 0x0001u;

		Class<Archive> Archive::sMetaclass("cio::zip::Archive");

		const Metaclass &Archive::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		void Archive::readZipEntry(Input &input, Buffer &entry)
		{
			entry.reserve(512u); // reserve a reasonable amount for a normal entry
			entry.reset(); // reset to read from input
			entry.limit(46u); // every entry is guaranteed to have at least 46 bytes
			input.fill(entry);

			// Inspect entry read so far to determine how many more bytes we need
			std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
			std::size_t extraLength = entry.getLittleFromPosition<std::uint16_t>(30u);
			std::size_t commentLength = entry.getLittleFromPosition<std::uint16_t>(32u);

			// Zip64 fields and possibly other fields are in "extra" section

			std::size_t totalExtraNeeded = nameLength + extraLength + commentLength;
			entry.reserve(totalExtraNeeded + 46u);
			entry.limit(entry.limit() + totalExtraNeeded);
			input.fill(entry);
			entry.flip();
		}
		
		Metadata Archive::decodeEntryMetadata(const Buffer &entry)
		{
			Metadata metadata;

			if (entry.remaining() >= 46u)
			{
				// Inspect entry read so far to determine how many more bytes we need
				std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
				std::size_t extraLength = entry.getLittleFromPosition<std::uint16_t>(30u);

				if (entry.remaining() >= (46u + nameLength))
				{
					Text text(reinterpret_cast<const char *>(entry.data() + 46u), nameLength);
					metadata.setLocation(Path::fromUnixFile(text));
				}

				std::uint64_t compressed = entry.getLittleFromPosition<std::uint32_t>(20u);
				std::uint64_t uncompressed = entry.getLittleFromPosition<std::uint32_t>(24u);

				// If either compressed or uncompressed size is set to Zip64 signature value, read for Zip64 extra info
				if (compressed == UINT32_MAX || uncompressed == UINT32_MAX)
				{
					// Need to parse extra fields, set up a read-only sub-buffer for that
					ReadBuffer extra(entry.data() + 46u + nameLength, extraLength);

					while (extra.available())
					{
						std::uint16_t signature = extra.getLittle<std::uint16_t>();
						std::uint16_t length = extra.getLittle<std::uint16_t>();
						std::size_t mark = extra.position();
						
						if (signature == ZIP64_EXTRA_SIGNATURE)
						{
							ReadBuffer zip64(extra.current(), length);

							if (uncompressed == UINT32_MAX)
							{
								uncompressed = zip64.getLittleFromPosition<std::uint64_t>(0u);
							}

							if (compressed == UINT32_MAX)
							{
								compressed = zip64.getLittleFromPosition<std::uint64_t>(8u);
							}
						}

						extra.position(mark + length);
					}

				}
				
				metadata.setLength(uncompressed);
				metadata.setPhysicalSize(compressed);
					
				std::uint32_t attributes = entry.getLittleFromPosition<std::uint32_t>(38u);
				if ((attributes & 0xFF) != 0)
				{
					metadata.setType(Resource::Directory);
				}
				else
				{
					metadata.setType(Resource::File);
				}
			}

			return metadata;
		}

		bool Archive::isMatchingEntry(const Buffer &entry, const Text &filename)
		{
			bool matches = false;
			std::size_t length = filename.strlen();

			if (entry.remaining() >= 46u)
			{
				// Inspect entry read so far to determine how many more bytes we need
				std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
				if (nameLength == length)
				{
					Text candidate(reinterpret_cast<const char *>(entry.data() + 46u), nameLength);
					matches = (candidate == filename);
				}
			}

			return matches;
		}

		Archive::Archive() noexcept :
			mZip64(false)
		{
			// nothing to do
		}

		Archive::Archive(const Path &path) :
			mZip64(false)
		{
			// Note: We cannot call any Archive virtual methods here
			const ProtocolFactory &factory = *ProtocolFactory::getDefault();
			std::shared_ptr<Input> input = factory.openInput(path, cio::ModeSet::readonly());
			if (input)
			{
				mZipInput = std::move(input);
				this->findCentralDirectory();

				if (mEntryProgress.total > 0)
				{
					Archive::readZipEntry(mCentralDirectory, mCurrentEntry);
					++mEntryProgress.count;
				}
			}
		}

		Archive::Archive(const Archive &in) = default;
		
		// VS2017 does not allow us to default this method
		Archive::Archive(Archive &&in) noexcept :
			Protocol(std::move(in)),
			mZipInput(std::move(in.mZipInput)),
			mCentralDirectory(std::move(in.mCentralDirectory)),
			mEntryProgress(std::move(in.mEntryProgress)),
			mCurrentEntry(std::move(in.mCurrentEntry)),
			mZip64(in.mZip64)
		{
			// nothing more to do
		}

		Archive &Archive::operator=(const Archive &in) = default;
		
		// VS2017 does not allow us to default this method
		Archive &Archive::operator=(Archive &&in) noexcept
		{
			if (&in != this)
			{
				Protocol::operator=(std::move(in));
				mZipInput = std::move(in.mZipInput);
				mCentralDirectory = std::move(in.mCentralDirectory);
				mEntryProgress = std::move(in.mEntryProgress);
				mCurrentEntry = std::move(in.mCurrentEntry);
				mZip64 = in.mZip64;
			}

			return *this;
		}

		Archive::~Archive() noexcept = default;

		const Metaclass &Archive::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void Archive::clear() noexcept
		{
			mZipInput.reset();
			mEntryProgress.clear();
			mCentralDirectory.clear();
			mCurrentEntry.clear();
			mZip64 = false;
		}
		
		Length Archive::size() const noexcept
		{
			return mEntryProgress.total;
		}
		
		Length Archive::capacity() const noexcept
		{
			return CIO_UNKNOWN_LENGTH;
		}

		
		ModeSet Archive::openWithFactory(const cio::Path &resource, cio::ModeSet modes, cio::ProtocolFactory *factory)
		{
			ModeSet actual;
			
			if (factory)
			{
				std::shared_ptr<Input> input = factory->openInput(resource, modes);
				if (input)
				{
					this->openZipInput(std::move(input));
					actual |= cio::Mode::Read | cio::Mode::Load;
				}
			}
			
			return actual;
		}

		Metadata Archive::readMetadata(const Path &input) const
		{
			Metadata read;

			if (input.isRelative())
			{
				read = this->readRelativeMetadata(input);
			}
			else
			{
				std::pair<cio::Path, cio::Path> split = cio::zip::Protocol::splitArchiveEntry(input);
				if (split.first != mZipInput->location())
				{
					throw std::runtime_error("Requested input was not relative to zip archive");
				}

				if (!split.second.empty())
				{
					read = this->readRelativeMetadata(split.second);
				}
			}

			return read;
		}

		Text Archive::filename() const
		{
			// Decode buffer for filename
			Text text;
			if (mCurrentEntry.available())
			{
				std::size_t length = mCurrentEntry.getLittleFromPosition<std::uint16_t>(28u);
				text.bind(reinterpret_cast<const char *>(mCurrentEntry.data() + 46u), length);
			}
			return text;
		}

		Metadata Archive::current() const
		{
			return Archive::decodeEntryMetadata(mCurrentEntry);
		}

		State Archive::next()
		{
			State state(Action::Discover);

			if (mEntryProgress.count < mEntryProgress.total)
			{
				Archive::readZipEntry(mCentralDirectory, mCurrentEntry);
				++mEntryProgress.count;
				state.succeed();
			}
			else
			{
				mCurrentEntry.limit(0);
				state.fail(Reason::Underflow);
			}

			return state;
		}

		State Archive::rewind()
		{
			State state(Action::Seek);
			if (!mCentralDirectory)
			{
				state.fail(Reason::Unopened);
				throw Exception(state);
			}

			mCentralDirectory.position(0);
			mEntryProgress.count = 0;
			this->next();

			if (mCentralDirectory)
			{
				state.succeed();
			}
			else
			{
				state.fail(Reason::Underflow);
			}

			return state;
		}

		State Archive::seekToEntry(const Path &relative)
		{
			State state(Action::Seek);

			if (relative.empty())
			{
				state.fail(Reason::Invalid);
				throw Exception(state, "Relative path to seek was empty");
			}

			if (!relative.isRelative())
			{
				state.fail(Reason::Unusable);
				throw Exception(state, "Absolute path provided for relative seek");
			}

			// Filename to search
			Text filename = relative.toUnixFile();

			// Check current entry first in case we're already there
			if (mCurrentEntry.available() && Archive::isMatchingEntry(mCurrentEntry, filename))
			{
				state.succeed();
			}
			else
			{
				// reset central directory and scan
				mCentralDirectory.position(0);
				State next = this->next();
				while (next.succeeded())
				{
					if (Archive::isMatchingEntry(mCurrentEntry, filename))
					{
						state.succeed();
						break;
					}

					next = this->next();
				}

				if (!state.succeeded())
				{
					if (next.reason == Reason::Underflow)
					{
						state.fail(Reason::Missing);
					}
					else
					{
						state.fail(next.reason);
						throw Exception(state);
					}
				}
			}

			return state;
		}

		std::unique_ptr<Input> Archive::openCurrentInput(ModeSet modes) const
		{
			std::unique_ptr<Input> input;
			
			if (!mZipInput)
			{
				throw Exception(Reason::Unopened);
			}
			
			if (!mCurrentEntry.available())
			{
				throw Exception(Reason::Underflow);
			}
			
			RelativeInput record = this->getCurrentFileRecord();
			
			if (!record)
			{
				throw Exception(Reason::Unusable, "Zip file entry is not something that could be opened");
			}
			
			input = this->openFileRecordInput(std::move(record), modes);

			return input;
		}

		std::unique_ptr<Input> Archive::openInput(const cio::Path &path, ModeSet modes) const
		{
			std::unique_ptr<Input> input;
			
			if (!mZipInput)
			{
				throw Exception(Reason::Unopened);
			}
			
			if (path.empty())
			{
				throw Exception(Reason::Invalid, "No path specified to open");
			}

			RelativeInput record;
			
			if (path.isRelative())
			{
				record = this->findFileRecord(path);
			}
			else
			{
				cio::Path zipLocation = mZipInput->location();
				std::pair<cio::Path, cio::Path> split = cio::zip::Protocol::splitArchiveEntry(path);
				if (split.first != zipLocation)
				{
					throw Exception(Reason::Invalid, "Path to open is not relative to archive");
				}
				record = this->findFileRecord(split.second);
			}
	
			if (!record)
			{
				throw Exception(Reason::Unusable, "Zip file entry is not something that could be opened");
			}
			
			input = this->openFileRecordInput(std::move(record), modes);

			return input;
		}

		bool Archive::traversable() const noexcept
		{
			return mCurrentEntry.available();
		}
		
		std::shared_ptr<Input> Archive::getZipInput() noexcept
		{
			return mZipInput;
		}
				
		std::shared_ptr<const Input> Archive::getZipInput() const noexcept
		{
			return mZipInput;
		}
				
		void Archive::openZipInput(std::shared_ptr<Input> input)
		{
			this->clear();

			mZipInput = std::move(input);

			this->findCentralDirectory();
			this->next();
		}

		void Archive::findCentralDirectory()
		{	
			// To find the EOCD record - have to read end of the input backwards until we find a valid record
			Length length = mZipInput->size();
			if (length == CIO_UNKNOWN_LENGTH)
			{
				// TODO capture into a local file
				throw cio::Exception(Reason::Unseekable, "Could not get archive length to find central directory");
			}
			
			// Minimum zip size is a single EOCD record with 22 bytes
			if (length < 22)
			{
				throw cio::Exception(Reason::Unparsable, "Archive length is too short to be a zip archive");
			}
			
			bool haveEocd = false;
			bool zip64 = false;
			
			// Start the scan 22 bytes before end. 99% of the time, the zip comment will be empty and this will be sufficient
			Length scanOffset = length - 22;
			std::uint8_t data[56] = { }; // size at 56 in case we need EOCD64 record
			cio::Buffer buffer(data, 56u, nullptr);
			
			buffer.limit(22u);
			mZipInput->fill(buffer, scanOffset, Seek::Begin);
			buffer.flip();
			
			std::uint32_t signature = buffer.getLittleFromPosition<std::uint32_t>(0u);			
			if (signature == Archive::EOCD_SIGNATURE)
			{
				std::uint16_t commentLength = buffer.getLittleFromPosition<std::uint16_t>(20u);
				haveEocd = (commentLength == 0);
			}
			
			// Didn't find the easy EOCD, do it for real
			if (!haveEocd)
			{
				// Initialize
				scanOffset = length - std::min(length, Length(65536));
						
				buffer.allocate(65536);
				mZipInput->requestFill(buffer, scanOffset, Seek::Begin);
				buffer.flip();
				
				std::uint32_t expectedCommentLength = 1u;
				buffer.position(buffer.limit());
				
				while (!haveEocd && buffer.position() > 22u)
				{
					buffer.position(buffer.position() - 23u); // back up then try for signature
					signature = buffer.getLittleFromPosition<std::uint32_t>(0u);

					if (signature == Archive::EOCD_SIGNATURE)
					{
						std::uint16_t commentLength = buffer.getLittleFromPosition<std::uint16_t>(20u);
						haveEocd = (commentLength == expectedCommentLength);
					}
					
					++expectedCommentLength;
				}
			}
			
			// Found a classic EOCD, let's explore further
			if (haveEocd)
			{
				// We now have a buffer containing the EOCD
				scanOffset += buffer.position();

				// Verify we started at the right spot
				std::uint16_t totalEntries = buffer.getLittleFromPosition<std::uint16_t>(10u);
				std::uint32_t cdSize = buffer.getLittleFromPosition<std::uint32_t>(12u);
				std::uint32_t cdStart = buffer.getLittleFromPosition<std::uint32_t>(16u);

				// Classic zip, central directory is directly specified
				if (cdStart != UINT32_MAX && cdSize != UINT32_MAX && totalEntries != UINT16_MAX)
				{
					mCentralDirectory.open(mZipInput, cdStart, cdSize);
					mEntryProgress.request(Action::Discover);
					mEntryProgress.start(totalEntries);
				}
				else
				{
					// Need to find Zip64 EOCD Locator right before this
					buffer.reset();
					buffer.limit(20u);
					mZipInput->fill(buffer, scanOffset - 20u, Seek::Begin);
					buffer.flip();

					std::uint32_t signature = buffer.getLittle<std::uint32_t>();
					if (signature != Archive::EOCD64_LOC_SIGNATURE)
					{
						throw cio::Exception(Reason::Unparsable, "Zip64 does not have locator record just prior to main EOCD");
					}

					// Have the locator, now we can parse EOCD64 record
					std::uint32_t diskNum = buffer.getLittle<std::uint32_t>();
					std::uint64_t offset = buffer.getLittle<std::uint64_t>();
					std::uint32_t diskCount = buffer.getLittle<std::uint32_t>();

					buffer.reset();
					buffer.limit(56u);

					mZipInput->fill(buffer, offset, Seek::Begin);
					buffer.flip();

					this->setupFromEocd64(buffer);
				}
			}
			// Did not find a classic EOCD
			else
			{
				throw cio::Exception(Reason::Unparsable, "Zip does not end with EOCD record");
			}
		}

		void Archive::setupFromEocd64(Buffer &buffer)
		{
			std::uint64_t signature = buffer.getLittle<std::uint32_t>();
			if (signature != Archive::EOCD64_SIGNATURE)
			{
				throw cio::Exception(Reason::Unparsable, "Zip EOCD64 signature was not valid");
			}

			std::uint64_t length = buffer.getLittle<std::uint64_t>();

			// We now have a buffer containing the EOCD64, positioned to start reading fields
			std::uint16_t writeVersion = buffer.getLittle<std::uint16_t>();
			std::uint16_t readVersion = buffer.getLittle<std::uint16_t>();
			std::uint32_t diskNum = buffer.getLittle<std::uint32_t>();
			std::uint32_t cdDiskStart = buffer.getLittle<std::uint32_t>();
			std::uint64_t diskEntries = buffer.getLittle<std::uint64_t>();
			std::uint64_t totalEntries = buffer.getLittle<std::uint64_t>();
			std::uint64_t cdSize = buffer.getLittle<std::uint64_t>();
			std::uint64_t cdStart = buffer.getLittle<std::uint64_t>();

			mCentralDirectory.open(mZipInput, cdStart, cdSize);
			mEntryProgress.request(Action::Discover);
			mEntryProgress.start(totalEntries);
			mZip64 = true;
		}
		
		RelativeInput Archive::getCentralDirectory() const
		{
			return RelativeInput(mZipInput, mCentralDirectory.start(), mCentralDirectory.limit());
		}
				
		RelativeInput Archive::getCurrentFileRecord() const
		{
			return this->getFileRecord(mCurrentEntry);
		}
				
		RelativeInput Archive::getFileRecord(const Buffer &entry) const
		{
			RelativeInput file;
			
			if (entry.remaining() >= 46u)
			{
				std::uint32_t signature = entry.getLittleFromPosition<std::uint32_t>(0);
				if (signature != Archive::CDENTRY_SIGNATURE)
				{
					throw std::runtime_error("Central directory signature missing, cannot parse zip header");
				}

				// Make sure it's not a directory first
				std::uint32_t attributes = entry.getLittleFromPosition<std::uint32_t>(38u);
				if ((attributes & 0xFF) == 0)
				{
					std::uint64_t compressed = entry.getLittleFromPosition<std::uint32_t>(20u);
					std::uint64_t uncompressed = entry.getLittleFromPosition<std::uint32_t>(24u);
					std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
					std::size_t extraLength = entry.getLittleFromPosition<std::uint16_t>(30u);
					std::uint64_t offset = entry.getLittleFromPosition<std::uint32_t>(42u);
					
					if (compressed == UINT32_MAX || uncompressed == UINT32_MAX || offset == UINT32_MAX)
					{
						// Need to parse extra fields, set up a read-only sub-buffer for that
						ReadBuffer extra(entry.data() + 46u + nameLength, extraLength);
					
						while (extra.available())
						{
							std::uint16_t signature = extra.getLittle<std::uint16_t>();
							std::uint16_t length = extra.getLittle<std::uint16_t>();
							std::size_t mark = extra.position();
							
							if (signature == ZIP64_EXTRA_SIGNATURE)
							{
								ReadBuffer zip64(extra.current(), length);

								if (uncompressed == UINT32_MAX && zip64.remaining() >= 8)
								{
									uncompressed = extra.getLittle<std::uint64_t>();
								}

								if (compressed == UINT32_MAX && zip64.remaining() >= 8)
								{
									compressed = extra.getLittle<std::uint64_t>();
								}

								if (offset == UINT32_MAX && zip64.remaining() >= 8)
								{
									offset = extra.getLittle<std::uint64_t>();
								}
							}

							extra.position(mark + length);
						}
					}

					// technically the local file record can have an arbitrary length 
					// with different extra fields than the main record and possibly other shenanigans
					// such a trailing descriptor - so set unknown length and hope for the best
					file.open(mZipInput, offset, CIO_UNKNOWN_LENGTH);
				}
			}
			
			return file;
		}
		
		RelativeInput Archive::findFileRecord(const Path &path) const
		{
			RelativeInput record;
			
			if (!path.empty() && path.isRelative())
			{
				// Note that zip filenames must be normalized relative paths
				// The inputs might not be so let's be safe
				cio::Path normalized = path.makeNormalized();
				Text filename = normalized.getPath();
				
				RelativeInput cfd = this->getCentralDirectory();
				Buffer entry(512u);
				
				while (cfd.readable())
				{
					Archive::readZipEntry(cfd, entry);
					
					if (entry.remaining() >= 46u)
					{
						std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
						if (entry.remaining() >= (46u + nameLength))
						{
							Text text(reinterpret_cast<const char *>(entry.data() + 46u), nameLength);
							if (filename.equalInsensitive(text.trimSuffix('/')))
							{
								record = this->getFileRecord(entry);
								break;
							}
						}
					}
				}
			}
			
			return record;
		}
		
		std::unique_ptr<Input> Archive::openFileRecordInput(RelativeInput record, ModeSet modes) const
		{
			std::unique_ptr<Input> input;
			
			Buffer header(512);
			header.limit(30u);
			record.fill(header);
			
			std::uint32_t signature = header.getLittleFromPosition<std::uint32_t>(0u);
			if (signature != Archive::LOCAL_FILE_SIGNATURE)
			{
				throw Exception(Reason::Unparsable, "Zip local file signature missing");
			}
			
			std::uint64_t compressed = header.getLittleFromPosition<std::uint32_t>(18u);
			std::uint64_t uncompressed = header.getLittleFromPosition<std::uint32_t>(22u);
			std::uint16_t nameLength = header.getLittleFromPosition<std::uint16_t>(26u);
			std::uint16_t extraLength = header.getLittleFromPosition<std::uint16_t>(28u);
			
			std::size_t headerLen = 30u + nameLength + extraLength;
			header.reserve(headerLen);
			header.limit(headerLen);
			record.fill(header);
			header.flip();
			
			Text filename(reinterpret_cast<const char *>(header.data() + 30u), nameLength);
			if (compressed == UINT32_MAX || uncompressed == UINT32_MAX)
			{
				// Need to parse extra fields, set up a read-only sub-buffer for that
				ReadBuffer extra(header.data() + 46u + nameLength, extraLength);
			
				while (extra.available())
				{
					std::uint16_t signature = extra.getLittle<std::uint16_t>();
					std::uint16_t length = extra.getLittle<std::uint16_t>();
					std::size_t mark = extra.position();
					
					if (signature == ZIP64_EXTRA_SIGNATURE)
					{
						ReadBuffer zip64(extra.current(), length);
						
						if (uncompressed == UINT32_MAX)
						{
							uncompressed = zip64.getLittle<std::uint64_t>();
						}
						
						if (compressed == UINT32_MAX)
						{
							compressed = zip64.getLittle<std::uint64_t>();
						}
					}

					extra.position(mark + length);
				}
			}
			
			std::uint16_t compression = header.getLittleFromPosition<std::uint16_t>(8u);
			
			Metadata metadata;
			metadata.setLocation(filename);
			metadata.setType(Resource::File);
			metadata.setMode(modes);
			metadata.setLength(uncompressed);
			metadata.setPhysicalSize(compressed);
			
			std::unique_ptr<RelativeInput> compressedInput(new RelativeInput(mZipInput, record.start() + headerLen, compressed));	

			if (compression == 0)
			{
				input = std::move(compressedInput);
			}
			else if (compression == 8)
			{
				// force raw input with the given metadata
				input.reset(new Inflator(std::move(compressedInput), metadata, -15));
			}
			
			return input;
		}

		Metadata Archive::readRelativeMetadata(const cio::Path &relativePath) const
		{
			Metadata md;
			if (!relativePath.empty() && relativePath.isRelative())
			{
				// Note that zip filenames must be normalized relative paths
				// The inputs might not be so let's be safe
				cio::Path normalized = relativePath.makeNormalized();
				Text filename = normalized.getPath();

				RelativeInput cfd = this->getCentralDirectory();
				Buffer entry(512u);

				while (cfd.readable())
				{
					Archive::readZipEntry(cfd, entry);

					if (entry.remaining() >= 46u)
					{
						std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
						if (entry.remaining() >= (46u + nameLength))
						{
							Text text(reinterpret_cast<const char *>(entry.data() + 46u), nameLength);
							if (filename.equalInsensitive(text.trimSuffix('/')))
							{
								md = this->readRecordMetadata(entry);
								break;
							}
						}
					}
				}
			}
			return md;
		}

		Metadata Archive::readRecordMetadata(const Buffer &entry) const
		{
			Metadata md;
			if (entry.remaining() >= 46u)
			{
				std::uint32_t signature = entry.getLittleFromPosition<std::uint32_t>(0);
				if (signature != Archive::CDENTRY_SIGNATURE)
				{
					throw std::runtime_error("Central directory signature missing, cannot parse zip header");
				}

				// Make sure it's not a directory first
				std::uint32_t attributes = entry.getLittleFromPosition<std::uint32_t>(38u);
				if ((attributes & 0xFF) == 0)
				{
					md.setType(cio::Resource::File);

					std::uint64_t compressed = entry.getLittleFromPosition<std::uint32_t>(20u);
					std::uint64_t uncompressed = entry.getLittleFromPosition<std::uint32_t>(24u);
					std::size_t nameLength = entry.getLittleFromPosition<std::uint16_t>(28u);
					std::size_t extraLength = entry.getLittleFromPosition<std::uint16_t>(30u);
					std::uint64_t offset = entry.getLittleFromPosition<std::uint32_t>(42u);

					Text text(reinterpret_cast<const char *>(entry.data() + 46u), nameLength);
					md.setLocation("jar://" + mZipInput->location().getPath() + "!/" + text.trimSuffix('/'));

					if (compressed == UINT32_MAX || uncompressed == UINT32_MAX || offset == UINT32_MAX)
					{
						// Need to parse extra fields, set up a read-only sub-buffer for that
						ReadBuffer extra(entry.data() + 46u + nameLength, extraLength);

						while (extra.available())
						{
							std::uint16_t signature = extra.getLittle<std::uint16_t>();
							std::uint16_t length = extra.getLittle<std::uint16_t>();
							std::size_t mark = extra.position();

							if (signature == ZIP64_EXTRA_SIGNATURE)
							{
								ReadBuffer zip64(extra.current(), length);

								if (uncompressed == UINT32_MAX && zip64.remaining() >= 8)
								{
									uncompressed = extra.getLittle<std::uint64_t>();
								}

								if (compressed == UINT32_MAX && zip64.remaining() >= 8)
								{
									compressed = extra.getLittle<std::uint64_t>();
								}

								if (offset == UINT32_MAX && zip64.remaining() >= 8)
								{
									offset = extra.getLittle<std::uint64_t>();
								}
							}

							extra.position(mark + length);
						}
					}

					// Currently we only allow read-only access
					md.setMode(cio::Mode::Load | cio::Mode::Read);
					md.setLength(uncompressed);
					md.setPhysicalSize(compressed);
				}
				else
				{
					// TODO figure out external file attributes
				}
			}

			return md;
		}
	}
}

