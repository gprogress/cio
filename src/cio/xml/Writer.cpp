/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Writer.h"

namespace cio
{
	namespace xml
	{
		void Writer::escapeAttributeValue(Text input, std::string &output)
		{
			for (char c : input)
			{
				if (c == '\0')
				{
					break;
				}

				// TODO handle more generally
				switch (c)
				{
					case '"':
						output.append("&quot;");
						break;

					case '&':
						output.append("&amp;");
						break;

					default:
						output.push_back(c);
						break;
				}
			}
		}

		void Writer::indent(std::string &str, std::size_t amount, char c /* = ' '*/)
		{
			str.reserve(str.size() + amount);

			for (unsigned i = 0; i < amount; ++i)
			{
				str.push_back(c);
			}
		}

		void Writer::escapeText(Text input, std::string &output)
		{
			// Preallocate the space
			output.reserve(output.size() + input.size());

			for (char c : input)
			{
				if (c == '\0')
				{
					break;
				}

				switch (c)
				{
					case '<':
						output.append("&lt;");
						break;

					case '>':
						output.append("&gt;");
						break;

					case '&':
						output.append("&amp;");
						break;

					case '\"':
						output.append("&quot;");
						break;

					default:
						output.push_back(c);
						break;
				}
			}
		}

		Writer::Writer() :
			mOutput(nullptr),
			mIndent(3),
			mIndentChar('\t'),
			mInsideElementOpen(false),
			mAfterText(false)
		{
			// do nothing :-)
		}

		Writer::Writer(Output *stream) :
			mOutput(stream),
			mIndent(3),
			mIndentChar('\t'),
			mInsideElementOpen(false),
			mAfterText(false)
		{
			// do nothing :-)
		}

		void Writer::setOutput(Output *stream)
		{
			if (mElements.empty())
			{
				mOutput = stream;
			}
			else
			{
				throw std::logic_error("Attempted to change output streams on a used XML writer!");
			}
		}

		Output *Writer::getOutput()
		{
			return mOutput;
		}

		Writer &Writer::writeHeader()
		{
			*mOutput << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			return *this;
		}

		Writer &Writer::startElement(Text element)
		{
			std::string buffer;

			if (mInsideElementOpen)
			{
				buffer.append(">\n");
			}

			mElements.push_back(element.str());

			if (!mAfterText)
			{
				this->indent(buffer, mIndent * (mElements.size() - 1), mIndentChar);
			}

			mAfterText = false;
			buffer.push_back('<');
			buffer.append(element.str());
			mOutput->write(buffer.c_str(), buffer.size());
			mInsideElementOpen = true;
			return *this;
		}

		Writer &Writer::writeComment(Text comment)
		{
			std::string buffer;

			if (mInsideElementOpen)
			{
				buffer.append(">\n");
			}

			if (!mAfterText)
			{
				this->indent(buffer, mIndent * mElements.size(), mIndentChar);
			}

			mAfterText = false;
			buffer.append("<!--");
			buffer.append(comment.str());
			buffer.append("-->\n");
			mOutput->write(buffer.c_str(), buffer.size());
			mInsideElementOpen = false;
			return *this;
		}

		Writer &Writer::writeAttribute(Text attr, Text value)
		{
			if (mInsideElementOpen)
			{
				std::string buffer(" ");
				buffer.append(attr.str());
				buffer.append("=\"");
				escapeAttributeValue(value, buffer);
				buffer.push_back('"');
				mOutput->write(buffer.c_str(), buffer.size());
			}
			else
			{
				throw std::logic_error(std::string("Attempted to write attribute ").append(attr.str()).append(" but we are not inside of an element tag"));
			}

			return *this;
		}

		Writer &Writer::endElement()
		{
			if (mElements.empty())
			{
				throw std::logic_error("Writer::endElement called without any elements open");
			}

			if (mInsideElementOpen)
			{
				*mOutput << "/>\n";
				mElements.pop_back();
				mInsideElementOpen = false;
			}
			else
			{
				std::string buffer;

				if (!mAfterText)
				{
					indent(buffer, mIndent * (mElements.size() - 1), mIndentChar);
				}

				mAfterText = false;
				buffer.append("</");
				buffer.append(mElements.back());
				buffer.append(">\n");
				mOutput->write(buffer.c_str(), buffer.size());
				mElements.pop_back();
			}

			return *this;
		}

		Writer &Writer::writeText(Text text)
		{
			std::string buffer;

			if (mInsideElementOpen)
			{
				buffer.append(">");
				mInsideElementOpen = false;
			}

			escapeText(text, buffer);
			mOutput->write(buffer.c_str(), buffer.size());
			mAfterText = true;
			return *this;
		}

		void Writer::setIndent(char c, unsigned amount)
		{
			mIndentChar = c;
			mIndent = amount;
		}

		void Writer::setIndentChar(char c)
		{
			mIndentChar = c;
		}

		char Writer::getIndentChar() const
		{
			return mIndentChar;
		}

		void Writer::setIndentAmount(unsigned amt)
		{
			mIndent = amt;
		}

		unsigned Writer::getIndentAmount() const
		{
			return mIndent;
		}
	}
}
