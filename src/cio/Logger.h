/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LOGGER_H
#define CIO_LOGGER_H

#include "Types.h"

#include "LogMessageStream.h"
#include "Severity.h"

#include <chrono>

#ifndef CIO_LOG_SEVERITY_NONE
#define CIO_LOG_SEVERITY_NONE 0
#endif

#ifndef CIO_LOG_SEVERITY_DEBUG
#define CIO_LOG_SEVERITY_DEBUG 1
#endif

#ifndef CIO_LOG_SEVERITY_INFO
#define CIO_LOG_SEVERITY_INFO 2
#endif

#ifndef CIO_LOG_SEVERITY_WARNING
#define CIO_LOG_SEVERITY_WARNING 3
#endif

#ifndef CIO_LOG_SEVERITY_ERROR
#define CIO_LOG_SEVERITY_ERROR 4
#endif

#ifndef CIO_LOG_SEVERITY_CRITICAL
#define CIO_LOG_SEVERITY_CRITICAL 5
#endif

#ifndef CIO_LOG_SEVERITY_FATAL
#define CIO_LOG_SEVERITY_FATAL 6
#endif

#ifndef CIO_LOG_SEVERITY_DISABLED
#define CIO_LOG_SEVERITY_DISABLED 7
#endif

// If we're not in a debug mode, and the developer has not specified a compile-time log level, default to compile time log severity of Severity::Info
// For debug builds, do not enable compile time log levels and defer purely to runtime choices
#if !defined CIO_LOG_SEVERITY
#if defined NDEBUG
#define CIO_LOG_SEVERITY CIO_LOG_SEVERITY_INFO
#endif
#endif

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Logger is a class that is used to convey information, typically to a console, file, or other output.
	 * This base class defines the core logger API and methods to perform logging that is conditional on runtime or compile severity thresholds.
	 * By default, in release builds all logging below Severity::Error level is disabled at compile time and should have no impact on performance
	 * while in debug builds all logging is enabled for runtime checking.
	 *
	 * If used directly, it logs output directly to std::clog.
	 */
	class CIO_API Logger
	{
		public:
#if defined CIO_LOG_SEVERITY
			/**
			 * The compile time severity threshold as an enum constant, taken from the CIO_LOG_SEVERITY macro.
			 */
			using Threshold = std::integral_constant<Severity, static_cast<Severity>(CIO_LOG_SEVERITY)>;
#else
			/**
			 * The compile time severity threshold as an enum constant, which is Severity::None since CIO_LOG_SEVERITY was not set.
			 */
			using Threshold = std::integral_constant<Severity, Severity::None>;
#endif
			/**
			 * Resource alias to obtain whether a particular logger severity level is enabled at compile time.
			 * The result type is either std::true_type or std::false_type indicating the result.
			 *
			 * @tparam <S> The compile time log severity level
			 */
			template <Severity S>
			using Enabled = std::integral_constant < bool, (S >= Logger::Threshold::value) >;

			/**
			 * Gets the metaclass for Logger.
			 *
			 * @return the metaclass for Logger
			 */
			static const Metaclass &getDeclaredMetaclass();

			/**
			 * Gets the global logger that can be used by anyone.
			 *
			 * @return the global logger
			 */
			static Logger *getGlobal();

			/**
			 * Sets the global logger that can be used by anyone.
			 *
			 * @param logger The global logger to use
			 */
			static void setGlobal(Logger *logger);

			/**
			 * Construct a logger with the default severity threshold and context.
			 * For debug builds, this is Severity::None.
			 * For release builds, this is Severity::Error.
			 * The default context is "Global".
			 */
			Logger();

			/**
			 * Construct a logger with the default severity threshold.
			 * For debug builds, this is Severity::None.
			 * For release builds, this is Severity::Error.
			 *
			 * @param context The logger context name, which will referenced (not owned) by the logger
			 */
			explicit Logger(const char *context);
			
			/**
			 * Construct a logger.
			 *
			 * @param context The logger context name, which will referenced (not owned) by the logger
			 * @param severity The severity threshold
			 */
			Logger(const char *context, Severity severity);
			
			/** 
			 * Constructs a logger with the context set to represent the given metaclass as a class logger.
			 *
			 * @param metaclass the metaclass to represent as the context
			 */
			explicit Logger(const Metaclass &metaclass);
			
			/** 
			 * Constructs a logger with the context set to represent the given metaclass as a class logger.
			 *
			 * @param metaclass the metaclass to represent as the context
			 * @param severity The severity threshold
			 */
			Logger(const Metaclass &metaclass, Severity severity);

			/**
			 * Destructor.
			 */
			virtual ~Logger() noexcept;

			/**
			 * Clears all state attached to this logger.
			 * In the base class, this resets the severity threshold to the default.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this object.
			 *
			 * @return the metaclass for this object
			 */
			virtual const Metaclass &getMetaclass() const noexcept;

			/**
			 * Logs a complete message to a logger with a context, severity, and message payload.
			 * The context may be different than this logger's context to use it with chained loggers, inheritance hierarchies, etc.
			 *
			 * @note This method bypasses all checks on logging severity being enabled, so only use it if you want to ensure the message is displayed
			 * regardless of user settings. 
			 *
			 * @param context The context (such as class name, test name, etc.)
			 * @param severity The severity of the message
			 * @param message The full message to log
			 * @return whether the message was logged
			 */
			virtual bool log(const char *context, Severity severity, const char *message);

			/**
			 * Logs a progress report of percentage of completion with a context, severity, and progress update.
			 * The base implementation formats a message of the form "Progress X / Y (ZZ.Z %)" if the progress total amount is known and nonzero
			 * or of the form "Progress X" otherwise.
			 *
			 * @param context The context (such as class name, test name, etc.)
			 * @param severity The severity of the message
			 * @param update The progress update
			 * @return whether the message was logged
			 */
			virtual bool reportProgressUpdate(const char *context, Severity severity, const Progress<Length> &update);
			
			/**
			 * Logs an elapsed time report with a context, severity, and time update.
			 * The base implementation formats a message of the form "Elapsed time X / Y (ZZ.Z %)" if the time total amount is known and nonzero
			 * or of the form "Elapsed X" otherwise.
			 *
			 * @param context The context (such as class name, test name, etc.)
			 * @param severity The severity of the message
			 * @param update The time update
			 * @return whether the message was logged
			 */
			virtual bool reportTimeUpdate(const char *context, Severity severity, const Progress<std::chrono::milliseconds> &update);

			/**
			 * Logs a particular message at a particular severity level if it passes the runtime severity threshold.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 *
			 * @param context The context (such as class name, test name, etc.)
			 * @param severity The severity of the message
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool logIfEnabled(const char *context, Severity severity, const char *text);

			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Debug level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			 */
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Debug>> debug();

			/**
			 * Logs a particular message at the Severity::Debug level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_DEBUG, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool debug(const char *text);

			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Info level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			 */
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Info>> info();

			/**
			 * Logs a particular message at the Severity::Info level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_INFO, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool info(const char *text);

			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Warning level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			 */
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Warning>> warning();

			/**
			 * Logs a particular message at the Severity::Warning level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_WARNING, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool warning(const char *text);

			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Error level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			 */
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Error>> error();

			/**
			 * Logs a particular message at the Severity::Error level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_ERROR, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool error(const char *text);

			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Critical level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			*/
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Critical>> critical();

			/**
			 * Logs a particular message at the Severity::Critical level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_CRITICAL, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool critical(const char *text);
			
			/**
			 * Logs a progress report as an info-level message with the current context.
			 *
			 * @param update The progress update
			 * @return whether the message was logged
			 */
			inline bool progress(const Progress<Length> &update);
			
			/**
			 * Logs an elapsed time report as an info-level message with the current context.
			 *
			 * @param update The time update
			 * @return whether the message was logged
			 */
			inline bool time(const Progress<std::chrono::milliseconds> &update);
			
			/**
			 * Initiates an incremental logging message for the given logger at the Severity::Fatal level checked at compile time.
			 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
			 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
			 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
			 *
			 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
			 * Otherwise, this has the same effect as the runtime-checked overload.
			 *
			 * @param logger The logger to log to
			 * @return the message builder object to use to incrementally build out the log message.
			*/
			inline LogMessageStream<Logger, Logger::Enabled<Severity::Fatal>> fatal();

			/**
			 * Logs a particular message at the Severity::Fatal level if it passes the compile time and runtime severity thresholds.
			 * If the threshold is met, this calls the virtual log method. Otherwise, nothing is done.
			 * If CIO_LOG_SEVERITY is defined at compile time and is greater than CIO_LOG_SEVERITY_FATAL, the method is conditionally compiled out to just return false.
			 * Otherwise, the runtime severity check is performed and the message is logged if it passes that.
			 *
			 * @param text The message to log if it meets the severity threshold
			 * @return whether the message is logged
			 */
			inline bool fatal(const char *text);

			/**
			 * Gets the default context that will be used for this logger.
			 *
			 * @return the default context
			 */
			inline const char *getContext() const;

			/**
			 * Sets the default context that will be used for this logger.
			 * This logger will reference (not own) the given context string.
			 *
			 * @param context The context for the logger
			 */
			inline void setContext(const char *context);
			
			/**
			 * Sets the default context that will be used for this logger to represent the class context represented by a metacalss.
			 *
			 * @param metaclass The metaclass to represent as the context for the logger
			 */
			void setContext(const Metaclass &metaclass);

			/**
			 * Gets the minimum severity level to log at runtime for this particular logger.
			 * If set to Severity::None, all severities should be logged.
			 * If set to Severity::Disable, no severities should be logged.
			 *
			 * @return the logger severity threshold
			 */
			inline Severity getMinimumSeverity() const;

			/**
			 * Sets the minimum severity level to log at runtime for this particular logger.
			 * If set to Severity::None, all severities should be logged.
			 * If set to Severity::Disable, no severities should be logged.
			 *
			 * @param severity the logger severity threshold
			 */
			inline void setMinimumSeverity(Severity severity);

			/**
			 * Checks whether the given severity level is enabled at runtime.
			 *
			 * @param severity The severity level
			 * @return whether the severity level is enabled
			 */
			inline bool isEnabled(Severity severity) const;

		private:
			/** The metaclass for this class */
			static const Class<Logger> sMetaclass;
		
			/** The default logger */
			static Logger sDefaultLogger;
		
			/** The pointer to the current global logger */
			static Logger *sGlobal;

			/** The default context to provide to logging methods when none is specified */
			const char *mContext;

			/** The minimum severity level to log at runtime for this particular logger */
			Severity mThreshold;
	};

	/**
	 * Initiates an incremental logging message for the given logger at the given severity level checked at runtime.
	 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
	 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
	 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
	 *
	 * @param logger The logger to log to
	 * @param severity The severity of the log message to initiate
	 * @return the message builder object to use to incrementally build out the log message.
	 */
	inline LogMessageStream<Logger> operator<<(Logger &logger, Severity severity);

	/**
	 * Initiates an incremental logging message for the given logger at the given severity level checked at compile time.
	 * If the given severity level passes the runtime log check, the log message may be incrementally constructed by more calls to operator<< with various items.
	 * The LogMessageStream object may also be captured into a variable for use across multiple lines.
	 * Once its destructor is called, or the log() method is explicitly called, the accumulated text will be logged to this logger.
	 *
	 * If the compile time severity check fails, the return log message builder is optimized into having no content and doing nothing.
	 * Otherwise, this has the same effect as the runtime-checked overload.
	 *
	 * @param logger The logger to log to
	 * @param severity The severity of the log message to initiate
	 * @return the message builder object to use to incrementally build out the log message.
	 */
	template <Severity S>
	inline LogMessageStream<Logger, Logger::Enabled<S>> operator<<(Logger &logger, std::integral_constant<Severity, S> severity);
}

/* Inline implementation */
namespace cio
{
	inline bool Logger::logIfEnabled(const char *context, Severity severity, const char *text)
	{
		bool matches = severity >= mThreshold;

		if (matches)
		{
			this->log(context, severity, text);
		}

		return matches;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Debug>> Logger::debug()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Debug>>(*this, mContext, Severity::Debug, Severity::Debug >= mThreshold);
	}

	inline bool Logger::debug(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_DEBUG >= CIO_LOG_SEVERITY
		logged = Severity::Debug >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Debug, text);
		}

#endif
		return logged;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Info>> Logger::info()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Info>>(*this, mContext, Severity::Info, Severity::Info >= mThreshold);
	}

	inline bool Logger::info(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_SEVERITY >= CIO_LOG_INFO
		logged = Severity::Info >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Info, text);
		}

#endif
		return logged;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Warning>> Logger::warning()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Warning>>(*this, mContext, Severity::Warning, Severity::Warning >= mThreshold);
	}

	inline bool Logger::warning(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_SEVERITY >= CIO_LOG_WARNING
		logged = Severity::Warning >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Warning, text);
		}

#endif
		return logged;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Error>> Logger::error()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Error>>(*this, mContext, Severity::Error, Severity::Error >= mThreshold);
	}

	inline bool Logger::error(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_SEVERITY >= CIO_LOG_ERROR
		logged = Severity::Error >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Error, text);
		}

#endif
		return logged;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Critical>> Logger::critical()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Critical>>(*this, mContext, Severity::Critical, Severity::Critical >= mThreshold);
	}

	inline bool Logger::critical(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_SEVERITY >= CIO_LOG_CRITICAL
		logged = Severity::Critical >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Critical, text);
		}

#endif
		return logged;
	}

	inline LogMessageStream<Logger, Logger::Enabled<Severity::Fatal>> Logger::fatal()
	{
		return LogMessageStream<Logger, Logger::Enabled<Severity::Fatal>>(*this, mContext, Severity::Fatal, Severity::Fatal >= mThreshold);
	}

	inline bool Logger::fatal(const char *text)
	{
		bool logged = false;
#if !defined CIO_LOG_SEVERITY || CIO_LOG_SEVERITY >= CIO_LOG_FATAL
		logged = Severity::Fatal >= mThreshold;

		if (logged)
		{
			this->log(mContext, Severity::Fatal, text);
		}

#endif
		return logged;
	}
	
	inline bool Logger::progress(const Progress<Length> &update)
	{
		return this->reportProgressUpdate(mContext, Severity::Info, update);
	}

	inline bool Logger::time(const Progress<std::chrono::milliseconds> &update)
	{
		return this->reportTimeUpdate(mContext, Severity::Info, update);
	}

	inline Severity Logger::getMinimumSeverity() const
	{
		return mThreshold;
	}

	inline void Logger::setMinimumSeverity(Severity severity)
	{
		mThreshold = severity;
	}

	inline bool Logger::isEnabled(Severity severity) const
	{
		return severity >= mThreshold;
	}

	inline const char *Logger::getContext() const
	{
		return mContext;
	}

	inline void Logger::setContext(const char *context)
	{
		mContext = context;
	}

	inline LogMessageStream<Logger> operator<<(Logger &logger, Severity severity)
	{
		return LogMessageStream<Logger>(logger, logger.getContext(), severity, logger.isEnabled(severity));
	}

	template <Severity S>
	inline LogMessageStream<Logger, Logger::Enabled<S>> operator<<(Logger &logger, std::integral_constant<Severity, S> severity)
	{
		return LogMessageStream<Logger, Logger::Enabled<S>>(logger, logger.getContext(), severity, logger.isEnabled(severity));
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
