/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_WRITER_H
#define CIO_XML_WRITER_H

#include "Types.h"

#include <cio/Text.h>
#include <cio/Output.h>

#include <sstream>
#include <string>
#include <stdexcept>
#include <vector>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		/**
		 * The XML Writer simplifies writing XML documents.  It makes it possible to write
		 * the logical content of the document without worrying about the XML syntax, entity conversions
		 * and escaping special characters.
		 */
		class CIO_XML_API Writer
		{
			public:
				/**
				 * Escapes the given string so that it is a legal attribute value.
				 * @param input The string to escape
				 * @param output The escaped string
				 */
				static void escapeAttributeValue(Text input, std::string &output);

				/**
				 * Indents a string by the given amount.
				 * @param str The string to indent
				 * @param amount The number of indent characters to indent by
				 */
				static void indent(std::string &str, std::size_t amount, char i = '\t');

				static void escapeText(Text input, std::string &output);

				/**
				 * Construct an XML writer.
				 */
				Writer();

				/**
				 * Construct an XML writer.
				 * @param stream The underlying output stream
				 */
				explicit Writer(Output *stream);

				/**
				 * Changes the output stream for this XML writer.
				 * @param stream The new output stream
				 */
				void setOutput(Output *stream);

				/**
				 * Gets the currently used output stream.
				 * @return the current output stream
				 */
				Output *getOutput();

				/**
				 * Writes the XML header information.  This currently sets the XML revision
				 * to 1.0 and the character set to UTF-8.
				 * @return this writer
				 */
				Writer &writeHeader();

				/**
				 * Start a new element in the document.
				 * @param element The element to start
				 * @return this writer
				 */
				Writer &startElement(Text element);

				/**
				 * Writes an XML comment in the document.
				 * @param comment The text of the comment
				 * @return this writer
				 */
				Writer &writeComment(Text comment);

				/**
				 * Add an attribute to the current element.  This is only valid
				 * if no text has been written after the most recent call to start element.
				 * @param attr The attribute name
				 * @param value The attribute value, which will be properly escaped by this method
				 */
				Writer &writeAttribute(Text attr, Text value);

				/**
				 * Add an attribute to the current element.  This is only valid
				 * if no text has been written after the most recent call to start element.
				 * @param attr The attribute name
				 * @param value The attribute value, which will be converted to text and properly escaped by this method
				 */
				template <class T>
				Writer &writeAttribute(Text attr, const T &value)
				{
					Text text = cio::print(value);
					return writeAttribute(attr, text);
				}

				/**
				* Writes a free-form text section provided by a character array, not necessarily
				* NULL-terminated.  This escapes a few of the more common
				* characters that are interpreted specially by XML into their entity equivalents.
				*
				* @param text The text to write out
				* @param len The number of characters in the text
				* @return this writer
				*/
				Writer &writeText(Text text);

				/**
				* Writes the string representation of an object as a text section.
				* @param object The object to write
				* @return this XML writer
				*/
				template <class T>
				Writer &writeText(const T &object)
				{
					Text text = cio::print(object);
					return writeText(text);
				}

				/**
				 * End the current element.  Only valid after calling start element.
				 * @return this writer
				 */
				Writer &endElement();

				void setIndent(char c, unsigned amount);

				void setIndentChar(char c);

				char getIndentChar() const;

				/**
				* Sets the indentation amount.  Each level of indent will be this amount.
				* @param amt The number of indent characters per level
				*/
				void setIndentAmount(unsigned amt);

				unsigned getIndentAmount() const;

			private:
				Output *mOutput;
				std::vector<std::string> mElements;
				unsigned mIndent;
				char mIndentChar;
				bool mInsideElementOpen;
				bool mAfterText;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
