/*==============================================================================
 * Copyright 2023-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "JsonWriter.h"

#include "Newline.h"
#include "Type.h"
#include "Print.h"

#include <cctype>

namespace cio
{
	JsonWriter::JsonWriter(char *storage, std::size_t capacity) :
		MarkupWriter(Buffer(storage, capacity))
	{
		// nothing more to do
	}

	JsonWriter::JsonWriter(Pointer<Output> stream) :
		MarkupWriter(std::move(stream))
	{
		// do nothing
	}

	JsonWriter::JsonWriter(Buffer buffer, Pointer<Output> stream) :
		MarkupWriter(std::move(buffer), std::move(stream))
	{
		// do nothing
	}

	JsonWriter::~JsonWriter() noexcept = default;

	State JsonWriter::startDocument()
	{
		State state = cio::MarkupWriter::startDocument();

		// Write leading { to start the document
		if (state.succeeded())
		{
			this->write('{');
		}

		return state;
	}

	State JsonWriter::endDocument()
	{
		State state = cio::MarkupWriter::endDocument();

		if (state.succeeded())
		{
			this->separate();
			this->write('}');
		}

		return state;
	}

	State JsonWriter::startElement(Text element)
	{
		// Attempt to start the element. If successful, the element key moves into the level's markup event.
		State state = cio::MarkupWriter::startElement(std::move(element));
		if (state.succeeded())
		{
			MarkupEvent &level = this->getCurrentLevel(); // We just created this
			MarkupEvent &parent = this->getParentLevel(); // Parent element, document, or root

			// If this is the first parent element, we need to start it
			if (parent.count <= 1)
			{
				// Go to new line if we're starting the first element in a array or document
				if (parent.markup == Markup::Document)
				{
					this->separate();
				}
				else if (parent.markup == Markup::Array)
				{
					this->separate();
				}
				// Only write leading '{' if the parent is an Element
				else if (parent.markup == Markup::Element)
				{
					this->write('{');
					this->separate();
				}
			}
			// Currently started element has at least one structured value already, so we need to separate with ','
			else
			{
				this->write(',');
				this->separate();
			}

			++parent.count;

			// Write out indent, then element key name
			this->indent();
			if (parent.markup != Markup::Array)
			{
				this->write('"');
				this->writeQuotedString(level.key);
				this->write("\": ", 3);
			}
		}

		return state;
	}

	State JsonWriter::startAttribute(Text key)
	{
		// Attempt to start the attribute. If successful, the element key moves into the level's markup event.
		State state = cio::MarkupWriter::startAttribute(std::move(key));
		if (state.succeeded())
		{
			// Base class does not adjust indent for attributes, but we need to
			++mDepth;

			MarkupEvent &level = this->getCurrentLevel(); // We just created this
			MarkupEvent &parent = this->getParentLevel(); // Parent element

			if (parent.key != "")
			{
				// Currently started element does not yet have any values, so write out the initial {
				if (parent.count <= 1)
				{
					this->write('{');
				}
				// Currently started element has at least one structured value already
				else
				{
					this->write(',');
				}

				this->separate();

				// Write out indent, then element key name
				this->indent();
			}
			this->write('"');
			this->writeQuotedString(level.key);
			this->write("\": ", 3);
		}

		return state;
	}

	State JsonWriter::endAttribute()
	{
		// We need to know the current level type to decide whether to close it
		// Unlike elements, attributes can only be the current level
		MarkupEvent &level = this->getCurrentLevel();
		Type type = level.type;
		std::size_t count = level.count;

		// Let base class tell us if we can proceed
		State state = cio::MarkupWriter::endAttribute();
		if (state.succeeded())
		{
			// No value ever written, write out JSON null
			if (count == 0)
			{
				this->write("null", 4);
			}

			if (type == Type::Any)
			{
				this->separate();
				this->indent();
				this->write('}');
			}

			// Base class does not adjust depth for attributes, but we need to
			--mDepth;
		}

		return state;
	}

	State JsonWriter::endElement()
	{
		// We need to know the current level type to decide whether to close it
		// However, the element might be some levels up if we have attributes, comments, etc.
		MarkupEvent &level = this->getCurrentLevel(Markup::Element);
		Type type = level.type;
		std::size_t count = level.count;

		// Let base class end intervening elements and tell us if we can proceed
		State state = cio::MarkupWriter::endElement();
		if (state.succeeded())
		{
			// Base class takes care of writing null for empty value
			if (type == Type::Object)
			{
				this->separate();
				this->indent(1); // base class has already decreased indent
				this->write('}');
			}
		}

		return state;
	}

	State JsonWriter::startValue(Type type)
	{
		State state = MarkupWriter::startValue(type);
		if (state.succeeded())
		{
			MarkupEvent &parent = this->getParentLevel();
			if (parent.markup != Markup::Element)
			{
				if (parent.count > 1)
				{
					this->write(',');
					this->separate();
					this->indent();
				}
				else if (parent.markup == Markup::Array)
				{
					this->separate();
					this->indent();
				}
			}
			
			if (parent.count > 1 && parent.markup == Markup::Element)
			{
				state.fail(Reason::Unexpected);
			}
			else
			{
				MarkupEvent &level = this->getCurrentLevel();
				if ((level.type == Type::Text || level.type == Type::Blob) && level.action == MarkupAction::Start)
				{
					this->write('"');
					level.action = MarkupAction::Continue;
				}
				else if (level.action != MarkupAction::Start)
				{
					state.fail(Reason::Unexpected);
				}
			}
		}

		return state;
	}

	State JsonWriter::endValue()
	{
		MarkupEvent &level = this->getCurrentLevel();
		Type type = level.type;
		MarkupAction action = level.action;

		State state = MarkupWriter::endValue();
		if (state.succeeded())
		{
			if ((type == Type::Text || type == Type::Blob) && action == MarkupAction::Continue)
			{
				this->write('"');
			}
		}

		return state;
	}

	void JsonWriter::writeQuotedString(Text value)
	{
		if (value.size() == 0)
		{
			this->write("");
		}
		else
		{
			// This method is optimized for the common case that most of the string doesn't need escapes
			// Process until we find something we can't directly print
			std::size_t length = value.strlen();
			std::size_t current = 0;
			std::size_t written = 0;
			char escaped[8] = { };

			while (current < length)
			{
				std::size_t escape = this->printEscape(value[current], escaped, 8);
				if (escape > 1)
				{
					this->write(value.data() + written, current - written);
					this->write(escaped, escape);
					written = current + 1;
				}

				++current;
			}

			this->write(value.data() + written, length - written);
		}
	}

	State JsonWriter::writeValue(const Text &value)
	{
		State state;

		// Distinguish between null (size = 0) and empty (size = 1 with null terminator)
		if (value.null())
		{
			state = this->writeEmptyValue();
		}
		else
		{
			state = this->startValue(Type::Text);

			if (state.succeeded())
			{
				this->writeQuotedString(value);
				this->endValue();
			}
		}

		return state;
	}

	State JsonWriter::writeValue(float value)
	{
		State state = this->startValue(Type::Real);

		if (state.succeeded())
		{
			if (std::isfinite(value))
			{
				this->write(value);
			}
			// JSON has no representation for infinity or NaN
			else
			{
				this->write("null", 4);
				state.fail(Reason::Unsupported);
			}

			this->endValue();
		}

		return state;
	}

	State JsonWriter::writeValue(double value)
	{
		State state = this->startValue(Type::Real);

		if (state.succeeded())
		{
			if (std::isfinite(value))
			{
				this->write(value);
			}
			// JSON has no representation for infinity or NaN so these will fail
			// ending the value will then result in "null" being written
			else
			{
				this->write("null", 4);
				state.fail(Reason::Unsupported);
			}

			this->endValue();
		}

		return state;
	}

	State JsonWriter::writeValue(const void *bytes, std::size_t length)
	{
		State state = this->startValue(Type::Blob);

		if (state.succeeded())
		{
			// Distinguish between null (bytes is null) and zero length
			if (!bytes)
			{
				state = this->writeEmptyValue();
			}
			else
			{
				if (length > 0)
				{
					std::string printed = cio::printBytes(bytes, length);
					this->write(printed);
				}
				state.succeed();
			}

			this->endValue();
		}

		return state;
	}

	State JsonWriter::writeEmptyValue()
	{
		State state = this->startValue(Type::None);
		if (state.succeeded())
		{
			MarkupEvent &level = this->getCurrentLevel();
			if (level.count == 0)
			{
				this->write("null", 4);
				level.count += 1;
			}
			this->endValue();
		}
		return state;
	}

	State JsonWriter::startComment()
	{
		State state = MarkupWriter::startComment();
		if (state.succeeded())
		{
			mFilterWrites = true;
		}
		return state;
	}

	State JsonWriter::endComment()
	{
		State state = MarkupWriter::endComment();
		if (state.succeeded())
		{
			mFilterWrites = false;
		}
		return state;
	}

	State JsonWriter::startInstruction(Text key)
	{
		State state = MarkupWriter::startInstruction(std::move(key));
		if (state.succeeded())
		{
			mFilterWrites = true;
		}
		return state;
	}

	State JsonWriter::endInstruction()
	{
		State state = MarkupWriter::endInstruction();
		if (state.succeeded())
		{
			mFilterWrites = false;
		}
		return state;
	}

	State JsonWriter::startArray(std::size_t count, Type type)
	{
		State state = MarkupWriter::startArray(count, type);
		if (state.succeeded())
		{
			MarkupEvent &parent = this->getParentLevel();

			if (parent.count > 1)
			{
				this->write(',');
				this->separate();
				this->indent();
			}

			this->write('[');
		}
		return state;
	}

	State JsonWriter::endArray()
	{
		State state = MarkupWriter::endArray();
		if (state.succeeded())
		{
			this->separate();

			this->indent(1); // base class already removed indent
			this->write(']');
		}

		return state;
	}

	std::size_t JsonWriter::needsEscape(char c) noexcept
	{
		std::size_t count = 0;

		switch (c)
		{
			case '"':
			case '\n':
			case '\r':
			case '\\':
				count = 2;
				break;

			default:
				count = (static_cast<std::uint8_t>(c) >= 128 || std::isprint(c)) ? 1 : 6;
				break;
		}

		return count;
	}

	std::size_t JsonWriter::printEscape(char c, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t count = 0;

		switch (c)
		{
			case '"':
			{
				reprint("\\\"", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\b':
			{
				reprint("\\b", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\n':
			{
				reprint("\\n", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\f':
			{
				reprint("\\f", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\r':
			{
				reprint("\\r", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\t':
			{
				reprint("\\t", 2, buffer, capacity);
				count = 2;
				break;
			}

			case '\\':
			{
				reprint("\\\\", 2, buffer, capacity);
				count = 2;
				break;
			}

			default:
			{
				// JSON is inherently Unicode, so we only need Unicode escapes for non-printable characters
				// We also pass through non-ASCII characters since these must be checked at a higher level
				if (static_cast<std::uint8_t>(c) >= 128 || std::isprint(c))
				{
					if (buffer && capacity > 0)
					{
						buffer[0] = c;
					}
					count = 1;
				}
				else
				{
					char temp[] = { '\\', 'u', '0', '0', printHexDigit(c >> 4), printHexDigit(c), 0 };
					reprint(temp, 6, buffer, capacity);
					count = 6;
				}

				break;
			}
		}

		return count;
	}
}
