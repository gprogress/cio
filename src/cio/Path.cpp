/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Path.h"

#include "Class.h"
#include "Parse.h"

#include <algorithm>
#include <cctype>
#include <ostream>
#include <vector>

#define IS_LETTER(x)		((((x) >= 'A') && ((x) <= 'Z')) || (((x) >= 'a') && ((x) <= 'z')))
#define IS_URI_OK_PUNCT(x)	(((x) == '-') || ((x) == '.') || ((x) == '_') || ((x) == '~'))
#define IS_NUMBER(x)		(((x) >= '0') && ((x) <= '9'))
#define IS_HEX_LETTER(x)	((((x) >= 'A') && ((x) <= 'F')) || (((x) >= 'a') && ((x) <= 'f')))
#define IS_HEX(x)			(IS_NUMBER(x) || IS_HEX_LETTER(x))
#define IS_URI_OK(x)		(IS_LETTER(x) || IS_URI_OK_PUNCT(x) || IS_NUMBER(x))
#define GET_HEX(x)			(x < 10) ? (48 + x) : (55 + x)

namespace cio
{
	Class<Path> Path::sMetaclass("cio::Path");
	
	const Class<Path> &Path::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	std::size_t Path::hasNativeRoot(const char *path, std::size_t length)  noexcept
	{
#if defined _WIN32
		return hasWindowsRoot(path, length);
#else
		return hasUnixRoot(path, length);
#endif
	}

	std::size_t Path::hasWindowsRoot(const char *path, std::size_t length) noexcept
	{
		std::size_t rootLength = 0;

		if (path && length >= 3 && path[0] && path[1] && path[2])
		{
			if (std::isalpha(path[0]) && path[1] == ':' && (path[2] == '/' || path[2] == '\\'))
			{
				rootLength = 3;
			}
			else if (path[0] == '/' || path[0] == '\\')
			{
				rootLength = 1 + Path::hasWindowsRoot(path + 1, length - 1);
			}
		}

		return rootLength;
	}


	bool Path::hasUnixRoot(const char *path, std::size_t length) noexcept
	{
		return path && length > 0 && *path == '/';
	}

	Path Path::fromNativeFile(const char *path, std::size_t len)
	{
#if defined _WIN32
		return fromWindowsFile(path, len);
#else
		return fromUnixFile(path, len);
#endif
	}

	Path Path::fromNativeFile(const char *path)
	{
		Path resource;

		if (path)
		{
			std::size_t len = std::strlen(path);
			resource = fromNativeFile(path, len);
		}

		return resource;
	}

	Path Path::fromNativeFile(const std::string &path)
	{
		return fromNativeFile(path.c_str(), path.size());
	}

	Path Path::fromWindowsFile(const char *path, std::size_t len)
	{
		Path resource;

		if (path && len)
		{
			std::string normalized;
			std::size_t offset = 0;

			if (Path::hasWindowsRoot(path, len))
			{
				const char prefix[] = "file:///";
				normalized.resize(8 + len);
				std::copy(prefix, prefix + 8, normalized.begin());
				offset = 8;
			}
			else
			{
				normalized.resize(len);
			}

			const char *cur = path;
			const char *end = path + len;

			// Skip a leading '/' since we already got it in the protocol section
			if (*cur == '/' || *cur == '\\')
			{
				++cur;
			}

			while (cur < end)
			{
				char c = *cur++;

				if (c == '\\')
				{
					c = '/';
				}

				normalized[offset++] = c;
			}

			resource.set(std::move(normalized));
		}

		return resource;
	}

	Path Path::fromWindowsFile(const char *path)
	{
		Path resource;

		if (path)
		{
			std::size_t len = std::strlen(path);
			resource = fromWindowsFile(path, len);
		}

		return resource;
	}

	Path Path::fromWindowsFile(const std::string &path)
	{
		return fromWindowsFile(path.c_str(), path.size());
	}

	Path Path::fromUnixFile(const char *path, std::size_t len)
	{
		Path resource;

		if (path && len)
		{
			std::string normalized;
			std::size_t offset = 0;

			if (Path::hasNativeRoot(path, len))
			{
				const char prefix[] = "file://";
				normalized.resize(7 + len);
				std::copy(prefix, prefix + 7, normalized.begin());
				offset = 7;
			}
			else
			{
				normalized.resize(len);
			}

			std::copy(path, path + len, normalized.begin() + offset);
			resource.set(std::move(normalized));
		}

		return resource;
	}

	Path Path::fromUnixFile(const char *path)
	{
		Path resource;

		if (path)
		{
			std::size_t len = std::strlen(path);
			resource = fromUnixFile(path, len);
		}

		return resource;
	}

	Path Path::fromUnixFile(const std::string &path)
	{
		return fromUnixFile(path.c_str(), path.size());
	}

	char Path::getNativeSeparator() noexcept
	{
#if defined CIO_HAVE_WINDOWS_H
		return '\\';
#else
		return '/';
#endif
	}

	bool Path::isNativeSeparator(char c) noexcept
	{
#if defined CIO_HAVE_WINDOWS_H
		return c == '\\' || c == '/';
#else
		return c == '/';
#endif
	}

	bool Path::encode(std::string &text)
	{
		bool success = true;

		std::size_t numberEscapes = 0;
		// Count number of characters will need to escape
		for (char &c : text)
		{
			if (!IS_URI_OK(c))
			{
				++numberEscapes;
			}
		}

		if (numberEscapes > 0)
		{
			std::size_t originalTextSize = text.size();
			std::size_t writeShift = numberEscapes * 2;
			text.resize(originalTextSize + writeShift);

			for (std::size_t readIndex = originalTextSize; readIndex > 0; --readIndex)
			{
				char &c = text[readIndex - 1];
				if (!IS_URI_OK(c))
				{
					char rightNibble = c & 0x0F;
					char leftNibble = c >> 4;

					text[readIndex + writeShift - 1] = GET_HEX(rightNibble);
					text[readIndex + writeShift - 2] = GET_HEX(leftNibble);

					writeShift -= 2;
					c = '%';
				}
				text[readIndex - 1 + writeShift] = c;
			}
		}

		return success;
	}

	bool Path::decode(std::string &text)
	{
		std::size_t textSize = text.size();

		bool decodable = true;
		for (std::size_t readIndex = 0; readIndex < textSize; ++readIndex)
		{
			char &c = text[readIndex];
			if (c == '%')
			{
				if (readIndex + 2 < textSize && IS_HEX(text[readIndex + 1]) 
					&& IS_HEX(text[readIndex + 2]) 
					&& cio::parseNibble(text[readIndex + 1]) < 8)
				{
					readIndex += 2;
				}
				else
				{
					decodable = false;
					break;
				}
			}
		}

		if (decodable)
		{
			std::size_t writeIndex = 0;
			for (std::size_t readIndex = 0; readIndex < textSize; ++readIndex)
			{
				char &c = text[readIndex];
				if (c == '%')
				{
					c = (cio::parseNibble(text[readIndex + 1]) << 4) 
						| (parseNibble(text[readIndex + 2]) & 0x0F);
					text[writeIndex++] = c;
					readIndex += 2;
				}
				else
				{
					text[writeIndex++] = c;
				}
			}

			if (writeIndex < textSize)
			{
				text.resize(writeIndex);
			}
		}
		return decodable;
	}

	std::string Path::validateNativeInputText(std::string text)
	{
		std::string output;

		if (!text.empty())
		{
			Path::decode(text);

			std::size_t windowsRoot = Path::hasWindowsRoot(text.c_str(), text.size());
			std::size_t unixRoot = text.front() == '/';

			if (windowsRoot || unixRoot)
			{
				if (windowsRoot)
				{
					output = "file:///" + text;
				}
				else
				{
					output = "file://" + text;
				}

				for (char &c : output)
				{
					if (c == '\\')
					{
						c = '/';
					}
				}
			}
			else
			{
				output = std::move(text);
			}
		}

		return output;
	}

	Path::Path() noexcept
	{
		// nothing more to do
	}

	Path::Path(const Text &path) :
		mText(path.str())
	{
		this->validateNativeInput();
	}

	Path::Path(const char *path) :
		mText(path)
	{
		this->validateNativeInput();
	}

	Path::Path(const std::string &path) :
		mText(path)
	{
		this->validateNativeInput();
	}

	Path::Path(std::string &&path) noexcept :
		mText(std::move(path))
	{
		this->validateNativeInput();
	}

	Path::Path(const Path &/* in */) = default;

	Path::Path(Path &&in) noexcept :
		mText(std::move(in.mText))
	{
		// nothing more to add
	}

	void Path::set(const Path &parent, const Text &child)
	{
		if (child)
		{
			std::size_t parlen = parent.size();
			std::size_t childlen = child.strlen();

			// If the parent has a query fragment, this is lost when appending a child
			Text parentQuery = parent.getQuery();
			if (!parentQuery.empty())
			{
				parlen = (parentQuery.data() - parent.mText.data());
			}

			bool separate = ((parlen == 0) || (parent[parlen - 1] != '/'));
			std::size_t needed = parent.size() + childlen + separate;
			mText.resize(needed);

			if (parlen > 0)
			{
				std::copy(parent.mText.begin(), parent.mText.begin() + parlen, mText.begin());
			}

			if (separate)
			{
				mText[parlen] = '/';
				++parlen;
			}

			std::copy(child.begin(), child.begin() + childlen, mText.begin() + parlen);
		}
		else
		{
			mText = parent.mText;
		}
	}

	void Path::set(const Path &parent, const Path &child)
	{
		this->set(parent, Text::view(child.mText));
	}

	Path &Path::operator=(const Path &/* in */) = default;

	Path &Path::operator=(Path &&in) noexcept
	{
		mText = std::move(in.mText);
		return *this;
	}

	Path::~Path()  noexcept
	{
		// nothing more to do
	}

	void Path::clear() noexcept
	{
		mText.clear();
	}
	
	Path &Path::merge(const Path &path)
	{
		if (mText.empty())
		{
			mText = path.mText;
		}
		
		return *this;
	}
			
	Path &Path::merge(Path &&path)
	{
		if (mText.empty())
		{
			mText = std::move(path.mText);
		}
		
		return *this;
	}

	bool Path::hasProtocol() const noexcept
	{
		return !this->getProtocol().empty();
	}
			
	Text Path::getProtocol() const noexcept
	{
		Text protocol;

		if (mText.size() > 4)
		{
			std::size_t protoDelim = mText.find(':');
			std::size_t protoEnd = mText.find('/');

			while (protoDelim != std::string::npos)
			{
				if (protoEnd == std::string::npos || protoEnd <= protoDelim)
				{
					// no valid protocol string, just return empty
					break;
				}

				if (protoEnd == protoDelim + 1)
				{
					if (protoDelim + 2 < mText.size() && mText[protoDelim + 2] == '/')
					{
						protocol = Text(mText.c_str(), protoDelim);
					}

					break;
				}

				protoDelim = mText.find(':', protoEnd + 1);
				protoEnd = mText.find('/', protoEnd + 1);
			}
		}

		return protocol;
	}

	void Path::setProtocol(const Text &protocol)
	{
		if (protocol.empty())
		{
			return;
		}

		Text current = this->getProtocol();

		if (current != protocol)
		{
			std::size_t protocolSize = protocol.size();
			if (current.size() != protocolSize)
			{
				std::string changed;

				// Have an existing protocol
				if (protocol)
				{
					// Rewriting protocol
					if (current)
					{
						changed.resize(mText.size() - current.size() + protocolSize);
						std::copy(mText.begin() + current.size(), mText.end(), changed.begin() + protocolSize);
					}
					// Adding protocol, must add :// too
					else
					{
						changed.resize(mText.size() + protocolSize + 3);
						changed[protocolSize] = ':';
						changed[protocolSize + 1] = '/';
						changed[protocolSize + 2] = '/';
						std::copy(mText.begin(), mText.end(), changed.begin() + protocolSize + 3);
					}
				}
				// Remove protocol, strip out :// too
				else
				{
					changed.resize(mText.size() - current.size() - 3);
					std::copy(mText.begin() + current.size() + 3, mText.end(), changed.begin());
				}

				mText = std::move(changed);
			}

			// Actually finally copy the protocol if the sizes match up
			if (protocol)
			{
				std::copy(protocol.begin(), protocol.end(), mText.begin());
			}
		}
	}

	Text Path::getRoot() const noexcept
	{
		Text root;
		Text protocol = this->getProtocol();
		
		std::size_t rootStart = 0;
		std::size_t rootLength = 0;

		// If we have a valid protocol, assume the root is properly prefixed and encoded
		if (protocol)
		{
			// Skip the ://
			rootStart = protocol.size() + 3;
			std::size_t delim = mText.find('/', rootStart);

			if (delim != std::string::npos)
			{
				rootLength = delim + 1 - rootStart;
			}
			else
			{
				rootLength = mText.size() - rootStart;
			}

			// If it's the file protocol, additional check for the Windows drive letter
			if (protocol == "file")
			{
				rootLength = Path::hasWindowsRoot(mText.data() + rootStart, mText.size() - rootStart);
			}
		}
		// If we do not have a protocol, check for native file root (Windows is a superset of Unix here)
		else
		{
			rootLength = Path::hasWindowsRoot(mText.c_str(), mText.size());
		}
		
		if (rootLength)
		{
			root = Text(mText.data() + rootStart, rootLength);
		}

		return root;
	}
	
	void Path::setRoot(const Text &text)
	{
		Text current = this->getRoot();
		if (current)
		{
			std::string replacement;
			replacement.append(mText.data(), current.data());
			replacement.append(text.data(), text.data() + text.strlen());
			if (current[0] == '/')
			{
				replacement.append(current.data() + 1, mText.data() + mText.size());
			}
			else
			{
				replacement.append(current.data(), mText.data() + mText.size());
			}
			mText = std::move(replacement);
		}
		else
		{
			Text protocol = this->getProtocol();
			if (protocol)
			{
				std::string replacement;
				replacement.append(protocol.data(), protocol.data() + protocol.size());
				replacement.append(text.data(), text.data() + text.strlen());
				replacement.append(protocol.data() + protocol.size(), mText.data() + mText.size());
				mText = std::move(replacement);
			}
			else
			{
				std::string replacement;
				replacement.append(text.data(), text.data() + text.strlen());
				replacement.append(mText.data(), mText.data() + mText.size());
				mText = std::move(replacement);
			}
		}
	}

	Text Path::getPath() const noexcept
	{
		Text protocol = this->getProtocol();
		std::size_t offset = 0;

		if (!protocol.empty())
		{
			// If protocol was found, we know we have at least a ":/" following it
			offset = protocol.size() + 3;
		}

		std::size_t queryStart = mText.find('?', offset + 1);

		if (queryStart == std::string::npos)
		{
			queryStart = mText.size() + 1; // include null terminator
		}

		return Text(mText.c_str() + offset, queryStart - offset);
	}
	
	void Path::setPath(const Text &text)
	{
		Text current = this->getPath();
		if (current)
		{
			std::string replacement;
			replacement.append(mText.data(), current.data());
			replacement.append(text.data(), text.data() + text.strlen());
			replacement.append(current.data() + current.strlen(), mText.data() + mText.size());
			mText = std::move(replacement);
		}
		else
		{
			mText.append(text.data(), text.data() + text.strlen());
		}	
	}

	bool Path::hasExtension() const noexcept
	{
		Text filename = this->getFilename();
		return filename.rfind('.') != std::string::npos;
	}

	Text Path::getExtension() const noexcept
	{
		Text ext;
		Text filename = this->getFilename();
		std::size_t pos = filename.rfind('.');
		if (pos != std::string::npos)
		{
			ext = filename.suffix(pos + 1);
		}
		return ext;
	}

	Text Path::getQuery() const noexcept
	{
		Text query;
		std::size_t queryEnd = mText.rfind('?');

		if (queryEnd != std::string::npos)
		{
			query = Text(mText.c_str() + queryEnd + 1, mText.size() - queryEnd - 1);
		}

		return query;
	}

	Text Path::getParent() const noexcept
	{
		Text parent;
		Text filename = this->getFilename();

		if (filename && filename.data() > mText.data())
		{
			parent = Text(mText.data(), filename.data() - mText.data() - 1);
		}

		return parent;
	}

	void Path::set(const std::string &path)
	{
		mText = path;
		this->validateNativeInput();
	}

	void Path::set(std::string &&path) noexcept
	{
		mText = std::move(path);
		this->validateNativeInput();
	}

	void Path::set(const char *path)
	{
		mText = path;
		this->validateNativeInput();
	}

	void Path::set(const Text &path)
	{
		mText.assign(path.begin(), path.end());
		this->validateNativeInput();
	}

	bool Path::isFileProtocol() const noexcept
	{
		Text protocol = this->getProtocol();
		return (protocol.empty() && Path::hasWindowsRoot(mText.data(), mText.size())) || protocol == "file";
	}
	
	bool Path::matchesExtension(const char *ext) const noexcept
	{
		bool matches = false;
		
		if (ext && *ext)
		{
			const char *search = ext + (*ext == '.');
			if (*search == '*')
			{
				matches = true;
			}
			else
			{
				Text actualExt = this->getExtension();
				const char *wildcard = std::strchr(search, '*');
				if (wildcard)
				{
					matches = (actualExt.comparePrefixInsensitive(search, wildcard - search) == 0);
				}
				else
				{
					matches = actualExt.equalInsensitive(search);
				}
			}
		}
		else
		{
			matches = this->getExtension().empty();
		}

		return matches;
	}
			
	bool Path::matchesExtension(const std::string &ext) const noexcept
	{
		bool matches = false;
		
		if (!ext.empty())
		{
			const char *search = ext.c_str() + (ext.front() == '.');
			
			if (*search == '*')
			{
				matches = true;
			}
			else
			{
				Text actualExt = this->getExtension();
				std::size_t wildcard = ext.find('*');
				if (wildcard != std::string::npos)
				{
					matches = (actualExt.comparePrefixInsensitive(search, wildcard) == 0);
				}
				else
				{
					
					matches = actualExt.equalInsensitive(search);
				}
			}
		}
		else
		{
			matches = this->getExtension().empty();
		}

		return matches;
	}
			
	bool Path::matchesExtension(const Text &ext) const noexcept
	{
		bool matches = false;

		if (ext)
		{
			
			Text search = ext.trimPrefix('.');
			if (search[0] == '*')
			{
				matches = true;
			}
			else
			{
				Text actualExt = this->getExtension();
				std::size_t wildcard = ext.find('*');
				if (wildcard != std::string::npos)
				{
					matches = (actualExt.comparePrefixInsensitive(search, wildcard) == 0);
				}
				else
				{
					
					matches = actualExt.equalInsensitive(search);
				}
			}
		}
		else
		{
			matches = this->getExtension().empty();
		}

		return matches;
	}

	void Path::setExtension(const Text &ext)
	{
		Text trimmed = ext.trimPrefix('.', 1);
		Text currentExt = this->getExtension();
		Text query = this->getQuery();

		if (currentExt)
		{
			std::size_t offset = currentExt.data() - mText.data();
			if (!trimmed)
			{
				--offset;
			}

			std::string changed;

			if (query)
			{
				changed.resize(offset + trimmed.size() + query.size() + 1);
				std::copy(mText.begin(), mText.begin() + offset, changed.begin());
				std::copy(trimmed.begin(), trimmed.end(), changed.begin() + offset);
				changed[offset + trimmed.size()] = '?';
				std::copy(query.begin(), query.end(), changed.begin() + offset + trimmed.size() + 1);
			}
			else
			{
				changed.resize(offset + trimmed.size());
				std::copy(mText.begin(), mText.begin() + offset, changed.begin());
				std::copy(trimmed.begin(), trimmed.end(), changed.begin() + offset);
			}

			mText = std::move(changed);
		}
		else if (query)
		{
			std::size_t offset = query.data() - mText.data() - 1;
			std::string changed;
			changed.resize(offset + trimmed.size() + query.size() + 2);
			std::copy(mText.begin(), mText.begin() + offset, changed.begin());
			changed[offset] = '.';
			std::copy(trimmed.begin(), trimmed.end(), changed.begin() + offset + 1);
			changed[offset + trimmed.size() + 1] = '?';
			std::copy(query.begin(), query.end(), changed.begin() + offset + trimmed.size() + 2);
			mText = std::move(changed);
		}
		else
		{
			std::size_t offset = mText.size();
			mText.resize(mText.size() + trimmed.size() + 1);
			mText[offset] = '.';
			std::copy(trimmed.begin(), trimmed.end(), mText.begin() + offset + 1);
		}
	}

	void Path::removeExtension()
	{
		Text currentExt = this->getExtension();

		if (currentExt && mText.size() > (currentExt.size()) + 1)
		{
			std::size_t offset = currentExt.data() - mText.data() - 1;
			Text query = this->getQuery();

			if (query)
			{
				std::string changed;
				changed.resize(offset + query.size() + 1);
				std::copy(mText.begin(), mText.begin() + offset, changed.begin());
				changed[offset] = '?';
				std::copy(query.begin(), query.end(), changed.begin() + offset + 1);
				mText = std::move(changed);
			}
			else
			{
				mText.resize(offset);
			}
		}
	}

	bool Path::matchesProtocol(const Text &protocol) const noexcept
	{
		bool matches = true;

		if (protocol)
		{
			Text actual = this->getProtocol();

			if (actual)
			{
				matches = (protocol == actual);
			}
		}

		return matches;
	}

	bool Path::isRelative() const noexcept
	{
		return !mText.empty() && this->getRoot().empty();
	}

	bool Path::isComplete() const noexcept
	{
		Text root = this->getRoot();
		return !root.empty();
	}

	bool Path::isRoot() const noexcept
	{
		bool isRoot = false;
		Text root = this->getRoot();

		if (!root.empty())
		{
			Text fullpath = this->getPath();
			isRoot = (fullpath == root);
		}

		return isRoot;
	}

	bool Path::normalize()
	{
		bool changed = false;

		// We're only concerned with the path portion
		Text path = this->getPath();

		// See if we need to do anything before allocating memory
		std::size_t split = path.find("/.");
		if (split != std::string::npos)
		{
			std::vector<Text> elements;
			std::size_t current = 0;
			std::size_t next = path.find('/');
			if (next == 0) // leading '/', we want to keep that regardless
			{
				path = path.trimPrefix('/');
				next = path.find('/');
			}

			while (next != std::string::npos)
			{
				Text element = path.substr(current, next - current);
				// Skip '.' elements and empty elements
				if (element && element != ".")
				{
					// If we have a '..' element, pop one off the elements stack
					if (!elements.empty() && element == "..")
					{
						elements.pop_back();
					}
					else
					{
						elements.emplace_back(std::move(element));
					}
				}
				current = next + 1;
				next = path.find('/', current);
			}

			if (current < path.strlen())
			{
				Text element = path.suffix(current);
				// Skip '.' elements and empty elements
				if (element && element != ".")
				{
					// If we have a '..' element, pop one off the elements stack
					if (!elements.empty() && element == "..")
					{
						elements.pop_back();
					}
					else
					{
						elements.emplace_back(std::move(element));
					}
				}
			}

			// If we built out an element stack, reconstruct the path
			if (!elements.empty())
			{
				std::string text;
				std::size_t prefix = path.data() - mText.data();
				std::size_t offset = prefix + path.size();
				std::size_t suffix = mText.size() - offset;
				std::size_t newSize = elements.front().size();
				for (std::size_t i = 1; i < elements.size(); ++i)
				{
					newSize += (elements[i].size() + 1);
				}

				// Allocate the new text
				text.resize(prefix + newSize + suffix);
				std::copy(mText.begin(), mText.begin() + prefix, text.begin());
				std::copy(elements.front().begin(), elements.front().end(), text.begin() + prefix);

				std::size_t current = prefix;
				for (std::size_t i = 1; i < elements.size(); ++i)
				{
					current += elements[i - 1].size();
					text[current++] = '/';
					std::copy(elements[i].begin(), elements[i].end(), text.begin() + current);
				}
				current += elements.back().size();
				std::copy(mText.begin() + offset, mText.end(), text.begin() + current);
				if (mText != text)
				{
					mText = std::move(text);
					changed = true;
				}
			}
		}

		return changed;
	}

	Path Path::makeNormalized() const
	{
		Path output;

		// We're only concerned with the path portion
		Text path = this->getPath();

		// See if we need to do anything before allocating memory
		std::size_t split = path.find("/.");
		if (split != std::string::npos)
		{
			std::vector<Text> elements;
			std::size_t current = 0;
			std::size_t next = path.find('/');
			if (next == 0) // leading '/', we want to keep that regardless
			{
				path = path.trimPrefix('/');
				next = path.find('/');
			}

			while (next != std::string::npos)
			{
				Text element = path.substr(current, next - current);
				// Skip '.' elements and empty elements
				if (element && element != ".")
				{
					// If we have a '..' element, pop one off the elements stack
					if (!elements.empty() && element == "..")
					{
						elements.pop_back();
					}
					else
					{
						elements.emplace_back(std::move(element));
					}
				}
				current = next + 1;
				next = path.find('/', current);
			}

			if (current < path.strlen())
			{
				Text element = path.suffix(current);
				// Skip '.' elements and empty elements
				if (element && element != ".")
				{
					// If we have a '..' element, pop one off the elements stack
					if (!elements.empty() && element == "..")
					{
						elements.pop_back();
					}
					else
					{
						elements.emplace_back(std::move(element));
					}
				}
			}

			// If we built out an element stack, reconstruct the path
			if (!elements.empty())
			{
				std::string text;
				std::size_t prefix = path.data() - mText.data();
				std::size_t offset = prefix + path.size();
				std::size_t suffix = mText.size() - offset;
				std::size_t newSize = elements.front().size();
				for (std::size_t i = 1; i < elements.size(); ++i)
				{
					newSize += (elements[i].size() + 1);
				}

				// Allocate the new text
				text.resize(prefix + newSize + suffix);
				std::copy(mText.begin(), mText.begin() + prefix, text.begin());
				std::copy(elements.front().begin(), elements.front().end(), text.begin() + prefix);

				std::size_t current = prefix;
				for (std::size_t i = 1; i < elements.size(); ++i)
				{
					current += elements[i - 1].size();
					text[current++] = '/';
					std::copy(elements[i].begin(), elements[i].end(), text.begin() + current);
				}
				current += elements.back().size();
				std::copy(mText.begin() + offset, mText.end(), text.begin() + current);
				output.mText = std::move(text);
			}
		}

		if (output.mText.empty())
		{
			output.mText = mText;
		}

		return  output;
	}

	bool Path::containsPath(const Path &element) const noexcept
	{
		bool found = false;
		if (element)
		{
			Text ourPath = this->getPath();
			Text theirPath = element.getPath();

			std::size_t ourLen = ourPath.strlen();
			std::size_t theirLen = theirPath.strlen();

			while (ourLen >= theirLen)
			{
				if (ourPath.startsWith(theirPath) && (ourLen == theirLen || ourPath[theirLen] == '/'))
				{
					found = true;
					break;
				}

				ourPath = ourPath.splitAfter('/');
				ourLen = ourPath.strlen();
			}
		}
		return found;
	}

	bool Path::containsPath(const Path &element, Case match) const noexcept
	{
		bool found = false;
		if (element)
		{
			Text ourPath = this->getPath();
			Text theirPath = element.getPath();

			std::size_t ourLen = ourPath.strlen();
			std::size_t theirLen = theirPath.strlen();

			while (ourLen >= theirLen)
			{
				if (cio::equalText(ourPath.c_str(), theirPath.c_str(), match, theirLen) && (ourLen == theirLen || ourPath[theirLen] == '/'))
				{
					found = true;
					break;
				}

				ourPath = ourPath.splitAfter('/');
				ourLen = ourPath.strlen();
			}
		}
		return found;
	}

	Path &Path::complete(const Path &base)
	{
		if (!base.empty() && this->isRelative())
		{
			bool needExtraDelim = base.mText.back() != '/';
			std::string completed;
			completed.resize(base.mText.size() + needExtraDelim + mText.size());
			std::copy(base.mText.begin(), base.mText.end(), completed.begin());

			if (needExtraDelim)
			{
				completed[base.mText.size()] = '/';
			}

			std::copy(mText.begin(), mText.end(), completed.begin() + base.mText.size() + needExtraDelim);
			mText = std::move(completed);
		}

		this->normalize();

		return *this;
	}

	Path Path::makeCompletePath(const Path &base) const
	{
		Path completed;

		if (base.empty() || !this->isRelative())
		{
			completed = *this;
		}
		else
		{
			bool needExtraDelim = base.mText.back() != '/';
			completed.mText.resize(base.mText.size() + needExtraDelim + mText.size());
			std::copy(base.mText.begin(), base.mText.end(), completed.mText.begin());

			if (needExtraDelim)
			{
				completed.mText[base.mText.size()] = '/';
			}

			std::copy(mText.begin(), mText.end(), completed.mText.begin() + base.mText.size() + needExtraDelim);
		}

		completed.normalize();
		return completed;
	}

	Path Path::makeRelativePath(const Path &target) const
	{
		Path result;
		Text protocol = this->getProtocol();
		Text targetProtocol = target.getProtocol();

		// protocols don't even match, we can't compute it so just return the input
		if (!protocol || !targetProtocol || protocol != targetProtocol)
		{
			result = target;
		}
		else
		{
			Text root = this->getRoot();
			Text targetRoot = target.getRoot();

			// If the roots don't match, we can't compute it so just return the input
			if (!root || !targetRoot || root != targetRoot)
			{
				result = target;
			}
			else
			{
				Text path = this->getPath();
				Text targetPath = target.getPath();

				// If the paths are identical, we can just do the relative path '.'
				if (path == targetPath)
				{
					result.mText = ".";
				}

				// Find longest common prefix that ends in a directory separator or end of the path
				// Since both paths are complete, we can skip the start of the path
				// Note that the paths may have different numbers of extraneous separators and so on so we track indices separately
				std::size_t prefix = path.data() - mText.data();
				std::size_t targetPrefix = targetPath.data() - target.mText.data();

				while (prefix < mText.size() && targetPrefix < target.mText.size())
				{
					// make sure the next separator (or end of path) are at the same location
					std::size_t nextLeft = mText.find('/', prefix);
					std::size_t nextRight = target.mText.find('/', targetPrefix);

					if (nextLeft == std::string::npos)
					{
						nextLeft = mText.size();
					}

					if (nextRight == std::string::npos)
					{
						nextRight = target.mText.size();
					}

					std::size_t length = nextLeft - prefix;
					std::size_t targetLength = nextRight - targetPrefix;

					// Mismatch vs. prefix to date
					if (length != targetLength)
					{
						break;
					}

					// Verify that the contents between current and next are the same
					bool matches = true;

					for (std::size_t i = 0; i < length && matches; ++i)
					{
						matches = (mText[i + prefix] == target[i + targetPrefix]);
					}

					// path element didn't match, break out of prefix loop
					if (!matches)
					{
						break;
					}

					// Advance past the path separator, if present, to the next path element
					prefix = std::min(mText.size(), nextLeft + 1);
					targetPrefix = std::min(target.mText.size(), nextRight + 1);
				}

				// Figure out how many directory steps up from this path to the prefix
				std::size_t current = mText.size();

				while (current > prefix)
				{
					result.mText += "../";
					current = mText.rfind('/', current);
					
					// if the path has a '/' at the end, ignore that
					if (current == mText.size() - 1)
					{
						current = mText.rfind('/', current - 1);
					}

					if (current > 0)
					{
						if (current != std::string::npos)
						{
							--current;
						}
						else
						{
							current = 0;
						}
					}
				}

				if (targetPrefix < target.size())
				{
					result.mText += target.mText.substr(targetPrefix);
				}
			}
		}

		return result;
	}

	Path Path::makePathWithExtension(const Text &ext) const
	{
		std::string text;

		Text filenameNoExt = this->getFilenameNoExtension();

		if (!filenameNoExt.empty())
		{
			std::size_t filenameOffset = filenameNoExt.data() - mText.data();
			std::size_t prefixLength = filenameOffset + filenameNoExt.size();

			Text pathNoExt(mText.data(), prefixLength);

			if (ext.empty())
			{
				text = pathNoExt;
			}
			else
			{
				bool delimiter = (ext.front() != '.');
				std::size_t prefix = pathNoExt.size();
				std::size_t suffix = ext.strlen();
				std::size_t total = prefix + suffix + delimiter;

				text.resize(total);

				std::memcpy(&text[0], pathNoExt.data(), prefix);
				if (delimiter)
				{
					text[prefix] = '.';
				}
				std::memcpy(&text[0] + prefix + delimiter, ext.data(), suffix);
			}
		}

		return Path(std::move(text));
	}

	Text Path::getFilename() const noexcept
	{
		Text result;
		Text root = this->getRoot();
		Text query = this->getQuery();
		std::size_t pathStart = 0;

		if (root)
		{
			pathStart = (root.data() - mText.data()) + root.size();
		}

		std::size_t filenameEnd = mText.size() - query.size();
		while (filenameEnd > 1 && mText[filenameEnd - 1] == '/') // trailing separator, do not want
		{
			--filenameEnd;
		}

		std::size_t lastSeparator = mText.rfind('/', filenameEnd - 1);

		if (lastSeparator == std::string::npos || lastSeparator < pathStart)
		{
			result = Text(mText.data() + pathStart, filenameEnd - pathStart);
		}
		else
		{
			result = Text(mText.data() + lastSeparator + 1, filenameEnd - lastSeparator - 1);
		}

		return result;
	}

	void Path::setFilename(const Text &text) 
	{
		// Get existing filename information
		Text root = this->getRoot();
		Text query = this->getQuery();
		std::size_t pathStart = 0;

		if (root)
		{
			pathStart = (root.data() - mText.data()) + root.size();
		}

		std::size_t filenameEnd = mText.size() - query.size();
		std::size_t lastSeparator = mText.rfind('/', filenameEnd);
		
		std::size_t toCopy = text.strlen();
		std::size_t insert = lastSeparator + 1;
		std::size_t oldLength = filenameEnd - lastSeparator - 1;
		std::size_t oldSize = mText.size();
		
		if (lastSeparator == std::string::npos || lastSeparator < pathStart)
		{
			insert = pathStart;
			oldLength = filenameEnd - pathStart;
		}
		
		if (oldLength < toCopy)
		{
			mText.resize(mText.size() + toCopy - oldLength);
			std::copy(mText.begin() + insert + oldLength, mText.begin() + oldSize, mText.begin() + insert + toCopy);
		}
		else if (oldLength > toCopy)
		{
			std::copy(mText.begin() + insert + oldLength, mText.begin() + oldSize, mText.begin() + insert + toCopy);
			mText.resize(mText.size() + toCopy - oldLength);
		}
		
		std::copy(text.data(), text.data() + toCopy, mText.begin() + insert); 
	}

	Text Path::getFilenameNoExtension() const noexcept
	{
		Text result;
		Text filename = this->getFilename();

		if (filename)
		{
			Text ext = this->getExtension();

			if (ext && ext.data() - filename.data() > 1)
			{
				result = Text(filename.data(), ext.data() - filename.data() - 1);
			}
			else
			{
				result = std::move(filename);
			}
		}

		return result;
	}
	
	Path &Path::addChild(const Path &child)
	{
		// see if child has anything other than leading '/'
		std::size_t offset = 0;

		while (offset < child.mText.size() && child.mText[offset] == '/')
		{
			++offset;
		}

		if (child.isRelative() && offset < child.mText.size())
		{
			// Add a separator if we need one
			if (!mText.empty() && mText.back() != '/')
			{
				mText.push_back('/');
			}

			// Skip all leading '/' on child
			mText.append(child.mText.begin() + offset, child.mText.end());
		}

		return *this;
	}

	Text Path::toWindowsFile() const
	{
		Text windowsFile;
		Text root = this->getRoot();
		const char *copyBegin;

		if (!root.empty())
		{
			// If we start with '/' and have an actual Windows drive after that, skip the initial '/'
			if (root.front() == '/' && Path::hasWindowsRoot(root.data() + 1, root.size() - 1))
			{
				copyBegin = root.begin();
				while (copyBegin != root.end() && *copyBegin == '/')
				{
					++copyBegin;
				}
			}
			// Otherwise just copy the root verbatim (should only happen on a unix path relative to current drive)
			else
			{
				copyBegin = root.begin();
			}
		}
		else
		{
			copyBegin = this->getPath().begin();
		}

		const char *copyEnd = this->getPath().end();
		windowsFile.bind(copyBegin, copyEnd - copyBegin);
		
		Text result = windowsFile.replace('/', '\\');
		result.nullTerminate();
		return result;
	}

	std::size_t Path::toWindowsFile(char *buffer, std::size_t length) const noexcept
	{
		Text root = this->getRoot();
		const char *copyBegin;

		if (root)
		{
			// If we start with '/' and have an actual Windows drive after that, skip the initial '/'
			if (root.front() == '/' && Path::hasWindowsRoot(root.data() + 1, root.strlen() - 1))
			{
				copyBegin = root.begin();
				while (copyBegin != root.end() && *copyBegin == '/')
				{
					++copyBegin;
				}
			}
			// Otherwise just copy the root verbatim (should only happen on a unix path relative to current drive)
			else
			{
				copyBegin = root.begin();
			}
		}
		else
		{
			copyBegin = this->getPath().begin();
		}

		const char *copyEnd = this->getPath().end();
		std::size_t needed = copyEnd - copyBegin;
		std::size_t toCopy = std::min(needed, length);

		for (std::size_t i = 0; i < toCopy; ++i)
		{
			char c = copyBegin[i];

			if (c == '/')
			{
				c = '\\';
			}

			buffer[i] = c;
		}

		if (toCopy < length)
		{
			std::memset(buffer + toCopy, 0, length - toCopy);
		}

		return needed;
	}

	Text Path::toUnixFile() const
	{
		// No transforms needed, URI to Unix file is identity
		Text path = this->getPath();
		path.nullTerminate();
		return path;
	}

	std::size_t Path::toUnixFile(char *buffer, std::size_t length) const noexcept
	{
		Text text = this->getPath();
		std::size_t needed = text.strlen();
		std::size_t toCopy = std::min(needed, length);

		if (toCopy > 0)
		{
			std::memcpy(buffer, text.data(), toCopy);
		}

		if (toCopy < length)
		{
			std::memset(buffer + toCopy, 0, length - toCopy);
		}

		return needed;
	}

	Text Path::toNativeFile() const
	{
#if defined _WIN32
		return toWindowsFile();
#else
		return toUnixFile();
#endif
	}

	std::size_t Path::toNativeFile(char *buffer, std::size_t length) const noexcept
	{
#if defined _WIN32
		return toWindowsFile(buffer, length);
#else
		return toUnixFile(buffer, length);
#endif
	}

	void Path::validateNativeInput()
	{
		mText = Path::validateNativeInputText(std::move(mText));
	}

	Path operator/(const Path &left, const Path &right)
	{
		Path result(left);
		result.addChild(right);
		return result;
	}

	Path &operator/=(Path &left, const Path &right)
	{
		left.addChild(right);
		return left;
	}
	
	std::size_t Path::strlen() const noexcept
	{
		return mText.size();
	}

	std::size_t Path::print(char *buffer, std::size_t capacity) const noexcept
	{
		cio::reprint(mText.data(), mText.size(), buffer, capacity);
		return mText.size();
	}

	const std::string &Path::print() const noexcept
	{
		return mText;
	}

	std::ostream &operator<<(std::ostream &stream, const Path &path)
	{
		return stream << path.get();
	}
}
