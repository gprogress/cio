/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "StandardPath.h"

#include <ostream>

namespace cio
{
	const char *sStandardPathText[] =
	{
		"None",
		"Application"
		"Library",
		"Current",
		"Temporary",
		"Null",
		"Home",
		"Documents",
		"Desktop"
	};

	const char *print(StandardPath type) noexcept
	{
		return sStandardPathText[static_cast<unsigned>(type)];
	}

	std::ostream &operator<<(std::ostream &stream, StandardPath type)
	{
		return (stream << print(type));
	}
}

