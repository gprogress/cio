/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_FILESYSTEM_H
#define CIO_FILESYSTEM_H

#include "Types.h"

#include "Protocol.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Filesystem class provides the adapter to use the local filesystem provided by the host OS with the CIO Protocol interface.
	 * 
	 * Currently the Filesystem class provides no meaningful traversal mechanism - the Directory subclass can be used to iterate through
	 * specific directories. In the future this may change.
	 */
	class CIO_API Filesystem : public Protocol
	{
		public:
			/**
			* Gets the metaclass for this class.
			* 
			* @return the metaclass for this class
			*/
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Gets the standard protocol prefix for the File Protocol.
			* This is the "file" prefix.
			* 
			* @return the standard protocol prefix for the File Protocol
			*/
			static Text getClassProtocolPrefix() noexcept;

			/**
			* Gets the standard protocol filter for the File Protocol.
			* This consists of a single filter matching "file" prefix.
			* 
			* @return the standard protocol filter for the File Protocol
			*/
			static Chunk<ProtocolFilter> getClassProtocolFilter() noexcept;

			/**
			 * Checks for the set of special filename entries for Windows, regardless of the
			 * current host operating system.
			 *
			 * This checks for "CON", "PRN", "NUL", "AUX", "COM0" through "COM9", and "LPT0" through "LTP9"
			 * and if any of these are matched, Resource::Device is returned.
			 *
			 * This method does not check for general special entries like '.' and '..' - call checkForSpecialDirectories
			 * to check for those.
			 *
			 * @param text The text entry
			 * @return the type of special entry found, or Resource::None if it's not a special entry
			 */
			static Resource checkForSpecialWindowsDevices(const Text &text) noexcept;

			/**
			* Construct the File Protocol with the default prefix and filter.
			*/
			Filesystem() noexcept;

			/**
			* Destructor.
			*/
			virtual ~Filesystem() noexcept override;

			/**
			* Clears the file protocol to the default state.
			*/
			virtual void clear() noexcept override;

			/**
			* Gets the metaclass for this object.
			* 
			* @return the metaclass
			*/
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			* Gets the metadata about the given resource.
			* The metadata includes the modes and permissions, overall category, and content length.
			* It also includes the canonical URI for the resource, following any links or redirects.
			* If the resource does not actually exist, the returned metadata will reflect that.
			*
			* @param input the URI to the resource
			* @return the metadata about the resource
			* @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			*/
			virtual Metadata readMetadata(const Path &path) const override;
				
			/**
			* Creates an empty resource using metadata as a template.
			*
			* The File Protocol implementation uses native operating system methods to create files and directories as appropriate.
			*
			* @param path The resource metadata describing the URI, permissions, and other parameters for the resource
			* @param createParents Whether to create missing parent resources and if so whether to do it recursively
			* @return the metadata describing the actually created resource
			* @throw Exception If any error (other than the resource already existing) prevented creating the resource
			*/
			virtual Metadata create(const Metadata &path, Traversal createParents = Traversal::Recursive) override;
				
			/**
			* Gets the overall resource type for a given resource.
			* The File Protocol implementation directly queries the native file system as efficiently as possible.
			*
			* @param path The resource path
			* @return the overall resource type for the resource, or None if it does not exist
			* @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			*/
			virtual Resource readType(const Path &path) const override;

			/**
			* Gets the resource available access modes for the given resource, as a subset of the desired modes.
			* The File Protocol implementation directly queries the native file system as efficiently as possible.
			*
			* @param path The resource path
			* @param desired The desired access modes
			* @return the resource modes and permissions
			* @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			*/
			virtual ModeSet readModes(const Path &path, ModeSet desired = ModeSet::general()) const override;

			/**
			* Gets the total logical content length for a given resource.
			* For inputs and outputs, this is the logical content size in bytes.
			* For directories this is the number of directory entries.
			* 
			* The File Protocol implementation directly queries the native file system as efficiently as possible.
			*
			* @param path The resource path
			* @return the overall content length for the resource, or CIO_UNKNOWN_LENGTH if not known
			* @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			*/
			virtual Length readSize(const Path &path) const override;

			/**
			* Gets the path for the current working directory formatted as a platform-independent URI path.
			* This is initially the directory in which the program was launched according to the operating system, but may be changed by the program.
			* Note that the current directory may be changed asynchronously by another thread, so it may have already changed by the time this call returns.
			*
			* @return the path for the current working directory at the time of this call
			* @throw std::bad_alloc If the path was known but memory could not be allocated for it
			* @throw std::runtime_error If the path itself could not be determined
			*/
			virtual Path getCurrentDirectory() const override;

			/**
			* Sets the current working directory to the given path.
			* This is initially the directory in which the program was launched according to the operating system, but may be changed by the program.
			* Note that the current directory may be changed asynchronously by another thread, and it is undefined which of multiple concurrent changes
			* actually is the end result.
			*
			* @param path The native file path for the new current working directory
			* @param createMissing If the directory does not currently exist, describe whether to create the immediate or all missing parent directories if possible
			* @throw Exception If setting the current directory failed
			*/
			virtual void setCurrentDirectory(const Path &path, Traversal createMissing = Traversal::None) override;

			/**
			* Gets the path for the given standard path type, if possible to determine.
			* This can be used to obtain the location of any StandardPath enumerated value
			* including the user home directory, documents directory, and so on.
			*
			* @param path the type of standard path to obtain
			* @param type The type of resource desired for the standard path
			* @return the standard path if determined
			* @throw Exception if the path was missing or otherwise could not be determined
			*/
			virtual Path getStandardPath(StandardPath path, Resource type = Resource::Unknown) const override;

			/**
			 * Checks whether the given filename entry is considered a "special" entry for this directory type.
			 *
			 * For native filesystems, "." and ".." are always special with Resource::Directory regardless
			 * of operating system. On Microsoft Windows, "CON", "PRN", "NUL", "AUX",
			 * "COM0" through "COM9" and "LPT0" through "LTP9" are also special with Resource::Device.
			 *
			 * @note Strictly speaking, NTFS and FAT32 file systems should consider the Windows device names
			 * special even when the host operating system is not Microsoft Windows. However, this is not yet
			 * detected and the Linux/Apple/Unix NTFS or FAT driver may or may not allow them.
			 *
			 * @param filename The filename entry, which is not necessarily null terminated
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			virtual Resource checkForSpecialFilename(const Text &filename) const noexcept override;

			/**
			* Gets the path for the currently running program file, if possible to determine.
			*
			* @return the path for the application program file
			* @throw std::bad_alloc If the path was known but memory could not be allocated for it
			* @throw Exception If the path itself could not be determined
			*/
			Path getApplicationFile() const;

			/**
			* Gets the path for the directory containing the currently running program, if possible to determine.
			* This is a more reliable way to resolve relative paths since it cannot be changed by the program and doesn't depend on where the user ran the program.
			*
			* @return the path for the application program directory
			* @throw std::bad_alloc If the path was known but memory could not be allocated for it
			* @throw Exception If the path itself could not be determined
			*/
			Path getApplicationDirectory() const;

			/**
			* Gets the system temporary directory that is guaranteed to be readable and writable by the current user.
			*
			* @return the path for the systemtemporary directory
			*/
			Path getTemporaryDirectory() const;

			/**
			* Creates a temporary directory that is guaranteed to be readable and writable by the current user
			* and did not previously exist before this call.
			*
			* The implementation may create the directory anywhere and name it anything so long as it meets these criteria.
			*
			* @return the path for the created temporary directory
			*/
			Path createTemporaryDirectory();

			/**
			* Creates the CIO Directory implementation most suitable for the given path, and then returns
			* an opened directory suitable for scanning directory entries.
			*
			* The File Protocol implementation creates a File Directory using native operating system directory implementation.
			*
			* @param path The path to the directory
			* @param modes The modes to open the diretory in
			* @return the opened directory
			* @throw Exception If the directory could not be opened for any reason
			* @throw std::bad_alloc If memory allocation failed
			*/
			virtual std::unique_ptr<Protocol> openDirectory(const Path &path, ModeSet modes = ModeSet::readonly(), Traversal createMissing = Traversal::None) override;

			/**
			 * Creates the CIO Protocol implementation most suitable for the given path as a directory, and then returns
			 * an opened directory suitable for scanning directory entries read-only.
			 *
			 * The File Protocol implementation creates a File Directory using native operating system directory implementation.
			 *
			 * @param path The path to the directory
			 * @param modes The modes to use to open the resource
			 * @return the opened directory
			 * @throw Exception If the directory could not be opened for any reason
			 * @throw std::bad_alloc If memory allocation failed
			 */
			virtual std::unique_ptr<Protocol> readDirectory(const Path &path, ModeSet modes = ModeSet::readonly()) const override;

			/**
			* Creates an empty directory using this protocol at the given path with the given modes, optionally also creating parent resources.
			*
			* The File Protocol uses native file system access to do the underlying creation.
			*
			* @param path The path of the directory to create
			* @param modes The desired modes of the directory
			* @param createParents Whether to create missing parent diretories and if so recursively
			* @return the outcome of the operation
			* @throw Exception If directory creation failed for any reason other than the directory already existing
			*/
			virtual State createDirectory(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
				
			/**
			* Removes and permanently erases (if possible) the resource at the given path.
			* The Traversal parameter controls what happens if the resource is a directory with children.
			* Traversal::None will only erase the resource if it has no children.
			* Traversal::Immediate will erase direct children of the resource (but not their children if non-empty).
			* Traversal::Recursive will recursively erase every child resource.
			* 
			* The File Protocol implementation directly uses native operating system functions to erase files and directories.
			*
			* @param path The path to the resource to erase
			* @param depth The depth controlling whether to erase non-empty directories and if so whether to erase only immediate children
			* or all children recursively
			* @return the total number of resources that were erased
			* @throw Exception If transport failures or other problems (other than the resource not existing) prevented erasure
			*/
			virtual Progress<std::uint64_t> erase(const Path &path, Traversal mode = Traversal::None) override;

			/**
			* Create an Input subclass instantiation that can read the content for the resource at the given path.
			*
			* The File Protocol opens or creates the path as a native File (typically) or some other device.
			*
			* @param path The path to the resource to read
			* @param modes The modes to use to open the resource
			* @return the opened input
			* @throw Exception If transport problems or lack of existence of the resource prevented creating an Input
			* @throw std::bad_alloc If allocating the new Input failed
			*/
			virtual std::unique_ptr<Input> openInput(const Path &path, ModeSet modes = ModeSet::readonly()) const override;

			/**
			* Create an Output subclass instantiation that can write the content for the resource at the given path.
			* The resource will be created if it does not already exist, but the existing content will be preserved if it is
			* a persistent resource.
			*
			* Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			*
			* The File Protocol opens or creates the path as a native File (typically) or some other device.
			*
			* @param path The path to the resource to read
			* @param modes The modes to use to open or create the resource
			* @param createParentResources Whether to create missing parent resources and, if so, whether to do it recursively
			* @return the opened output
			* @throw Exception If transport problems or inability to open or create the resource prevented creating an Output
			* @throw std::bad_alloc If allocating the new Input failed
			*/
			virtual std::unique_ptr<Output> openOutput(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
				
			/**
			* Create an Channel subclass instantiation that can both read and write the content for the resource at the given path.
			* The resource will be created if it does not already exist, but the existing content will be preserved if it is
			* a persistent resource.
			*
			* Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			*
			* The File Protocol opens or creates the path as a native File (typically) or some other device.
			*
			* @param path The path to the resource to read
			* @param modes The modes to use to open or create the resource
			* @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			* @return the opened channel
			* @throw Exception If the native file system could not open or create the file
			* @throw std::bad_alloc If allocating the new Channel failed
			*/
			virtual std::unique_ptr<Channel> openChannel(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
				
			/**
			* Creates an empty file using this protocol at the given path with the given modes, optionally also creating parent resources.
			*
			* The File Protocol uses native operating system functions to do this as efficiently as possible.
			*
			* @param path The path to the file to create
			* @param modes The desired modes for the file
			* @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			* @return the outcome of this operation, which should either be a success or a failure with Reason::Exists
			* @throw Exception If creating the file failed for any reason other than it already existing
			*/	
			virtual State createFile(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;

			/**
			* Reads the target URI specified by the given path which is a link.
			*
			* The File Protocol will check to see if the path is a symbolic link (if supported by the system) and if so return the file path
			* read from it.
			* 
			* This method is smart enough to detect infinite link loops if Traversal is Recursive and will throw an exception in that case.
			* Otherwise, the specified traversal depth will be followed.
			*
			* @param path The path to the link
			* @param depth How many links to traverse
			* @return the path to the resource the link references
			* @throw Exception If the given path didn't exist or wasn't a link
			*/
			virtual Path readLink(const Path &path, Traversal depth = Traversal::Immediate) const override;
			
			/**
			* Creates a link at the given link path that references the given target path.
			*
			* The File Protocol will create a symbolic link if supported by the system.
			*
			* @param link The path of the link to create
			* @param target The path the link should point to
			* @param modes The modes for the created link
			* @param createParents Whether to create missing parent directories
			* @return the outcome of the operation
			* @throw Exception If the given link path already existed or the link couldn't be created
			*/
			virtual State createLink(const Path &link, const Path &target, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive) override;
			
			/**
			* Performs a direct copy from input to output path up to the specified traversal depth.
			*
			* The File Protocol implementation uses native operating system file functions when possible.
			*
			* @param input The input path
			* @param output the output path
			* @param depth The copy depth
			* @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			* @return the outcome and how many total resources were copied out of
			* the examined resources
			*/
			virtual Progress<std::uint64_t> copy(const Path &input, const Path &output, Traversal depth = Traversal::Recursive, Traversal createParents = Traversal::Recursive) override;
				
			/**
			* Performs a direct move from input to output path.
			*
			* The File Protocol implementation uses native operating system file functions when possible.
			* Typically a move can be done immediately as long as the input and output are on the same hardware device.
			*
			* By definition a move must be fully recursive, unlike a copy.
			*
			* @param input The input path
			* @param output the output path
			* @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			* @return the outcome and how many total resources were copied out of
			* the examined resources
			*/
			virtual Progress<std::uint64_t> move(const Path &input, const Path &output, Traversal createParents = Traversal::Recursive) override;
			
			// Native path methods - operating on native file paths in fixed buffers and null-terminated native strings

			/**
			* Gets the native file path for the given standard path type, if possible to determine.
			* At most the given number of bytes are used in the text buffer. If fewer bytes are needed, the buffer will be null terminated.
			*
			* Due to limitations in underlying operating systems, the returned result may be set to CIO_UNKNOWN_SIZE with Status::Overflow
			* if the actual path could not fit into the buffer with a null-terminator. In this case, the caller should retry with a larger size but
			* no guidance is given on what the correct size is. If the system does support it, it will report the actual size in the result.
			*
			* @param path the type of standard path to obtain
			* @param type the type of resource desired
			* @param buffer The text buffer to copy the application native file path into
			* @param count The maximum number of bytes to use for the file path
			* @return the actual number of bytes needed for the application file path (or CIO_UNKNOWN_SIZE) and the status describing success or failure
			*/
			Progress<std::size_t> getNativeStandardPath(StandardPath path, Resource type, char *buffer, std::size_t count) const noexcept;

			/**
			 * Gets the native file path for the executing program formatted exactly as provided by the native operating system.
			 * 
			 * @param buffer The text buffer to copy the native current directory file path into
			 * @param count The maximum number of bytes to use for the file path
			 * @return the actual number of bytes needed for the application file path and the status if a failure occurred
			 */
			Progress<std::size_t> getNativeApplicationFile(char *buffer, std::size_t count) const noexcept;

			/**
			 * Gets the native file path for the library directory associated with the currently executing program exactly as
			 * provied by the native operating system. This is primarily used to locate dynamic shared libraries.
			 * 
			 * On Windows this is the same as the application directory. On other platforms, it is the library directory
			 * that is the sibling of the application directory.
			 *
			 * @param buffer The text buffer to copy the native current directory file path into
			 * @param count The maximum number of bytes to use for the file path
			 * @return the actual number of bytes needed for the library directory path and the status if a failure occurred
			 */
			Progress<std::size_t> getNativeLibraryDirectory(char *buffer, std::size_t count) const noexcept;

			/**
			 * Gets the native file path for the current working directory formatted exactly as provided by the native operating system.
			 * This is initially the directory in which the program was launched according to the operating system, but may be changed by the program.
			 * Note that the current directory may be changed asynchronously by another thread, so it may have already changed by the time this call returns.
			 *
			 * @param buffer The text buffer to copy the native current directory file path into
			 * @param count The maximum number of bytes to use for the file path
			 * @return the actual number of bytes needed for the current directory file path and the status if a failure occurred
			 */
			Progress<std::size_t> getNativeCurrentDirectory(char *buffer, std::size_t count) const noexcept;

			/**
			 * Sets the current working directory to the given native file path.
			 * This is initially the directory in which the program was launched according to the operating system, but may be changed by the program.
			 * Note that the current directory may be changed asynchronously by another thread, and it is undefined which of multiple concurrent changes
			 * actually is the end result.
			 *
			 * This can fail if the specified directory does not exist and possibly for other reasons.
			 *
			 * @param path The native file path for the new current working directory as a null-terminated C string
			 * @return the status describing whether this operation was successful
			 */
			State setNativeCurrentDirectory(const char *path) noexcept;

			/**
			 * Creates a directory at the given native file path.
			 **
			 * This is the lowest level operating system method that will only succeed if the directory does not already exist,
			 * the parent directory does exist, and the caller has permission to create the directory. It will not create missing parent directories.
			 *
			 * @param path The native directory path as a null-termianted C string
			 * @return the status of this operation, Success if created, Exists if it already exists, and a failure status otherwise
			 */
			State createNativeDirectory(const char *path) noexcept;

			/**
			 * Removes the directory at the given native file path if it is empty.
			 *
			 * @param path The path to the directory to remove
			 * @return the status of this operation: Success if removed, Missing if it doesn't exist, Exists if it has children,
			 * Unsupported if it's a file, or another failure status if the operation failed for other reasons
			 */
			State removeNativeDirectory(const char *path) noexcept;

			/**
			 * Creates an empty file at the given path.
			 *
			 * @param path The file path
			 * @return the status of this operation: Success if created, Exists if it already exists, and a failure status otherwise
			 */
			State createNativeFile(const char *path) noexcept;

			/**
			 * Creates a symbolic link at the given link path that points to the given target, if supported by the system.
			 *
			 * @param link The native path of the link to create
			 * @param target The native path of the link's target
			 * @return the status of this operation: Success if created, Exists if it already exists with the same target, and a failure status otherwise
			 */
			State createNativeLink(const char *link, const char *target) noexcept;

			/**
			 * Reads the target of a symbolic link at the given native link path.
			 *
			 * @param link The native path of the link to read
			 * @param buffer The text buffer to receive the link target
			 * @param length The maximum length of the text buffer to use
			 * @return the outcome of the read attempt and the actual needed size for the link buffer
			 */
			Progress<std::size_t> readNativeLink(const char *link, char *buffer, std::size_t length) const noexcept;

			/**
			 * Removes the file at the given path.
			 *
			 * @param path The path to the file to remove
			 * @return the status of this operation: Success is removed, Missing if it doesn't exist, Unsupported if it's a directory,
			 * or a failure status if the operation failed for other reasons
			 */
			State removeNativeFile(const char *path) noexcept;

			/**
			 * Removes the given native file path regardless of its type.
			 * This will only remove directories if they are empty.
			 *
			 * @param path The native file path as a null-terminated C string
			 * @return Success if removed, Missing if it did not exist, Exists if it is a directory with children, or some other error status on failure
			 */
			State removeNative(const char *path) noexcept;

			/**
			* Deletes the given directory and its parents to the depth specified.
			* 
			* @param dir The directory to start deletion from
			* @param depth The number of directories to delete
			* 
			* @return Success if removed all directories directed in depth, Failed if reached a directory that cannot be deleted
			*/
			State pruneEmptyDirectories(const Path &dir, Traversal depth = Traversal::Recursive);

			/**
			 * Moves a directory from one native file path to another if this can be done as a single operation.
			 * Typically this can only be done if both are on the same device and file system.
			 * If the move cannot be done in a single operation, the returned status will be Unsupported.
			 *
			 * @param source The source path
			 * @param destination The destination path
			 * @return the status of the move
			 */
			State moveNativeDirectory(const char *source, const char *destination) noexcept;

			/**
			 * Moves a file from one native file path to another if this can be done as a single operation.
			 * Typically this can only be done if both are on the same device and file system.
			 * If the move cannot be done in a single operation, the returned status will be Unsupported.
			 *
			 * @param source The source path
			 * @param destination The destination path
			 * @return the status of the move
			 */
			State moveNativeFile(const char *source, const char *destination) noexcept;

			/**
			 * Copies a file from one native file path to another in the most efficient native way possible.
			 *
			 * @param source The source path
			 * @param destination The destination path
			 * @return the status of the copy
			 */
			State copyNativeFile(const char *source, const char *destination) noexcept;

			/**
			 * Moves a file from one native file path to another if this can be done as a single operation,
			 * falling back to a native file copy otherwise. On some platforms this can be done as a single operation,
			 * otherwise it is implemented as a native move followed by a native copy.
			 *
			 * @note If the move fails, the copy succeeds, but the original cannot be deleted, then you may end up with two copies of the file
			 *
			 * @param source The source path
			 * @param destination The destination path
			 * @return the status of the move or copy
			 */
			State moveOrCopyNativeFile(const char *source, const char *destination) noexcept;

			/**
			 * Gets the metadata for a given path.
			 *
			 * @param path The path
			 * @return the metadata for the resource
			 * @throw cio::Exception If the metadata could not be obtained
			 */
			cio::Metadata getNativeMetadata(const char *path) const;

			/**
			* Gets the size of a file on disk in bytes.
			*
			* @param path The native file path as a null-terminated C string
			* @return the size of the file in byte
			*/
			Response<Length> getNativeFileSize(const char *path) const noexcept;

			/**
			 * Gets the overall type of a native file path.
			 *
			 * @param path The native file path as a null-terminated C-style string
			 * @return the type, or Resource::None if missing
			 */
			cio::Resource getNativeType(const char *path) const noexcept;

			/**
			 * Gets the modes for a native file path.
			 *
			 * @param path The native file path as a null-terminated C-style string
			 * @return the type
			 * @throw std::bad_alloc If internal memory allocations failed
			 */
			cio::ModeSet getNativeModes(const char *path) const noexcept;

			/**
			 * Gets the best matching modes for the given type and desired mode set.
			 * If the type is None, then creation modes will be selected if in the desired set.
			 * Otherwise load modes will be selected if in the desired set.
			 *
			 * @param type The type
			 * @param desired The desired modes
			 * @return the matching modes
			 */
			cio::ModeSet getModesForType(cio::Resource type, cio::ModeSet desired) const noexcept;

			/**
			 * Checks that the given file system native path exists regardless of type.
			 *
			 * @param path The path as a null-terminated C-style string
			 * @return whether any resource exists at that path
			 */
			bool existsNative(const char *path) const noexcept;

			/**
			 * Checks that the given file system native path exists and is a regular file.
			 *
			 * @param path The path as a null-terminated C-style string
			 * @return whether a file exists at that path
			 */
			bool isNativeFile(const char *path) const noexcept;

			/**
			 * Checks that the given file system native path exists and is a directory.
			 *
			 * @param path The path as a null-terminated C-style string
			 * @return whether a directory exists at that path
			 */
			bool isNativeDirectory(const char *path) const noexcept;

		private:
			/** The metaclass for this class */
			static Class<Filesystem> sMetaclass;

			/** The default filter for this class */
			static ProtocolFilter sDefaultFilter[];
				
			/** File protocol prefix */
			static const Text sFilePrefix;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

