/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Node.h"

#include <stdexcept>

namespace cio
{
	namespace xml
	{
		Node::Node() noexcept :
			mType(Type::None)
		{
			// ntohing more to do
		}

		Node::Node(Type type) noexcept :
			mType(type)
		{
			// nothing more to do
		}

		Node::Node(Type type, std::string value) :
			mValue(std::move(value)),
			mType(type)
		{
			// nothing more to do
		}


		Node::Node(const Node &in) = default;

		Node::Node(Node &&in) noexcept : /* = default */
			mValue(std::move(in.mValue)),
			mAttrs(std::move(in.mAttrs)),
			mChildren(std::move(in.mChildren)),
			mType(in.mType)
		{
			in.mType = Type::None;
		}

		Node &Node::operator=(const Node &) = default;

		Node &Node::operator=(Node &&in)  noexcept /* = default */
		{
			if (this != &in)
			{
				mValue = std::move(in.mValue);
				mAttrs = std::move(in.mAttrs);
				mChildren = std::move(in.mChildren);
				mType = in.mType;
				in.mType = Type::None;
			}

			return *this;
		}

		Node::~Node() noexcept = default;

		void Node::clear() noexcept
		{
			mValue.clear();
			mAttrs.clear();
			mChildren.clear();
			mType = Type::None;
		}

		const std::string &Node::getText() const noexcept
		{
			return mValue;
		}

		void Node::setText(std::string value)
		{
			mValue = std::move(value);
		}

		void Node::setText(Text value)
		{
			mValue = value.str();
		}

		void Node::appendText(Text value)
		{
			mValue.insert(mValue.end(), value.begin(), value.end());
		}

		Node &Node::addChild()
		{
			mChildren.emplace_back(Type::Element);
			return mChildren.back();
		}

		Node &Node::addChild(Node child)
		{
			mChildren.emplace_back(std::move(child));
			return mChildren.back();
		}

		Node &Node::addChild(Type type)
		{
			mChildren.emplace_back(type);
			return mChildren.back();
		}

		Node &Node::addChild(Type type, std::string value)
		{
			mChildren.emplace_back(type, std::move(value));
			return mChildren.back();
		}

		bool Node::removeChild(unsigned idx)
		{
			bool valid = idx < mChildren.size();

			if (valid)
			{
				mChildren.erase(mChildren.begin() + idx);
			}

			return valid;
		}

		void Node::clearChildren() noexcept
		{
			mChildren.clear();
		}

		Node &Node::getChild(unsigned idx)
		{
			return mChildren.at(idx);
		}

		const Node &Node::getChild(unsigned idx) const
		{
			return mChildren.at(idx);
		}

		Node &Node::getChild(Text name)
		{
			std::vector<Node>::iterator found;

			for (found = mChildren.begin(); found != mChildren.end() && found->getText() != name; ++found)
			{
				// nothing more to do
			}

			if (found == mChildren.end())
			{
				throw std::invalid_argument("No child element");
			}

			return *found;
		}

		std::size_t Node::getChildCount() const noexcept
		{
			return mChildren.size();
		}

		Node &Node::getFirstChild()
		{
			return mChildren.front();
		}

		const Node &Node::getFirstChild() const
		{
			return mChildren.front();
		}

		Node &Node::getLastChild()
		{
			return mChildren.back();
		}

		const Node &Node::getLastChild() const
		{
			return mChildren.back();
		}

		std::size_t Node::getAttributeCount() const noexcept
		{
			return mAttrs.size();
		}

		std::pair<std::string, std::string> &Node::getAttribute(std::size_t i)
		{
			return mAttrs.at(i);
		}

		const std::pair<std::string, std::string> &Node::getAttribute(std::size_t i) const
		{
			return mAttrs.at(i);
		}

		Text Node::getAttributeValue(Text key) const
		{
			Text value;

			for (const auto &item : mAttrs)
			{
				if (item.first == key)
				{
					value = item.second;
					break;
				}
			}

			return value;
		}

		void Node::addAttribute(Text key, Text value)
		{
			mAttrs.emplace_back(key, value);
		}

		void Node::addAttribute(std::string key, std::string value)
		{
			mAttrs.emplace_back(std::move(key), std::move(value));
		}

		void Node::addAttribute(std::pair<std::string, std::string> attribute)
		{
			mAttrs.emplace_back(std::move(attribute));
		}

		void Node::removeAttribute(std::size_t i)
		{
			mAttrs.erase(mAttrs.begin() + i);
		}

		void Node::clearAttributes() noexcept
		{
			mAttrs.clear();
		}

		void Node::mergeAdjacentTextChildren()
		{
			auto priorText = mChildren.end();
			std::size_t skipped = 0;

			for (auto ii = mChildren.begin(); ii != mChildren.end(); ++ii)
			{
				auto jj = ii;

				if (skipped > 0)
				{
					jj = ii - skipped;
					*jj = std::move(*ii);
				}

				if (jj->isText())
				{
					if (priorText != mChildren.end())
					{
						priorText->appendText(jj->getText());
						jj->clear();
						++skipped;
					}
					else
					{
						priorText = jj;
					}
				}
			}

			if (skipped > 0)
			{
				mChildren.resize(mChildren.size() - skipped);
			}
		}
	}
}
