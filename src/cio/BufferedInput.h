/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_BUFFEREDINPUT_H
#define CIO_BUFFEREDINPUT_H

#include "Types.h"

#include "Input.h"

#include "Buffer.h"
#include "Pointer.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Buffered Input implements a layer on top of another Input by reading chunks of data into
	 * a memory buffer and then serving read requests from the buffer.
	 * 
	 * It is primarily useful when reading lots of small data elements from inputs where doing so is relatively expensive.
	 * This class will primarily serve the reads from a fast memory buffer, only infrequently requesting large reads from
	 * the underlying input.
	 * 
	 * It is also useful as a base class for decoder classes to process encoded data.
	 */
	class CIO_API BufferedInput : public Input
	{
		public:
			/**
			 * Gets the metaclass for this type.
			 * 
			 * @return the metaclass 
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/** Default buffer size */
			static const std::size_t DefaultBufferSize;

			/**
			* Construct an empty buffered input.
			*/
			BufferedInput() noexcept;

			/**
			 * Construct a buffered input with the given input source and the default buffer size.
			 * 
			 * @param source The input source 
			 */
			explicit BufferedInput(Pointer<Input> source);

			/**
			 * Construct a buffered input with the given input source and buffer size to allocate.
			 * 
			 * @param source The input source
			 * @param bufferSize The buffer size
			 */
			BufferedInput(Pointer<Input> source, std::size_t bufferSize);

			/**
			 * Construct a buffered input with the giveninput source and buffer.
			 *
			 * @param source The input source
			 * @param buffer The buffer
			 */
			BufferedInput(Pointer<Input> source, Buffer buffer);

			/**
			 * Move constructor.
			 * 
			 * @param in The buffered input to move 
			 */
			BufferedInput(BufferedInput &&in) noexcept;
			
			/**
			 * Move assignment.
			 * 
			 * @param in The buffered input to move 
			 * @return This buffered input
			 */
			BufferedInput &operator=(BufferedInput &&in) noexcept;
		
			/**
			 * Destructor.
			 */
			virtual ~BufferedInput() noexcept override;

			/**
			 * Gets the metaclass for this object.
			 * 
			 * @return The metaclass for this object
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears the buffered input.
			 */
			virtual void clear() noexcept override;
				
			/**
			* Checks whether the input is open.
			* This is true if the buffer has unread data or the input source is open.
			*
			* @return whether the channel is open
			*/
			virtual bool isOpen() const noexcept override;

			/**
			 * Opens the resource using the given open modes.
			 * For Buffered Input, this refers back to the factory to open the actual input, then sets up the buffer.
			 *
			 * @param resource The resource to open
			 * @param modes The desired resource modes to use to open the resource
			 * @param factory The factory to use to construct underlying transport and codec protocols
			 * @return the actual resource modes that the resource could be opened with
			 * @throw cio::Exception If the resource could not be opened
			 */
			virtual cio::ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;

			/**
			 * Requests the metadata for the currently opened resource if possible to obtain.
			 * This delegates to the underlying input.
			 *
			 * @return all known metadata for the open channel
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Metadata metadata() const override;

			/**
			 * Gets the most specific path location of the resource, if possible.
			 * This delegates to the underlying input.
			 *
			 * @return the resource location of the input if known
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Path location() const override;

			/**
			 * Gets the overall resource type if known.
			 * This delegates to the underlying input.
			 *
			 * @return the resource type of the inputif known
			 */
			virtual Resource type() const noexcept override;

			/**
			 * Gets the current modes of the resource to the extent known.
			 * This delegates to the underlying input.
			 *
			 * @return the modes of the input if known
			 */
			virtual ModeSet modes() const noexcept override;

			/**
			 * Gets the total input size in bytes.
			 * This is the size of the underlying input, or the buffer limit if no input was set.
			 * 
			 * @return the input size
			 */
			virtual Length size() const noexcept override;

			/**
			 * Gets the number of readable bytes remaining.
			 * If the underlying input reports a known value, it is added to the remainin read buffer size.
			 *
			 * @return the number of bytes readable if known
			 */
			virtual Length readable() const noexcept override;

			/**
			 * Gets whether the read position is directly seekable on this input and if so in which direction.
			 * This delegates to the underlying input.
			 *
			 * @return the read seekability status
			 */
			virtual Resize isReadSeekable() const noexcept override;

			/**
			 * Attempts to read the given number of bytes into memory.
			 * If the read buffer has any data remaining, only that data is read.
			 * The buffer is refilled if it is empty and the read request is smaller than the buffer size.
			 * Otherwise, if the read request is larger than the total buffer size, the underlying input directly handles the request.
			 *
			 * @param data The memory to read into
			 * @param bytes The number of bytes requested to read
			 * @return the status of bytes actually read
			 */
			virtual Progress<std::size_t> requestRead(void *data, std::size_t bytes) noexcept override;

			/**
			 * Attempts to read data from the given position. If the position starts inside of the read buffer, it will be taken
			 * from the read buffer. Otherwise the request will be forwarded to the underlying input.
			 *
			 * @param data The memory to read into
			 * @param bytes The number of bytes to read
			 * @param position The desired position to read from
			 * @param mode The seek mode to use to locate the position
			 * @return the status of the read request
			 */
			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			/**
			 * Gets the current read position. This is tracked relative to the last read request from the input
			 * and the current read buffer position.
			 *
			 * @return the current read position
			 */
			virtual Length getReadPosition() const noexcept override;

			/**
			 * Requests to change the read position. If the requested position is within the current read buffer,
			 * the buffer position is updated. Otherwise, the buffer is cleared and the request is forwarded to the underlying input.
			 *
			 * @param position The desired position
			 * @param mode The seek mode for the position
			 * @return the status of the seek request
			 */
			virtual Progress<Length> requestSeekToRead(Length position, Seek mode) noexcept override;

			/**
			 * Requests skipping the given number of bytes. This is applied to the current read buffer first,
			 * then to the underlying input if necessary.
			 *
			 * @param bytes The number of bytes to skip
			 * @return the status of the skip request
			 */
			virtual Progress<Length> requestDiscard(Length bytes) noexcept override;

			/**
			 * Recommends the most efficient read block size and alignment for reading input from this input.
			 * This reports the current buffer size.
			 *
			 * @return the recommended alignment size for reads
			 */
			virtual Response<std::size_t> recommendReadAlignment() const noexcept override;

			/**
			 * Sets the maximum read timeout for all read operations not otherwise specifying a timeout.
			 * This delegates to the underlying input.
			 *
			 * @return the outome of setting the timeout
			 * @throw cio::Exception If the class can normally set a read timeout, but doing so failed
			 */
			virtual State setReadTimeout(std::chrono::milliseconds timeout) override;

			/**
			 * Gets the maximum read timeout for all read operations not otherwise specifying a timeout.
			 * This delegates to the underlying input.
			 *
			 * @return the read timeout
			 */
			virtual std::chrono::milliseconds getReadTimeout() const override;

			/**
			 * @copydoc Input::getText
			 * 
			 * This override directly implements searching the read buffer for the text end, refilling the buffer
			 * as needed to avoid needing to pull individual characters from the input.
			 */
			virtual Text getText(std::size_t bytes = SIZE_MAX) override;

			/**
			 * @copydoc Input::getTextLine
			 *
			 * This override directly implements searching the memory buffer for the text line end.
			 */
			virtual Text getTextLine(std::size_t bytes = SIZE_MAX) override;

			/**
			 * Copies up to the given number of bytes remaining in the input to the specified output.
			 * This class overrides the method to use its own read buffer to perform the copy.
			 *
			 * @param output the output to copy bytes to
			 * @param length The maximum number of bytes to copy
			 * @return the outcome of the copy operation
			 */
			virtual Progress<Length> requestCopyTo(Output &output, Length length = CIO_UNKNOWN_LENGTH) override;

			/**
			 * Instructs the Input that a read buffer consisting of at least the specified number of bytes should be used where relevant.
			 *
			 * @param bytes The requested number of bytes for the read buffer
			 * @return the status and actual number of bytes in the read buffer
			 */
			virtual Progress<std::size_t> requestReadBuffer(std::size_t bytes) noexcept override;

			/**
			 * Gets the read buffer in use.
			 *
			 * @return the read buffer
			 */
			Buffer &getReadBuffer() noexcept;

			/**
			 * Gets the read buffer in use.
			 *
			 * @return the read buffer
			 */
			const Buffer &getReadBuffer() const noexcept;

			/**
			 * Sets the read buffer to use.
			 *
			 * @param buffer The buffer to use
			 * @throw Exception If there is data in the current read buffer and the new buffer is too small to copy it
			 */
			void setReadBuffer(Buffer buffer);

			/**
			 * Gets the current input source.
			 * 
			 * @return the input source 
			 */
			Input *getInput() noexcept;

			/**
			 * Gets the current input source.
			 *
			 * @return the input source
			 */
			const Input *getInput() const noexcept;

			/**
			 * Sets the current input source.
			 * The buffer is also reset.
			 *
			 * @param source The new input source
			 */
			void setInput(Pointer<Input> source);

			/**
			 * Removes the input source and returns it.
			 * 
			 * @return the removed input source 
			 */
			Pointer<Input> takeInput() noexcept;

			/**
			 * Refills the buffer from the underlying input if available.
			*/
			void refillBuffer();

		private:
			/** The metaclass for this class */
			static Class<BufferedInput> sMetaclass;
			
			/** The input source */
			Pointer<Input> mSource;

			/** The read buffer */
			Buffer mBuffer;

			/** The input position of the buffer start */
			Length mPosition;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
