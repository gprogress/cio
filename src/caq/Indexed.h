/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_INDEXED_H
#define CAQ_INDEXED_H

#include "Types.h"

#include "Accessor.h"
#include "Reader.h"
#include "Native.h"

namespace caq
{
	/**
	 * The Indexed array accessor combined two other accessors to use an index array to select data from a data sample array.
	 * This can be used by higher-level libraries to implement things such as palette images and indexed triangle meshes.
	 * Instead of looking up values directly, the index array is first consulted to obtain an integer index, and that  index
	 * is used to read the data array.
	 * 
	 * @tparam D The accessor type for the data sample array
	 * @tparam I The accessor type for the index array
	 */
	template <typename D, typename I>
	class Indexed : public Reader<typename D::Component>
	{
		public:
			/** The underlying component type of this accessor */
			using T = typename D::Component;

			/**
			 * Destructor.
			 */
			inline virtual ~Indexed() noexcept override = default;

			// Overrides for Abstract accessor

			/**
			 * Gets the number of array elements in this accessor.
			 * For the Indexed accessor, this is the number of array elements in the index array.
			 *
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return mIndices.size();
			}

			/**
			 * Gets the number of element components in this accessor.
			 * For the Indexed accessor, this is the number of components in the values array.
			 *
			 * @return The number of components
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return mValues.components();
			}

			/**
			 * Interprets whether this accessor is usable.
			 * The Indexed accessor returns true if the values and indices accessor are usable.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mValues && mIndices;
			}

			// Overrides for Reader accessor


			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The Indexed accessor will return the values accessor if no indices are set, or if the indices are sequential starting from 0.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				Accessor<T> acc;
				if (!mIndices)
				{
					acc.copy(mValues);
				}
				else
				{
					bool sequential = true;
					for (unsigned long long n = 0; n < mIndices.size(); ++n)
					{
						if (mIndices.get(n, 0) != n)
						{
							sequential = false;
							break;
						}
					}

					if (sequential)
					{
						acc.copy(mValues);
					}
					else
					{
						acc.copy(*this);
					}
				}
				return acc;
			}

			/**
			 * Gets a single component value at a given array index.
			 * For the Indexed accessor, this first retrieves the index value from the index array, and then uses that index
			 * to retrieve the component value from the values array.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const override
			{
				return mValues.get(mIndices.get(n, 0), c);
			}

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 * For the Indexed accessor, this first retrieves the index value from the index array, and then uses that index
			 * to retrieve the component values from the values array.
			 *
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const override
			{
				return mValues.retrieve(buffer, mIndices.get(n, 0));
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 * For the Indexed accessor, this first retrieves the index value from the index array for each index, and then uses that index
			 * to retrieve the component values from the values array.
			 * 
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const override
			{
				std::remove_const_t<T> *next = buffer;
				for (unsigned long long n = 0; n < count; ++n)
				{
					auto in = mIndices.get(noff + n, 0);
					next = mValues.retrieve(next, in);
				}
				return next;
			}

			/**
			 * Gets the underlying values accessor.
			 *
			 * @return The values accessor
			 */
			inline D &values() noexcept
			{
				return mValues;
			}

			/**
			 * Gets the underlying values accessor.
			 *
			 * @return The values accessor
			 */
			inline const D &values() const noexcept
			{
				return mValues;
			}

			/**
			 * Gets the underlying indices accessor.
			 * 
			 * @return The indexer accessor
			 */
			inline I &indices() noexcept
			{
				return mIndices;
			}

			/**
			 * Gets the underlying indices accessor.
			 *
			 * @return The indexer accessor
			 */
			inline const I &indices() const noexcept
			{
				return mIndices;
			}

		private:
			/** The underlying data array accessor */
			D mValues;

			/** The underlying index array accessor */
			I mIndices;
	};
}

#endif
