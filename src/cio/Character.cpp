/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Character.h"

#include "FixedText.h"
#include "Parse.h"

#include <cctype>
#include <ostream>

namespace cio
{
	Parse<Character, char> Character::decode(char c) noexcept
	{
		Parse<Character, char> result;
		if (static_cast<unsigned char>(c) <= 0x7F)
		{
			result.value.mCodepoint = static_cast<char32_t>(c);
			result.parsed = 1;
			result.status = Validation::Exact;
		}
		else
		{
			result.terminator = c;
			result.status = Validation::TooShort;
		}
		return result;
	}

	Parse<Character, char16_t> Character::decode(char16_t c) noexcept
	{
		Parse<Character, char16_t> result;
		if (c <= 0xD7FF)
		{
			result.value.mCodepoint = static_cast<char32_t>(c);
			result.parsed = 1;
			result.status = Validation::Exact;
		}
		else
		{
			result.terminator = c;
			result.status = Validation::TooShort;
		}

		return result;
	}

	Parse<Character, char32_t> Character::decode(char32_t c) noexcept
	{
		Parse<Character, char32_t> result;
		if (c <= 0x10FFFF)
		{
			result.value.mCodepoint = c;
			result.parsed = 1;
			result.status = Validation::Exact;
		}
		else
		{
			result.terminator = c;
			result.status = Validation::AboveMaximum;
		}
		return result;
	}

	Parse<Character, char> Character::decode(const char *text, std::size_t length) noexcept
	{
		// Note - we need to force chars to be unsigned for comparisons to work
		Parse<Character, char> result;
		if (!text)
		{
			result.status = Validation::Missing;
		}
		else if (length == 0 || text[0] == 0)
		{
			result.status = Validation::TooShort;
		}
		else
		{
			unsigned char c = text[0];
			if (c <= 0x7F)
			{
				result.value.mCodepoint = static_cast<char32_t>(c);
				result.parsed = 1;
				result.status = Validation::Exact;
				result.terminator = (length > 1) ? text[1] : 0;
			}
			else
			{
				if (length <= 1 || text[1] == 0)
				{
					result.status = Validation::TooShort;
					result.terminator = c;
				}
				else if ((c & 0xE0) == 0xC0)
				{
					unsigned char c2 = text[1];
					if ((c2 & 0xC0) == 0x80)
					{
						char32_t codepoint = (static_cast<char32_t>(c & 0x1F) << 6) | (static_cast<char32_t>(c2 & 0x3F));
						result.value.mCodepoint = codepoint;
						result.parsed = 2;
						result.status = Validation::Exact;
						result.terminator = (length > 2) ? text[2] : 0;
					}
					else
					{
						result.status = Validation::InvalidValue;
						result.terminator = c;
					}
				}
				else if ((c & 0xF0) == 0xE0)
				{
					if (length <= 2 || text[2] == 0)
					{
						result.status = Validation::TooShort;
						result.terminator = c;
					}
					else
					{
						unsigned char c2 = text[1];
						unsigned char c3 = text[2];
						if ((c2 & 0xC0) == 0x80 && (c3 & 0xC0) == 0x80)
						{
							char32_t codepoint = (static_cast<char32_t>(c & 0x0F) << 12) | (static_cast<char32_t>(c2 & 0x3F) << 6)
								| (static_cast<char32_t>(c3 & 0x3F));
							result.value.mCodepoint = codepoint;
							result.parsed = 3;
							result.status = Validation::Exact;
							result.terminator = (length > 3) ? text[3] : 0;
						}
						else
						{
							result.status = Validation::InvalidValue;
							result.terminator = c;
						}
					}
				}
				else if ((c & 0xF8) == 0xF0)
				{
					if (length <= 3 || text[3] == 0)
					{
						result.status = Validation::TooShort;
						result.terminator = c;
					}
					else
					{
						unsigned char c2 = text[1];
						unsigned char c3 = text[2];
						unsigned char c4 = text[3];
						if ((c2 & 0xC0) == 0x80 && (c3 & 0xC0) == 0x80 && (c4 & 0xC0) == 0x80)
						{
							char32_t codepoint = (static_cast<char32_t>(c & 0x07) << 18) | (static_cast<char32_t>(c2 & 0x3F) << 12)
								| (static_cast<char32_t>(c3 & 0x3F) << 6) | (static_cast<char32_t>(c4 & 0x3F));
							result.value.mCodepoint = codepoint;
							result.parsed = 4;
							result.status = Validation::Exact;
							result.terminator = (length > 4) ? text[4] : 0;
						}
						else
						{
							result.status = Validation::InvalidValue;
							result.terminator = c;
						}
					}
				}
				else
				{
					result.status = Validation::InvalidValue;
					result.terminator = c;
				}
			}
		}
		return result;
	}

	Parse<Character, char16_t> Character::decode(const char16_t *text, std::size_t length) noexcept
	{
		Parse<Character, char16_t> result;
		if (!text)
		{
			result.status = Validation::Missing;
		}
		else if (length == 0 || text[0] == 0)
		{
			result.status = Validation::TooShort;
		}
		else
		{
			char16_t c = text[0];
			if (c <= 0x7FFF)
			{
				result.value.mCodepoint = static_cast<char32_t>(c);
				result.parsed = 1;
				result.status = Validation::Exact;
				result.terminator = (length > 1) ? text[1] : 0;
			}
			else if (c <= 0xDBFF)
			{
				if (length == 1 || text[1] == 0)
				{
					result.status = Validation::TooShort;
					result.terminator = c;
				}
				else
				{
					char16_t c2 = text[1];
					if (c2 < 0xDC00)
					{
						result.status = Validation::BelowMinimum;
						result.terminator = c;
					}
					else
					{
						char32_t surrogate = c2 - 0xDC00;
						char32_t codepoint = (c | surrogate) + 0x10000;
						result.value.mCodepoint = codepoint;
						result.parsed = 2;
						result.status = Validation::Exact;
						result.terminator = (length > 2) ? text[2] : 0;
					}
				}
			}
			else
			{
				result.status = Validation::AboveMaximum;
				result.terminator = c;
			}
		}
		return result;
	}

	Parse<Character, char32_t> Character::decode(const char32_t *text, std::size_t length) noexcept
	{
		Parse<Character, char32_t> result;
		if (!text)
		{
			result.status = Validation::Missing;
		}
		else if (length == 0 || text[0] == 0)
		{
			result.status = Validation::TooShort;
		}
		else
		{
			char32_t c = text[0];
			if (c <= 0x10FFFF)
			{
				result.value.mCodepoint = c;
				result.parsed = 1;
				result.status = Validation::Exact;
				result.terminator = (length > 1) ? text[1] : 0;
			}
			else
			{
				result.terminator = c;
				result.status = Validation::AboveMaximum;
			}
		}
		return result;
	}

	std::size_t Character::transcode(const char32_t *input, std::size_t count, char *output, std::size_t capacity) noexcept
	{
		std::size_t offset = 0;
		std::size_t needed = 0;

		if (input)
		{
			if (output)
			{
				while (offset < count && needed < capacity)
				{
					char32_t codepoint = input[offset];
					if (codepoint == 0)
					{
						offset = count;
						std::memset(output + needed, 0, capacity - needed);
					}
					else
					{
						needed += cio::print(codepoint, output + needed, capacity - needed);
						++offset;
					}
				}
			}

			// Unprinted inputs
			while (offset < count)
			{
				char32_t codepoint = input[offset];
				if (codepoint == 0)
				{
					break;
				}

				needed = cio::strlen(codepoint);
				++offset;
			}
		}
		return needed;
	}

	std::size_t Character::transcode(const char16_t *input, std::size_t count, char *output, std::size_t capacity) noexcept
	{
		std::size_t offset = 0;
		std::size_t needed = 0;

		if (input)
		{
			if (output)
			{
				while (offset < count && needed < capacity)
				{
					char16_t c = input[offset];
					if (c == 0)
					{
						offset = count;
						std::memset(output + needed, 0, capacity - needed);
					}
					else if (c <= 0xD7FF)
					{
						needed += cio::print(c, output + needed, capacity - needed);
					}
					else if (c <= 0xDBFF)
					{
						++offset;
						if (offset < count)
						{
							char16_t surrogate = input[offset];
							char32_t codepoint = (c | surrogate) + 0x10000;
							needed += cio::print(codepoint, output + needed, capacity - needed);
						}
					}

					++offset;
				}
			}

			// Unprinted inputs
			while (offset < count)
			{
				char16_t c = input[offset];
				if (c == 0)
				{
					break;
				}
				else if (c <= 0xD7FF)
				{
					char32_t codepoint = static_cast<char32_t>(c);
					needed += cio::strlen(codepoint);
				}
				else if (c <= 0xDBFF)
				{
					++offset;
					if (offset < count)
					{
						char16_t surrogate = input[offset];
						char32_t codepoint = (c | surrogate) + 0x10000;
						needed += cio::strlen(codepoint);
					}
				}

				++offset;
			}
		}

		return needed;
	}

	Character::Character(char c) noexcept
	{
		*this = Character::decode(c);
	}

	Character::Character(char16_t c) noexcept
	{
		*this = Character::decode(c);
	}

	Character::Character(const char *buffer, std::size_t capacity)
	{
		*this = Character::decode(buffer, capacity);
	}

	Character::Character(const char16_t *buffer, std::size_t capacity)
	{
		*this = Character::decode(buffer, capacity);
	}

	Character::Character(const char32_t *buffer, std::size_t capacity)
	{
		*this = Character::decode(buffer, capacity);
	}

	bool Character::isAscii() const noexcept
	{
		return mCodepoint >= 0 && mCodepoint < 128;
	}

	bool Character::isAlphabetic() const noexcept
	{
		return !this->isAscii() || std::isalpha(mCodepoint);
	}

	bool Character::isPhonetic() const noexcept
	{
		return this->isAlphabetic();
	}

	bool Character::isConceptual() const noexcept
	{
		return this->isPhonetic() || this->isDigit();
	}

	bool Character::isPunctuation() const noexcept
	{
		return this->isAscii() && std::ispunct(mCodepoint);
	}

	bool Character::isWhitespace() const noexcept
	{
		return isAscii() && std::isspace(mCodepoint)
			|| mCodepoint == 0x85 // LaTeX next line 
			|| mCodepoint == 0xA0 // HTML non-breaking space
			|| mCodepoint == 0x1680 // Ogham space
			|| (mCodepoint >= 0x2000 && mCodepoint <= 0x200A) // Various HTML and print space
			|| mCodepoint == 0x2028 // line separator
			|| mCodepoint == 0x2029 // paragraph separator
			|| mCodepoint == 0x202F // narrow no break space
			|| mCodepoint == 0x205F // math space
			|| mCodepoint == 0x3000; // CJK ideograph space
	}

	bool Character::isGlyph() const noexcept
	{
		return !isWhitespace() && (!isAscii() || std::isgraph(mCodepoint));
	}

	bool Character::isVisible() const noexcept
	{
		return !isAscii() || std::isgraph(mCodepoint);
	}

	bool Character::isDigit() const noexcept
	{
		return mCodepoint >= '0' && mCodepoint <= '9';
	}

	bool Character::isUppercase() const noexcept
	{
		return this->isAscii() && std::isupper(mCodepoint);
	}

	bool Character::isLowercase() const noexcept
	{
		return this->isAscii() && std::islower(mCodepoint);
	}

	Character Character::toUppercase() const noexcept
	{
		return Character(isAscii() ? static_cast<char32_t>(std::toupper(mCodepoint)): mCodepoint);
	}

	Character Character::toLowercase() const noexcept
	{
		return Character(isAscii() ? static_cast<char32_t>(std::tolower(mCodepoint)) : mCodepoint);
	}

	std::size_t Character::strlen() const noexcept
	{
		return cio::strlen(mCodepoint);
	}

	cio::FixedText<4> Character::print() const noexcept
	{
		cio::FixedText<4> buffer;
		cio::print(mCodepoint, buffer.data(), 4);
		return buffer;
	}

	std::size_t Character::print(char *buffer, std::size_t capacity, std::size_t offset) const noexcept
	{
		return cio::print(mCodepoint, buffer, capacity, offset);
	}

	std::size_t strlen(char value) noexcept
	{
		return static_cast<std::size_t>(value > 0 && value <= 0x7F);
	}

	std::size_t strlen(char16_t value) noexcept
	{
		std::size_t needed = 0;
		if (value > 0)
		{
			if (value <= 0x7F)
			{
				needed = 1;
			}
			else if (value <= 0x7FF)
			{
				needed = 2;
			}
			else if (value <= 0x7FFF)
			{
				needed = 3;
			}
		}
		return needed;
	}

	std::size_t strlen(char32_t value) noexcept
	{
		std::size_t needed = 0;
		if (value > 0)
		{
			if (value <= 0x7F)
			{
				needed = 1;
			}
			else if (value <= 0x7FF)
			{
				needed = 2;
			}
			else if (value <= 0xFFFF)
			{
				needed = 3;
			}
			else if (value <= 0x10FFFF)
			{
				needed = 4;
			}
		}
		return needed;
	}

	std::size_t strlen(const char *value, std::size_t limit) noexcept
	{
		std::size_t count = 0;
		if (value)
		{
			std::size_t remaining = limit;
			cio::Parse<Character, char> decoded = Character::decode(value + count, remaining);
			while (decoded.valid())
			{
				count += decoded.parsed;
				if (remaining < SIZE_MAX)
				{
					remaining -= count;
				}
				decoded = Character::decode(value + count, remaining);
			}
		}
		return count;
	}

	std::size_t strlen(const char16_t *value, std::size_t limit) noexcept
	{
		std::size_t count = 0;
		if (value)
		{
			std::size_t remaining = limit;
			cio::Parse<Character, char16_t> decoded = Character::decode(value + count, remaining);
			while (decoded.valid())
			{
				count += decoded.parsed;
				if (remaining < SIZE_MAX)
				{
					remaining -= count;
				}
				decoded = Character::decode(value + count, remaining);
			}
		}
		return count;
	}

	std::size_t strlen(const char32_t *value, std::size_t limit) noexcept
	{
		std::size_t count = 0;
		if (value)
		{
			std::size_t remaining = limit;
			cio::Parse<Character, char32_t> decoded = Character::decode(value + count, remaining);
			while (decoded.valid())
			{
				count += decoded.parsed;
				if (remaining < SIZE_MAX)
				{
					remaining -= count;
				}
				decoded = Character::decode(value + count, remaining);
			}
		}
		return count;
	}

	CIO_API std::size_t print(char value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t valid = static_cast<std::size_t>(value != 0);
		if (buffer && capacity)
		{
			if (valid)
			{
				buffer[0] = value;
			}

			if (capacity > valid)
			{
				std::memset(buffer + valid, 0, capacity - valid);
			}
		}
		return valid;
	}


	std::size_t print(char32_t value, char *buffer, std::size_t capacity, std::size_t offset) noexcept
	{
		std::size_t needed = 0;
		if (value <= 0x7F)
		{
			if (offset == 0)
			{
				cio::print(static_cast<char>(value), buffer, capacity);
				needed = 1;
			}
		}
		else if (value <= 0x7FF)
		{
			if (offset < 2)
			{
				char bytes[2] =
				{
					static_cast<char>(((value >> 6) & 0x1F) | 0xC0),
					static_cast<char>((value & 0x3F) | 0x80)
				};
				cio::reprint(bytes + offset, 2 - offset, buffer, capacity);
				needed = 2 - offset;
			}
		}
		else if (value <= 0xFFFF)
		{
			if (offset < 3)
			{
				char bytes[3] =
				{
					static_cast<char>(((value >> 12) & 0x0F) | 0xE0),
					static_cast<char>(((value >> 6) & 0x3F) | 0x80),
					static_cast<char>(((value) & 0x3F) | 0x80)
				};
				cio::reprint(bytes + offset, 3 - offset, buffer, capacity);
				needed = 3 - offset;
			}
		}
		else if (value <= 0x10FFFF)
		{
			if (offset < 4)
			{
				char bytes[4] =
				{
					static_cast<char>(((value >> 18) & 0x07) | 0xF0),
					static_cast<char>(((value >> 12) & 0x3F) | 0x80),
					static_cast<char>(((value >> 6) & 0x3F) | 0x80),
					static_cast<char>(((value) & 0x3F) | 0x80),
				};
				cio::reprint(bytes + offset, 4 - offset, buffer, capacity);
				needed = 4 - offset;
			}
		}
		return needed;
	}

	std::size_t print(char16_t value, char *buffer, std::size_t capacity, std::size_t offset) noexcept
	{
		std::size_t printed = 0;
		if (value <= 0xD7FF)
		{
			char32_t codepoint = static_cast<char32_t>(value);
			printed = print(codepoint, buffer, capacity, offset);
		}
		// else unterminated multi-UTF-16 sequence, cannot print
		return printed;
	}

	std::ostream &operator<<(std::ostream &s, const Character &c)
	{
		if (c)
		{
			s << c.print();
		}
		return s;
	}
}