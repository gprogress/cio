/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_DIRECTORY_H
#define CIO_DIRECTORY_H

#include "Types.h"

#include "Filesystem.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
		/**
		 * The CIO Directory extends the base Filesystem protocol to traverse a particular disk directory using the native directory read implementation
		 * as provided by the host operating system for local filesystem directories.
		 */
		class CIO_API Directory : public Filesystem
		{
			public:
				/**
				 * The listNativeEntries method is a convenience method to list all entries in a directory, excluding special entries.
				 * The entries will be returned exactly as provided by the native filesystem.
				 *
				 * @param path The path to the directory
				 * @return the list of entries, excluding special entries
				 * @throw Exception If the directory did not exist or otherwise could not be read
				 */
				static std::vector<std::string> listNativeEntries(const Path &path);

				/**
				 * The list method is a convenience method to list all children of a given directory, excluding special entries.
				 * Each child is constructed as an absolute path by taking the directory path and appending the directory entry.
				 *
				 * @param path The path to the directory
				 * @return the list of children, excluding special entries
				 * @throw Exception If the directory did not exist or otherwise could not be read
				 */
				static std::vector<Path> list(const Path &path);

				/**
				 * The list method lists all children of a given directory that are file types that match one of the given
				 * extensions.
				 * Each child is constructed as an absolute path by taking the directory path and appending the directory entry.
				 *
				 * @param path The path to the directory
				 * @param extensions The list of extensions to match
				 * @return the list of children, excluding special entries
				 * @throw Exception If the directory did not exist or otherwise could not be read
				 */
				static std::vector<Path> list(const Path &path, const std::vector<std::string> &extensions);

				/**
				 * Gets the metaclass for the Input class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct a default (unopened) directory.
				 */
				Directory() noexcept;

				/**
				 * Construct a directory and immediately open the given path in read-only mode.
				 *
				 * @param path the path to open
				 * @throw Exception If the directory did not exist or otherwise could not be read
				 */
				explicit Directory(const Path &path);

				/**
				 * Construct a directory and immediately open the given path with the given modes.
				 *
				 * @param path the path to open
				 * @param modes The open modes
				 * @throw Exception If the directory did not exist or otherwise could not be read
				 */
				Directory(const Path &path, ModeSet modes);

				/**
				 * Construct a directory by transferring the native handle from the given directory.
				 *
				 * @param in The directory to move
				 */
				Directory(Directory &&in) noexcept;

				/**
				 * Move a directory.
				 *
				 * @param in The directory to move
				 * @return this directory
				 */
				Directory &operator=(Directory &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Directory() noexcept override;

				/**
				 * Gets the metaclass for the runtime type of this object.
				 *
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Opens the Directory to read entries and obtains the first entry.
				 * All native directories should have at least two special entries ('.' and '..').
				 *
				 * @param resource The resource to open
				 * @param modes The desired modes
				 * @param factory The protocol factory, which is not used for this class
				 * @return the opened modes
				 * @throw Exception If the directory could not be opened for any reason
				 */
				virtual ModeSet openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory) override;

				/**
				 * Gets the metadata of the current entry as reported by the underlying physical representation.
				 * This includes a copy of the filename entry plus any metadata easily available on the current platform.
				 *
				 * On Windows this reports whether the entry file type is Directory or File, the logical size, and the modes as either read-only or general read-write.
				 *
				 * @return the metadata of the current entry
				 * @throw std::bad_alloc If the memory for the metadata could not be allocated
				 */
				virtual Metadata current() const override;

				/**
				 * Gets the current raw filename entry as reported by the underlying transport layer.
				 * On current platforms this is returned straight from the directory entry and will only
				 * be valid until calling next() or clear().
				 *
				 * @return the current filename entry
				 */
				virtual Text filename() const override;

				/**
				 * Advances to the next directory entry if possible.
				 * The returned state will succeed if an entry was found or fail with Reason::Underflow if no more entries exist.
				 * If a transport error or other failure to read the directory itself occurs, an Exception is thrown with the information
				 * about the error.
				 *
				 * @return Success if another entry was read, Underflow if no more entries exist
				 * @throw Exception If a transport error occurred that prevented reading the directory
				 */
				virtual State next() override;

				/**
				 * Seeks back to the first directory entry.
				 * This uses native operating system methods.
				 *
				 * @return the outcome of rewinding to the first entry, which is either successful or fails with Reason::Underflow if no entries are present
				 * @throw Exception If a transport error occurred that prevented reading the directory
				 */
				virtual State rewind() override;

				/**
				 * Seeks the directory to the given entry described as a relative path.
				 * This typically requires rewinding and scanning the entire directory until it is found.
				 * This will check the current entry (if any) first to minimize effort.
				 *
				 * The base implementation uses the described algorithm to find the current entry.
				 * Subclasses may override for performance or to more directly use native information.
				 *
				 * @param relative The relative path to find
				 * @return the outcome of this operation, which is either successful or fails with Reason::Missing if the entry is not present
				 * @throw Exception If seeking failed for any reason other than the entry not existing
				 */
				virtual State seekToEntry(const Path &relative) override;

				/**
				 * Create an Input subclass instantiation that can read the content for the current directory entry.
				 *
				 * This class uses native operating system methods to handle opening the directory entry using a File, Device,
				 * or other appropriate Input subclass.
				 *
				 * @return the Input to use to read the current entry
				 * @throw Exception If the entry did not exist, was not readable, was a subdirectory, or otherwise could not be opened
				 * @throw std::bad_alloc If allocating the new Input failed
				 */
				virtual std::unique_ptr<Input> openCurrentInput(ModeSet modes = ModeSet::readonly()) const override;

				/**
				 * Create an Input subclass instantiation that can read the content for the resource at the given path.
				 *
				 * This class uses native operating system methods to handle opening the directory entry using a File, Device,
				 * or other appropriate Input subclass.
				 *
				 * @param path The path to the resource to read
				 * @return the Input to use to read
				 * @throw Exception If the entry did not exist, was not readable, was a subdirectory, or otherwise could not be opened
				 * @throw std::bad_alloc If allocating the new Input failed
				 */
				virtual std::unique_ptr<Input> openInput(const Path &path, ModeSet modes = ModeSet::readonly()) const override;

				/**
				 * Checks whether the directory is open and has a native entry currently available.
				 *
				 * @return whether the current directory entry is valid
				 */
				virtual bool traversable() const noexcept override;

				/**
				 * Clears the current state of the Directory. This should close any files, handles, or other resources
				 * and free any memory used.
				 *
				 * It does NOT remove the content of the directory.
				 */
				virtual void clear() noexcept override;

				/**
				 * Opens the given native directory path exactly as provided.
				 *
				 * @param path The path to open
				 * @param modes The requested open modes
				 * @return the status of the open request
				 */
				State openNativeHandle(const char *path, ModeSet modes) noexcept;

				/**
				 * Gets the underlying native directory handle as used by the operating system.
				 *
				 * @return the native handle
				 */
				void *getNativeHandle() noexcept;

				/**
				 * Gets the underlying current native directory entry.
				 *
				 * @return the native entry, or null if none
				 */
				void *getNativeEntry() noexcept;

				/**
				 * Disposes the native directory handle.
				 */
				void disposeNativeHandle() noexcept;

			private:
				/** The metaclass for this class */
				static Class<Directory> sMetaclass;

				/** The OS native handle for the directory */
				void *mDirectory;

				/** The OS native handle for the current entry */
				void *mEntry;
		};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
