/*=====================================================================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
=====================================================================================================================*/
#include "FixedTextTest.h"

#include <cio/test/Assert.h>

#include <cio/FixedText.h>

namespace cio
{
	FixedTextTest::FixedTextTest() :
		cio::test::Suite("cio::FixedText<N>")
	{
		CIO_TEST_METHOD(cio::FixedTextTest, split);
		this->addTestCase("operator==", &FixedTextTest::equalTest);
	}

	FixedTextTest::~FixedTextTest() noexcept = default;

	void FixedTextTest::splitTest()
	{
		FixedText<11> input;
		std::pair<FixedText<11>, FixedText<11>> actual;
		
		// initial test case, input is empty, outputs should be empty
		actual = input.split(':');
		CIO_ASSERT_EQUAL("", actual.first);
		CIO_ASSERT_EQUAL("", actual.second);

		// Test split char being in middle
		input = "EPSG:43265";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("EPSG", actual.first);
		CIO_ASSERT_EQUAL("43265", actual.second);

		// Tests split char being up front
		input = ":432";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("", actual.first);
		CIO_ASSERT_EQUAL("432", actual.second);

		// Tests split char being at end
		input = "FOOBAR:";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("FOOBAR", actual.first);
		CIO_ASSERT_EQUAL("", actual.second);

		// Tests only having the split char
		input = ":";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("", actual.first);
		CIO_ASSERT_EQUAL("", actual.second);

		// Tests having multiple split chars
		input = ":::";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("", actual.first);
		CIO_ASSERT_EQUAL("::", actual.second);

		// Tests having multiple split chars with other junk
		input = "AA:::BBB";
		actual = input.split(':');
		CIO_ASSERT_EQUAL("AA", actual.first);
		CIO_ASSERT_EQUAL("::BBB", actual.second);
	}

	void FixedTextTest::equalTest()
	{
		FixedText<11> a;
		FixedText<11> b;

		CIO_ASSERT_EQUAL(a, b);
	}
}

