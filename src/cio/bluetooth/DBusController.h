/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_DBUSCONTROLLER_H
#define CIO_BLUETOOTH_DBUSCONTROLLER_H

#include "Types.h"

#include "Controller.h"
#include "DBusClient.h"

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO D-Bus Controller implements the Controller interface for local adapters using the BlueZ D-Bus technology.
		 * This is the recommended implementation to use on Linux.
		 */
		class CIO_BLUETOOTH_API DBusController : public Controller
		{
			public:							
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/** The text for the BlueZ D-Bus domain. Currently org.bluez. */
				static const char *BLUEZ_DOMAIN;
				
				/** The text for the D-Bus interface for BlueZ adapters. Currently org.bluez.Adapter1. */
				static const char *BLUEZ_ADAPTER_IFACE;
				
				/** The text for the D-Bus interface for BlueZ peripherals. Currently org.bluez.Device1. */
				static const char *BLUEZ_PERIPHERAL_IFACE;
				
				/**
				 * Construct an unconnected D-Bus Controller.
				 */
				DBusController() noexcept;
				
				/**
				 * Construct a D-Bus Controller and immediately open the given device.
				 *
				 * @param in The device to open
				 */
				DBusController(const Device &in);
				
				/**
				 * Move constructor for D-Bus Controller.
				 * 
				 * @param in The controller to move
				 */
				DBusController(DBusController &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Controller.
				 *
				 * @param in The controller to move
				 * @return this controller
				 */
				DBusController &operator=(DBusController &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				virtual ~DBusController() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the D-Bus Controller state.
				 */				
				virtual void clear() noexcept override;
				
				/**
				 * Performs validation checks to determine if the Bluetooth controller is usable and if not, attempt to determine why.
				 * The returned Exception object contains the success state, and if marked as a failure, a general reason why and a specific error message.
				 * 
				 * The DBus Controller will validate that the current user is a member of the bluetooth group. If so, Bluetooth is assumed to be usable.
				 * 
				 * @return an exception object indicating whether Bluetooth support is usable and, if not, why not
				 */
				virtual Exception validate() override;
				
				// Connection management
				
				/**
				 * Opens the currently associated device, if any.
				 * This opens the D-Bus shared system bus and computes the D-Bus object path if the device if one was set.
				 */
				virtual void openCurrentDevice() override;
				
				/**
				 * Checks whether the controller is currently open.
				 *
				 * @return whether the controller is currently open
				 */
				virtual bool isOpen() const noexcept override;
				
				/**
				 * Closes the D-Bus controller. This clears the device object path and releases the reference to the shared D-Bus system bus.
				 */
				virtual void close() noexcept override;
				
				// D-Bus specific methods
				
				/**
				 * Gets the connection to the D-Bus system bus.
				 *
				 * @return the D-Bus system connection
				 */
				DBusClient &getSystemBus() noexcept;
				
				/**
				 * Gets the connection to the D-Bus system bus.
				 *
				 * @return the D-Bus system connection
				 */
				const DBusClient &getSystemBus() const noexcept;
				
				/**
				 * Gets the connection to the D-Bus system bus,
				 * creating a new connection if none exists.
				 *
				 * @return the D-Bus system connection
				 */
				DBusClient &getOrCreateSystemBus();
				
				/**
				 * Sets the connection to the D-Bus system bus.
				 * This controller will take ownership of the connection.
				 *
				 * @param connection The D-Bus system connection to use
				 */
				void setSystemBus(const DBusClient &connection) noexcept;
				
				// Device discovery
				
				virtual Device findDefaultDevice() override;
							
				/**
				 * Scans the local system to find all active Bluetooth devices.
				 *
				 * @return the list of known devices
				 * @throw cio::Exception If there was a failure in setting up the HCI connection to scan for local devices
				 * @throw std::bad_alloc If the memory for the list could not be allocated
				 */
				virtual std::vector<Device> discoverLocalDevices() override;
				
				/**
				 * Reads the local name of the currently open device.
				 * This reads the "Name" property from the D-Bus object's org.bluez.Adapter1 interface if one was set, or prints the empty string otherwise.
				 *
				 * @param text The buffer to store the device name into
				 * @param length the maximum number of text bytes available to store
				 * @return the total number of bytes needed for the name (which may be more or less than the buffer)
				 */
				virtual std::size_t readLocalName(char *text, std::size_t length) override;
				
				/**
				 * Reads the local device address of the currently open device.
				 * This reads and parses the "Address" property from the D-Bus object's org.bluez.Adapter1 interface if one was set, or returns the empty address otherwise.
				 *
				 * @return the local device address
				 */
				virtual DeviceAddress readLocalAddress() override;
				
				/**
				 * Uses the controller to start the Bluetooth Low Energy discovery process.
				 * This is a mode setting that enables the scanning mode. The D-Bus Controller does not require administrative privileges to start or stop scans.
				 * Scanning is inherently asynchronous so use listenForAdvertisements method to actually receive the scan results.
				 *
				 * This attempts to read the "Discovering" property from the D-Bus object's org.bluez.Adapter1 interface.
				 * If true, then scanning is already in progress and this method just returns State::Exists.
				 * Otherwise, this invokes the "StartDiscovery" method on the org.bluez.Adapter1 interface.
				 * 
				 * @warning This does not wait until the device has actually transitioned to discovery mode.
				 * 
				 * @param scanner The scanner to use for parameters settings
				 * @return the status which should either be Success if scanning was started or Exists if scanning was already in progress
				 */
				virtual State startLowEnergyScan() override;
				
				/**
				 * Uses the controller to recieve advertisements for peripherals for an execution period.
				 * The receiver callback function is called once for each device advertisement until the execution's timer has elapsed or the execution
				 * has been asynchronously canceled by another thread.
				 *
				 * Currently, this implementation simply waits until the entire execution time has elapsed, then synchronizes the current state of discovered
				 * devices with the in-memory cache and reports all known peripherals. Future versions may support more efficient incremental scanning.
				 * 
				 * @param receiver The callback function to receive each discovered peripheral advertisement
				 * @param exec The executor that controls the execution time and cancelation mechanism
				 * @return the result of the scanning in terms of number of advertisements and final status
				 */
				virtual Progress<std::size_t> listenForAdvertisements(AdvertisementListener &listener) noexcept override;
				
				/**
				 * Uses the controller to stop the Bluetooth Low Energy discovery process if it was enabled.
				 * This is a mode setting that disables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 *
				 * This attempts to read the "Discovering" property from the D-Bus object's org.bluez.Adapter1 interface.
				 * If true, then scanning is in progress and this method invokes the "StopDiscovery" method on the org.bluez.Adapter1 interface.
				 * Otherwise this just returns State::Missing.
				 * 
				 * @return the status which should either be Success if scanning was stopped or Missing if scanning was not enabled at all
				 */
				virtual State stopLowEnergyScan() override;
				
				/**
				 * Uses the current device controller to establish a connection to the given remote peripheral.
				 *
				 * This method computes the object path of the specified peripheral then reads its "Connected" property from its org.bluez.Device1 interface.
				 * If the property is true, the connection already exists and is reused and this method just returns State::Exists.
				 * Otherwise, the "Connect" method is invoked on the org.bluez.Device1 interface.
				 * This method will then wait until the "Connected" property becomes true, eventually throwing an exception if that times out.
				 *
				 * @param peripheral The peripheral to connect to
				 * @return State::Success upon completion or State::Exists if the peripheral was already connected
				 * @throw cio::Exception If the process of connecting failed
				 */ 
				virtual State startConnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Disconnects the currently active connection to the given remote peripheral.
				 * This method computes the object path of the specified peripheral then reads its "Connected" property from its org.bluez.Device1 interface.
				 * If the property is true, the connection exists and the "Disconnect" method is invoked on the org.bluez.Device1 interface.
				 * Otherwise, the method simply returns State::Missing.
				 * This method does NOT wait until the disconnection is completed.
				 *
				 * @param peripheral The peripheral to disconnect from
				 * @return State::Success upon completion or State::Missing if the peripheral was not actually connected
				 * @throw cio::Exception If no local device is selected or if the process of disconnecting failed
				 */
				virtual State startDisconnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Uses the current device controller to pair with the given remote peripheral for authentication.
				 * This method computes the object path of the specified peripheral then reads its "Paired" property from its org.bluez.Device1 interface.
				 * If the property is true, the pairing already exists and is reused and this method just returns State::Exists.
				 * Otherwise, the "Pair" method is invoked on the org.bluez.Device1 interface.
				 * This method will then wait until the "Paired" property becomes true, eventually throwing an exception if that times out.
				 *
				 * @note This method currently does not provide for advanced agent-based pairing to negotiate input/output capabilities.
				 * This will generally cause new pairing to error out with an "Authentication Failed" error until we figure this out.
				 *
				 * @param peripheral The peripheral to pair with
				 * @return State::Success upon completion or State::Exists if the peripheral was already paired
				 * @throw cio::Exception If the process of pairing failed
				 */ 
				virtual State startPairing(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Unpairs the current device with the given peripheral.
				 * This method computes the object path of the specified peripheral then reads its "Paired" property from its org.bluez.Device1 interface.
				 * If the property is true, the connection exists and the "CancelPairing" method is invoked on the org.bluez.Device1 interface.
				 * Otherwise, the method simply returns State::Missing.
				 * This method does NOT wait until the unpairing is completed.
				 *
				 * @param peripheral The peripheral to unpair from
				 * @return State::Success upon completion or State::Missing if the peripheral was not actually paired
				 * @throw cio::Exception If no local device is selected or if the process of unpairing failed
				 */
				virtual State startUnpairing(const Peripheral &peripheral, Task &exec) override;
				
				// DBUs adapter helpers
				
				/**
				 * Scans the D-Bus Object Manager to locate all local and peripheral devices currently present.
				 */
				void populateDeviceCache();
				
				/**
				 * Scans the D-Bus Object Manager to locate all local and peripheral devices currently present,
				 * reusing the existing device cache if a previous scan was completed.
				 */
				void synchronizeDeviceCache();
				
				/**
				 * Prints the D-Bus object path for the given local device.
				 *
				 * @param device The device
				 * @param text The text buffer to print into
				 * @param length The maximum number of text bytes to print to the buffer
				 * @return the actual number of text bytes needed for the device object path
				 */
				std::size_t printAdapterPath(const Device &device, char *text, std::size_t length) const noexcept;
				
				/**
				 * Updates the in-memory device cache properties of the given D-Bus object.
				 *
				 * @param object The D-Bus object path
				 */
				void updateDeviceInfo(const char *object);
				
				/**
				 * Convenience method to get a property from the current device object using the org.bluez.Adapter1 interface.
				 *
				 * @param property The property to get
				 * @return the value of the property
				 * @throw std::bad_alloc If allocating memory failed
				 */
				Variant getAdapterProperty(const char *property) const;
		
				/**
				 * Convenience method to call a particular D-Bus method that takes no arguments on the current device D-Bus object via the org.bluez.Adapter1 interface.
				 * The method waits until a response is received from D-Bus.
				 *
				 * @param command The command name
				 * @return the status of the command execution
				 */
				State invokeAdapterCommand(const char *command);
		
				// DBus peripheral helpers
				
				/**
				 * Prints the D-Bus object path for the given peripheral using the currently associated local device.
				 *
				 * @param address The address of the peripheral
				 * @param text The text buffer to print into
				 * @param length The maximum number of text bytes to print to the buffer
				 * @return the actual number of text bytes needed for the peripheral object path
				 */
				std::size_t printPeripheralPath(const DeviceAddress &address, char *text, std::size_t length) const noexcept;
				
				/**
				 * Updates the in-memory peripheral cache properties of the given D-Bus object.
				 *
				 * @param object The D-Bus object path
				 */
				void updatePeripheralInfo(const char *peripheralObject);
				
			private:
				/** The metaclass for this class */
				static Class<DBusController> sMetaclass;
				
				/** D-Bus system bus */
				DBusClient mSystemBus;
				
				/** Path to the currently open local adapter as used for D-Bus invocations */
				cio::Path mAdapterPath;
				
				/** Map of D-Bus paths to known devices */
				std::map<cio::Path, Device> mKnownDevices;
				
				/** Currently known peripherals visible to the current device */
				std::vector<Peripheral> mKnownPeripherals;
				
		};
	}
}

#endif

