/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TASK_H
#define CIO_TASK_H

#include "Types.h"

#include "Exception.h"
#include "Length.h"

#include <chrono>
#include <condition_variable>
#include <exception>
#include <mutex>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Task class provides a thread-safe shared state for asynchronous execution of tasks or operations.
	 * It provides capabilities to set execution timeouts with cooperative polling, cancelation and interrupt mechanisms
	 * (including hooks to interrupt OS-level system calls when relevant), progress reporting, and asychronous success and failure states.
	 */ 
	class CIO_API Task
	{
		public:
			/**
			 * Construct an empty execution state.
			 */
			Task() noexcept;
			
			/**
			 * Constructs a Task and starts it immediately using the given timeout.
			 * This is equivalent to constructing a task and then calling this->start(timeout)
			 * however it is more efficient since it does not require locks or checking existing status.
			 * 
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			explicit Task(std::chrono::milliseconds timeout, Logger *logger = nullptr) noexcept;
			
			/**
			 * Constructs a Task and starts it immediately using the given timeout and maximum progress.
			 * This is equivalent to constructing a task and then calling this->start(timeout)
			 * however it is more efficient since it does not require locks or checking existing status.
			 * 
			 * @param maxProgress The maximum progress report count
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			explicit Task(Length maxProgress, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;
			
			/**
			 * Constructs a Task with a timeout and a given action.
			 *
			 * @param action The action
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			explicit Task(Action action, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;	
			
			/**
			 * Constructs a Task with a timeout and a given action.
			 *
			 * @param action The action
			 * @param maxProgress The maximum number of progress units
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			Task(Action action, Length maxProgress, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;
			
			/**
			 * Constructs a Task with a timeout and a given action.
			 *
			 * @param action The action
			 * @param maxProgress The maximum number of progress units
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			Task(Action action, Status status, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;

			/**
			 * Constructs a Task with a timeout and a given action.
			 *
			 * @param action The action
			 * @param maxProgress The maximum number of progress units
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			Task(Action action, Status status, Length maxProgress, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;

			/**
			 * Constructs a Task with a timeout and a given initial progress state.
			 * This can be used to efficiently set up a status and maximum progress count.
			 *
			 * If the initial progress status is considered a "running" status (at least Started but not Success)
			 * then the task is also started. 
			 *
			 * @param status The initial progress status
			 * @param timeout The timeout
			 * @param logger The logger to use or nullptr for none
			 */
			explicit Task(Progress<Length> progress, std::chrono::milliseconds timeout = std::chrono::milliseconds::max(), Logger *logger = nullptr) noexcept;	
			
			/**
			 * Construct a copy of a task.
			 * This will copy the execution state, timeout, start time, and interruptor.
			 * The input task will be locked for the duration.
			 *
			 * @param task The task to copy
			 */
			Task(const Task &task);
			
			/**
			 * Construct a task by moving.
			 * This will move the execution state, timeout, start time, and interruptor.
			 * The input task will be locked for the duration and will be reset to a non-started state.
			 *
			 * @warning Using this on an actively running task can interfere with the input task execution.
			 *
			 * @param task The task to move
			 */
			Task(Task &&task) noexcept;
			
			/**
			 * Copies a task.
			 * This will copy the execution state, timeout, start time, and interruptor.
			 * Both this and the input task will be locked for the duration.
			 *
			 * @warning Using this if this task is actively running can interfere with its task execution.
			 *
			 * @param in The task to copy
			 * @return this task
			 */
			Task &operator=(const Task &in);
			
			/**
			 * Moves a task.
			 * This will move the execution state, timeout, start time, and interruptor.
			 * Both this and the input task will be locked for the duration.
			 *
			 * @warning Using this if either this task or the input task are actively running can interfere
			 * with the execution of both tasks.
			 *
			 * @param in The task to cmove
			 * @return this task
			 */
			Task &operator=(Task &&in) noexcept;
			
			/**
			 * Destructor.
			 */
			~Task() noexcept;
			
			/**
			 * Clears the task.
			 * This locks the task and then resets it state.
			 *
			 * @warning Using this if this task is actively running can interfere with its task execution.
			 */
			void clear() noexcept;
			
			/**
			 * Marks the task as requested if it is not currently running.
			 *
			 * @return the actual state after this operation
			 */
			State request() noexcept;

			/**
			 * Starts an execution pass reusing the previously set timeout.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start() noexcept;
			
			/**
			 * Starts an execution pass reusing the previously set timeout.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @param action The action to start
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start(Action action) noexcept;

			/**
			 * Starts an execution pass with the given timeout for execution.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @param action The action to start
			 * @param timeout The desired timeout
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start(Action action, std::chrono::milliseconds timeout) noexcept;

			/**
			 * Starts an execution pass with the given timeout for execution.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @param action The action to start
			 * @param maxProgress The maximum progress units
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start(Action action, Length maxProgress) noexcept;

			/**
			 * Starts an execution pass with the given timeout for execution, using the given start time point.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @param action The action to start
			 * @param startTime The start time to use for the execution
			 * @param timeout The desired timeout
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start(Action action, std::chrono::time_point<std::chrono::steady_clock> startTime, std::chrono::milliseconds timeout) noexcept;
			
			/**
			 * Starts an execution pass with the given timeout for execution, using the given start time point.
			 * The task status is set to Started if it is not already at least Partial.
			 *
			 * @param action The action to start
			 * @param maxProgress The maximum progress units
			 * @param startTime The start time to use for the execution
			 * @param timeout The desired timeout
			 * @return the remaining time and current status
			 */
			Progress<std::chrono::milliseconds> start(Action action, Length maxProgress, std::chrono::time_point<std::chrono::steady_clock> startTime, std::chrono::milliseconds timeout) noexcept;

			/**
			 * Resets the task to remove errors and set Status to None.
			 * This is useful for recurring tasks that might be executed many times with the same parameters.
			 * If the task has been started but not yet completed, this will wait for completion.
			 */
			void reset() noexcept;

			/**
			 * Gets the Action that this task is performing.
			 * This is intended to be informative to the user.
			 *
			 * @return the action
			 */
			Action action() const noexcept;

			/**
			 * Sets the Action that this task is performing.
			 * This is intended to be informative to the user.
			 *
			 * @param action the action
			 */
			void action(Action action) noexcept;

			/**
			 * Checks the tasks's current status.
			 *
			 * @return the current status
			 */
			Status status() const noexcept;

			/**
			 * Gets the current execution state.
			 *
			 * @return the current execution state
			 */
			State state() const noexcept;

			/**
			 * Gets the current execution state and progress so far.
			 * 
			 * @return the progress
			 */
			Progress<Length> progress() const noexcept;
			
			/**
			 * Checks whether execution is currently running.
			 *
			 * @return whether the task has been started but not yet completed
			 */
			bool running() const noexcept;
			
			/**
			 * Checks whether execution has completed either by success, failure, or cancellation.
			 *
			 * @return whether the task was started and completed
			 */
			bool completed() const noexcept;
			
			/**
			 * Checks to see how much of the original execution time is remaining and what the current
			 * execution status is.
			 *
			 * This method does not perform any updates or cancellation if the timeout is exceeded
			 * and does not modify execution state.
			 *
			 * @return the execution time remaining and current status
			 */
			Progress<std::chrono::milliseconds> remaining() const noexcept;
			
			/**
			 * Polls to check how much execution time is remaining and the current executation status,
			 * canceling the execution if the timeout has been exceeded and the execution is not yet completed.
			 * If the task has been paused, this method will block until it has been resumed or canceled.
			 * If the task has been canceled, this method will complete the task as Abandoned.
			 *
			 * This method is intended to be used by cooperative tasks before any substantive processing
			 * so they can either set timeouts on OS-level calls or bail out if appropriate.
			 *
			 * If a logger is set, this will also notify the logger of time updates.
			 *
			 * @return the execution time remaining and current status
			 */
			Progress<std::chrono::milliseconds> poll() noexcept;
			
			/**
			 * Cancels the execution if it has not already completed.
			 * This changes the execution state to Status::Canceled, calls the interrupt handler if set,
			 * and notifies all waiting listeners of the execution state change.
			 *
			 * This is equivalent to calling interrupt(Status::Canceled).
			 *
			 * @return the actual status, which is either Status::Canceled or a completion status if the task
			 * already completed before cancelation
			 */ 
			State cancel(Reason reason = Reason::Canceled) noexcept;
			
			/**
			 * Attempts to pause execution if it has not already completed or been canceled.
			 * This changes the execution state to Status::Paused, calls the interrupt handler if set,
			 * and notifies all waiting listeners of the execution state change.
			 *
			 * All future calls to poll(), progress(), and waitToResume() will block until resume() or pause(false) are called,
			 * or until the operation is canceled or completed.
			 *
			 * @return the actual status, which is either Status::Paused, Status::Canceled, or a completion status if the task
			 * already completed before pause
			 */
			State pause() noexcept;
			
			/**
			 * Attempts to resume execution if it was paused.
			 * This will set the execution state to Started (if no progress has been made) or Partial.
			 * If the task was not paused, this just returns the current state.
			 *
			 * All future calls to poll(), progress(), and waitToResume() will stop waiting upon notification from this method.
			 *
			 * @return the actual status, which represents the current status of the task after unpause
			 */
			State resume() noexcept;
			
			/**
			 * Attempts to pause or resume execution as specified by the provided value.
			 * If value is true, this calls pause(). If value is false, this calls resume().
			 *
			 * @return the status after the pause or resume action
			 */
			State pause(bool value) noexcept;
			
			/**
			 * Interupts the execution if it has not already completed with Reason::Interrupted.
			 * This calls the interrupt handler if set, and notifies all waiting listeners, but does not otherwise change state.
			 * This can be used to break execution tasks out of low-level system calls.
			 *
			 * @return the actual status
			 */ 
			State interrupt() noexcept;
			
			/**
			 * Interupts the execution if it has not already completed, providing the given status to the handler.
			 * This calls the interrupt handler if set, and notifies all waiting listeners, and updates the current reason.
			 *
			 * This can be used to break subtasks out of low-level system calls or execution loops.
			 *
			 * @param the status to provide to the interrupt handler
			 * @return the actual status
			 */ 
			State interrupt(Reason reason) noexcept;		
			
			/**
			 * Reports some progress on execution.
			 *
			 * The execution state's progress is updated to add the given count for progress units achieved.
			 *
			 * If the achieved progress is nonzero, this indicate some partial work has been completed.
			 *
			 * If a logger is set, this will also notify the logger of the accumulated progress after applying this update.
			 *
			 * This also polls for timeout and returns the results.
			 *
			 * @param achieved The amount of progress units achieved by this update
			 * @return the poll results for timeout
			 */
			Progress<std::chrono::milliseconds> update(Length achieved) noexcept;
			
			/**
			 * Reports some progress on execution.
			 *
			 * The execution state's progress is updated to add the given count for both progress units achieved
			 * and progress units total if new progress units were discovered.
			 *
			 * If the achieved progress is nonzero, this indicate some partial work has been completed.
			 * If the discovered progress units is nonzero, this indicates more work has been added to the task.
			 *
			 * If a logger is set, this will also notify the logger of the accumulated progress after applying this update.
			 *
			 * This also polls for timeout and returns the results.
			 *
			 * @param achieved The amount of progress units achieved by this update
			 * @param discovered Any new progress units (completed or not) discovered during this update
			 * @return the poll results for timeout
			 */
			Progress<std::chrono::milliseconds> update(Length achieved, Length discovered) noexcept;
			
			/**
			 * Finishes execution successfully.
			 * This changes the execution state to Status::Success and notifies all listeners.
			 */			
			void succeed(Reason r = Reason::None) noexcept;
			
			/**
			 * Finishes execution with the given status.
			 * This changes the execution state to the given status and notifies all listeners.
			 *
			 * @warning If the given status is not actually a completion state, this may confuse users
			 * of this class.
			 *
			 * @param status The final state of the execution
			 */	
			void complete(Outcome outcome = Outcome::Completed, Reason reason = Reason::None) noexcept;
			
			/**
			 * Finishes execution in failure due to the given reason.
			 * 
			 * @param r The reason for failure
			 */
			void fail(Reason r = Reason::Unknown) noexcept;
			
			/**
			 * Finishes execution in failure due to the given exception being thrown.
			 * This adopts the given exception into the final execution state.
			 * 
			 * @param e The exception representing the failure to adopt
			 */
			void fail(Exception &&e) noexcept;
			
			/**
			 * Finishes execution in failure due to the given exception being thrown.
			 * This adopts the given exception into the final execution state.
			 * 
			 * @param e The exception representing the failure to adopt
			 */
			void fail(std::exception_ptr e) noexcept;
			
			/**
			 * Finishes execution in failure due to the given exception being thrown.
			 * This adopts the given exception into the final execution state.
			 * 
			 * @param e The exception representing the failure to adopt
			 * @param status The exact nature of the failure status
			 */
			void fail(std::exception_ptr e, Reason reason) noexcept;
			
			/**
			 * Sets an interrupt handler to apply interrupt states.
			 * This should generally be set by cooperative tasks before long-running system operations
			 * such as file or device I/O to provide the means to cancel execution of these native tasks.
			 * The status that generated the interruption is passed to the interruptor to allow for chained states
			 * such as a parent task interrupting a child task to cancel it, and the interruptor returns the actual
			 * status after interruption (if known).
			 *
			 * @param interruptor The interrupt handler to use
			 * @return the current execution status
			 */
			State setInterruptor(std::function<State (Reason)> &&interruptor) noexcept;
			
			/**
			 * Sets an interrupt handler to apply interrupt states by sending them to the given subtask.
			 * This is a convenience method to set up the interruptor to call the subtask's interrupt method.
			 *
			 * @param subtask The subtask to use for interruption
			 * @return the current execution status
			 */
			State setSubtaskInterruptor(Task &subtask);
			
			/**
			 * Gets the logger currently in use for progress, time, and other reporting if any.
			 *
			 * @return the current task logger, or null if none
			 */
			Logger *getLogger() const noexcept;
			
			/**
			 * Sets the logger to use for progress, time, and other reporting if any.
			 *
			 * @param logger the current task logger, or null if none
			 */
			void setLogger(Logger *logger) noexcept;
			
			/**
			 * Unsets the logger to use for progress, time, and other reporting if any.
			 */
			void unsetLogger() noexcept;
			
			/**
			 * Waits indefinitely until any execution state change is noticed.
			 * Note that multiple updates may occur before this method notices and returns.
			 *
			 * @return the current execution status 
			 */
			State waitForUpdate() const noexcept;
			
			/**
			 * Waits until any execution state change is noticed.
			 * Note that multiple updates may occur before this method notices and returns.
			 *
			 * @return the current execution status
			 */
			State waitForUpdate(std::chrono::milliseconds timeout) const noexcept;
			
			/**
			 * Waits indefinitely until execution completes either by success or failure.
			 *
			 * @return the final execution status 
			 */
			State waitForCompletion() const noexcept;
			
			/**
			 * Waits up to the given timeout until execution completes either by success or failure.
			 *
			 * @param timeout The desired timeout
			 * @return the final execution status, or Status::Timeout if the timeout was exceeded
			 */
			State waitForCompletion(std::chrono::milliseconds timeout) const noexcept;
			
			/**
			 * Waits indefinitely until execution completes successfully.
			 * If execution fails, this throws the exception that caused the failure.
			 *
			 * @return the final execution status 
			 * @throw std::exception If execution failed
			 */
			State waitForSuccess() const;
			
			/**
			 * Waits up to the given timeout until execution completes successfully.
			 * If execution fails, this throws the exception that caused the failure.
			 *
			 * @param timeout The desired timeout
			 * @return the final execution status or Status::Timeout if the timeout was exceeded
			 * @throw std::exception If execution failed
			 */
			State waitForSuccess(std::chrono::milliseconds timeout) const;
			
			/**
			 * Waits indefinitely for execution to resume if it was paused.
			 * This returns immediately if the task isn't paused.
			 *
			 * @return the status after resume
			 */
			State waitToResume() const noexcept;
						
			/**
			 * Waits up to the given timeout for execution to resume if it was paused.
			 * This returns immediately if the task isn't paused.
			 *
			 * @return the status after resume
			 */
			State waitToResume(std::chrono::milliseconds timeout) const noexcept;
			
			// All functions below lock() require obtaining a lock to be thread-safe
			
			/**
			 * Acquires a lock on the execution state to directly modify it.
			 *
			 * @return the lock on the execution state
			 */
			std::unique_lock<std::mutex> lock() const noexcept;
			
			/**
			 * Waits indefinitely until execution completes either by success or failure,
			 * returning with a lock on the execution state. This is primarily useful
			 * as a helper method for other classes to implement additional completion logic such as result
			 * processing without race conditions.
			 *
			 * @return the lock on the execution state
			 */
			std::unique_lock<std::mutex> lockForCompletion() const noexcept;
			
			/**
			 * Waits up to the specified timeout until execution completes either by success or failure,
			 * returning with a lock on the execution state regardless of completion. This is primarily useful
			 * as a helper method for other classes to implement additional logic such as result
			 * processing without race conditions.
			 *
			 * @param timeout The desired timeout
			 * @return the lock on the execution state
			 */
			std::unique_lock<std::mutex> lockForCompletion(std::chrono::milliseconds timeout) const noexcept;
			
			/**
			 * Gets the start time of task execution.
			 * 
			 * @return the start time of the task
			 */
			std::chrono::time_point<std::chrono::steady_clock> getStartTime() const noexcept;

			/**
			 * Manually overrides the start time of task execution.
			 * This should not normally need to be done.
			 *
			 * @param time the start time of the task
			 */
			void setStartTime(std::chrono::time_point<std::chrono::steady_clock> time) noexcept;

			/**
			 * Sets the execution timeout. This can affect existing execution to extend or reduce time, but
			 * normally should be set prior to execution start.
			 *
			 * The default if not otherwise set is 0.
			 *
			 * @param timeout The desired execution timeout
			 */			
			void setTimeout(std::chrono::milliseconds timeout) noexcept;
			
			/**
			 * Disables the execution timeout. In practice this sets the timeout to the maximum possible value
			 * which should never be reached.
			 */
			void unsetTimeout() noexcept;
			
			/**
			 * Gets the execution timeout.
			 *
			 * @return the execution timeout
			 */
			std::chrono::milliseconds getTimeout() const noexcept;
			
			/**
			 * Checks to see how long this task has been executing and what the current
			 * execution status is.
			 *
			 * This method does not perform any updates or cancellation if the timeout is exceeded
			 * and does not modify execution state.
			 *
			 * @return the execution time remaining and current status
			 */ 
			Progress<std::chrono::milliseconds> checkElapsed() const noexcept;
			
			/**
			 * Checks to see how much of the original execution time is remaining and what the current
			 * execution status is.
			 *
			 * This method does not perform any updates or cancellation if the timeout is exceeded
			 * and does not modify execution state.
			 *
			 * @return the execution time remaining and current status
			 */ 
			Progress<std::chrono::milliseconds> checkRemaining() const noexcept;
			
			/**
			 * Sets the progress amount and status.
			 *
			 * @param amount The amount of progress to add
			 * @param discovered Any new progress units (completed or not) discovered during this update
			 */
			void executeProgress(Length achieved, Length discovered) noexcept;
			
			/**
			 * Performs the steps to execute a copy of an input task.
			 * This will copy the execution state, timeout, start time, and interruptor.
			 * No locks are obtained and it is up to the caller to ensure thread safety.
			 *
			 * @param in The task to copy
			 */
			void executeTaskCopy(const Task &in);
			
			/**
			 * Performs the steps to execute a move of an input task.
			 * This will move the execution state, timeout, start time, and interruptor.
			 * No locks are obtained and it is up to the caller to ensure thread safety.
			 *
			 * @param in The task to copy
			 */
			void executeTaskMove(Task &&in) noexcept;
			
			/**
			 * Performs the steps to clear the task level state.
			 * This will clear the execution state, timeout, start time, and interruptor.
			 * No locks are obtained and it is up to the caller to ensure thread safety.
			 */
			void executeTaskClear() noexcept;
			
			/**
			 * Performs the steps to execute a task start.
			 * 
			 * @param startTime the official start time of the task
			 */
			Progress<std::chrono::milliseconds> executeStart(std::chrono::time_point<std::chrono::steady_clock> startTime) noexcept;
			
			/**
			 * Polls to check how much execution time is remaining and the current execution status,
			 * canceling the execution if the timeout has been exceeded and the execution is not yet completed.
			 * If the task has been paused, this method will block until it has been resumed or canceled.
			 * If the task has been canceled, this method will complete the task as Abandoned.
			 *
			 * This method optimizes for the case in which the caller already has the lock and wants to keep it after the method returns.
			 *
			 * If a logger is set, this will also notify the logger of time updates.
			 *
			 * @param lock The currently held lock on this task's mutex
			 * @return the execution time remaining and current status
			 */
			Progress<std::chrono::milliseconds> executePoll(std::unique_lock<std::mutex> &lock) noexcept;
			
			/**
			 * Notifies waiting threads that something interesting happened.
			 */
			void notify() const noexcept;
			
		private:
			/** Mutex guarding execution state */
			mutable std::mutex mMutex;
			
			/** Condition variable to notify updates on execution state */
			mutable std::condition_variable mCondition;
			
			/** Current progress and status */
			Progress<Length> mProgress;
			
			/** Current exception, if any */
			std::exception_ptr mException;

			/** Logger for progress reports and such */
			Logger *mLogger;
			
			/** Original timeout provided to start */
			std::chrono::milliseconds mTimeout;
			
			/** Time point at which execution was started */
			std::chrono::time_point<std::chrono::steady_clock> mStarted;
			
			/** Interrupt handler if any */
			std::function<State (Reason)> mInterruptor;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

