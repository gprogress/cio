/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_RESIZE_H
#define CIO_RESIZE_H

#include "Types.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * The Resize enumeration describes if and how resources, objects, or memory may be resized.
	 */
	enum class Resize : std::uint8_t
	{
		/** The resource cannot be resized, it can only be truncated or removed if at all */
		None = 0,
		
		/** The resource may be reduced in size, but not grown */
		Shrink = 1,
		
		/** The resource may be grown in size, but not reduced */
		Grow = 2,
		
		/** The resource may be resized in any way */
		Any = 3,
		
		/** Whether the resource may be resized is not currently known */
		Unknown = 4
	};

	/**
	 * Gets whether the resize policy allows to increase size.
	 * 
	 * @param policy The resize policy 
	 * @return Whether the policy allows to increase size
	*/
	constexpr bool growable(Resize policy) noexcept
	{
		return (static_cast<unsigned>(policy) & 2u) != 0;
	}

	/**
	 * Gets whether the resize policy allows to decrease size.
	 *
	 * @param policy The resize policy
	 * @return Whether the policy allows to decrease size
	*/
	constexpr bool shrinkable(Resize policy) noexcept
	{
		return (static_cast<unsigned>(policy) & 1u) != 0;
	}

	/**
	 * Gets the text representation of a Resize enumeration value.
	 * 
	 * @param value The value
	 * @return the text representation
	 */
	CIO_API const char *print(Resize value) noexcept;
	
	/**
	 * Parses a null-terminated C string to get the matching Resize value.
	 * If the text does not match any known value, Resize::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed value
	 */
	CIO_API Resize parseResize(const char *text) noexcept;

	/**
	 * Parses a text string to get the matching Resize value.
	 * If the text does not match any known type, Resize::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed value
	 */
	CIO_API Resize parseResize(const std::string &text) noexcept;

	/**
	 * Prints the text for Resize value to a C++ stream.
	 *
	 * @param stream The C++ stream
	 * @param value The Resize value to print
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, Resize value);

	/**
	 * Parses text from a C++ stream to obtain the matching Resize value.
	 * If the text does not match any known value, Resize::Unknown is set.
	 *
	 * @param stream The C++ stream
	 * @param value The reference to the Resize value to store the parse results into
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::istream &operator>>(std::istream &stream, Resize &value);	
}

#endif

