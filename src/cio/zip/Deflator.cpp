/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Deflator.h"

#include <cio/Class.h>
#include <cio/ModeSet.h>
#include <cio/ProtocolFactory.h>
#include <cio/Traversal.h>

#include <zlib.h>

#include <sstream>

namespace cio
{
	namespace zip
	{
		Class<Deflator> Deflator::sMetaclass("cio::zip::Deflator");

		const Metaclass &Deflator::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		Deflator::Deflator() noexcept :
			mEncoder(nullptr),
			mLevel(9)
		{
			// nothing to do
		}

		Deflator::Deflator(std::size_t bufferSize, int level) :
			mEncoder(nullptr),
			mStoreBuffer(bufferSize),
			mLevel(level)
		{
			// do nothing :-)
		}

		Deflator::Deflator(Deflator &&in) noexcept :
			cio::Output(std::move(in)),
			mSink(std::move(in.mSink)),
			mEncoder(in.mEncoder),
			mStoreBuffer(std::move(in.mStoreBuffer)),
			mLevel(in.mLevel)
		{
			in.mEncoder = nullptr;
		}

		Deflator &Deflator::operator=(Deflator &&in) noexcept
		{
			if (this != &in)
			{
				this->dispose();
				cio::Output::operator=(std::move(in));
				mSink = std::move(in.mSink);
				mEncoder = in.mEncoder;
				in.mEncoder = nullptr;
				mStoreBuffer = std::move(in.mStoreBuffer);
				mLevel = in.mLevel;
			}

			return *this;
		}

		Deflator::~Deflator() noexcept
		{
			this->dispose();
		}
		
		bool Deflator::isOpen() const noexcept
		{
			return mSink && mSink->isOpen();
		}

		const Metaclass &Deflator::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		int Deflator::getCompressionLevel() const
		{
			return mLevel;
		}

		void Deflator::setCompressionLevel(int value)
		{
			if (!mEncoder)
			{
				mLevel = value;
			}
		}

		cio::ModeSet Deflator::openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory)
		{
			this->dispose();

			cio::ModeSet actual;
			
			if (factory)
			{
				std::unique_ptr<cio::Protocol> proto = factory->getProtocol(resource, Resource::File);
				mSink = proto->openOutput(resource, modes, cio::Traversal::Recursive);
				if (mSink)
				{
					this->configureNewOutput();
					actual = mSink->modes();
				}
			}

			return actual;
		}

		void Deflator::openWithUnownedOutput(Output *output)
		{
			this->dispose();
			mSink = cio::not_shared_ptr(output);
			this->configureNewOutput();
		}

		void Deflator::openWithOutput(std::shared_ptr<Output> output)
		{
			this->dispose();
			mSink = std::move(output);
			this->configureNewOutput();
		}

		std::shared_ptr<Output> Deflator::getOutput() noexcept
		{
			return mSink;
		}

		std::shared_ptr<const Output> Deflator::getOutput() const noexcept
		{
			return mSink;
		}

		std::shared_ptr<Output> Deflator::takeOutput()
		{
			std::shared_ptr<Output> output = mSink;
			this->dispose();
			return output;
		}

		void Deflator::configureNewOutput()
		{
			z_stream *encoder = new z_stream();
			mEncoder = encoder;
			encoder->zalloc = 0;
			encoder->zfree = 0;
			encoder->opaque = 0;
			const int method = Z_DEFLATED;
			const int windowBits = 31;
			const int memLevel = 8;
			const int strategy = Z_DEFAULT_STRATEGY;
			const char *zlibVersion = ZLIB_VERSION;
			const int streamSize = sizeof(z_stream);
			// 31 forces a Gzip stream, 8 specifies default memory level
			int result = deflateInit2_(encoder, mLevel, method, windowBits, memLevel, strategy, zlibVersion, streamSize);

			if (result == Z_OK)
			{
				if (mStoreBuffer.size() == 0)
				{
					mStoreBuffer.allocate(4096); // default buffer size
				}

				mStoreBuffer.reset();
			}
			else
			{
				std::ostringstream message;

				if (encoder->msg)
				{
					message << encoder->msg << ": ";
				}

				if (result == Z_MEM_ERROR)
				{
					message << "Not enough memory";
				}
				else if (result == Z_STREAM_ERROR)
				{
					message << "Invalid parameters\n";
					message << "\tLevel = " << mLevel << '\n';
					message << "\tMethod = " << method << '\n';
					message << "\tWindow Bits = " << windowBits << '\n';
					message << "\tMemory Level = " << memLevel << '\n';
					message << "\tStrategy = " << strategy << '\n';
				}
				else
				{
					message << "Unknown error";
				}

				throw std::runtime_error(message.str());
			}
		}

		Progress<std::size_t> Deflator::requestWrite(const void *buffer, std::size_t bufferLen) noexcept
		{
			Progress<std::size_t> result;

			if (mSink)
			{
				

				while (result.count < bufferLen && !result.failed())
				{
					if (!mStoreBuffer.available())
					{
						Progress<std::size_t> bufferProgress = this->flushStoreBuffer();
						result.status = bufferProgress.status;
					}

					if (result.succeeded())
					{
						Bytef *bufferBytes = static_cast<Bytef *>(mStoreBuffer.current());
						z_stream *encoder = static_cast<z_stream *>(mEncoder);
						encoder->next_in = reinterpret_cast<Bytef *>(const_cast<void *>(buffer)) + result.count;
						encoder->avail_in = static_cast<unsigned>(bufferLen - result.count);
						encoder->next_out = bufferBytes;
						encoder->avail_out = static_cast<unsigned>(mStoreBuffer.remaining());
						int zProgress = deflate(encoder, 0);
						result.count = (bufferLen - encoder->avail_in);
						mStoreBuffer.skip(encoder->next_out - bufferBytes);

						if (zProgress != Z_OK)
						{
							result.fail(Reason::Unknown);
						}
					}
				}
			}

			return result;
		}

		Progress<std::size_t> Deflator::requestWriteBuffer(std::size_t bufferSize) noexcept
		{
			Progress<std::size_t> result;

			// If we're already compressing, we can't reduce the buffer size below current in buffer
			if (mSink)
			{
				mStoreBuffer.compact();

				if (mStoreBuffer.remaining() > bufferSize)
				{
					result.count = mStoreBuffer.trim();
					result.succeed(Reason::Overflow);
				}
				else
				{
					mStoreBuffer.reallocate(bufferSize);
					result.count = bufferSize;
					result.succeed();
				}
			}
			else
			{
				mStoreBuffer.reallocate(bufferSize);
				result.count = bufferSize;
				result.succeed();
			}

			return result;
		}

		Progress<std::size_t> Deflator::flushEncoderBuffer()
		{
			Progress<std::size_t> result;

			if (mSink)
			{
				int zProgress;
				z_stream *encoder = static_cast<z_stream *>(mEncoder);
				encoder->next_in = 0;
				encoder->avail_in = 0;

				do
				{
					if (!mStoreBuffer.available())
					{
						result += this->flushStoreBuffer();
					}

					Bytef *bytes = static_cast<Bytef *>(mStoreBuffer.current());
					encoder->next_out = bytes;
					encoder->avail_out = static_cast<unsigned>(mStoreBuffer.remaining());
					zProgress = deflate(encoder, Z_SYNC_FLUSH);
					mStoreBuffer.skip(encoder->next_out - bytes);
				}
				while (zProgress == Z_OK && !result.failed() && encoder->avail_out != 0);

				if (!result.failed() && zProgress != Z_OK)
				{
					result.fail(Reason::Unknown);
				}
			}

			return result;
		}

		Progress<std::size_t> Deflator::flushStoreBuffer()
		{
			Progress<std::size_t> result;

			if (mSink)
			{
				// Flip the buffer
				mStoreBuffer.flip();

				// Write out everything
				while (!result.failed() && mStoreBuffer.available())
				{
					result += mSink->requestDrain(mStoreBuffer);
				}

				// Resets the buffer
				mStoreBuffer.reset();
			}

			return result;
		}

		Progress<Traversal> Deflator::flush(Traversal recursive) noexcept
		{
			Progress<Traversal> result(Traversal(), recursive);

			if (mSink)
			{
				if (recursive)
				{
					Progress<std::size_t> flushed = flushEncoderBuffer();

					if (result.succeeded())
					{
						flushed += flushStoreBuffer();
					}

					if (flushed.succeeded())
					{
						result.count = Traversal::Immediate;
						result.succeed();
						result += mSink->flush(recursive - 1);
					}
				}
			}

			return result;
		}

		void Deflator::clear() noexcept
		{
			this->dispose();
			cio::Output::clear();
		}

		void Deflator::dispose()
		{
			if (mSink)
			{
				// Finish the Gzip compression
				Progress<Traversal> result;
				int zProgress;
				z_stream *encoder = static_cast<z_stream *>(mEncoder);
				encoder->next_in = 0;
				encoder->avail_in = 0;

				do
				{
					if (!mStoreBuffer.available())
					{
						result += this->flush(Traversal::None);
					}

					Bytef *bytes = static_cast<Bytef *>(mStoreBuffer.current());
					encoder->next_out = bytes;
					encoder->avail_out = static_cast<unsigned>(mStoreBuffer.remaining());
					zProgress = deflate(encoder, Z_FINISH);
					mStoreBuffer.skip(encoder->next_out - bytes);
				}
				while (zProgress == Z_OK && !result.failed());

				result += this->flush(Traversal::None);
				deflateEnd(encoder);
				delete encoder;
				mEncoder = nullptr;
				mSink.reset();
			}
		}
	}
}
