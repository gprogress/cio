/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Peripheral.h"

#include "AddressType.h"

#include <algorithm>
#include <cstring>
#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		Peripheral::Peripheral() noexcept :
			mSignalStrength(INT16_MIN),
			mMinConnectionInterval(0xFFFFu),
			mMaxConnectionInterval(0xFFFFu),
			mAddressType(AddressType::None),
			mLowEnergyFlags(0)
		{
			std::memset(mShortName, 0, 8);
		}
		
		Peripheral::Peripheral(const DeviceAddress &address) noexcept :
			mAddress(address),
			mSignalStrength(INT16_MIN),			
			mMinConnectionInterval(0xFFFFu),
			mMaxConnectionInterval(0xFFFFu),
			mAddressType(AddressType::Public),
			mLowEnergyFlags(0)
		{
			std::memset(mShortName, 0, 8);
		}
				
		Peripheral::Peripheral(const DeviceAddress &address, AddressType type) noexcept :
			mAddress(address),
			mSignalStrength(INT16_MIN),
			mMinConnectionInterval(0xFFFFu),
			mMaxConnectionInterval(0xFFFFu),
			mAddressType(type),
			mLowEnergyFlags(0)
		{
			std::memset(mShortName, 0, 8);
		}
				
		Peripheral::Peripheral(const Peripheral &in) = default;
		
		Peripheral &Peripheral::operator=(const Peripheral &in) = default;
		
		Peripheral::~Peripheral() noexcept = default;
		
		void Peripheral::clear() noexcept
		{
			mAddress.clear();
			mSignalStrength = INT16_MIN;
			mMinConnectionInterval = 0xFFFFu;
			mMaxConnectionInterval = 0xFFFFu;
			mAddressType = AddressType::None;
			mLowEnergyFlags = 0;
			std::memset(mShortName, 0, 8);
		}
		
		const DeviceAddress &Peripheral::getAddress() const noexcept
		{
			return mAddress;
		}
		
		void Peripheral::setAddress(const DeviceAddress &address) noexcept
		{
			mAddress = address;
		}
		
		AddressType Peripheral::getAddressType() const noexcept
		{
			return mAddressType;
		}
		
		void Peripheral::setAddressType(AddressType type) noexcept
		{
			mAddressType = type;
			
			if (type == AddressType::Classic)
			{
				// Disable "BR/EDR not supported" flag since Classic address mode makes it mandatory
				mLowEnergyFlags &= ~0x04;
			}
		}
		
		std::int16_t Peripheral::getSignalStrength() const noexcept
		{
			return mSignalStrength;
		}
		
		void Peripheral::setSignalStrength(std::int16_t strength) noexcept
		{
			mSignalStrength = strength;
		}
		
		void Peripheral::clearSignalStrength() noexcept
		{
			mSignalStrength = INT16_MIN;
		}
		
		bool Peripheral::hasName() const noexcept
		{
			return mShortName[0] != 0;
		}
		
		const char *Peripheral::getShortName() const noexcept
		{
			return mShortName;
		}
		
		void Peripheral::setShortName(const char *text) noexcept
		{
			std::size_t toCopy = 0;
		
			if (text)
			{
				std::size_t length = std::strlen(text);
				toCopy = std::min(length, std::size_t(7u));
			}
			
			if (toCopy > 0)
			{
				std::memcpy(mShortName, text, toCopy);
			}
			
			if (toCopy < 8)
			{
				std::memset(mShortName + toCopy, 0, 8 - toCopy);
			}
		}
		
		void Peripheral::setShortName(const char *text, std::size_t length) noexcept
		{
			std::size_t toCopy = 0;
		
			if (text)
			{
				toCopy = std::min(length, std::size_t(7u));
			}
			
			if (toCopy > 0)
			{
				std::memcpy(mShortName, text, toCopy);
			}
			
			if (toCopy < 8)
			{
				std::memset(mShortName + toCopy, 0, 8 - toCopy);
			}
		}
		
		std::uint16_t Peripheral::getMinConnectionInterval() const noexcept
		{
			return mMinConnectionInterval;
		}
		
		void Peripheral::setMinConnectionInterval(std::uint16_t value) noexcept
		{
			mMinConnectionInterval = value;
		}
		
		std::uint16_t Peripheral::getMaxConnectionInterval() const noexcept
		{
			return mMaxConnectionInterval;
		}	
		
		void Peripheral::setMaxConnectionInterval(std::uint16_t value) noexcept
		{
			mMaxConnectionInterval = value;
		}
		
		std::uint8_t Peripheral::getLowEnergyFlags() const noexcept
		{
			return mLowEnergyFlags;
		}
		
		void Peripheral::setLowEnergyFlags(std::uint8_t value) noexcept
		{
			mLowEnergyFlags = value;
		}
		
		bool Peripheral::getLimitedDiscoverableMode() const noexcept
		{
			return (mLowEnergyFlags & 0x01) != 0;
		}
	
		void Peripheral::setLimitedDiscoverableMode(bool value) noexcept
		{
			mLowEnergyFlags = (mLowEnergyFlags & ~0x01) | (static_cast<std::uint8_t>(value));
		}

		bool Peripheral::getGeneralDiscoverableMode() const noexcept
		{
			return (mLowEnergyFlags & 0x02) != 0;
		}
		
		void Peripheral::setGeneralDiscoverableMode(bool value) noexcept
		{
			mLowEnergyFlags = (mLowEnergyFlags & ~0x02) | (static_cast<std::uint8_t>(value) << 1);
		}
		
		bool Peripheral::getClassicSupported() const noexcept
		{
			return mAddressType == AddressType::Classic || (mLowEnergyFlags & 0x04) == 0;
		}

		void Peripheral::setClassicSupported(bool value) noexcept
		{
			if (mAddressType != AddressType::Classic)
			{
				mLowEnergyFlags = (mLowEnergyFlags & ~0x04) | (static_cast<std::uint8_t>(!value) << 2);
			}
		}

		bool Peripheral::getSimultaneousCapable() const noexcept
		{
			return (mLowEnergyFlags & 0x08) != 0;
		}

		void Peripheral::setSimultaneousCapable(bool value) noexcept
		{
			mLowEnergyFlags = (mLowEnergyFlags & ~0x08) | (static_cast<std::uint8_t>(value) << 3);
		}
		
		bool Peripheral::isLowEnergy() const noexcept
		{
			return mAddressType == AddressType::Public || mAddressType == AddressType::Random;
		}
		
		std::size_t Peripheral::print(char *buffer, std::size_t length) const noexcept
		{
			return mAddress.print(buffer, length);
		}
		
		Peripheral::operator bool() const noexcept
		{
			return static_cast<bool>(mAddress);
		}
		
		bool operator==(const Peripheral &left, const Peripheral &right) noexcept
		{
			// all of our members are C++ primitives so we can just use memcmp
			return std::memcmp(&left, &right, sizeof(Peripheral)) == 0;
		}
		
		bool operator!=(const Peripheral &left, const Peripheral &right) noexcept
		{
			// all of our members are C++ primitives so we can just use memcmp
			return std::memcmp(&left, &right, sizeof(Peripheral)) != 0;
		}
		
		std::ostream &operator<<(std::ostream &stream, const Peripheral &p)
		{
			char buffer[64];
			std::size_t actual = p.print(buffer, 64);
			stream.write(buffer, actual);
			return stream;
		}
	}
}
