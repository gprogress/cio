/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "RelativeInput.h"

#include "Class.h"
#include "Progress.h"
#include "Seek.h"

namespace cio
{
	Class<RelativeInput> RelativeInput::sMetaclass("cio::RelativeInput");

	const Metaclass &RelativeInput::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &RelativeInput::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	RelativeInput::RelativeInput() noexcept :
		mStart(0),
		mLength(0),
		mCurrent(0)
	{
		// nothing to do
	}
	
	RelativeInput::RelativeInput(std::shared_ptr<Input> input, Length start, Length length) noexcept :
		mInput(std::move(input)),
		mStart(start),
		mLength(length),
		mCurrent(0)
	{
		// nothing to do
	}
	
	RelativeInput::RelativeInput(const RelativeInput &in) = default;

	RelativeInput::RelativeInput(RelativeInput &&in) noexcept = default;
	
	RelativeInput &RelativeInput::operator=(const RelativeInput &in) = default;
	
	RelativeInput &RelativeInput::operator=(RelativeInput &&in) noexcept = default;

	RelativeInput::~RelativeInput() noexcept = default;

	void RelativeInput::clear() noexcept
	{
		mInput.reset();
		mStart = 0;
		mLength = 0;
		mCurrent = 0;
	}

	bool RelativeInput::isOpen() const noexcept
	{
		return mInput && mInput->isOpen();
	}

	Length RelativeInput::size() const noexcept
	{
		return mLength;
	}

	Length RelativeInput::readable() const noexcept
	{
		return mLength - mCurrent;
	}

	Progress<std::size_t> RelativeInput::requestRead(void *data, std::size_t bytes) noexcept
	{
		Progress<std::size_t> result(Action::Read, 0, bytes);
		if (mInput)
		{
			if (bytes > 0)
			{
				if (mCurrent >= mLength)
				{
					result.fail(Reason::Underflow);
				}
				else
				{
					Length offset = mStart + mCurrent;
					std::size_t requested = bytes;
					if (mLength - mCurrent < bytes)
					{
						requested = (mLength - mCurrent).size();
					}
					
					result += mInput->requestReadFromPosition(data, requested, offset, Seek::Begin);
					mCurrent += result.count;
				}
			}
		}
		else
		{
			result.fail(Reason::Unopened);
		}
	
		return result;
	}

	Progress<std::size_t> RelativeInput::requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Read, 0, bytes);
		if (mInput)
		{
			if (bytes > 0)
			{
				Length absolute;
		
				switch (mode)
				{
					case Seek::Current:
						absolute = mCurrent + position;
						break;
						
					case Seek::End:
						absolute = mLength - position;
						break;
						
					default:
						absolute = position;
						break;
				}
		
				result.total = bytes;
				if (absolute < 0)
				{
					result.fail(Reason::Invalid);
				}
				else if (absolute >= mLength)
				{
					result.fail(Reason::Underflow);
				}
				else
				{
					Length offset = mStart + absolute;
					std::size_t requested = bytes;
					if (mLength - absolute < bytes)
					{
						requested = (mLength - absolute).size();
					}
					
					result = mInput->requestReadFromPosition(data, requested, offset, Seek::Begin);
					
					mCurrent += result.count;
				}
			}
		}
		else
		{
			result.fail(Reason::Unopened);
		}
	
		return result;
	}

	Length RelativeInput::getReadPosition() const noexcept
	{
		return mCurrent;
	}

	Progress<Length> RelativeInput::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result;
		
		Length absolute;
		
		switch (mode)
		{
			case Seek::Current:
				absolute = mCurrent + position;
				break;
				
			case Seek::End:
				absolute = mLength - position;
				break;
				
			default:
				absolute = position;
				break;
		}
		
		if (absolute < 0)
		{
			mCurrent = 0;
			result.fail(Reason::Underflow);
			result.count = 0;
		}
		else if (absolute > mLength)
		{
			mCurrent = mLength;
			result.fail(Reason::Overflow);;
			result.count = mLength;
		}
		else
		{
			mCurrent = absolute;
			result.succeed();
			result.count = mCurrent;
		}
		
		return result;
	}

	Progress<Length> RelativeInput::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Read, 0, bytes);
		
		if (mCurrent + bytes <= mLength)
		{
			result.succeed();
			result.count = bytes;
			mCurrent += bytes;
		}
		else
		{
			result.fail(Reason::Underflow);
			result.count = mLength - mCurrent;
			mCurrent = mLength;				
		}
		
		return result;
	}
	
	void RelativeInput::open(std::shared_ptr<Input> input, Length start, Length length)
	{
		mInput = std::move(input);
		mStart = start;
		mLength = length;
		mCurrent = 0;
	}
	
	std::shared_ptr<Input> RelativeInput::getInput() noexcept
	{
		return mInput;
	}
			
	std::shared_ptr<const Input> RelativeInput::getInput() const noexcept
	{
		return mInput;
	}
			
	void RelativeInput::setInput(std::shared_ptr<Input> input) noexcept
	{
		mInput = std::move(input);
	}
			
	Length RelativeInput::start() const noexcept
	{
		return mStart;
	}

	void RelativeInput::start(Length start) noexcept
	{
		mStart = start;
	}

	Length RelativeInput::position() const noexcept
	{
		return mCurrent;
	}
	
	void RelativeInput::position(Length position) noexcept
	{
		mCurrent = position;
	}

	Length RelativeInput::limit() const noexcept
	{
		return mLength;
	}
	
	void RelativeInput::limit(Length limit) noexcept
	{
		mLength = limit;
	}
}
