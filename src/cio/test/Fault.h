/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_FAULT_H
#define CIO_TEST_FAULT_H

#include "Types.h"

#include <cio/Source.h>

#include <stdexcept>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#endif

namespace cio
{
	namespace test
	{
		/**
		 * The Fault class provides the exception type for reporting that a fault failed.
		 */
		class CIO_TEST_API Fault : public std::exception
		{
			public:
				/**
				 * Construct the empty fault.
				 */
				Fault() noexcept;

				/**
				 * Construct a fault with the given message.
				 *
				 * @param message The message
				 */
				explicit Fault(const char *message);

				/**
				 * Construct a fault with the given message.
				 *
				 * @param message The message
				 */
				explicit Fault(std::string message);

				/**
				 * Construct a fault with the given file and line.
				 *
				 * @param message The fault message
				 * @param source The source originating the fault
				 */
				Fault(const char *message, const Source &source, Severity severity);

				/**
				 * Construct a fault with the given file and line.
				 *
				 * @param message The fault message
				 * @param file The originating source file of the fault
				 * @param line The originating source line of the fault
				 */
				Fault(std::string message, const Source &source, Severity severity);

				/**
				 * Construct a copy of a fault.
				 *
				 * @param in The fault to copy
				 */
				Fault(const Fault &in);

				/**
				 * Construct a fault by transferring the input of a given fault.
				 *
				 * @param in The fault to move
				 */
				Fault(Fault &&in) noexcept;

				/**
				 * Assign a copy of a fault.
				 *
				 * @param in The fault to copy
				 */
				Fault &operator=(const Fault &in);

				/**
				 * Transfer the contents of a fault into this one.
				 *
				 * @param in The fault to move
				 */
				Fault &operator=(Fault &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Fault() noexcept override;

				/**
				 * Clears the fault and restores it to its default state.
				 */
				virtual void clear() noexcept;

				/**
				 * Gets the message to report as the exception message.
				 *
				 * @return the exception message
				 */
				virtual const char *what() const noexcept override;

				/**
				 * Gets the message to report as the exception message.
				 *
				 * @return the exception message
				 */
				const std::string &getMessage() const;

				/**
				 * Sets the message to report as the exception message.
				 *
				 * @param message the exception message
				 */
				void setMessage(const char *message);

				/**
				 * Sets the message to report as the exception message.
				 *
				 * @param message the exception message
				 */
				void setMessage(std::string message);

				/**
				 * Gets the source location that was the origination of the fault (typically where the exception was thrown).
				 *
				 * @return the origin of the fault
				 */
				const Source &getSource() const;

				/**
				 * Sets the source location hat was the origination of the fault (typically where the exception was thrown).
				 *
				 * @param source the origin of the fault
				 */
				void setSource(const Source &source);

				/**
				 * Gets the reported severity of the fault.
				 *
				 * @return the severity
				 */
				Severity getSeverity() const;

				/**
				 * Sets the reported severity of the fault.
				 *
				 * @param severity the severity
				 */
				void setSeverity(Severity severity);

				/**
				 * Determines if htis fault represents a substantial failure.
				 * This will return true if the severity is Error or higher and false if Warning or lower.
				 *
				 * @return true if this fault represents a substantial failure
				 */
				bool failed() const;

				/**
				 * Determines if htis fault does not represent a substantial failure and thus the originating test passed.
				 * This will return true if the severity is Warning or lower and false if Error or higher.
				 *
				 * @return true if this fault does not represent a substantial failure
				 */
				bool passed() const;

				/**
				 * Determines if this fault is considered true in a Boolean context.
				 * Since a fault represents a failure, it is true if a significant failure actually occurred.
				 * This will return true if the severity is Error or higher and false if Warning or lower.
				 *
				 * @return true if this fault represents a substantial failure
				 */
				explicit operator bool() const;

			private:
				/** The message to report as the exception */
				std::string mMessage;

				/** The source location that was the origin of the fault */
				Source mSource;

				/** The reported severity of the fault */
				Severity mSeverity;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
