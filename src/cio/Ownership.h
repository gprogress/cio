/*==============================================================================
 * Copyright 2022 Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_OWNERSHIP_H
#define CIO_OWNERSHIP_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The Ownership enumeration describes the resource ownership (typically memory) of a data element
	 * relative to its container.
	 */
	enum class Ownership : std::uint8_t
	{
		/** No ownership mode has been specified */
		None = 0,

		/** The data element is an embedded part of its container */
		Embed,

		/** The data element is a reference or pointer to an externally managed resource that will outlive this container */
		Reference,

		/** The data element is a reference or pointer to a resource uniquely allocated by this container */
		Allocate,

		/** The data element is a reference or pointer to a resource shared between this and another container */
		Share
	};

	/**
	 * Gets the text label representing an ownership value.
	 *
	 * @param value The ownership value
	 * @return the text label
	 */
	CIO_API const char *print(Ownership value) noexcept;

	/**
	 * Prints the text label representing an ownership value to a C++ stream.
	 *
	 * @param s The stream
	 * @param value The ownership value
	 * @return the stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Ownership value);
}

#endif
