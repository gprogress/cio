/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include <cio/zip/Program.h>

#include <iostream>

int main(int argc, char **argv)
{
	int status = 0;

	cio::zip::Program program(argc, argv);
	try
	{
		program.execute();
		status = 0;
	}
	catch (const std::exception &e)
	{
		std::cerr << "Failed to execute: " << e.what();
		status = -1;
	}
	catch (...)
	{
		status = -1;
	}

	return status;
}