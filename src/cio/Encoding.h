/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ENCODING_H
#define CIO_ENCODING_H

#include "Types.h"

#include "Domain.h"
#include "Normalized.h"
#include "Signedness.h"

#include <iosfwd>
#include <limits>
#include <string>
#include <type_traits>

namespace cio
{
	/**
	 * The Encoding describes the binary layout of machine-binary representations of integers, fractional, and complex numbers.
	 * It is optimized into a 64-bit packed structure.
	 * 
	 * Real and Imaginary number consist of one component with the specified encoding.
	 * Complex numbers consist of two contiguous components with the specified encoding.
	 */
	class CIO_API Encoding
	{
		public:
			/** Number of bits used in mantissa, negative indicates signed */
			std::uint16_t mantissa = 0;
			
			/** Exponent bias */
			std::int16_t bias = 0;

			/** Number of bits in exponent */
			std::uint8_t exponent = 0;

			/** Sign mode for encoding */
			Signedness signedness = Signedness::None;
			
			/** Whether exponent values > 0 imply that the mantissa has a leading 1 bit set */
			Normalized normalized = Normalized::No;

			/** The numeric domain (real, imaginary, or complex) of the number. */
			Domain domain = Domain::Real;
	
			/**
			 * Creates a encoding for the given data type.
			 * This is specialized for all built-in types to return the appropriate information.
			 *
			 * For the general unspecialized template, it just assumes that sizeof(T) mantissa bits exist as an unsigned value.
			 *
			 * @tparam <T> The type of interest
			 * @return the encoding for this built in type
			 */
			template <typename T>
			inline static Encoding create() noexcept;

			/**
			 * Creates a encoding for a fixed point normalized integer representation.
			 * This uses the given number of mantissa bits to encode the range from [0.0, 1.0] inclusive.
			 * The bias is set to represent the negative fixed exponent and the item is marked as normalized, but no exponent bits are allocated.
			 * 
			 * @param mantissa the number of mantissa bits
			 * @return the encoding that was requested
			 */
			static Encoding createNormalized(std::size_t mantissa) noexcept;

			/**
			 * Creates a encoding for a signed fixed point normalized integer representation.
			 * This uses the given number of mantissa bits to encode the range from [-1.0, 1.0] inclusive.
			 * The bias is set to represent the negative fixed exponent and the item is marked as normalized, but no exponent bits are allocated.
			 *
			 * @param mantissa the number of mantissa bits
			 * @return the encoding that was requested
			 */
			static Encoding createSignedNormalized(std::size_t mantissa) noexcept;

			/**
			 * Creates a encoding for a fixed point normalized integer representation that matches the given integer type.
			 * If the integer type is unsigned, this is equivalent to createNormalized(sizeof(T) * 8).
			 * If the integer type is signed, this is equivalent to createSignedNormalized(sizeof(T) * ).
			 *
			 * @tparam<T> The integer type to use for a normalized reprsentation
			 * @return the encoding that was requested
			 */
			template <typename T>
			static Encoding createNormalized() noexcept;

			/**
			 * Computes ULP distance of float value from same-signed float zero value.
			 * Assumes IEEE 754 binary32 integer representation of given float values.
			 *
			 * @param value The value to get ULP distance from zero for
			 * @return ULP distance of float value from same-signed float zero value
			 */
			static std::int32_t calculateULPDistanceFromZero(float value) noexcept;

			/**
			 * Computes ULP distance of double value from same-signed double zero value.
			 * Assumes IEEE 754 binary64 integer representation of given double values.
			 *
			 * @param value The value to get ULP distance from zero for
			 * @return ULP distance of double value from same-signed double zero value
			 */
			static std::int64_t calculateULPDistanceFromZero(double value) noexcept;

			/**
			 * Computes a - b based on unit in last place values.
			 * Assumes IEEE 754 binary32 integer representation of given float values.
			 * Where values have different signs, the sum of each of their distances from matching-signed zero is used.
			 *
			 * @param a The first value to compare
			 * @param b The second value to compare
			 * @return a - b based on unit in last place values.
			 */
			static std::int32_t calculateDifferenceULP(float a, float b) noexcept;

			/**
			 * Computes a - b based on unit in last place values.
			 * Assumes IEEE 754 binary64 integer representation of given double values.
			 * Where values have different signs, the sum of each of their distances from matching-signed zero is used.
			 *
			 * @param a The first value to compare
			 * @param b The second value to compare
			 * @return a - b based on unit in last place values.
			 */
			static std::int64_t calculateDifferenceULP(double a, double b) noexcept;

			/**
			 * Compares float values using Unit in Last Place.
			 * Where values have different signs, the sum of each of their distances from matching-signed zero is used.
			 * If either value is NAN value, functions returns false.
			 * Returns false for non-equal comparison to infinity value(s).
			 *
			 * @param a The first value to compare
			 * @param b The second value to compare
			 * @param maxULPSDiff The max difference to return true, inclusive
			 * @return True if values are within maxULPSDiff, inclusive; false otherwise
			 */
			static inline bool equalWithinULP(float a, float b, std::uint16_t maxULPSDiff);

			/**
			 * Compares double values using Unit in Last Place.
			 * Where values have different signs, the sum of each of their distances from matching-signed zero is used.
			 * If either value is NAN value, functions returns false.
			 * Returns false for non-equal comparison to infinity value(s).
			 *
			 * @param a The first value to compare
			 * @param b The second value to compare
			 * @param maxULPSDiff The max difference to return true, inclusive
			 * @return True if values are within maxULPSDiff, inclusive; false otherwise
			 */
			static inline bool equalWithinULP(double a, double b, std::uint32_t maxULPSDiff);

			/**
			 * Creates the default encoding for void which is no bits set for anything.
			 */
			inline Encoding() noexcept = default;
			
			/**
			 * Creates the encoding appropriate for the default component type of the given primitive.
			 * For this method, special values None and Any result in default construction.
			 *
			 * @param prim The primitive type
			 */
			Encoding(Type prim) noexcept;
			
			/**
			 * Creates the encoding appropriate for the given component type.
			 * For this method, special values None, Default, and Quantized result in default construction.
			 *
			 * @param component The component type
			 * @param domain The domain type
			 */
			Encoding(Primitive component, Domain domain = Domain::Real) noexcept;

			/**
			 * Creates the encoding appropriate for the given primitive's encoding.
			 * For this method, special value None results in default construction.
			 * Default and Quantized select the default component for the primitive.
			 *
			 * @param prim The primitive type
			 * @param component The component type
			 */
			Encoding(Type prim, Primitive component) noexcept;

			/**
			 * Construct a copy of a encoding.
			 *
			 * @param in The encoding to copy
			 */
			inline Encoding(const Encoding &in) noexcept = default;

			/**
			 * Construct the default unsigned integer layout with the given number of mantissa bits.
			 *
			 * @param mantissa The number of mantissa bits
			 */
			explicit Encoding(std::size_t mantissa) noexcept;
			
			/**
			 * Construct the default integer layout with the given number of mantissa bits and signedness.
			 *
			 * @param mantissa The number of mantissa bits
			 * @param signedness The signedness
			 */
			Encoding(std::size_t mantissa, Signedness signedness) noexcept;
			
			/**
			 * Construct an unsigned encoding by specifying the mantissa, exponent, and bias.
			 *
			 * @param mantissa The mantissa bit count
			 * @param exponent The exponent bit count
			 * @param bias The exponent bias
			 */
			Encoding(std::size_t mantissa, unsigned exponent, int bias) noexcept;

			/**
			 * Construct a encoding by specifying the mantissa, exponent, bias, and the normalization type.
			 *
			 * @param mantissa The mantissa bit count
			 * @param exponent The exponent bit count
			 * @param signedness The signedness
			 * @param normalized The normalization type
			 */
			Encoding(std::size_t mantissa, unsigned exponent, int bias, Signedness signedness, Normalized normalized = Normalized::No, Domain domain = Domain::Real) noexcept;

			/**
			 * Construct a encoding by specifying the mantissa, exponent, bias, and the flag fields.
			 *
			 * @param mantissa The mantissa bit count
			 * @param exponent The exponent bit count
			 * @param bias The exponent bias
			 * @param signedness The signedness
			 * @param normalized Whether the mantissa has an implicit leading 1 bit if exponent value is greater than 0
			 * @param nan Whether the standard bit pattern for "Not a Number" is in use
			 * @param infinity Whether the standard bit pattern for "Infinity" is in use
			 */
			Encoding(std::size_t mantissa, unsigned exponent, int bias, Signedness signedness, bool normalized, bool nan, bool infinity) noexcept; 
			
			/**
			 * Destructor.
			 */
			inline ~Encoding() noexcept = default;

			/**
			 * Checks whether the number is signed (may have negative values).
			 *
			 * @return whether the number is signed
			 */
			bool isSigned() const noexcept;

			/**
			 * Sets whether the number is signed (may have negative values).
			 *
			 * @param value whether the number is signed
			 */
			void setSigned(bool value) noexcept;

			/**
			 * Checks whether the number has an explicit leading bit for representing the sign.
			 * If the number is signed and this is false, the standard two's complement signed representation is used in the mantissa.
			 * If the number is signed and this is true, the mantissa always represents the absolute value and the sign bit indicates negativity.
			 * If the number is unsigned and this is true, the sign bit is present in the binary layout but ignored.
			 *
			 * @return whether the first bit of the number indicates the sign
			 */
			bool hasSignBit() const noexcept;

			/**
			 * Sets whether the number has an explicit leading bit for representing the sign.
			 * If the number is signed and this is false, the standard two's complement signed representation is used in the mantissa.
			 * If the number is signed and this is true, the mantissa always represents the absolute value and the sign bit indicates negativity.
			 * If the number is unsigned and this is true, the sign bit is present in the binary layout but ignored.
			 *
			 * @param value Whether the first bit of the number indicates the sign
			 */
			void setSignBit(bool value) noexcept;

			/**
			 * Checks whether the number is a two's complement signed integer.
			 * This is true if the number is signed but does not have a sign bit.
			 * All built-in C++ signed integers use this format.
			 *
			 * @return whether the number is signed two's complement
			 */
			bool isTwosComplement() const noexcept;

			/**
			 * Sets whether the number is a two's complement signed integer.
			 * If true, then the number is set to sign and the sign bit is disabled.
			 * If false, if the number is signed then the sign bit is enabled. If the number is unsigned, then no changes occurs.
			 *
			 * @param value whether to set the nubmer as signed two's complement
			 */
			void setTwosComplement(bool value) noexcept;

			/**
			 * Gets whether this type is an unsigned integer type.
			 * This is true if the mantissa has nonzero bits, the exponent has zero bits, the bias is 0, Signedness is None, and Normalized is No.
			 * 
			 * @return whether this is an unsigned integer definition
			 */
			bool isUnsignedInteger() const noexcept;

			/**
			 * Checks whether this type is a C++ standard integer type, signed or unsigned.
			 * This verifies that only the mantissa and possibly the signed status is set and the mantissa is either 8, 16, 32, or 64 bits.
			 * If true, the number of bytes (1, 2, 4 or 8) is returned. Otherwise 0 is returned.
			 *
			 * @return if this is a standard integer type, the number of bytes, otherwise 0
			 */
			std::size_t isStandardInteger() const noexcept;

			/**
			 * Checks whether this type is a C++ standard unsigned integer type.
			 * This verifies that only the mantissa is set and the mantissa is either 8, 16, 32, or 64 bits.
			 * If true, the number of bytes (1, 2, 4 or 8) is returned. Otherwise 0 is returned.
			 *
			 * @return if this is a standard unsigned integer type, the number of bytes, otherwise 0
			 */
			std::size_t isStandardUnsigned() const noexcept;

			/**
			 * Checks whether this type is a C++ standard signed integer type.
			 * This verifies that the signed status is enabled (but the sign bit is off) and otherwise only the mantissa is set and the mantissa is either 8, 16, 32, or 64 bits.
			 * If true, the number of bytes (1, 2, 4 or 8) is returned. Otherwise 0 is returned.
			 *
			 * @return if this is a standard signed integer type, the number of bytes, otherwise 0
			 */
			std::size_t isStandardSigned() const noexcept;

			/**
			 * Checks whether the number is normalized, meaning that there is an implicit mantissa bit that is 1 before all explicit mantissa bits
			 * for all values of the exponent greater than the bias.
			 * All built-in C++ floating point types (per IEEE 754) are normalized.
			 * This has no effect if the number of exponent bits are 0.
			 *
			 * @return whether the number mantissa is normalized per IEEE 754 rules
			 */
			bool isNormalized() const noexcept;

			/**
			 * Checks whether the number is normalized, meaning that there is an implicit mantissa bit that is 1 before all explicit mantissa bits
			 * for all values of the exponent greater than the bias.
			 * All built-in C++ floating point types (per IEEE 754) are normalized.
			 * This has no effect if the number of exponent bits are 0.
			 *
			 * @return whether the number mantissa is normalized per IEEE 754 rules
			 */
			void setNormalized(bool value) noexcept;

			/**
			 * Checks whether the number has a "Not a Number" value to represent missing data.
			 * All built-in C++ floating point types (per IEEE 754) have NaN.
			 * If true, the NaN value is defined when all exponent bits and all mantissa bits are set.
			 * If no exponent bits are defined, then NaN value is either all mantissa bits set (if unsigned or using a sign bit) or the highest mantissa bit set (for signed two's complement).
			 * The latter case can be used to implement an integer type with a consistent NaN value.
			 *
			 * @param value Whether the number has a "Not a Number" value
			 */
			bool hasNoData() const noexcept;

			/**
			 * Sets whether the number has a "Not a Number" value to represent missing data.
			 * All built-in C++ floating point types (per IEEE 754) have NaN.
			 * If true, the NaN value is defined when all exponent bits are set and all mantissa bits are non-zero per IEEE 754.
			 * If no exponent bits are defined, then NaN value depends on the sign representation.
			 * For unsigned or using a sign bit, the NaN value has all mantissa bits set. In the case of a sign bit, negative NaN is also a valid value.
			 * For signed two's complement, the NaN value only has the highest mantissa bit set, to use the most negative possible bit pattern (and only positive NaN is available in this case).
			 * The latter cases can be used to implement an integer type with a consistent NaN value. As is usual with NaN, sorting order is not well defined.
			 *
			 * @param value Whether the number has a "Not a Number" value
			 */
			void setNoData(bool enabled) noexcept;

			/**
			 * Checks whether the number has an "Infinity" value to represent unbounded data limits.
			 * All built-in C++ floating point types (per IEEE 754) have Infinity.
			 * If true, the Infinity value is defined when all exponent bits are set and all mantissa bits are zero per IEEE 754.
			 * If no exponent bits are defined, then the positive infinity value depends on the sign representation.
			 * For unsigned or using a sign bit, the positive infinity value has all mantissa bits set, except the lowest bit if NaN is also available.
			 * For signed two's complement, the positive infinity value is the highest possible positive mantissa (all bits set except the highest).
			 * For all signed representations, negative infinity is the negation of positive infinity.
			 * The latter cases can be used to implement an integer type with a consistent infinity value that sorts as expected relative to finite numbers.
			 *
			 * @return Whether the number has an "Infinity" value
			 */
			bool hasInfinity() const noexcept;

			/**
			 * Sets whether the number has an "Infinity" value to represent unbounded data limits.
			 * All built-in C++ floating point types (per IEEE 754) have Infinity.
			 * If true, the Infinity value is defined when all exponent bits are set and all mantissa bits are zero per IEEE 754.
			 * If no exponent bits are defined, then the positive infinity value depends on the sign representation.
			 * For unsigned or using a sign bit, the positive infinity value has all mantissa bits set, except the lowest bit if NaN is also available.
			 * For signed two's complement, the positive infinity value is the highest possible positive mantissa (all bits set except the highest).
			 * For all signed representations, negative infinity is the negation of positive infinity.
			 * The latter cases can be used to implement an integer type with a consistent infinity value that sorts as expected relative to finite numbers.
			 *
			 * @param value Whether the number should have an "Infinity" value
			 */
			void setInfinity(bool enabled) noexcept;

			/**
			 * Gets the number of explicitly represented mantissa bits.
			 *
			 * @return the number of mantissa bits
			 */
			std::size_t getMantissaBits() const noexcept;

			/**
			 * Sets the number of explicitly represented mantissa bits.
			 *
			 * @param value the number of mantissa bits
			 */
			void setMantissaBits(std::size_t value) noexcept;

			/**
			 * Gets the number of explicitly represented exponent bits.
			 * If non-zero, then this data type is a floating point value of some kind, although it may or may not conform to IEEE 754.
			 *
			 * @return the number of exponent bits
			 */
			unsigned getExponentBits() const noexcept;

			/**
			 * Sets the number of explicitly represented exponent bits.
			 * If non-zero, then this data type is a floating point value of some kind, although it may or may not conform to IEEE 754.
			 *
			 * @param value the number of exponent bits
			 */
			void setExponentBits(unsigned value) noexcept;

			/**
			 * Gets the bias (offset) of the exponent value.
			 * If any exponent bits are defined, then the exponent bit pattern 0 is equal to this value.
			 * It can still be used even if no exponent bits are defined to set up a binary fixed-point type.
			 *
			 * @return the exponent bias to use
			 */
			int getExponentBias() const noexcept;

			/**
			 * Sets the bias (offset) of the exponent value.
			 * If any exponent bits are defined, then the exponent bit pattern 0 is equal to this value.
			 * It can still be used even if no exponent bits are defined to set up a binary fixed-point type.
			 *
			 * @param  the exponent bias to use
			 */
			void setExponentBias(int bias) noexcept;

			/**
			 * Sets this number to represent the IEEE 754 floating point layout for the given number of mantissa bits, exponent bits, and exponent bias.
			 * In addition to setting these three values, this also sets the encoding as signed using a sign bit, normalized, and having NaN and infinity values.
			 *
			 * @param mantissa The number of mantissa bits
			 * @param exponent The number of exponent bits
			 * @param bias The exponent bias
			 */
			void setFloatingPoint(std::size_t mantissa, unsigned exponent, int bias) noexcept;

			/**
			 * Checks whether this encodings matches a floating point layout as generalized by this class.
			 * This requires the number to be signed using a sign bit, normalized, having a NaN and infinity value, and have a non-zero number of exponent bits.
			 * To check against specific standard layouts like single-precision float or double-precision double, instead use an equal comparision against Encoding::create<T>
			 * for the appropriate type.
			 *
			 * @return whether this encoding is a generalized floating point number
			 */
			bool isFloatingPoint() const noexcept;

			/**
			 * Checks whether this encoding matches an IEEE 754 standard floating point layout.
			 * If so, the number of bytes (not bits) is returned i.e. 4 is float and 8 is double.
			 * If not, 0 is returned.
			 *
			 * @return if this is a standard IEEE 754 float layout, the number of bytes, otherwise 0
			 */
			std::size_t isStandardFloat() const noexcept;

			/**
			 * Checks if the value can physically fit in the bits defined
			 *
			 * @param value The value to check if can physically fit in the bits defined
			 *
			 * @return whether the value can physically fit in the bits defined 
			*/
			template <typename T>
			inline bool isBelowRepresentation(const T value) const noexcept;

			/**
			 * Checks if the value can physically fit in the bits defined
			 *
			 * @param value The value to check if can physically fit in the bits defined
			 *
			 * @return whether the value can physically fit in the bits defined
			 */
			template <typename T>
			inline bool isAboveRepresentation(const T value) const noexcept;

			/**
			 * Sets the encoding from a packed 64-bit binary value.
			 *
			 * @param packed The binary packed value
			 */
			void unpack(std::uint64_t packed) noexcept;

			/**
			 * Gets a packed 64-bit binary value representing this encoding's current state.
			 *
			 * @return the binary packed value
			 */
			std::uint64_t pack() const noexcept;
			
			/**
			 * Creates the encoding appropriate for the default component type of the given primitive.
			 * For this method, special values None and Any result in default construction.
			 *
			 * @param prim The primitive type
			 * @param domain The domain type
			 */
			void setEncoding(Type prim, Domain domain = Domain::Real) noexcept;
			
			/**
			 * Creates the encoding appropriate for the given component type.
			 * For this method, special values None, Default, and Quantized result in default construction.
			 *
			 * @param component The component type
			 * @param domain The domain type
			 */
			void setEncoding(Primitive component, Domain domain = Domain::Real) noexcept;
			
			/**
			 * Creates the encoding appropriate for the given primitive's encoding.
			 * For this method, special value None results in default construction.
			 * Default and Quantized select the default component for the primitive.
			 *
			 * @param prim The primitive type
			 * @param component The component type
			 * @param domain The domain type
			 */
			void setEncoding(Type prim, Primitive component, Domain domain = Domain::Real) noexcept;

			/**
			 * Computes the number of required bits for this encoding.
			 * This is the sum of the explicit mantissa bits, the exponent bits, and the sign bit if present.
			 *
			 * @return the number of bits required for this number
			 */
			std::size_t computeRequiredBits() const noexcept;

			/**
			 * Computes the number of required bits for an optimally packed array of this encoding.
			 * This is simply the number of bits per element multiplied by the provided count.
			 *
			 * @param count The number of array elements
			 * @return the number of bits required for an array of length count of this number
			 */
			std::size_t computeRequiredBits(std::size_t count) const noexcept;

			/**
			 * Computes the number of required bytes for this encoding.
			 * This starts with the required number of bits divided by 8, rounded up if the bit count is not a multiple of 8.
			 *
			 * @return the number of bytes required for this number
			 */
			std::size_t computeRequiredBytes() const noexcept;

			/**
			 * Computes the number of required bytes for an optimally packed array of this encoding.
			 * This is the total number of bits for the array divided by 8, then rounded up if the final bit count is not a multiple of 8.
			 * Note that this can be different than multiplying the count by the number of bytes per element due to the differences in rounding.
			 *
			 * @param count The number of array elements
			 * @return the number of bytes required for an array of length count of this number
			 */
			std::size_t computeRequiredBytes(std::size_t count) const noexcept;

			/**
			 * Clears the contents of this encoding, effectively restoring it to the void layout.
			 */
			void clear() noexcept;

			/**
			 * Determines whether the encoding matches the given (generally C++ standard) type.
			 *
			 * @tparam <T> The data type such as float, int, long, etc.
			 * @return whether this encoding exactly represents the given data type
			 */
			template <typename T>
			inline bool is() const noexcept;
			
			/**
			 * Analyzes whether this enoding is sufficiently compatible with the given encoding
			 * that a bitwise copy would result in the equivalent of a cast conversion.
			 *
			 * This is true if both layouts have the same number of mantissa and exponent bits,
			 * the same exponent bias, are both normalized or both denormalized, and the signedness
			 * are either both or neither set to Prefix.
			 *
			 * @param encoding The other encoding of interest
			 * @return whether the encodings support bitwise copying
			 */
			bool isCopyableTo(const Encoding &encoding) const noexcept;
			
			/**
			 * Checks to see if this is a C++ primitive number - either integer or floating point.
			 * If it is, the number of bytes is returned as the first item in the pair and the style of primitive is the Signedness in the second item.
			 * For unsigned integer, it will be Signedness::None.
			 * For signed integer, it will be Signedness::Negation.
			 * For floating point, it will be Signedness::Prefix.
			 *
			 * @return a pair describing the primitive number of bytes and style of this number layout, or an empty pair if it is not a primitive
			 */
			std::pair<std::size_t, Signedness> isPrimitive() const noexcept;

			/**
			 * Checks whether this encoding defines a real number domain component.
			 * 
			 * @return whether a real component is present
			 */
			bool hasReal() const noexcept;

			/**
			 * Checks whether this encoding defines an imaginry number domain component.
			 *
			 * @return whether an imaginary component is present
			 */
			bool hasImaginary() const noexcept;

			/**
			 * Sets whether this encoding includes a real component.
			 * 
			 * @param enabled Whether a real component is enabled
			 */
			void setReal(bool enabled) noexcept;

			/**
			 * Sets whether this encoding includes an imaginary component.
			 *
			 * @param enabled Whether an imaginary component is enabled
			 */
			void setImaginary(bool enabled) noexcept;

			/**
			 * Interprets the layout in a Boolean context.
			 * It is true if either mantissa or exponent have bits.
			 *
			 * @return whether this encoding is valid
			 */
			explicit operator bool() const noexcept;
	};

	/**
	 * Check if two encodings are equal. This is true if they have exactly the same field values.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the layouts are identical
	 */
	CIO_API bool operator==(Encoding left, Encoding right) noexcept;

	/**
	 * Check if two encodings are different. This is true if any of their field values are different.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the layouts are different
	 */
	CIO_API bool operator!=(Encoding left, Encoding right) noexcept;

	/**
	 * Checks to see if one encoding sorts before the other.
	 * This is true if the packed 64-bit integer of the first is less than the packed 64-bit integer of the second.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the first layout sorts before the second
	 */
	CIO_API bool operator<(Encoding left, Encoding right) noexcept;

	/**
	 * Checks to see if one encoding sorts before or is equal to the other.
	 * This is true if the packed 64-bit integer of the first is less than or equal to the packed 64-bit integer of the second.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the first layout sorts before or equal to the second
	 */
	CIO_API bool operator<=(Encoding left, Encoding right) noexcept;

	/**
	 * Checks to see if one encoding sorts after the other.
	 * This is true if the packed 64-bit integer of the first is greater than the packed 64-bit integer of the second.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the first layout sorts after the second
	 */
	CIO_API bool operator>(Encoding left, Encoding right) noexcept;

	/**
	 * Checks to see if one encoding sorts after or is equal to the other.
	 * This is true if the packed 64-bit integer of the first is greater than or equal to the packed 64-bit integer of the second.
	 *
	 * @param left The first layout
	 * @param right The second layout
	 * @return whether the first layout sorts after or equal to the second
	 */
	CIO_API bool operator>=(Encoding left, Encoding right) noexcept;

	/**
	 * Prints the encoding to a readable string.
	 *
	 * @param layout The layout
	 * @return the printed string
	 * @throw std::bad_alloc If the string memory could not be allocated
	 */
	CIO_API std::string print(Encoding layout);
	
	/**
	 * Prints the encoding to a C++ stream.
	 *
	 * @param s The stream
	 * @param layout The layout to print
	 * @return the stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Encoding layout);
}

/* Inline implementation */

#include <cmath>
#include <cstring>

namespace cio
{
	template <typename T>
	inline Encoding Encoding::createNormalized() noexcept
	{
		Encoding layout = Encoding::create<T>();
		layout.setNormalized(true);
		layout.setExponentBias(-static_cast<int>(sizeof(T) * 8));
		return layout;
	}

	template <>
	inline Encoding Encoding::create<bool>() noexcept
	{
		return Encoding(sizeof(bool) * 8);
	}

	template <>
	inline Encoding Encoding::create<void>() noexcept
	{
		// For the purposes of encodings, void is used to mark opaque binary bytes
		// This is handled as an unsigned integer type internally
		return Encoding(8);
	}

	template <>
	inline Encoding Encoding::create<char>() noexcept
	{
		return Encoding(sizeof(char) * 8, std::is_signed<char>::value ? Signedness::Negation : Signedness::None);
	}

	template <>
	inline Encoding Encoding::create<wchar_t>() noexcept
	{
		return Encoding(sizeof(wchar_t) * 8, std::is_signed<wchar_t>::value ? Signedness::Negation : Signedness::None);
	}

	template <>
	inline Encoding Encoding::create<unsigned char>() noexcept
	{
		return Encoding(sizeof(unsigned char) * 8);
	}

	template <>
	inline Encoding Encoding::create<signed char>() noexcept
	{
		return Encoding(sizeof(signed char) * 8, Signedness::Negation);
	}

	template <>
	inline Encoding Encoding::create<unsigned short>() noexcept
	{
		return Encoding(sizeof(unsigned short) * 8);
	}

	template <>
	inline Encoding Encoding::create<signed short>() noexcept
	{
		return Encoding(sizeof(signed short) * 8, Signedness::Negation);
	}

	template <>
	inline Encoding Encoding::create<unsigned int>() noexcept
	{
		return Encoding(sizeof(unsigned int) * 8);
	}

	template <>
	inline Encoding Encoding::create<signed int>() noexcept
	{
		return Encoding(sizeof(signed int) * 8, Signedness::Negation);
	}

	template <>
	inline Encoding Encoding::create<unsigned long>() noexcept
	{
		return Encoding(sizeof(unsigned long) * 8);
	}

	template <>
	inline Encoding Encoding::create<signed long>() noexcept
	{
		return Encoding(sizeof(signed long) * 8, Signedness::Negation);
	}

	template <>
	inline Encoding Encoding::create<unsigned long long>() noexcept
	{
		return Encoding(sizeof(unsigned long long) * 8);
	}

	template <>
	inline Encoding Encoding::create<signed long long>() noexcept
	{
		return Encoding(sizeof(signed long long) * 8, Signedness::Negation);
	}

	template <>
	inline Encoding Encoding::create<float>() noexcept
	{
		return Encoding(23, 8, -127, Signedness::Prefix, Normalized::YesWithInfinityNaN); 
	}

	template <>
	inline Encoding Encoding::create<double>() noexcept
	{
		return Encoding(52, 11, -1023, Signedness::Prefix, Normalized::YesWithInfinityNaN);
	}

	template <typename T>
	inline bool Encoding::isBelowRepresentation(const T value) const noexcept
	{
		bool below = false;

		if (this->isFloatingPoint())
		{
			if (this->is<float>())
			{
				below = value < std::numeric_limits<float>::lowest();
			}
			else if (this->is<double>())
			{
				below = value < std::numeric_limits<double>::lowest();
			}
		}
		else if (this->isStandardInteger())
		{
			std::size_t numberOfBits = computeRequiredBits();
			if (numberOfBits == 8u)
			{
				if (this->isSigned())
				{
					below = value < std::numeric_limits<std::int8_t>::lowest();
				}
				else
				{
					below = value < static_cast<std::int16_t>(std::numeric_limits<std::uint8_t>::lowest());
				}
			}
			else if (numberOfBits == 16u)
			{
				if (this->isSigned())
				{
					below = value < std::numeric_limits<std::int16_t>::lowest();
				}
				else
				{
					below = value < static_cast<std::int32_t>(std::numeric_limits<std::uint16_t>::lowest());
				}
			}
			else if (numberOfBits == 32u)
			{
				if (this->isSigned())
				{
					below = value < std::numeric_limits<std::int32_t>::lowest();
				}
				else
				{
					below = value < static_cast<std::int64_t>(std::numeric_limits<std::uint32_t>::lowest());
				}
			}
			else if (numberOfBits == 64u)
			{
				if (this->isSigned())
				{
					below = value < std::numeric_limits<std::int64_t>::lowest();
				}
				else
				{
					below = value < std::numeric_limits<std::uint64_t>::lowest();
				}
			}
		}

       		return below;
    	}

	template <typename T>
	inline bool Encoding::isAboveRepresentation(const T value) const noexcept
	{
		bool above = false;
		if (this->isFloatingPoint())
		{
			if (this->is<float>())
			{
				above = std::numeric_limits<float>::max() < value;
			}
			else if (this->is<double>())
			{
				above = std::numeric_limits<double>::max() < value;
			}
		}
		else if (this->isStandardInteger())
		{
			std::size_t numberOfBits = this->computeRequiredBits();
			if (numberOfBits == 8u)
			{
				if (this->isSigned())
				{
				    above = std::numeric_limits<std::int8_t>::max() < value;
				}
				else
				{
				    above = static_cast<std::int16_t>(std::numeric_limits<uint8_t>::max()) < value;
				}
			}
			else if (numberOfBits == 16u)
			{
				if (this->isSigned())
				{
				    above = std::numeric_limits<std::int16_t>::max() < value;
				}
				else
				{
				    above = static_cast<std::int32_t>(std::numeric_limits<std::uint16_t>::max()) < value;
				}
			}
			else if (numberOfBits == 32u)
			{
				if (this->isSigned())
				{
				    above = std::numeric_limits<std::int32_t>::max() < value;
				}
				else
				{
				    above = static_cast<int64_t>(std::numeric_limits<std::uint32_t>::max()) < value;
				}
			}
			else if (numberOfBits == 64u)
			{
				if (this->isSigned())
				{
				    above = std::numeric_limits<std::int64_t>::max() < value;
				}
				else
				{
				    above = std::numeric_limits<std::uint64_t>::max() < value;
				}
			}
		}

		return above;
	}

	template <typename T>
	inline bool Encoding::is() const noexcept
	{
		return Encoding::create<T>() == *this;
	}

	inline bool Encoding::equalWithinULP(float a, float b, std::uint16_t maxULPSDiff)
	{	
		return !std::isnan(a) && !std::isnan(b)
			&& ((a == b) || (!std::isinf(a) && !std::isinf(b) && abs(Encoding::calculateDifferenceULP(a, b)) <= maxULPSDiff));
	}

	inline bool Encoding::equalWithinULP(double a, double b, std::uint32_t maxULPSDiff)
	{
		return !std::isnan(a) && !std::isnan(b)
			&& ((a == b) || (!std::isinf(a) && !std::isinf(b) && abs(Encoding::calculateDifferenceULP(a, b)) <= maxULPSDiff));
	}
}

#endif
