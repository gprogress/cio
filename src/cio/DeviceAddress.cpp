/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "DeviceAddress.h"

#include <iostream>

#include <cio/Parse.h>
#include <cio/Print.h>
#include <ostream>

#define CIO_EUI48_SHIFT 24
#define CIO_EUI48_OUI   0x0000FFFFFF000000ull
#define CIO_EUI48_LOCAL 0x0000000000FFFFFFull

#define CIO_EUI64_SHIFT 40
#define CIO_EUI64_OUI   0xFFFFFF0000000000ull
#define CIO_EUI64_LOCAL 0x000000FFFFFFFFFFull

#define CIO_EUI48_GAP   0x000000FFFF000000ull
#define CIO_EUI_OUI_ADJUST 16

#define CIO_EUI64_MULTICAST (1ull << 56)
#define CIO_EUI64_UNIVERSAL (1ull << 57)

namespace cio
{
	DeviceAddress DeviceAddress::fromMac(std::uint64_t mac) noexcept
	{
		DeviceAddress addr;
		addr.setMac(mac);
		return addr;
	}

	DeviceAddress DeviceAddress::fromExtended48(std::uint64_t eui48) noexcept
	{
		DeviceAddress addr;
		addr.setExtended48(eui48);
		return addr;
	}

	DeviceAddress DeviceAddress::fromExtended64(std::uint64_t eui64) noexcept
	{
		DeviceAddress addr;
		addr.setExtended64(eui64);
		return addr;
	}

	DeviceAddress DeviceAddress::fromText(const char *text) noexcept
	{
		return DeviceAddress(text);
	}

	DeviceAddress DeviceAddress::fromText(const char *text, std::size_t length) noexcept
	{
		return DeviceAddress(text, length);
	}

	DeviceAddress DeviceAddress::fromText(const std::string &text) noexcept
	{
		return DeviceAddress(text);
	}
	
	DeviceAddress::DeviceAddress(const char *text) noexcept :
		mExtended64(0)
	{
		this->parse(text);
	}

	DeviceAddress::DeviceAddress(const char *text, std::size_t length) noexcept :
		mExtended64(0)
	{
		this->parse(text, length);
	}
	
	DeviceAddress::DeviceAddress(const std::string &text) noexcept :
		mExtended64(0)
	{
		this->parse(text);
	}

	bool DeviceAddress::isMac() const noexcept
	{
		return (mExtended64 & CIO_EUI48_GAP) == 0;
	}

	bool DeviceAddress::isExtended48() const noexcept
	{
		return (mExtended64 & CIO_EUI48_GAP) == 0;
	}

	std::uint64_t DeviceAddress::getMac() const noexcept
	{
		std::uint64_t value = 0;

		if ((mExtended64 & CIO_EUI48_GAP) == 0)
		{
			std::uint64_t macOui = (mExtended64 & CIO_EUI64_OUI) >> CIO_EUI_OUI_ADJUST;
			std::uint64_t macLocal = (mExtended64 & CIO_EUI48_LOCAL);
			value = macOui | macLocal;
		}

		return value;
	}

	void DeviceAddress::setMac(std::uint64_t value) noexcept
	{
		std::uint64_t oui = ((value & CIO_EUI48_OUI));
		std::uint64_t local = (value & CIO_EUI48_LOCAL);
		mExtended64 = (oui  << CIO_EUI_OUI_ADJUST) | local;
	}

	std::uint64_t DeviceAddress::getExtended48() const noexcept
	{
		std::uint64_t value = 0;

		if ((mExtended64 & CIO_EUI48_GAP) == 0)
		{
			std::uint64_t macOui = (mExtended64 & CIO_EUI64_OUI) >> CIO_EUI_OUI_ADJUST;
			std::uint64_t macLocal = (mExtended64 & CIO_EUI48_LOCAL);
			value = macOui | macLocal;
		}

		return value;
	}

	void DeviceAddress::setExtended48(std::uint64_t value) noexcept
	{
		std::uint64_t oui = ((value & CIO_EUI48_OUI));
		std::uint64_t local = (value & CIO_EUI48_LOCAL);
		mExtended64 = (oui  << CIO_EUI_OUI_ADJUST) | local;
	}

	std::uint32_t DeviceAddress::getOrganizationId() const noexcept
	{
		return static_cast<std::uint32_t>(mExtended64 >> CIO_EUI64_SHIFT);
	}

	std::uint64_t DeviceAddress::getLocalId() const noexcept
	{
		return mExtended64 & CIO_EUI64_LOCAL;
	}

	bool DeviceAddress::isUniversal() const noexcept
	{
		return (mExtended64 & CIO_EUI64_UNIVERSAL) != 0;
	}

	std::size_t DeviceAddress::printMac(char *text, std::size_t len) const noexcept
	{
		return this->printExtended48(text, len);
	}

	std::size_t DeviceAddress::printExtended48(char *text, std::size_t len) const noexcept
	{
		std::size_t length = 0;

		if (this->isExtended48())
		{
			length = 17;

			if (len >= 17)
			{
				std::uint64_t eui48 = this->getExtended48();
				std::uint64_t big = Big::order(eui48);
				const std::uint8_t *ptr = reinterpret_cast<const std::uint8_t *>(&big);
				
				// First 2 bytes of ptr will be 0, skip them
				
				cio::printByte(ptr[2], text);
				text[2] = ':';
				cio::printByte(ptr[3], text + 3);
				text[5] = ':';
				cio::printByte(ptr[4], text + 6);
				text[8] = ':';
				cio::printByte(ptr[5], text + 9);
				text[11] = ':';
				cio::printByte(ptr[6], text + 12);
				text[14] = ':';
				cio::printByte(ptr[7], text + 15);

				// null terminate if buffer is longer
				if (len > 17)
				{
					std::memset(text + 17, 0, len - 17);
				}
			}
		}

		return length;
	}

	std::size_t DeviceAddress::printExtended64(char *text, std::size_t len) const noexcept
	{
		std::size_t length = 23;

		if (len >= 23)
		{
			std::uint64_t big = Big::order(mExtended64);
			const std::uint8_t *ptr = reinterpret_cast<const std::uint8_t *>(&big);
			
			cio::printByte(ptr[0], text);
			text[2] = ':';
			cio::printByte(ptr[1], text + 3);
			text[5] = ':';
			cio::printByte(ptr[2], text + 6);
			text[8] = ':';
			cio::printByte(ptr[3], text + 9);
			text[11] = ':';
			cio::printByte(ptr[4], text + 12);
			text[14] = ':';
			cio::printByte(ptr[5], text + 15);
			text[17] = ':';
			cio::printByte(ptr[6], text + 18);
			text[20] = ':';
			cio::printByte(ptr[7], text + 21);
			
			// null terminate if buffer is longer
			if (len > 23)
			{
				std::memset(text + 23, 0, len - 23);
			}
		}

		return length;
	}

	std::size_t DeviceAddress::print(char *text, std::size_t len) const noexcept
	{
		std::size_t length = 0;

		// EUI48 and MAC are actually same thing, so only one case needed
		if (this->isExtended48())
		{
			length = this->printExtended48(text, len);
		}
		else
		{
			length = this->printExtended64(text, len);
		}

		return length;
	}

	TextParseStatus DeviceAddress::parse(const char *text) noexcept
	{
		TextParseStatus parse;

		if (text)
		{
			std::size_t len = std::strlen(text);
			parse = this->parse(text, len);
		}
		else
		{
			parse.status = Validation::Missing;
		}

		return parse;
	}

	TextParseStatus DeviceAddress::parse(const std::string &text) noexcept
	{
		return this->parse(text.c_str(), text.size());
	}

	TextParseStatus DeviceAddress::parse(const char *text, std::size_t len) noexcept
	{
		TextParseStatus parse;
		std::size_t off = 0;
		std::uint8_t byte = 0;

		std::uint64_t raw = 0;
		std::size_t length = 0;
		mExtended64 = 0;

		if (text && len >= 2)
		{
			if (cio::parseByte(text[0], text[1], &byte))
			{
				raw = byte;
				length = 1;
				off += 2;
				
				parse.parsed += 2;

				while (off + 3 <= len)
				{
					if (text[off] == ':' && cio::parseByte(text[off + 1], text[off + 2], &byte))
					{
						raw = (raw << 8) | static_cast<std::uint64_t>(byte);
						++length;
						off += 3;
						parse.parsed += 3;
					}
					else
					{
						parse.status = Validation::InvalidValue;
						parse.terminator = text[off];
						break;
					}
				}

				// No other failures detected, validate data length
				if (parse.status == Validation::None)
				{
					if (length < 6)
					{
						parse.status = Validation::TooShort;
						this->setExtended48(raw);
					}
					else if (length > 8)
					{
						parse.status = Validation::TooLong;
						this->setExtended64(raw);
					}
					else if (length == 6)
					{
						parse.status = Validation::Exact;
						this->setExtended48(raw);
					}
					else if (length == 7)
					{
						parse.status = Validation::LosslessConversion;
						this->setExtended64(raw);
					}
					else if (length == 8)
					{
						parse.status = Validation::Exact;
						this->setExtended64(raw);
					}
				}
			}
			else
			{
				parse.status = Validation::InvalidValue;
				parse.terminator = text[0];
			}
		}
		else
		{
			if (text && length > 0)
			{
				parse.status = Validation::TooShort;
				parse.terminator = text[0];
			}
			else
			{
				parse.status = Validation::Missing;
			}
		}

		return parse;
	}

	std::string print(const DeviceAddress &address)
	{
		char text[24];
		std::size_t actual = address.print(text, 24);
		return std::string(text, text + actual);
	}

	std::ostream &operator<<(std::ostream &stream, const DeviceAddress &address)
	{
		char text[24];
		std::size_t actual = address.print(text, 24);
		return stream << text;
	}
}

