/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Input.h"

#include "Buffer.h"
#include "Class.h"
#include "Metadata.h"
#include "ModeSet.h"
#include "Output.h"
#include "Path.h"
#include "ProtocolFactory.h"
#include "Resize.h"
#include "Response.h"
#include "Seek.h"

namespace cio
{
	Class<Input> Input::sMetaclass("cio::Input");

	const Metaclass &Input::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Input::~Input() noexcept = default;

	const Metaclass &Input::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Input::clear() noexcept
	{
		// nothing to do
	}

	ModeSet Input::openWithFactory(const Path& resource, ModeSet modes, ProtocolFactory* factory)
	{
		return ModeSet();
	}

	Metadata Input::metadata() const
	{
		return Metadata(this->location(), this->modes(), this->type(), this->size());
	}

	Resource Input::type() const noexcept
	{
		return Resource::Unknown;
	}

	ModeSet Input::modes() const noexcept
	{
		return ModeSet();
	}

	Path Input::location() const
	{
		return Path();
	}

	Length Input::size() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	bool Input::empty() const noexcept
	{
		return this->size() == 0;
	}

	bool Input::isOpen() const noexcept
	{
		return false;
	}

	cio::ModeSet Input::openToRead(const cio::Path &resource)
	{
		return this->openWithFactory(resource, cio::Mode::Load | cio::Mode::Read, cio::ProtocolFactory::getDefault());
	}

	Length Input::calculateReadPosition(Length value, Seek mode) const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;

		switch (mode)
		{
			case Seek::None:
				result = Length();
				break;

			case Seek::Begin:
				result = value;
				break;

			case Seek::Current:
			{
				Length current = this->getReadPosition();
				if (current.known())
				{
					result = current + value;
				}
				break;
			}

			case Seek::End:
			{
				Length base = this->size();
				if (base.known())
				{
					result = base + value;
				}
				break;
			}

			default:
				break;
		}

		return result;
	}

	Length Input::getReadPosition() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	Progress<Length> Input::requestSeekToRead(Length /*position*/, Seek /*mode*/) noexcept
	{
		return cio::fail(Action::Seek, Reason::Unsupported);
	}

	Progress<std::size_t> Input::requestReadFromPosition(void * /*data*/, std::size_t /*length*/, Length /*position*/, Seek /*mode*/) noexcept
	{
		return cio::fail(Action::Read, Reason::Unsupported);
	}

	Length Input::readable() const noexcept
	{
		Length available = CIO_UNKNOWN_LENGTH;
		Length current = this->getReadPosition();

		if (current.known())
		{
			Length total = this->size();
			if (total.known())
			{
				if (current < total)
				{
					available = total - current;
				}
				else
				{
					available.clear();
				}
			}
		}

		return available;
	}

	Text Input::viewText(std::size_t bytes)
	{
		return this->getText(bytes);
	}

	Text Input::getText(std::size_t bytes)
	{
		Text text;
		char temp[512] = { };
		Buffer buffer;
		std::size_t offset = 0;

		Progress<std::size_t> processed;
		while (processed.count < bytes)
		{
			char c = 0;
			processed += this->requestRead(&c, 1);

			if (processed.failed())
			{
				if (processed.reason != Reason::Underflow)
				{
					throw Exception(processed);
				}

				break;
			}

			if (offset == 512)
			{
				buffer.remaining(512);
				buffer.write(temp, 512);
				offset = 0;
			}

			// Append to temp buffer
			temp[offset++] = c;

			// Null terminator successfully read, we're done
			if (!c)
			{
				break;
			}
		}

		// Simple case, no new memory allocations
		if (buffer.empty())
		{
			text.internalize(temp, offset);
		}
		else
		{
			if (offset > 0)
			{
				buffer.remaining(offset);
				buffer.write(temp, offset);
			}

			buffer.flip();
			text = buffer.getText().duplicate();
		}

		return text;
	}

	Text Input::getTextLine(std::size_t bytes)
	{
		Text text;
		char temp[512] = { };
		Buffer buffer;
		std::size_t offset = 0;

		Progress<std::size_t> processed;
		while (processed.count < bytes)
		{
			char c = 0;
			processed += this->requestRead(&c, 1);

			if (processed.failed())
			{
				if (processed.reason != Reason::Underflow)
				{
					throw Exception(processed);
				}

				break;
			}

			if (offset == 512)
			{
				buffer.remaining(512);
				buffer.write(temp, 512);
				offset = 0;
			}

			// Append to temp buffer
			temp[offset++] = c;

			// Null terminator or newline successfully read, we're done
			if (!c || c == '\n')
			{
				break;
			}
		}

		// Simple case, no new memory allocations
		if (buffer.empty())
		{
			text.internalize(temp, offset);
		}
		else
		{
			if (offset > 0)
			{
				buffer.remaining(offset);
				buffer.write(temp, offset);
			}

			buffer.flip();
			text = buffer.getText().duplicate();
		}

		return text;
	}

	Progress<std::size_t> Input::requestRead(void * /*data*/, std::size_t /*length*/) noexcept
	{
		return cio::fail(Action::Seek, Reason::Unsupported);
	}

	Progress<Length> Input::requestDiscard(Length bytes) noexcept
	{
		unsigned char tmp[512];
		Length bufsize(512);

		std::size_t toLoad = std::min(bytes, bufsize).size();

		Progress<std::size_t> chunkProgress = this->requestRead(tmp, toLoad);
		Progress<Length> result = chunkProgress;

		while (!chunkProgress.failed() && result.count < bytes)
		{
			Length remaining = bytes - chunkProgress.count;
			toLoad = std::min(remaining, bufsize).size();
			chunkProgress = this->requestRead(tmp, toLoad);
			result += chunkProgress;
		}

		return result;
	}

	Resize Input::isReadSeekable() const noexcept
	{
		return Resize::Unknown;
	}

	Response<std::size_t> Input::recommendReadAlignment() const noexcept
	{
		return Response<std::size_t>(Reason::Missing, sizeof(void *));
	}
	
	State Input::setReadTimeout(std::chrono::milliseconds timeout)
	{
		return cio::fail(Action::None, Reason::Unsupported);
	}
			
	std::chrono::milliseconds Input::getReadTimeout() const
	{
		return std::chrono::milliseconds(0);
	}
	
	Progress<std::size_t> Input::requestReadBuffer(std::size_t /*bytes*/) noexcept
	{
		return cio::fail(Action::Read, Reason::Unsupported);
	}

	State Input::adviseUpcomingRead(Length pos, Seek mode, std::size_t bytes) noexcept
	{
		return cio::fail(Action::Read, Reason::Unsupported);
	}
	
	void Input::discard(Length bytes)
	{
		if (bytes.known() && bytes > 0)
		{
			Progress<Length> current = this->requestDiscard(bytes);
			while (current.count < bytes && !current.failed())
			{
				current += this->requestDiscard(bytes - current.count);
			}

			if (current.count != bytes)
			{
				throw cio::Exception(current, "Could not discard input bytes");
			}
		}
	}

	void Input::seekToRead(Length position, Seek mode)
	{
		Progress<Length> current = this->requestSeekToRead(position, mode);
		while (current.unfinished())
		{
			current = this->requestSeekToRead(position, mode);
		}

		if (!current.succeeded())
		{
			throw cio::Exception(current, "Could not set read position");
		}
	}

	void Input::read(void *data, std::size_t length)
	{
		if (length > 0)
		{
			Progress<std::size_t> current = this->requestRead(data, length);

			while (current.count < length && !current.failed())
			{
				current += this->requestRead(static_cast<std::uint8_t *>(data) + current.count, length - current.count);
			}

			if (current.count < length)
			{
				throw cio::Exception(current, "Could not read data");
			}
		}
	}

	void Input::readFromPosition(void *data, std::size_t length, Length position, Seek mode)
	{
		if (length > 0)
		{
			Progress<std::size_t> current = this->requestReadFromPosition(data, length, position, mode);

			while (current.count < length && !current.failed())
			{
				current += this->requestReadFromPosition(static_cast<std::uint8_t *>(data) + current.count,
					length - current.count, position + current.count, mode);
			}

			if (current.count < length)
			{
				throw cio::Exception(current, "Could not read data from position");
			}
		}
	}
	
	Progress<Length> Input::copyTo(Output &output, Length length)
	{
		Progress<Length> progress = this->requestCopyTo(output, length);
		if (progress.failed())
		{
			throw Exception(progress, "Failed to copy input to output");
		}
		return progress;
	}
	
	Progress<Length> Input::requestCopyTo(Output &output, Length length)
	{
		Progress<Length>  progress(Action::Copy, 0, length);
		if (length > 0)
		{
			char bytes[4096];
			Buffer buffer(bytes, 4096, nullptr);
			buffer.flip(); // start empty
			
			Length acquired;
			
			// if we know input remaining, use it, otherwise run with specified length
			Length remaining = std::min(length, this->readable());
			progress.total = remaining;
			
			while (!progress.completed())
			{
				if (buffer.available())
				{
					Progress<Length> drained = output.requestDrain(buffer);
					remaining -= drained.count;	
					progress.count += drained.count;
					if (drained.failed())
					{
						progress.fail(drained.reason);
					}
				}
				else if (remaining > 0)
				{
					buffer.reset();
					buffer.limit(remaining.size()); // clamps to actual size
					
					Progress<std::size_t> current = this->requestFill(buffer);
					buffer.flip();
					
					acquired += current.count;
					
					if (current.failed())
					{
						progress.fail(current.reason);
					}
					else if (current.reason == Reason::Underflow)
					{
						remaining = buffer.limit();
					}
				}
				else
				{
					progress.total = acquired;
					progress.succeed();
				}
			}
		}
		
		return progress;
	}
	
	Progress<std::size_t> Input::requestFill(Buffer &buffer) noexcept
	{
		Progress<std::size_t> action = this->requestRead(buffer.current(), buffer.remaining());
		buffer.skip(action.count);
		return action;
	}

	Progress<std::size_t> Input::requestFill(Buffer &buffer, Length offset, Seek whence) noexcept
	{
		Progress<std::size_t> action = this->requestReadFromPosition(buffer.current(), buffer.remaining(), offset, whence);
		buffer.skip(action.count);
		return action;
	}

	Progress<std::size_t> Input::fill(Buffer &buffer)
	{
		Progress<std::size_t> action;
		
		try
		{
			std::size_t bytes = buffer.remaining();
			this->read(buffer.current(), bytes);
			buffer.skip(bytes);
		}
		catch (const Exception &e)
		{
			buffer.skip(e.progress().count);
			throw;
		}
		
		return action;
	}

	Progress<std::size_t> Input::fill(Buffer &buffer, Length position, Seek whence)
	{
		Progress<std::size_t> action;

		try
		{
			std::size_t bytes = buffer.remaining();
			this->readFromPosition(buffer.current(), bytes, position, whence);
			buffer.skip(bytes);
		}
		catch (const Exception &e)
		{
			buffer.skip(e.progress().count);
			throw;
		}

		return action;
	}

	Progress<std::size_t> Input::append(Buffer &buffer) noexcept
	{
		 Progress<std::size_t> action;
		 std::size_t mark = buffer.position();
		 buffer.unflip();
		 action = this->requestFill(buffer);
		 buffer.flip(mark);
		 return action;
	}
			
	Progress<std::size_t> Input::receive(Buffer &buffer) noexcept
	{
		 Progress<std::size_t> action(Action::Read);
		 buffer.compact();
		 buffer.unflip();
		 action = this->requestFill(buffer);
		 buffer.flip();
		 return action;
	}

	Input::operator bool() const noexcept
	{
		return this->isOpen();
	}
}
