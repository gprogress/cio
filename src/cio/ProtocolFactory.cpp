/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "ProtocolFactory.h"

#include "Channel.h"
#include "Class.h"
#include "Exception.h"
#include "Metadata.h"
#include "ProtocolFilter.h"
#include "State.h"

// Note when a unified build is performed, the end of this file will will set up protocol registrars for all integrated modules

namespace cio
{
	Class<ProtocolFactory> ProtocolFactory::sMetaclass("cio::ProtocolFactory");

	const Text ProtocolFactory::sFileDefaultPrefix("file");

	// ProtocolFactory ProtocolFactory::sDefault; Located in Static.cpp

	const Metaclass &ProtocolFactory::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &ProtocolFactory::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	ProtocolFactory::ProtocolFactory() noexcept :
		Protocol(sFileDefaultPrefix)
	{
		// nothing more to do
	}

	ProtocolFactory::ProtocolFactory(const ProtocolFactory &in) = default;

	ProtocolFactory::ProtocolFactory(ProtocolFactory &&in) noexcept = default;

	ProtocolFactory &ProtocolFactory::operator=(const ProtocolFactory &in) = default;

	ProtocolFactory &ProtocolFactory::operator=(ProtocolFactory &&in) noexcept = default;

	ProtocolFactory::~ProtocolFactory() noexcept = default;

	void ProtocolFactory::clear() noexcept
	{
		Protocol::clear();
		mDefaultPrefix = sFileDefaultPrefix;
	}	
	
	std::unique_ptr<Protocol> ProtocolFactory::getProtocol(const Path &path, Resource type) const
	{
		std::unique_ptr<Protocol> protocol;

		const ProtocolFilter &filter = this->findBestFilter(path, type);
		if (filter)
		{
			protocol = filter.metaclass->create<Protocol>();
		}
		else
		{
			throw Exception(Reason::Unsupported, "No protocol available to handle path for type");
		}
		
		return protocol;
	}

	std::unique_ptr<Protocol> ProtocolFactory::readProtocol(const Path &path, Resource type) const
	{
		// TODO limit this to actually being read-only
		return this->getProtocol(path, type);
	}

	std::unique_ptr<Protocol> ProtocolFactory::openProtocol(const Path &path, Resource type)
	{
		return this->getProtocol(path, type);
	}

	Metadata ProtocolFactory::readMetadata(const Path &input) const
	{
		Metadata metadata;
		std::unique_ptr<Protocol> protocol = this->readProtocol(input);

		if (protocol)
		{
			metadata = protocol->readMetadata(input);
		}

		return metadata;
	}

	Metadata ProtocolFactory::create(const Metadata &input, Traversal createParentResource)
	{
		Metadata created;
		std::unique_ptr<Protocol> protocol = this->openProtocol(input.getLocation(), input.getType());

		if (protocol)
		{
			created = protocol->create(input, createParentResource);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}

		return created;
	}

	Resource ProtocolFactory::readType(const Path &path) const
	{
		Resource type = Resource::Unknown;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path);

		if (protocol)
		{
			type = protocol->readType(path);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return type;
	}

	ModeSet ProtocolFactory::readModes(const Path &path, ModeSet desired) const
	{
		ModeSet modes;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path);

		if (protocol)
		{
			modes = protocol->readModes(path, desired);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return modes;
	}

	Length ProtocolFactory::readSize(const Path &path) const
	{
		Length length(0);
		std::unique_ptr<Protocol> protocol = this->readProtocol(path);

		if (protocol)
		{
			length = protocol->readSize(path);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return length;
	}

	std::unique_ptr<Protocol> ProtocolFactory::readDirectory(const Path &path, ModeSet modes) const
	{
		std::unique_ptr<Protocol> dir;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path, Resource::Directory);

		if (protocol)
		{
			dir = protocol->readDirectory(path, modes);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}

		return dir;
	}

	std::unique_ptr<Protocol> ProtocolFactory::openDirectory(const Path &path, ModeSet modes, Traversal createMissing)
	{
		std::unique_ptr<Protocol> dir;
		std::unique_ptr<Protocol> protocol = this->openProtocol(path, Resource::Directory);

		if (protocol)
		{
			dir = protocol->openDirectory(path, modes, createMissing);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}

		return dir;
	}
	
	State ProtocolFactory::createDirectory(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
		
		std::unique_ptr<Protocol> protocol = this->openProtocol(path, Resource::Directory);
		
		if (protocol)
		{
			state = protocol->createDirectory(path, modes, createParents);
		}
		else
		{
			state.fail(Reason::Unsupported);
			throw Exception(state);
		}
		
		return state;
	}
			
	State ProtocolFactory::createParents(const Path &path, Traversal depth)
	{
		State state(Action::Create);
		
		std::unique_ptr<Protocol> protocol = this->openProtocol(path.getParent(), Resource::Directory);
		
		if (protocol)
		{
			state = protocol->createParents(path, depth);
		}
		else
		{
			state.fail(Reason::Unsupported);
			throw Exception(state);
		}
		
		return state;
	}
	
	std::vector<Path> ProtocolFactory::listDirectory(const Path &path, Traversal mode) const
	{
		std::vector<Path> result;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path, Resource::Directory);

		if (protocol)
		{
			result = protocol->listDirectory(path, mode);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return result;
	}

	std::size_t ProtocolFactory::enumerateDirectory(const Path &path, Traversal mode, const std::function<bool(const Path &)> &receiver) const
	{
		std::size_t found = 0;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path, Resource::Directory);

		if (protocol)
		{
			found = protocol->enumerateDirectory(path, mode, receiver);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return found;
	}

	Progress<std::uint64_t> ProtocolFactory::erase(const Path &path, Traversal mode)
	{
		Progress<std::uint64_t> erased(Action::Remove);
		std::unique_ptr<Protocol> protocol = this->openProtocol(path);

		if (protocol)
		{
			erased = protocol->erase(path, mode);
		}
		else
		{
			erased.fail(Reason::Unsupported);
			throw Exception(erased);
		}
		
		return erased;
	}

	std::unique_ptr<Input> ProtocolFactory::openInput(const Path &path, ModeSet modes) const
	{
		std::unique_ptr<Input> reader;
		std::unique_ptr<Protocol> protocol = this->readProtocol(path, Resource::File);

		if (protocol)
		{
			reader = protocol->openInput(path, modes);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return reader;
	}

	std::unique_ptr<Output> ProtocolFactory::openOutput(const Path &path, ModeSet modes, Traversal createParents)
	{
		std::unique_ptr<Output> writer;
		std::unique_ptr<Protocol> protocol = this->openProtocol(path, Resource::File);

		if (protocol)
		{
			writer = protocol->openOutput(path, modes, createParents);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return writer;
	}


	std::unique_ptr<Channel> ProtocolFactory::openChannel(const Path &path, ModeSet modes, Traversal createParents)
	{
		std::unique_ptr<Channel> writer;
		std::unique_ptr<Protocol> protocol = this->openProtocol(path, Resource::File);

		if (protocol)
		{
			writer = protocol->openChannel(path, modes, createParents);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return writer;
	}

	State ProtocolFactory::createFile(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state;
		
		std::unique_ptr<Protocol> protocol = this->openProtocol(path, Resource::File);
		if (protocol)
		{
			state = protocol->createFile(path, modes, createParents);
		}
		else
		{
			state.fail(Reason::Unsupported);
			throw Exception(state);
		}
		
		return state;
	}

	Path ProtocolFactory::readLink(const Path &path, Traversal depth) const
	{	
		Path target;
		
		std::unique_ptr<Protocol> protocol = this->readProtocol(path, Resource::Link);
		if (protocol)
		{
			target = protocol->readLink(path, depth);
		}
		else
		{
			throw Exception(Reason::Unsupported);
		}
		
		return target;
	}
			
	State ProtocolFactory::createLink(const Path &link, const Path &target, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
		
		std::unique_ptr<Protocol> protocol = this->openProtocol(link, Resource::Link);
		if (protocol)
		{
			state = protocol->createLink(link, target, modes, createParents);
		}
		else
		{
			state.fail(Reason::Unsupported);
			throw Exception(state);
		}
		
		return state;
	}

	Progress<std::uint64_t> ProtocolFactory::copy(const Path &input, const Path &output, Traversal depth, Traversal createParents)
	{
		Progress<std::uint64_t> progress(Action::Copy);
		std::unique_ptr<Protocol> protocol = this->openProtocol(input);
		if (protocol)
		{
			if (protocol->compatible(output))
			{
				progress = protocol->copy(input, output, depth, createParents);
			}
			else
			{
				std::unique_ptr<Protocol> target = this->openProtocol(output);
				if (target)
				{
					progress = Protocol::crossCopy(input, *protocol, output, *target, depth, createParents);
				}
				else
				{
					progress.fail(Reason::Unsupported);
					throw Exception(progress);
				}
			}
		}
		else
		{
			progress.fail(Reason::Unsupported);
			throw Exception(progress);
		}
		
		return progress;
	}
	
	Progress<std::uint64_t> ProtocolFactory::move(const Path &input, const Path &output, Traversal createParents)
	{
		Progress<std::uint64_t> progress(Action::Move);
		std::unique_ptr<Protocol> protocol = this->openProtocol(input);
		if (protocol)
		{
			if (protocol->compatible(output))
			{
				progress = protocol->move(input, output, createParents);
			}
			else
			{
				std::unique_ptr<Protocol> target = this->openProtocol(output);
				if (target)
				{
					progress = Protocol::crossMove(input, *protocol, output, *target, createParents);
				}
				else
				{
					progress.fail(Reason::Unsupported);
					throw Exception(progress);
				}
			}
		}
		else
		{
			progress.fail(Reason::Unsupported);
			throw Exception(progress);
		}
		
		return progress;
	}
}

	
