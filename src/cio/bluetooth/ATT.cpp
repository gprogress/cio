/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ATT.h"

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sAttText[] = {
			"None",
			"Error",
			"Exchange MTU Request",
			"Exchange MTU Response",
			"Find Information Request",
			"Find Information Response",
			"Find By Type Request",
			"Find By Type Response",
			"Read By Type Request",
			"Read By Type Response",
			"Read Request",
			"Read Response",
			"Read Blob Request",
			"Read Blob Response",
			"Read Multiple Request",
			"Read Multiple Response",
			"Read By Group Request",
			"Read By Group Response",
			"Write Request",
			"Write Response",
			"Unknown", // Gap
			"Unknown", // Gap
			"Prepare Write Request",
			"Prepare Write Response",
			"Execute Write Request",
			"Execute Write Response",
			"Unknown", // Gap
			"Notify",
			"Unknown",
			"Indicate",
			"Confirm",
			// discontinuity
			"Write Command",
			"Signed Write Command"
		};
		
		const char *print(ATT code) noexcept
		{
			const char *text = "Unknown";
			if (code <= ATT::Confirm)
			{
				text = sAttText[static_cast<std::size_t>(code)];
			}
			else if (code == ATT::Write)
			{
				text = sAttText[0x1F];
			}
			else if (code == ATT::SignedWrite)
			{
				text = sAttText[0x20];
			}
			return text;
		}
		
		std::ostream &operator<<(std::ostream &s, ATT code)
		{
			return s << print(code);
		}
	}
}
