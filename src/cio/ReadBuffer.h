/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_READBUFFER_H
#define CIO_READBUFFER_H

#include "Types.h"

#include "Factory.h"
#include "Exception.h"
#include "Order.h"

#include <cstring>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Read Buffer class provides a high-performance read API outside of the Input / Output / Channel interface
	 * intended for optimized decoding of text and binary content. It is intended for use with the
	 * Input, Output, and Channel interfaces in situations where raw performance is of the utmost importance.
	 */
	class CIO_API ReadBuffer
	{
		public:
			/** Buffer and ReadBuffer interact directly for performance reasons */
			friend class Buffer;

			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct a new empty buffer with initial capacity of 0.
			* The position and limit are also initialized to 0.
			* The allocator is disabled.
			*/
			ReadBuffer() noexcept;
			
			/**
			 * Construct a copy of a buffer.
			 * If the input buffer is a non-owning reference to data, this buffer will reference the same data.
			 * Otherwise this buffer will duplicate the input data.
			 *
			 * @param buffer The input buffer
			 */
			ReadBuffer(const Buffer &buffer) noexcept;
			
			/**
			 * Constructs a read buffer to view the given read-write buffer, transferring ownership of the memory.
			 * On construction the position, limit, and size are initialized to match the input buffer.
			 * However they will be separately usable after that.
			 *
			 * @param buffer The input buffer
			 */
			ReadBuffer(Buffer &&buffer) noexcept;
			
			/**
			 * Construct a buffer to non-intrusively use the given piece of memory.
			 * The capacity and limit are initialized to the provided capacity.
			 * The position is initialized to 0.
			 * The memory will not be destroyed when this buffer is destroyed or cleared.
			 *
			 * @param data The pointer to the memory to use
			 * @param capacity The number of bytes in the memory to use as the capacity
			 */
			ReadBuffer(const void *data, std::size_t capacity) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * The memory will not be destroyed when this buffer is destroyed or cleared.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param limit The initial limit
			 * @param position The initial position
			 */
			ReadBuffer(const void *data, std::size_t capacity, std::size_t limit, std::size_t position) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared.
			 * The capacity will be set to the given capacity.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 */
			ReadBuffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared.
			 * The capacity will be set to the given capacity.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 * @param limit The initial limit
			 * @param position The initial position
			 */
			ReadBuffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator, std::size_t limit, std::size_t position) noexcept;

			/**
			 * Construct a copy of a buffer.
			 * If the input buffer is a non-owning reference to data, this buffer will reference the same data.
			 * Otherwise this buffer will duplicate the input data.
			 *
			 * @param in The buffer to copy
			 */
			ReadBuffer(const ReadBuffer &in);

			/**
			* Move buffer underlying data into a new instance of this class.
			*
			* @param in The buffer to move
			*/
			ReadBuffer(ReadBuffer &&in) noexcept;

			/**
			 * Copy a buffer.
			 *
			 * @param in The buffer to copy
			 * @return this buffer
			 */
			ReadBuffer &operator=(const ReadBuffer &in);

			/**
			* Move buffer underlying data into this object.
			*
			* @param in The buffer to move
			* @return this buffer
			*/
			ReadBuffer &operator=(ReadBuffer &&in) noexcept;

			/**
			* Destructor.
			*/
			~ReadBuffer() noexcept;

			/**
			 * Sets the capacity, limit, and position to zero and deallocates the array.
			 */
			void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this class.
			 * 
			 * @return the metaclass
			 */
			const Metaclass &getMetaclass() const noexcept;

			// Baseline buffer API - direct manipulation of pointers and sizes
			
			/**
			 * Interprets the buffer in a Boolean context.
			 * It is considered true if it has bytes remaining between current position and limit.
			 *
			 * @return the input as a Boolean
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Gets the raw data array.
			 *
			 * @return the raw data array
			 */
			inline const std::uint8_t *data() const noexcept;

			/**
			* Gets the raw data array offset to the current position.
			*
			* @return the raw data array at the current position
			*/
			inline const std::uint8_t *current() const noexcept;

			/**
			 * Sets the position to zero and the limit to the capacity.
			 * Typically used to start a new read or write operation.
			 *
			 * @return this buffer for convenience
			 */
			inline ReadBuffer &reset() noexcept;

			/**
			 * Sets the limit to the current position and the current position to zero.
			 * Typically used after data is written to the buffer to set up to read that data back.
			 *
			 * @return the buffer
			 */
			inline ReadBuffer &flip() noexcept;
			
			/**
			 * Sets the limit to the current position and the current position to the given start.
			 * Typically used after data is written to the buffer to set up to read that data back,
			 * but starting at a known mark offset rather than at the beginning.
			 *
			 * @param start The new position after flipping
			 * @return the buffer
			 */
			inline ReadBuffer &flip(std::size_t start) noexcept;
			
			/**
			 * Sets the position to the current limit and the limit to the current size.
			 * Typically used to append more data to the unused portion at the end of the buffer.
			 *
			 * @return the buffer
			 */
			inline ReadBuffer &unflip() noexcept;

			/**
			 * Directly gets the memory position of the buffer.
			 * This is equivalent to calling getReadPosition() or getWritePosition() but returns std::size_t.
			 *
			 * @return the current buffer position
			 */
			inline std::size_t position() const noexcept;

			/**
			 * Directly sets the memory position of the buffer.
			 * This is equivalent to calling requestSeekToRead or requestSeekToWrite with Seek::Begin but operates directly
			 * on the std::size_t field and returns the position without a status.
			 *
			 * The position is clipped to the current limit.
			 *
			 * @param position The desired new position
			 * @return the actual position set
			 */
			inline std::size_t position(std::size_t position) noexcept;

			/**
			 * Directly advances the memory position of the buffer.
			 * This is equivalent to calling requestSeekToRead or requestSeekToWrite with Seek::Current but operates directly
			 * on the std::size_t field and returns the delta without a status.
			 *
			 * The position is clipped to the current limit.
			 * 
			 * @param bytes The number of bytes to skip
			 * @return the number of bytes actually advanced
			 */
			inline std::size_t skip(std::size_t bytes) noexcept;

			/**
			 * Directly gets the memory limit of the buffer.
			 *
			 * @return the current buffer limit
			 */
			inline std::size_t limit() const noexcept;

			/**
			 * Directly sets the memory limit of the buffer.
			 *
			 * The position is clipped to the current size.
			 *
			 * @param limit The desired new limit
			 * @return the actual position set
			 */
			inline std::size_t limit(std::size_t limit) noexcept;

			/**
			 * Sets both the position and limit in one call.
			 * They are both clipped to the size and the position is clipped to the limit.
			 *
			 * @param position The desired position
			 * @param limit The desired limit
			 */
			inline void reslice(std::size_t position, std::size_t limit) noexcept;

			/**
			 * Gets the number of bytes currently remaining between the position and the limit.
			 * This is always equal to readable() and writable() but returned as std::size_t.
			 *
			 * @return the number of bytes available
			 */
			inline std::size_t remaining() const noexcept;

			/**
			 * Gets whether there are any bytes available between current position and the limit.
			 *
			 * @return whether there are available bytes between the position and the limit
			 */
			inline bool available() const noexcept;
			
			/**
			 * Gets the number of "unused" bytes in the buffer between the limit and the size.
			 *
			 * @return the number of unused bytes between the limit and size
			 */
			inline std::size_t unused() const noexcept;
			
			/**
			 * Gets the allocator currently in use for this class.
			 * If the "null" allocator, the data is merely referenced rather than owned.
			 * 
			 * @return the allocator in use
			 */
			inline Factory<std::uint8_t> allocator() const noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * The data array will not be managed by this Buffer and will not be destroyed by it.
			 * The capacity will be set to the given capacity.
			 * The allocator will be set to nullptr.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 */
			void bind(const void *data, std::size_t capacity) noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, then the data array will be destroyed when this Buffer is destroyed or cleared.
			 * The position will be set to 0 and the limit to the size.
			 *
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the bytes, if any
			 */
			void bind(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept;

			/**
			 * Internalizes the data so that the buffer owns it.
			 * If there is no current allocator, the default allocator will be adopted
			 * and the current data will be copied. Otherwise nothing happens.
			 *
			 * @return the internalized data
			 */
			const std::uint8_t *internalize();

			/**
			 * Creates a copy of the given data bytes using the default metaclass for std::uint8_t as the allocator.
			 *
			 * @param data The data to duplicate
			 * @param length The number of bytes to duplicate
			 * @return the duplicated data
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			const std::uint8_t *internalize(const void *data, std::size_t length);

			/**
			 * Creates a copy of the given data bytes using the given metaclass as an allocator.
			 *
			 * @param data The data to duplicate
			 * @param length The number of bytes to duplicate
			 * @return the duplicated data
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			const std::uint8_t *internalize(const void *data, std::size_t length, Factory<std::uint8_t> allocator);

			/**
			 * Duplicates the current data using the default allocator and returns a buffer with the duplicate.
			 * Since the data is copied and therefore mutable, it is returned as a full read-write buffer.
			 *
			 * @return the duplicate buffer
			 */
			Buffer duplicate() const;

			/**
			 * Gets the maximum capacity of the buffer.
			 * This is theoretically the memory size limit CIO_UNKNOWN_SIZE although in practice allocations
			 * will fail at lower values.
			 *
			 * @return the theoretical maximum capacity
			 */
			inline std::size_t capacity() const noexcept;

			/**
			 * Gets the current data array capacity of the buffer.
			 *
			 * @return the current array capacity
			 */
			inline std::size_t size() const noexcept;

			/**
			 * Gets whether the buffer is currently empty.
			 * This is true if size() is 0.
			 *
			 * @return whether the buffer is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Requests to read into an output buffer the given number of bytes, up to the buffer limit.
			 * If fewer bytes are available before the buffer limit, the returned byte count will reflect that.
			 * The buffer position will be advanced by the number of bytes actually read.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @return the number of bytes actually read, with either Status::Success or Status::Underflow
			 */
			inline std::size_t requestRead(void *output, std::size_t length) noexcept;

			/**
			 * Reads into an output buffer the given number of bytes.
			 * If fewer bytes are available, this method will throw an exception with Reason::Underflow and the buffer will not be advanced.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @throw Exception If not enough data existed to read
			 */
			inline void read(void *output, std::size_t length);

			/**
			 * Requests to read into an output buffer the given number of bytes at the given absolute offset without modifying the buffer position.
			 * If fewer bytes are available before the buffer limit, the returned count will reflect that.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @param position The absolute byte offset to read from
			 * @return the number of bytes actually read
			 */
			inline std::size_t requestReadFromPosition(void *output, std::size_t length, std::size_t position) const noexcept;

			/**
			 * Reads into an output buffer the given number of bytes at the given absolute offset without modifying the buffer position.
			 * If fewer bytes are available, this method will throw an exception with Reason::Underflow.
			 *
			 * @param output The data buffer to read into
			 * @param length The number of bytes to read into the output
			 * @param position The absolute byte offset to read from
			 * @throw Exception If not enough data existed to read
			 */
			inline void readFromPosition(void *output, std::size_t length, std::size_t position) const;
			
			/**
			 * Gets a single byte from the current position.
			 *
			 * @return the byte
			 * @throw cio::Exception If the read could not be done
			 */
			inline std::uint8_t get();
			
			/**
			 * Gets a C++ primitive type from the current position in Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T get();
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBig();
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Little to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittle();

			/**
			 * Gets the text currently in the buffer up to (and including) a null terminator or the end of buffer.
			 * The text bytes will be internalized to the returned text buffer for safety.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 *
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getText();

			/**
			 * Gets the text currently in the buffer up to (and including) a null terminator, the end of buffer, or the given number of bytes.
			 * The text bytes will be internalized to the returned text buffer for safety.
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getText(std::size_t bytes);

			/**
			 * Gets a referenced view of the text currently in the buffer up to (and including) a null terminator, the end of the current line,
			 * the end of the buffer, or the given number of bytes.
			 *
			 * The buffer current position will be advanced by the number of bytes actually processed.
			 *
			 * @warning Modifying the buffer content may invalidate the view.
			 *
			 * @param bytes The maximum number of bytes to read
			 * @return the text
			 * @throw std::bad_alloc If the text could not be copied
			 */
			Text getTextLine(std::size_t bytes = SIZE_MAX) noexcept;

			/**
			 * Gets a fixed text of an exact number of bytes from the buffer starting at the current position.
			 * Exactly N bytes will be read regardless of null termination.
			 *
			 * @tparam <N> The number of bytes of text to read
			 * @return the fixed text read
			 */
			template <std::size_t N>
			inline FixedText<N> getFixedText();

			/**
			 * Gets a C++ primitive type from the given absolute buffer position in Host byte order.
			 * This does not modify the current position.
			 *
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from the given absolute buffer position, converting from Big to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBigFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from the given absolute buffer position, converting from Little to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param position The position from beginning of buffer
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittleFromPosition(std::size_t position) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position in Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getFromOffset(std::size_t poffset) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position, converting from Big to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getBigFromOffset(std::size_t offset) const;

			/**
			 * Gets a C++ primitive type from an offset from the current position, converting from Little to Host byte order.
			 * This does not modify the current position.
			 * 
			 * @tparam <T> The type of primitive
			 * @param offset The offset relative to the current position
			 * @return the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline T getLittleFromOffset(std::size_t offset) const;

			/**
			 * Gets a C++ primitive type from the current position in host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void get(T &value);
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getBig(T &value);
			
			/**
			 * Gets a C++ primitive type from the current position, converting from Little to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param value The value to receive the read value
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getLittle(T &value);
			
			/**
			 * Gets an array of C++ primitive type from the current position in host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getArray(T *data, std::size_t count);
			
			/**
			 * Gets an array of C++ primitive type from the current position, converting from Big to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getBigArray(T *data, std::size_t count);
			
			/**
			 * Gets an array of C++ primitive type from the current position, converting from Little to Host byte order.
			 *
			 * @tparam <T> The type of primitive
			 * @param data The data array
			 * @param count the number of array elements
			 * @throw cio::Exception If the read could not be done
			 */
			template <typename T>
			inline void getLittleArray(T *data, std::size_t count);
			
			/**
			 * Copies bytes from the current position in this buffer to the current position in the given output buffer.
			 * At most the lesser of the output's remaining bytes and this buffer's bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param output The buffer to copy to
			 * @return the number of bytes actually copied
			 */
			std::size_t send(Buffer &output) noexcept;
			
			/**
			 * Copies bytes from the current position in this buffer to the current position in the given output buffer.
			 * At most the lesser of the output's remaining bytes and this buffer's bytes will be copied, and the provided maximum bytes will be copied.
			 * The positions of both buffers will be advanced by the amount actually copied.
			 *
			 * @param output The buffer to copy to
			 * @param maxLength The maximum number of bytes to copy to the buffer
			 * @return the number of bytes actually copied
			 */
			std::size_t send(Buffer &output, std::size_t maxLength) noexcept;

			/**
			 * Gets a read buffer that represents a read-only view of the current buffer.
			 * All fields have the same values, but the allocator on the returned read buffer will be null to indicate it's a view-only reference.
			 * Changes to the returned buffer will not affect this buffer.
			 *
			 * @return the read-only view
			 */
			ReadBuffer view() const noexcept;

			/**
			 * Gets a read buffer that represents a read-only view of the data already processed.
			 * The returned buffer will have position of 0, limit and capacity set to the position in this buffer,
			 * and point to position 0 of this buffer.
			 *
			 * Changes to the returned buffer will not affect this buffer.
			 *
			 * @return the read-only view of the data already processed
			 */
			ReadBuffer viewProcessed() const noexcept;

			/**
			 * Gets a read buffer that represents a read-only view of the currently available data.
			 * The returned buffer will have position of 0, limit and capacity set to the remaining bytes in this buffer,
			 * and point to the current position in this buffer.
			 *
			 * Changes to the returned buffer will not affect this buffer.
			 *
			 * @return the read-only view of the remaining data
			 */
			ReadBuffer viewRemaining() const noexcept;

			/**
			 * Gets a referenced view of the text already processed in the buffer from position 0 to the current position.
			 *
			 * @return the processed text view
			 */
			Text viewProcessedText() const noexcept;

			/**
			 * Gets a referenced view of the remaining text not processed in the buffer from the current position to the limit.
			 *
			 * @return the uprocessed text view
			 */
			Text viewRemainingText() const noexcept;

			/**
			 * Prints the Read Buffer itself to a returned UTF-8 string.
			 * This prints JSON elements for each class member, and dumps the entire buffer bytes to hexadecimal.
			 *
			 * @return the printed string
			 */
			std::string print() const;

			/**
			 * Prints the Read Buffer itself to a UTF-8 text buffer.
			 * This prints JSON elements for each class member, and dumps the entire buffer bytes to hexadecimal.
			 *
			 * @param buffer The text buffer to print
			 * @param capacity The capacity of the text buffer
			 * @return the printed string
			 */
			std::size_t print(char *buffer, std::size_t capacity) const noexcept;

			/**
			 * Prints the Read Buffer to a UTF-8 markup writer.
			 * This prints elements for each class member.
			 *
			 * @param writer The markup writer to print to
			 * @throw Exception If writing the markup fails
			 */
			void print(MarkupWriter &writer) const;

		private:
			/** The metaclass for this class */
			static Class<ReadBuffer> sMetaclass;
			
			/** The allocator, if any */
			Factory<std::uint8_t> mFactory = nullptr;
			
			/** The actual data */
			const std::uint8_t *mElements = nullptr;

			/** The allocated size of the data */
			std::size_t mSize = 0;

			/** The maximum valid position relative to the beginning of the chunk */
			std::size_t mLimit = 0;

			/** The current position relative to the beginning of the chunk */
			std::size_t mPosition = 0;
	};
}

/* Inline implementation */

namespace cio
{
	// Buffer API - direct calls

	inline ReadBuffer::operator bool() const noexcept
	{
		return mPosition < mLimit;
	}
	
	inline ReadBuffer &ReadBuffer::flip() noexcept
	{
		mLimit = mPosition;
		mPosition = 0;
		return *this;
	}
		
	inline ReadBuffer &ReadBuffer::flip(std::size_t start) noexcept
	{
		mLimit = mPosition;
		mPosition = std::min(mLimit, start);
		return *this;
	}
	
	inline ReadBuffer &ReadBuffer::unflip() noexcept
	{
		mPosition = mLimit;
		mLimit = mSize;
		return *this;
	}

	inline std::size_t ReadBuffer::remaining() const noexcept
	{
		return mLimit - mPosition;
	}

	inline bool ReadBuffer::available() const noexcept
	{
		return mPosition < mLimit;
	}

	inline std::size_t ReadBuffer::unused() const noexcept
	{
		return mSize - mLimit;
	}

	inline Factory<std::uint8_t> ReadBuffer::allocator() const noexcept
	{
		return mFactory;
	}

	inline void ReadBuffer::reslice(std::size_t position, std::size_t limit) noexcept
	{
		mLimit = std::min(limit, mSize);
		mPosition = std::min(position, mLimit);
	}

	inline ReadBuffer &ReadBuffer::reset() noexcept
	{
		mPosition = 0;
		mLimit = mSize;
		return *this;
	}

	inline const std::uint8_t *ReadBuffer::data() const noexcept
	{
		return mElements;
	}

	inline const std::uint8_t *ReadBuffer::current() const noexcept
	{
		return mElements + mPosition;
	}

	inline std::size_t ReadBuffer::position() const noexcept
	{
		return mPosition;
	}

	inline std::size_t ReadBuffer::position(std::size_t position) noexcept
	{
		mPosition = std::min(mLimit, position);
		return mPosition;
	}

	inline std::size_t ReadBuffer::skip(std::size_t count) noexcept
	{
		std::size_t actual = std::min(mLimit - mPosition, count);
		mPosition += actual;
		return actual;
	}

	inline std::size_t ReadBuffer::limit() const noexcept
	{
		return mLimit;
	}

	inline std::size_t ReadBuffer::limit(std::size_t limit) noexcept
	{
		mLimit = std::min(limit, mSize);
		mPosition = std::min(mPosition, mLimit);
		return mLimit;
	}
	
	inline std::size_t ReadBuffer::capacity() const noexcept
	{
		// memory size limit
		return SIZE_MAX;
	}

	inline std::size_t ReadBuffer::size() const noexcept
	{
		return mSize;
	}
	
	inline bool ReadBuffer::empty() const noexcept
	{
		return mSize == 0;
	}

	inline std::size_t ReadBuffer::requestRead(void *output, std::size_t length) noexcept
	{
		std::size_t actual = 0;
		if (output)
		{
			actual = std::min(length, mLimit - mPosition);
			std::memcpy(output, mElements + mPosition, actual);
			mPosition += actual;
		}
		return actual;
	}
			
	inline void ReadBuffer::read(void *output, std::size_t length)
	{
		if (!output || mLimit - mPosition < length)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(output, mElements + mPosition, length);
		mPosition += length;
	}

	inline std::size_t ReadBuffer::requestReadFromPosition(void *output, std::size_t length, std::size_t position) const noexcept
	{
		std::size_t actual = 0;

		if (output && position <= mLimit)
		{
			actual = std::min(length, mLimit - position);
			std::memcpy(output, mElements + position, actual);
		}

		return actual;
	}

	inline void ReadBuffer::readFromPosition(void *output, std::size_t length, std::size_t position) const
	{
		if (!output || mLimit - position < length)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}

		std::memcpy(output, mElements + position, length);
	}

	inline std::uint8_t ReadBuffer::get()
	{
		if (mPosition >= mLimit)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		return mElements[mPosition++];
	}
						
	template <typename T>
	inline T ReadBuffer::get()
	{
		T value;
		
		if (mLimit - mPosition < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(&value, mElements + mPosition, sizeof(T));
		mPosition += sizeof(T);
		
		return value;
	}
	
	template <typename T>
	inline T ReadBuffer::getBig()
	{
		return Big::order(this->get<T>());
	}
	
	template <typename T>
	inline T ReadBuffer::getLittle()
	{
		return Little::order(this->get<T>());
	}

	template <std::size_t N>
	inline FixedText<N> ReadBuffer::getFixedText()
	{
		FixedText<N> text;
		if (mLimit - mPosition < N)
		{
			throw Exception(Action::Read, Reason::Overflow);
		}
		std::memcpy(text.data(), mElements + mPosition, N);
		mPosition += N;
		return text;
	}

	template <typename T>
	inline T ReadBuffer::getFromPosition(std::size_t position) const
	{
		T value;

		if (mLimit - position < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}

		std::memcpy(&value, mElements + position, sizeof(T));

		return value;
	}

	template <typename T>
	inline T ReadBuffer::getBigFromPosition(std::size_t position) const
	{
		return Big::order(this->getFromPosition<T>(position));
	}

	template <typename T>
	inline T ReadBuffer::getLittleFromPosition(std::size_t position) const
	{
		return Little::order(this->getFromPosition<T>(position));
	}

	template <typename T>
	inline T ReadBuffer::getFromOffset(std::size_t offset) const
	{
		return this->getFromPosition<T>(mPosition + offset);
	}

	template <typename T>
	inline T ReadBuffer::getBigFromOffset(std::size_t offset) const
	{
		return Big::order(this->getFromPosition<T>(mPosition + offset));
	}

	template <typename T>
	inline T ReadBuffer::getLittleFromOffset(std::size_t offset) const
	{
		return Little::order(this->getFromPosition<T>(mPosition + offset));
	}

	template <typename T>
	inline void ReadBuffer::get(T &value)
	{
		if (mLimit - mPosition < sizeof(T))
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(&value, mElements + mPosition, sizeof(T));
		mPosition += sizeof(T);
	}
			
	template <typename T>
	inline void ReadBuffer::getBig(T &value)
	{
		this->get(value);	
		Big::reorder(value);
	}
	
	template <typename T>
	inline void ReadBuffer::getLittle(T &value)
	{
		this->get(value);	
		Little::reorder(value);
	}	
	
	template <typename T>
	inline void ReadBuffer::getArray(T *data, std::size_t count)
	{
		std::size_t bytes = sizeof(T) * count;
		if (mLimit - mPosition < bytes)
		{
			throw Exception(Action::Read, Reason::Underflow);
		}
		
		std::memcpy(data, mElements + mPosition, bytes);
		mPosition += bytes;
	}
			
	template <typename T>
	inline void ReadBuffer::getBigArray(T *data, std::size_t count)
	{
		this->getArray(data, count);	
		Big::reorder(data, count);
	}
	
	template <typename T>
	inline void ReadBuffer::getLittleArray(T *data, std::size_t count)
	{
		this->getArray(data, count);	
		Little::reorder(data, count);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
