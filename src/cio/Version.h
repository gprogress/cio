/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_VERSION_H
#define CIO_VERSION_H

#include "Types.h"

#include "Text.h"
#include "Variant.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Version is a simple structure that represents a particular publication or release version as a Variant.
	 * Typically the version will be a sequence of non-negative integers that can be packed into the Variant's embedded memory.
	 * However, some version descriptions can be complicated text strings.
	 */
	class CIO_API Version
	{
		public:
			/**
			 * Gets a Version that represents the highest possible version number.
			 * This can be used to indicate a maximum version is unbounded.
			 * 
			 * @return the highest possible version
			 */
			static Version max() noexcept;

			/**
			 * Unpacks a 64-bit unsigned integer into the four version fields.
			 * Major version is the highest 16 bits and tweak version is the lowest 16 bits.
			 *
			 * @param value The version value to unpack
			 */
			static Version unpack(std::uint64_t value) noexcept;

			/**
			 * Default constructor.
			 */
			Version() noexcept;

			/**
			 * Constructs a version to use the given major.minor.patch.tweak fields.
			 *
			 * @param major The major version number
			 * @param minor The minor version number
			 * @param patch The patch version number
			 * @param tweak The tweak version number
			 */
			explicit Version(std::uint16_t major, std::uint16_t minor = 0, std::uint16_t patch = 0, std::uint16_t tweak = 0) noexcept;

			/**
			 * Construct a version from text.
			 *
			 * @param text The text
			 */
			Version(const char *text);

			/**
			 * Construct a version from text.
			 *
			 * @param text The text
			 */
			Version(const std::string &text);
			
			/**
			 * Construct a version from text.
			 *
			 * @param text The text
			 */
			Version(const Text &text);

			/**
			 * Copy constructor.
			 * 
			 * @param in The version to copy
			 * @throw std::bad_alloc If the copying failed to allocate needed memory
			 */
			Version(const Version &in);

			/**
			 * Move constructor.
			 * 
			 * @param in The version to move
			 */
			Version(Version &&in) noexcept;

			/**
			 * Copy assignment.
			 * 
			 * @param in The version to copy
			 * @return this version
			 * @throw std::bad_alloc If the copying failed to allocate needed memory
			 */
			Version &operator=(const Version &in);

			/**
			 * Move assignment.
			 * 
			 * @param in The version to move
			 */
			Version &operator=(Version &&in) noexcept;

			/**
			 * Destructor.
			 */
			~Version() noexcept;

			/**
			 * Clears the version.
			 */
			void clear() noexcept;
			
			/**
			 * Gets the major version number.
			 *
			 * @return the major version number
			 */
			std::uint16_t getMajor() const noexcept;

			/**
			 * Sets the major version number.
			 * 
			 * @param major The major version
			 */
			void setMajor(std::uint16_t major) noexcept;
			
			/**
			 * Gets the minor version number.
			 *
			 * @return the minor version number
			 */
			std::uint16_t getMinor() const noexcept;

			/**
			 * Sets the minor version number.
			 *
			 * @param minor The minor version
			 */
			void setMinor(std::uint16_t minor) noexcept;

			/**
			 * Gets the patch version number.
			 *
			 * @return the patch version number
			 */
			std::uint16_t getPatch() const noexcept;

			/**
			 * Sets the patch version number.
			 *
			 * @param patch The minor version
			 */
			void setPatch(std::uint16_t patch) noexcept;

			/**
			 * Gets the tweak version number.
			 *
			 * @return the tweak version number
			 */
			std::uint16_t getTweak() const noexcept;

			/**
			 * Sets the tweak version number.
			 *
			 * @param tweak The tweak version
			 */
			void setTweak(std::uint16_t tweak) noexcept;

			/**
			 * Gets the major and minor version packed into a 32-bit unsigned integer.
			 * The major version is the high 16 bits.
			 * The minor version is the low 16 bits.
			 * 
			 * @return the packed major and minor version
			 */
			std::uint32_t getMajorMinor() const noexcept;

			/**
			 * Sets the major and minor version from a packed 32-bit unsigned integer.
			 * The major version is the high 16 bits.
			 * The minor version is the low 16 bits.
			 *
			 * @param value the packed major and minor version
			 */
			void setMajorMinor(std::uint32_t value);

			/**
			 * Gets the major and minor version packed into a 32-bit unsigned integer.
			 * The major version is packed in bits 32-47.
			 * The minor version is packed in bits 16-31.
			 * The patch version is packed in bits 0-15.
			 *
			 * @return the packed major/minor/patch version
			 */
			std::uint64_t getMajorMinorPatch() const noexcept;

			/**
			 * Sets the major and minor version from a packed 32-bit unsigned integer.
			 * The major version is packed in bits 32-47.
			 * The minor version is packed in bits 16-31.
			 * The patch version is packed in bits 0-15.
			 *
			 * @return the packed major/minor/patch version
			 */
			void setMajorMinorPatch(std::uint64_t value) noexcept;

			/**
			 * Gets the four version fields packed into a 64-bit unsigned integer.
			 * The major version is packed in the highest bits and the tweak version is packed in the lowest bits.
			 * 
			 * @return the packed version number
			 */
			std::uint64_t getValue() const noexcept;

			/**
			 * Sets the four version fields from a packed 64-bit unsigned integer.
			 * The major version is packed in the highest bits and the tweak version is packed in the lowest bits.
			 *
			 * @param value the packed version number to set
			 */
			void setValue(std::uint64_t value);

			/**
			 * Sets the version to use the given major.minor.patch.tweak fields.
			 * 
			 * @param major The major version number
			 * @param minor The minor version number
			 * @param patch The patch version number
			 * @param tweak The tweak version number
			 */
			void set(std::uint16_t major, std::uint16_t minor = 0, std::uint16_t patch = 0, std::uint16_t tweak = 0);

			/**
			 * Gets the version number for the given element index.
			 *
			 * @param idx The element index
			 * @return the version number, or 0 if not present
			 */
			std::uint16_t &operator[](std::size_t idx) noexcept;

			/**
			 * Gets the version number for the given element index.
			 *
			 * @param idx The element index
			 * @return the version number, or 0 if not present
			 */
			const std::uint16_t &operator[](std::size_t idx) const noexcept;

			/**
			 * Sets the version suffix to use the given printed text string.
			 *
			 * @param text The version suffix text to set
			 * @throw std::bad_alloc If the copying failed to allocate needed memory
			 */
			void setSuffix(Text text);

			/**
			 * Gets the version suffix text.
			 * If no suffix was set, this returns the empty string.
			 *
			 * @return the text suffix
			 */
			Text getSuffix() const noexcept;

			/**
			 * Clears the version suffix text.
			 */
			void clearSuffix() noexcept;

			/**
			 * Parses the version from the given text.
			 * 
			 * @param text The text
			 * @return the parse status
			 */
			TextParseStatus parse(Text text);

			/**
			 * Gets the printed length of the version text.
			 * 
			 * @return the printed length
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints the version to a returned UTF-8 buffer.
			 * This includes the version numbers plus the suffix.
			 *
			 * @return the printed text
			 */
			Text print() const;

			/**
			 * Prints the version to the given UTF-8 buffer.
			 * This includes the version numbers plus the suffix.
			 * 
			 * @param buffer The buffer to print to
			 * @param length The number of characters in the print buffer
			 * @return the number of characters actually needed
			 */
			std::size_t print(char *buffer, std::size_t length) const noexcept;

			/**
			 * Prints the version to the given UTF-8 buffer.
			 * This includes the version numbers but omits the suffix.
			 *
			 * @param buffer The buffer to print to
			 * @param length The number of characters in the print buffer
			 * @return the number of characters actually needed
			 */
			std::size_t printWithoutSuffix(char *buffer, std::size_t length) const noexcept;

			/**
			 * Gets the number of version elements present.
			 *
			 * @return the number of version elements
			 */
			std::size_t size() const noexcept;

			/**
			 * Gets whether the version is empty.
			 *
			 * @return whether the version is empty
			 */
			bool empty() const noexcept;
			
			/**
			 * Interprets the version in a Boolean context.
			 * It is true if it is not empty.
			 *
			 * @return whether the version is not empty
			 */
			explicit operator bool() const noexcept;

		private:
			/** Individual fields */
			std::uint16_t mElements[4];

			/** Text suffix */
			Text mSuffix;
	};
	
	/**
	 * Checks to see if two versions are equal.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the versions are equal
	 */
	CIO_API bool operator==(const Version &left, const Version &right) noexcept;
	
	/**
	 * Checks to see if two versions are not equal.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the versions are not equal
	 */
	CIO_API bool operator!=(const Version &left, const Version &right) noexcept;
	
	/**
	 * Checks to see if one version is before another version.
	 * This uses the semantic rules for major, minor, patch, and tweak.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the first version is before the second
	 */
	CIO_API bool operator<(const Version &left, const Version &right) noexcept;
	
	/**
	 * Checks to see if one version is before or equal to another version.
	 * This uses the semantic rules for major, minor, patch, and tweak.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the first version is before the second
	 */
	CIO_API bool operator<=(const Version &left, const Version &right) noexcept;
	
	/**
	 * Checks to see if one version is after another version.
	 * This uses the semantic rules for major, minor, patch, and tweak.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the first version is after the second
	 */
	CIO_API bool operator>(const Version &left, const Version &right) noexcept;
	
	/**
	 * Checks to see if one version is after or equal to another version.
	 * This uses the semantic rules for major, minor, patch, and tweak.
	 *
	 * @param left The first version
	 * @param right The second version
	 * @return whether the first version is after the second
	 */
	CIO_API bool operator>=(const Version &left, const Version &right) noexcept;
	
	/**
	 * Prints a version to a C++ stream.
	 *
	 * @param s The stream
	 * @param version The version
	 * @return the stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, const Version &version);
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
