/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MODE_H
#define CIO_MODE_H

#include "Types.h"

#include <cstdint>
#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * The Mode enumeration describes the types of states that affect various kinds of resources, inputs, and outputs.
	 * It includes modes for whether a resource is readable or writable, whether is can be loaded from existing content,
	 * created from scratch, or replace existing content, and some specialized modes for temporary (delete after use) status
	 * and for how particular input and output operations perform.
	 */
	enum class Mode : std::uint8_t
	{
		/** Require the resource to be readable after open */
		Read,

		/** Require the resource to be writable after open */
		Write,

		/** Require the resource to be an executable program */
		Execute,

		/** Load the resource if it already exists */
		Load,

		/** Create the resource if it is missing */
		Create,

		/** Replace the resource if it already exists */
		Replace,

		/** Request the resource to be temporary and (if possible) deleted on close */
		Temporary,

		/** Request that reads and writes go directly to a resource without intermediate caching */
		Unbuffered,

		/** Require writes to the resource to be fully committed synchronously */
		Synchronous,

		/** Request that reads and writes do not block the calling thread if they cannot immediately complete */
		Nonblocking,

		/** Sentinel for an invalid resource mode due to bad parse or other issues */
		Invalid
	};

	/**
	 * Gets the human readable text for the Mode enumeration.
	 *
	 * @param mode The mode
	 * @return the text
	 */
	CIO_API const char *print(Mode mode);

	/**
	 * Parses the mode enumeration from a text string.
	 * If the text is unrecognized, the Invalid mode is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed mode, or Invalid if not recognized
	 */
	CIO_API Mode parseMode(const char *text);

	/**
	 * Parses the mode enumeration from a text string.
	 * If the text is unrecognized, the Invalid mode is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed mode, or Invalid if not recognized
	 */
	CIO_API Mode parseMode(const std::string &text);

	/**
	 * Prints the Mode enumeration to a C++ stream.
	 *
	 * @param stream The stream to print to
	 * @param mode The mode to print
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, Mode mode);

	/**
	 * Parses the Mode enumeration from text in a C++ stream.
	 *
	 * @param stream The stream to parse from
	 * @param mode The mode to update with parse results
	 * @return the stream
	 */
	CIO_API std::istream &operator>>(std::istream &stream, Mode &mode);
}

#endif
