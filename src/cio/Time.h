/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TIME_H
#define CIO_TIME_H

#include "Types.h"

#include <chrono>
#include <ctime>
#include <iosfwd>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Time class provides a representation of a particular real-world calendar time.
	 * It can be printed or converted to conventional dates and times, but the current time is dependent
	 * on the system clock and time zone which may be changed by the user.
	 */
	class CIO_API Time
	{
		public:
			/**
			 * Gets the calendar time representing the end of time.
			 * 
			 * @return The end of time
			 */
			inline static Time max() noexcept;

			/**
			 * Gets the current real world time.
			 * 
			 * @return The current real world time
			 */
			inline static Time now() noexcept;

			/** 
			 * The calendar time value implementing the time 
			 */
			std::chrono::time_point<std::chrono::system_clock> value;

			/**
			 * Construct a calendar time representing the beginning of time.
			 */
			inline Time() noexcept;

			/**
			 * Construct a calendar time using its given C++ representation.
			 * 
			 * @param value The calendar time value
			 */
			inline Time(std::chrono::time_point<std::chrono::system_clock> value) noexcept;

			/**
			 * Calculates how much time has elapsed in milliseconds since the given epoch time.
			 * 
			 * @param epoch The epoch time
			 * @return The amount of time that elapsed between the epoch and this time
			 */
			inline std::chrono::milliseconds elapsed(const Time &epoch) const noexcept;

			/**
			 * Resets this calendar time to the beginning of time.
			 */
			inline void clear() noexcept;

			/**
			 * Adds a time duration to this calendar time to get a scheduled future calendar time.
			 * If the given offset is not positive, this instead returns the end of time to indicate
			 * the time will never be scheduled.
			 * 
			 * @tparam <R> The representation type of the duration offset
			 * @tparam <P> The period ratio type of the duration offset
			 * @param offset The interval to schedule the next calendar time
			 * @return The next scheduled calendar time, or the end of time if the offset was not valid
			 */
			template <typename R, typename P>
			inline Time schedule(std::chrono::duration<R, P> offset) const noexcept;

			/**
			 * Sets the current calendar time from the given year, month, and day in UTC.
			 * The time portion will be set to midnight.
			 * 
			 * @param year The year relative to the common era (negative for BC)
			 * @param month The month from 1 to 12
			 * @param day The day of month from 1 to 31
			 * @return Whether the time was valid
			 */
			bool setDate(int year, unsigned month = 1, unsigned day = 1) noexcept;

			/**
			 * Sets the current calendar time from the given year, month, day, hours, minutes, and seconds in UTC.
			 *
			 * @param year The year relative to the common era (negative for BC)
			 * @param month The month from 1 to 12
			 * @param day The day of month from 1 to 31
			 * @return Whether the time was valid
			 */
			bool setDate(int year, unsigned month, unsigned day, unsigned hours, unsigned minutes = 0, unsigned seconds = 0) noexcept;

			/**
			 * Sets the current calendar time from the given C date time structure in UTC.
			 * 
			 * @param date The C date time structure
			 * @return Whether the time was avlid
			 */
			bool setDate(std::tm date) noexcept;
			
			/**
			 * Gets the C date time structure in UTC from the current calendar time.
			 * 
			 * @return The C date time structure
			 */
			std::tm getDate() const noexcept;

			/**
			 * Gets the number of UTF-8 characters needed to print the time.
			 * Currently this is always 19 and cannot accommodate dates outside of the POSIX range.
			 * 
			 * @return The number of UTF-8 characters needed to print the time
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints a calendar time to a UTF-8 text buffer.
			 * This prints the time in a locale-independent way using UTC formatted as "YYYY-MM-DD HH:MM:SS".
			 * 
			 * @warning Due to gaps in the underlying C implementation, this will not correctly print dates prior to 1970.
			 *
			 * @param buffer The text buffer
			 * @param length The size of the text buffer
			 * @return The actual number of text bytes needed to print the time
			 */
			std::size_t print(char *buffer, std::size_t length) const noexcept;

			/**
			 * Prints a calendar time to a returned UTF-8 text buffer.
			 * This prints the time in a locale-independent way using UTC formatted as "YYYY-MM-DD HH:MM:SS".
			 * 
			 * @warning Due to gaps in the underlying C implementation, this will not correctly print dates prior to 1970.
			 *
			 * @return The printed text
			 */
			cio::FixedText<19> print() const noexcept;
			
			/**
			 * Gets the standard POSIX time for the current calendar time.
			 *
			 * @return The standard POSIX time
			 */
			std::time_t getPosixTime() const noexcept;

			/**
			 * Sets the current calendar time to the given standard POSIX time.
			 *
			 * @param incoming The standard POSIX time to use
			 */
			void setPosixTime(std::time_t value) noexcept;

			/**
			 * Interprets the calendar time in a Boolean context.
			 * This is true if it is not the beginning of time.
			 * 
			 * @return The calendar time interpreted as a Boolean
			 */
			inline explicit operator bool() const noexcept;
	};

	/**
	 * Calculates the duration between two calendar times.
	 * 
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return The duration between the two times in milliseconds
	 */
	inline std::chrono::milliseconds operator-(const Time &left, const Time &right) noexcept;

	/**
	 * Adds a duration offset to a calendar time.
	 * 
	 * @tparam <R> The representation type of the duration offset
	 * @tparam <P> The period ratio type of the duration offset
	 * @param left The calendar time
	 * @param offset The duration to add
	 * @return The calendar time after the offset is added
	 */
	template <typename R, typename P>
	inline Time &operator+=(Time &left, std::chrono::duration<R, P> offset) noexcept;

	/**
	 * Adds a duration offset to a calendar time to get a new calendar time.
	 *
	 * @tparam <R> The representation type of the duration offset
	 * @tparam <P> The period ratio type of the duration offset
	 * @param left The calendar time
	 * @param offset The duration to add
	 * @return The calendar time with the offset added
	 */
	template <typename R, typename P>
	inline Time operator+(const Time &left, std::chrono::duration<R, P> offset) noexcept;

	/**
	 * Checks to see if two calendar times are equal.
	 * 
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the calendar times are equal
	 */
	inline bool operator==(const Time &left, const Time &right) noexcept;

	/**
	 * Checks to see if two calendar times are not equal.
	 *
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the calendar times are not equal
	 */
	inline bool operator!=(const Time &left, const Time &right) noexcept;
	
	/**
	 * Checks to see if a calendar time is before another calendar time.
	 * 
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the first time is before the second
	 */
	inline bool operator<(const Time &left, const Time &right) noexcept;

	/**
	 * Checks to see if a calendar time is not after another calendar time.
	 *
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the first time is not after the second
	 */
	inline bool operator<=(const Time &left, const Time &right) noexcept;

	/**
	 * Checks to see if a calendar time is after another calendar time.
	 *
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the first time is after the second
	 */
	inline bool operator>(const Time &left, const Time &right) noexcept;

	/**
	 * Checks to see if a calendar time is not before another calendar time.
	 *
	 * @param left The first calendar time
	 * @param right The second calendar time
	 * @return Whether the first time is not before the second
	 */
	inline bool operator>=(const Time &left, const Time &right) noexcept;

	/**
	 * Prints a calendar time to a C++ stream.
	 * This prints the time in a locale-independent way using UTC formatted as "YYYY-MM-DD HH:MM:SS".
	 * 
	 * @warning Due to gaps in the underlying C implementation, this will not correctly print dates prior to 1970.
	 * 
	 * @param stream The stream
	 * @param time The calendar time
	 * @return The stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Time &time);
}

/* Inline implementation */

namespace cio
{
	inline Time Time::max() noexcept
	{
		return std::chrono::time_point<std::chrono::system_clock>::max();
	}

	inline Time Time::now() noexcept
	{
		return std::chrono::system_clock::now();
	}

	inline Time::Time() noexcept :
		value()
	{
		// nothing more to do
	}

	inline Time::Time(std::chrono::time_point<std::chrono::system_clock> value) noexcept :
		value(value)
	{
		// nothing more to do
	}

	inline std::chrono::milliseconds Time::elapsed(const Time &epoch) const noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(this->value - epoch.value);
	}

	inline void Time::clear() noexcept
	{
		this->value = std::chrono::time_point<std::chrono::system_clock>();
	}

	template <typename R, typename P>
	inline Time Time::schedule(std::chrono::duration<R, P> offset) const noexcept
	{
		return offset.count() > 0 ? (this->value + offset) : std::chrono::time_point<std::chrono::system_clock>::max();
	}

	inline Time::operator bool() const noexcept
	{
		return this->value != std::chrono::time_point<std::chrono::system_clock>();
	}

	template <typename R, typename P>
	inline Time &operator+=(Time &left, std::chrono::duration<R, P> offset) noexcept
	{
		left.value += offset;
		return left;
	}

	template <typename R, typename P>
	inline Time operator+(const Time &left, std::chrono::duration<R, P> offset) noexcept
	{
		return left.value + offset;
	}

	inline std::chrono::milliseconds operator-(const Time &left, const Time &right) noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(left.value - right.value);
	}

	inline bool operator==(const Time &left, const Time &right) noexcept
	{
		return left.value == right.value;
	}

	inline bool operator!=(const Time &left, const Time &right) noexcept
	{
		return left.value != right.value;
	}

	inline bool operator<(const Time &left, const Time &right) noexcept
	{
		return left.value < right.value;
	}

	inline bool operator<=(const Time &left, const Time &right) noexcept
	{
		return left.value <= right.value;
	}

	inline bool operator>(const Time &left, const Time &right) noexcept
	{
		return left.value > right.value;
	}

	inline bool operator>=(const Time &left, const Time &right) noexcept
	{
		return left.value >= right.value;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif