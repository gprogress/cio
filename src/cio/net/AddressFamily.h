/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_ADDRESSFAMILY_H
#define CIO_NET_ADDRESSFAMILY_H

#include "Types.h"

#include <string>

#include <cio/Text.h>
#include <cio/Validation.h>

namespace cio
{
	namespace net
	{
		/**
		 * The CIO Net Address Family provides an abstraction of the native Address Family (AF) identifers
		 * used by the native socket library API. The native Address Families are typically integer codes
		 * from a set of predefined constants. However, which constants are available and what their values are vary by platform.
		 * This class provides baseline ability to always get, set, print, and parse the most common Address Familys (currently INET4 and INET6).
		 * Additional native Address Families may be valid and passed through as raw integers.
		 */
		class CIO_NET_API AddressFamily
		{
			public:
				/**
				 * Gets the Address Family for the TCP/IP protocol version 4 (IPv4 aka AF_INET).
				 *
				 * @return the IPv4 Address Family
				 */
				static AddressFamily ipv4() noexcept;

				/**
				 * Gets the Address Family for the TCP/IP protocol version 6 (IPv6 aka AF_INET6).
				 *
				 * @return the IPv6 Address Family
				 */
				static AddressFamily ipv6() noexcept;

				/**
				 * Construct an Address Family without setting the constant.
				 */
				inline AddressFamily() noexcept;

				/**
				 * Construct an Address Family with the given native OS constant.
				 *
				 * @param code The native OS constant
				 */
				inline explicit AddressFamily(int code) noexcept;

				/**
				* Sets the given native OS constant.
				*
				* @param code The native OS constant
				*/
				inline void set(int code) noexcept;

				/**
				 * Parses the given null-terminated text to see if a built-in Address Family is recognized.
				 * Currently this only recognizes "IPv4" and "IPv6".
				 * Note that these strings are not defined by the underlying operating system.
				 *
				 * @param text The text to parse
				 * @return the validation results of parsing
				 */
				Validation parse(const char *text) noexcept;

				/**
				 * Parses the given null-terminated text to see if a built-in Address Family is recognized.
				 * Currently this only recognizes "IPv4" and "IPv6".
				 * Note that these strings are not defined by the underlying operating system.
				 *
				 * @param text The text to parse
				 * @param len The number of bytes in the text
				 * @return the validation results of parsing
				 */
				Validation parse(const char *text, std::size_t len) noexcept;

				/**
				 * Parses the given null-terminated text to see if a built-in Address Family is recognized.
				 * Currently this only recognizes "IPv4" and "IPv6".
				 * Note that these strings are not defined by the underlying operating system.
				 *
				 * @param text The text to parse
				 * @return the validation results of parsing
				 */
				Validation parse(const std::string &text) noexcept;

				/**
				 * Gets the native OS constant for the Address Family.
				 *
				 * @return the native OS constant value
				 */
				inline int get() const noexcept;

				/**
				 * Prints the Address Family to a text buffer.
				 * If the buffer is not large enough, the output will be truncated but the returned count will specify how many were needed.
				 * If the buffer is larger than needed, the text will be null-terminated.
				 *
				 * @param text The text buffer to print to
				 * @param len The number of bytes in the buffer
				 * @return the number of bytes actually needed to print this protocol type
				 */
				std::size_t print(char *text, std::size_t len) const noexcept;

				/**
				 * Prints the Address Family to a string and returns it.
				 *
				 * @return the printed text
				 * @throw std::bad_alloc If not enough memory could be allocated for the string
				 */
				std::string print() const;

				/**
				 * Clears the Address Family and sets it to the invalid value.
				 */
				inline void clear() noexcept;

				/**
				 * Checks whether the Address Family has been set when used in a Boolean context.
				 *
				 * @return whether the Address Family has been set
				 */
				inline explicit operator bool() const noexcept;

			private:
				/** The native OS constant for the Address Family */
				int mCode;
		};

		/**
		 * Prints the Address Family to a C++ stream.
		 *
		 * @param stream The stream to print to
		 * @param type The Address Family
		 * @return the stream
		 */
		CIO_NET_API std::ostream &operator<<(std::ostream &stream, const AddressFamily &type);
	}
}

/* Inline implementation */

namespace cio
{
	namespace net
	{
		inline AddressFamily::AddressFamily() noexcept :
			mCode(0)
		{
			// nothing more to do
		}

		inline AddressFamily::AddressFamily(int code) noexcept :
			mCode(code)
		{
			// nothing more to do
		}

		inline int AddressFamily::get() const noexcept
		{
			return mCode;
		}

		inline void AddressFamily::set(int code) noexcept
		{
			mCode = code;
		}

		inline void AddressFamily::clear() noexcept
		{
			mCode = 0;
		}

		inline AddressFamily::operator bool() const noexcept
		{
			return mCode != 0;
		}
	}
}

#endif

