/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_PARSER_H
#define CIO_XML_PARSER_H

#include "Types.h"

#include <cio/Path.h>
#include <cio/Text.h>

#include <memory>

namespace cio
{
	namespace xml
	{
		/**
		 * The XML Parser provides both stream and tree based XML parsing.
		 */
		class CIO_XML_API Parser
		{
			public:
				/**
				 * Construct the XML parser.
				 */
				Parser();

				/** Destructor. */
				~Parser();

				void parseText(Text text, Handler &handler);

				void parseFile(const cio::Path &path, Handler &handler, std::size_t bufferSize = 4096);

				void parse(Input &stream, Handler &handler, std::size_t bufferSize = 4096);

				Node buildTreeFromText(Text text);

				Node buildTreeFromFile(const cio::Path &path, std::size_t bufferSize = 4096);

				Node buildTree(Input &stream, std::size_t bufferSize = 4096);

			private:
				static void startElement(void *data, const char *el, const char **attr);
				static void endElement(void *data, const char *el);

				static void processText(void *data, const char *text, int len);

				static void processComment(void *data, const char *text);
		};
	}
}

#endif
