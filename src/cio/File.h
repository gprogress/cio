/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_FILE_H
#define CIO_FILE_H

#include "Types.h"

#include "Device.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The File class provides the lowest level access to the native file read/write API.
	* In most cases this is just using functionality provided by Channel base class.
	*/
	class CIO_API File : public Device
	{
		public:
			/**
			* Gets the metaclass for the File class.
			*
			* @return the metaclass for this class
			*/
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct an instance not connected to any file.
			*/
			File() noexcept;

			/**
			* Construct an instance and opens the file.
			*
			* @param path The path to open
			*/
			explicit File(const Path &path);

			/**
			* Construct an instance and opens the device.
			*
			* @param path The path to open
			* @param modes The file modes to use to open the file
			*/
			File(const Path &path, ModeSet modes);
				
			/**
			 * Construct a file with the given native device handle.
			 *
			 * @param handle The handle
			 */
			explicit File(std::uintptr_t handle) noexcept;

			/**
			* Construct an instance and opens the device.
			*
			* @param path The path to open
			*/
			explicit File(const char *path);

			/**
			* Construct an instance and opens the device.
			*
			* @param path The path to open
			*/
			explicit File(const std::string &path);

			/**
			* Construct an instance and opens the device.
			*
			* @param path The path to open
			* @param modes The file modes to use to open the file
			*/
			File(const char *path, ModeSet modes);

			/**
			* Construct an instance and opens the device.
			*
			* @param path The path to open
			* @param modes The file modes to use to open the file
			*/
			File(const std::string &path, ModeSet modes);

			/**
			* Move a device connection into a new instance of this class.
			*
			* @param in The device to move
			*/
			File(File &&in) noexcept;

			/**
			* Move a device connection into this object.
			* The existing device connection is closed.
			*
			* @param in The device to move
			* @return this device
			*/
			File &operator=(File &&in) noexcept;

			/**
			* Close and tear down a device instance.
			*/
			virtual ~File() noexcept override;

			/**
			* Gets the metaclass for the runtime type of this object.
			*
			* @return the metaclass
			*/
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Opens the file specified by the given local filesystem resource using the given modes.
			 * 
			 * @param resource The path to the file
			 * @param modes The modes to use to open the file
			 * @param factory The protocol factory, which is not used
			 * @return the actual modes of the file as opened
			 */
			virtual ModeSet openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory) override;
				
		private:
			/** The metaclass for this class */
			static Class<File> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

