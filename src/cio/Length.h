/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LENGTH_H
#define CIO_LENGTH_H

#include "Types.h"

#include <algorithm>
#include <iosfwd>

namespace cio
{
	class CIO_API Length
	{
		public:
			/**
			 * The length value in bytes. 
			 * INT64_MAX indicates unknown forward length.
			 * INT64_MIN indicates unknown backward length.
			 */
			std::int64_t value;

			/**
			 * Construct a length initialized to 0 bytes.
			 */
			inline Length() noexcept;

			/**
			 * Construct a length from the given signed value.
			 *
			 * @param value The signed value
			 */
			inline Length(int value) noexcept;

			/**
			 * Construct a length from the given unsigned value.
			 *
			 * @param value The unsigned value
			 */
			inline Length(unsigned value) noexcept;
			
			/**
			 * Construct a length from the given signed value.
			 *
			 * @param value The signed value
			 */
			inline Length(long value) noexcept;

			/**
			 * Construct a length from the given unsigned value.
			 *
			 * @param value The unsigned value
			 */
			inline Length(unsigned long value) noexcept;
			
			/**
			 * Construct a length from the given signed value.
			 * 
			 * @param value The signed value
			 */
			inline Length(long long value) noexcept;

			/**
			 * Construct a length from the given unsigned value.
			 * If the unsigned value is larger than INT64_MAX, it is clamped to INT64_MAX.
			 *
			 * @param value The unsigned value
			 */
			inline Length(unsigned long long value) noexcept;

			/**
			 * Resets the length to 0.
			 */
			inline void clear() noexcept;

			/**
			 * Destructor.
			 */
			inline ~Length() noexcept = default;

			/**
			 * Sets the length to the largest possible value to indicate unbounded length.
			 */
			inline void unbound() noexcept;

			/**
			 * Checks whether the current length indicates a known value.
			 * 
			 * @return whether the current length is known
			 */
			inline bool known() const noexcept;

			/**
			 * Checks whether the current length indicates a forward position.
			 * This is true if it is zero or positive.
			 * 
			 * @return whether the length is a forward position
			 */
			inline bool forward() const noexcept;

			/**
			 * Checks whether the current length indicates a backward position.
			 * This is true if it is zero or negative.
			 *
			 * @return whether the length is a negative position
			 */
			inline bool backward() const noexcept;

			/**
			 * Print a length to a buffer.
			 * The length is printed to three significant digits with an appropriate decimal unit suffix
			 *
			 * @param buffer The buffer to print to
			 * @param length the length of the buffer
			 * @return the needed length of the string, which is generally 6 (three digits, a space, and two chars for unit)
			 */
			std::size_t print(char *buffer, std::size_t length) const noexcept;

			/**
			 * Adds to this length.
			 * 
			 * @param right The amount to add
			 * @return this length
			 */
			inline Length &operator+=(const Length &right) noexcept;

			/**
			 * Subtracts from this length.
			 *
			 * @param right The amount to add
			 * @return this length
			 */
			inline Length &operator-=(const Length &right) noexcept;
			
			/**
			 * Checks whether the current value is representable as std::size_t.
			 * This is true if it is non-negative and less tha SIZE_MAX (which is reserved to indicate unknown size).
			 *
			 * @return whether this length can be expressed in std::size_t
			 */
			inline bool sizeable() const noexcept;
			
			/**
			 * Gets the value as a memory size, clamped to range [0, SIZE_MAX].
			 *
			 * @return the size value
			 */
			inline std::size_t size() const noexcept;
			
			/**
			 * Gets the value as a file offset size.
			 * For now we assume this is equivalent to std::int64_t since that is
			 * currently how it is defined on most common Unix platforms.
			 *
			 * @return the size value
			 */
			inline std::int64_t offset() const noexcept;

			/**
			 * Casts the length to a memory size.
			 * If the length is too large, SIZE_MAX is returned.
			 * If the length is negative, 0 is returned.
			 * 
			 * @return the memory size
			 */
			inline explicit operator std::size_t() const noexcept;

			/**
			 * Interprets the length as a Boolean.
			 * It is true if it is nonzero.
			 * 
			 * @return whether the length is nonzero
			 */
			inline explicit operator bool() const noexcept;
	};

	/**
	 * Adds two lengths to get a resulting length.
	 * 
	 * @param left The first length
	 * @param right The second length
	 * @return the length sum
	 */
	inline Length operator+(Length left, Length right) noexcept;

	/**
	 * Subtracts two lengths to get the distance between them.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return the length difference
	 */
	inline Length operator-(Length left, Length right) noexcept;

	/**
	 * Checks to see if two Lengths are equal.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return whether the lengths are equal
	 */
	inline bool operator==(const Length &left, const Length &right) noexcept;

	/**
	 * Checks to see if two Lengths are not equal.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return whether the lengths are not equal
	 */
	inline bool operator!=(const Length &left, const Length &right) noexcept;

	/**
	 * Checks to see if one length is less than another.
	 * 
	 * @param left The first length
	 * @param right The second length
	 * @return whether the first length is less than the second length
	 */
	inline bool operator<(const Length &left, const Length &right) noexcept;

	/**
	 * Checks to see if one length is less than or equal to another.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return whether the first length is less than or equal to the second length
	 */
	inline bool operator<=(const Length &left, const Length &right) noexcept;

	/**
	 * Checks to see if one length is greater than another.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return whether the first length is greater than the second length
	 */
	inline bool operator>(const Length &left, const Length &right) noexcept;

	/**
	 * Checks to see if one length is greater than or equal to another.
	 *
	 * @param left The first length
	 * @param right The second length
	 * @return whether the first length is greater than or equal to the second length
	 */
	inline bool operator>=(const Length &left, const Length &right) noexcept;

	/**
	 * Print a length to a C++ stream.
	 * The length is printed to three significant digits with an appropriate decimal unit suffix.
	 * If the length matches an unknown value, "Unknown" is printed instead. 
	 * 
	 * @param stream the stream
	 * @param length The length
	 * @return the stream after printing
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Length &length);
}

/* Inline implementation */

namespace cio
{
	inline Length::Length() noexcept :
		value(0)
	{
		// nothing to do
	}

	inline Length::Length(int value) noexcept :
		value(value)
	{
		// nothign to do
	}

	inline Length::Length(unsigned value) noexcept :
		value(value)
	{
		// nothign to do
	}
	
	inline Length::Length(long value) noexcept :
		value(static_cast<std::int64_t>(value))
	{
		// nothing to do
	}

	inline Length::Length(unsigned long value) noexcept :
#if ULONG_SIZE >= 8
		value(value < static_cast<unsigned long long>(INT64_MAX) ? static_cast<std::int64_t>(value) : INT64_MAX)
#else
		value(static_cast<std::int64_t>(value))
#endif
	{
		// nothing to do
	}
	
	inline Length::Length(long long value) noexcept :
		value(static_cast<std::int64_t>(value))
	{
		// nothing to do
	}

	inline Length::Length(unsigned long long value) noexcept :
		value(value < static_cast<unsigned long long>(INT64_MAX) ? static_cast<std::int64_t>(value) : INT64_MAX)
	{
		// nothing to do
	}

	inline void Length::clear() noexcept
	{
		this->value = 0;
	}

	inline void Length::unbound() noexcept
	{
		this->value = INT64_MAX;
	}

	inline bool Length::known() const noexcept
	{
		return std::abs(this->value) != INT64_MAX;
	}

	inline bool Length::forward() const noexcept
	{
		return this->value >= 0;
	}

	inline bool Length::backward() const noexcept
	{
		return this->value <= 0;
	}

	inline Length &Length::operator+=(const Length &right) noexcept
	{
		this->value += right.value;
		return *this;
	}

	inline Length &Length::operator-=(const Length &right) noexcept
	{
		this->value -= right.value;
		return *this;
	}

	inline bool Length::sizeable() const noexcept
	{
		return this->value >= 0 && static_cast<std::uint64_t>(this->value) < SIZE_MAX;
	}

	inline std::size_t Length::size() const noexcept
	{
		return (this->value < 0) ? 0 : static_cast<std::size_t>(std::min(static_cast<std::uint64_t>(this->value), static_cast<std::uint64_t>(SIZE_MAX)));
	}
	
	inline std::int64_t Length::offset() const noexcept
	{
		return this->value;
	}

	inline Length::operator std::size_t() const noexcept
	{
		return this->value > 0 ? (this->value < SIZE_MAX ? static_cast<std::size_t>(this->value) : SIZE_MAX) : 0;
	}

	inline Length::operator bool() const noexcept
	{
		return this->value != 0;
	}

	inline Length operator+(Length left, Length right) noexcept
	{
		return Length(left.value + right.value);
	}

	inline Length operator-(Length left, Length right) noexcept
	{
		return Length(left.value - right.value);
	}

	inline bool operator==(const Length &left, const Length &right) noexcept
	{
		return left.value == right.value;
	}

	inline bool operator!=(const Length &left, const Length &right) noexcept
	{
		return left.value != right.value;
	}

	inline bool operator<(const Length &left, const Length &right) noexcept
	{
		return left.value < right.value;
	}

	inline bool operator<=(const Length &left, const Length &right) noexcept
	{
		return left.value <= right.value;
	}

	inline bool operator>(const Length &left, const Length &right) noexcept
	{
		return left.value > right.value;
	}

	inline bool operator>=(const Length &left, const Length &right) noexcept
	{
		return left.value >= right.value;
	}
}

#endif
