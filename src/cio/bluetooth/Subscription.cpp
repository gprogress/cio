/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Subscription.h"

#include "Property.h"

namespace cio
{
	namespace bluetooth
	{
		Subscription::Subscription() noexcept :
			mValue(0),
			mCurrentMode(Property::Read)
		{
			// nothing more to do
		}
		
		Subscription::Subscription(std::uint16_t handle) noexcept :
			mValue(handle),
			mCurrentMode(Property::Read)
		{
			// nothing more to do
		}
					
		Subscription::Subscription(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval) noexcept :
			mValue(handle),
			mListener(std::move(listener)),
			mPollInterval(interval),
			mCurrentMode(Property::Read)
		{
			// nothing more to do
		}
						
		Subscription::Subscription(const Subscription &in) = default;
				
		Subscription::Subscription(Subscription &&in) noexcept :
			mValue(in.mValue),
			mListener(std::move(in.mListener)),
			mPollInterval(in.mPollInterval),
			mNotified(in.mNotified),
			mModes(in.mModes),
			mCurrentMode(in.mCurrentMode)
		{
			// nothing more to do
		}
				
		Subscription &Subscription::operator=(const Subscription &in) = default;
				
		Subscription &Subscription::operator=(Subscription &&in) noexcept
		{
			mValue = in.mValue;
			mListener = std::move(in.mListener);
			mPollInterval = in.mPollInterval;
			mNotified = in.mNotified;
			mModes = in.mModes;
			mCurrentMode = in.mCurrentMode;
			return *this;
		}
				
		Subscription::~Subscription() noexcept = default;
				
		void Subscription::clear() noexcept
		{
			mValue = 0;
			mListener = AttributeCallback();
			mPollInterval = std::chrono::milliseconds();
			mNotified = std::chrono::time_point<std::chrono::steady_clock>();
			mModes.clear();
			mCurrentMode = Property::Read;
		}

		void Subscription::subscribe(std::uint16_t value)
		{
			mValue = value;
		}
				
		std::uint16_t Subscription::getHandle() const noexcept
		{
			return mValue;
		}
				
		void Subscription::setModes(Properties modes) noexcept
		{
			mModes = modes;
		}
				
		Properties Subscription::getModes() const noexcept
		{
			return mModes;
		}
				
		Property Subscription::getCurrentMode() const noexcept
		{
			return mCurrentMode;
		}
				
		void Subscription::setCurrentMode(Property mode) noexcept
		{
			mCurrentMode = mode;
		}
				
		void Subscription::unsubscribe() noexcept
		{
			mValue = 0;
		}
				
		std::chrono::milliseconds Subscription::getPollInterval() const noexcept
		{
			return mPollInterval;
		}
				
		void Subscription::setPollInterval(std::chrono::milliseconds interval) noexcept
		{
			mPollInterval = interval;
		}
				
		const AttributeCallback &Subscription::getListener() const noexcept
		{
			return mListener;
		}
				
		void Subscription::setListener(AttributeCallback &&listener) noexcept
		{
			mListener = std::move(listener);
		}
				
		void Subscription::setListener(const AttributeCallback &listener)
		{
			mListener = listener;
		}

		std::chrono::time_point<std::chrono::steady_clock> Subscription::notified() const noexcept
		{
			return mNotified;
		}
				
		std::chrono::time_point<std::chrono::steady_clock> Subscription::scheduled() const noexcept
		{
			return mNotified + mPollInterval;
		}
				
		std::chrono::milliseconds Subscription::remaining() const noexcept
		{
			std::chrono::milliseconds left;
			std::chrono::time_point<std::chrono::steady_clock> current = std::chrono::steady_clock::now();
			std::chrono::milliseconds elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(current - mNotified);
			if (elapsed < mPollInterval)
			{
				left = mPollInterval - elapsed;
			}
			return left;
		}
		
		State Subscription::notify(const void *buffer, std::size_t length)
		{
			State status;
		
			if (mListener)
			{
				mListener(mValue, buffer, length);
				status.succeed();
			}
			else
			{
				status.complete();
			}
			
			return status;
		}
				
		Subscription::operator bool() const noexcept
		{
			return mValue != 0;
		}
	}
}
