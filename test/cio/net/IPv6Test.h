/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_IPV6TEST_H
#define CIO_NET_IPV6TEST_H

#include <cio/test/Suite.h>
#include <cio/net/IPv6.h>

namespace cio
{
	namespace net
	{
		class IPv6Test : public cio::test::Suite
		{
			public:
				IPv6Test();

				virtual ~IPv6Test() noexcept override;

				void parseTest();

				void printTest();

				void setTest();

				void clearTest();
		};
	}
}

#endif

