/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_UNALIGNED_H
#define CAQ_UNALIGNED_H

#include "Types.h"

#include "Accessor.h"
#include "Aligned.h"
#include "Native.h"
#include "Reader.h"

namespace caq
{
	/**
	 * The Unaligned accessor decodes native data elements packed into an unaligned bitstream, possibly into fewer or more bits than the actual data type.
	 * 
	 * @tparam T The logical component type
	 * @tparam C The number of components
	 */
	template <typename T, std::size_t C>
	class Unaligned : public Reader<T>
	{
		public:
			/**
			 * Destructor.
			 */
			inline virtual ~Unaligned() noexcept override = default;

			// Override Abstract accessor methods

			/**
			 * Gets the number of array elements in this accessor.
			 * 
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return mSize;
			}

			/**
			 * Gets the fixed number of element components in this accessor.
			 * 
			 * @return The number of components
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return C;
			}

			/**
			 * Interprets whether this accessor is usable.
			 * The Aligned accessor returns true if the data pointer is set and the size is nonzero.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mData != nullptr && mSize > 0;
			}

			// Override Reader<T> accessor methods

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The Aligned accessor can be optimized into the Native accessor if the components are contiguous.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				Accessor<T> acc;
				if (this->aligned())
				{
					if (this->contiguous())
					{
						acc.copy(Native<T, T, C>(reinterpret_cast<T *>(mData + mOffsets[0] / 8u), mSize));
					}
					else
					{
						Aligned<T, T, C> *aligned = acc.copy(Aligned<T, T, C>());
						for (std::size_t i = 0; i < C; ++i)
						{
							aligned->setup(i, mOffsets[i] / 8u, mStrides[i] / 8u);
						}
						aligned->bind(reinterpret_cast<T *>(mData), mSize);
					}
				}
				else
				{
					acc.copy(*this);
				}
				return acc;
			}

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const noexcept override
			{
				std::remove_cv_t<T> answer = std::remove_cv_t<T>();
				const std::size_t width = mWidths[c];
				std::size_t start = mOffsets[c] + mStrides[c] * n;
				std::size_t end = start + width;

				std::size_t sb = start / 8u;
				std::size_t eb = end / 8u;

				std::size_t leading = start % 8u;
				std::size_t trailing = end % 8u;

				if (sb == eb)
				{
					answer = (mData[sb] >> leading) & ((1u << trailing) - 1u);
				}
				else
				{
					answer = (mData[sb]) >> leading;

					std::size_t shift = (8u - leading);
					for (std::size_t cb = sb + 1; cb < eb; ++cb)
					{
						answer |= (std::remove_cv_t<T>(mData[cb]) << shift);
						shift += 8;
					}

					if (trailing)
					{
						answer |= ((std::remove_cv_t<T>(mData[eb]) & ((1u << trailing) - 1u)) << shift);
					}
				}

				return answer;
			}


			// Methods unique to Aligned<T, C>

			/**
			 * Sets the offset and stride for a given component.
			 * 
			 * @param c The component index
			 * @param offset The initial element offset in units of T
			 * @param stride The number of elements between adjacent array index values of this component
			 */
			inline void setup(std::size_t c, std::size_t width, std::size_t offset, std::ptrdiff_t stride) noexcept
			{
				mWidths[c] = width;
				mOffsets[c] = offset;
				mStrides[c] = stride;
			}

			/**
			 * Sets the data array and array index count.
			 * 
			 * @param data The data array
			 * @param size The number of array elements
			 */
			inline void bind(const void *data, std::size_t size) noexcept
			{
				mData = reinterpret_cast<const unsigned char *>(data);
				mSize = size;
			}

			/**
			 * Checks whether all components are aligned to the natural memory unit of component type T.
			 * This is true if their width is the number of bits in T and their offset is a multiple of that.
			 * 
			 * @return Whether the components are natively aligned to T
			 */
			inline bool aligned() const noexcept
			{
				bool matched = true;
				for (std::size_t c = 0; c < C; ++c)
				{
					if (mWidths[c] != (sizeof(T) * 8u) || mOffsets[c] % (sizeof(T) * 8u) != 0)
					{
						matched = false;
						break;
					}
				}
				return matched;
			}

			/**
			 * Checks whether all components are packed such that they are adjacent to each other.
			 * This is true if the offsets are increasing and match the component widths, and each component has the same stride.
			 * 
			 * @return Whether the components are adjacent
			 */
			inline bool adjacent() const noexcept
			{
				bool matched = true;
				for (std::size_t c = 1; c < C; ++c)
				{
					if (mOffsets[c] - mOffsets[c - 1] != mWidths[c - 1])
					{
						matched = false;
						break;
					}

					if (mStrides[c] != mStrides[c - 1])
					{
						matched = false;
						break;
					}
				}
				return matched;
			}

			/**
			 * Checks whether the components are fully contiguous.
			 * This is true if the components are adjacent and their stride is equal to the sum of the component widths.
			 * 
			 * @return Whether the components are contiguous
			 */
			inline bool contiguous() const noexcept
			{
				bool matched = this->adjacent();
				if (matched)
				{
					std::size_t stride = mWidths[0];
					for (std::size_t c = 1; c < C; ++c)
					{
						stride += mWidths[1];
					}

					// Adjacent already checked that they have the same stride
					matched &= (mStrides[0] == stride);
				}
				return matched;
			}

		private:
			/** The the packed width (in bits) of each data component in the bit stream */
			std::size_t mWidths[C] = { };

			/** The initial offset (in bits) for each component */
			std::size_t mOffsets[C] = { };

			/** The spacing (in bits) between the adjacent array elements of each component */
			std::ptrdiff_t mStrides[C] = { };

			/** The number of array index elements */
			std::size_t mSize = 0;

			/** The array data pointer */
			const unsigned char *mData = nullptr;
	};
}

#endif
