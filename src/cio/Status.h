/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_STATUS_H
#define CIO_STATUS_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The Status enumeration describes the execution state of an action as it progresses.
	 */
	enum class Status : std::uint8_t
	{
		/** No status to report */
		None,

		/** The operation has been requested but not yet started */
		Requested,
		
		/** The operation has been scheduled for execution but not yet started */
		Submitted,
		
		/** The operation has been started */
		Started,
					
		/** The operation has made an update on progress so far */
		Updated,
							
		/** The operation had been paused and is now resumed */
		Resumed,
		
		/** The operation has been interrupted for an asynchronous event but not paused or canceled */
		Interrupted,
		
		/** The operation was paused by an external request, and may be resumed or canceled later */
		Paused,
		
		/** The operation has been requested to cancel but not yet halted */
		Canceled,
		
		/** The operation has completed */
		Completed
	};
	
	/**
	 * Checks whether the operation has been requested
	 * This is true if the status is any status other than None.
	 *
	 * @param status the status
	 * @return whether the operation was requested
	 */
	inline bool requested(Status status) noexcept;
	
	/**
	 * Checks whether the operation has been requested but is not Canceled or Completed.
	 *
	 * @param status the status
	 * @return whether the operation was unfinished
	 */
	inline bool unfinished(Status status) noexcept;
	
	/**
	 * Checks whether the operation has started.
	 * This is true for any status that is at least Started.
	 *
	 * @param status the status
	 * @return whether the operation was started
	 */
	inline bool started(Status status) noexcept;
	
	/**
	 * Checks whether the operation is running.
	 * This validates that the status is at least Started and not Canceled or Completed.
	 *
	 * @return whether the operation is currently running
	 */
	inline bool running(Status status) noexcept;

	/**
	 * Checks whether a status is considered a completion state.
	 * This is true for Done.
	 *
	 * @param status The status of interest
	 * @return whether the status indicates completion
	 */
	inline bool completed(Status status) noexcept; 
	
	/**
	 * Checks whether a status is considered interrupting.
	 * This is true for Interrupted, Paused, Canceled, and Completed.
	 * When in this status, interruptors should be called.
	 *
	 * @param status The status to check
	 * @return whether the status is considered interrupting
	 */
	inline bool interrupting(Status status) noexcept;

	/**
	 * Gets the text value of the Status value.
	 *
	 * @param status The status value
	 * @return the text value
	 */
	CIO_API const char *print(Status status) noexcept;

	/**
	 * Gets a detailed description of the status value.
	 *
	 * @param status The status value
	 * @return the description text
	 */
	CIO_API const char *getDescription(Status status) noexcept;

	/**
	 * Prints the text value of the Status value to a C++ standard output stream.
	 *
	 * @param s The stream
	 * @param value The status value
	 * @return the stream after printing
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Status status);
}

/* Inline implementation */

namespace cio
{	
	inline bool requested(Status status) noexcept
	{
		return status >= Status::Requested;
	}
	
	inline bool unfinished(Status status) noexcept
	{
		return status >= Status::Requested && status < Status::Canceled;
	}

	inline bool started(Status status) noexcept
	{
		return status >= Status::Started;
	}

	inline bool running(Status status) noexcept
	{
		return status >= Status::Started && status < Status::Canceled;
	}
	
	inline bool completed(Status status) noexcept
	{
		return status >= Status::Completed;
	}
	
	inline bool interrupting(Status status) noexcept
	{
		return status >= Status::Interrupted;
	}
}

#endif
