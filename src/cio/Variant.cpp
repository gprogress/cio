/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Variant.h"

#include "Character.h"
#include "Parse.h"
#include "Print.h"
#include "Class.h"

#include <cmath>
#include <cstdio>
#include <cstring>
#include <limits>
#include <stdexcept>

#include <ostream>

namespace cio
{
	// Most negative integer value, stored as a double.  Used for conversion range checking.
	const double Variant::DBL_MIN_INT = static_cast<double>(std::numeric_limits<int64_t>::min());

	// Most positive integer value, stored as a double.  Used for conversion range checking.
	const double Variant::DBL_MAX_INT = static_cast<double>(std::numeric_limits<int64_t>::max());

	// Most positive integer value, stored as a double.  Used for conversion range checking.
	const double Variant::DBL_MAX_UNSIGNED_INT = static_cast<double>(std::numeric_limits<uint64_t>::max());

	std::uint32_t Variant::getBitsPerItem(Type type) noexcept
	{
		std::uint32_t bits;
		switch (type)
		{
			case Type::Boolean:
				bits = 1;
				break;

			case Type::Blob:
			case Type::Text:
				bits = 8;
				break;
			
			case Type::Object:
				bits = sizeof(void *) * 8;
				break;

			default:
				bits = 64;
				break;
		}

		return bits;
	}
	
	std::uint32_t Variant::getDefaultItemCount(Type type) noexcept
	{
		return type == Type::None || type == Type::Text || type == Type::Blob ? 0 : 1;
	}

	// Public member methods
	Variant::Variant() noexcept :
		mUnsigned(),
		mType(Type::None),
		mOwnership(Ownership::Embed),
		mItemSize(0),
		mItemCount(0),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(Type type) noexcept :
		mUnsigned(),
		mType(type),
		mOwnership(Ownership::Embed),
		mItemSize(Variant::getBitsPerItem(type)),
		mItemCount(Variant::getDefaultItemCount(type)),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(bool initialValue) noexcept :
		mBoolean(initialValue),
		mType(Type::Boolean),
		mOwnership(Ownership::Embed),
		mItemSize(1),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(int initialValue) noexcept :
		mInteger(initialValue),
		mType(Type::Integer),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(unsigned initialValue) noexcept :
		mUnsigned(initialValue),
		mType(Type::Unsigned),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(long initialValue) noexcept :
		mInteger(initialValue),
		mType(Type::Integer),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(unsigned long initialValue) noexcept :
		mUnsigned(initialValue),
		mType(Type::Unsigned),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(long long initialValue) noexcept :
		mInteger(initialValue),
		mType(Type::Integer),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(unsigned long long initialValue) noexcept :
		mUnsigned(initialValue),
		mType(Type::Unsigned),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(float initialValue) noexcept :
		mReal(initialValue),
		mType(Type::Real),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(double initialValue) noexcept :
		mReal(initialValue),
		mType(Type::Real),
		mOwnership(Ownership::Embed),
		mItemSize(64),
		mItemCount(1),
		mObjectMetaclass(nullptr)
	{
		// nothing more to do
	}

	Variant::Variant(const char *initialValue) :
		mUnsigned(),
		mType(Type::None),
		mOwnership(Ownership::Embed),
		mItemSize(0),
		mItemCount(0),
		mObjectMetaclass(nullptr)
	{
		this->setString(initialValue);
	}

	Variant::Variant(const std::string &initialValue) :
		mUnsigned(),
		mType(Type::None),
		mOwnership(Ownership::Embed),
		mItemSize(0),
		mItemCount(0),
		mObjectMetaclass(nullptr)
	{
		this->setString(initialValue);
	}

	Variant::Variant(const cio::Text &initialValue) :
		mUnsigned(),
		mType(Type::None),
		mOwnership(Ownership::Embed),
		mItemSize(0),
		mItemCount(0),
		mObjectMetaclass(nullptr)
	{
		this->setText(initialValue);
	}

	Variant::Variant(void *initialValue, const Metaclass *metaclass) :
		mObjectPointer(initialValue),
		mType(Type::Object),
		mOwnership(Ownership::Embed),
		mItemSize(sizeof(void *) * 8),
		mItemCount(1),
		mObjectMetaclass(metaclass)
	{
		// nothing else to do
	}

	Variant::Variant(const Variant &in) :
		mUnsigned(),
		mType(in.mType),
		mOwnership(Ownership::Embed),
		mItemSize(in.mItemSize),
		mItemCount(0),
		mObjectMetaclass(in.mObjectMetaclass)
	{
		this->resize(in.size());
		std::memcpy(this->data(), in.data(), in.bytes());
	}

	Variant::Variant(Variant &&in) noexcept :
		mType(in.mType),
		mOwnership(in.mOwnership),
		mItemSize(in.mItemSize),
		mItemCount(in.mItemCount),
		mObjectMetaclass(in.mObjectMetaclass)
	{
		std::memcpy(mBlob, in.mBlob, 8);
		std::memset(in.mBlob, 0, 8);

		in.mType = Type::None;
		in.mOwnership = Ownership::Embed;
		in.mItemSize = 0;
		in.mItemCount = 0;
	}

	Variant &Variant::operator=(const Variant &rhs)
	{
		if (&rhs != this)
		{
			this->resetToDefaultValue();
			
			mType = rhs.mType;
			mOwnership = rhs.mOwnership;
			mItemSize = rhs.mItemSize;
			mObjectMetaclass = rhs.mObjectMetaclass;
			
			if (rhs.mItemCount > 0)
			{
				this->resize(rhs.size());
				std::memcpy(this->data(), rhs.data(), rhs.bytes());
			}
		}

		return *this;
	}

	Variant &Variant::operator=(Variant &&rhs) noexcept
	{
		if (&rhs != this)
		{
			this->resetToDefaultValue();

			mType = rhs.mType;
			std::memcpy(mBlob, rhs.mBlob, 8);
			std::memset(rhs.mBlob, 0, 8);
			mOwnership = rhs.mOwnership;
			mItemSize = rhs.mItemSize;
			mItemCount = rhs.mItemCount;
			mObjectMetaclass = rhs.mObjectMetaclass;

			rhs.mType = Type::None;
			rhs.mOwnership = Ownership::Embed;
			rhs.mItemSize = 0;
			rhs.mItemCount = 0;
		}

		return *this;
	}

	Variant::~Variant() noexcept
	{
		this->clearData();
	}

	void Variant::clear() noexcept
	{
		this->setType(Type::None);
	}

	bool Variant::isDefaultValue() const noexcept
	{
		// All default values are inlined and equal to 0
		return mOwnership == Ownership::Embed && mUnsigned == 0 && mItemCount == this->getDefaultItemCount(mType);
	}

	void Variant::resizeBytes(std::size_t bytes)
	{
		this->resizeBits(bytes * 8);
	}

	void Variant::resizeBits(std::size_t bits)
	{
		if (mItemSize)
		{
			this->resize((bits + mItemSize - 1) / mItemSize);
		}
	}

	void Variant::resize(std::size_t count)
	{
		if (count >= UINT32_MAX)
		{
			throw std::overflow_error("Could not allocate variant with more than 4B items");
		}

		std::size_t neededBytes = (count * mItemSize + 7u) / 8u;
		std::size_t currentBytes = (mItemCount * mItemSize + 7u) / 8u;

		if (neededBytes != currentBytes)
		{
			std::uint8_t *current = this->data();
			Ownership ownership = mOwnership;

			if (neededBytes <= 8)
			{
				// Copy old memory to embedded memory
				mOwnership = Ownership::Embed;
				if (neededBytes > 0 && ownership != Ownership::Embed)
				{
					std::memcpy(mBlob, current, neededBytes);
				}

				// Zero-fill unused embedded memory
				if (neededBytes < currentBytes)
				{
					std::memset(mBlob + neededBytes, 0, 8 - neededBytes);
				}
			}
			else
			{
				// Allocate new memory and copy old memory to it
				mBlobPointer = new std::uint8_t[neededBytes];
				mOwnership = Ownership::Allocate;
				if (currentBytes > 0)
				{
					std::memcpy(mBlobPointer, current, std::min(neededBytes, currentBytes));
				}

				// Zero-fill newly-created memory
				if (neededBytes > currentBytes)
				{
					std::memset(mBlobPointer + currentBytes, 0, neededBytes - currentBytes);
				}
			}

			// Delete old memory now that it is unused
			if (ownership == Ownership::Allocate)
			{
				delete[] current;
			}
		}

		// Update length
		mItemCount = static_cast<std::uint32_t>(count);
	}

	bool Variant::getInteger(std::int64_t &value) const
	{
		bool success = false;

		switch (mType)
		{
			case Type::Boolean:
				value = mBoolean;
				success = true;
				break;

			case Type::Unsigned:
				value = mUnsigned;
				success = true;
				break;

			case Type::Integer:
			case Type::Enum:
				value = mInteger;
				success = true;
				break;

			case Type::Real:
				success = (mReal >= DBL_MIN_INT && mReal <= DBL_MAX_INT);
				if (success)
				{
					value = static_cast<std::int64_t>(mReal);
				}
				else
				{
					value = 0;
				}
				break;

			case Type::Text:
			{
				cio::TextParse<std::int64_t> parse = cio::parseInteger64(this->c_str());
				value = parse.value;
				success = parse.valid();
				break;
			}

			// TODO interpret binary field as signed integer

			default:
				value = 0;
				break;
		}

		return success;
	}

	std::int64_t Variant::getInteger() const
	{
		std::int64_t retInt;

		if (!getInteger(retInt))
		{
			throw std::runtime_error("Value is not an integer");
		}

		return retInt;
	}

	cio::Text Variant::viewText() const
	{
		cio::Text text;

		switch (mType)
		{
			case Type::Boolean:
			{
				text.bind(cio::print(mBoolean));
				break;
			}

			case Type::Unsigned:
			{
				text = cio::print(mUnsigned);
				break;
			}

			case Type::Integer:
			case Type::Enum:
			{
				text = cio::print(mInteger);
				break;
			}

			case Type::Real:
			{
				text = cio::print(mReal);
				break;
			}

			case Type::Text:
			{
				text.bind(this->c_str(), this->bytes());
				break;
			}

			case Type::Blob:
			{
				std::size_t bytes = this->bytes();
				char *allocated = text.allocate(bytes * 2);
				printBytes(this->data(), bytes, allocated, bytes * 2);
				break;
			}

			default:
				// nothing more to do
				break;
		}

		return text;
	}

	bool Variant::getUnsignedInteger(std::uint64_t &value) const
	{
		bool success = false;

		switch (mType)
		{
			case Type::Boolean:
				value = mBoolean;
				success = true;
				break;

			case Type::Unsigned:
				value = mUnsigned;
				success = true;
				break;

			case Type::Integer:
			case Type::Enum:
				value = mInteger;
				success = true;
				break;

			case Type::Real:
				success = (mReal >= 0.0 && mReal <= DBL_MAX_UNSIGNED_INT);
				if (success)
				{
					value = static_cast<std::uint64_t>(mReal);
				}
				else
				{
					value = 0;
				}
				break;

			case Type::Text:
			{
				cio::TextParse<std::uint64_t> parse = cio::parseUnsigned64(this->c_str());
				value = parse.value;
				success = parse.valid();
				break;
			}

			// TODO interpret binary field as signed integer

			default:
				value = 0;
				break;
		}

		return success;
	}

	std::uint64_t Variant::getUnsignedInteger() const
	{
		std::uint64_t retInt;

		if (!getUnsignedInteger(retInt))
		{
			throw std::runtime_error("Value is not an unsigned integer");
		}

		return retInt;
	}

	bool Variant::getString(std::string &value) const
	{
		bool success = false;

		switch (mType)
		{
			case Type::Boolean:
			{
				value = cio::print(mBoolean);
				success = true;
				break;
			}

			case Type::Unsigned:
			{
				value = cio::print(mUnsigned).str();
				success = true;
				break;
			}

			case Type::Integer:
			case Type::Enum:
			{
				value = cio::print(mInteger).str();
				success = true;
				break;
			}

			case Type::Real:
			{
				value = cio::print(mReal).str();
				success = true;
				break;
			}

			case Type::Text:
			{
				const char* text = this->c_str();
				std::size_t len = cio::terminator(text, this->bytes());
				value.assign(text, text + len);
				success = true;
				break;
			}

			case Type::Blob:
			{
				std::size_t bytes = this->bytes();
				value.resize(bytes * 2);
				printBytes(this->data(), bytes, &value[0], bytes * 2);
				success = true;
				break;
			}

			default:
				value.clear();
				break;
		}

		return success;
	}

	std::string Variant::getString() const
	{
		std::string retString;

		if (!this->getString(retString))
		{
			throw std::runtime_error("Value is not a string");
		}

		return retString;
	}

	bool Variant::getDouble(double &value) const
	{
		bool success = false;

		switch (mType)
		{
			case Type::Real:
			{
				value = mReal;
				success = true;
				break;
			}

			case Type::Boolean:
			{
				value = mBoolean;
				success = true;
				break;
			}

			case Type::Unsigned:
			{
				// always succeeds, though unsigned may lose a bit of precision if really high
				value = static_cast<double>(mUnsigned);
				success = true;
				break;
			}

			case Type::Integer:
			case Type::Enum:
			{
				// always succeeds, though integer may lose a bit of precision if really high
				value = static_cast<double>(mInteger);
				success = true;
				break;
			}

			case Type::Text:
			{
				cio::TextParse<double> parse = cio::parseDouble(this->c_str());
				value = parse.value;
				success = parse.valid();
				break;
			}

			case Type::Blob:
			{
				std::size_t len = this->bytes();
				const std::uint8_t *darray = this->data();
				if (len == sizeof(float))
				{
					float tmp;
					std::memcpy(&tmp, darray, sizeof(float));
					value = tmp;
					success = true;
				}
				else if (len == sizeof(double))
				{
					std::memcpy(&value, darray, sizeof(double));
					success = true;
				}
				break;
			}

			default:
				value = std::numeric_limits<double>::quiet_NaN();
				break;
		}

		return success;
	}

	double Variant::getDouble() const
	{
		double retDouble;

		if (!this->getDouble(retDouble))
		{
			throw std::runtime_error("Value is not a double");
		}

		return retDouble;
	}

	bool Variant::getBoolean(bool &value) const
	{
		bool success = false;

		switch (mType)
		{
			case Type::Boolean:
			{
				value = mBoolean;
				success = true;
				break;
			}

			case Type::Unsigned:
			case Type::Enum:
				value = (mUnsigned != 0);
				success = true;
				break;

			case Type::Integer:
				value = (mInteger != 0);
				success = true;
				break;

			case Type::Real:
				// NaN is always != 0.0 so we have to be a bit more clever for it to be false too
				value = (mReal > 0.0) || (mReal < 0.0);
				success = true;
				break;

			case Type::Text:
			{
				cio::TextParse<bool> parsed = cio::parseBoolean(this->c_str());
				value = parsed.value;
				success = parsed.valid();
				break;
			}

			case Type::Blob:
			{
				std::size_t len = this->bytes();
				const std::uint8_t *darray = this->data();
				for (size_t i = 0; i < len && !success; ++i)
				{
					success = (darray[i] != 0);
				}
				break;
			}

			default:
				value = false;
				break;
		}

		return success;
	}

	bool Variant::getBoolean() const
	{
		bool retBool = false;

		if (!this->getBoolean(retBool))
		{
			throw std::runtime_error("Value is not a boolean");
		}

		return retBool;
	}

	template<>
	std::int64_t Variant::getValue() const
	{
		return getInteger();
	}

	template<>
	std::uint64_t Variant::getValue() const
	{
		return getUnsignedInteger();
	}
	
	template<>
	bool Variant::getValue() const
	{
		return getBoolean();
	}

	template<>
	double Variant::getValue() const
	{
		return getDouble();
	}

	template<>
	std::string Variant::getValue() const
	{
		return this->getString();
	}

	void Variant::setInteger(std::int64_t value)
	{
		this->setType(Type::Integer);
		mInteger = value;
	}

	void Variant::setUnsignedInteger(std::uint64_t value)
	{
		this->setType(Type::Unsigned);
		mUnsigned = value;
	}

	void Variant::setText(const cio::Text &text)
	{
		this->setString(text.data(), text.size());
	}

	void Variant::setString(const char *value)
	{
		std::size_t length = value ? (std::strlen(value) + 1) : 0;
		this->setString(value, length);
	}

	void Variant::setString(const char *value, std::size_t length)
	{
		this->setType(Type::Text);

		// NUL pointer is represented by the default (zero-length) string
		if (value && length > 0)
		{
			std::size_t allocated = length;

			// Add null terminator if not present
			if (value[length - 1] != 0)
			{
				++allocated;
			}
			
			this->resize(allocated);
			std::memcpy(this->data(), value, length);
		}
	}

	void Variant::setString(const std::string &value)
	{
		this->setString(value.c_str(), value.size() + 1);
	}

	void Variant::setBinary(const void *value, std::size_t len)
	{
		this->setType(Type::Blob);

		if (value && len > 0)
		{
			this->resize(len);
			std::memcpy(this->data(), value, len);
		}
	}

	void Variant::setDouble(double value)
	{
		this->setType(Type::Real);
		mReal = value;
	}

	void Variant::setBoolean(bool value)
	{
		this->setType(Type::Boolean);
		mInteger = value;
	}

	void Variant::setObject(void *value, const Metaclass *metaclass)
	{
		mObjectPointer = value;
		mObjectMetaclass = metaclass;
	}

	bool Variant::update(int value)
	{
		return this->update(static_cast<long long>(value));
	}

	bool Variant::update(long value)
	{
		return this->update(static_cast<long long>(value));
	}

	bool Variant::update(long long value)
	{
		bool success = false;
		this->resetToDefaultValue();

		switch (mType)
		{
			case Type::Integer:
				mInteger = value;
				success = true;
				break;

			case Type::Unsigned:
			case Type::Enum:
				mUnsigned = value;
				success = true;
				break;

			case Type::Boolean:
				mBoolean = (value != 0);
				success = true;
				break;

			case Type::Real:
				// Might lose precision but can't fail
				mReal = static_cast<double>(value);
				success = true;
				break;

			case Type::Text:
			{
				char tmp[32];
				cio::print(value, tmp);
				this->setString(tmp);
				success = true;
				break;
			}

			case Type::Blob:
			{
				this->setBinary(&value, 8);
				success = true;
				break;
			}

			default:
				// nothing can be done
				break;
		};

		return success;
	}

	bool Variant::update(unsigned long long value)
	{
		bool success = false;
		this->resetToDefaultValue();

		switch (mType)
		{
			case Type::Unsigned:
			case Type::Enum:
				mUnsigned = value;
				success = true;
				break;

			case Type::Boolean:
				mBoolean = (value != 0);
				success = true;
				break;

			case Type::Integer:
				mInteger = value;
				success = true;
				break;

			case Type::Real:
				// Might lose precision but can't fail
				mReal = static_cast<double>(value);
				success = true;
				break;

			case Type::Text:
			{
				char tmp[32];
				cio::print(value, tmp);
				this->setString(tmp);
				success = true;
				break;
			}

			case Type::Blob:
			{
				this->setBinary(&value, 8);
				success = true;
				break;
			}

			default:
				// nothing can be done
				break;
		};

		return success;
	}

	bool Variant::update(double value)
	{
		bool success = false;
		this->resetToDefaultValue();

		switch (mType)
		{
			case Type::Real:
				mReal = value;
				success = true;
				break;

			case Type::Integer:
			case Type::Enum:
				success = (value >= DBL_MIN_INT && value <= DBL_MAX_INT);
				if (success)
				{
					mInteger = static_cast<std::int64_t>(value);
				}
				break;

			case Type::Unsigned:
				success = (value >= 0.0 && value <= DBL_MAX_INT);
				if (success)
				{
					mUnsigned = static_cast<std::uint64_t>(value);
				}
				break;

			case Type::Boolean:
				mBoolean = (value != 0.0 && !std::isnan(value));
				success = true;
				break;

			case Type::Text:
			{
				char tmp[32];
				cio::print(value, tmp);
				this->setString(tmp);
				success = true;
				break;
			}

			case Type::Blob:
			{
				this->setBinary(&value, 8);
				success = true;
				break;
			}

			default:
				// nothing can be done
				break;
		}

		return success;
	}

	bool Variant::update(bool value)
	{
		bool success = false;
		this->resetToDefaultValue();

		switch (mType)
		{
			case Type::Integer:
			case Type::Enum:
				mInteger = value;
				success = true;
				break;

			case Type::Boolean:
			{
				mBoolean = value;
				success = true;
				break;
			}

			case Type::Unsigned:
				mUnsigned = value;
				success = true;
				break;

			case Type::Real:
				mReal = value;
				success = true;
				break;

			case Type::Text:
			{
				this->setString(cio::print(value));
				success = true;
				break;
			}

			case Type::Blob:
			{
				std::int64_t tmp = value;
				this->setBinary(&tmp, 8);
				success = true;
				break;
			}

			default:
				// nothing can be done
				break;
		}

		return success;
	}

	bool Variant::update(const char *value)
	{
		cio::Text temp(value);
		return this->update(temp);
	}

	bool Variant::update(const std::string &value)
	{
		cio::Text temp(value.c_str(), value.size() + 1);
		return this->update(temp);
	}

	bool Variant::update(const Text &value)
	{
		bool success = false;
		this->resetToDefaultValue();

		std::size_t length = value.size();

		switch (mType)
		{
			case Type::Integer:
			case Type::Enum:
			{
				TextParse<long long> parsed = parseInteger(value);
				mInteger = parsed.value;
				success = parsed.valid();
				break;
			}

			case Type::Unsigned:
			{
				TextParse<unsigned long long> parsed = parseUnsigned(value);
				mUnsigned = parsed.value;
				success = parsed.valid();
				break;
			}

			case Type::Real:
			{
				TextParse<double> parsed = parseDouble(value);
				mReal = parsed.value;
				success = parsed.valid();
				break;
			}

			case Type::Boolean:
			{
				TextParse<bool> parsed = parseBoolean(value);
				mBoolean = parsed.value;
				success = parsed.valid();
				break;
			}

			case Type::Text:
			{
				std::size_t maxLen = value ? (length + 1) : 0;
				this->resize(maxLen);
				if (maxLen)
				{
					std::memcpy(this->data(), value.data(), maxLen);
				}
				success = true;
				break;
			}

			case Type::Blob:
			{
				std::size_t outLen = length / 2;
				this->resize(outLen);
				std::size_t realLen = parseBytes(value, this->data(), outLen);
				success = (realLen == outLen);
				break;
			}

			default:
				// nothing can be done
				break;
		}

		return success;
	}

	void Variant::setRawBits(std::uint64_t bits, Type type, std::size_t dataLength, Ownership ownership) noexcept
	{
		this->resetToDefaultValue();
		
		mType = type;
		mOwnership = ownership;
		mItemSize = Variant::getBitsPerItem(type);
		mItemCount = static_cast<std::uint32_t>(dataLength);
		mUnsigned = bits;
	}

	bool Variant::isSameAs(const Variant &other) const noexcept
	{
		Type t1 = mType;

		bool areSame = (t1 == other.getType() && mItemSize == other.mItemSize && mItemCount == other.mItemCount);

		if (areSame)
		{
			if (t1 == Type::Object)
			{
				areSame = this->mObjectMetaclass == other.mObjectMetaclass 
					&& this->getObject() == other.getObject();
			}
			else
			{
				int compare = std::memcmp(this->data(), other.data(), this->bytes());
				areSame = (compare == 0);
			}
		}

		return areSame;
	}

	void Variant::setType(Type type) noexcept
	{
		this->clearData();
		
		mType = type;
		mOwnership = Ownership::Embed;
		mItemSize = Variant::getBitsPerItem(type);
		mItemCount = Variant::getDefaultItemCount(type);
		mUnsigned = 0;
	}

	void Variant::convertToType(Type type)
	{
		Type oldType = mType;

		if (type != oldType)
		{
			switch (type)
			{
				case Type::Boolean:
				{
					bool temp = this->getBoolean();
					this->setBoolean(temp);
					break;
				}
				
				case Type::Integer:
				case Type::Enum:
				{
					std::int64_t temp = this->getInteger();
					this->setInteger(temp);
					break;
				}

				case Type::Unsigned:
				{
					std::uint64_t temp = this->getUnsignedInteger();
					this->setUnsignedInteger(temp);
					break;
				}

				case Type::Text:
				{
					std::string temp = this->getString();
					this->setString(temp);
					break;
				}

				case Type::Real:
				{
					double temp = this->getDouble();
					this->setDouble(temp);
					break;
				}

				case Type::Blob:
				{
					if (oldType == Type::None)
					{
						throw std::runtime_error("Value is invalid");
					}
					// Try to decode binary from text
					else if (oldType == Type::Text)
					{
						// Tricky because we don't want to kill the old array
						// while calculating the new one
						// Capture old pointer && decide if we need to delete it when done
						std::uint8_t *oldPtr = this->data();
						bool deleteOldPtr = (mOwnership == Ownership::Allocate);

						// Ensure old pointer doesn't get deleted (irrelevant if inlined)
						mBlobPointer = nullptr;

						// Change attribute type to binary && then try to update
						this->setType(Type::Blob);
						bool success = this->update(oldPtr);

						// Delete old pointer if we previously allocated it
						if (deleteOldPtr)
						{
						    delete [] oldPtr;
						}

						// Now throw conversion exception if we failed since we've
						// safely freed memory
						if (!success)
						{
						    throw std::runtime_error("Value does not match old type");
						}
					}
					break;
				}

				default:
					throw std::runtime_error("Value is invalid");
			}
		}
	}

	void Variant::clearData() noexcept
	{
		if (mOwnership == Ownership::Allocate)
		{
			delete [] mBlobPointer;
		}
		
		mUnsigned = 0;
		mOwnership = Ownership::Embed;
	}

	void Variant::resetToDefaultValue() noexcept
	{
		this->clearData();
		
		mOwnership = Ownership::Embed;
		mUnsigned = 0;
		mItemCount = Variant::getDefaultItemCount(mType);
	}

	bool Variant::matches(const char *text) const noexcept
	{
		bool matches = false;
	
		// current type is text, match can be done directly
		if (mType == Type::Text)
		{
			const char *ours = reinterpret_cast<const char *>(this->c_str());
			matches = (std::strcmp(ours, text) == 0);
			
		}
		// binary value has two text characters per byte, we can do this one iteratively
		else if (mType == Type::Blob)
		{
			matches = true;
		
			const std::uint8_t *bytes = this->data();
			const std::uint8_t *end = bytes + this->bytes();
			const char *current = text;
			
			while (bytes < end && *current)
			{
				char printed = cio::printHexDigit(*bytes);
				if (printed == *current)
				{
					++current;
					
					printed = cio::printHexDigit(*bytes >> 4);
					
					if (printed != *current)
					{
						matches = false;
						break;
					}
				}
				else
				{
					matches = false;
					break;
				}
			
				++bytes;
				++current;
			}
			
			if ((bytes == end) != (*current == 0))
			{
				matches = false;
			}
		}
		// Reminder can be printed to a fixed size buffer
		else
		{	
			char ours[64] = { };
			this->print(ours, 64);
			matches = (std::strcmp(ours, text) == 0);
		}
		
		return matches;
	}
	
	std::size_t Variant::print(char *text, std::size_t length) const noexcept
	{
		std::string printed = this->getString();
		cio::reprint(printed.c_str(), printed.size(), text, length);
		return printed.size();
	}

	bool Variant::isValid() const noexcept
	{
		return mType != Type::None;
	}

	Type Variant::isNumber() const noexcept
	{
		Type type = mType;
		if (type == Type::Text)
		{
			type = cio::detectParsedType(this->viewText());
		}

		return (type >= Type::Boolean && type <= Type::Enum) ? type : Type::None;
	}

	bool Variant::isNumberType() const noexcept
	{
		return (mType >= Type::Boolean && mType <= Type::Enum);
	}

	Type Variant::getType() const noexcept
	{
		return mType;
	}

	Ownership Variant::getOwnership() const noexcept
	{
		return mOwnership;
	}
	
	std::size_t Variant::bytes() const noexcept
	{
		return (mItemCount * mItemSize + 7u) / 8u;
	}

	std::size_t Variant::bits() const noexcept
	{
		return mItemCount * mItemSize;
	}

	std::size_t Variant::size() const noexcept
	{
		return mItemCount;
	}

	bool Variant::empty() const noexcept
	{
		return mItemCount == 0;
	}

	std::uint8_t* Variant::data() noexcept
	{
		return (mOwnership == Ownership::Embed) ? mBlob : mBlobPointer;
	}

	const std::uint8_t* Variant::data() const noexcept
	{
		return (mOwnership == Ownership::Embed) ? mBlob : mBlobPointer;
	}

	void *Variant::getObject() const noexcept
	{
		return mObjectPointer;
	}

	template <typename T>
	T *Variant::getObject(const cio::Class<T> &c) const noexcept
	{
		T *result = nullptr;
		if (mObjectMetaclass && *mObjectMetaclass == c.getMetaclass())
		{
			result = (T *)mObjectPointer;
		}
		return result;
	}

	const cio::Metaclass *Variant::getObjectMetaclass() const noexcept
	{
		return mObjectMetaclass;
	}

	const char* Variant::c_str() const noexcept
	{
		return (mOwnership == Ownership::Embed) ? mText : mTextPointer;
	}

	std::uint64_t Variant::getRawBits() const noexcept
	{
		return mUnsigned;
	}

	Variant::operator bool() const noexcept
	{
		bool value = false;
		this->getBoolean(value);
		return value;
	}

	// Global operators

	bool operator==(const Variant &lhs, const Variant &rhs)
	{
		return lhs.isSameAs(rhs);
	}

	bool operator!=(const Variant &lhs, const Variant &rhs)
	{
		return !(lhs == rhs);
	}

	bool operator<(const Variant &lhs, const Variant &rhs)
	{
		bool before = false;
		if (rhs.isValid())
		{
			if (lhs.isValid())
			{
				if (lhs.getType() == Type::Text || rhs.getType() == Type::Text)
				{
					before = lhs.getString() < rhs.getString();
				}
				else if (lhs.getType() == Type::Blob || rhs.getType() == Type::Blob)
				{
					std::size_t length = std::min(lhs.bytes(), rhs.bytes());
					int cmp = std::memcmp(lhs.data(), rhs.data(), length);
					if (cmp == 0)
					{
						before = lhs.size() < rhs.size();
					}
					else
					{
						before = (cmp < 0);
					}
				}
				else if (lhs.getType() == Type::Real || rhs.getType() == Type::Real)
				{
					before = lhs.getDouble() < rhs.getDouble();
				}
				else
				{
					before = lhs.getInteger() < rhs.getInteger();
				}
			}
			else
			{
				// invalid left is always before valid right
				before = true;
			}
		}
		// else left is never before invalid right

		return before;
	}

	bool operator<=(const Variant &lhs, const Variant &rhs)
	{
		return !(lhs > rhs);
	}

	bool operator>(const Variant &lhs, const Variant &rhs)
	{
		bool after = false;
		if (lhs.isValid())
		{
			if (rhs.isValid())
			{
				if (lhs.getType() == Type::Text || rhs.getType() == Type::Text)
				{
					after = lhs.getString() > rhs.getString();
				}
				else if (lhs.getType() == Type::Blob || rhs.getType() == Type::Blob)
				{
					std::size_t length = std::min(lhs.bytes(), rhs.bytes());
					int cmp = std::memcmp(lhs.data(), rhs.data(), length);
					if (cmp == 0)
					{
						after = lhs.size() > rhs.size();
					}
					else
					{
						after = (cmp > 0);
					}
				}
				else if (lhs.getType() == Type::Real || rhs.getType() == Type::Real)
				{
					after = lhs.getDouble() > rhs.getDouble();
				}
				else
				{
					after = lhs.getInteger() > rhs.getInteger();
				}
			}
			// valid left is always after invalid right
			else
			{
				after = true;
			}
		}
		// else invalid left is never after right

		return after;
	}

	bool operator>=(const Variant &lhs, const Variant &rhs)
	{
		return !(lhs < rhs);
	}

	Variant operator+(const Variant &lhs, const Variant &rhs)
	{
		Variant sum;

		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				sum = rhs;
			}
		}
		else
		{
			if (rt == Type::None)
			{
				sum = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				sum.setDouble(lhs.getDouble() + rhs.getDouble());
			}
			else
			{
				sum.setInteger(lhs.getInteger() + rhs.getInteger());
			}
		}

		return sum;
	}

	Variant operator-(const Variant &lhs)
	{
		Variant output;
		Type lt = lhs.isNumber();
		if (lt != Type::None)
		{
			if (lt == Type::Real)
			{
				output.setDouble(-lhs.getDouble());
			}
			else
			{
				output.setInteger(-lhs.getInteger());
			}
		}
		return output;
	}

	Variant operator-(const Variant &lhs, const Variant &rhs)
	{
		Variant diff;

		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				diff = -rhs;
			}
		}
		else
		{
			if (rt == Type::None)
			{
				diff = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				diff.setDouble(lhs.getDouble() - rhs.getDouble());
			}
			else
			{
				diff.setInteger(lhs.getInteger() - rhs.getInteger());
			}
		}

		return diff;
	}

	Variant operator*(const Variant &lhs, const Variant &rhs)
	{
		Variant prod;

		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				prod = rhs;
			}
		}
		else
		{
			if (rt == Type::None)
			{
				prod = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				prod.setDouble(lhs.getDouble() * rhs.getDouble());
			}
			else
			{
				prod.setInteger(lhs.getInteger() * rhs.getInteger());
			}
		}

		return prod;
	}

	Variant operator/(const Variant &lhs, const Variant &rhs)
	{
		Variant quot;

		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				quot.setDouble(1.0 / rhs.getDouble());
			}
		}
		else
		{
			if (rt == Type::None)
			{
				quot = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				quot.setDouble(lhs.getDouble() / rhs.getDouble());
			}
			else
			{
				// Avoid divide by zero, result is invalid variant
				long long denom = rhs.getInteger();
				if (denom)
				{
					quot.setInteger(lhs.getInteger() / denom);
				}
			}
		}

		return quot;
	}

	Variant lower(const Variant &lhs, const Variant &rhs)
	{
		Variant result;
		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				result = rhs;
			}
		}
		else
		{
			if (rt == Type::None)
			{
				result = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				result = (lhs.getDouble() <= rhs.getDouble()) ? lhs : rhs;
			}
			else
			{
				result = (lhs.getInteger() <= rhs.getInteger()) ? lhs : rhs;
			}
		}
		return result;
	}

	Variant upper(const Variant &lhs, const Variant &rhs)
	{
		Variant result;
		Type lt = lhs.isNumber();
		Type rt = rhs.isNumber();

		if (lt == Type::None)
		{
			if (rt != Type::None)
			{
				result = rhs;
			}
		}
		else
		{
			if (rt == Type::None)
			{
				result = lhs;
			}
			else if (lt == Type::Real || rt == Type::Real)
			{
				result = (lhs.getDouble() >= rhs.getDouble()) ? lhs : rhs;
			}
			else
			{
				result = (lhs.getInteger() >= rhs.getInteger()) ? lhs : rhs;
			}
		}
		return result;
	}

	std::ostream& operator<<(std::ostream &stream, const Variant& value)
	{
		stream << "Type: " << value.getType() << ", Value: ";

		switch (value.getType())
		{
			case Type::Integer:
			case Type::Enum:
				stream << value.getInteger();
				break;

			case Type::Unsigned:
				stream << value.getUnsignedInteger();
				break;

			case Type::Boolean:
				stream << std::boolalpha << value.getBoolean();
				break;

			case Type::Text:
				stream << value.data();
				break;

			case Type::Real:
				stream << value.getDouble();
				break;

			case Type::Blob:
				stream << value.getUnsignedInteger();
				break;

			default:
				stream << " -invalid-";
				break;
		}

		return stream;
	}
}
