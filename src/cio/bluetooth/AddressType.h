/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ADDRESSTYPE_H
#define CIO_BLUETOOTH_ADDRESSTYPE_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{	
		/**
		 * The type of Bluetooth device address.
		 */
		enum class AddressType : std::uint8_t
		{
			/** Bluetooth Classic BR/EDR */
			Classic = 0x00,
			
			/** Bluetooth Low Energy Public Address */
			Public = 0x01,
			
			/** Bluetooth Low Energy Random Address */
			Random = 0x02,
			
			/** No address type set */
			None = 0xFF
		};
		
		/**
		 * Gets the text representation of a Bluetooth address type.
		 * Unrecognized address types will return "None".
		 *
		 * @param type The address type
		 * @return the text of the address type
		 */
		CIO_BLUETOOTH_API const char *print(AddressType type) noexcept;
		
		/**
		 * Prints the text representation of an address type to a C++ stream.
		 *
		 * @param stream The stream
		 * @param type The address type
		 * @return the stream
		 * @throw std::exception If the underlying C++ stream operation throws an exception
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, AddressType type);
	}
}

#endif
