/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Subscriptions.h"

#include "Property.h"
#include "Subscription.h"

#include <cio/Response.h>

namespace cio
{
	namespace bluetooth
	{
		Subscriptions::Subscriptions() noexcept
		{
			// nothing to do
		}
				
		Subscriptions::Subscriptions(const Subscriptions &in) = default;
		
		Subscriptions::Subscriptions(Subscriptions &&in) noexcept :
			mSubscriptions(std::move(in.mSubscriptions))
		{
			// nothing more to do
		}
		
		Subscriptions &Subscriptions::operator=(const Subscriptions &in)
		{
			mSubscriptions = in.mSubscriptions;
			return *this;
		}
		
		Subscriptions &Subscriptions::operator=(Subscriptions &&in) noexcept
		{
			mSubscriptions = std::move(in.mSubscriptions);
			return *this;
		}
		
		Subscriptions::~Subscriptions() noexcept = default;
		
		void Subscriptions::clear() noexcept
		{
			mSubscriptions.clear();
		}
	
		Response<Subscription *> Subscriptions::subscribe(std::uint16_t handle)
		{
			return this->subscribe(handle, AttributeCallback(), std::chrono::milliseconds());
		}
		
		Response<Subscription *> Subscriptions::subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval)
		{
			Response<Subscription *> result;

			// Item to insert
			std::pair<std::uint16_t, Subscription> item(handle, Subscription(handle, std::move(listener), interval));
			
			auto ii = mSubscriptions.find(handle);
			
			if (ii != mSubscriptions.end())
			{
				item.second.setModes(ii->second.getModes());
				item.second.setCurrentMode(ii->second.getCurrentMode());
				result.reason = Reason::Exists;
			}

			auto pib = mSubscriptions.insert(std::move(item));
			result.receive(&(pib->second));
			
			return result;
		}
		
		State Subscriptions::notify(std::uint16_t handle, const void *data, std::size_t length) noexcept
		{
			State status(Action::Notify);
			auto ii = mSubscriptions.find(handle);
			while (ii != mSubscriptions.end() && ii->second.getHandle() == handle)
			{
				status.succeed();
				try
				{
					ii->second.notify(data, length);
				}
				catch (...)
				{
					// TODO figure out what to do with this
				}

				++ii;
			}
			return status;
		}
		
		State Subscriptions::unsubscribe(const Subscription *subscription)
		{
			State status(Action::Unsubscribe);
			if (subscription)
			{
				auto ii = mSubscriptions.find(subscription->getHandle());
				status.succeed();
			}
			
			return status;	
		}
		
		Progress<std::size_t> Subscriptions::unsubscribe(std::uint16_t handle)
		{
			Progress<std::size_t> result(Action::Unsubscribe);
			std::size_t count = mSubscriptions.erase(handle);
			if (count > 0)
			{
				result.succeed();
				result.count = count;
			}
			return result;
		}
		
		Progress<std::size_t> Subscriptions::unsubscribe() noexcept
		{
			Progress<std::size_t> result(Action::Unsubscribe);
			if (!mSubscriptions.empty())
			{
				result.succeed();
				result.count = mSubscriptions.size();
				mSubscriptions.clear();
			}
			return result;
		}
		
		Subscription *Subscriptions::top(std::uint16_t handle) noexcept
		{
			Subscription *sub = nullptr;
			auto ii = mSubscriptions.find(handle);
			if (ii != mSubscriptions.end())
			{
				sub = &(ii->second);
			}
			return sub;
		}
		
		Subscription *Subscriptions::top() noexcept
		{
			Subscription *sub = nullptr;
			if (!mSubscriptions.empty())
			{
				sub = &(mSubscriptions.begin()->second);
			}
			return sub;
		}
		
		Subscription *Subscriptions::schedule(std::chrono::milliseconds timeout)
		{
			// TODO we probably want something more efficient like a priority queue?
			Subscription *result = nullptr;
			std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
			std::chrono::time_point<std::chrono::steady_clock> soonest;
			
			for (std::pair<const std::uint16_t, Subscription> &item : mSubscriptions)
			{
				// Ignore subscriptions that are set up for asynchronous mode
				if (item.second.getCurrentMode() == Property::Read)
				{
					std::chrono::time_point<std::chrono::steady_clock> scheduled = item.second.scheduled();
					if (!result || scheduled < soonest)
					{
						if (scheduled <= now)
						{
							soonest = scheduled;
							result = &(item.second);
						}
						else
						{
							std::chrono::milliseconds remaining = std::chrono::duration_cast<std::chrono::milliseconds>(scheduled - now);
							if (remaining <= timeout && remaining >= item.second.getPollInterval())
							{
								soonest = scheduled;
								result = &(item.second);
							}
						}
					}
				}
			}
			
			return result;
		}
	}
}

