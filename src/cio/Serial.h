/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_SERIAL_H
#define CIO_SERIAL_H

#include "Types.h"

#include "Device.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The Serial class provides the lowest level access to the native serial device read/write API.
	* Typically this primarily uses functionality from Device but adds some additional configuration methods and ways
	* to interact with terminals or COM port devices.
	*
	* Serial devices generally exist on the filesystem like files (with some special filenames on Windows) but in terms of operations
	* actually work much more like network stream channels.
	*/
	class CIO_API Serial : public Device
	{
		public:
			enum class Parity
			{
				None = 0,
				Odd = 1,
				Even = 2,
				Mark = 3,
				Space = 4
			};

			enum class StopBits
			{
				One = 0,
				OnePlusHalf = 1,
				Two = 2
			};

			/**
			* Gets the metaclass for the Serial class.
			*
			* @return the metaclass for this class
			*/
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct an instance not connected to any device.
			*/
			Serial();

			/**
			* Move a device connection into a new instance of this class.
			*
			* @param in The device to move
			*/
			Serial(Serial &&in) noexcept;

			/**
			* Move a device connection into this object.
			* The existing device connection is closed.
			*
			* @param in The device to move
			* @return this device
			*/
			Serial &operator=(Serial &&in) noexcept;

			/**
			* Close and tear down a device instance.
			*/
			virtual ~Serial() noexcept override;

			/**
			* Gets the metaclass for the runtime type of this object.
			*
			* @return the metaclass
			*/
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			* Configures the most common settings of an open serial (COM) port device.
			*
			* @param baud The baud rate
			* @param bitsPerByte The number of bits per byte on this line (8 is most common)
			* @param parity The line parity value (Parity::None is the most common)
			* @param stopBits The stop bits duration on the line (StopBits::One is the most common)
			* @throw std::system_error If the device could not be configured
			*/
			void configureSerialPort(unsigned baud, unsigned bitsPerByte = 8, Parity parity = Parity::None, StopBits stopBits = StopBits::One);

			/**
			* Opens the first available serial (COM) port device.
			*
			* @throw std::runtime_error If no serial ports were available
			* @return the serial port that was opened
			*/
			std::string detectOpenSerialPort();
			
		private:
			/** The metaclass for this class */
			static Class<Serial> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

