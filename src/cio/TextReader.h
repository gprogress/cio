/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TEXTREADER_H
#define CIO_TEXTREADER_H

#include "Types.h"

#include "Allocator.h"
#include "Buffer.h"
#include "Input.h"
#include "Class.h"
#include "Progress.h"
#include "Seek.h"
#include "Source.h"
#include "Token.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The TextReader implements parsing CIO Input via CIO Tokens using a CIO Buffer to store the data
	 */
	class CIO_API TextReader
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct a new empty text reader.
			*/
			TextReader() noexcept;

			/**
			* Construct a new text reader with the given Input.  Specify a 
			* capacity for the internal buffer if different than default.
			*
			* @param input The underlying input used to get the data
			* @param capacity The capacity of the buffer that holds the input data
			*/
			TextReader(Input *input, std::size_t capacity = 4096) noexcept;

			/**
			* Construct a new text reader with the given Input, Source and capacity
			* if a capacity other than the default is desired.
			*
			* @param input The underlying input used to get the data
			* @param source The underlying data source information
			* @param capacity The capacity of the buffer that holds the input data
			*/
			TextReader(Input* input, const Source& source, std::size_t capacity = 4096) noexcept;

			/**
			 * Construct a copy of a text reader.
			 *
			 * @param in The text reader to copy
			 */
			TextReader(const TextReader &in);

			/**
			* Move text reader underlying data into a new instance of this class.
			*
			* @param in The device to move
			*/
			TextReader(TextReader &&in) noexcept;

			/**
			 * Copy a text reader.
			 *
			 * @param in The text reader to copy
			 * @return this text reader
			 */
			TextReader &operator=(const TextReader &in);

			/**
			* Move text reader underlying data into this object.
			*
			* @param in The text reader to move
			* @return this text reader
			*/
			TextReader &operator=(TextReader &&in) noexcept;

			/**
			* Destructor.
			*/
			~TextReader()  noexcept = default;

			/**
			 * Calls clear for buffer and source and sets the input to null.
			 */
			inline void clear() noexcept;
			
			/**
			 * Gets the underlying input for the text reader.
			 *
			 * @return the underlying input used to get the data
			 */
			inline Input *input() const noexcept;
			
			/**
			 * Sets the underlying input for the text reader and 
			 * also fills the buffer to the given capacity.
			 *
			 * @param input the underlying input used to get the data
			 * @param capacity the size of the underlying buffer to fill
			 */
			void setInput(Input *input, std::size_t capacity=4096) noexcept;

			/**
			 * Gets the underlying buffer used to store bytes.
			 *
			 * @return the underlying buffer of stored bytes
			 */
			inline const Buffer& buffer() const noexcept;

			/**
			 * Sets the underlying buffer used to store bytes.
			 *
			 * @param buffer the buffer to be used to read tokens from
			 */
			void setBuffer(Buffer buffer) noexcept;

			/**
			 * Gets the current Source information (line and
			 * column number changes reflect information that has
			 * been read).
			 *
			 * @return the current Source information
			 */
			inline const Source& source() const noexcept;

			/**
			 * Sets the current Source information.
			 *
			 * @param the current Source information
			 */
			void setSource(const Source& source) noexcept;

			/**
			 * Gets the runtime metaclass for this class.
			 * 
			 * @return the metaclass
			 */
			const Metaclass &getMetaclass() const noexcept;

			/**
			 * Sets the Source line number to 0 and column to 0.
			 *
			 * @return this text reader for convenience
			 */
			TextReader &reset() noexcept;

			/**
			 * Refills the buffer
			 * @return the actual progress after the refill
			 */
			Progress<std::size_t> refill() noexcept;

			/**
			 * Processes the next token in the buffer
			 * @param discardWhitespace Indicates whether the token should ignore the
			 * whitespace at the beginning of the token (if any).
			 * @return the next token
			 */
			Token nextToken(bool discardWhitespace = false) noexcept;

		private:
			/** The metaclass for this class */
			static Class<TextReader> sMetaclass;

			/** The Input for this class */
			Input *mInput;

			/** The Buffer of stored bytes for this class */
			Buffer mBuffer;

			/** The Source information for this class */
			Source mSource;
	};
}

/* Inline implementation */

namespace cio
{
	inline Input* TextReader::input() const noexcept
	{
		return mInput;
	}

	inline const Source& TextReader::source() const noexcept
	{
		return mSource;
	}

	inline const Buffer& TextReader::buffer() const noexcept
	{
		return mBuffer;
	}

	void inline TextReader::clear() noexcept
	{
		mInput = nullptr;
		mBuffer.clear();
		mSource.clear();
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
