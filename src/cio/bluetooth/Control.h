/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_CONTROL_H
#define CIO_BLUETOOTH_CONTROL_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Control class represents the primary control opcodes of a Bluetooth Controller.
		 * These are 16-bit command codes that are traditionally grouped using a 10-bit group code
		 * and a 6-bit operation code known as OGC and OGF.
		 */
		enum class Control : std::uint16_t
		{
			/** No control was set */
			None = 0x0000,
			
			/** Requests disconnection from an established connection */
			Disconnect = 0x0406,
			
			/** Requests a reset of the Bluetooth controller */
			Reset = 0x0C03,
		
			/** Sets the low energy scan parameters */
			SetScanParameters = 0x200B,
			
			/** Enables or disables low energy scanning */
			Scan = 0x200C,
		
			/** Requests to create a low energy connection */
			LowEnergyCreateConnection = 0x200D,
			
			/** Requests to cancel a low-energy connection currently in progress */
			LowEnergyCancelConnection = 0x200E,
			
			/** Updates the parameters for an existing low-energy connection */
			LowEnergyUpdateConnection = 0x2013,
			
			/** Requests low energy encryption */
			Encrypt = 0x2017,
			
			/** Generates a random number */
			Rand = 0x2018,
			
			/** Starts low energy encryption */
			StartEncryption = 0x2019,
			
			/** Provides a reply to a long term key request */
			ProvideLongTermKey = 0x201A,
			
			/** Denies a reply to a long term key request */
			DenyLongTermKey = 0x201B
		};

		/**
		 * Gets a human readable text representation of an HCI Command opcode.
		 *
		 * @param type The HCI command opcode
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(Control code) noexcept;

		/**
		 * Prints a human readable text representation of an HCI Command opcode to a C++ stream.
		 *
		 * @param s The stream
		 * @param type The HCI Command opcode
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, Control code);
	}
}

#endif
