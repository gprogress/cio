/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Filesystem.h"


#include "Class.h"

#include "Directory.h"
#include "Exception.h"
#include "File.h"
#include "Metadata.h"
#include "Path.h"
#include "Progress.h"
#include "ProtocolFilter.h"
#include "ProtocolRegistrar.h"
#include "Response.h"
#include "StandardPath.h"

#include <cctype>

#if defined CIO_HAVE_WINDOWS_H

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <windows.h>
#endif

#if defined _MSC_VER
#pragma warning(disable: 4996)
#endif

#if defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#endif

#if defined CIO_HAVE_FCNTL_H
#include <fcntl.h>
#endif

#if defined CIO_HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif

namespace cio
{
	Class<Filesystem> Filesystem::sMetaclass("cio::Filesystem");

	/* In Static.cpp
	const Text Filesystem::sFilePrefix("file", nullptr);

	ProtocolFilter Filesystem::sDefaultFilter[1] = {
		{ &sMetaclass, sFilePrefix, sWildcard, Resource::Unknown }
	};
	
	ProtocolRegistrar<Protocol> sProtocolRegistrar;
	*/
		
	const Metaclass &Filesystem::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Resource Filesystem::checkForSpecialWindowsDevices(const Text &filename) noexcept
	{
		Resource type = Resource::None;
		std::size_t length = filename.strlen();

		if (length >= 3)
		{
			switch (std::toupper(filename[0]))
			{
				// Check for AUX
				case 'A':
					if (std::toupper(filename[1]) == 'U' &&
						std::toupper(filename[2]) == 'X' &&
						(length == 3 || filename[3] == 0))
					{
						type = Resource::Device;
					}
					break;

					// Check for CON or COM
				case 'C':
				{
					if (std::toupper(filename[1]) == 'O')
					{
						// check for CON
						if (std::toupper(filename[2]) == 'N' &&
							(length == 3 || filename[3] == 0))
						{
							type = Resource::Device;
						}
						// check for COM0 through COM9
						else if (length >= 4 &&
							std::toupper(filename[2]) == 'M' &&
							filename[3] >= '0' && filename[3] <= '9' &&
							(length == 4 || filename[4] == 0))
						{
							type = Resource::Device;
						}
					}
					break;
				}

				// Check for LPT0 through LPT9
				case 'L':
				{
					if (length >= 4)
					{
						if (std::toupper(filename[1]) == 'P' &&
							std::toupper(filename[2]) == 'T' &&
							filename[3] >= '0' && filename[3] <= '9' &&
							(length == 4 || filename[4] == 0))
						{
							type = Resource::Device;
						}
					}

					break;
				}

				// Check for NUL
				case 'N':
				{
					if (std::toupper(filename[1]) == 'U' &&
						std::toupper(filename[2]) == 'L' &&
						(length == 3 || filename[3] == 0))
					{
						type = Resource::Device;
					}
					break;
				}

				// Check for PRN
				case 'P':
				{
					if (std::toupper(filename[1]) == 'R' &&
						std::toupper(filename[2]) == 'N' &&
						(length == 3 || filename[3] == 0))
					{
						type = Resource::Device;
					}
				}

				default:
					// nothing to do
					break;
			}
		}
		return type;
	}

	const Metaclass &Filesystem::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Text Filesystem::getClassProtocolPrefix() noexcept
	{
		return sFilePrefix;
	}

	Chunk<ProtocolFilter> Filesystem::getClassProtocolFilter() noexcept
	{
		return Chunk<ProtocolFilter>(sDefaultFilter, 1);
	}
		
	Path Filesystem::getStandardPath(StandardPath path, Resource type) const
	{
		// Start out with a static buffer of size 255 plus null terminator
		Path result;
		char tmp[256];
			
		Progress<std::size_t> attempt = this->getNativeStandardPath(path, type, tmp, 256);
		if (attempt.succeeded())
		{
			result = Path::fromNativeFile(tmp);
		}
		else
		{
			std::size_t previous = 256;
			std::vector<char> buffer;
				
			while (attempt.reason == Reason::Overflow)
			{
				if (attempt.count != CIO_UNKNOWN_SIZE)
				{
					buffer.resize(attempt.count);
				}
				else
				{
					buffer.resize(previous * 3 / 2);
				}
					
				attempt = this->getNativeStandardPath(path, type, buffer.data(), buffer.size());
			}
				
			// Failure other than overflow - this is a hard stop
			if (attempt.failed())
			{
				throw Exception(attempt);
			}
				
			// Trim to actual size
			result = Path::fromNativeFile(buffer.data(), attempt.count);
		}
			
		return result;
	}

	Path Filesystem::getApplicationFile() const
	{
		return this->getStandardPath(StandardPath::Application, Resource::File);
	}

	Path Filesystem::getApplicationDirectory() const
	{
		return this->getStandardPath(StandardPath::Application, Resource::Directory);
	}

	Path Filesystem::getCurrentDirectory() const
	{
		return this->getStandardPath(StandardPath::Current, Resource::Directory);
	}

	void Filesystem::setCurrentDirectory(const Path &path, Traversal createParents)
	{
		// Ensure it exists
		this->createParents(path, createParents);

		Text nativePath = path.toNativeFile();
		State state = this->setNativeCurrentDirectory(nativePath.c_str());
		if (state.failed())
		{
			throw Exception(state);
		}
	}
		
	Path Filesystem::getTemporaryDirectory() const
	{
		return this->getStandardPath(StandardPath::Temporary);
	}

	Path Filesystem::createTemporaryDirectory()
	{
		Path result;
		Path base = Filesystem::getTemporaryDirectory();
			
		char tmp[24] = { 't', 'e', 'm', 'p' };

		State status;
		while (!status.succeeded())
		{
			// TODO replace with PID and timestamp
			std::uint32_t value = std::rand();
			std::sprintf(tmp + 4, "%u", value);

			result = base / tmp;

			Text native = result.toNativeFile();		
			status = this->createNativeDirectory(native.data());
			if (status.failed())
			{
				if (status.reason == Reason::Missing)
				{
					throw Exception(status, "System temporary directory does not exist!");
				}
				else if (status.reason != Reason::Exists)
				{
					throw Exception(status, "Unexpected failure in temporary directory creation");
				}
				// else this failed because the directory already exists, re-roll and try again
			}
		}

		return result;
	}
		
	Filesystem::Filesystem() noexcept :
		Protocol(sFilePrefix, getClassProtocolFilter())
	{
		// nothing more to do
	}

	Filesystem::~Filesystem() noexcept = default;

	void Filesystem::clear() noexcept
	{
		Protocol::clear();

		mDefaultPrefix = sFilePrefix;
		mFilters.bind(sDefaultFilter, 1, nullptr);
	}

	Resource Filesystem::checkForSpecialFilename(const Text &filename) const noexcept
	{
		// First perform base class check for '.' and '..'
		Resource type = Protocol::checkForSpecialDirectories(filename);

		// TODO check for filesystem type and perform check even on non-Windows platforms where appropriate
#if defined CIO_HAVE_WINDOWS_H
		// If it's not one of these, check for the various Windows special device names
		if (type == Resource::None)
		{
			type = Filesystem::checkForSpecialWindowsDevices(filename);
		}
#endif

		return type;
	}

	Metadata Filesystem::readMetadata(const Path &path) const
	{
		Text file = path.toNativeFile();
		return this->getNativeMetadata(file.data());
	}

	Metadata Filesystem::create(const Metadata &path, Traversal createParents)
	{
		Metadata result;
		Text file = path.getLocation().toNativeFile();

		if (!file.empty())
		{
			bool attemptMetadata = false;
				
			this->createParents(path.getLocation(), createParents);

			if (path.getType() == Resource::Directory)
			{
				State status = this->createNativeDirectory(file.data());

				if (status.succeeded() || status.reason == Reason::Exists)
				{
					result.setType(Resource::Directory);
					result.setLocation(file);
				}
			}
			else if (path.getType() == Resource::Directory)
			{
				State status = this->createNativeFile(file.data());

				if (status.succeeded() || status.reason == Reason::Exists)
				{
					result.setType(Resource::File);
					result.setLocation(file);
				}
			}
		}

		return result;
	}

	Resource Filesystem::readType(const Path &path) const
	{
		Text nativeFile = path.toNativeFile();
		return this->getNativeType(nativeFile.data());
	}

	ModeSet Filesystem::readModes(const Path &path, ModeSet desired) const
	{
		Text nativeFile = path.toNativeFile();
		Resource type = this->getNativeType(nativeFile.data());
		return this->getModesForType(type, desired);
	}

	Length Filesystem::readSize(const Path &path) const
	{
		Text nativeFile = path.toNativeFile();
		return this->getNativeFileSize(nativeFile.data());
	}

	std::unique_ptr<Protocol> Filesystem::openDirectory(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state;

		if (createParents)
		{
			state = this->createDirectory(path, modes, createParents);
		}

		return std::unique_ptr<Protocol>(new Directory(path, modes));
	}

	std::unique_ptr<Protocol> Filesystem::readDirectory(const Path &path, ModeSet modes) const
	{
		return std::unique_ptr<Protocol>(new Directory(path, modes));
	}

	State Filesystem::createDirectory(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
			
		Text nativeFile = path.toNativeFile();
			
		state = this->createNativeDirectory(nativeFile.data());
			
		if (state.failed() && state.reason == Reason::Missing && createParents && !path.isRoot())
		{
			this->createParents(path, createParents);
			state = this->createNativeDirectory(nativeFile.data());
		}
			
		if (state.failed() && state.reason != Reason::Exists)
		{
			throw Exception(state);
		}

		return state;
	}

	Progress<std::uint64_t> Filesystem::erase(const Path &path, Traversal mode)
	{
		Progress<std::uint64_t> count(Action::Remove);
		Text file = path.toNativeFile();
		Resource type = this->getNativeType(file.c_str());

		if (!file.empty() && type != Resource::None)
		{
			if (mode && type == Resource::Directory)
			{
				std::vector<Path> children = this->listDirectory(path, Traversal::Immediate);

				for (const Path &child : children)
				{
					count += this->erase(child, mode - 1);
				}
			}

			if (type == Resource::Directory)
			{
				State status = this->removeNativeDirectory(file.c_str());
				count.count += status.succeeded();
			}
			else
			{
				State status = this->removeNativeFile(file.c_str());
				count.count += status.succeeded();
			}
		}

		return count;
	}

	std::unique_ptr<Input> Filesystem::openInput(const Path &path, ModeSet modes) const
	{
		return std::unique_ptr<Input>(new File(path, modes));
	}

	std::unique_ptr<Output> Filesystem::openOutput(const Path &path, ModeSet modes, Traversal createParents)
	{
		this->createParents(path, createParents);
		return std::unique_ptr<Output>(new File(path, modes));
	}

	std::unique_ptr<Channel> Filesystem::openChannel(const Path &path, ModeSet modes, Traversal createParents)
	{
		this->createParents(path, createParents);
		return std::unique_ptr<Channel>(new File(path, modes));
	}
		
	State Filesystem::createFile(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
			
		Text nativeFile = path.toNativeFile();
			
		state = this->createNativeFile(nativeFile.data());
		if (state.failed() && state.reason == Reason::Missing && createParents)
		{
			this->createParents(path, createParents);
			state = this->createNativeFile(nativeFile.data());
		}
			
		if (state.failed() && state.reason != Reason::Exists)
		{
			throw Exception(state, "Could not create file");
		}
			
		return state;
	}

	Path Filesystem::readLink(const Path &path, Traversal depth) const
	{
		Path target = path;
		std::vector<Path> parents;
		Text nativeLink = target.toNativeFile();

		while (depth)
		{
			// 99.9% of the time, the link will fit in this buffer
			Text nativeTarget;
			char buffer[512];
			Progress<std::size_t> result = this->readNativeLink(nativeLink.data(), buffer, 512);
			if (result.succeeded())
			{
				nativeTarget = buffer;
			}
			// For really long links, reallocate buffer until we have it
			else
			{
				std::size_t previous = 512;

				while (result.failed() && result.reason == Reason::Underflow)
				{
					std::size_t next = result.total;
					if (next == CIO_UNKNOWN_SIZE)
					{
						next = previous * 2;
					}

					std::vector<char> tmp(next);
					result = this->readNativeLink(nativeLink.data(), tmp.data(), tmp.size());

					if (result.succeeded())
					{
						nativeTarget.internalize(tmp.data());
					}
				}

				if (result.failed())
				{
					throw Exception(result, "Could not read link");
				}
			}

			target = Path::fromNativeFile(nativeTarget);

			if (depth > Traversal::Immediate)
			{
				Resource targetType = this->getNativeType(nativeTarget.c_str());

				if (targetType == Resource::Link)
				{
					if (depth == Traversal::Recursive)
					{
						if (std::find(parents.begin(), parents.end(), target) != parents.end())
						{
							throw Exception(Action::Read, Reason::Infinite);
						}

						parents.emplace_back(std::move(target));
					}

					nativeLink = std::move(nativeTarget);
				}
				else
				{
					depth = Traversal::Immediate;
					break;
				}
			}

			--depth;
		}
			
		return target;
	}
			
	State Filesystem::createLink(const Path &link, const Path &target, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
			
		Text nativeLink = link.toNativeFile();
		Text nativeTarget = target.toNativeFile();
			
		state = this->createNativeLink(nativeLink.c_str(), nativeTarget.c_str());
		if (state.failed() && state.reason == Reason::Missing && createParents)
		{
			this->createParents(link, createParents);
			state = this->createNativeLink(nativeLink.c_str(), nativeTarget.c_str());
		}
						
		if (state.failed() && state.reason != Reason::Exists)
		{
			throw Exception(state, "Could not create link");
		}
			
			
		return state;
	}
							
	Progress<std::uint64_t> Filesystem::copy(const Path &source, const Path &destination, Traversal depth, Traversal createParents)
	{
		Progress<std::uint64_t> status(Action::Copy);
		Text sourceFile = source.toNativeFile();
		Text destFile = destination.toNativeFile();
			
		Resource sourceType = this->getNativeType(sourceFile.data());
		Resource targetType = this->getNativeType(destFile.data());
			
		if (targetType == Resource::None)
		{
			// ensure parents are created if needed
			this->createParents(destination, createParents);
		}

		if (sourceType == Resource::File)
		{
			status = this->copyNativeFile(sourceFile.data(), destFile.data());
			status.update(1, 1);
		}
		else if (sourceType == Resource::Directory)
		{
			status = this->createNativeDirectory(destFile.data());
			std::vector<Path> toCopy = Directory::list(source);
			Path subdir = Path::fromNativeFile(destFile);
			status.update(1, 1 + toCopy.size());
				
			for (const Path &path : toCopy)
			{
				status += this->copy(path, subdir, depth - 1, Traversal::None);
			} 
				
			status.total -= toCopy.size(); // double counted by sub-copy
		}
			
		if (status.failed())
		{
			throw Exception(status, "Could not perform filesystem copy");
		}
			
		return status;
	}
		
	Progress<std::uint64_t> Filesystem::move(const Path &source, const Path &destination, Traversal createParents)
	{
		Progress<std::uint64_t> status(Action::Move);
		Text sourceFile = source.toNativeFile();
		Text destFile = destination.toNativeFile();
			
		Resource sourceType = this->getNativeType(sourceFile.data());
		Resource targetType = this->getNativeType(destFile.data());	
			
		if (targetType == Resource::None)
		{
			// ensure parents are created if needed
			this->createParents(destination, createParents);
		}
				
		if (sourceType == Resource::File)
		{
			status = this->moveNativeFile(sourceFile.data(), destFile.data());
			status.update(1, 1);
		}
		else if (sourceType == Resource::Directory)
		{
			status = this->moveNativeDirectory(sourceFile.data(), destFile.data());
			status.update(1, 1);
		}
			
		if (status.failed())
		{
			throw Exception(status, "Could not perform filesystem move");
		}	
			
		return status;
	}

	// Native OS API

	struct StandardPathLookup
	{
		StandardPath type;

		const char *initial;

		const char *env;

		const char *suffix;
	};

	const StandardPathLookup sPathLookup[] = {
#if defined CIO_HAVE_WINDOWS_H
		{ StandardPath::None, nullptr, nullptr, nullptr },
		{ StandardPath::Application, nullptr, nullptr, nullptr },
		{ StandardPath::Library, nullptr, nullptr, nullptr },
		{ StandardPath::Current, nullptr, "CD", nullptr },
		{ StandardPath::Temporary, "C:\\Temp", "TEMP", nullptr },
		{ StandardPath::Null, "NUL", nullptr, nullptr },
		{ StandardPath::Home, nullptr, "USERPROFILE", nullptr },
		{ StandardPath::Documents, nullptr, "USERPROFILE", "\\Documents" },
		{ StandardPath::Desktop, nullptr, "USERPROFILE", "\\Desktop" }
#else
		{ StandardPath::None, nullptr, nullptr, nullptr },
		{ StandardPath::Application, nullptr, nullptr, nullptr },
		{ StandardPath::Library, nullptr, nullptr, nullptr },
		{ StandardPath::Current, nullptr, "PWD", nullptr },
		{ StandardPath::Temporary, "/tmp", "TMPDIR", nullptr },
		{ StandardPath::Null, "/dev/null", nullptr, nullptr },
		{ StandardPath::Home, nullptr, "HOME", nullptr },
		{ StandardPath::Documents, nullptr, "HOME", "/Documents" },
		{ StandardPath::Desktop, nullptr, "HOME", "/Desktop" }
#endif
	};

	Progress<std::size_t> Filesystem::getNativeApplicationFile(char *buffer, std::size_t length) const noexcept
	{
		Progress<std::size_t> result(Action::Discover);
#if defined CIO_HAVE_WINDOWS_H
		// When passing null to GetModuleHandle, it returns handle of exe itself
		HMODULE hModule = ::GetModuleHandle(nullptr);

		if (!hModule)
		{
			result.fail(Reason::Missing);
		}
		else
		{
			int actualLength = ::GetModuleFileName(hModule, buffer, static_cast<DWORD>(length));
			int error = ::GetLastError();
			if (error == 0)
			{
				result.count = static_cast<std::size_t>(actualLength);
				result.succeed();

				if (static_cast<std::size_t>(actualLength) < length)
				{
					std::memset(buffer + actualLength, 0, length - static_cast<std::size_t>(actualLength));
				}
			}
			else if (actualLength == length)
			{
				result.count = CIO_UNKNOWN_SIZE;
				result.fail(Reason::Overflow);
			}
			else
			{
				result.fail(errorWin32(error));
			}
		}

#elif defined __linux__
		// proc/self/exe is a Linux-specific technique
		ssize_t linkBytesRead = ::readlink("/proc/self/exe", buffer, length);

		if (linkBytesRead > 0)
		{
			// ::readlink unfortunately doesn't distinguish between completely filling buffer and overflow
			if (linkBytesRead < length)
			{
				result.count = static_cast<std::size_t>(linkBytesRead);
				result.succeed();
				std::memset(buffer + linkBytesRead, 0, length - linkBytesRead);
			}
			else
			{
				result.count = CIO_UNKNOWN_SIZE;
				result.fail(Reason::Overflow);
			}
		}
		else
		{
			result.fail(error());
		}

#else
#error "No implementation for Filesystem::getApplicationFile()"
#endif
		return result;
	}

	Progress<std::size_t> Filesystem::getNativeLibraryDirectory(char *buffer, std::size_t length) const noexcept
	{
		Progress<std::size_t> result = getNativeStandardPath(StandardPath::Application, Resource::Directory, buffer, length);

#ifndef CIO_HAVE_WINDOWS_H
		// Backtrack to directory separator and rewrite bin with lib 
		std::size_t actual = result.count;
		while (actual > 0 && buffer[actual] != '/')
		{
			--actual;
		}
		std::memcpy(buffer + actual, "/lib", 5);
		result.count += (4 - actual);
#endif
		return result;
	}

	Progress<std::size_t> Filesystem::getNativeCurrentDirectory(char *buffer, std::size_t length) const noexcept
	{
		Progress<std::size_t> result;
#if defined CIO_HAVE_WINDOWS_H
		DWORD actualLength = ::GetCurrentDirectory(static_cast<DWORD>(length), buffer);
		if (actualLength == 0)
		{
			result.fail(Reason::Missing);
		}
		else if (actualLength > length)
		{
			result.count = actualLength;
			result.fail(Reason::Overflow);
		}
		else
		{
			result.count = actualLength;
			result.succeed();
			if (actualLength < length)
			{
				std::memset(buffer + actualLength, 0, length - actualLength);
			}
		}

#elif defined CIO_HAVE_UNISTD_H
		char *actual = ::getcwd(buffer, length);
		if (actual)
		{
			result.count = std::strlen(buffer) + 1;
			result.succeed();
			if (result.count < length)
			{
				std::memset(buffer + result.count, 0, length - result.count);
			}
		}
		else
		{
			int error = errno;
			if (error == ERANGE)
			{
				result.count = CIO_UNKNOWN_SIZE;
				result.fail(Reason::Overflow);
			}
			else
			{
				result.fail(cio::error(error));
			}
		}
#else
#error "No implementation for Filesystem::getCurrentDirectory()"
#endif
		return result;
	}

	State Filesystem::setNativeCurrentDirectory(const char *path) noexcept
	{
		State status;
#if defined CIO_HAVE_WINDOWS_H
		bool success = ::SetCurrentDirectory(path);

		if (success)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}

#else
		int success = ::chdir(path);
		if (success == 0)
		{
			status.succeed();
		}
		else
		{
			status.fail(error());
		}
#endif
		return status;
	}

	Progress<std::size_t> Filesystem::getNativeStandardPath(StandardPath path, Resource type, char *buffer, std::size_t length) const noexcept
	{
		Progress<std::size_t> result;

		switch (path)
		{
			case StandardPath::Application:
			{
				result = Filesystem::getNativeApplicationFile(buffer, length);

				if (type == Resource::Directory)
				{
					std::size_t actual = result.count;
					while (actual > 0 && buffer[actual] != '/' && buffer[actual] != '\\')
					{
						--actual;
					}

					if (actual < result.count)
					{
						std::memset(buffer + actual, 0, result.count - actual);
						result.count = actual;
					}
				}

				break;
			}

			case StandardPath::Library:
				result = Filesystem::getNativeLibraryDirectory(buffer, length);
				break;

			case StandardPath::Current:
				result = Filesystem::getNativeCurrentDirectory(buffer, length);
				break;

			default:
			{
				const StandardPathLookup &lookup = sPathLookup[static_cast<std::size_t>(path)];
				const char *base = lookup.initial;
				if (lookup.env)
				{
					const char *env = std::getenv(lookup.env);
					if (env)
					{
						base = env;
					}
				}

				if (base)
				{
					std::size_t baseLength = std::strlen(base);
					std::size_t baseToCopy = std::min(baseLength, length);

					if (baseToCopy > 0)
					{
						std::memcpy(buffer, base, baseToCopy);
					}

					result.update(baseToCopy, baseLength);

					if (lookup.suffix)
					{
						std::size_t addedLength = std::strlen(lookup.suffix);
						std::size_t totalLength = baseLength + addedLength;

						std::size_t suffixToCopy = std::min(addedLength, length - baseToCopy);


						if (suffixToCopy > 0)
						{
							std::memcpy(buffer + baseToCopy, lookup.suffix, suffixToCopy);
						}

						result.update(suffixToCopy, addedLength);
					}

					if (result.count < length)
					{
						std::memset(buffer + result.count, 0, length - result.count);
					}

					if (result.total <= length)
					{
						result.succeed();
					}
					else
					{
						result.fail(Reason::Overflow);
					}
				}
				else
				{
					result.fail(Reason::Missing);
				}

				break;
			}
		};

		return result;
	}


	State Filesystem::createNativeDirectory(const char *path) noexcept
	{
		State result(Action::Create);
#if defined CIO_HAVE_WINDOWS_H
		bool success = ::CreateDirectory(path, nullptr);

		if (success)
		{
			result.succeed();
		}
		else
		{
			result.fail(errorWin32());
		}

#elif defined CIO_HAVE_SYS_STAT_H
		int success = ::mkdir(path, S_IRWXU | S_IRWXG);

		if (success == 0)
		{
			result.succeed();
		}
		else
		{
			result.fail(error());
		}

#else
#error "No implementation for Filesystem::createDirectory(const char *path)"
#endif
		return result;
	}

	State Filesystem::createNativeFile(const char *path) noexcept
	{
		State status(Action::Create);
#if defined CIO_HAVE_WINDOWS_H
		HANDLE handle = ::CreateFile(path, GENERIC_READ | GENERIC_WRITE, 0, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, INVALID_HANDLE_VALUE);

		if (handle != INVALID_HANDLE_VALUE)
		{
			status.succeed();
			::CloseHandle(handle);
		}
		else
		{
			status.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int fd = ::open(path, O_WRONLY | O_CREAT | O_EXCL, S_IRWXU | S_IRWXG);

		if (fd != -1)
		{
			status.succeed();
			::close(fd);
		}
		else
		{
			status.fail(error());
		}

#else
#error "No implementation for FileFilesystem::createFile"
#endif
		return status;
	}

	State Filesystem::createNativeLink(const char *link, const char *target) noexcept
	{
		State state(Action::Create);

#if defined CIO_HAVE_WINDOWS_H
		bool result = ::CreateSymbolicLink(link, target, 0);
		if (result)
		{
			state.succeed();
		}
		else
		{
			state.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int result = ::symlink(target, link); // arguments are swapped for Unix symlink
		if (result == 0)
		{
			state.succeed();
		}
		else
		{
			state.fail(error());
		}
#endif

		return state;
	}

	Progress<std::size_t> Filesystem::readNativeLink(const char *link, char *buffer, std::size_t length) const noexcept
	{
		Progress<std::size_t> result(Action::Read);

		if (buffer)
		{
#if defined CIO_HAVE_WINDOWS_H
			HANDLE handle = ::CreateFile(link, GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_FLAG_OPEN_REPARSE_POINT, NULL);
			if (handle != INVALID_HANDLE_VALUE)
			{
				DWORD requested = static_cast<DWORD>(std::min<std::size_t>(std::numeric_limits<DWORD>::max(), length));
				DWORD actual = 0;
				bool success = ::ReadFile(handle, buffer, requested, &actual, nullptr);
				if (success)
				{
					if (actual < length)
					{
						result.count = actual;
						result.total = actual;
						result.succeed();

						std::memset(buffer + actual, 0, length - actual);
					}
					else
					{
						result.count = actual;
						result.total = CIO_UNKNOWN_SIZE;
						result.fail(Reason::Underflow);
					}
				}
				else
				{
					result.fail(errorWin32());
				}
			}
			else
			{
				result.fail(errorWin32());
			}

#elif defined CIO_HAVE_UNISTD_H
			if (length > 0)
			{
				ssize_t actual = ::readlink(link, buffer, length); // note: this silently truncates and does not set a null byte
				if (actual > 0)
				{
					// If the actual count is equal to the input length, we have no way of knowng if it was everything
					if (actual == length)
					{
						result.count = actual;
						result.total = CIO_UNKNOWN_SIZE;
						result.fail(Reason::Underflow);
					}
					else
					{
						result.count = actual;
						result.total = actual;
						std::memset(buffer + actual, 0, length - actual); // null terminate
						result.succeed();
					}
				}
				else
				{
					result.fail(error());
				}
			}
			else
			{
				result.total = CIO_UNKNOWN_SIZE;
				result.fail(Reason::Underflow);
			}
#else
			result.fail(Reason::Unsupported);
#endif
		}
		else
		{
			result.fail(Reason::Invalid);
		}

		return result;
	}

	State Filesystem::removeNativeFile(const char *path) noexcept
	{
		State status(Action::Remove);
#if defined CIO_HAVE_WINDOWS_H
		BOOL result = ::DeleteFile(path);

		if (result)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int result = ::unlink(path);

		if (result == 0)
		{
			status.succeed();
		}
		else
		{
			status.fail(error());
		}

#else // C89 method
		std::remove(path);
		status.succeed();
		// TODO capture errors
#endif
		return status;
	}

	State Filesystem::removeNativeDirectory(const char *path) noexcept
	{
		State status(Action::Remove);
#if defined CIO_HAVE_WINDOWS_H
		BOOL result = ::RemoveDirectory(path);

		if (result)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int result = ::rmdir(path);

		if (result == 0)
		{
			status.succeed();
		}
		else
		{
			status.fail(error());
		}

#else // C89 method
		std::remove(path);
		status.succeed();
#endif
		return status;
	}

	State Filesystem::removeNative(const char *path) noexcept
	{
		State result(Action::Remove);
		Resource type = Filesystem::getNativeType(path);
		if (type == Resource::File)
		{
			result = Filesystem::removeNativeFile(path);
		}
		else if (type == Resource::Directory)
		{
			result = Filesystem::removeNativeDirectory(path);
		}
		else if (type != Resource::None)
		{
			result.fail(Reason::Unsupported);
		}

		return result;
	}

	State Filesystem::pruneEmptyDirectories(const Path &dir, Traversal depth)
	{
		State stat = this->removeNativeDirectory(dir.toNativeFile().c_str());;

		if (depth > 0)
		{
			Path parent = dir.getParent();
			while (--depth && !stat.failed())
			{
				stat = this->removeNativeDirectory(parent.toNativeFile().c_str());
				parent = parent.getParent();
			}
		}

		return stat;
	}

	State Filesystem::moveNativeDirectory(const char *source, const char *destination) noexcept
	{
		State status(Action::Move);
#if defined CIO_HAVE_WINDOWS_H	
		BOOL result = ::MoveFileEx(source, destination, MOVEFILE_REPLACE_EXISTING);
		if (result)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}
#elif defined CIO_HAVE_UNISTD_H
		int success = ::rename(source, destination);

		if (success == 0)
		{
			status.succeed();
		}
		else
		{
			status.fail(error());
		}
#endif
		return status;
	}

	State Filesystem::copyNativeFile(const char *source, const char *dest) noexcept
	{
		State status(Action::Copy);
#if defined CIO_HAVE_WINDOWS_H				
		BOOL success = ::CopyFile(source, dest, FALSE);
		if (success)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}
#elif defined CIO_HAVE_UNISTD_H
		int fin = ::open(source, O_RDONLY);
		int fout = ::open(dest, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG);
		if (fin >= 0 && fout >= 0)
		{
			char block[4096];

			ssize_t in = ::read(fin, block, 4096);
			while (in > 0)
			{
				ssize_t out = ::write(fout, block, in);
				if (out < in)
				{
					status.fail(error());
					break;
				}

				in = ::read(fin, block, 4096);
			}

			if (in < 0)
			{
				status.fail(error());
			}
		}
		else
		{
			status.fail(error());
		}

		if (!status.failed())
		{
			status.succeed();
		}

		if (fin >= 0)
		{
			::close(fin);
		}

		if (fout >= 0)
		{
			::close(fout);
		}
#endif
		return status;
	}

	State Filesystem::moveNativeFile(const char *source, const char *dest) noexcept
	{
		State status(Action::Move);
#if defined CIO_HAVE_WINDOWS_H
		BOOL result = ::MoveFileEx(source, dest, MOVEFILE_REPLACE_EXISTING);
		if (result)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int success = ::rename(source, dest);

		if (success == 0)
		{
			status.succeed();
		}
		else
		{
			status.fail(error());
		}
#endif
		return status;
	}

	State Filesystem::moveOrCopyNativeFile(const char *source, const char *dest) noexcept
	{
		State status(Action::Move);
#if defined CIO_HAVE_WINDOWS_H
		BOOL result = ::MoveFileEx(source, dest, MOVEFILE_REPLACE_EXISTING | MOVEFILE_COPY_ALLOWED);
		if (result)
		{
			status.succeed();
		}
		else
		{
			status.fail(errorWin32());
		}

#elif defined CIO_HAVE_UNISTD_H
		int success = ::rename(source, dest);

		if (success == 0)
		{
			status.succeed();
		}
		else
		{
			int error = errno;

			// happens if the source and target paths are on different file systems, used by moveOrCopy to fall back to copy + delete
			if (error == EXDEV)
			{
				status = Filesystem::copyNativeFile(source, dest);
				if (status.succeeded())
				{
					Filesystem::removeNativeFile(source);
				}
			}
			else
			{
				status.fail(cio::error(error));
			}
		}
#endif
		return status;
	}

	Metadata Filesystem::getNativeMetadata(const char *path) const
	{
		Metadata result;

		Resource type = Filesystem::getNativeType(path);
		ModeSet modes = Filesystem::getModesForType(type, ModeSet::all());
		result.setType(type);
		result.setMode(modes, true);
		result.setLength(this->getNativeFileSize(path));

		return result;
	}

	Response<Length> Filesystem::getNativeFileSize(const char *path) const noexcept
	{
		Response<Length> result;

#if defined CIO_HAVE_WINDOWS_H
		WIN32_FILE_ATTRIBUTE_DATA info;
		BOOL success;
		success = ::GetFileAttributesEx(path, GetFileExInfoStandard, &info);

		if (success)
		{
			std::uint64_t ulo = static_cast<std::uint64_t>(info.nFileSizeLow);
			std::uint64_t uhi = static_cast<std::uint64_t>(info.nFileSizeHigh) << 32;
			result.receive(Length(uhi | ulo));
		}
		else
		{
			int error = ::GetLastError();
			// TODO map errors (not documented in MSDN)
			result.fail(errorWin32(error));
		}

#elif defined CIO_HAVE_SYS_STAT_H
		struct stat buffer = { };
		int e = ::stat(path, &buffer);

		if (e != -1)
		{
			if (S_ISREG(buffer.st_mode) || S_ISLNK(buffer.st_mode))
			{
				result.receive(Length(buffer.st_size));
			}
			else
			{
				result.fail(Reason::Unsupported);
			}
		}
		else
		{
			result.fail(error());
		}

#else
		// TODO: generic implementation using file::Channel?
#error "No implementation for Filesystem::getFileSize"
#endif

		return result;
	}

	Resource Filesystem::getNativeType(const char *path) const noexcept
	{
		Resource type = Resource::Unknown;
#if defined CIO_HAVE_WINDOWS_H
		DWORD attrs = ::GetFileAttributes(path);

		if (attrs != INVALID_FILE_ATTRIBUTES)
		{
			if (attrs & FILE_ATTRIBUTE_DIRECTORY)
			{
				type = Resource::Directory;
			}
			else if (attrs & FILE_ATTRIBUTE_DEVICE)
			{
				type = Resource::Device;
			}
			else
			{
				type = Resource::File;
			}
		}
		else
		{
			type = Resource::None;
		}

#elif defined CIO_HAVE_SYS_STAT_H
		struct stat buffer = { };
		int result = ::stat(path, &buffer);

		if (result != -1)
		{
			if (S_ISDIR(buffer.st_mode))
			{
				type = Resource::Directory;
			}
			else if (S_ISBLK(buffer.st_mode) ||
				S_ISCHR(buffer.st_mode) ||
				S_ISFIFO(buffer.st_mode))
			{
				type = Resource::Device;
			}
			else if (S_ISREG(buffer.st_mode))
			{
				type = Resource::File;
			}
		}
		else
		{
			int error = errno;

			if (error == ENOENT || error == ENOTDIR)
			{
				type = Resource::None;
			}
		}

#else
#error "No implementation for Filesystem::getResourceTypeForPath"
#endif
		return type;
	}

	ModeSet Filesystem::getNativeModes(const char *path) const noexcept
	{
		ModeSet modes;
#if defined CIO_HAVE_WINDOWS_H
		DWORD attrs = ::GetFileAttributes(path);

		if (attrs != INVALID_FILE_ATTRIBUTES)
		{
			modes.insert(Mode::Read);

			if ((attrs & FILE_ATTRIBUTE_READONLY) == 0)
			{
				modes.insert(Mode::Write);
			}
		}
		else
		{
			// TODO validate that this is a valid enough path to possibly create
			modes.insert(Mode::Create);
		}

#elif defined CIO_HAVE_SYS_STAT_H
		struct stat buffer = { };
		int result = ::stat(path, &buffer);

		if (result != -1)
		{
			// TODO validate this is actually true for the active user
			modes.insert(Mode::Read);
			modes.insert(Mode::Write);
		}
		else
		{
			int error = errno;

			if (error == ENOENT)
			{
				modes.insert(Mode::Create);
			}
		}

#else
#error "No implementation for Filesystem::getResourceModesForPath"
#endif
		return modes;
	}

	ModeSet Filesystem::getModesForType(Resource type, ModeSet desired) const noexcept
	{
		ModeSet actual = desired;

		if (desired.count(Mode::Load))
		{
			if (type != Resource::None)
			{
				actual.erase(Mode::Create);
			}
		}

		if (desired.count(Mode::Create))
		{
			if (type == Resource::None)
			{
				actual.erase(Mode::Load);
			}
		}

		return actual;
	}

	bool Filesystem::existsNative(const char *path) const noexcept
	{
		return this->getNativeType(path) != Resource::None;
	}

	bool Filesystem::isNativeFile(const char *path) const noexcept
	{
		return this->getNativeType(path) == Resource::File;
	}

	bool Filesystem::isNativeDirectory(const char *path) const noexcept
	{
		return this->getNativeType(path) == Resource::Directory;
	}
}
