/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Reporter.h"

#include "Fault.h"
#include "Test.h"

#include <cio/Class.h>
#include <cio/Severity.h>

#include <sstream>

namespace cio
{
	namespace test
	{
		const Class<Reporter> Reporter::sMetaclass("cio::test::Reporter");

		const Metaclass &Reporter::getDeclaredMetaclass()
		{
			return sMetaclass;
		}

		Reporter::Reporter() :
			Logger("cio::test::Reporter"),
			mOutput(nullptr)
		{
			// nothing more to do
		}

		Reporter::~Reporter() noexcept = default;

		const Metaclass &Reporter::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Reporter::clear() noexcept
		{
			// do nothing in base class
		}

		void Reporter::start(const Test &test)
		{
			this->logIfEnabled(test.getName().c_str(), Severity::Info, "Started");
		}

		void Reporter::complete(const Test &test, Severity severity)
		{
			bool passed = severity < Severity::Error;
			const char *text = passed ? "Completed (Passed)" : "Completed (Failed)";
			this->logIfEnabled(test.getName().c_str(), severity, text);
		}

		void Reporter::fail(const Test &test, const Fault &fault)
		{
			std::ostringstream stream;
			stream << "Fault (Failed)\n";
			stream << "\tFault at " << fault.getSource() << '\n';
			stream << '\t' << fault.getMessage() << '\n';
			std::string message = stream.str();
			this->logIfEnabled(test.getName().c_str(), fault.getSeverity(), message.c_str());
		}

		void Reporter::skip(const Test &test, Severity severity)
		{
			this->logIfEnabled(test.getName().c_str(), severity, "Skipped");
		}

		bool Reporter::log(const char *context, Severity severity, const char *message)
		{
			return mOutput ? mOutput->logIfEnabled(context, severity, message) : Logger::log(context, severity, message);
		}

		void Reporter::setOutput(Logger *logger)
		{
			mOutput = logger;
		}

		Logger *Reporter::getOutput()
		{
			return mOutput;
		}
	}
}
