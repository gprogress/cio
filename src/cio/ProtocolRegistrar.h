/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_PROTOCOLREGISTRAR_H
#define CIO_PROTOCOLREGISTRAR_H

#include "Types.h"

#include "ProtocolFactory.h"
#include "Text.h"

namespace cio
{
	/**
	 * The Protocol Factory Registrar is a helper to automatically register and unregister a protocol from
	 * a Protocol Factory in a particular scope.
	 *
	 * Typically it is used at global static scope to bind protocols to the default protocol factory on library load.
	 *
	 * @tparam <T> The subclass of cio::Protocol to register
	 */
	template <typename T>
	class ProtocolRegistrar
	{
		public:
			/**
			 * Construct a protocol registration using static methods expected to be present.
			 */
			inline explicit ProtocolRegistrar(ProtocolFactory *factory = ProtocolFactory::getDefault()) noexcept;

			/** 
			 * Construct a protocol registration using a global default instance of the protocol.
			 * 
			 * @param protocol The default protocol instance
			 * @param factory The factory to register with
			 */
			inline explicit ProtocolRegistrar(T *protocol, ProtocolFactory *factory = ProtocolFactory::getDefault()) noexcept;

			/**
			 * Unregisters the protocol from the factory it was registered with.
			 */
			inline ~ProtocolRegistrar() noexcept;
			
			/**
			 * Rebinds this protocol registrar to a protocol factory.
			 * The existing registration, if any, is removed and then a new registration is performed.
			 *
			 * @param prefixes The protocol prefixes
			 * @param extensions The protocol extensions
			 * @param factory The factory to bind to
			 */
			inline void bind(ProtocolFactory *factory);
			
			/**
			 * Unregisters the protocol from the factory it was registered with.
			 */
			inline void clear() noexcept;
			
		private:
			T *mProtocol;
			
			
			/** The factory that this registrar used */
			ProtocolFactory *mFactory;
	};
}

/* Inline implementation */

namespace cio
{
	template <typename T>
	inline ProtocolRegistrar<T>::ProtocolRegistrar(ProtocolFactory *factory) noexcept :
		mProtocol(nullptr),
		mFactory(factory)
	{
		if (mFactory)
		{
			mFactory->addFilterList(T::getClassProtocolFilter());
		}
	}

	template <typename T>
	inline ProtocolRegistrar<T>::ProtocolRegistrar(T *protocol, ProtocolFactory *factory) noexcept :
		mProtocol(protocol),
		mFactory(factory)
	{
		if (mProtocol && mFactory)
		{
			mFactory->addFilterList(mProtocol->getFilterList());
		}
	}	

	template <typename T>
	inline ProtocolRegistrar<T>::~ProtocolRegistrar() noexcept
	{
		if (mFactory)
		{
			// TODO figure this one out
		}
	}
	
	template <typename T>
	inline void ProtocolRegistrar<T>::bind(ProtocolFactory *factory)
	{
		mFactory = factory;
		if (mProtocol && mFactory)
		{
			mFactory->addFilterList(mProtocol->getFilterList());
		}
	}
	
	template <typename T>
	inline void ProtocolRegistrar<T>::clear() noexcept
	{
		if (mFactory)
		{
			mFactory = nullptr;
		}

		mProtocol = nullptr;
	}
}

#endif

