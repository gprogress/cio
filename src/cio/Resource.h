/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_RESOURCE_H
#define CIO_RESOURCE_H

#include "Types.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * Enumeration of overall resource category logical types.
	 */
	enum class Resource : std::uint8_t
	{
		/** No resource */
		None,

		/** Resource is a top-level protocol */
		Protocol,

		/** Resource is a regular file with content */
		File,

		/** Resource is a directory containing other resources */
		Directory,

		/** Resource is an archive file that may contain other resources */
		Archive,

		/** Resource is a low-level device */
		Device,

		/** Resource is a redirect of some kind such as a symbolic link or a URL redirect */
		Link,

		/** Resource is an abstraction of a network connection as a whole */
		Connection,

		/** Resource is an unformatted block of memory */
		Memory,

		/** Resource is a virtual data provider by some algorithm */
		Virtual,

		/** Resource type is unknown but may be possible to determine later */
		Unknown
	};

	/**
	 * Gets a text representation of the Resource enumeration.
	 *
	 * @param mode The type
	 * @return the text for the type
	 */
	CIO_API const char *print(Resource mode) noexcept;

	/**
	 * Parses a null-terminated C string to get the matching type.
	 * If the text does not match any known type, Resource::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed type
	 */
	CIO_API Resource parseType(const char *text) noexcept;

	/**
	 * Parses a text string to get the matching type.
	 * If the text does not match any known type, Resource::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed type
	 */
	CIO_API Resource parseType(const std::string &text) noexcept;

	/**
	 * Prints the text for a category type to a C++ stream.
	 *
	 * @param stream The C++ stream
	 * @param mode The type to print
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, Resource mode);

	/**
	 * Parses text from a C++ stream to obtain the matching type.
	 * If the text does not match any known type, Resource::Unknown is set.
	 *
	 * @param stream The C++ stream
	 * @param mode The reference to the type to store the parse results into
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::istream &operator>>(std::istream &stream, Resource &mode);
}

#endif
