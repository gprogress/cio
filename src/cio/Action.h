/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ACTION_H
#define CIO_ACTION_H

#include "Types.h"

#include "Enumeration.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Action enumeration describes logical events representing actions in the I/O architecture.
	 * It is primarily used as a component of the Progress, Event, and Response structures.
	 */
	enum class Action : std::uint8_t
	{
		// Default state

		/** No action performed */
		None,

		// Actions for general process execution

		/** Prepare to perform an execution step */
		Prepare,

		/** Execute a process of some kind */
		Execute,

		/** Performs post-execution cleanup steps */
		Complete,
		
		// Data actions on single resources
		
		/** Read data from an input */
		Read,
		
		/** Write data to an output */
		Write,
		
		/** Read, modify, and write data to a resource */
		Edit,
		
		/** Receive an asynchronous update from a resource */
		Notify,
		
		/** Acknowledge receipt of an asynchronous update from a resource */
		Acknowledge,
		
		// Metadata actions on single resources
		
		/** Increase size of a resource */
		Grow,
		
		/** Descrease size of a resource */
		Shrink,
		
		/** Resize a resource in general */
		Resize,

		/** Change a seek position on a resource */
		Seek,
		
		/** Inquiry on current state of metadata */
		Inquire,
		
		/** Change other metadata on a resource such as permissions */
		Metadata,
		
		/** Subscribe for asynchronous updates */
		Subscribe,
		
		/** Unsubscribe from asynchrnous updates */
		Unsubscribe,
		
		// Actions regarding resource existence
			
		/** Create a new resource */
		Create,
		
		/** Open an existing resource */ 
		Open,
		
		/** Closes an existing resource */
		Close,
		
		/** Removes an existing resource */
		Remove,
		
		/** Renames a resource */
		Rename,
		
		/** Connects to a remote resource */
		Connect,
		
		/** Disconnects from a remote resource */
		Disconnect,
		
		/** Scans to find what resources may exist */
		Scan,
		
		/** Discovers the existence of a resource */
		Discover,
	
		// Actions regarding multiple resources
		
		/** Copies content from one resource to another */
		Copy,
		
		/** Moves content from one resource to another */
		Move
	};

	/**
	 * Specializes Enumeration for the C++ Action enum.
	 */
	template <>
	class CIO_API Enumeration<Action> final : public Enumerants<Action>
	{
		public:
			/** The array of enum values */
			static const Enumerant<Action> values[];

			/** The enumeration global instance */
			static const Enumeration<Action> instance;

			/** Constructor */
			Enumeration();

			/** Destructor */
			virtual ~Enumeration() noexcept override;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
