/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_ADDRESS_H
#define CIO_NET_ADDRESS_H

#include "Types.h"

#include "IPv4.h"
#include "IPv6.h"

#include <iosfwd>
#include <string>

namespace cio
{
	namespace net
	{
		/**
		 * The Address class provides an encapsulation of representing a particular internet service or address as used
		 * by the native host OS. This structure is set up for maximum performance with the native OS APIs since it
		 * gets used a lot by underlying Socket calls.
		 *
		 * It is optimized for the common case of IPv4 and IPv6 where the address is a small structure that can fit in 32 bytes.
		 * However, it can also represent larger addresses for other protocols as needed through a dynamic allocation.
		 */
		class CIO_NET_API Address
		{
			public:
				/**
				 * Construct an empty address.
				 */
				Address() noexcept;

				/**
				 * Construct an address using the given native OS address buffer.
				 *
				 * @param data The data to copy
				 * @param len The number of bytes to copy
				 * @throw std::bad_alloc If the address was large enough to require dynamic allocation but that failed
				 */
				Address(const void *data, std::size_t len);

				/**
				 * Construct an address for an IPv4.
				 *
				 * @param ip The IP
				 * @param port The port
				 */
				Address(IPv4 ip, std::uint16_t port = 0) noexcept;

				/**
				 * Construct an address for an IPv6.
				 *
				 * @param ip The IP
				 * @param port The port
				 */
				Address(IPv6 ip, std::uint16_t port = 0)  noexcept;

				/**
				 * Construct a copy of an address.
				 * If the address is large enough to require dynamic allocation, a new address buffer is allocated and copied.
				 *
				 * @param in The address to copy
				 * @throw std::bad_alloc If a new address allocation fails for large address buffers
				 */
				Address(const Address &in);

				/**
				 * Construct an address by moving the contents of an existing address.
				 *
				 * @param in The address to move
				 */
				Address(Address &&in) noexcept;

				/**
				 * Copy an address.
				 * If the address is large enough to require dynamic allocation, a new address buffer is allocated and copied.
				 *
				 * @param in The address to copy
				 * @return this address
				 * @throw std::bad_alloc If a new address allocation fails for large address buffers
				 */
				Address &operator=(const Address &in);

				/**
				 * Move an address.
				 *
				 * @param in The address to move
				 * @return this address
				 */
				Address &operator=(Address &&in) noexcept;

				/**
				 * Destructor. Deallocates the address buffer if it was large enough for dynamic allocation.
				 */
				~Address() noexcept;

				/**
				 * Clears the address, sets the length to 0, and deallocates the address buffer if it was large enough for dynamic allocation.
				 */
				void clear() noexcept;

				/**
				 * Gets the native OS address family of this address.
				 *
				 * @return the native OS address family
				 */
				AddressFamily getAddressFamily() const noexcept;

				/**
				 * Gets the port assigned to the current address.
				 * If this is an IP protocol, the current port is returned.
				 * Otherwise, the invalid port 0 is returned.
				 *
				 * @return the port of this address
				 */
				std::uint16_t getPort() const noexcept;

				/**
				 * Sets the port assigned to the current address.
				 * If this is an IP protocol, the current port is updated to the specified port.
				 * Otherwise, no change is made.
				 *
				 * @param port The port to set for this address
				 * @return whether this operation had an effect
				 */
				bool setPort(std::uint16_t port) noexcept;

				/**
				 * Checks whether this address has a port set.
				 * This is true if this address is for an IP protocol and the port is greater than 0.
				 *
				 * @return whether this address has a port
				 */
				bool hasPort() const noexcept;

				/**
				 * Sets this address to represent the given IPv4 address.
				 *
				 * @param ip The IP
				 * @param port The port
				 */
				void setIP4(IPv4 ip, std::uint16_t port = 0) noexcept;

				/**
				 * Sets this address to represent the given IPv6 address.
				 *
				 * @param ip The IP
				 * @param port The port
				 */
				void setIP6(IPv6 ip, std::uint16_t port = 0) noexcept;

				/**
				 * Gets the IPv4 address that this address represents.
				 * If it is not an IPv4 address, the default "any" address is returned.
				 *
				 * @return the IPv4 address
				 */
				IPv4 getIP4() const noexcept;

				/**
				 * Gets the IPv6 address that this address represents.
				 * If it is not an IPv6 address, the default "any" address is returned.
				 *
				 * @return the IPv6 address
				 */
				IPv6 getIP6() const noexcept;

				/**
				 * Checks whether this address is an IP-based address.
				 * If so it will return 4 for IPv4 or 6 for IPv6.
				 * If not, it will return 0.
				 *
				 * @return the IP version, or 0 if not an IP
				 */
				unsigned isIP() const noexcept;

				/**
				 * Checks whether this address is an IPv4 address.
				 *
				 * @return whether the address is IPv4
				 */
				bool isIP4() const noexcept;

				/**
				 * Checks whether this address is an IPv6 address.
				 *
				 * @return whether the address is IPv6
				 */
				bool isIP6() const noexcept;

				/**
				 * Checks whether this address is the IPv4 or IPv6 "any" wildcard address.
				 * The IP version is returned if so, otherwise 0 is returned.
				 * 
				 * @return the IP version of the "any" address, or 0 if this is not the "any" address
				 */
				unsigned isAnyIP() const noexcept;
				
				/**
				 * Checks whether this adddress is the canonical IPv4 "broadcast" address.
				 * There is no IPv6 equivalent.
				 *
				 * @return whether the given address is the IPv4 canonical broadcast address
				 */
				bool isBroadcast() const noexcept;
				
				/**
				 * Checks whether the address supports the given flow type.
				 * For IPv4 and IPv6, this is delegated to the appropriate classes.
				 * All other types are assumed to only support Unicast.
				 * If no address is set, this returns false.
				 *
				 * @param flow The flow type
				 * @return whether the flow type is supported by this address type
				 */
				bool supports(Flow flow) const noexcept;
				
				/**
				 * Prints the most conventional text representation of the address.
				 * If this is an address family not known to CIO, this will print the data as a hexadecimal array.
				 *
				 * @return the text representation of the address
				 */
				std::string print() const;

				/**
				 * Attempts to parse the given address to see if it is a known format.
				 * If it matches a format for IPv4 or IPv6, it will update the address bytes accordingly.
				 * The IP text format may optionally include a port number after a ':' which will be detected too.
				 * Note that if a port is specified for IPv6, it must use the [] delimiters too.
				 * 
				 * @param text The text to parse
				 * @return the parse status
				 */
				TextParseStatus parse(const Text &text) noexcept;

				/**
				 * Sets the address to represent a copy of the given native OS address bytes.
				 *
				 * @param data The address buffer
				 * @param length The address length
				 */
				void set(const void *data, std::size_t length);

				/**
				 * Gets the native address buffer that can be used for OS calls.
				 *
				 * @return the native address buffer
				 */
				void *data() noexcept;

				/**
				 * Gets the native address buffer that can be used for OS calls.
				 *
				 * @return the native address buffer
				 */
				const void *data() const noexcept;

				/**
				 * Gets the number of bytes in the native address buffer that can be used for OS calls.
				 *
				 * @return the size of the native address buffer
				 */
				std::size_t size() const noexcept;

				/**
				 * Checks to see if the address is empty.
				 * 
				 * @return whether the address is empty
				 */
				bool empty() const noexcept;

				/**
				 * Gets whether any address is set.
				 *
				 * @return whether an address was set
				 */
				explicit operator bool() const noexcept;

			private:
				/** Direct pointer to data, either to mBytes or to allocated data */
				std::uint8_t *mData;

				/** Bytes for storing common case of IP and similar networks where address is <= 32 bytes */
				std::uint8_t mBytes[32];

				/** Number of bytes in address */
				std::size_t mLength;
		};

		/**
		 * Prints an Address to a C++ stream in the most human-readable way.
		 * This will print an IPv4 address with port or an IPv6 address with port if the address is an IP address.
		 * Otherwise it will print a hexadecimal digit sequence representing the bytes of the address.
		 *
		 * @param stream The C++ stream
		 * @param address The address to print
		 * @return the stream
		 */
		CIO_NET_API std::ostream &operator<<(std::ostream &stream, const Address &address);
	}
}

#endif

