/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_TYPEID_H
#define CIO_BLUETOOTH_TYPEID_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Type ID enumeration represent standard 16-bit Bluetooth short UUID.
		 */
		enum class TypeId : std::uint16_t
		{

			/** Invalid / no type UUID set */
			None =					0x0000,
			
			/** Service for the Generic Access Profile (GAP) */
			GAP =					0x1800,
			
			/** Root service for the GATT data */
			GATT =					0x1801,
			
			/** Primary service declaration */
			PrimaryService = 			0x2800,
			
			/** Secondary service declaration */
			SecondaryService = 			0x2801,
			
			/** Included service declaration for a service */
			Include = 				0x2802,
			
			/** Characteristic declaration */
			Characteristic =			0x2803,
			
			/** Characteristic extended properties */
			CharacteristicExtendedProperties = 	0x2900,
			
			/** Characteristic user description */
			CharacteristicUserDescription =		0x2901,
			
			/** Client Characteristic Configuration Descriptor (CCCD) which is used to set whether to enable notifications/indications */
			ClientCharacteristicConfiguration =	0x2902,
							
			/** Server Characteristic Configuration Descriptor (SCCD) */
			ServerCharacteristicConfiguration =	0x2903,
			
			/** Characteristic presentation format */
			CharacteristicFormat =			0x2904,
			
			/** Characteristic data type sequence for complex types */
			CharacteristicAggregateFormat =		0x2905,
			
			/** Valid data range */
			ValidRange =				0x2906,
			
			/** Characteristic for device name text */
			DeviceName =				0x2A00,
			
			/** Characteristic for device appearance category enumeration */
			Appearance =				0x2A01,
			
			/** Characteristic to notify/indicate when a service declaration has changed */
			ServiceChanged =			0x2A05,	
			
			/** Characteristic for central address resolution */
			CentralAddressResolution =		0x2AA6	
		};
		
		/**
		 * Parses the given text to get the Bluetooth TypeId that matches it.
		 * If no name matches, the output is set to TypeId::None. 
		 *
		 * @param text The text
		 * @param id The output Bluetooth TypeId to set
		 * @return whether the name matched any known ID
		 */
		CIO_BLUETOOTH_API bool parse(const Text &text, TypeId &id) noexcept;
		
		/**
		 * Prints the given Bluetooth TypeId to its standard name per the Bluetooth specification.
		 * If the ID is unrecognized (either not a valid value, or this code does not yet represent it) then
		 * the static string "Unknown" is returned.
		 *
		 * @param id The type ID
		 * @return the text representing the type ID
		 */
		CIO_BLUETOOTH_API const char *print(TypeId id) noexcept;
		
		/**
		 * Prints the given type ID to the given C++ stream.
		 * If the ID is unrecognized (either not a valid value, or this code does not yet represent it) then
		 * the static string "Unknown" is printed.
		 *
		 * @param stream The stream
		 * @param id The type ID
		 * @return the stream
		 * @throw std::exception If the underlying C++ stream throws an exception on write
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, TypeId id);
	}
}

#endif

