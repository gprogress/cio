/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_EVENT_H
#define CIO_BLUETOOTH_EVENT_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Event enumeration represents the standard codes for Bluetooth Controller events that can be
		 * received from the controller interface.
		 */
		enum class Event : std::uint8_t
		{
			/** No event was set */
			None = 0x00,
			
			/** A classic inquiry to discover devices was completed */
			InquiryCompleted = 0x01,
			
			/** A classic inquiry advertising result for a peripheral */
			InquiryResult = 0x02,
			
			/** A connection was established */
			Connected = 0x03,
			
			/** A connection was requested by the remote central device */
			ConnectionRequested = 0x04,
			
			/** A disconnection request was completed */
			Disconnected = 0x05,
			
			/** An authentication request was completed */
			Authenticated = 0x06,
			
			/** A remote name request was completed */
			RemoteNameReceived = 0x07,
			
			/** A change occurred in the encryption status */
			EncryptionChanged = 0x08,
			
			/** The conneciton link key change was completed */
			ConnectionLinkKeyChanged = 0x09,
			
			/** The master link key request was completed */
			MasterLinkKeyCompleted = 0x0A,
			
			/** The read remote features request completed */
			RemoteFeaturesRead = 0x0B,
			
			/** The read remote version request completed */
			RemoteVersionRead = 0x0C,
			
			/** The Quality of Service setup request completed */
			QualityOfServiceSetup = 0x0D,
			
			/** A command was completed */
			CommandCompleted = 0x0E,
			
			/** Status update for a command */
			CommandStatus = 0x0F,
			
			/** A hardware error occurred */
			HardwareError = 0x10,
			
			/** the device was flushed */
			Flushed = 0x11,
		
			/** The device role (central vs. peripheral) changed */
			RoleChanged = 0x12,
			
			/** The number of completed packets is provided */
			CompletedPacketCount = 0x13,
			
			/** A device mode changed */
			ModeChanged = 0x14,
			
			/** Link keys provided in response to a request */
			LinkKeysReturned = 0x15,
			
			/** A PIN code was requested for authentication */
			PINCodeRequested = 0x16,
			
			/** A link key was requested for authentication */
			LinkKeyRequested = 0x17,
			
			/** A link key was provided for authentication */
			LinkKeyProvided = 0x18,
			
			/** Data provided through loopback */
			Loopback = 0x19,
			
			/** An internal buffer overflow occurred on the device */
			DataBufferOverflow = 0x1A,
			
			/** The max number of internal radio slots changed */
			MaxSlotsChanged = 0x1B,
			
			/** The clock offset was read */
			ClockOffsetRead = 0x1C,
			
			/** The connection packet type changed */
			ConnectionPacketTypeChanged = 0x1D,
			
			/** A quality of service violation occurred */
			QualityOfServiceViolation = 0x1E,
			
			/** The page scan mode was changed */
			PageScanModeChanged = 0x1F,
			
			/** The page scan repetition mode was changed */
			PageScanRepetitionModeChanged = 0x20,
			
			/** Flow control parameters were specified */
			FlowControlSpecified = 0x21,
			
			/** The classic inquiry result was returned with RSSI information */
			InquiryResultWithRSSI = 0x22,
			
			/** The extended features were read from the remote device */
			RemoteExtendedFeaturesRead = 0x23,
			
			// Gap - no codes for 0x24 through 0x2B
			
			/** A synchronous connection was established */
			SynchronousConnectionCompleted = 0x2C,
			
			/** The parameters of a synchronous connection changed */
			SynchronousConnectionChanged = 0x2D,
			
			/** A "sniff subrating" event occurred to renegotiate keep-alive heartbeat time */
			SniffSubrating = 0x2E,
			
			/** The result of an extended inquiry is provided */
			ExtendedInquiryResult = 0x2F,
			
			/** The encryption key was updated */
			EncryptionKeyRefreshed = 0x30,
			
			/** An I/O capability as requested */
			IOCapabilityRequested = 0x31,
			
			/** The device provided a requested I/O capability */
			IOCapabilityProvided = 0x32,
			
			/** The pairing requested the user to confirm a yes/no choice to proceed */
			UserConfirmationRequested = 0x33,
			
			/** The pairing requested the user to enter a passkey */
			UserPasskeyRequested = 0x34,
			
			/** The pairing requested out-of-band (OOB) data for authentication */
			OutOfBandDataRequested = 0x35,
			
			/** Simple pairing completed */
			SimplePairingCompleted = 0x36,
			
			// Gap - no code for 0x37
			
			/** The link supervision timeout for a low-energy connection changed */
			LinkSupervisionTimeoutChanged = 0x38,
			
			/** An enhanced flush operation completed */
			EnhancedFlushCompleted = 0x39,
			
			// Gap - no code for 0x3A
			
			/** The user provided a passkey during pairing */
			UserPasskeyProvided = 0x3B,
			
			/** The user confirmed a request during pairing */
			KeypressProvided = 0x3C,
			
			/** The peripheral responded with the low-energy features */
			RemoteLowEnergyFeaturesProvided = 0x3D,
			
			/** A low-energy event occurred, with details specified by the LowEnergyEvent */
			LowEnergy = 0x3E
		};
		
		/**
		 * Gets a human readable text representation of an HCI Event code.
		 *
		 * @param type The HCI event code
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(Event type) noexcept;

		/**
		 * Prints a human readable text representation of an HCI Event code to a C++ stream.
		 *
		 * @param s The stream
		 * @param type The HCI Event code
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, Event type);
	}
}

#endif

