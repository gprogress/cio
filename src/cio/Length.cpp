/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Length.h"

#include <cstring>
#include <ostream>

namespace cio
{
	std::size_t Length::print(char *buffer, std::size_t length) const noexcept
	{
		std::size_t needed = 0;
		char temp[8];
		char *next = temp;
		const char *text = temp;

		if (!this->known())
		{
			text = "Unknown";
			needed = 7;
		}
		else
		{
			std::uint64_t absolute = std::abs(this->value);
			if (this->value < 0)
			{
				*next++ = '-';
			}

			if (absolute < 10)
			{
				*next++ = static_cast<char>(absolute) + '0';
			}
			else if (absolute < 100)
			{
				*next++ = static_cast<char>(absolute / 10) + '0';
				*next++ = static_cast<char>(absolute) + '0';
			}
			else if (absolute < 1000)
			{
				*next++ = static_cast<char>(absolute / 100) + '0';
				*next++ = static_cast<char>(absolute / 10) + '0';
				*next++ = static_cast<char>(absolute) + '0';
			}
			// Large numbers - track to 3 digits
			else
			{
				unsigned shift = 0;
				std::uint64_t current = absolute;
				while (current > 1000)
				{
					current /= 10;
					++shift;
				}

				const char *suffix = "kMGTPE";

				unsigned unit = shift / 3;
				unsigned dec = 3 - shift % 3;

				switch (dec)
				{
					case 1:
						*next++ = static_cast<char>(current / 100) + '0';
						*next++ = '.';
						*next++ = static_cast<char>(current / 10) + '0';
						*next++ = static_cast<char>(current) + '0';
						break;

					case 2:
						*next++ = static_cast<char>(current / 100) + '0';
						*next++ = static_cast<char>(current / 10) + '0';
						*next++ = '.';
						*next++ = static_cast<char>(current) + '0';
						break;

					default:
						*next++ = static_cast<char>(current / 100) + '0';
						*next++ = static_cast<char>(current / 10) + '0';
						*next++ = static_cast<char>(current) + '0';
				}

				*next++ = ' ';
				*next++ = suffix[unit];
			}

			needed = next - temp;
		}

		if (buffer && length > 0)
		{
			std::memcpy(buffer, text, std::min(needed, length));

			if (needed < length)
			{
				std::memset(buffer, 0, length - needed);
			}
		}

		return needed;
	}

	std::ostream &operator<<(std::ostream & stream, const Length & length)
	{
			char temp[8] = { };
			std::size_t actual = length.print(temp, 8);
			stream.write(temp, actual);
			return stream;
	}
}
