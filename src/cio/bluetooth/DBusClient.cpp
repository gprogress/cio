/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DBusClient.h"

#include <cio/Buffer.h>
#include <cio/Exception.h>
#include <cio/Class.h>
#include <cio/Parse.h>
#include <cio/Status.h>
#include <cio/Task.h>
#include <cio/Variant.h>

#include <cio/bluetooth/UniqueId.h>

#include <dbus/dbus.h>

namespace cio
{
	namespace bluetooth
	{
		Class<DBusClient> DBusClient::sMetaclass("cio::bluetooth::DBusClient");
		
		const char *DBusClient::DBUS_DOMAIN = "org.freedesktop.DBus";
		
		const char *DBusClient::DBUS_OBJECTMANAGEMENT_IFACE = "org.freedesktop.DBus.ObjectManager";
			
		const char *DBusClient::DBUS_PROPERTIES_IFACE = "org.freedesktop.DBus.Properties";
		
		std::atomic<unsigned> DBusClient::sThreaded;
		
		std::mutex DBusClient::sMutex;
		
		// helper method to avoid exposing DBus internals in header but process DBus scalars into CIO variants
		static void extractDbusVariant(DBusMessageIter &item, Variant &v)
		{
			switch (dbus_message_iter_get_arg_type(&item))
			{
				case DBUS_TYPE_BOOLEAN:
				{
					dbus_bool_t messageValue = false;
					dbus_message_iter_get_basic(&item, &messageValue);
					v.setBoolean(messageValue);
					break;
				}
				
				case DBUS_TYPE_INT16:
				{
					std::int16_t messageValue = 0;
					dbus_message_iter_get_basic(&item, &messageValue);
					v.setInteger(messageValue);
					break;
				}
				
				case DBUS_TYPE_UINT16:
				{
					std::uint16_t messageValue = 0;
					dbus_message_iter_get_basic(&item, &messageValue);
					v.setUnsignedInteger(messageValue);
					break;
				}
			
				case DBUS_TYPE_STRING:
				case DBUS_TYPE_OBJECT_PATH:
				{
					const char *messageText = nullptr;
					dbus_message_iter_get_basic(&item, &messageText);
					v.setString(messageText);
					break;
				}
				
				default:
					// don't know how to do this
					break;
			}
		}	
						
		const Metaclass &DBusClient::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		bool DBusClient::enableThreads() noexcept
		{
			// Using atomic + status to ensure this is only called once without requiring static mutex locks
			bool success = true;
			unsigned status = 0;
			bool first = sThreaded.compare_exchange_strong(status, 1);
			if (first)
			{
				success = dbus_threads_init_default();
				if (success)
				{	
					sThreaded.store(2);
				}
				else
				{
					sThreaded.store(3);
				}
			}
			else
			{
				while (status == 1)
				{
					// spin until we get out of the "requested" state
					status = sThreaded.load();
				}
				
				success = (status == 2);
			}
			
			return success;
		}
		
		DBusClient::DBusClient() noexcept :
			mSystemBus(nullptr),
			mDomain(DBUS_DOMAIN)
		{
			// Must be called at least once to make D-Bus thread safe
			DBusClient::enableThreads();
		}
		
		DBusClient::DBusClient(const char *domain) noexcept :
			mSystemBus(nullptr),
			mDomain(domain)
		{
			// Must be called at least once to make D-Bus thread safe
			DBusClient::enableThreads();
		}
		
		DBusClient::DBusClient(const DBusClient &in) :
			mSystemBus(in.mSystemBus),
			mDomain(in.mDomain)
		{
			if (mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				dbus_connection_ref(mSystemBus);
			}
		}
		
		DBusClient::DBusClient(DBusClient &&in) noexcept :
			mSystemBus(in.mSystemBus),
			mDomain(in.mDomain)
		{
			in.mSystemBus = nullptr;
			in.mDomain = DBUS_DOMAIN;
		}

		DBusClient &DBusClient::operator=(const DBusClient &in) noexcept
		{
			if (this != &in)
			{
				if (mSystemBus)
				{
					std::lock_guard<std::mutex> lock(sMutex);
					dbus_connection_unref(mSystemBus);
				}
			
				mSystemBus = in.mSystemBus;
				
				if (mSystemBus)
				{
					std::lock_guard<std::mutex> lock(sMutex);
					dbus_connection_ref(mSystemBus);	
				}

				mDomain = in.mDomain;
			}
			
			return *this;
		}
		
		DBusClient &DBusClient::operator=(DBusClient &&in) noexcept
		{
			if (this != &in)
			{
				mSystemBus = in.mSystemBus;
				in.mSystemBus = nullptr;
				
				mDomain = in.mDomain;
				in.mDomain = DBUS_DOMAIN;
			}
			
			return *this;
		}
			
		DBusClient::~DBusClient() noexcept
		{
			if (mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				dbus_connection_unref(mSystemBus);
			}
		}

		void DBusClient::clear() noexcept
		{
			if (mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				dbus_connection_unref(mSystemBus);
				mSystemBus = nullptr;
			}
			
			mDomain = DBUS_DOMAIN;
		}
			
		void DBusClient::open()
		{
			if (!mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				mSystemBus = dbus_bus_get(DBUS_BUS_SYSTEM, nullptr);
			}
		}
		
		void DBusClient::open(const char *domain)
		{
			if (!mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				mSystemBus = dbus_bus_get(DBUS_BUS_SYSTEM, nullptr);
			}
			this->setDomain(domain);
		}
					
		bool DBusClient::isOpen() const noexcept
		{	
			return mSystemBus != nullptr;
		}			
		
		DBusConnection *DBusClient::getConnection() const noexcept
		{
			return mSystemBus;
		}
				
		DBusConnection *DBusClient::getOrCreateConnection()
		{
			if (!mSystemBus)
			{
				std::lock_guard<std::mutex> lock(sMutex);
				mSystemBus = dbus_bus_get(DBUS_BUS_SYSTEM, nullptr);
			}
			
			return mSystemBus;
		}	
		
		void DBusClient::setConnection(DBusConnection *connection) noexcept
		{
			if (mSystemBus != connection)
			{
				if (mSystemBus)
				{
					std::lock_guard<std::mutex> lock(sMutex);
					dbus_connection_unref(mSystemBus);
				}
				
				mSystemBus = connection;
			}
		}		
		
		void DBusClient::setDomain(const char *domain) noexcept
		{
			if (domain)
			{
				mDomain = domain;
			}
			else
			{
				mDomain = DBUS_DOMAIN;
			}
		}
				
		const char *DBusClient::getDomain() const noexcept
		{
			return mDomain;
		}
		
		std::vector<cio::Path> DBusClient::findMatchingObjects(const char *iface) const
		{
			std::vector<cio::Path> objects;
			auto receiver = [&](const char *object, const char *iface)
			{
				objects.emplace_back(object);
			};
			this->findMatchingObjects(&iface, 1, receiver);
			return objects;
		}
				
		std::map<std::string, std::vector<cio::Path>> DBusClient::findMatchingObjects(const char **ifaces, std::size_t count) const
		{
			std::map<std::string, std::vector<cio::Path>> objects;
			auto receiver = [&](const char *object, const char *iface)
			{
				objects[iface].emplace_back(object);
			};
			
			this->findMatchingObjects(ifaces, count, receiver);
			return objects;
		}
						
		void DBusClient::findMatchingObjects(const char **ifaces, std::size_t count, const std::function<void (const char *object, const char *iface)> & receiver) const
		{			
			if (mSystemBus)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, "/", DBUS_OBJECTMANAGEMENT_IFACE, "GetManagedObjects");
				
				// Execute with lock
				DBusMessage *response = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
					
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				// Decode response
				DBusMessageIter topList;
				if (dbus_message_iter_init(response, &topList))
				{
					// Response is array as expected, process each element
					if (dbus_message_iter_get_arg_type(&topList) == DBUS_TYPE_ARRAY && dbus_message_iter_get_element_type(&topList) == DBUS_TYPE_DICT_ENTRY)
					{
						DBusMessageIter topItem;
						dbus_message_iter_recurse(&topList, &topItem);
						
						do
						{
							const char *objectPath = nullptr;
						
							// Object is a key (object path) and value (array of interfaces)
							DBusMessageIter objectDict;
							dbus_message_iter_recurse(&topItem, &objectDict);
							if (dbus_message_iter_get_arg_type(&objectDict) == DBUS_TYPE_OBJECT_PATH)
							{
								dbus_message_iter_get_basic(&objectDict, &objectPath);
							}
							
							if (objectPath)
							{
								dbus_message_iter_next(&objectDict);
								if (dbus_message_iter_get_arg_type(&objectDict) == DBUS_TYPE_ARRAY && 
									dbus_message_iter_get_element_type(&objectDict) == DBUS_TYPE_DICT_ENTRY)
								{
									DBusMessageIter ifaceArray;
									dbus_message_iter_recurse(&objectDict, &ifaceArray);
									
									do
									{
										DBusMessageIter ifaceDict;
										dbus_message_iter_recurse(&ifaceArray, &ifaceDict);
										
										if (dbus_message_iter_get_arg_type(&ifaceDict) == DBUS_TYPE_STRING)
										{
											const char *ifaceName = nullptr;
											dbus_message_iter_get_basic(&ifaceDict, &ifaceName);
											for (std::size_t i = 0; i < count; ++i)
											{
												if (std::strcmp(ifaces[i], ifaceName) == 0)
												{
													receiver(objectPath, ifaces[i]);
													break;
												}
											}
										}
										
									} while (dbus_message_iter_next(&ifaceArray));
								}		
							}					
							
						} while (dbus_message_iter_next(&topItem));
					}
				}

				dbus_message_unref(response);
			}
		}
		
		Variant DBusClient::getProperty(const char *object, const char *iface, const char *property) const
		{
			Variant output;
		
			auto receiver = [&](const char *property, const Variant &value)
			{
				output = value;
			};
			
			this->getProperty(object, iface, property, receiver);
			
			return output;
		}
		
		std::size_t DBusClient::getProperty(const char *object, const char *iface, const char *property, const std::function<void (const char *property, const Variant &value)> &receiver) const
		{
			std::size_t count = 0;
		
			if (mSystemBus && object && iface && property)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, object, DBUS_PROPERTIES_IFACE, "Get");
				
				DBusMessageIter args;
				dbus_message_iter_init_append(request, &args);
				dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &iface);
				dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &property);
				
				DBusMessage *response = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
				
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				DBusMessageIter results;
				if (dbus_message_iter_init(response, &results) && dbus_message_iter_get_arg_type(&results) == DBUS_TYPE_VARIANT)
				{							
					Variant v;
					DBusMessageIter value;
					dbus_message_iter_recurse(&results, &value);
					
					switch (dbus_message_iter_get_arg_type(&value))
					{
						case DBUS_TYPE_ARRAY:
						{
							if (dbus_message_iter_get_element_type(&value) == DBUS_TYPE_STRING)
							{
								DBusMessageIter element;
								dbus_message_iter_recurse(&value, &element);
								
								do
								{
									extractDbusVariant(element, v);
									receiver(property, v);
									++count;
								} while (dbus_message_iter_next(&element));
							}
							break;
						}
						
						default:
						{
							extractDbusVariant(value, v);
							receiver(property, v);
							count = 1;
							break;
						}
					}
				}
				else
				{
					sMetaclass.info() << "Unexpected message type: " << static_cast<char>(dbus_message_iter_get_arg_type(&results));
				}
				
				dbus_message_unref(response);
			}
			
			return count;
		}
		
		State DBusClient::invokeCommand(const char *object, const char *iface, const char *command)
		{
			State status;
			if (mSystemBus && object)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, object, iface, command);
				
				DBusMessage *response = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
				
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				dbus_message_unref(response);
				status.succeed();
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}
		
		Progress<std::size_t> DBusClient::executeGattRead(const char *object, const char *iface, std::uint16_t handle, const AttributeCallback &listener, std::size_t offset) const
		{
			Progress<std::size_t> result(Action::Read);
			
			if (mSystemBus && object)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, object, iface, "ReadValue");
				
				DBusMessageIter args;
				
				// The parameter to ReadValue is always a dict array (gross)
				dbus_message_iter_init_append(request, &args);
					
				DBusMessageIter dictArray;
				dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY, "{sv}", &dictArray);
					
				// if offset is nonzero, we have to pass it as a dict option (gross)
				const char *option = "offset";
				std::uint16_t offsetArg = static_cast<std::uint16_t>(offset);
				if (offsetArg > 0)
				{
					DBusMessageIter dictValue;
					dbus_message_iter_open_container(&dictArray, DBUS_TYPE_DICT_ENTRY, nullptr, &dictValue);
					dbus_message_iter_append_basic(&dictValue, DBUS_TYPE_STRING, &option);
					
					DBusMessageIter value;
					dbus_message_iter_open_container(&dictValue, DBUS_TYPE_VARIANT, "q", &value);
					dbus_message_iter_append_basic(&value, DBUS_TYPE_UINT16, &offsetArg);
					dbus_message_iter_close_container(&dictValue, &value);
					
					dbus_message_iter_close_container(&dictArray, &dictValue);
				}
				
				dbus_message_iter_close_container(&args, &dictArray);
				
				DBusMessage *response = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
				
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				// Response should be array of byte
				DBusMessageIter results;
				if (dbus_message_iter_init(response, &results) && dbus_message_iter_get_arg_type(&results) == DBUS_TYPE_ARRAY)
				{				
					if (dbus_message_iter_get_element_type(&results) == DBUS_TYPE_BYTE)
					{
						std::size_t elements = dbus_message_iter_get_element_count(&results);
						
						void *received = nullptr;
						int actual = 0;
							
						DBusMessageIter value;
						dbus_message_iter_recurse(&results, &value);
						dbus_message_iter_get_fixed_array(&value, &received, &actual);
						
						listener(handle, received, static_cast<std::size_t>(actual));
						
						result.succeed();
						result.count = elements;
					}
				}
				
				dbus_message_unref(response);
				result.succeed();
			}
			else
			{
				result.fail(Reason::Unopened);
			}
			
			return result;
		}
		
		Progress<std::size_t> DBusClient::listenForProperties(const std::function<void (const char *object, const char *iface, const char *property, const void *value, std::size_t length)> &receiver, Task &exec) const
		{
			Progress<std::size_t> processed(Action::Notify);
			Progress<std::chrono::milliseconds> timeout = exec.poll();
			do
			{		
				int ms = timeout.count.count();
				
				DBusMessage *message = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					dbus_connection_read_write(mSystemBus, ms);
					message = dbus_connection_pop_message(mSystemBus);
				}
				
				while (message)
				{
					if (dbus_message_is_signal(message, DBUS_PROPERTIES_IFACE, "PropertiesChanged"))
					{
						// Validate sender is in our domain and grab object
						const char *sender = dbus_message_get_sender(message);

						// TODO this is surprisingly involved
						// if (std::strcmp(sender, mDomain) == 0)
						{
							const char *object = dbus_message_get_path(message);
						
							DBusMessageIter args;
							if (dbus_message_iter_init(message, &args))
							{
								// message should be String (interface), dict of String -> Variant for edits, and an array of String for removals
								const char *iface = nullptr;
								if (dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_STRING)
								{
									dbus_message_iter_get_basic(&args, &iface);
								}
								
								if (dbus_message_iter_next(&args) && dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_ARRAY)
								{
									DBusMessageIter dictArray;
									dbus_message_iter_recurse(&args, &dictArray);
									
									while (dbus_message_iter_get_arg_type(&dictArray) == DBUS_TYPE_DICT_ENTRY)
									{
										DBusMessageIter dictEntry;
										dbus_message_iter_recurse(&dictArray, &dictEntry);
										
										const char *property = nullptr;
										if (dbus_message_iter_get_arg_type(&dictEntry) == DBUS_TYPE_STRING)
										{
											dbus_message_iter_get_basic(&dictEntry, &property);
										}
										
										const void *bytes = nullptr;
										std::size_t length = 0;
										
										dbus_message_iter_next(&dictEntry);
										if (dbus_message_iter_get_arg_type(&dictEntry) == DBUS_TYPE_VARIANT)
										{
											DBusMessageIter value;
											dbus_message_iter_recurse(&dictEntry, &value);
											if (dbus_message_iter_get_arg_type(&value) == DBUS_TYPE_STRING)
											{
												const char *text = nullptr;
												dbus_message_iter_get_basic(&value, &text);
												bytes = text;
												length = std::strlen(text);
											}
											else if (dbus_message_iter_get_arg_type(&value) == DBUS_TYPE_ARRAY)
											{
												DBusMessageIter arrayItem;
												dbus_message_iter_recurse(&value, &arrayItem);
												
												if (dbus_message_iter_get_arg_type(&arrayItem) == DBUS_TYPE_BYTE)
												{
													int readLength = 0;
													dbus_message_iter_get_fixed_array(&arrayItem, &bytes, &readLength);
													length = static_cast<std::size_t>(readLength);
												}
											}
											// Temporary hackery to handle connect and disconnect
											// We will need to either implement a true listener API or a true
											// Variant class to handle the full range of types
											else if (dbus_message_iter_get_arg_type(&value) == DBUS_TYPE_BOOLEAN)
											{
												dbus_bool_t messageValue = false;
												dbus_message_iter_get_basic(&value, &messageValue);
												if (messageValue)
												{
													bytes = "true";
													length = 4;
												}
												else
												{
													bytes = "false";
													length = 5;
												}
											}
										}
										
										receiver(object, iface, property, bytes, length);
										++processed.count;
										processed.succeed();
										
										dbus_message_iter_next(&dictArray);
									}
								}
							}
						}
					}
					
					dbus_message_unref(message);
					message = dbus_connection_pop_message(mSystemBus);
				}
				
				timeout = exec.poll();
			}
			while (timeout.running());
			
			return processed;
		}
		
		Progress<std::size_t> DBusClient::executeGattWrite(const char *object, const char *iface, const void *buffer, std::size_t requested, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Write);
			
			if (mSystemBus && object)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, object, iface, "WriteValue");
				
				DBusMessageIter args;
				dbus_message_iter_init_append(request, &args);
				
				// Add buffer as an array of byte
				DBusMessageIter bytes;
				dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY, "y", &bytes);
				dbus_message_iter_append_fixed_array(&bytes, DBUS_TYPE_BYTE, &buffer, requested);
				dbus_message_iter_close_container(&args, &bytes);
				
				// We have to specify type as request as a dict option (gross)
				DBusMessageIter dictArray;
				dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY, "{sv}", &dictArray);
				
				// Type argument
				const char *typeOption = "type";
				const char *typeText = "request";
				DBusMessageIter typeDictEntry;
				dbus_message_iter_open_container(&dictArray, DBUS_TYPE_DICT_ENTRY, nullptr, &typeDictEntry);
				dbus_message_iter_append_basic(&typeDictEntry, DBUS_TYPE_STRING, &typeOption);
				
				DBusMessageIter typeValue;
				dbus_message_iter_open_container(&typeDictEntry, DBUS_TYPE_VARIANT, "s", &typeValue);
				dbus_message_iter_append_basic(&typeValue, DBUS_TYPE_STRING, &typeText);
				dbus_message_iter_close_container(&typeDictEntry, &typeValue);
				dbus_message_iter_close_container(&dictArray, &typeDictEntry);
				
				// if offset is nonzero, we have to pass it as a dict option (gross)
				const char *option = "offset";				
				if (offset > 0)
				{
					DBusMessageIter offsetDictEntry;
					dbus_message_iter_open_container(&dictArray, DBUS_TYPE_DICT_ENTRY, nullptr, &offsetDictEntry);
					dbus_message_iter_append_basic(&offsetDictEntry, DBUS_TYPE_STRING, &option);
					
					std::uint16_t offsetArg = static_cast<std::uint16_t>(offset);
					DBusMessageIter offsetValue;
					dbus_message_iter_open_container(&offsetDictEntry, DBUS_TYPE_VARIANT, "q", &offsetValue);
					dbus_message_iter_append_basic(&offsetValue, DBUS_TYPE_UINT16, &offsetArg);
					dbus_message_iter_close_container(&offsetDictEntry, &offsetValue);
					dbus_message_iter_close_container(&dictArray, &offsetDictEntry);
				}
				
				dbus_message_iter_close_container(&args, &dictArray);
				
				DBusMessage *response = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
				
				// Response should just be confirmation of write
				
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				dbus_message_unref(response);
				result.succeed();
			}
			else
			{
				result.fail(Reason::Unopened);
			}
			
			return result;
		}
				
		Progress<std::size_t> DBusClient::executeGattSend(const char *object, const char *iface, const void *buffer, std::size_t requested)
		{
			Progress<std::size_t> result(Action::Write);
			
			if (mSystemBus && object)
			{
				DBusError error;
				dbus_error_init(&error);
				
				// Send request
				DBusMessage *request = dbus_message_new_method_call(mDomain, object, iface, "WriteValue");
				
				DBusMessageIter args;
				dbus_message_iter_init_append(request, &args);
				
				// Add buffer as an array of byte
				DBusMessageIter bytes;
				dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY, "y", &bytes);
				dbus_message_iter_append_fixed_array(&bytes, DBUS_TYPE_BYTE, &buffer, requested);
				dbus_message_iter_close_container(&args, &bytes);
				
				// We have to specify type as request as a dict option (gross)
				DBusMessageIter dictArray;
				dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY, "{sv}", &dictArray);
				
				// Type argument
				const char *typeOption = "type";
				const char *typeText = "command";
				DBusMessageIter typeDictEntry;
				dbus_message_iter_open_container(&dictArray, DBUS_TYPE_DICT_ENTRY, nullptr, &typeDictEntry);
				dbus_message_iter_append_basic(&typeDictEntry, DBUS_TYPE_STRING, &typeOption);
				
				DBusMessageIter typeValue;
				dbus_message_iter_open_container(&typeDictEntry, DBUS_TYPE_VARIANT, "s", &typeValue);
				dbus_message_iter_append_basic(&typeValue, DBUS_TYPE_STRING, &typeText);
				dbus_message_iter_close_container(&typeDictEntry, &typeValue);
				dbus_message_iter_close_container(&dictArray, &typeDictEntry);
				
				dbus_message_iter_close_container(&args, &dictArray);
				
				DBusMessage *response  = nullptr;
				{
					std::lock_guard<std::mutex> lock(sMutex);
					response = dbus_connection_send_with_reply_and_block(mSystemBus, request, DBUS_TIMEOUT_USE_DEFAULT, &error);
				}
				
				// Response should just be confirmation of write
				
				dbus_message_unref(request);
				
			   	if (dbus_error_is_set(&error))
				{ 
					cio::Exception e;
					e.setMessage(error.message);
					dbus_error_free(&error);
					throw e; 
				}
				
				dbus_message_unref(response);
				result.succeed();
			}
			else
			{
				result.fail(Reason::Unopened);
			}
			
			return result;
		}
		
		void DBusClient::subscribe(const char *object, const char *iface, const char *name)
		{
			char rule[512] = { };
			this->printMatchRule(object, iface, name, rule, 512);

			DBusError error;
			dbus_error_init(&error);  
			
			{
				std::lock_guard<std::mutex> lock(sMutex);
				dbus_bus_add_match(mSystemBus, rule, &error);
	   			dbus_connection_flush(mSystemBus);
   			}
   			
  			if (dbus_error_is_set(&error))
  			{
  				cio::Exception e;
  				e.setMessage(error.message);
				dbus_error_free(&error);  				
  				throw e;
  			}
		}
				
		void DBusClient::unsubscribe(const char *object, const char *iface, const char *name)
		{
			char rule[512] = { };
			this->printMatchRule(object, iface, name, rule, 512);
			
			DBusError error;
			dbus_error_init(&error);  
			
			{
				std::lock_guard<std::mutex> lock(sMutex);
				dbus_bus_remove_match(mSystemBus, rule, &error);
	   			dbus_connection_flush(mSystemBus);
   			}
   			
  			if (dbus_error_is_set(&error))
  			{
  				cio::Exception e;
  				e.setMessage(error.message);
				dbus_error_free(&error);  				
  				throw e;
  			}
		}
				
		std::size_t DBusClient::printMatchRule(const char *object, const char *iface, const char *name, char *rule, std::size_t length) noexcept
		{
			const char *fixedText[] = {
				"type='signal',sender='",
				"',interface='",
				"',member='",
				"',path='",
				"'"
			};
			
			std::size_t i = 0;
			
			std::size_t objLen = std::strlen(object);
			std::size_t domainLen = std::strlen(mDomain);
			std::size_t nameLen = std::strlen(name);
			std::size_t ifaceLen = std::strlen(iface);
			
			std::size_t neededBytes = objLen + domainLen + nameLen + ifaceLen +
				sizeof(fixedText[0]) + sizeof(fixedText[1]) + sizeof(fixedText[2]) + sizeof(fixedText[3]) + sizeof(fixedText[4]) - 5; 
			
			Buffer tmp(rule, length, nullptr);
			
			tmp.requestWrite(fixedText[0], std::strlen(fixedText[0]));
			tmp.requestWrite(mDomain, domainLen);
			tmp.requestWrite(fixedText[1], std::strlen(fixedText[1]));
			tmp.requestWrite(iface, ifaceLen);
			tmp.requestWrite(fixedText[2], std::strlen(fixedText[2]));
			tmp.requestWrite(name, nameLen);
			tmp.requestWrite(fixedText[3], std::strlen(fixedText[3]));
			tmp.requestWrite(object, objLen);
			tmp.requestWrite(fixedText[4], std::strlen(fixedText[4]));
			
			if (neededBytes < length)
			{
				tmp.requestSplat(0, length - neededBytes);
			}
			
			return neededBytes;
		}
				
		void DBusClient::subscribePropertyUpdates(const char *object)
		{
			this->subscribe(object, DBUS_PROPERTIES_IFACE, "PropertiesChanged");
		}
				
		void DBusClient::unsubscribePropertyUpdates(const char *object)
		{
			this->unsubscribe(object, DBUS_PROPERTIES_IFACE, "PropertiesChanged");
		}
				
		DBusClient::operator bool() const noexcept
		{
			return mSystemBus != nullptr;
		}
	}
}
