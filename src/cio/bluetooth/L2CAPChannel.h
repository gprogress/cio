/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_L2CAPCHANNEL_H
#define CIO_BLUETOOTH_L2CAPCHANNEL_H

#include "Types.h"

#include <cio/net/Channel.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The L2CAP Channel provides a refinement of the general CIO Net Channel implementation
		 * to add specific support for working with Bluetooth L2CAP sequential datagram sockets.
		 * These are the primary socket type for Bluetooth Low Energy (BLE) and are also used with Bluetooth Classic (BR/EDR).
		 * 
		 * The primary capabilities this class adds are opening connections based on Bluetooth devices
		 * and setting Bluetooth-specific socket options.
		 */
		class CIO_BLUETOOTH_API L2CAPChannel : public cio::net::Channel
		{
			public:
				/**
				 * Gets the declared metaclass for this class type.
				 *
				 * @return the metaclass for L2CAPChannel
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Creates a network address suitable for representing an L2CAP device and parameters.
				 *
				 * @param address The device physical address
				 * @param addressType The address type of the address
				 * @param cid The Channel ID (CID)
			 	 * @param psm The Protocol Service Multiplexor (PSM) to use (default is 0 to only use CID)
				 * @return a packed binary network address suitable for CIO Net Socket operaitons.
				 */
				static cio::net::Address makeAddress(const DeviceAddress &address, AddressType remoteType, std::uint16_t cid, std::uint16_t psm);
				
				/**
				 * Creates a CIO Net Socket that is initialized for the proper address family, channel type, and protocol family for Bluetooth L2CAP.
				 * The socket is not bound to any local device or connected to any remote device.
				 *
				 * @return a socket suitable for use for Bluetooth L2CAP
				 */
				static cio::net::Socket makeSocket();
				
				/**
				 * Gets the CIO Network Address Family for Bluetooth.
				 * @warning The underlying integer value is different on different platforms
				 * @return the Bluetooth Address Family
				 */
				static cio::net::AddressFamily family() noexcept;
				
				/**
				 * Gets the channel type for L2CAP. This is always ChannelType::Sequential.
				 * 
				 * @return the L2CAP channel type
				 */
				static cio::net::ChannelType channel() noexcept;

				/**
				 * Gets the CIO Network Protocol Family for L2CAP
				 * @warning The underlying integer value is different on different platforms
				 * @return the L2CAP Protocol Family
				 */
				static cio::net::ProtocolFamily protocol() noexcept;
				
				/**
				 * Construct an unconnected L2CAP socket.
				 */
				L2CAPChannel() noexcept;
				
				/**
				 * Construct an L2CAP channel by transferring the native socket of the given channel
				 *
				 * @param in The channel to transfer
				 */
				L2CAPChannel(L2CAPChannel &&in) noexcept;
				
				/**
				 * Moves an L2CAP channel.
				 *
				 * @param in The channel to move
				 * @return this channel
				 */
				L2CAPChannel &operator=(L2CAPChannel &&in) noexcept;
				
				/**
				 * Destructor. Closes the socket connection.
				 */
				virtual ~L2CAPChannel() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Opens this Datagram Channel to work with the given resource location.
				 * The resource format is bluetooth://interface@recipicent where
				 * <ul>
				 * <li>interface is the index or MAC of the local adapter to use, if specified</li>
				 * <li>recipient is the MAC of the remote device to connect to</li>
				 * </ul>
				 *
				 * Datagram sockets by nature are full duplex read/write and are always newly created, so the returned ModeSet will always indicate that.
				 *
				 * @warning This method is not yet implemented
				 *
				 * @param path The resource path to connect to
				 * @param modes The desired modes
				 * @return the actual modes
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				virtual cio::ModeSet openWithFactory(const cio::Path &path, cio::ModeSet modes, cio::ProtocolFactory *factory) override;
				
				/**
				 * Binds this L2CAP channel to the given local device and parameters.
				 * Any subsequent connection to a remote device will only be made through the given local device.
				 *
				 * @param local The local device to bind to
				 * @param localType The address type of the local device
				 * @param cid The Channel ID (CID) to bind to
				 * @param psm The Platform-Specific Multiplexor (PSM) to use, or 0 to only use the CID
				 * @throw cio::Exception If the channel could not be bound to the given local device
				 */
				void connectThroughDevice(const DeviceAddress &local, AddressType localType, std::uint16_t cid, std::uint16_t psm);
				
				/**
				 * Connects this L2CAP channel to the given remote device and parameters.
				 *
				 * @param remote The remote device to bind to
				 * @param remoteType The address type of the remote device
				 * @param cid The Channel ID (CID) to bind to
				 * @param psm The Platform-Specific Multiplexor (PSM) to use, or 0 to only use the CID
				 * @throw cio::Exception If the channel could not be connected to the given remote device
				 */
				void connectToRemoteDevice(const DeviceAddress &remote, AddressType remoteType, std::uint16_t cid, std::uint16_t psm);
				
			private:
				/** The metaclass for this class */
				static Class<L2CAPChannel> sMetaclass;
				
				/** Class logger for this class */
				static Logger sLogger;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

