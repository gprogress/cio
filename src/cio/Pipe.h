/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PIPE_H
#define CIO_PIPE_H

#include "Types.h"

#include "Device.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The Pipe class provides the lowest level access to the native pipe device read/write API.
	* Typically this primarily relies on the cio::file::Channel support and adds some configuration methods.
	*
	* Pipes generally exist on the filesystem like files but in terms of operations actually work much more like network stream channels.
	*/
	class CIO_API Pipe : public Device
	{
		public:
			/**
			* Gets the metaclass for the Pipe class.
			*
			* @return the metaclass for this class
			*/
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct an instance not connected to any device.
			*/
			Pipe();

			/**
			 * Construct a pipe from an already-open pipe handle.
			 * It is up to the caller to ensure the handle is valid.
			 * The handle will be owned by the pipe and closed on clear.
			 * 
			 * @param handle The handle to adopt
			*/
			explicit Pipe(std::uintptr_t handle);

			/**
			* Move a device connection into a new instance of this class.
			*
			* @param in The device to move
			*/
			Pipe(Pipe &&in) noexcept;

			/**
			* Move a device connection into this object.
			* The existing device connection is closed.
			*
			* @param in The device to move
			* @return this device
			*/
			Pipe &operator=(Pipe &&in) noexcept;

			/**
			* Close and tear down a device instance.
			*/
			virtual ~Pipe() noexcept override;

			/**
			* Gets the metaclass for the runtime type of this object.
			*
			* @return the metaclass
			*/
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Creates an unnamed pipe.
			 * This pipe will contain the writable end.
			 * The returned pipe will contain the readable end.
			 * 
			 * @param bufferSize The buffer size for the pipe
			 * @return the readable end of the pipe
			*/
			Pipe createAnonymousPipe(std::size_t bufferSize = 65535);

			/**
			* Creates a named pipe for full duplex operation as a device.
			* Named pipes are effectively channels using a fixed buffer size and shared memory.
			*
			* @param name The name of the pipe to create (may be platform dependent)
			* @param bufferSize The size of the pipe buffer
			*/
			void createPipe(const char *name, std::size_t bufferSize = 65535);

			/**
			* Connects to a previously created named pipe as a device.
			* Reading from this device will receive data written to the other side of the pipe.
			* Writing to this device will send data back to be read by the other side of the pipe.
			*
			* @param name The name of the pipe to connect to
			*/
			void connectPipe(const char *name);
				
		private:
			/** The metaclass for this class */
			static Class<Pipe> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

