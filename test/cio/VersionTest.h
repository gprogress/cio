/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_VERSIONTEST_H
#define CIO_VERSIONTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the cio::Version class.
	 */
	class VersionTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructor.
			 */
			VersionTest();

			/**
			 * Destructor.
			 */
			virtual ~VersionTest() noexcept override;
			
			/**
			 * Tests Version::getMajor()
			 */
			void testGetMajor();
			
			/**
			 * Tests Version::getMinor()
			 */
			void testGetMinor();
			
			/**
			 * Tests Version::getPatch()
			 */
			void testGetPatch();
			
			/**
			 * Tests Version::getTweak()
			 */
			void testGetTweak();
			
			/**
			 * Tests Version::operator[](std::size_t)
			 */
			void testBracket();
			
			/**
			 * Tests operator<(const Version &, const Version &)
			 */
			void testLess();
			
			/**
			 * Tests operator>(const Version &, const Version &)
			 */
			void testGreater();
			
			/**
			 * Tests operator==(const Version &, const Version &)
			 */
			void testEqual();
	};
}

#endif

