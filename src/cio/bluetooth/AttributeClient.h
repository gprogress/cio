/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ATTRIBUTECLIENT_H
#define CIO_BLUETOOTH_ATTRIBUTECLIENT_H

#include "Types.h"

#include "Subscriptions.h"
#include "TypeDictionary.h"

#include <cio/State.h>


#include <string>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Attribute Client provides the core interface for performing Bluetooth Low Energy GAP and GATT queries.
		 * This can be implemented by one of several underlying technologies including L2CAP sockets with manual packet processing, DBus on Linux, and so on.
		 * 
		 * Most users will want to use the Adapter or Connection classes to set up and obtain the best implementation your current platform.
		 * This class is intended to provide the minimal API needed to implement all higher-level queries in a technology-agnostic way.
		 *
		 * The base class provides a placeholder implementation that returns Unsupported or empty results for all operations.
		 *
		 * Unless otherwise noted, subclasses shall ensure all methods are reentrant and thread-safe.
		 */
		class CIO_BLUETOOTH_API AttributeClient
		{
			public:				
				/** The default maximum packet size (Maximum Transmission Unit aka MTU) - per spec this is 23 bytes */
				static const std::size_t DEFAULT_MAX_PACKET_SIZE;
				
				/** The maximum possible packet size (Maximum Transmission Unit aka MTU) - per spec this is 251 bytes */
				static const std::size_t HIGHEST_MAX_PACKET_SIZE;
				
				/**
				 * The default channel ID (CID) used for low energy L2CAP connections for attributes.
				 * This is currently the constant 4.
				 *
				 * @return the default low-energy CID for attributes
				 */
				static const std::uint16_t DEFAULT_ATT_CID;
				
				/**
				 * The default protocol/service multiplexer (PSM) used for low energy L2CAP connections for attributes.
				 * This is currently the constant 31.
				 *
				 * @return the default low-energy PSM for attributes
				 */
				static const std::uint16_t DEFAULT_ATT_PSM;
										
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
			
				/**
				 * General method to take an unorganized and unsorted list of all services, all characteristics, and all attribute declarations
				 * and organize them into a sorted hierarchy underneath the services.
				 *
				 * This method does not actually use the attribute client itself in any way to discover anything.
				 * 
				 * The method does the following steps:
				 * <ol>
				 * <li>Sorts the service list in ascending handle order</li>
				 * <li>Validates service end handles to mark service boundaries</li>
				 * <li>Sorts characteristic list in ascending handle order</li>
				 * <li>Filters out characteristics not belonging to any service</li>
				 * <li>Moves characteristics into cached characteristic list on containing service</li>
				 * <li>Validates characteristic end handles based on parent service</li>
				 * <li>Sorts declarations list in ascending handle order</li>
				 * <li>Filters out declarations not beloning to any surviving characteristics</li>
				 * <li>Moves declarations into cached declaration list on containing characteristic</li>
				 * </ol>
				 *
				 * @param services The unorganized list of services
				 * @param chars The unorganized list of characteristics
				 * @param decls The unorganized list of descriptors
				 * @return a reference to the list of services after it was organized
				 */
				static std::vector<Service> &organizeServices(std::vector<Service> &services, std::vector<Characteristic> &chars, std::vector<Descriptor> &decls);
				
				/**
				 * Construct an unopened Bluetooth Attribute Client.
				 */
				AttributeClient() noexcept;
				
				/**
				 * Constructs a Bluetooth Attribute Client by adopting the state of a given client.
				 * 
				 * @param in The client to move
				 */
				AttributeClient(AttributeClient &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Attribute Client into this connection.
				 * The current state is cleared first.
				 *
				 * @param in The connection to move
				 * @return this connection
				 */
				AttributeClient &operator=(AttributeClient &&in) noexcept;
				
				/**
				 * Destructor. Clears all device state.
				 */
				virtual ~AttributeClient() noexcept;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept;
				
				/**
				 * Clears the connection state and disconnects from any connected remote device.
				 */				
				virtual void clear() noexcept;
				
				// Connection management
				
				/**
				 * Connects to the given peripheral using the first available local adapter.
				 *
				 * @param peripheral The peripheral to connect to
				 */
				virtual void connect(const Peripheral &peripheral);
					
				/**
				 * Connects to the given peripheral using the given local device as the adapter.
				 *
				 * @param adapter The local adapter to use
				 * @param peripheral The peripheral to connect to
				 */								
				virtual void connect(const Device &adapter, const Peripheral &peripheral);
				
				/**
				 * Disconnects from the current peripheral.
				 */
				virtual void disconnect() noexcept;
				
				/**
				 * Checks whether the client is currently connected.
				 *
				 * @return whether the client is connected
				 */
				virtual bool isOpen() const noexcept;
				
				/**
				 * Gets the most recently negotiated maximum packet size.
				 * The default if not negotiated is the ATT default of 23.
				 *
				 * @return the maximum packet size
				 */
				std::size_t getMaxPacketSize() const noexcept;

				/**
				 * Sets the most recently negotiated maximum packet size.
				 * This sets the value without any validation.
				 * Most users will instead want to use negotiateMaxPacketSize.
				 *
				 * @param mtu The maximum packet size
				 */
				void setMaxPacketSize(std::size_t mtu) noexcept;

				// High-level discovery API - most users will want to use this rather than lower-level functionality
				
				// GAP basics - convenience wrappers for maximum ease of use and performance
				
				/**
				 * Uses the Generic Access Profile to get the device name.
				 *
				 * @return the device name
				 */
				std::string getDeviceName();

				// Discovery - simplified high level API
				
				/**
				 * Gets the full list of every attribute on the remote device.
				 * It's generally more useful to start with services and characteristics, but if you need the full list in one pass, this will do it.
				 *
				 * @return the full list of attributes on the remote device
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				std::vector<Attribute> discoverAllAttributes();
				
				/**
				 * Finds the handle of the first attribute with the given type.
				 *
				 * @param type The type
				 * @return the first attribute handle with the given type
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				std::uint16_t findAttributeHandle(const UniqueId &type);
				
				/**
				 * Discovers all primary services on the remote device.
				 *
				 * @return the list of all primary services
				 */
				std::vector<Service> discoverAllPrimaryServices();
												
				/**
				 * Finds a primary service given its service type.
				 *
				 * @param id The service type (value UUID of the service)
				 * @return the first service with the given definition
				 */
				Service findPrimaryService(const UniqueId &id);
				
				/**
				 * Discovers all characteristics on the remote device.
				 * It's generally more useful to start with services and find associated characteristics, but if you need the full list in one pass, this will do it.
				 *
				 * @return the list of all primary services
				 */		
				std::vector<Characteristic> discoverAllCharacteristics();

				/**
				 * Finds a characteristic given its value type.
				 *
				 * @param id The value type (value UUID of the characteristic)
				 * @return the first characteristic with the given value type
				 */
				Characteristic findCharacteristic(const UniqueId &id);
				
				/**
				 * Gets the full list of every descriptor on the remote device.
				 * It's generally more useful to start with services and characteristics, but if you need the full list in one pass, this will do it.
				 *
				 * @return the full list of descriptors on the remote device
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				std::vector<Descriptor> discoverAllDescriptors();
												
				/**
				 * Finds the first descriptor with the given type.
				 *
				 * @return the attribute with the given type
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				Descriptor findDescriptor(const UniqueId &type);
				
				/**
				 * Requests to read the given attribute given its handle.
				 * This method must handle all kinds of attributes including Services (whose value is the little-endian service UUID) and Characteristics (whose value
				 * is the combination of little-endian value type UUID and handle) as well as regular Descriptors with actual data.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to store the read bytes into
				 * @param requested The requested number of bytes
				 * @param offset The offset of the remote attribute value to start reading at
				 * @return the actual number of bytes processed and the State
				 */
				Progress<std::size_t> getValue(std::uint16_t handle, void *buffer, std::size_t length, std::size_t offset = 0);
				
				/**
				 * Requests to at most one value whose attribute matches the given type.
				 * This method must handle all kinds of attributes including Services (whose value is the little-endian service UUID) and Characteristics (whose value
				 * is the combination of little-endian value type UUID and handle) as well as regular Descriptors with actual data.
				 *
				 * @param type The type
				 * @param buffer The buffer to store the value in
				 * @param length The maximum number of bytes to use
				 * @return the State of the read request
				 */	
				Progress<std::size_t> getValueByType(const UniqueId &type, void *buffer, std::size_t length);	
								
				/**
				 * Requests to read one or more values whose attribute matches the given type.
				 * This method must handle all kinds of attributes including Services (whose value is the little-endian service UUID) and Characteristics (whose value
				 * is the combination of little-endian value type UUID and handle) as well as regular Descriptors with actual data.				 
				 *
				 * @param type The type
				 * @param listener The listener to receive each read value
				 */
				Progress<std::size_t> readValuesByType(const UniqueId &type, const AttributeCallback &listener);
				
				// Discovery - polymorphic API for technologies to implement
				
				/**
				 * Sets up the maximum packet size with the peripheral.
				 *
				 * @param mtu The local maximum packet size supported by this adapter
				 * @return the remote maximum packet size supported by peripheral
				 */
				virtual std::size_t negotiateMaxPacketSize(std::size_t mtu);
				
				/**
				 * Gets an attribute's type given its handle.
				 *
				 * @param handle The attribute handle
				 * @return the attribute type
				 */
				virtual UniqueId getAttributeType(std::uint16_t handle);
				
				/**
				 * Gets the list of every attribute in a handle range on the remote device.
				 * It's generally more useful to work with the hierarchy of services, characteristics, and descriptors,
				 * but this method will get you a flat list of attributes.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of attributes on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Attribute> discoverAttributes(std::uint16_t start, std::uint16_t end);
				
				/**
				 * Finds the handle of the first attribute with the given type within the given handle range.
				 *
				 * @param type The type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the first attribute handle with the given type
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual std::uint16_t findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end);
				
				/**
				 * Gets a service given its handle.
				 *
				 * @param handle The service handle
				 * @return the service
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Service getService(std::uint16_t handle);
				
				/**
				 * Discovers all primary services on the remote device.
				 * This issues a sequence of read value by group type requests to process all possible handle ranges.
				 * 
				 * @return the list of all primary services
				 */
				virtual std::vector<Service> discoverPrimaryServices(std::uint16_t start, std::uint16_t end);

				/**
				 * Finds a primary service given its service type in a particular handle range.
				 *
				 * @param id The service type (value UUID of the service)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first service with the given definition
				 */
				virtual Service findPrimaryServiceInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end);
				
				/**
				 * Gets a characteristic given its handle.
				 *
				 * @param handle The characteristic handle
				 * @return the characteristic
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Characteristic getCharacteristic(std::uint16_t handle);
				
				/**
				 * Discovers characteristics on the remote device within a specified handle range.
				 * It's generally more useful to start with services and find associated characteristics, but if you need a particular range in one pass, this will do it.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of characteristics in the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the characteristics
				 */		
				virtual std::vector<Characteristic> discoverCharacteristics(std::uint16_t start, std::uint16_t end);
				
				/**
				 * Finds a characteristic given its value type in a particular handle range.
				 *
				 * @param id The value type (value UUID of the characteristic)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first characteristic with the given value type
				 */
				virtual Characteristic findCharacteristicInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end);
				
				/**
				 * Gets an attribute given its handle.
				 *
				 * @param handle The attribute handle
				 * @return the attribute
				 * @throw cio::Exception If a transport or packet error prevented reading the attribute or it did not exist
				 */
				virtual Descriptor getDescriptor(std::uint16_t handle);
				
				/**
				 * Gets the list of every descriptor in a handle range on the remote device.
				 * It's generally more useful to start with services and characteristics, but if you need the full list in one pass, this will do it.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of descriptors on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Descriptor> discoverDescriptors(std::uint16_t start, std::uint16_t end);

				/**
				 * Finds the first descriptor with the given type within the given handle range.
				 *
				 * @return the attribute with the given type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual Descriptor findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end);
				
				/**
				 * Requests to read the given attribute given its handle.
				 * This method must handle all kinds of attributes including Services (whose value is the little-endian service UUID) and Characteristics (whose value
				 * is the combination of little-endian value type UUID and handle) as well as regular Descriptors with actual data.
				 *
				 * @param handle The attribute handle
				 * @param listener The listener to receive the read value
				 * @param offset The offset of the remote attribute value to start reading at
				 * @return the actual number of bytes processed and the State
				 */
				virtual Progress<std::size_t> readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset);
				
				/**
				 * Requests to read one or more values in an attribute range whose attribute matches the given type.
				 * This method must handle all kinds of attributes including Services (whose value is the little-endian service UUID) and Characteristics (whose value
				 * is the combination of little-endian value type UUID and handle) as well as regular Descriptors with actual data.
				 *
				 * @param type The type
				 * @param listener The listener to receive each read value
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 */
				virtual Progress<std::size_t> readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end);
				
				/**
				 * Requests to write the given attribute with confirmation given its handle.
				 * This method is only required to handle Descriptor attributes since Service and Characteristic data values are immutable.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @param offset The offset of the remote attribute value to start write at
				 * @return the actual number of bytes processed and the State
				 */
				virtual Progress<std::size_t> writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset);
				
				/**
				 * Requests to write the given attribute without confirmation given its handle.
				 * This method is only required to handle Descriptor attributes since Service and Characteristic data values are immutable.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @return the actual number of bytes processed and the State
				 */
				virtual Progress<std::size_t> sendValue(std::uint16_t handle, const void *buffer, std::size_t requested);
				
				/**
				 * Attempts to subscribe to receive updates for the given characteristic's value.
				 * Each received update will be given to the provided listener.
				 *
				 * This method must handle all kinds of attributes including Services (whose value is the service UUID) and Characteristics (whose value
				 * is the combination of value type and handle) as well as regular Descriptors.
				 *
				 * The attribute will be examined to determine what types of updates it supports compared to what modes
				 * the user specifies as acceptable. If more than one mode is available, they are preferred as follows:
				 * <ol>
				 * <li>Notify without confirmation</li>
				 * <li>Indicate with confirmation</li>
				 * <li>Periodic polling via read</li>
				 * </ol>
				 *
				 * Only Characteristic Value Descriptors can support indicate and notify modes, and only if the underlying technology supports it.
				 * All other attributes (Services, Characteristics, and non-value Descriptors) will use read polling.
				 *
				 * The base class sets up the subscription in the subscriptions map.
				 * Subclasses must override to actually enable subscriptions on the peripheral.
				 *
				 * Subclasses should ensure this method is thread-safe relative to subscribe, unsubscribe, and receiveSubscriptions so that
				 * the client can be used in multi-threaded asynchronous code.
				 *
				 * Subclasses shall ensure that the number of subscriptions to each characteristic is tracked so that it is only unsubscribed
				 * when all of them have called unsubscribe().
				 *
				 * The returned Subscription pointer is stable and will not be deleted until unsubscribe is called.
				 * Users should use it as a key if they want to manage when this particular subscription is unsubscribed.
				 * 
				 * @param handle The handle of the characteristic to subscribe to
				 * @param listener The listener to receive subscriptions
				 * @param interval The polling interval to use, or 0 to receive all updates
				 * @return the State of this operation
				 */
				virtual const Subscription *subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval);
				
				/**
				 * Runs a loop to receive incoming subscription updates based on the given executor.
				 * The executor specifies the maximum amount of time to run and allows for asynchronous cancelation, and the client should report
				 * completion State (either success or errors) ot the executor without throwing exceptions.
				 *
				 * The base class does a polling loop to read all subscribed attributes that support reading.
				 * Subclasses must override to actually receive notify/indicate requests as well.
				 *
				 * It is allowed but not mandatory for this to be run endlessly in a dedicated thread, but it can also be run incrementally in between
				 * other tasks.
				 *
				 * Subclasses should ensure this method is thread-safe relative to subscribe, unsubscribe, and receiveSubscriptions so that
				 * the client can be used in multi-threaded asynchronous code.
				 *
				 * @param exec The executor to manage timeout and cancelation and report results to
				 * @return the final State of the execution, Success if it ran to the full timeout, Abandoned if canceled, or a failure State if
				 * an actual error prevented any subscription processing
				 */
				virtual State executeSubscriptions(Task &exec) noexcept;
				
				/**
				 * Unsubscribes the subscription of the given characteristic.
				 *
				 * The base class simply updates the subscriptions map.
				 * Subclass must override to actually stop subscriptions on the peripheral.
				 *
				 * Subclasses should ensure this method is thread-safe relative to subscribe, unsubscribe, and receiveSubscriptions so that
				 * the client can be used in multi-threaded asynchronous code.
				 *
				 * Subclasses shall ensure that the number of subscriptions to each characteristic is tracked so that it is only unsubscribed
				 * when all of them have called unsubscribe().
				 *
				 * @param c The characteristic of interest
				 * @param modes which types of subscription modes to unsubscribe
				 * @return the State of this operation
				 */
				virtual State unsubscribe(const Subscription *subscription);
				
				/**
				 * Unsubscribes all subscriptions for the given attribute handle.
				 *
				 * The base class simply updates the subscriptions map.
				 * Subclass must override to actually stop subscriptions on the peripheral.
				 *
				 * @warning This invalidates all Subscription pointers for this handle.
				 *
				 * @param handle The attribute handle
				 * @return the number of subscriptions removed for the attribute
				 */
				virtual Progress<std::size_t> clearSubscriptions(std::uint16_t handle);
				
				/**
				 * Unsubscribes all subscriptions for all attributes.
				 *
				 * @warning This invalidates all Subscription pointers.
				 *
				 * The base class simply clears the subscriptions map.
				 * Subclass must override to actually stop subscriptions on the peripheral.
				 *
				 * @return the number of subscriptions removed
				 */
				virtual Progress<std::size_t> clearAllSubscriptions();
								
				/**
				 * Gets direct access to the subscriptions map.
				 * 
				 * @warning directly editing this object may result in inconsistencies for peripheral state
				 * @return the subscriptions map
				 */
				Subscriptions &getSubscriptions() noexcept;
				
				/**
				 * Gets direct access to the subscriptions map.
				 * This can be used to examine the full details of all currently active subscriptions.
				 * 
				 * @return the subscriptions map
				 */			
				const Subscriptions &getSubscriptions() const noexcept;
								
				/**
				 * Gets the type dictionary currently used by this connection.
				 * By default this is the global default type dictionary.
				 *
				 * @return the type dictionary
				 */
				TypeDictionary &getTypes() noexcept;
				
				/**
				 * Gets the type dictionary currently used by this connection.
				 * By default this is the global default type dictionary.
				 *
				 * @return the type dictionary
				 */
				const TypeDictionary &getTypes() const noexcept;

				/**
				 * Sets whether to monitor the BLE connection state.
				 * The base class simply changes the flag indicating whethe it is monitored.
				 * Subclasses must overridden to actually enable or disable moitoring.
				 *
				 * @param value Whether to monitor connection state
				 */
				virtual void setMonitorConnectionState(bool value);
				
				/**
				 * Gets whether this client is set to monitor connection state.
				 *
				 * @return whether this client is set to monitor connection state
				 */
				bool getMonitorConnectionState() const noexcept;
				
				/**
				 * Gets the currently known connection state, if any.
				 *
				 * @return the connection state
				 */
				State getConnectionState() const noexcept;
							
				/**
				 * Interprets the client in a Boolean context.
				 * The client is considered true if it is connected and therefore isOpen() returns true.
				 *
				 * @return whether the client is connected
				 */
				explicit operator bool() const noexcept;
				
			protected:	
				/** The type dictionary used by this client */
				TypeDictionary mTypes;
				
				/** Current set of subscriptions */
				Subscriptions mSubscriptions;
				
				/** Most recently negotiated max packet size */
				std::size_t mMaxPacketSize;
								
				/** Whether to actively monitor connection state */
				bool mMonitorConnectionState;
				
				/** Currently known connection state, if known */
				State mConnectionState;
				
			private:
				/** The metaclass for this class */
				static Class<AttributeClient> sMetaclass;
		
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

