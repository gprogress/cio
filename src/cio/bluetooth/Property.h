/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_PROPERTY_H
#define CIO_BLUETOOTH_PROPERTY_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Characteristic Property value is a bitset of 8 possible items.
		 * To get the whether the property was set, take the value of this enum and use it as a left shift amount to select the proper bit.
		 */
		enum class Property : std::uint8_t
		{
			/** The characteristic value can be included in advertising broadcasts */
			Broadcast,
			
			/** The characteristic value can be directly read */
			Read,
			
			/* The characteristic value can be written as a command without confirmation response */
			CommandWrite,
			
			/** The characteristic value can be written with confirmation response */
			Write,
			
			/** The characteristic value can be set up to notify the client on change */
			Notify,
			
			/** The characteristic value can be set up to indicate the client on change, expecting the client to confirm receipt */
			Indicate,
			
			/** The characteristic value can be written as a cryptographically signed command without confirmation response */
			CommandSignedWrite,
			
			/** The characteristic has extended properties in addition to the prior options */
			Extended,
		};
		
		/**
		 * Gets the text representation of a property.
		 * Any unrecognized property value (which should not happen) will return the text "Unknown".
		 *
		 * @param value The property value
		 * @return the text represented the property value
		 */
		CIO_BLUETOOTH_API const char *print(Property value) noexcept;
		
		/**
		 * Prints the text representation of a property to a C++ stream.
		 * Any unrecognized property value (which should not happen) will print the text "Unknown".
		 *
		 * @param stream The stream
		 * @param value The property value
		 * @return the stream after printing
		 */
		CIO_BLUETOOTH_API std::ostream &stream(std::ostream &stream, Property value);
	}
}

#endif

