/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_VARIANT_H
#define CIO_VARIANT_H

#include "Types.h"

#include "Ownership.h"
#include "Type.h"
#include "Text.h"
#include "Class.h"

#include <string>

namespace cio
{
	/**
	 * The Variant is a single value that may be any variety of primitive type
	 * including signed and unsigned integers, floating point values, text, and bytes.
	 *
	 * Variants with numeric data types are stored directly on this class.
	 * For text and bytes, the value may be allocated and owned by the Variant itself, or it may merely
	 * reference data owned by another object.
	 *
	 * @note Developers should be aware that this class has a very specific binary layout that is optimized
	 * for compactness across multiple platforms.  On any platform with support for doubles and 64-bit integers,
	 * alignment requirements will always force it to be 128 bits regardless of native pointer size.
	 */
	class CIO_API Variant
	{
		public:		
			/**
			 * Gets the item size in bits for the given item type.
			 * 
			 * @param type The variant type
			 * @return the number of bits per item
			 */
			static std::uint32_t getBitsPerItem(Type type) noexcept;

			/**
			 * Gets the default item count for the given item type.
			 * This is 0 for Text and Blob and 1 for everything else.
			 *
			 * @param type The variant type
			 * @return the default item count
			 */
			static std::uint32_t getDefaultItemCount(Type type) noexcept;

			/**
			 * Construct a new empty variant.
			 */
			Variant() noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(bool initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(int initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(unsigned initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 * @param id The ID
			 * @param initialValue The initial value
			 */
			Variant(long initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(unsigned long initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(long long initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(unsigned long long initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial integer value
			 */
			Variant(float initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial integer value
			 */
			Variant(double initialValue) noexcept;

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(const char *initialValue);

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(const std::string &initialValue);

			/**
			 * Construct a new variant using the provided initial value.
			 *
			 * @param initialValue The initial value
			 */
			Variant(const cio::Text &initialValue);

			Variant(void *initialValue, const Metaclass *metaclass);

			/**
			 * Construct a new variant with the given type.
			 * The value will be initialized to the default value and data length
			 * for its type.
			 *
			 * @param type The variant type
			 */
			Variant(Type type) noexcept;

			/**
			 * Construct a copy of a variant.
			 *
			 * @note For incoming variants with allocated data, a copy
			 * of the allocated data is made for this variant.  If the incoming variant value
			 * references external data, this variant will also reference the same data.
			 *
			 * @param in The variant to copy
			 */
			Variant(const Variant &in);

			/**
			 * Construct a variant by transferring data from a variant.
			 * This is very efficient since it only has to copy the raw bit patterns.
			 *
			 * @param in The variant to copy
			 */
			Variant(Variant &&in) noexcept;

			/**
			 * Copy an existing variant instance.
			 *
			 * @note If the variant has its own allocated data, a copy of the
			 * allocated data will be made for this variant.  If the variant references external data,
		 	 * this variant will reference the same data.
	 		 *
			 * @param rhs The variant to copy
			 * @return this variant
			 */
			Variant& operator=(const Variant &rhs);

			/**
			 * Transfers data from an existing variant instance.
			 * This is very efficient since only bit patterns need to be copied.
			 *
			 * @param rhs The variant to transfer
			 * @return this variant
			 */
			Variant& operator=(Variant &&rhs) noexcept;
			
			/**
			 * Destructor. Destroys the data array if this variant allocated it.
			 */
			~Variant() noexcept;
			
			/**
			 * Checks to see if the variant is valid.
			 * This requires the type to be something other than Type::None.
			 *
			 * @return Whether the variant is valid
			 */
			bool isValid() const noexcept;

			/**
			 * Checks to see if the variant is valid and represents numeric data.
			 * This is true if the type is a numeric type (Boolean, Integer, Unsigned, Real, Enum)
			 * or is a text type that parses to a numeric type.
			 * 
			 * The detected number type is returned if valid. Otherwise Type::None is returned.
			 * 
			 * @return Whether this variant represents a number and if so what type
			 */
			Type isNumber() const noexcept;

			/**
			 * Checks to see if the variant is valid and has a numeric.
			 * This is true if the type is a numeric type (Boolean, Integer, Unsigned, Real, Enum).
			 *
			 * @return Whether this variant represents a number
			 */
			bool isNumberType() const noexcept;

			/**
			 * Clears the variant.
			 * Deallocates any owned memory.
			 */
			void clear() noexcept;

			/**
			 * Checks to see if the variant has the default value for its type.
			 * This returns true if the value is equal to 0 for integers, 0.0 for doubles,
			 * false for booleans, and the empty array
			 * for strings and binary.
			 *
			 * @return whether the variant is currently the default value
			 */
			bool isDefaultValue() const noexcept;

			/**
			 * Gets the data type.
			 *
			 * @return the data type
			*/
			Type getType() const noexcept;

			/**
			 * Forcibly sets the data type.
			 * This reverts the variant value to its default embedded value.
			 * The data array will be deallocated if this variant allocated it.
			 *
			 * @param type The new variant type
			 */
			void setType(Type type) noexcept;

			/**
			 * Resets the variant to the default value for its current type.
			 * The data array will be deallocated if this value allocated it.
			 *
			 * @see getDefaultValue and getDefaultDataLength
			 * @post this->getRawBits() == Variant::getDefaultValue(this->getType())
			 * @post this->getDataLength() == Variant::getDefaultDataLength(this->getType())
			 */
			void resetToDefaultValue() noexcept;

			/**
			 * Forcibly sets the data type, and attempts to convert the current data into the new type.
			 *
			 * @param type The new type
			 */
			void convertToType(Type type);

			/**
			 * Determines how this variant relates to its contained data.
			 *
			 * @return the data ownership mode
			 */
			Ownership getOwnership() const noexcept;

			/**
			 * Gets the number of items in the data array for this value.
			 *
			 * @return the number of items in the data array
			 */
			std::size_t size() const noexcept;

			/**
			 * Gets the number of bytes in this variant.
			 * If the number of bits is not a multiple of 8, the partial byte is included in the count.
			 *
			 * @return the number of ytes
			 */
			std::size_t bytes() const noexcept;

			/**
			 * Gets the number of bits in this variant.
			 * 
			 * @return the number of bits
			 */
			std::size_t bits() const noexcept;
			
			/**
			 * Checks whether the variant is empty.
			 *
			 * @return whether the variant is empty
			 */
			bool empty() const noexcept;
			
			/**
			 * Gets the data array for this value.  If the data ownership mode is Embed,
			 * this is the actual 8-byte data buffer on this object.  Otherwise, the data buffer is
			 * interpreted as a pointer referencing an external data array presumably longer than 8 bytes.
			 *
			 * The data array may be directly modified to change its contents but not its length.
			 * Changing the variant type or length may invalidate the returned pointer.
			 *
			 * @return the data array
			 */
			std::uint8_t *data() noexcept;
			
			/**
			 * Gets the read-only data array for this value.  If the data ownership mode is Embed,
			 * this is the actual 8-byte data buffer on this object.  Otherwise, the data buffer is
			 * interpreted as a pointer referencing an external data array presumably longer than 8 bytes.
			 *
			 * Changing the variant type or length may invalidate the returned pointer.
			 *
			 * @return the data array
			 */
			const std::uint8_t *data() const noexcept;
			
			void *getObject() const noexcept;

			template<typename T>
			T *getObject(const cio::Class<T> &c) const noexcept;
			
			const cio::Metaclass *getObjectMetaclass() const noexcept;

			/**
			 * Gets direct acess to the null-terminated C string used to store text data.
			 * This is equivalent to calling this->data()->text.
			 * It is only guaranteed to be valid if the current type is Type::Text.
			 *
			 * @return the C string
			 */
			const char *c_str() const noexcept;

			/**
			 * Resizes the data array.
			 *
			 * @param count The requested number of items
			 */
			void resize(std::size_t count);

			/**
			 * Resizes the data array to the given number of bytes.
			 *
			 * @param bytes The requested number of bytes
			 */
			void resizeBytes(std::size_t bytes);

			/**
			 * Resizes the data array to the given number of Bits.
			 *
			 * @param bits The requested number of bits
			 */
			void resizeBits(std::size_t bits);

			/**
			 * Gets the value of this variant as an integer.
			 *
			 * String variants will be parsed as an integer if possible (this will also consider truncating conversions
			 * from parsed doubles or recognizing boolean strings using boolean rules).
			 *
			 * Binary variants will be decoded as a native-endian
			 * signed two's complement integer if they have data length from 1 to 8, or otherwise fail.  
			 * 
			 * Double variants will be truncated to an integer value if the value is within the representable
			 * range of an int64_t, otherwise the conversion will fail; special values such as infinity or NaN will
			 * always result in a failed conversion.
			 *
			 * Boolean values will be converted to integer 0 or 1.
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * @return The result
			 * @throw std::runtime_error If an integer cannot be retrieved
			 */
			std::int64_t getInteger() const;

			/**
			 * Gets the value of this variant as an integer.
			 *
			 * String variants will be parsed as an integer if possible (this will also consider truncating conversions
			 * from parsed doubles or recognizing boolean strings using boolean rules).
			 *
			 * Binary variants will be decoded as a native-endian
			 * signed two's complement integer if they have data length from 0 to 8, or otherwise fail.  
			 * 
			 * Double variants will be truncated to an integer value if the value is within the representable
			 * range of an int64_t, otherwise the conversion will fail; special values such as infinity or NaN will
			 * always result in a failed conversion.
			 *
			 * Boolean values will be converted to integer 0 or 1.
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * Failed conversions will result in the value being set to 0.
			 *
			 * @param[out] value The result
			 * @return whether the variant could be retrieved as an integer
			 */
			bool getInteger(std::int64_t &value) const;

			/**
			 * Gets the value of this variant as an unsigned integer.
			 *
			 * String variants will be parsed as an unsigned integer if possible (this will also consider truncating 
			 * conversions from parsed doubles or recognizing boolean strings using boolean rules).
			 *
			 * Binary variants will be decoded as a native-endian unsigned integer if they have data length from 1 to 
			 * 8, or otherwise fail.
			 *
			 * Double variants will be truncated to an unsigned integer value if the value is within the 
			 * representable range of a uint64_t, otherwise the conversion will fail; special values such as infinity 
			 * or NaN will always result in a failed conversion.
			 *
			 * Boolean values will be converted to integer 0 or 1.
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * @return The result
			 * @throw std::runtime_error If an integer cannot be retrieved
			 */
			std::uint64_t getUnsignedInteger() const;

			/**
			 * Gets the value of this variant as an unsigned integer.
			 *
			 * String variants will be parsed as an unsigned integer if possible (this will also consider truncating 
			 * conversions from parsed doubles or recognizing boolean strings using boolean rules).
			 *
			 * Binary variants will be decoded as a native-endian unsigned integer if they have data length from 1 to 
			 * 8, or otherwise fail.
			 *
			 * Double variants will be truncated to a signed integer value, since doubles are signed, if the value is 
			 * within the representable range of an int64_t, otherwise the conversion will fail; special values such as 
			 * infinity or NaN will always result in a failed conversion.
			 *
			 * Boolean values will be converted to integer 0 or 1.
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * Failed conversions will result in the value being set to 0.
			 *
			 * @param[out] value The result
			 * @return whether the variant could be retrieved as an integer
			 */
			bool getUnsignedInteger(std::uint64_t &value) const;

			/**
			 * Views this variant as a UTF-8 text.
			 * This returns a reference to the text if the variant is currently text or Boolean, and prints to returned text otherwise.
			 * 
			 * @return The view of the text
			 */
			cio::Text viewText() const;

			/**
			 * Gets a copy of this variant as a UTF-8 textual string value.
			 *
			 * Numeric types will be printed into strings.
			 * Boolean values will result in the string "true" or "false".
			 * Binary variants will be converted into a hexadecimal encoding so they may
			 * be safely stored in text formats.
			 * 
			 * Conversion fails for Type::None types. 
			 *
			 * @return The result
			 * @throw std::runtime_error If the type is Type::None
			 */
			std::string getString() const;

			/**
			 * Gets a copy of this variant as a string value.
			 *
			 * Numeric types will be printed into strings.
			 * Boolean values will result in the string "true" or "false".
			 * Binary variants will be placed into the string unmodified as UTF-8 data, which may contain embedded null bytes.
			 * 
			 * Conversion fails for Type::None types.  
			 *
			 * @todo Should binary variants instead be pretty-printed into hex or base64 encoding?
			 *
			 * @param[out] value The result
			 * @return whether the variant could be retrieved as a string
			 */
			bool getString(std::string &value) const;

			/**
			 * Gets the value of this variant as a double.
			 *
			 * Integer variants will be converted into a double, possibly with roundoff error for values outside of the range
			 * (-(2^53), (2^53)).   Truncating integer conversions are still considered successful.
			 * 
			 * String variants will be parsed for a double using the rules of the std::strtod method.
			 *
			 * Binary variants will be decoded as an IEEE-754 single-precision
			 * float if they have data length of 4 and double-precision if they have data length of 8, otherwise
			 * they will fail.  
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * Booleans will result in a value of 0.0 or 1.0.
			 *
			 * @return The result
			 * @throw std::runtime_error If a double cannot be retrieved
			 */
			double getDouble() const;
            
			/**
			* Gets the value of this variant as a double.
			 *
			 * Integer variants will be converted into a double, possibly with roundoff error for values outside of the range
			 * (-(2^53), (2^53)).   Truncating integer conversions are still considered successful.
			 * 
			 * String variants will be parsed for a double using the rules of the std::strtod method.
			 *
			 * Binary variants will be decoded as an IEEE-754 single-precision
			 * float if they have data length of 4 and double-precision if they have data length of 8, otherwise
			 * they will fail.  
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * Booleans will result in a value of 0.0 or 1.0.
			 *
			 * Failed conversions will result in a value of NaN.
			 *
			 * @param[out] value The result
			 * @return whether the variant could be retrieved as a double
			 */
			bool getDouble(double &value) const;

			/**
			 * Gets the value of this variant as a boolean.
			 *
			 * Integer values will be considered true if non-zero.  Double values will be considered true if not exactly
			 * equal to 0.0 and are not NaN.
			 *
			 * This will attempt to parse string variants using a variety of strings that could represent true or false.
			 * It will also consider strings that successfully parse as an integer or double using the rules for converting
			 * those types to boolean.  If no match is found, the conversion will fail.
			 *
			 * Binary variants will return true if any bytes exist and
			 * any of them are nonzero.  Fails for Type::None types.
			 *
			 * @return The result
			 * @throw std::runtime_error If a boolean cannot be retrieved
			 */
			bool getBoolean() const;

			/**
			 * Gets the value of this variant as a boolean.
			 *
			 * Integer values will be considered true if non-zero.  Double values will be considered true if not exactly
			 * equal to 0.0 and are not NaN.
			 *
			 * This will attempt to parse string variants using a variety of strings that could represent true or false.
			 * It will also consider strings that successfully parse as an integer or double using the rules for converting
			 * those types to boolean.  If no match is found, the conversion will fail.
			 *
			 * Binary variants will return true if any bytes exist and
			 * any of them are nonzero.  Fails for Type::None types.
			 *
			 * Conversion always fails for Type::None types.
			 *
			 * Failed conversions will result in a value of false.
			 *
			 * @param[out] value The result
			 * @return whether the variant could be retrieved as a boolean
			 */
			bool getBoolean(bool &value) const;

			/**
			 * Gets the value of this variant appropriately for the desired value type.
			 *
			 * @tparam T The type of value to get, which must be one of int64_t, double, bool, or std::string.
			 *
			 * @warning This generic version has no implementation.  You must use a fully specialized
			 * variant or you will get linker errors.
			 *
			 * @note To retrieve a binary variant, use T = std::string to get it as if it were a UTF-8 encoded string.
			 *
			 * @return The result
			 * @throw std::runtime_error If value cannot be retrieved
			 */
			template <typename T>
			T getValue() const;

			/**
			 * Gets the direct contents of the data buffer regardless of variant interpretation.
			 * For data, this is the actual raw data.  For referenced or allocated data, this is
			 * the bit pattern of the pointer to the data possibly with additional garbage if pointers are
			 * not 64 bits.
			 *
			 * @return the raw bit pattern of the data buffer
			 */
			std::uint64_t getRawBits() const noexcept;

			/**
			 * Sets the raw bit pattern of the variant value data buffer.  Note that if the ownership mode is not Embed,
			 * the bit pattern must represent a pointer to the referenced or allocated data and the data length
			 * must be appropriately specified.  The variant type remains unchanged.
			 *
			 * @param bits The bit pattern
			 * @param type The variant type
			 * @param dataLength The data length
			 * @param ownership The data ownership mode
			 */
			void setRawBits(std::uint64_t bits, Type type, size_t dataLength, Ownership ownership) noexcept;

			/**
			 * Sets this variant to be an integer value.
			 *
			 * @param value The new value
			 */
			void setInteger(std::int64_t value);
		        
			/**
			 * Sets this variant to be an unsinged integer value.
			 *
			 * @param value The new value
			 */
			void setUnsignedInteger(std::uint64_t value);

			/**
			 * Sets this variant to be an allocated string value with the given string.
			 * If the string is 8 bytes or less (including null byte) it will be inlined
			 * into the data buffer.
			 *
			 * @pre The provided pointer is to a C-style null-terminated string.
			 *
			 * @note If the string is short enough (8 chars or less counting null byte) it will be stored inline
			 * in the variant value itself.  Otherwise a new string will be dynamically allocated.
			 * If the string does not have a null byte, one will be appended.
			 *
			 * @param value The string to copy
			 */
			void setText(const cio::Text &value);

			/**
			 * Sets this variant to be an allocated string value with the given string.
			 * If the string is 8 bytes or less (including null byte) it will be inlined
			 * into the data buffer.
			 *
			 * @pre The provided pointer is to a C-style null-terminated string.
			 *
			 * @note If the string is short enough (8 chars or less counting null byte) it will be stored inline
			 * in the variant value itself.  Otherwise a new string will be dynamically allocated.
			 *
			 * @param value The string to copy
			 */
			void setString(const char *value);

			/**
			 * Sets this variant to be an allocated string value with the given string.
			 * If the string is 8 bytes or less (including null byte) it will be inlined
			 * into the data buffer.
			 *
			 * @pre The provided pointer is to a C-style null-terminated string.
			 *
			 * @note If the string is short enough (8 chars or less counting null byte) it will be stored inline
			 * in the variant value itself.  Otherwise a new string will be dynamically allocated.
			 * If the string does not have a null byte, one will be appended.
			 *
			 * @param value The string to copy
			 * @param length The length of the string in bytes
			 */
			void setString(const char *value, std::size_t length);
				
			/**
			 * Sets this variant to be an allocated string value with the given string.
			 *
			 * @note If the string is short enough (8 chars or less counting null byte) it will be stored inline
			 * in the variant value itself.  Otherwise a new string will be dynamically allocated.
			 *
			 * @param value The string to copy
			 */
			void setString(const std::string &value);

			/**
			 * Sets this variant to be a binary value using the given input binary buffer.
			 *
			 * @param bytes The binary byte
			 * @param len The number of bytes 
			 */
			void setBinary(const void *bytes, std::size_t len);

			/**
			 * Sets this variant to be a double value.
			 *
			 * @param value The new value
			 */
			void setDouble(double value);

			/**
			 * Sets this variant to be a boolean value.
			 *
			 * @param value The new value
			 */
			void setBoolean(bool value);

			void setObject(void *value, const Metaclass *metaclass);
			
			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * usigned integer without changing the variant type.
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(unsigned long long value);

			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * integer without changing the variant type.
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(long long value);

			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * integer without changing the variant type.
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(long value);

			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * integer without changing the variant type.
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(int value);

			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * double without changing the variant type.
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(double value);

			/**
			 * Updates the value of this variant to the nearest native representation of the given
			 * boolean without changing the variant type.
			 *
			 * @note String variants will be set to an string with the contents "true" or "false"
			 *
			 * @param value The new value
			 * @return whether the value could be updated
			 */
			bool update(bool value);

			/**
			 * Updates this variant value with the given string reference.
			 * If this variant value is a string type, it will create an allocated or inlined copy
			 * of the incoming string.  If the variant is a binary type, the given string
			 * will be parsed as a hexadecimal encoding such as produced by getString().
			 *
			 * @pre value must be a C-style null-terminated string
			 *
			 * @param value The new value
			 * @return whether this value could be updated
			 */
			bool update(const char *value);

			/**
			 * Updates this variant value with the given string reference.
			 * If this variant value is a string type, it will create an allocated or inlined copy
			 * of the incoming string.  If the variant is a binary type, the given string
			 * will be parsed as a hexadecimal encoding such as produced by getString().
			 *
			 * @param value The new value
			 * @return whether this value could be updated
			 */
			bool update(const std::string &value);

			/**
			 * Updates this variant value with the given string reference.
			 * If this variant value is a string type, it will create an allocated or inlined copy
			 * of the incoming string.  If the variant is a binary type, the given string
			 * will be parsed as a hexadecimal encoding such as produced by getString().
			 *
			 * @param value The new value
			 * @return whether this value could be updated
			 */
			bool update(const cio::Text &text);

			/**
			 * Tests if the other Variant is logically equivalent to this one.
			 *
			 * This operation checks if both Variants have the same type and value as each other,
			 * ignoring their Variant IDs.  The operator==() checks these and the Variant IDs.
			 * Internally this uses std::memcmp to perform the binary comparison if the two variants
			 * have the same type and data length.
			 *
			 * @warning Double variants are compared using the exact bit pattern, not a tolerance check,
			 * so roundoff errors may affect the results in unexpected ways.
			 *
			 * @param other The Variant to compare against this one
			 * @return True both are of the same type and have the same value, false otherwise
			 */
			bool isSameAs(const Variant &other) const noexcept;
			
			/**
			 * Checks whether the current value of the variant is equal to the given text.
			 * If the variant is also text, a string comparison is directly performed.
			 * Otherwise, the text value of the variant is used for comparison.
			 *
			 * @param text The text to match
			 */
			bool matches(const char *text) const noexcept;

			/**
			 * Prints the variant to the given text buffer.
			 * At most length bytes will be printed.
			 * The actual number of bytes needed is returned.
			 * If fewer bytes are printed than requested, the rest are set to null.
			 *
			 * @param text The text buffer to receive characters
			 * @param length The maximum number of characters to print
			 * @return the actual number of characters needed to print the current value
			 */
			std::size_t print(char *text, std::size_t length) const noexcept;

			/**
			 * Interprets this object in a Boolean context.
			 * This is true if the value is set and can be interpreted as Boolean true.
			 * 
			 * @return whether this object is true
			 */
			explicit operator bool() const noexcept;

		private:
			/** Most negative double that can be safely cast to an int64_t */
			static const double DBL_MIN_INT;

			/** Most positive double that can be safely cast to an int64_t */
			static const double DBL_MAX_INT;

			/** Most positive double that can be safely cast to an uint64_t */
			static const double DBL_MAX_UNSIGNED_INT;

			/** Data union specifies all valid options */
			union
			{
				/** Unsigned integer value */
				unsigned long long mUnsigned;

				/** Integer value */
				long long mInteger;

				/** Real value as 64-bit floating point */
				double mReal;

				/** Boolean value */
				bool mBoolean;

				/** Text value of 7 bytes or less plus null */
				char mText[8];

				/** Blob value of 8 bytes or less */
				std::uint8_t mBlob[8];

				/** Pointer to text value of 8 bytes or more, with or without null */
				char *mTextPointer;

				/** Pointer to blob value of 9 bytes or more */
				std::uint8_t *mBlobPointer;

				/** Object pointer */
				void *mObjectPointer;
			};

			/** Data type */
			Type mType;
			
			/** Ownership */
			Ownership mOwnership;

			/** Size of an individual item in bits */
			std::uint16_t mItemSize;
	
			/** Item count */
			std::uint32_t mItemCount;

			/** Metaclass for generic pointer */
			const cio::Metaclass *mObjectMetaclass;

			/** 
			 * Deallocates the data array without otherwise touching the metadata.
			 * This should only be used by methods which will immediately set the metadata anyways.
			 */
			void clearData() noexcept;
	};
	
	/**
	 * Checks if two Variant objects are equal.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return True if the two are equal, false otherwise
	 */
	CIO_API bool operator==(const Variant &lhs, const Variant &rhs);

	/**
	 * Checks if two Variant objects are different.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return True if the two are different, false otherwise
	 */
	CIO_API bool operator!=(const Variant &lhs, const Variant &rhs);

	/**
	 * Checks if a Variant sorts before another Variant.
	 * If both variants are numeric, then a numeric sort is used.
	 * If at least one variant is text, then a text sort is used.
	 * Otherwise a binary sort is used.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return whether the first variant is less than the second
	 */
	CIO_API bool operator<(const Variant &lhs, const Variant &rhs);

	/**
	 * Checks if a Variant sorts before or equal to another Variant.
	 * If both variants are numeric, then a numeric sort is used.
	 * If at least one variant is text, then a text sort is used.
	 * Otherwise a binary sort is used.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return whether the first variant is less than or equal to the second
	 */
	CIO_API bool operator<=(const Variant &lhs, const Variant &rhs);

	/**
	 * Checks if a Variant sorts after another Variant.
	 * If both variants are numeric, then a numeric sort is used.
	 * If at least one variant is text, then a text sort is used.
	 * Otherwise a binary sort is used.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return whether the first variant is greater than the second
	 */
	CIO_API bool operator>(const Variant &lhs, const Variant &rhs);

	/**
	 * Checks if a Variant sorts after or equal to another Variant.
	 * If both variants are numeric, then a numeric sort is used.
	 * If at least one variant is text, then a text sort is used.
	 * Otherwise a binary sort is used.
	 *
	 * @param lhs The object on the left hand side of the operator
	 * @param rhs The object on the right hand side of the operator
	 * @return whether the first variant is greater than or equal to the second
	 */
	CIO_API bool operator>=(const Variant &lhs, const Variant &rhs);

	/**
	 * Adds two variants.
	 *
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return the negated variant
	 */
	CIO_API Variant operator+(const Variant &lhs, const Variant &rhs);

	/**
	 * Negates a variant. If the value is currently a number, the same number type is preserved.
	 * 
	 * @param lhs The variant to negate
	 * @return the negated variant
	 */
	CIO_API Variant operator-(const Variant &lhs);

	/**
	 * Subtracts two variants.
	 *
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return the negated variant
	 */
	CIO_API Variant operator-(const Variant &lhs, const Variant &rhs);

	/**
	 * Multiplies two variants.
	 *
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return the negated variant
	 */
	CIO_API Variant operator*(const Variant &lhs, const Variant &rhs);

	/**
	 * Divides two variants.
	 *
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return the negated variant
	 */
	CIO_API Variant operator/(const Variant &lhs, const Variant &rhs);

	/**
	 * Determines which of the two variants has the lower value, and returns that as a result.
	 * Unlike operator< this will not consider empty values and only operates on numbers.
	 * 
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return The lower of the two variants
	 */
	CIO_API Variant lower(const Variant &lhs, const Variant &rhs);

	/**
	 * Determines which of the two variants has the higher value, and returns that as a result.
	 * Unlike operator> this will not consider empty values and only operates on number.
	 *
	 * @param lhs The first variant
	 * @param rhs The second variant
	 * @return The higher of the two variants
	 */
	CIO_API Variant upper(const Variant &lhs, const Variant &rhs);

	/**
	 * Prints an Variant in the following format:
	 * Type, Value of the type (e.g. if type is "int" Value would be an integer value)
	 *
	 * @param stream The output stream to write to
	 * @param value The variant to write
	 * @return The output stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Variant &value);

	/**
	 * @copydoc Variant::getInteger()
	 */
	template<>
	std::int64_t CIO_API Variant::getValue() const;

	/**
	 * @copydoc Variant::getUnsignedInteger()
	 */
	template<>
	std::uint64_t CIO_API Variant::getValue() const;

	/**
	 * @copydoc Variant::getBoolean()
	 */
	template<>
	bool CIO_API Variant::getValue() const;

	/**
	 * @copydoc Variant::getDouble()
	 */
	template<>
	double CIO_API Variant::getValue() const;

	/**
	 * @copydoc Variant::getString
	 */
	template<>
	std::string CIO_API Variant::getValue() const;
}

/* Specialize std::common_type so Variant plays nicely with other types */

namespace std
{
	template <typename T>
	struct common_type<cio::Variant, T>
	{
		using type = cio::Variant;
	};

	template <typename T>
	struct common_type<T, cio::Variant>
	{
		using type = cio::Variant;
	};

	template <>
	struct common_type<cio::Variant, cio::Variant>
	{
		using type = cio::Variant;
	};
}

#endif
