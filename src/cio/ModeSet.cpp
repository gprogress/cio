/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "ModeSet.h"

#include <iostream>

namespace cio
{
	std::string ModeSet::print() const
	{
		std::string result;
		result.push_back('{');
		const char *delim = "";

		for (unsigned i = 0; i < static_cast<unsigned>(Mode::Invalid); ++i)
		{
			Mode mode = static_cast<Mode>(i);

			if (this->count(mode))
			{
				result.append(delim);
				result.append(cio::print(mode));
				delim = ", ";
			}
		}

		result.push_back('}');
		return result;
	}

	std::ostream &operator<<(std::ostream &stream, ModeSet modes)
	{
		stream << "{";
		const char *delim = "";

		for (unsigned i = 0; i < static_cast<unsigned>(Mode::Invalid); ++i)
		{
			Mode mode = static_cast<Mode>(i);

			if (modes.count(mode))
			{
				stream << delim << print(mode);
				delim = ", ";
			}
		}

		stream << "}";
		return stream;
	}
}
