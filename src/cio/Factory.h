/*==============================================================================
 * Copyright 2020-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Metaclass.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_FACTORY_H
#define CIO_FACTORY_H

#include "Types.h"

#include "Allocator.h"
#include "Metaclass.h"
#include "Metatypes.h"

#include <memory>

namespace cio
{
	/**
	 * The Factory provides a simple direct factory for allocating and deallocating objects with type-safe
	 * construction, assignment, and destruction semantics. The only state is a pointer to the allocator in use,
	 * typically the global default New allocator, and for the non-trivial specialization the default metaclass to allocate.
	 *
	 * This is primarily used as the backend of container classes to adapt the Metaclass allocators to
	 * constructing and destroying container objects.
	 *
	 * The general template implements the general case where object construction and destruction via the metaclass
	 * Metaclass is needed. Specializations can be implemented to optimize this behavior for specific types.
	 * 
	 * @tparam T The type to allocate and deallocate
	 * @tparam C Whether T is a non-trivial class type where you need to perform full construction/destruction
	 */
	template <typename T>
	class Factory
	{
		public:
			/**
			 * Gets the default created metaclass for this factory type.
			 * 
			 * @return the default created metaclass for type T
			 */
			inline static const Metaclass *getDefaultMetaclass() noexcept
			{
				return &getDeclaredMetaclass<std::remove_cv_t<T>>();
			}

			/**
			 * Default constructor.
			 * Initializes the underlying memory allocator to the default allocator from the
			 * metaclass for type T.
			 * 
			 * This is generally C++ new/delete unless specifically changed.
			 */
			Factory() noexcept :
				mCreatedMetaclass(getDefaultMetaclass()),
				mAllocator(mCreatedMetaclass->getDefaultAllocator())
			{
				// nothing more to do
			}

			/**
			 * Construct with the null allocator.
			 * Initializes the underlying metaclass and memory allocator to null.
			 *
			 * @param nope The null pointer to select this constructor
			 */
			Factory(std::nullptr_t nope) noexcept :
				mCreatedMetaclass(nullptr),
				mAllocator(nullptr)
			{
				// nothing more to do
			}

			/**
			 * Construct with either the default allocator or the null allocator depending on a condition.
			 * 
			 * @param useDefault Whether to use the default allocator
			 */
			explicit Factory(bool useDefault) noexcept :
				mCreatedMetaclass(getDefaultMetaclass()),
				mAllocator(useDefault ? mCreatedMetaclass->getDefaultAllocator() : nullptr)
			{
				// ntohing more to do
			}

			/**
			 * Construct with the given allocator.
			 *
			 * @param allocator The allocator to use
			 */
			inline Factory(Allocator *allocator) noexcept :
				mCreatedMetaclass(getDefaultMetaclass()),
				mAllocator(allocator)
			{
				// nothing more to do
			}

			/**
			 * Construct with the given metaclass and its default allocator.
			 *
			 * @param metaclass The metaclass to use
			 */
			inline Factory(const Metaclass *metaclass) noexcept :
				mCreatedMetaclass(metaclass),
				mAllocator(metaclass ? metaclass->getDefaultAllocator() : nullptr)
			{
				// nothing more to do
			}

			/**
			 * Construct with the given metaclass and allocator.
			 *
			 * @param metaclass The metaclass to use
			 * @param allocator The allocator to use
			 */
			inline Factory(const Metaclass *metaclass, Allocator *allocator) noexcept :
				mCreatedMetaclass(metaclass),
				mAllocator(allocator)
			{
				// nothing more to do
			}

			/**
			 * Copy constructor.
			 *
			 * @param in The factory to copy
			 */
			inline Factory(const Factory<T> &in) noexcept :
				mCreatedMetaclass(in.mCreatedMetaclass),
				mAllocator(in.mAllocator)
			{
				// nothing more to do
			}

			/**
			 * Copies the allocator from another factory type.
			 *
			 * @tparam U The input factory data type
			 * @tparam D The input factory class state
			 * @param in The input factory
			 */
			template <typename U>
			inline Factory(const Factory<U> &in) noexcept :
				mCreatedMetaclass(in.getCreatedMetaclass()),
				mAllocator(in.getAllocator())
			{
				// nothing more to do
			}

			/**
			 * Copies an allocator.
			 *
			 * @param in The allocator to copy
			 * @return this allocator
			 */
			inline Factory<T> &operator=(const Factory<T> &in) = default;

			/**
			 * Copies the allocator from another factory type.
			 *
			 * @tparam U The input factory data type
			 * @tparam D The input factory class state
			 * @param in The input factory
			 * @return this factory
			 */
			template <typename U>
			inline Factory<T> &operator=(const Factory<U> &in) noexcept
			{
				mCreatedMetaclass = in.getCreatedMetaclass();
				mAllocator = in.getAllocator();
				return *this;
			}

			/**
			 * Assigns the null allocator.
			 * Sets the underlying memory allocator to null and resets to the default metaclass.
			 *
			 * @param nope The null pointer to select this assignment
			 * @return this allocator
			 */
			inline Factory<T> &operator=(std::nullptr_t nope) noexcept
			{
				mCreatedMetaclass = nullptr;
				mAllocator = nullptr;
				return *this;
			}

			/**
			 * Assigns the given allocator.
			 * Sets the underlying memory allocator to the given allocator and resets to the default metaclass.
			 *
			 * @param allocator The memory allocator to use
			 * @return this allocator
			 */
			inline Factory<T> &operator=(Allocator *allocator) noexcept
			{
				this->setCreatedMetaclass();
				mAllocator = allocator;
				return *this;
			}

			/**
			 * Assigns the given allocator.
			 * Sets the metaclass and uses its default allocator.
			 *
			 * @param mc The metaclass to use
			 * @return this allocator
			 */
			inline Factory<T> &operator=(const Metaclass *mc) noexcept
			{
				this->setCreatedMetaclass(mc);
				this->setDefaultAllocator();
				return *this;
			}

			/**
			 * Destructor.
			 */
			inline ~Factory() noexcept = default;

			/**
			 * Clears the allocator state.
			 * This restores the allocator to the global default memory allocator,
			 * which is normally C++ new/delete unless otherwise changed.
			 */
			inline void clear() noexcept
			{
				this->setCreatedMetaclass();
				this->setDefaultAllocator();
			}

			/**
			 * Gets the default metaclass for objects created by this class.
			 *
			 * @return the default metaclass for objects created by this class
			 */
			inline const Metaclass *getCreatedMetaclass() const noexcept
			{
				return mCreatedMetaclass;
			}

			/**
			 * Sets the default metaclass for objects created by this class.
			 * This should be the metaclass for T, one of its subclasses, or something else that has a memory-compatible alignment and layout.
			 * This does not modify the default allocator.
			 * 
			 * @param metaclass default metaclass for objects created by this class
			 */
			inline void setCreatedMetaclass(const Metaclass *metaclass) noexcept
			{
				mCreatedMetaclass = metaclass;
			}

			/**
			 * Sets the default metaclass for objects created by this class to the metaclass for type U.
			 * This should be the metaclass for T, one of its subclasses, or something else that has a memory-compatible alignment and layout.
			 * This does not modify the default allocator.
			 * 
			 * @tparam U The class type to allocate
			 */
			template <typename U>
			inline void setCreatedMetaclass() noexcept
			{
				mCreatedMetaclass = &getDeclaredMetaclass<std::remove_cv_t<U>>();
			}

			/**
			 * Sets the default metaclass for objects created by this class to the metaclass for type T.
			 * This does not modify the default allocator.
			 */
			inline void setCreatedMetaclass() noexcept
			{
				mCreatedMetaclass = &getDeclaredMetaclass<std::remove_cv_t<T>>();
			}

			/**
			 * Gets the actual metaclass for the given object.
			 * This gets its runtime metaclass if it is non-null, or the default metaclass otherwise.
			 *
			 * @param value The pointer to check
			 * @return the runtime metaclass for the value
			 */
			inline const Metaclass *getRuntimeMetaclass(const T *value) const noexcept
			{
				return value ? &getPointerMetaclass(value) : mCreatedMetaclass;
			}

			/**
			 * Gets the memory allocator backing this allocator.
			 * This can be nullptr if none is assigned.
			 *
			 * @return the memory allocator
			 */
			inline Allocator *getAllocator() const noexcept
			{
				return mAllocator;
			}

			/**
			 * Sets the memory allocator backing this allocator to the null allocator.
			 *
			 * @param nope The null pointer to select this overload
			 */
			inline void setAllocator(std::nullptr_t nope) noexcept
			{
				mAllocator = nullptr;
			}

			/**
			 * Sets the memory allocator backing this allocator to the default allocator for T's metaclass.
			 */
			inline void setDefaultAllocator() noexcept
			{
				mAllocator = mCreatedMetaclass ? mCreatedMetaclass->getDefaultAllocator() : nullptr;
			}

			/**
			 * Sets the memory allocator backing this allocator to the given allocator.
			 *
			 * @param allocator The allocator to use
			 */
			inline void setAllocator(Allocator *allocator) noexcept
			{
				mAllocator = allocator;
			}

			/**
			 * Allocates uninitialized memory sufficient to hold count objects.
			 *
			 * @param count The number of objects desired
			 * @return the allocated memory
			 */
			inline void *allocate(std::size_t count = 1)
			{
				return mAllocator ? mAllocator->allocate(mCreatedMetaclass->size() * count) : nullptr;
			}

			/**
			 * Allocates and constructs an object of type T.
			 *
			 * @return the allocated value
			 * @throw std::bad_alloc If memory allocation fails
			 */
			inline T *create() const
			{
				return mAllocator ? static_cast<T *>(mAllocator->create(mCreatedMetaclass)) : nullptr;
			}

			/**
			 * Allocates and constructs count objects of type T.
			 * If count is 0, this returns nullptr.
			 *
			 * @param count The number of array elements desired
			 * @return the allocated object array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			inline T *create(std::size_t count) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->create(mCreatedMetaclass, count)) : nullptr;
			}

			/**
			 * Destroys and deallocates a value of type T.
			 * Since this is often used in destructors and move operations, it cannot throw.
			 *
			 * @param data The data
			 * @return nullptr for convenience
			 */
			inline std::nullptr_t destroy(T *data) const noexcept
			{
				return mAllocator ? mAllocator->destroy(this->getRuntimeMetaclass(data), data) : nullptr;
			}

			/**
			 * Destroys and deallocates count values of type T.
			 * Since this is often used in destructors and move operations, it cannot throw.
			 *
			 * @param data The data
			 * @param count The number of elements to destroy
			 * @return nullptr for convenience
			 */
			inline std::nullptr_t destroy(T *data, std::size_t count) const noexcept
			{
				return mAllocator ? mAllocator->destroy(this->getRuntimeMetaclass(data), data, count) : nullptr;
			}

			/**
			 * Allocates and constructs a copy of the given data.
			 *
			 * @param data The value to copy
			 * @return the duplicated value
			 * @throw std::bad_alloc If allocating mAllocator for the duplicate failed
			 */
			T *copy(const T *data) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->copy(this->getRuntimeMetaclass(data), data)) : nullptr;
			}

			/**
			 * Allocates and constructs a copy of the given data.
			 *
			 * @param data The data to copy
			 * @param count The number of data elements to copy
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating mAllocator for the duplicate failed
			 */
			T *copy(const T *data, std::size_t count) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->copy(this->getRuntimeMetaclass(data), data, count)) : nullptr;
			}

			/**
			 * Allocates and constructs a copy of the given data, copying a different number of elements.
			 * If allocated is greater than copied, the additional elements not present in the input are default constructed.
			 *
			 * @param data The data to copy
			 * @param copied The number of data elements to copy
			 * @param allocated The desired number of elements in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			inline T *copy(const T *data, std::size_t copied, std::size_t allocated) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->copy(this->getRuntimeMetaclass(data), data, copied, allocated)) : nullptr;
			}

			/**
			 * Allocates a value, moving the existing value into it..
			 *
			 * @param data The value to move
			 * @return the duplicated value
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			inline T *move(T *data) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->move(this->getRuntimeMetaclass(data), data)) : nullptr;
			}

			/**
			 * Allocates an array, moving some or all of the existing elements into the new array.
			 * If allocated is greater than moved, the additional elements not present in the input are default constructed.
			 *
			 * @param data The data to move
			 * @param moved The number of data elements to move
			 * @param allocated The desired number of elements in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			inline T *move(T *data, std::size_t moved, std::size_t allocated) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->move(this->getRuntimeMetaclass(data), data, moved, allocated)) : nullptr;
			}

			/**
			 * Reallocates an array of count T elements.
			 * If the desired count is 0, this destroys the input and returns nullptr.
			 * If the data input is null or the current count is 0, this default constructs a new array.
			 * The values are moved when possible if resizing requires it.
			 *
			 * @param data The existing data
			 * @param current The current number of elements
			 * @param desired The desired number of elements
			 * @return the reallocated array, or nullptr if desired is 0
			 * @throw std::bad_alloc If new memory needed to be allocated and doing so failed
			 */
			inline T *resize(T *data, std::size_t current, std::size_t desired) const
			{
				return mAllocator ? static_cast<T *>(mAllocator->resize(this->getRuntimeMetaclass(data), data, current, desired)) : nullptr;
			}

			/**
			 * Uses this allocator as a delete function in classes such as std::unique_ptr and std::shared_ptr.
			 * This will delete a single value of type T using its metaclass.
			 *
			 * @param value The value to delete
			 */
			inline void operator()(T *value) const noexcept
			{
				if (mAllocator) mAllocator->destroy(this->getRuntimeMetaclass(value), value);
			}

			/**
			 * Interprets the allocator in a Boolean context.
			 * This should be true if the allocator is in a state where it can function.
			 * The base class always returns true.
			 *
			 * @return whether the allocator is usable
			 */
			inline explicit operator bool() const noexcept
			{
				return mAllocator != nullptr;
			}

		private:
			/**
			 * The default class type to allocate if not explicitly specified.
			 */
			const Metaclass *mCreatedMetaclass;

			/**
			 * The underlying memory allocator used by this allocator.
			 */
			Allocator *mAllocator;
	};
}

#endif
