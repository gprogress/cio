/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_PROGRAM_H
#define CIO_ZIP_PROGRAM_H

#include "Types.h"

#include <cio/Action.h>
#include <cio/Path.h>

#include <iosfwd>
#include <memory>
#include <utility>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace zip
	{
		/**
		 * The CIO Zip Program provides an implementation of a command line program intended to emulate
		 * a subset of Unix tar for working with archives. It is usable programmatically via CIO Zip C++ API
		 * as well as via the cio-zip executable (if built).
		 * 
		 * Unlike Unix tar, CIO Zip Program can work with any input that CIO considers a directory.
		 * Currently this includes: Zip Archives and native filesystem directories.
		 */
		class CIO_ZIP_ABI Program
		{
			public:
				/**
				 * Gets the metaclass for this class.
				 * 
				 * @return the metaclass for this class
				 */
				const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct the empty program.
				 */
				Program() noexcept;

				/**
				 * Construct a program and parse the given command line.
				 * Per convention the first argument is the name of the program and the rest are actual arguments.
				 * 
				 * @param argc The number of arguments
				 * @param argv The list of arguments
				 */
				Program(int argc, char **argv);

				/**
				 * Copy constructor. Both copies will share the same archive if open.
				 * 
				 * @param in The program to copy
				 */
				Program(const Program &in);

				/**
				 * Move constructor.
				 * 
				 * @param in The program to move
				 */
				Program(Program &&in) noexcept;

				/**
				 * Copy assignment. Both copies will share the same archive if open.
				 * 
				 * @param in The program to copy|
				 * @return this program
				 */
				Program &operator=(const Program &in);

				/**
				 * Move assignment.
				 * 
				 * @param in The program to move
				 * @return this progrma
				 */
				Program &operator=(Program &&in) noexcept;

				/**
				 * Destructor.
				 */
				~Program() noexcept;

				/**
				 * Clears program state.
				 */
				void clear() noexcept;

				/**
				 * Gets the metaclass for this object.
				 * 
				 * @return the metaclass
				 */
				const Metaclass &getMetaclass() const noexcept;

				/**
				 * Parses the given command line to configure this program.
				 * Per convention the first argument is the name of the program and the rest are actual arguments.
				 *
				 * @param argc The number of arguments
				 * @param argv The list of arguments
				 */
				void parse(int argc, char **argv);

				/**
				 * Prints the command line help to the given stream.
				 * 
				 * @param s The stream
				 * @throw std::exception If the stream throws an exception
				 */
				void help(std::ostream &s);

				/**
				 * Executes the currently configured program state.
				 * This will look at the directed action and call the appropriate methods.
				 */
				void execute();

				/**
				 * Opens the configured archive input.
				 * If a path is specified, it will be used with the protocol to open the input.
				 */
				void open();

				/**
				 * Extracts the named list of entries relative to the specified working directories.
				 * If no entries are specified, all entires will be extracted.
				 * Any entries with the empty working directory will use the current system working directory.
				 */
				void extract();

				/**
				 * Extracts the named list of entries relative to the specified working directories.
				 * If no entries are specified, this method does nothing.
				 * Any entries with the empty working directory will use the current system working directory.
				 */
				void extractSelected();

				/**
				 * Extracts all entries relative to the last specified working directory.
				 * If no working directory was specified, entires will be extracted relative to the current system working directory.
				 */
				void extractAll();

				/**
				 * Lists the entries in the current archive.
				 */
				void list(std::ostream &printer);

				/**
				 * Gets the Protocol Factory that may be used by this Program.
				 *
				 * @return the protocol factory in use
				 */
				const ProtocolFactory *getProtocolFactory() const noexcept;

				/**
				 * Sets the Protocol Factory that may be used by this Program.
				 *
				 * @param factory the protocol factory in use
				 */
				void setProtocolFactory(const ProtocolFactory *factory) noexcept;

			private:
				/** The metaclass for this class */
				static Class<Program> sMetaclass;

				/** The primary directed action for this program */
				Action mAction;

				/** The archive path if specified */
				Path mArchivePath;

				/** The currently open archive directory */
				std::shared_ptr<cio::Protocol> mArchive;

				/** Last specified working directory */
				Path mWorkingDirectory;

				/** Map of working directories to archive entries to process */
				std::vector<std::pair<Path, std::vector<Path>>> mSelectedEntries;

				/** Protocol factory to use */
				const ProtocolFactory *mFactory;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#pragma warning(disable:4251)
#endif

#endif
