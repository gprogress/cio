/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_PROTOCOLFAMILY_H
#define CIO_NET_PROTOCOLFAMILY_H

#include "Types.h"

#include "ChannelType.h"

#include <string>

#include <cio/Text.h>
#include <cio/Validation.h>

namespace cio
{
	namespace net
	{
		/**
		 * The CIO Net Protocol Family provides an abstraction of the native Protocol Family (PF) identifers
		 * used by the native socket library API. The native Protocol Familys are typically integer codes
		 * from a set of predefined constants. However, which constants are available and what their values are vary by platform.
		 * This class provides baseline ability to always get, set, print, and parse the most common protocols (currently TCP and UDP).
		 * Additional native protocols may be valid and passed through as raw integers.
		 */
		class CIO_NET_API ProtocolFamily
		{
			public:
				/**
				 * Gets the Protocol Family for the TCP/IP protocol.
				 *
				 * @return the TCP/IP Protocol Family
				 */
				static ProtocolFamily tcp();

				/**
				 * Gets the Protocol Family for the UDP/IP protocol.
				 *
				 * @return the UDP/IP Protocol Family
				 */
				static ProtocolFamily udp();

				/**
				 * Construct a Protocol Family without setting the constant.
				 */
				inline ProtocolFamily() noexcept;

				/**
				 * Construct a Protocol Family with the given native OS constant.
				 *
				 * @param code The native OS constant
				 */
				inline explicit ProtocolFamily(int code) noexcept;

				/**
				* Sets the given native OS constant.
				*
				* @param code The native OS constant
				*/
				inline void set(int code) noexcept;

				/**
				 * Parses the given null-terminated text to see if a built-in Protocol Family is recognized.
				 * Currently this only recognizes "tcp" and "udp" and the Resolver must be used for any other Protocol Familys.
				 *
				 * @param text The text to parse
				 * @return the validation results of parsing
				 */
				Validation parse(const char *text) noexcept;

				/**
				 * Parses the given text to see if a built-in Protocol Family is recognized.
				 * Currently this only recognizes "tcp" and "udp" and the Resolver must be used for any other Protocol Familys.
				 *
				 * @param text The text to parse
				 * @param len The number of bytes in the text
				 * @return the validation results of parsing
				 */
				Validation parse(const char *text, std::size_t len) noexcept;

				/**
				 * Parses the given text to see if a built-in Protocol Family is recognized.
				 * Currently this only recognizes "tcp" and "udp" and the Resolver must be used for any other Protocol Familys.
				 *
				 * @param text The text to parse
				 * @return the validation results of parsing
				 */
				Validation parse(const std::string &text) noexcept;

				/**
				 * Gets the native OS constant for the Protocol Family.
				 *
				 * @return the native OS constant value
				 */
				inline int get() const noexcept;

				/**
				 * Prints the Protocol Family to a text buffer.
				 * If the buffer is not large enough, the output will be truncated but the returned count will specify how many were needed.
				 * If the buffer is larger than needed, the text will be null-terminated.
				 *
				 * @param text The text buffer to print to
				 * @param len The number of bytes in the buffer
				 * @return the number of bytes actually needed to print this Protocol Family
				 */
				std::size_t print(char *text, std::size_t len) const noexcept;

				/**
				 * Prints the Protocol Family to a string and returns it.
				 *
				 * @return the printed text
				 * @throw std::bad_alloc If not enough memory could be allocated for the string
				 */
				std::string print() const;

				/**
				 * Clears the Protocol Family and sets it to the invalid value.
				 */
				inline void clear() noexcept;

				/**
				 * Checks whether the Protocol Family has been set when used in a Boolean context.
				 *
				 * @return whether the Protocol Family has been set
				 */
				inline explicit operator bool() const noexcept;

				/**
				 * Gets the channel type most normally associated with this Protocol Family.
				 *
				 */
				ChannelType getChannelType() const noexcept;

				/**
				 * Checks whether the protocol family matches TCP.
				 * 
				 * @return whether this is the TCP protocol family
				 */
				bool isTcp() const noexcept;

				/**
				 * Checks whether the protocol family matches UDP.
				 *
				 * @return whether this is the UDP protocol family
				 */
				bool isUdp() const noexcept;

			private:
				/** The native OS constant for the Protocol Family */
				int mCode;
		};

		/**
		 * Checks to see if two protocol families are equal.
		 * 
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether they are equal
		 */
		CIO_NET_API bool operator==(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Checks to see if two protocol families are not equal.
		 *
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether they are not equal
		 */
		CIO_NET_API bool operator!=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Checks to see if one protocol family sorts before another.
		 * The sort order is based on text label to ensure it is platform independent.
		 * 
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether the first sorts before the second
		 */
		CIO_NET_API bool operator<(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Checks to see if one protocol family sorts before or equal to another.
		 * The sort order is based on text label to ensure it is platform independent.
		 *
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether the first sorts before or equal to the second
		 */
		CIO_NET_API bool operator<=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Checks to see if one protocol family sorts after another.
		 * The sort order is based on text label to ensure it is platform independent.
		 *
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether the first sorts after the second
		 */
		CIO_NET_API bool operator>(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Checks to see if one protocol family sorts after or equal to another.
		 * The sort order is based on text label to ensure it is platform independent.
		 *
		 * @param left The first protocol family
		 * @param right The second protocol family
		 * @return whether the first sorts after or equal to the second
		 */
		CIO_NET_API bool operator>=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept;

		/**
		 * Prints the Protocol Family to a C++ stream.
		 *
		 * @param stream The stream to print to
		 * @param type The Protocol Family
		 * @return the stream
		 */
		CIO_NET_API std::ostream &operator<<(std::ostream &stream, const ProtocolFamily &type);
	}
}

/* Inline implementation */

namespace cio
{
	namespace net
	{
		inline ProtocolFamily::ProtocolFamily() noexcept :
			mCode(0)
		{
			// nothing more to do
		}

		inline ProtocolFamily::ProtocolFamily(int code) noexcept :
			mCode(code)
		{
			// nothing more to do
		}

		inline int ProtocolFamily::get() const noexcept
		{
			return mCode;
		}

		inline void ProtocolFamily::set(int code) noexcept
		{
			mCode = code;
		}

		inline void ProtocolFamily::clear() noexcept
		{
			mCode = 0;
		}

		inline ProtocolFamily::operator bool() const noexcept
		{
			return mCode != 0;
		}
	}
}

#endif

