/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CASE_H
#define CIO_CASE_H

#include "Types.h"

#include "Enumeration.h"

#include <cstring>
#include <string>

#if defined _WIN32
/** Platform macro to map to native case-insensitive string compare */
#define CIO_STRCASECMP _stricmp
/** Platform macro to map to native case-insensitive string compare with max length */
#define CIO_STRNCASECMP _strnicmp
#else
/** Platform macro to map to native case-insensitive string compare */
#define CIO_STRCASECMP strcasecmp
/** Platform macro to map to native case-insensitive string compare with max length */
#define CIO_STRNCASECMP strncasecmp
#endif

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Case enumeration describes how to handle differences in text case.
	 */
	enum class Case : std::uint8_t
	{
		/** Case should not be preserved nor used in calculations, but no guidance is given on what the case should be. */
		Insensitive = 0,
		
		/** Case should be forced to lower case always. Comparisons should always ignore case */
		Lower = 1,

		/** Case should be forced to upper case always. Comparisons should always ignore case */
		Upper = 2,

		/** Case should be preserved but not used in calculations. Differences in case are purely stylistic but don't imply a difference in value. */
		Preserve = 3,
		
		/** Case should both be preserved and used in calculations. Differences in case imply differences in value. */
		Sensitive = 4
	};

	/**
	 * Specializes Enumeration for the C++ Case enum.
	 */
	template <>
	class CIO_API Enumeration<Case> final : public Enumerants<Case>
	{
		public:
			/** The array of enum values */
			static const Enumerant<Case> values[];

			/** The enumeration global instance */
			static const Enumeration<Case> instance;

			/** Constructor */
			Enumeration();

			/** Destructor */
			virtual ~Enumeration() noexcept override;
	};

	/**
	 * CaseInsensitive is a compile-time constant type used to represent compile-time Case Insensitive values.
	 */
	using CaseInsensitive = std::integral_constant<Case, Case::Insensitive>;

	/**
	 * Lowercase is a compile-time constant type used to represent compile-time Case::Upper values.
	 */
	using Lowercase = std::integral_constant<Case, Case::Lower>;
	
	/**
	 * Uppercase is a compile-time constant type used to represent compile-time Case::Lower values.
	 */
	using Uppercase = std::integral_constant<Case, Case::Upper>;

	/**
	 * PreserveCase is a compile-time constant type used to represent compile-time Preserve Case values.
	 */
	using PreserveCase = std::integral_constant<Case, Case::Preserve>;
	
	/**
	 * CaseSensitive is a compile-time constant type used to represent compile-time Case Sensitive values.
	 */
	using CaseSensitive = std::integral_constant<Case, Case::Sensitive>;

	/**
	 * Check to see whether the given text data buffer is null-terminated within an expected number of characters.
	 *
	 * @param text The text data buffer
	 * @param length The maximum number of characters to consider in the buffer
	 * @return whether there was a null terminator in the text buffer
	 */
	inline bool isTerminated(const char *text, std::size_t length) noexcept
	{
		std::size_t i = 0;
		while (i < length && text[i])
		{
			++i;
		}
		return i < length;
	}

	/**
	 * Gets the character offset in the given text data buffer that contains the first null terminator.
	 *
	 * @param text The text data buffer
	 * @param length The maximum number of characters to consider in the buffer
	 * @return the character offset containing the null terminator, or length if none was found
	 */
	inline std::size_t getTerminator(const char *text, std::size_t length) noexcept
	{
		const char *current = text;
		const char *end = text + length;
		while (current < end && *current)
		{
			++current;
		}
		return current - text;
	}

	/**
	 * Performs a case-sensitive three-way comparison on two null-terminated strings.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal, 0 is returned.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareText(const char *left, const char *right) noexcept
	{
		return std::strcmp(left, right);
	}

	/**
	 * Performs a case-insensitive three-way comparison on two strings considering at most the given number of characters.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal for that number of characters, 0 is returned.
	 *
	 * The strings do not have to be null-terminated as long as they are at least the given number of characters.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param maxLength The maximum number of characters to compare
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareText(const char *left, const char *right, std::size_t length) noexcept
	{
		return std::strncmp(left, right, length);
	}

	/**
	 * Performs a case-insensitive three-way comparison on two null-terminated strings.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal, 0 is returned.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareTextCaseInsensitive(const char *left, const char *right) noexcept
	{
		return CIO_STRCASECMP(left, right);
	}

	/**
	 * Performs a case-insensitive three-way comparison on two strings considering at most the given number of characters.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal for that number of characters, 0 is returned.
	 *
	 * The strings do not have to be null-terminated as long as they are at least the given number of characters.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param maxLength The maximum number of characters to compare
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareTextCaseInsensitive(const char *left, const char *right, std::size_t maxLength) noexcept
	{
		return CIO_STRNCASECMP(left, right, maxLength);
	}

	/**
	 * Performs a three-way comparison on two null-terminated strings using the specified Case rules.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal, 0 is returned.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param mode The case rules to use for the comparison
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareText(const char *left, const char *right, Case mode) noexcept
	{
		return mode == Case::Sensitive ? std::strcmp(left, right) : CIO_STRCASECMP(left, right);
	}

	/**
	 * Performs a three-way comparison on two strings considering at most the given number of characters using the specified Case rules.
	 * If the first string is before the second, a negative value is returned.
	 * If the first string is after the second, a positive value is returned.
	 * If they are equal for that number of characters, 0 is returned.
	 *
	 * The strings do not have to be null-terminated as long as they are at least the given number of characters.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param mode The case rules to use for the comparison
	 * @param maxLength The maximum number of characters to compare
	 * @return the comparison result (negative for less, positive for greater, 0 for equal)
	 */
	inline int compareText(const char *left, const char *right, Case mode, std::size_t maxLength) noexcept
	{
		return mode == Case::Sensitive ? std::strncmp(left, right, maxLength) : CIO_STRNCASECMP(left, right, maxLength);
	}

	/**
	 * Checks whether two null-terminated strings are equal.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @return whether the strings are equal
	 */
	inline bool equalText(const char *left, const char *right) noexcept
	{
		return std::strcmp(left, right) == 0;
	}

	/**
	 * Checks whether two strings are equal up to the given number of characters.
	 * The strings do not need to be null terminated so long as they are at least length characters.
	 * The comparison will stop at the null terminators if present.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param maxLength The maximum number of characters to compare
	 * @return whether the strings are equal for that many characters
	 */
	inline bool equalText(const char *left, const char *right, std::size_t length) noexcept
	{
		return std::strncmp(left, right, length) == 0;
	}

	/**
	 * Checks whether two strings are equal up to the given number of characters using the given case rules.
	 * The strings do not need to be null terminated so long as they are at least length characters.
	 * The comparison will stop at the null terminators if present.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param mode The case rules to use for the comparison
	 * @param length The maximum number of characters to compare
	 * @return whether the strings are equal for that many characters
	 */
	inline bool equalText(const char *left, const char *right, Case mode, std::size_t length) noexcept
	{
		return (mode == Case::Sensitive ? std::strncmp(left, right, length) : CIO_STRNCASECMP(left, right, length)) == 0;
	}

	/**
	 * Checks whether two null-terminated strings are equal while ignoring case.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @return whether the strings are equal ignoring case
	 */
	inline bool equalTextInsensitive(const char *left, const char *right) noexcept
	{
		return CIO_STRCASECMP(left, right) == 0;
	}
	
	/**
	 * Checks whether two potentially null-terminated strings are equal while ignoring case.
	 *
	 * @param left The first string
	 * @param right The second string
	 * @param maxLength The maximum number of characters to compare
	 * @return whether the strings are equal ignoring case
	 */
	inline bool equalTextInsensitive(const char *left, const char *right, std::size_t maxLength) noexcept
	{
		return CIO_STRNCASECMP(left, right, maxLength) == 0;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
