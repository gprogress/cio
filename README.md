# README #

CIO - the Careful I/O Library

### What is this repository for? ###

CIO - The Careful I/O Library - provides a streamlined LPGL open source C++ object-oriented library to perform common binary and text I/O workflows.

The C++ standard library is woefully lacking in this regard, and the existing C and C++ I/O subsystems are clunky and don't
perform ideally on every platform. For this library, we use inspiration from other languages such as Java and C# combined with
the native code and memory performance capabilities of C++ to provide a comprehensive high-performance interface for I/O workflows.
This library also includes a few other support classes to deal with platform abstractions not available in standard C++-11 but useful
for execution speed and factory-based service APIs.

Note that this library has a limited scope to basic I/O workflows not involving application schemas or data model semantics.
If data modeling capabilities are of interest to your application, consider the Geosemantic Data Exchange (Geodex) library built on
top of CIO to meet your needs.

The class hierarchy of CIO provides the following general types of capabilities:
* Metatype API for providing basic opt-in reflection services for C++ classes, used internally for service factories and buffer management
* Memory buffer management through unstructured byte arrays and structured binary accessors describing bit-level numeric layouts
* Vector/Matrix/Tensor overlays on memory buffers to provide structured dimensional access to binary data (planned)
* Text management for managing platform differences regarding newlines, printing and parsing primitive types, and organizing buffers into logical text
* Platform abstractions of multi-threading, dynamic libraries, and other foundational capabilities (planned)
* Transport-specific classes for optimal read/write of specific I/O use cases including files, serial devices, networks, and more
* Encoders and decoders for general-purpose transport alterations such as compression and encryption (planned)
* Transport-agnostic interface for efficient binary streaming inputs and outputs over any supported transport layer and codec
* Resource API and discovery interfaces for working with resource hierarchies over any supported transport protocol
* Structured text and binary markup (i.e. XML) and similarly generic data format readers and writers over any supported transport protocol

There are also a few commonly used specific applications of I/O workflows included for the purposes of the library unit testing itself:
* Logger API for capturing structured messages organized by origin and severity
* Test API for creating and running hierarchical test suites and reporting fatal and non-fatal faults

The unit test framework is designed to be easy to use and support multi-threaded test execution with precise control over sequential vs. parallel stages
so it may be useful to other applications as well.

The current and planned CIO transport layers:
* Local filesystem support for files, directories, pipes, serial ports, and other platform-specific hardware devices using native host OS APIs
* Memory buffer adapters to service in-memory operations with transport API 
* Bluetooth transport via RFCOMM and L2CAP, device abstractions via HCI (currently Linux-only using BlueZ API, Windows support planned)
* TCP/IP and UDP/IP network connections using native host OS APIs (planned)
* HTTP and HTTPS network connections overlayed on compatible transports (planned)
* SSH, SFTP, SCP capabilities overlayed on compatible transports (planned)
* Deflate codec overlay on transports (planned using zlib)
* Bzip2 codec overlay on transports (planned using libbz2)
* LZMA codec overlay on transports (planned using liblzma)

The current and planned CIO resource discovery layers:
* Local filesystem directory and device discovery
* Discovery of anonymous and shared memory chunks (anonymous via pointer address implemented, shared memory planned)
* Network resource discovery over all supported connection types (planned)
* Bluetooth resource discovery via SDP and HCI
* Archive content resource discovery for Zip, Zip64, Tar, and 7-Zip archives (planned)

The current and planned CIO data format readers and writers
* XML streaming push, pull, and DOM parsing (implemented using libexpat in asynchronous thread)
* JSON streaming push, pull, and DOM parsing (planned)
* Protocol Buffer 3.x streaming push, pull, and DOM parsing (planned) 

### How do I get set up? ###

CIO is a conventional CMake project that should cleanly compile on any C++14 compiler with CMake 3.0 or newer
following the standard instructions for a CMake build process. 

In most cases, this means:
* Create a build directory (conventionally "build" but can be anywhere)
* From that build directory, run cmake (or launch the CMake GUI) with the top-level CIO directory as the source directory
* Specify all configuration variables such as CMAKE_BUILD_TYPE, CMAKE_INSTALL_PREFIX, and any hints to find dependencies
* Configure and generate your native build system (typically a Visual Studio IDE on Windows and Makefiles everywhere else)
* Use your native build system as normal

Currently tested configurations include:
* Windows 10 x64 with Visual Studio 2017 IDE, Visual Studio 2019 IDE, and MinGW GCC 10 using MinGW Makefiles
* Ubuntu Linux 18.04 amd64 and aarch64 with GCC 7.5 using Makefiles
* Ubuntu Linux 20.10 amd64 with GCC 10 using Makefiles

CIO has no mandatory dependencies, but does use some optional dependencies for specific components
* libexpat 2.0 or higher for XML read/write
* BlueZ libbluetooth for Bluetooth support on Linux

All CIO optional components may be enabled or disabled via CMake cache variables. The external dependency locations may also be
specified by CMake variables to provide hints to the CMake Find scripts. 

The following CMake Cache variables are particularly useful:
* CIO_BUILD_SHARED_LIBS - if true, build CIO as a shared (DLL/so) library, if false build as a static library
* CMAKE_BUILD_TYPE - for non-IDE platforms, set Release, Debug, or RelWithDebInfo build type (no effect on IDE builds) per CMake norm 
* CMAKE_INSTALL_PREFIX - final install directory to copy all built artifacts, per CMake norm
* CIO_BUILD_BLUETOOTH - enable or disable Bluetooth transport layer if libbluetooth is found
* CIO_BUILD_XML - enable or disable XML read/write if libexpat is found 
* CIO_BUILD_TEST - enable or disable Test API
* EXPAT_DIR - base directory contining lib, bin, and include directories of libexpat if in a non-standard location
* BLUETOOTH_DIR - base directory containing lib, bin, and include directories of libbluetooth if in a non-standard location

### Contribution guidelines ###

We welcome contributions to CIO, particularly for implementing transport layers, codecs, or resource discovery layers.
All contributions should be submitted as a pull request where they will be reviewed and any concerns addressed.
The contributions must cleanly compile on standard C++11 compilers, provide a general utility related to I/O workflows, be licensed
under the LGPL v3 or a compatible license such as MIT/BSD, and (where applicable) assign the copyright to Geometric Progress LLC.

Exceptions to the guidelines can be discussed on a case by case basis for unusual circumstances.

### Who do I talk to? ###

CIO is owned and maintained by Geometric Progress LLC. Please contact admin@geometricprogress.com for more information.
