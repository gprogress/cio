/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_READERHANDLER_H
#define CIO_XML_READERHANDLER_H

#include "Types.h"

#include "Event.h"
#include "Handler.h"

#include <condition_variable>
#include <deque>
#include <mutex>
#include <utility>
#include <stdexcept>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		class CIO_XML_API ReaderHandler : public Handler
		{
			public:
				ReaderHandler();

				~ReaderHandler() override;

				/**
				 * Called when a new document is parsed.
				 *
				 * @throw std::exception To end parsing
				 */
				virtual void startDocument() override;

				/**
				 * Called after a document has been fully parsed.
				 *
				 * @throw std::exception To notify the user of a delayed parse error
				 */
				virtual void endDocument() override;

				/**
				 * Called when a new element is started.
				 *
				 * @param element The element that just started
				 * @throw std::exception To end parsing
				 */
				virtual void startElement(Text element) override;

				/**
				 * Called when an element is ended.
				 *
				 * @param element The element that just ended
				 * @throw std::exception To end parsing
				 */
				virtual void endElement(Text element) override;

				/**
				 * Processes a single attribute for the current element.
				 *
				 * @param key The attribute key
				 * @param value The attribute value
				 * @throw std::exception To end parsing
				 */
				virtual void processAttribute(Text key, Text value) override;

				/**
				 * Processes a comment.
				 *
				 * @param comment The comment text
				 * @throw std::exception To end parsing
				 */
				virtual void processComment(Text comment) override;

				/**
				 * Process a chunk of text.  The text might not be null-terminated.
				 * It might not be the entire text under the current element.
				 *
				 * @param text The text to process
				 * @throw std::exception To end parsing
				 */
				virtual void processTextChunk(Text text) override;

				/**
				 * Handle a parser error.
				 *
				 * @param message The error message
				 * @throw std::exception To end parsing
				 */
				virtual void handleError(Text message) override;

				void setEventQueueSize(std::size_t capacity);

				std::size_t getEventQueueSize() const;

				Event peekNextEvent() const;

				Event peekNextEventType() const;

				Event takeNextEvent();

				Event takeNextEvent(Type type);

				Event takeNextEvent(Type type, EventType etype);

				void skipCurrentAttributes();

				void skipCurrentElement();

				Event skipToTakeNextElementStart();

				Event skipToNextChild();

				Event skipToNextChildElement();

				void cancel();

				explicit operator bool() const;

			private:
				mutable std::mutex mMutex;
				mutable std::condition_variable mWaitCondition;
				std::deque<Event> mEvents;
				std::size_t mEventCapacity;
				bool mCancelParse;
				bool mIsDone;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
