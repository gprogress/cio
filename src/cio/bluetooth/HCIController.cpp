/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "HCIController.h"

#include "Packet.h"
#include "Event.h"
#include "HCICodec.h"
#include "LowEnergyEvent.h"
#include "Peripheral.h"

#include <cio/net/AddressFamily.h>
#include <cio/net/Channel.h>
#include <cio/net/ChannelType.h>
#include <cio/net/ProtocolFamily.h>

#include <cio/DeviceAddress.h>
#include <cio/Exception.h>
#include <cio/Listener.h>
#include <cio/Class.h>
#include <cio/State.h>

#include <unistd.h>

#if defined CIO_HAVE_IOCTL_H
#include <sys/ioctl.h>
#endif

#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

namespace cio
{
	namespace bluetooth
	{
		Class<HCIController> HCIController::sMetaclass("cio::bluetooth::HCIController");
		
		HCICodec HCIController::sDefaultCodec;
		
		const Metaclass &HCIController::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		cio::net::Socket HCIController::makeSocket()
		{
			return cio::net::Socket(family(), channel(), protocol());
		}
						
		cio::net::AddressFamily HCIController::family() noexcept
		{
			// AF_BLUETOOTH
			return cio::net::AddressFamily(31);
		}
				
		cio::net::ChannelType HCIController::channel() noexcept
		{
			return cio::net::ChannelType::Raw;
		}
				
		cio::net::ProtocolFamily HCIController::protocol() noexcept
		{
			// BTPROTO_HCI
			return cio::net::ProtocolFamily(1);
		}
		
		HCIController::HCIController() noexcept :
			mResponseTimeout(1000),
			mRequestRetryCount(0),
			mScanCount(0),
			mCodec(&sDefaultCodec)
		{
			// nothing more to do
		}
			
		HCIController::HCIController(const Device &in) :
			Controller(in),
			mResponseTimeout(1000),
			mRequestRetryCount(0),
			mScanCount(0),
			mCodec(&sDefaultCodec)
		{
			this->executeDeviceOpen();
		}	
		
		HCIController::HCIController(HCIController &&in) noexcept :
			Controller(std::move(in)),
			mChannel(std::move(in.mChannel)),
			mResponseTimeout(in.mResponseTimeout),
			mRequestRetryCount(in.mRequestRetryCount),
			mScanCount(in.mScanCount),
			mDeviceBuffer(std::move(in.mDeviceBuffer)),
			mWriteBuffer(std::move(in.mWriteBuffer)),
			mReadBuffer(std::move(in.mReadBuffer)),
			mCodec(in.mCodec)
		{
			in.mScanCount = 0;
			in.mCodec = &sDefaultCodec;
		}
			
		HCIController &HCIController::operator=(HCIController &&in) noexcept
		{
			if (this != &in)
			{
				Controller::operator=(std::move(in));
				
				mChannel = std::move(in.mChannel);
				
				mResponseTimeout = in.mResponseTimeout;
				mRequestRetryCount = in.mRequestRetryCount;
				
				mScanCount = in.mScanCount;
				in.mScanCount = 0;
				
				mDeviceBuffer = std::move(in.mDeviceBuffer);
				mWriteBuffer = std::move(in.mWriteBuffer);
				mReadBuffer = std::move(in.mReadBuffer);
				
				mCodec = in.mCodec;
				in.mCodec = &sDefaultCodec;
			}
			
			return *this;
		}
			
		HCIController::~HCIController() noexcept = default;
		
		const Metaclass &HCIController::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void HCIController::clear() noexcept
		{
			mCodec = &sDefaultCodec;
			mChannel.clear();
			mDeviceBuffer.clear();
			mWriteBuffer.clear();
			mReadBuffer.clear();
			
			mResponseTimeout = std::chrono::milliseconds(1000);
			mRequestRetryCount = 0;
			
			Controller::clear();
		}
		
		void HCIController::openCurrentDevice()
		{
			if (!mChannel)
			{
				this->executeDeviceOpen();
			}
		}
		
		void HCIController::executeDeviceOpen()
		{
			cio::net::Socket socket = HCIController::makeSocket();
			const Device &device = this->getDevice();
			
			if (device)
			{			
				// struct sockaddr_hci :)
				std::uint16_t addressBytes[3] = {
					static_cast<std::uint16_t>(family().get()),
					static_cast<std::uint16_t>(device.getIndex()),
					0
				};
				
				socket.bind(addressBytes, 6);
			}
			
			mChannel.adopt(std::move(socket));
			mChannel.setReadTimeout(mResponseTimeout);
			
			std::size_t psize = this->getMaxPacketSize();
			mReadBuffer.resize(psize);
			mWriteBuffer.resize(psize);
		}
						
		void HCIController::close() noexcept
		{
			Controller::close();
		
			mChannel.clear();
			mDeviceBuffer.clear();
			mReadBuffer.clear();
			mWriteBuffer.clear();
		}
		
		cio::net::Channel &HCIController::getChannel() noexcept
		{
			return mChannel;
		}
		
		const cio::net::Channel &HCIController::getChannel() const noexcept
		{
			return mChannel;
		}
		
		void HCIController::adoptChannel(cio::net::Channel &&channel) noexcept
		{
			mChannel = std::move(channel);
			if (mChannel)
			{
				std::size_t psize = this->getMaxPacketSize();
				mReadBuffer.resize(psize);
				mWriteBuffer.resize(psize);
				mChannel.setReadTimeout(mResponseTimeout);
			}
			else
			{
				mReadBuffer.clear();
				mWriteBuffer.clear();
			}
		}
	
		std::size_t HCIController::getMaxPacketSize() const noexcept
		{
			return 260;
		}
		
		Device HCIController::findDefaultDevice() noexcept
		{
			Device device;
			
			try
			{
				std::lock_guard<std::mutex> lock(mMutex);

				std::size_t count = this->refreshLocalDevices();
				if (count > 0)
				{
					device = mCodec->decodeNextDevice(mDeviceBuffer);
				}
			}
			catch (...)
			{
				// nothing to do, just return empty device
			}
			return device;
		}
		
		std::vector<Device> HCIController::discoverLocalDevices()
		{
			std::vector<Device> devices;
			
			std::lock_guard<std::mutex> lock(mMutex);


			std::size_t count = this->refreshLocalDevices();
			
			devices.resize(count);
			for (std::size_t i = 0; i < count; ++i)
			{
				devices[i] = mCodec->decodeNextDevice(mDeviceBuffer);
			}
			
			return devices; 
		}
		
		std::size_t HCIController::refreshLocalDevices()
		{
			// Setup request memory
			mCodec->reserveLocalDeviceList(mDeviceBuffer);

#if defined CIO_HAVE_IOCTL_H
			// Execute Linux ioctl 
			int dd = this->getHandle();
			const int opcode = _IOR('H', 210, int); // HCIGETDEVLIST
			int result = ::ioctl(dd, opcode, mDeviceBuffer.current());
			
			if (result < 0)
			{
				throw cio::Exception(cio::status(), "Could not scan for local Bluetooth devices");
			}
#else
			// No platform backend
			throw cio::Exception(Reason::Unsupported, "Could not scan for local Bluetooth devices");
#endif
			return mCodec->decodeDeviceListHeader(mDeviceBuffer);
		}
		
		std::size_t HCIController::readLocalName(char *text, std::size_t length)
		{
			int dd = this->getHandle();
			int result = ::hci_read_local_name(dd, length, text, 0);
			
			if (result != 0)
			{
				int error = errno;
				sMetaclass.error() << "Failed to obtain local device name: " << std::strerror(error);
			}

			return std::strlen(text);
		}
		
		DeviceAddress HCIController::readLocalAddress()
		{
			DeviceAddress address;
			
			return address;
		}

		State HCIController::startLowEnergyScan()
		{
#if 0		
			State status = State::Success;
			
			std::lock_guard<std::mutex> lock(mMutex);
			
			int hci = this->getHandle();
			
			if (mScanCount++ > 0)
			{
				// Disable scanning to change parameters
				::hci_le_set_scan_enable(hci, 0 /* disable*/, 0, 10000);

				sMetaclass.debug() << "Disabled prior BLE scanning";
			}	
			

			
			// Set the scan parameters for BLE
			std::uint8_t active = scanner.getActiveScan();
			
			std::chrono::milliseconds intervalMs = scanner.getScanInterval();
			std::chrono::milliseconds windowMs = scanner.getScanWindow();
			
			std::uint16_t interval = static_cast<std::uint16_t>(std::min<std::uint64_t>(65535, intervalMs.count() * 5 / 4));
			std::uint16_t window = static_cast<std::uint16_t>(std::min<std::uint64_t>(65535, windowMs.count() * 5 / 4));
			std::uint8_t ownType = static_cast<std::uint8_t>(this->getAddressType());
			std::uint8_t filter = 0x00;
			int bleTimeout = 0;
			
			sMetaclass.debug() << "BLE Scan parameters: " << (active ? "active" : "passive") << ", interval " << interval << ", window " << window;
			
			int bleScanParamState = ::hci_le_set_scan_parameters(hci, active, interval, window, ownType, filter, bleTimeout);
			if (bleScanParamState == 0)
			{
				sMetaclass.debug() << "Set BLE scan parameters";
			}
			else
			{
				int error = errno;
				sMetaclass.error() << "Failed to set BLE scan parameters: " << std::strerror(error);
				throw cio::Exception(cio::status(error), "Failed to set BLE scan parameters");
			}

			// Enable scanning with our current parameters
			std::uint8_t duplicates = scanner.getFilterDuplicates();
			
			int bleScanEnabled = ::hci_le_set_scan_enable(hci, 0x01 /*enable */, duplicates, bleTimeout);
			if (bleScanEnabled == 0)
			{
				sMetaclass.debug() << "Enabled BLE scanning";
			}
			else
			{
				int error = errno;
				sMetaclass.error() << "Failed to enable BLE scanning: " << std::strerror(error);
				throw cio::Exception(cio::status(error), "Failed to enable BLE scanning");
			}
			
			return status;
#endif
			return cio::fail(Action::Scan, Reason::Unsupported);
		}
				
		Progress<std::size_t> HCIController::listenForAdvertisements(AdvertisementListener &listener) noexcept
		{
			Progress<std::size_t> result;
			Progress<std::chrono::milliseconds> timeout = listener.poll();
			
			try
			{
				while (!cio::completed(timeout.status))
				{			
					std::lock_guard<std::mutex> lock(mMutex);
							
					cio::net::Socket &hciSocket = mChannel.get();
					int hci = hciSocket.get();
				
					// Install new HCI filter get the meta events		
					struct hci_filter newFilter;
					hci_filter_clear(&newFilter);
					hci_filter_set_ptype(HCI_EVENT_PKT, &newFilter);
					hci_filter_set_event(EVT_LE_META_EVENT, &newFilter);
					hciSocket.setGeneralOption(SOL_HCI, HCI_FILTER, &newFilter, sizeof(struct hci_filter));
							
					timeout = listener.poll();
					mChannel.setReadTimeout(timeout.count);
				
					Buffer &events = this->readEvent();
					
					if (events.available())
					{
						Packet packet = mCodec->decodePacketHeader(events);
						if (packet == Packet::Event)
						{
							Event evt = mCodec->decodeEventHeader(events);
							while (events.available())
							{
								if (evt == Event::LowEnergy)
								{
									sMetaclass.info() << "Found a BLE Meta event";
									
									LowEnergyEvent subevent = mCodec->decodeLowEnergyHeader(events);
									if (subevent == LowEnergyEvent::Advertisement)
									{
										sMetaclass.info() << "Found a BLE Adverting Report subevent";
										
										std::size_t reportCount = mCodec->decodeAdvertisementHeader(events);
										sMetaclass.info() << "State contains " << reportCount << " reports";
										
										for (std::size_t i = 0; i < reportCount; ++i)
										{
											Peripheral ad = mCodec->decodeNextAdvertisement(events);
							  				listener.post(ad);
							  				
							  				result.succeed();
							  				++result.count;
						  				}
									}
									else
									{
										sMetaclass.info() << "Found a different BLE meta event: " << static_cast<unsigned>(subevent);
										break;
									}
								}
								else
								{
									sMetaclass.info() << "Found a different non-BLE event: " << evt;
									break;
								}
							}
						}
					}				
				}
			}
			catch (Exception &e)
			{
				timeout = e.state();
				listener.fail(std::move(e));
			}
			catch (...)
			{
				timeout.fail();
				listener.fail();
			}
			
			// Timeout exceeded with no other failure, so this is actually a success
			if (timeout.reason == Reason::Timeout)
			{
				result.succeed();
				listener.complete();
			}
			else
			{
				result.status = timeout.status;
			}
			
			result.status = timeout.status;
			
			return result;
		}		
				
		State HCIController::stopLowEnergyScan()
		{
			State status;
			
			std::lock_guard<std::mutex> lock(mMutex);
			if (--mScanCount == 0)
			{
				int dd = this->getHandle();
				int disabledScan = ::hci_le_set_scan_enable(dd, 0x00, 0x00, 10000);
				if (disabledScan == 0)
				{
					sMetaclass.debug() << "Disabled BLE scanning";
				}
				else
				{
				    	int error = errno;
	    				sMetaclass.warning() << "Failed to disable BLE scanning: " << std::strerror(error);
				}
			}
			
			return status;
		}
		
		State HCIController::startConnection(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Connect, Reason::Unsupported);
		}
				
		State HCIController::startDisconnection(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Disconnect, Reason::Unsupported);
		}
				
		State HCIController::startPairing(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Connect, Reason::Unsupported);
		}
				
		State HCIController::startUnpairing(const Peripheral &peripheral, Task &exec)
		{
			return cio::fail(Action::Disconnect, Reason::Unsupported);
		}		
				
		std::unique_lock<std::mutex> HCIController::lock() const
		{
			return std::unique_lock<std::mutex>(mMutex);
		}
		
		std::chrono::milliseconds HCIController::getResponseTimeout() const noexcept
		{
			return mResponseTimeout;
		}

		void HCIController::setResponseTimeout(std::chrono::milliseconds timeout)
		{
			mResponseTimeout = timeout;
			if (mChannel)
			{
				mChannel.setReadTimeout(timeout);
			}
		}
				
		std::size_t HCIController::getRequestRetryCount() const noexcept
		{
			return mRequestRetryCount;
		}

		void HCIController::setRequestRetryCount(std::size_t count) noexcept
		{
			mRequestRetryCount = count;
		}
		
		Buffer &HCIController::requestWriteBuffer()
		{
			mWriteBuffer.reset();
			return mWriteBuffer;
		}
		
		Buffer &HCIController::requestResponse(Buffer &request)
		{
			return this->requestResponse(request, this->requestReadBuffer());
		}
		
		Buffer &HCIController::requestResponse(Buffer &request, Buffer &response)
		{	
			if (!request.available())
			{
				throw cio::Exception(Reason::Underflow, "Attempted to send empty HCI request packet");		
			}
			
			if (request.remaining () > this->getMaxPacketSize())
			{
				sMetaclass.info() << "Request packet size " << request.remaining() << " larger than HCI max packet size " << this->getMaxPacketSize();
				throw cio::Exception(Reason::Overflow, "Attempted to send HCI request larger than maximum packet size");
			}

			for (std::size_t i = 0; i <= mRequestRetryCount; ++i)
			{
				Progress<std::size_t> sent = mChannel.requestWrite(request.current(), request.remaining());
				if (sent.failed())
				{
					throw cio::Exception(sent, "Could not send HCI request");
				}

				
				std::size_t mark = response.position();
				std::size_t end = response.limit();
				
				this->readEvent(response);
				if (response.available())
				{
					request.skip(request.remaining());
					break;
				}
				else
				{
					response.limit(end);
					response.position(mark);
				}
			}
			
			// We can only get here if the retry count is exceeded
			if (request.available())
			{
				throw cio::Exception(Reason::Unanswered, "No HCI response received for request");
			}
			
			return response;
		}
				
		void HCIController::command(Buffer &packet)
		{
			if (!packet.available())
			{
				throw cio::Exception(Reason::Underflow, "Attempted to send empty packet");
			}
			
			if (packet.remaining () > this->getMaxPacketSize())
			{
				sMetaclass.info() << "Packet size " << packet.remaining() << " larger than HCI max packet size " << this->getMaxPacketSize();
				throw cio::Exception(Reason::Overflow, "Attempted to send HCI packet larger than max packet size");
			}
			
			// should throw on failure	
			mChannel.drain(packet);
		}
				
		Buffer &HCIController::requestReadBuffer()
		{
			mReadBuffer.reset();
			return mReadBuffer;
		}
				
		Buffer &HCIController::readEvent()
		{
			return this->readEvent(this->requestReadBuffer());
		}
				
		Buffer &HCIController::readEvent(Buffer &packet)
		{
			Progress<std::size_t> processed = mChannel.append(packet);

			if (processed.failed())
			{
				throw Exception(processed);
			}
			
			return packet;
		}
		
		int HCIController::getHandle() const noexcept
		{
			return mChannel.get().get();
		}
		
		HCICodec &HCIController::getCodec() noexcept
		{
			return *mCodec;
		}
				
		const HCICodec &HCIController::getCodec() const noexcept
		{
			return *mCodec;
		}
				
		void HCIController::setCodec(HCICodec &codec) noexcept
		{
			mCodec = &codec;	
		}
	}
}
