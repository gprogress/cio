/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "EncodingTest.h"

#include <cio/Encoding.h>
#include <cio/Type.h>
#include <cio/test/Assert.h>

#include <cfloat>
#include <iostream>
#include <sstream>

namespace cio
{
	EncodingTest::EncodingTest() :
		cio::test::Suite("cio/Encoding")
	{		
		this->addTestCase("Encoding", &EncodingTest::testConstruction);
		this->addTestCase("operator=", &EncodingTest::testAssignment);
		this->addTestCase("clear", &EncodingTest::testClear);
		this->addTestCase("create<T>",  &EncodingTest::testCreate);
		this->addTestCase("createNormalized",  &EncodingTest::testCreateNormalized);
		this->addTestCase("createSignedNormalized",  &EncodingTest::testCreateSignedNormalized);
		this->addTestCase("isSigned",  &EncodingTest::testIsSigned);
		this->addTestCase("setSigned",  &EncodingTest::testSetSigned);
		this->addTestCase("hasSignBit",  &EncodingTest::testHasSignBit);
		this->addTestCase("setSignBit",  &EncodingTest::testSetSignBit);
		this->addTestCase("setTwosComplement",  &EncodingTest::testSetTwosComplement);
		this->addTestCase("isUnsignedInteger",  &EncodingTest::testIsUnsignedInteger);
		this->addTestCase("isStandardInteger",  &EncodingTest::testIsStandardInteger);
		this->addTestCase("isStandardUnsigned",  &EncodingTest::testIsStandardUnsigned);
		this->addTestCase("isStandardSigned",  &EncodingTest::testIsStandardSigned);
		this->addTestCase("setNormalized",  &EncodingTest::testSetNormalized);
		this->addTestCase("setNoData",  &EncodingTest::testSetNoData);
		this->addTestCase("setInfinity",  &EncodingTest::testSetInfinity);
		this->addTestCase("setMantissaBits",  &EncodingTest::testSetMantissaBits);
		this->addTestCase("setExponentBits",  &EncodingTest::testSetExponentBits);
		this->addTestCase("setExponentBias",  &EncodingTest::testSetExponentBias);
		this->addTestCase("setFloatingPoint",  &EncodingTest::testSetFloatingPoint);
		this->addTestCase("isStandardFloat",  &EncodingTest::testIsStandardFloat);
		this->addTestCase("isBelowRepresentation",  &EncodingTest::testIsBelowRepresentation);
		this->addTestCase("isAboveRepresentation",  &EncodingTest::testIsAboveRepresentation);
		this->addTestCase("pack",  &EncodingTest::testPack);
		this->addTestCase("setEncoding",  &EncodingTest::testSetEncoding);
		this->addTestCase("computeRequiredBits",  &EncodingTest::testComputeRequiredBits);
		this->addTestCase("computeRequiredBytes",  &EncodingTest::testComputeRequiredBytes);
		this->addTestCase("is",  &EncodingTest::testIs);
		this->addTestCase("isCopyableTo",  &EncodingTest::testIsCopyableTo);
		this->addTestCase("isPrimitive",  &EncodingTest::testIsPrimitive);
		this->addTestCase("setReal",  &EncodingTest::testSetReal);
		this->addTestCase("setImaginary",  &EncodingTest::testSetImaginary);
		this->addTestCase("equalWithinULP", &EncodingTest::testEqualWithinULP);
	}

	EncodingTest::~EncodingTest() noexcept = default;

	void EncodingTest::testConstruction()
	{
		// Default construction
		{
			cio::Encoding e;
			// Validate that it packs to 64-bits as intended
			CIO_ASSERT_EQUAL(8u, sizeof(e));

			// Validate default values
			CIO_ASSERT_EQUAL(0u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// Simple mantissa construction
		{
			cio::Encoding e(20u);
			CIO_ASSERT_EQUAL(20u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// Mantissa + sign construction
		{
			cio::Encoding e(13u, Signedness::Negation);
			CIO_ASSERT_EQUAL(13u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// Floating point construction
		{
			cio::Encoding e(14u, 3u, -24);
			CIO_ASSERT_EQUAL(14u, e.mantissa);
			CIO_ASSERT_EQUAL(3u, e.exponent);
			CIO_ASSERT_EQUAL(-24, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// Floating point with normalization
		{
			cio::Encoding e(14u, 3u, -24, Signedness::Prefix, Normalized::YesWIthInfinity);
			CIO_ASSERT_EQUAL(14u, e.mantissa);
			CIO_ASSERT_EQUAL(3u, e.exponent);
			CIO_ASSERT_EQUAL(-24, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWIthInfinity, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// FLoating point with flags
		{
			cio::Encoding e(14u, 3u, -24, Signedness::Prefix, true, false, true);
			CIO_ASSERT_EQUAL(14u, e.mantissa);
			CIO_ASSERT_EQUAL(3u, e.exponent);
			CIO_ASSERT_EQUAL(-24, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWIthInfinity, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
	}

	void EncodingTest::testAssignment()
	{
		cio::Encoding e = cio::Encoding::create<float>();
		cio::Encoding e2;
		e2 = e;
		CIO_ASSERT_EQUAL(e, e2);
		CIO_ASSERT_EQUAL(true, e <= e2);
		CIO_ASSERT_EQUAL(true, e >= e2);
		CIO_ASSERT_EQUAL(false, e != e2);
		CIO_ASSERT_EQUAL(false, e < e2);
		CIO_ASSERT_EQUAL(false, e > e2);

		e2 = e2;
		CIO_ASSERT_EQUAL(e, e2);
		CIO_ASSERT_EQUAL(true, e <= e2);
		CIO_ASSERT_EQUAL(true, e >= e2);
		CIO_ASSERT_EQUAL(false, e != e2);
		CIO_ASSERT_EQUAL(false, e < e2);
		CIO_ASSERT_EQUAL(false, e > e2);

		e = cio::Encoding(1);
		e2 = cio::Encoding(2);
		CIO_ASSERT_EQUAL(false, e == e2);
		CIO_ASSERT_EQUAL(false, e >= e2);
		CIO_ASSERT_EQUAL(true, e <= e2);
		CIO_ASSERT_EQUAL(true, e2 >= e);
		CIO_ASSERT_EQUAL(false, e2 <= e);
		CIO_ASSERT_EQUAL(true, e != e2);
		CIO_ASSERT_EQUAL(true, e < e2);
		CIO_ASSERT_EQUAL(true, e2 > e);
		CIO_ASSERT_EQUAL(false, e > e2);
		CIO_ASSERT_EQUAL(false, e2 < e);
	}
	
	void EncodingTest::testClear()
	{
		cio::Encoding e = cio::Encoding::create<double>();
		e.domain = Domain::Imaginary;
		e.clear();

		// Validate default values
		CIO_ASSERT_EQUAL(0u, e.mantissa);
		CIO_ASSERT_EQUAL(0u, e.exponent);
		CIO_ASSERT_EQUAL(0, e.bias);
		CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
		CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
		CIO_ASSERT_EQUAL(Domain::Real, e.domain);
	}

	void EncodingTest::testCreate()
	{
		// Bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(sizeof(bool) * 8u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
	}

	void EncodingTest::testCreateNormalized()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(sizeof(bool) * 8, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-8, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// Char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(sizeof(char) * 8, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-8, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// wchar_t
		{
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			// wchar_t is unsigned on Windows but signed on Linux
			cio::Encoding e = cio::Encoding::createNormalized<wchar_t>();
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-int(sizeof(wchar_t) * 8), e.bias);
			CIO_ASSERT_EQUAL(cio::signedness<wchar_t>(), e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// unsigned char
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned char>();
			CIO_ASSERT_EQUAL(sizeof(unsigned char) * 8, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-8, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(23, e.mantissa);
			CIO_ASSERT_EQUAL(8, e.exponent);
			CIO_ASSERT_EQUAL(-32, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(52, e.mantissa);
			CIO_ASSERT_EQUAL(11, e.exponent);
			CIO_ASSERT_EQUAL(-64, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		// Bool
		{
			cio::Encoding e = cio::Encoding::createNormalized(58);
			CIO_ASSERT_EQUAL(58u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-58, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
	}

	void EncodingTest::testCreateSignedNormalized()
	{
		{
			cio::Encoding e = cio::Encoding::createSignedNormalized(16);
			CIO_ASSERT_EQUAL(16, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(-16, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		{
			cio::Encoding e = cio::Encoding::createSignedNormalized(8);
			CIO_ASSERT_EQUAL(8, e.mantissa);
			CIO_ASSERT_EQUAL(0, e.exponent);
			CIO_ASSERT_EQUAL(-8, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		{
			cio::Encoding e = cio::Encoding::createSignedNormalized(23);
			CIO_ASSERT_EQUAL(23, e.mantissa);
			CIO_ASSERT_EQUAL(0, e.exponent);
			CIO_ASSERT_EQUAL(-23, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
		
		{
			cio::Encoding e = cio::Encoding::createSignedNormalized(52);
			CIO_ASSERT_EQUAL(52, e.mantissa);
			CIO_ASSERT_EQUAL(0, e.exponent);
			CIO_ASSERT_EQUAL(-52, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
	}

	void EncodingTest::testIsSigned()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}
	}

	void EncodingTest::testSetSigned()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.isSigned());
			e.setSigned(true);
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
			e.setSigned(false);
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
			e.setSigned(false);
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isSigned());
			e.setSigned(true);
			CIO_ASSERT_EQUAL(true, e.isSigned());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
			e.setSigned(false);
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.isSigned());
			e.setSigned(false);
			CIO_ASSERT_EQUAL(false, e.isSigned());
		}
	}

	void EncodingTest::testHasSignBit()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<signed int>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}
	}

	void EncodingTest::testSetSignBit()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
			e.setSignBit(true);
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
			e.setSignBit(true);
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
			e.setSignBit(true);
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
			e.setSignBit(true);
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
			e.setSignBit(false);
			CIO_ASSERT_EQUAL(false, e.hasSignBit());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.hasSignBit());
			e.setSignBit(false);
			CIO_ASSERT_EQUAL(false , e.hasSignBit());
		}
	}

	void EncodingTest::testSetTwosComplement()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
			e.setTwosComplement(false);
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
			e.setTwosComplement(false);
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}
		
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
			e.setTwosComplement(false);
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
			e.setTwosComplement(false);
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(false, e.isTwosComplement());
			e.setTwosComplement(true);
			CIO_ASSERT_EQUAL(true, e.isTwosComplement());
		}
	}

	void EncodingTest::testIsUnsignedInteger()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(true, e.isUnsignedInteger());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.isUnsignedInteger());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(false, e.isUnsignedInteger());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(true, e.isUnsignedInteger());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, e.isUnsignedInteger());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, e.isUnsignedInteger());
		}
	}
	
	void EncodingTest::testIsStandardInteger()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(1, e.isStandardInteger());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(1, e.isStandardInteger());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(4, e.isStandardInteger());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(4, e.isStandardInteger());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(0, e.isStandardInteger());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(0, e.isStandardInteger());
		}
	}

	void EncodingTest::testIsStandardUnsigned()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(1, e.isStandardUnsigned());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(0, e.isStandardUnsigned());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(0, e.isStandardUnsigned());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(4, e.isStandardUnsigned());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(0, e.isStandardUnsigned());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(0, e.isStandardUnsigned());
		}
	}
	
	void EncodingTest::testIsStandardSigned()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(0, e.isStandardSigned());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(1, e.isStandardSigned());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(4, e.isStandardSigned());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(0, e.isStandardSigned());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(0, e.isStandardSigned());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(0, e.isStandardSigned());
		}
	}

	void EncodingTest::testSetNormalized()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.isNormalized());
			e.setNormalized(true);
			CIO_ASSERT_EQUAL(true, e.isNormalized());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.isNormalized());
			e.setNormalized(true);
			CIO_ASSERT_EQUAL(true, e.isNormalized());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(false, e.isNormalized());
			e.setNormalized(true);
			CIO_ASSERT_EQUAL(true, e.isNormalized());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isNormalized());
			e.setNormalized(true);
			CIO_ASSERT_EQUAL(true, e.isNormalized());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}
		
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.isNormalized());
			e.setNormalized(false);
			CIO_ASSERT_EQUAL(false, e.isNormalized());
		}
	}

	void EncodingTest::testSetNoData()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}
		
		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasNoData());
			e.setNoData(true);
			CIO_ASSERT_EQUAL(true, e.hasNoData());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.hasNoData());
			e.setNoData(false);
			CIO_ASSERT_EQUAL(false, e.hasNoData());
		}
	}

	void EncodingTest::testSetInfinity()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
			e.setInfinity(true);
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.hasInfinity());
			e.setInfinity(false);
			CIO_ASSERT_EQUAL(false, e.hasInfinity());
		}
	}
	
	void EncodingTest::testSetMantissaBits()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(8, e.getMantissaBits());
			e.setMantissaBits(4);
			CIO_ASSERT_EQUAL(4, e.getMantissaBits());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(8, e.getMantissaBits());
			e.setMantissaBits(4);
			CIO_ASSERT_EQUAL(4, e.getMantissaBits());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(32, e.getMantissaBits());
			e.setMantissaBits(64);
			CIO_ASSERT_EQUAL(64, e.getMantissaBits());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(32, e.getMantissaBits());
			e.setMantissaBits(64);
			CIO_ASSERT_EQUAL(64, e.getMantissaBits());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(23, e.getMantissaBits());
			e.setMantissaBits(20);
			CIO_ASSERT_EQUAL(20, e.getMantissaBits());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(52, e.getMantissaBits());
			e.setMantissaBits(32);
			CIO_ASSERT_EQUAL(32, e.getMantissaBits());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(8, e.getMantissaBits());
			e.setMantissaBits(16);
			CIO_ASSERT_EQUAL(16, e.getMantissaBits());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(8, e.getMantissaBits());
			e.setMantissaBits(4);
			CIO_ASSERT_EQUAL(4, e.getMantissaBits());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(32, e.getMantissaBits());
			e.setMantissaBits(16);
			CIO_ASSERT_EQUAL(16, e.getMantissaBits());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(32, e.getMantissaBits());
			e.setMantissaBits(64);
			CIO_ASSERT_EQUAL(64, e.getMantissaBits());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(23, e.getMantissaBits());
			e.setMantissaBits(20);
			CIO_ASSERT_EQUAL(20, e.getMantissaBits());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(52, e.getMantissaBits());
			e.setMantissaBits(21);
			CIO_ASSERT_EQUAL(21, e.getMantissaBits());
		}
	}
	
	void EncodingTest::testSetExponentBits()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBits());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBits());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBits());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBits());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(8, e.getExponentBits());
			e.setExponentBits(20);
			CIO_ASSERT_EQUAL(20, e.getExponentBits());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(11, e.getExponentBits());
			e.setExponentBits(32);
			CIO_ASSERT_EQUAL(32, e.getExponentBits());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(16);
			CIO_ASSERT_EQUAL(16, e.getExponentBits());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBits());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(16);
			CIO_ASSERT_EQUAL(16, e.getExponentBits());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBits());
			e.setExponentBits(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBits());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(8, e.getExponentBits());
			e.setExponentBits(20);
			CIO_ASSERT_EQUAL(20, e.getExponentBits());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(11, e.getExponentBits());
			e.setExponentBits(21);
			CIO_ASSERT_EQUAL(21, e.getExponentBits());
		}
	}
	
	void EncodingTest::testSetExponentBias()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(0, e.getExponentBias());
			e.setExponentBias(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBias());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(0, e.getExponentBias());
			e.setExponentBias(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBias());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBias());
			e.setExponentBias(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBias());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(0, e.getExponentBias());
			e.setExponentBias(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBias());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(-127, e.getExponentBias());
			e.setExponentBias(20);
			CIO_ASSERT_EQUAL(20, e.getExponentBias());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(-1023, e.getExponentBias());
			e.setExponentBias(32);
			CIO_ASSERT_EQUAL(32, e.getExponentBias());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(-8, e.getExponentBias());
			e.setExponentBias(16);
			CIO_ASSERT_EQUAL(16, e.getExponentBias());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(-8, e.getExponentBias());
			e.setExponentBias(4);
			CIO_ASSERT_EQUAL(4, e.getExponentBias());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(-32, e.getExponentBias());
			e.setExponentBias(16);
			CIO_ASSERT_EQUAL(16, e.getExponentBias());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(-32, e.getExponentBias());
			e.setExponentBias(64);
			CIO_ASSERT_EQUAL(64, e.getExponentBias());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(-32, e.getExponentBias());
			e.setExponentBias(20);
			CIO_ASSERT_EQUAL(20, e.getExponentBias());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(-64, e.getExponentBias());
			e.setExponentBias(21);
			CIO_ASSERT_EQUAL(21, e.getExponentBias());
		}
	}
	
	void EncodingTest::testSetFloatingPoint()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(4,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(4,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(64,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(64,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
			e.setFloatingPoint(0,0,0);
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
			e.setFloatingPoint(0,0,0);
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(16,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(4,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(4,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isFloatingPoint());
			e.setFloatingPoint(64,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
			e.setFloatingPoint(0,0,0);
			CIO_ASSERT_EQUAL(false , e.isFloatingPoint());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
			e.setFloatingPoint(4,4,4);
			CIO_ASSERT_EQUAL(true, e.isFloatingPoint());
		}
	}
	
	void EncodingTest::testIsStandardFloat()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(0, e.isStandardFloat());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(0, e.isStandardFloat());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(0, e.isStandardFloat());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(0, e.isStandardFloat());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(4, e.isStandardFloat());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(8, e.isStandardFloat());
		}
	}

	void EncodingTest::testIsBelowRepresentation()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(7));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(9));
			CIO_ASSERT_EQUAL(false, e.isBelowRepresentation(19));
		}
	}

	void EncodingTest::testIsAboveRepresentation()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(256));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(257));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(256));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(257));
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<signed int>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(std::numeric_limits<int32_t>::max()));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(std::numeric_limits<int64_t>::max()));
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(std::numeric_limits<int32_t>::max()));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(std::numeric_limits<int64_t>::max()));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(std::numeric_limits<float>::max()));
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(std::numeric_limits<double>::max()));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(0));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(8));
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(std::numeric_limits<double>::max()));
#if defined _MSC_VER // MS long double is same layout as double
			CIO_ASSERT_EQUAL(false, e.isAboveRepresentation(std::numeric_limits<long double>::max()));
#else // all other compilers have a wider long double
			CIO_ASSERT_EQUAL(true, e.isAboveRepresentation(std::numeric_limits<long double>::max()));
#endif
		}
	}

	void EncodingTest::testPack()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(bool) * 8u, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(0, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::None, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(char) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(0, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::create<wchar_t>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(0, unpacked.bias);
			// wchar_t is unsigned on Windows but signed on Linux
			CIO_ASSERT_EQUAL(cio::signedness<wchar_t>(), unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::create<unsigned char>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(unsigned char) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(0, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::None, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(23, unpacked.mantissa);
			CIO_ASSERT_EQUAL(8, unpacked.exponent);
			CIO_ASSERT_EQUAL(-127, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(52, unpacked.mantissa);
			CIO_ASSERT_EQUAL(11, unpacked.exponent);
			CIO_ASSERT_EQUAL(-1023, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(bool) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(-8, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::None, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// Char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(char) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(-8, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::createNormalized<wchar_t>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(-int(sizeof(wchar_t) * 8), unpacked.bias);
			// wchar_t is unsigned on Windows but signed on Linux
			CIO_ASSERT_EQUAL(cio::signedness<wchar_t>(), unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned char>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(sizeof(unsigned char) * 8, unpacked.mantissa);
			CIO_ASSERT_EQUAL(0u, unpacked.exponent);
			CIO_ASSERT_EQUAL(-8, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::None, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::Yes, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(23, unpacked.mantissa);
			CIO_ASSERT_EQUAL(8, unpacked.exponent);
			CIO_ASSERT_EQUAL(-32, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			std::uint64_t packed = e.pack();
			cio::Encoding unpacked;
			unpacked.unpack(packed);
			CIO_ASSERT_EQUAL(52, unpacked.mantissa);
			CIO_ASSERT_EQUAL(11, unpacked.exponent);
			CIO_ASSERT_EQUAL(-64, unpacked.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, unpacked.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, unpacked.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, unpacked.domain);
		}
	}
	
	void EncodingTest::testSetEncoding()
	{
		// bool
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Boolean);
			CIO_ASSERT_EQUAL(1u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0u, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// complex
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Real, cio::Domain::Complex);
			CIO_ASSERT_EQUAL(23u, e.mantissa);
			CIO_ASSERT_EQUAL(8u, e.exponent);
			CIO_ASSERT_EQUAL(-127, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Complex, e.domain);
		}

		// integer
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Integer);
			CIO_ASSERT_EQUAL(32u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Negation, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// real
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Real);
			CIO_ASSERT_EQUAL(23u, e.mantissa);
			CIO_ASSERT_EQUAL(8u, e.exponent);
			CIO_ASSERT_EQUAL(-127, e.bias);
			CIO_ASSERT_EQUAL(Signedness::Prefix, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::YesWithInfinityNaN, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// none
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::None);
			CIO_ASSERT_EQUAL(0u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0u, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// text
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Text);
			CIO_ASSERT_EQUAL(8u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0u, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}

		// blob
		{
			cio::Encoding e;
			e.setEncoding(cio::Type::Blob);
			CIO_ASSERT_EQUAL(8u, e.mantissa);
			CIO_ASSERT_EQUAL(0u, e.exponent);
			CIO_ASSERT_EQUAL(0u, e.bias);
			CIO_ASSERT_EQUAL(Signedness::None, e.signedness);
			CIO_ASSERT_EQUAL(Normalized::No, e.normalized);
			CIO_ASSERT_EQUAL(Domain::Real, e.domain);
		}
	}
	
	void EncodingTest::testComputeRequiredBits()
	{
		// empty
		{
			cio::Encoding e;
			CIO_ASSERT_EQUAL(0u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(0u, e.computeRequiredBits(8));
		}
		
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(64u, e.computeRequiredBits(8));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(96u, e.computeRequiredBits(12));
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::create<wchar_t>();
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 64, e.computeRequiredBits(8));
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(176u, e.computeRequiredBits(22));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(32u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(256u, e.computeRequiredBits(8));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(64u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(512u, e.computeRequiredBits(8));
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(64u, e.computeRequiredBits(8));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(24u, e.computeRequiredBits(3));
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::createNormalized<wchar_t>();
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8u * 18u, e.computeRequiredBits(18));
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned char>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(88u, e.computeRequiredBits(11));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(32u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(160u, e.computeRequiredBits(5));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(64u, e.computeRequiredBits());
			CIO_ASSERT_EQUAL(2432u, e.computeRequiredBits(38));
		}
	}
	
	void EncodingTest::testComputeRequiredBytes()
	{
		// empty
		{
			cio::Encoding e;
			CIO_ASSERT_EQUAL(0u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(0u, e.computeRequiredBytes(8));
		}
		
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBytes(8));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(12u, e.computeRequiredBytes(12));
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::create<wchar_t>();
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t), e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 8, e.computeRequiredBytes(8));
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(22u, e.computeRequiredBytes(22));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(4u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(32u, e.computeRequiredBytes(8));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(64u, e.computeRequiredBytes(8));
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBytes(8));
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(3u, e.computeRequiredBytes(3));
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::createNormalized<wchar_t>();
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			CIO_ASSERT_EQUAL(sizeof(wchar_t), e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(sizeof(wchar_t) * 18, e.computeRequiredBytes(18));
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned char>();
			CIO_ASSERT_EQUAL(1u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(11u, e.computeRequiredBytes(11));
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(4u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(20u, e.computeRequiredBytes(5));
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(8u, e.computeRequiredBytes());
			CIO_ASSERT_EQUAL(304u, e.computeRequiredBytes(38));
		}
	}

	void EncodingTest::testIs()
	{
		// bool
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<bool>());
		}

		// char
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::Negation;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<char>());
		}

		// wchar_t
		{
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			// wchar_t signedness values by platform, unsigned on Windows and signed otherwise
			cio::Encoding actual;
			actual.mantissa = sizeof(wchar_t) * 8;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = cio::signedness<wchar_t>();
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<wchar_t>());
		}

		// unsigned char
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<unsigned char>());
		}

		// float
		{
			cio::Encoding actual;
			actual.mantissa = 23u;
			actual.exponent = 8u;
			actual.bias = -127;
			actual.signedness = Signedness::Prefix;
			actual.normalized = Normalized::YesWithInfinityNaN;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<float>());
		}

		// double
		{
			cio::Encoding actual;
			actual.mantissa = 52u;
			actual.exponent = 11u;
			actual.bias = -1023;
			actual.signedness = Signedness::Prefix;
			actual.normalized = Normalized::YesWithInfinityNaN;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(true, actual.is<double>());
		}

		// bool
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = -8;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::Yes;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(false, actual.is<bool>());
		}
	}

	void EncodingTest::testIsCopyableTo()
	{
		// bool
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));
		}

		// char
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::Negation;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;
			
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));
		}

		// wchar_t
		{
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			// wchar_t signedness values by platform, unsigned on Windows and signed otherwise
			cio::Encoding actual;
			actual.mantissa = sizeof(wchar_t) * 8;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = cio::signedness<wchar_t>();
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));
		}

		// unsigned char
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = 0u;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::No;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));
		}

		// float
		{
			cio::Encoding actual;
			actual.mantissa = 23u;
			actual.exponent = 8u;
			actual.bias = -127;
			actual.signedness = Signedness::Prefix;
			actual.normalized = Normalized::YesWithInfinityNaN;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));
		}

		// double
		{
			cio::Encoding actual;
			actual.mantissa = 52u;
			actual.exponent = 11u;
			actual.bias = -1023;
			actual.signedness = Signedness::Prefix;
			actual.normalized = Normalized::YesWithInfinityNaN;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<wchar_t>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<unsigned char>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, actual.isCopyableTo(e));

			e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, actual.isCopyableTo(e));
		}

		// bool
		{
			cio::Encoding actual;
			actual.mantissa = 8u;
			actual.exponent = 0u;
			actual.bias = -8;
			actual.signedness = Signedness::None;
			actual.normalized = Normalized::Yes;
			actual.domain = Domain::Real;

			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(e.mantissa, actual.mantissa);
			CIO_ASSERT_EQUAL(e.exponent, actual.exponent);
			CIO_ASSERT_EQUAL(e.bias, actual.bias);
			CIO_ASSERT_EQUAL(e.signedness, actual.signedness);
			CIO_ASSERT_EQUAL(e.normalized, actual.normalized);
			CIO_ASSERT_EQUAL(e.domain, actual.domain);
			CIO_ASSERT_EQUAL(false, actual.is<bool>());
		}
	}

	void EncodingTest::testIsPrimitive()
	{
		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(1, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(1, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::Negation, isPrim.second);
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::create<wchar_t>();
			
			// wchar_t size varies by platform, 16-bit on Windows and 32-bit otherwise
			// wchar_t signedness values by platform, unsigned on Windows and signed otherwise
			bool wcharSigned = std::is_signed<wchar_t>::value;
			
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(sizeof(wchar_t), isPrim.first);
			CIO_ASSERT_EQUAL(cio::signedness<wchar_t>(), isPrim.second);
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::create<unsigned char>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(1, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(4, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::Prefix, isPrim.second);
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(8, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::Prefix, isPrim.second);
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// Char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// wchar_t
		{
			cio::Encoding e = cio::Encoding::createNormalized<wchar_t>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// unsigned char
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned char>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}
		
		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			std::pair<std::size_t, cio::Signedness> isPrim = e.isPrimitive();
			CIO_ASSERT_EQUAL(0, isPrim.first);
			CIO_ASSERT_EQUAL(cio::Signedness::None, isPrim.second);
		}
	}

	void EncodingTest::testSetReal()
	{
		// Domain::None
		{
			cio::Encoding e;
			e.domain = Domain::None;
			CIO_ASSERT_EQUAL(false, e.hasReal());
			e.setReal(true);
			CIO_ASSERT_EQUAL(true, e.hasReal());
		}

		// Domain::Imaginary
		{
			cio::Encoding e;
			e.domain = Domain::Imaginary;
			CIO_ASSERT_EQUAL(false, e.hasReal());
			e.setReal(true);
			CIO_ASSERT_EQUAL(true, e.hasReal());
		}

		// Domain::Complex
		{
			cio::Encoding e;
			e.domain = Domain::Complex;
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(true);
			CIO_ASSERT_EQUAL(true, e.hasReal());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(true, e.hasReal());
			e.setReal(false);
			CIO_ASSERT_EQUAL(false, e.hasReal());
		}
	}
	
	void EncodingTest::testSetImaginary()
	{
		// Domain::None
		{
			cio::Encoding e;
			e.domain = Domain::None;
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// Domain::Real
		{
			cio::Encoding e;
			e.domain = Domain::Real;
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// Domain::Complex
		{
			cio::Encoding e;
			e.domain = Domain::Complex;
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::create<bool>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::create<char>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::create<int>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::create<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::create<float>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::create<double>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// bool
		{
			cio::Encoding e = cio::Encoding::createNormalized<bool>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// char
		{
			cio::Encoding e = cio::Encoding::createNormalized<char>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// int
		{
			cio::Encoding e = cio::Encoding::createNormalized<int>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// unsigned int
		{
			cio::Encoding e = cio::Encoding::createNormalized<unsigned int>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// float
		{
			cio::Encoding e = cio::Encoding::createNormalized<float>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}

		// double
		{
			cio::Encoding e = cio::Encoding::createNormalized<double>();
			CIO_ASSERT_EQUAL(false, e.hasImaginary());
			e.setImaginary(true);
			CIO_ASSERT_EQUAL(true, e.hasImaginary());
		}
	}

	void EncodingTest::testEqualWithinULP()
	{
		// Test for float values

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(INFINITY, FLT_MAX, 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(FLT_MAX, INFINITY, 1));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-INFINITY, -FLT_MAX, 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-FLT_MAX, -INFINITY, 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(INFINITY, INFINITY, 0));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-INFINITY, -INFINITY, 0));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(INFINITY, -INFINITY, 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-INFINITY, INFINITY, 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0f, -0.0f, 0));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0f, 0.0f, 0));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min(), 0.0f, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0f, std::numeric_limits<float>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min(), -0.0f, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0f, std::numeric_limits<float>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), 0.0f, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0f, -std::numeric_limits<float>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), -0.0f, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0f, -std::numeric_limits<float>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min(), -std::numeric_limits<float>::denorm_min(), 2));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), std::numeric_limits<float>::denorm_min(), 2));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min(), -std::numeric_limits<float>::denorm_min(), 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), std::numeric_limits<float>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 5, std::numeric_limits<float>::denorm_min() * 2, 3));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 2, std::numeric_limits<float>::denorm_min() * 5, 3));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 5, std::numeric_limits<float>::denorm_min() * 2, 2));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 2, std::numeric_limits<float>::denorm_min() * 5, 2));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), std::numeric_limits<float>::denorm_min() * 2, 3));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 2, -std::numeric_limits<float>::denorm_min(), 3));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<float>::denorm_min(), std::numeric_limits<float>::denorm_min() * 2, 2));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<float>::denorm_min() * 2, -std::numeric_limits<float>::denorm_min(), 2));
	
		// Test for double values

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::infinity(), DBL_MAX, 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(DBL_MAX, std::numeric_limits<double>::infinity(), 1));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::infinity(), -DBL_MAX, 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-DBL_MAX, -std::numeric_limits<double>::infinity(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), 0));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), 0));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0, -0.0, 0));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0, 0.0, 0));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min(), 0.0, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0, std::numeric_limits<double>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min(), -0.0, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0, std::numeric_limits<double>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), 0.0, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(0.0, -std::numeric_limits<double>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), -0.0, 1));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-0.0, -std::numeric_limits<double>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min(), -std::numeric_limits<double>::denorm_min(), 2));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), std::numeric_limits<double>::denorm_min(), 2));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min(), -std::numeric_limits<double>::denorm_min(), 1));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), std::numeric_limits<double>::denorm_min(), 1));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 5, std::numeric_limits<double>::denorm_min() * 2, 3));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 2, std::numeric_limits<double>::denorm_min() * 5, 3));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 5, std::numeric_limits<double>::denorm_min() * 2, 2));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 2, std::numeric_limits<double>::denorm_min() * 5, 2));

		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), std::numeric_limits<double>::denorm_min() * 2, 3));
		CIO_ASSERT_EQUAL(true, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 2, -std::numeric_limits<double>::denorm_min(), 3));

		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(-std::numeric_limits<double>::denorm_min(), std::numeric_limits<double>::denorm_min() * 2, 2));
		CIO_ASSERT_EQUAL(false, cio::Encoding::equalWithinULP(std::numeric_limits<double>::denorm_min() * 2, -std::numeric_limits<double>::denorm_min(), 2));
	}
}
