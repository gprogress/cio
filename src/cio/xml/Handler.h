/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_HANDLER_H
#define CIO_XML_HANDLER_H

#include "Types.h"


#include <cio/Text.h>

namespace cio
{
	namespace xml
	{
		/**
		 * Interface for an XML streaming handler.
		 * This base handler is stateless and suitable for optimized parsing.
		 */
		class CIO_XML_API Handler
		{
			public:

				/**
				 * Default constructor.
				 */
				Handler();

				/**
				 * Destructor.
				 */
				virtual ~Handler();

				/**
				 * Called when a new document is parsed.
				 *
				 * @throw std::exception To end parsing
				 */
				virtual void startDocument();

				/**
				 * Called after a document has been fully parsed.
				 *
				 * @throw std::exception To notify the user of a delayed parse error
				 */
				virtual void endDocument();

				/**
				 * Called when a new element is started.
				 *
				 * @param element The element that just started
				 * @throw std::exception To end parsing
				 */
				virtual void startElement(Text element);

				/**
				 * Called when an element is ended.
				 *
				 * @param element The element that just ended
				 * @throw std::exception To end parsing
				 */
				virtual void endElement(Text element);

				/**
				 * Processes a single attribute for the current element.
				 *
				 * @param key The attribute key
				 * @param value The attribute value
				 * @throw std::exception To end parsing
				 */
				virtual void processAttribute(Text key, Text value);

				/**
				 * Processes a comment.
				 *
				 * @param comment The comment text
				 * @throw std::exception To end parsing
				 */
				virtual void processComment(Text comment);

				/**
				 * Process a chunk of text.  The text might not be null-terminated.
				 * It might not be the entire text under the current element.
				 *
				 * @param text The text to process
				 * @throw std::exception To end parsing
				 */
				virtual void processTextChunk(Text text);

				/**
				 * Handle a parser error.
				 *
				 * @param message The error message
				 * @throw std::exception To end parsing
				 */
				virtual void handleError(Text message);
		};
	}
}

#endif
