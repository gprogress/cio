/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ProtocolFamily.h"

#include <cio/Case.h>

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2
#if defined CIO_HAVE_WS2TCPIP_H
#include <Ws2tcpip.h>
#endif
#endif

#include <ostream>

// Don't warn for use of std::strncpy
#if defined _MSC_VER
#pragma warning(disable: 4996)
#endif

namespace cio
{
	namespace net
	{
		ProtocolFamily ProtocolFamily::tcp()
		{
			return ProtocolFamily(IPPROTO_TCP);
		}

		ProtocolFamily ProtocolFamily::udp()
		{
			return ProtocolFamily(IPPROTO_UDP);
		}

		Validation ProtocolFamily::parse(const char *text) noexcept
		{
			Validation valid = Validation::Missing;

			if (text)
			{
				std::size_t len = std::strlen(text);
				valid = this->parse(text, len);
			}

			return valid;
		}

		Validation ProtocolFamily::parse(const char *text, std::size_t len) noexcept
		{
			Validation valid = Validation::Missing;

			if (text && len > 0)
			{
				// Find first colon and only consider up to that
				std::size_t mark = 0;
				while (mark < len && text[mark] != ':')
				{
					++mark;
				}

				if (CIO_STRNCASECMP(text, "tcp", mark) == 0)
				{
					mCode = IPPROTO_TCP;
					valid = Validation::Exact;
				}
				else if (CIO_STRNCASECMP(text, "udp", mark) == 0)
				{
					mCode = IPPROTO_UDP;
					valid = Validation::Exact;
				}
				else
				{
					// TODO add platform-specific protocols and/or look up services?
					mCode = 0;
					valid = Validation::InvalidValue;
				}
			}

			return valid;
		}

		Validation ProtocolFamily::parse(const std::string &text) noexcept
		{
			return this->parse(text.c_str(), text.size());
		}

		std::size_t ProtocolFamily::print(char *text, std::size_t len) const noexcept
		{
			std::size_t actual = 0;

			switch (mCode)
			{
				case IPPROTO_TCP:
					actual = 3;

					if (text && len > 0)
					{
						std::strncpy(text, "tcp", len - 1);
					}

					break;

				case IPPROTO_UDP:
					actual = 3;

					if (text && len > 0)
					{
						std::strncpy(text, "udp", len - 1);
					}

					break;

				default:
					// TODO add platform specific ones or lookup somehow?
					// nothing to do
					break;
			}

			if (actual < len)
			{
				std::memset(text + actual, 0, len - actual);
			}

			return actual;
		}

		std::string ProtocolFamily::print() const
		{
			char buffer[16];
			std::size_t actual = this->print(buffer, 16);
			return std::string(buffer, buffer + 16);
		}

		ChannelType ProtocolFamily::getChannelType() const noexcept
		{
			ChannelType type;

			switch (mCode)
			{
				case 0:
					type = ChannelType::None;
					break;

				case IPPROTO_TCP:
					type = ChannelType::Stream;
					break;

				case IPPROTO_UDP:
					type = ChannelType::Datagram;
					break;

				default:
					type = ChannelType::Raw;
					break;
			}

			return type;
		}

		bool ProtocolFamily::isTcp() const noexcept
		{
			return mCode == IPPROTO_TCP;
		}

		bool ProtocolFamily::isUdp() const noexcept
		{
			return mCode == IPPROTO_UDP;
		}
		
		bool operator==(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			return left.get() == right.get();
		}

		bool operator!=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			return left.get() != right.get();
		}

		bool operator<(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			char leftLabel[64];
			char rightLabel[64];

			left.print(leftLabel, 64);
			right.print(rightLabel, 64);
			return cio::compareTextCaseInsensitive(leftLabel, rightLabel) < 0;
		}

		bool operator<=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			return !(left > right);
		}

		bool operator>(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			char leftLabel[64];
			char rightLabel[64];

			left.print(leftLabel, 64);
			right.print(rightLabel, 64);
			return cio::compareTextCaseInsensitive(leftLabel, rightLabel) > 0;
		}

		bool operator>=(const ProtocolFamily &left, const ProtocolFamily &right) noexcept
		{
			return !(left < right);
		}

		std::ostream &operator<<(std::ostream &stream, const ProtocolFamily &type)
		{
			char buffer[16];
			type.print(buffer, 16);
			return stream << buffer;
		}
	}
}
