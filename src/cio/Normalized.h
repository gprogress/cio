/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NORMALIZED_H
#define CIO_NORMALIZED_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Normalized enumeration describes the different encoding methodologies that can be used
	 * to represent normalized floating point numbers, possibly with special values like infinity or NaN.
	 * 
	 * Normalized values have an implicit leading bit set when the exponent is non-zero, allowing for
	 * efficient floating point representation.
	 * 
	 * If infinity is a valid special value, it is represented by all exponent bits set and all mantissa
	 * bits being zero.
	 * 
	 * If NaN is a valid special value, it is represented by all exponent bits set for any value other than infinity if also present.
	 * 
	 * The enumeration is structured as an overlay on a bitset where bit 0 indicates normalization,
	 * bit 1 indicates NaN, and bit 3 indicates infinity. The Encoding class provides methods
	 * to more easily work with the individual flags.
	 */
	enum class Normalized : std::uint8_t
	{
		/** The value is not normalized and does not have any special values. This is integer mode. */
		No = 0,

		/** The value is normalized but does not have any special values. */
		Yes = 1,

		/** The value is not normalized but does have a no-data NaN value. */
		NoWithNaN = 2,

		/** The value is  normalized and does have a no-data NaN value. */
		YesWithNaN = 3,

		/** The value is not normalized but does have infinity. */
		NoWithInfinity = 4,

		/** The value is normalized and does have infinity. */
		YesWIthInfinity = 5,

		/** The value is not normalized but does have both infinity and NaN values. */
		NoWithInfinityNaN = 6,

		/** The value is normalized dand has both infinity and NaN values. This is IEEE754 floating point mode. */
		YesWithInfinityNaN = 7
	};
	
	/**
	 * Gets the label representing a signedness type.
	 *
	 * @param type The type
	 * @return the label
	 */
	CIO_API const char *print(Normalized type) noexcept;
	
	/**
	 * Prints the label representing a signedness type to a C++ stream.
	 *
	 * @param s The C++ stream
	 * @param type The signedness type
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Normalized type);
}

#endif

