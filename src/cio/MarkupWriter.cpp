/*==============================================================================
 * Copyright 2023-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "MarkupWriter.h"

#include "Print.h"
#include "Newline.h"

namespace cio
{
	Class<MarkupWriter> MarkupWriter::sMetaclass("cio::MarkupWriter");

	const Metaclass &MarkupWriter::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	MarkupWriter::MarkupWriter(Pointer<Output> stream) :
		mOutput(std::move(stream)),
		mDepth(0),
		mIndent(1),
		mIndentChar('\t'),
		mNewline(Newline::None),
		mFilterWrites(false)
	{
		// do nothing
	}

	MarkupWriter::MarkupWriter(Buffer buffer, Pointer<Output> stream) :
		mOutput(std::move(stream)),
		mWriteBuffer(std::move(buffer)),
		mDepth(0),
		mIndent(1),
		mIndentChar('\t'),
		mNewline(Newline::None),
		mFilterWrites(false)
	{
		// do nothing
	}

	MarkupWriter::~MarkupWriter() noexcept
	{
		if (mOutput)
		{
			if (mWriteBuffer.position() > 0)
			{
				mWriteBuffer.flip();
				mOutput->requestDrain(mWriteBuffer);
			}
		}
	}

	void MarkupWriter::clear() noexcept
	{
		try
		{
			this->endDocument();
			this->flush();
		}
		catch (...)
		{
			// oh well
		}

		mOutput = nullptr;
		mWriteBuffer.clear();
		mDepth = 0;
		mIndent = 1;
		mIndentChar = '\t';
		mNewline = Newline::None;

		mLevels.clear();
		mRootLevel.clear();
	}

	void MarkupWriter::flush()
	{
		if (mOutput)
		{
			mWriteBuffer.flip();
			mOutput->drain(mWriteBuffer);
			mWriteBuffer.reset();
		}
	}

	void MarkupWriter::setOutput(Pointer<Output> stream)
	{
		mOutput = std::move(stream);
	}

	Pointer<Output> MarkupWriter::getOutput() noexcept
	{
		return mOutput.view();
	}

	Pointer<Output> MarkupWriter::takeOutput() noexcept
	{
		return std::move(mOutput);
	}

	void MarkupWriter::separate()
	{
		if (mNewline == Newline::None)
		{
			this->splat(' ', 1);
		}
		else
		{
			this->write(mNewline);
		}
	}

	State MarkupWriter::startDocument()
	{
		State state;

		if (mLevels.empty())
		{
			mLevels.emplace_back(Markup::Document, MarkupAction::Start);
			mDepth = 0;
			state.succeed();
		}
		else
		{
			state.fail(Reason::Exists);
		}

		return state;
	}

	State MarkupWriter::endDocument()
	{
		State state;

		if (mLevels.empty() || mLevels.front().markup != Markup::Document)
		{
			state.fail(Reason::Missing);
		}
		else
		{
			this->endCurrentLevels(mLevels.size() - 1);
			mLevels.clear();
			state.succeed();
			mDepth = 0;
		}

		return state;
	}

	State MarkupWriter::startElement(Text element)
	{
		State state;
		
		// Pop off irrelevant levels
		// We can always start a new element, but it may require going up to Document or root level
		while (!mLevels.empty() && !state.completed())
		{
			MarkupEvent &level = mLevels.back();

			switch (level.markup)
			{
				case Markup::Document:
				{
					level.count += 1;
					level.action = MarkupAction::Continue;
					state.succeed();
					break;
				}

				// If it is to become a child element of the current element
				// Make sure the current element isn't restricted to primitive data
				case Markup::Array:
				case Markup::Element:
				{
					if (level.type == Type::None)
					{
						level.type = Type::Object;
					}

					if (level.type == Type::Object || level.type == Type::Any)
					{
						level.count += 1;
						level.action = MarkupAction::Continue;
						state.succeed();
					}
					else
					{
						state.fail(Reason::Unexpected);
					}
					break;
				}

				default:
					this->endCurrentLevel();
					break;
			}
		}

		// If we pop off all the way to the root, create a new element there
		if (mLevels.empty())
		{
			mRootLevel.type = Type::Object;
			mRootLevel.count += 1;
			mRootLevel.action = MarkupAction::Continue;
			state.succeed();
		}

		if (!state.failed())
		{
			mLevels.emplace_back(std::move(element), Markup::Element, MarkupAction::Start);
			state.succeed();
			++mDepth;
		}

		return state;
	}

	State MarkupWriter::endElement()
	{
		State state = this->endCurrentChildLevels(Markup::Element);
		if (state.succeeded())
		{
			if (mLevels.back().count == 0)
			{
				this->writeEmptyValue();
			}

			mLevels.pop_back();
			--mDepth;
		}

		return state;
	}

	State MarkupWriter::writeEmptyElement(Text name)
	{
		State state = this->startElement(name);
		if (state.succeeded())
		{
			state = this->endElement();
		}
		return state;
	}

	State MarkupWriter::startAttribute(Text key)
	{
		State state;
		if (mLevels.empty())
		{
			state.fail(Reason::Unexpected);
		}
		else
		{
			MarkupEvent &level = mLevels.back();

			if (level.markup == Markup::Attribute)
			{
				this->endAttribute();
			}

			MarkupEvent &parent = mLevels.back();

			switch (level.markup)
			{
				case Markup::Element:
				case Markup::Instruction:
					if (level.type == Type::None)
					{
						level.type = Type::Object;
					}
					level.count += 1;
					level.action = MarkupAction::Continue;
					
					// these are fine
					break;

				default:
					// remaining types cannot have attributes
					state.fail(Reason::Unexpected);
					break;
			}
		}

		if (!state.failed())
		{
			mLevels.emplace_back(std::move(key), Markup::Attribute, MarkupAction::Start);
			state.succeed();
		}

		return state;
	}

	State MarkupWriter::endAttribute()
	{
		// End current value if that hasn't happened yet
		State state = this->endCurrentChildLevels(Markup::Attribute);
		if (state.succeeded())
		{
			if (mLevels.back().count == 0)
			{
				this->writeEmptyValue();
			}

			mLevels.pop_back();
		}

		return state;
	}

	State MarkupWriter::startValue(Type type)
	{
		State state;

		// Allow writing top level values for testing
		if (!mLevels.empty())
		{
			MarkupEvent &level = this->getCurrentLevel();

			switch (level.markup)
			{
				case Markup::Array:
				case Markup::Element:
				case Markup::Attribute:
				case Markup::Comment:
				{
					// these are fine
					level.count += 1;
					level.action = MarkupAction::Continue;
					break;
				}

				case Markup::Value:
				{
					if (level.type == Type::None)
					{
						level.type = type;
					}

					if (level.count > 0)
					{
						level.action = MarkupAction::Continue;
					}
					break;
				}
				
				// Everything else is not fine
				default:
					state.fail(Reason::Unexpected);
					break;
			}
		}
		else
		{
			mRootLevel.count += 1;
			mRootLevel.action = MarkupAction::Continue;
		}

		if (!state.failed())
		{
			mLevels.emplace_back(Markup::Value, MarkupAction::Start, type);
			state.succeed();
		}

		return state;
	}

	State MarkupWriter::endValue()
	{
		State state;
		
		if (mLevels.empty())
		{
			state.fail(Reason::Missing);
		}
		else if (mLevels.back().markup == Markup::Value)
		{
			mLevels.pop_back();
			state.succeed();
		}
		else
		{
			state.fail(Reason::Unexpected);
		}

		return state;
	}

	State MarkupWriter::writeValue(const Text &value)
	{
		State state = this->startValue(Type::Text);

		if (state.succeeded())
		{
			if (value.size() > 0)
			{
				this->write(value);
			}
			this->endValue();
		}

		return state;
	}

	State MarkupWriter::writeValue(bool value)
	{
		State state = this->startValue(Type::Boolean);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(int value)
	{
		State state = this->startValue(Type::Integer);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(unsigned value)
	{
		State state = this->startValue(Type::Unsigned);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(long value)
	{
		State state = this->startValue(Type::Integer);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(unsigned long value)
	{
		State state = this->startValue(Type::Unsigned);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(long long value)
	{
		State state = this->startValue(Type::Integer);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(unsigned long long value)
	{
		State state = this->startValue(Type::Unsigned);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(float value)
	{
		State state = this->startValue(Type::Real);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(double value)
	{
		State state = this->startValue(Type::Real);
		if (state.succeeded())
		{
			this->write(value);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeValue(const void *bytes, std::size_t length)
	{
		State state = this->startValue(Type::Blob);
		if (state.succeeded())
		{
			std::string printed = cio::printBytes(bytes, length);
			this->write(printed);
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::writeEmptyValue()
	{
		State state = this->startValue(Type::None);
		if (state.succeeded())
		{
			this->endValue();
		}
		return state;
	}

	State MarkupWriter::startComment()
	{
		State state;

		if (!mLevels.empty())
		{
			MarkupEvent &level = mLevels.back();
			switch (level.markup)
			{
				case Markup::Attribute:
					this->endAttribute();
					break;

				case Markup::Comment:
					this->endComment(); // split into separate comments
					break;

				case Markup::Instruction:
					this->endInstruction();
					break;

				case Markup::Value:
					this->endValue();
					break;

				default:
					// rest are fine
					break;
			}
		}

		mLevels.emplace_back(Markup::Comment, MarkupAction::Start);
		state.succeed();
		return state;
	}

	State MarkupWriter::endComment()
	{
		State state;

		if (mLevels.empty())
		{
			state.fail(Reason::Missing);
		}
		else
		{
			if (mLevels.back().markup == Markup::Comment)
			{
				state.succeed();
			}
			else
			{
				state.fail(Reason::Missing);
			}
		}

		return state;
	}

	State MarkupWriter::writeComment(Text text)
	{
		State state = this->startComment();
		if (state.succeeded())
		{
			state = this->writeValue(std::move(text));
			this->endComment();
		}
		return state;
	}

	State MarkupWriter::startInstruction(Text key)
	{
		State state;

		if (!mLevels.empty())
		{
			switch (mLevels.back().markup)
			{
				case Markup::Attribute:
					this->endAttribute();
					break;

				case Markup::Comment:
					this->endComment();
					break;

				case Markup::Value:
					this->endValue();
					break;

				case Markup::Instruction:
					this->endInstruction();
					break;

				default:
					// everything else is fine
					break;
			}
		}

		if (!state.failed())
		{
			mLevels.emplace_back(std::move(key), Markup::Instruction, MarkupAction::Start);
			state.succeed();
			++mDepth;
		}

		return state;
	}

	State MarkupWriter::endInstruction()
	{
		State state;

		if (mLevels.empty())
		{
			state.fail(Reason::Missing);
		}
		else
		{
			if (mLevels.back().markup == Markup::Instruction)
			{
				mLevels.pop_back();
				state.succeed();
				--mDepth;
			}
			else
			{
				state.fail(Reason::Unexpected);
			}
		}

		return state;
	}
	
	State MarkupWriter::startArray(std::size_t count, Type type)
	{
		State state;

		if (!mLevels.empty())
		{
			bool done = false;
			while (!done)
			{
				MarkupEvent &level = mLevels.back();
				switch (level.markup)
				{
					case Markup::Attribute:
						this->endAttribute();
						break;

					case Markup::Comment:
						this->endComment();
						break;

					case Markup::Instruction:
						this->endInstruction();
						break;

					case Markup::Array:
					case Markup::Element:
						done = true;
						level.count += 1;
						level.action = MarkupAction::Continue;
						break;

					default:
						state.fail(Reason::Unexpected);
						done = true;
						break;
				}
			}
		}
		else
		{
			mRootLevel.count += 1;
			mRootLevel.action = MarkupAction::Continue;
			mRootLevel.type = Type::Object;
		}

		if (!state.failed())
		{
			mLevels.emplace_back(Markup::Array, MarkupAction::Start, type, count);
			state.succeed();
			++mDepth;
		}

		return state;
	}

	State MarkupWriter::endArray()
	{
		State state = this->endCurrentChildLevels(Markup::Array);
		if (state.succeeded())
		{
			--mDepth;
			mLevels.pop_back();
		}

		return state;
	}

	void MarkupWriter::setIndent(char c, unsigned amount)
	{
		mIndentChar = c;
		mIndent = amount;
	}

	void MarkupWriter::setIndentChar(char c)
	{
		mIndentChar = c;
	}

	char MarkupWriter::getIndentChar() const
	{
		return mIndentChar;
	}

	void MarkupWriter::setIndentAmount(unsigned amt)
	{
		mIndent = amt;
	}

	unsigned MarkupWriter::getIndentAmount() const
	{
		return mIndent;
	}

	unsigned MarkupWriter::getCurrentIndentAmount() const
	{
		return mIndent * mDepth;
	}

	Newline MarkupWriter::getNewlineStyle() const noexcept
	{
		return mNewline;
	}

	void MarkupWriter::setNewlineStyle(Newline newline) noexcept
	{
		mNewline = newline;
	}

	Buffer &MarkupWriter::getWriteBuffer() noexcept
	{
		return mWriteBuffer;
	}

	const Buffer &MarkupWriter::getWriteBuffer() const noexcept
	{
		return mWriteBuffer;
	}

	void MarkupWriter::setWriteBuffer(Buffer buffer) noexcept
	{
		mWriteBuffer = std::move(buffer);
	}

	Buffer MarkupWriter::takeWriteBuffer() noexcept
	{
		return std::move(mWriteBuffer);
	}

	void MarkupWriter::indent()
	{
		if (mNewline != Newline::None)
		{
			unsigned amount = mDepth * mIndent;
			this->splat(mIndentChar, amount);
		}
	}

	void MarkupWriter::indent(int relative)
	{
		if (mNewline != Newline::None)
		{
			if (relative > -static_cast<int>(mDepth))
			{
				unsigned amount = (mDepth + relative) * mIndent;
				this->splat(mIndentChar, amount);
			}
		}
	}

	void MarkupWriter::indentAbsolute(unsigned depth)
	{
		if (mNewline != Newline::None)
		{
			unsigned amount = depth * mIndent;
			this->splat(mIndentChar, amount);
		}
	}


	void MarkupWriter::splat(char c, std::size_t length)
	{
		// First try to generate enough room in the buffer, possibly draining to the output
		if (!mFilterWrites)
		{
			std::size_t available = this->remaining(length);
			if (available >= length)
			{
				mWriteBuffer.splat(c, length);
			}
			// If we still don't have enough room, we have to write straight to the device
			else if (mOutput)
			{
				mOutput->splat(c, length);
			}
			else
			{
				throw Exception(Action::Write, Reason::Overflow);
			}
		}
	}

	std::size_t MarkupWriter::remaining(std::size_t needed)
	{
		std::size_t available = mWriteBuffer.remaining();
		if (available < needed)
		{
			if (mOutput)
			{
				mWriteBuffer.flip();
				mOutput->drain(mWriteBuffer);
				mWriteBuffer.reset();
			}

			available = mWriteBuffer.remaining(needed);
		}

		return available;
	}

	std::size_t MarkupWriter::writable(std::size_t needed)
	{
		std::size_t available = mWriteBuffer.remaining();
		if (available < needed)
		{
			if (mOutput)
			{
				mWriteBuffer.flip();
				mOutput->drain(mWriteBuffer);
				mWriteBuffer.reset();
			}

			available = mWriteBuffer.writable(needed);
		}

		return available;
	}

	void MarkupWriter::write(const char *text, std::size_t length)
	{
		if (!mFilterWrites)
		{
			// First try to generate enough room in the buffer, possibly draining to the output
			std::size_t available = this->remaining(length);
			if (available >= length)
			{
				mWriteBuffer.write(text, length);
			}
			// If we still don't have enough room, we have to write straight to the device
			else if (mOutput)
			{
				mOutput->write(text, length);
			}
			else
			{
				throw Exception(Action::Write, Reason::Overflow);
			}
		}
	}

	MarkupEvent &MarkupWriter::getCurrentLevel() noexcept
	{
		return mLevels.empty() ? mRootLevel : mLevels.back();
	}

	const MarkupEvent &MarkupWriter::getCurrentLevel() const noexcept
	{
		return mLevels.empty() ? mRootLevel : mLevels.back();
	}

	MarkupEvent &MarkupWriter::getCurrentLevel(Markup type) noexcept
	{
		return this->getParentLevel(this->hasCurrentLevel(type));
	}

	const MarkupEvent &MarkupWriter::getCurrentLevel(Markup type) const noexcept
	{
		return this->getParentLevel(this->hasCurrentLevel(type));
	}

	std::size_t MarkupWriter::hasCurrentLevel(Markup type) const noexcept
	{
		std::size_t depth = mLevels.size();
		for (std::size_t i = depth; i > 0; --i)
		{
			if (mLevels[i - 1].markup == type)
			{
				depth -= i;
				break;
			}
		}
		return depth;
	}

	MarkupEvent &MarkupWriter::getParentLevel(std::size_t depth) noexcept
	{
		return depth < mLevels.size() ? mLevels[mLevels.size() - depth - 1] : mRootLevel;
	}

	const MarkupEvent &MarkupWriter::getParentLevel(std::size_t depth) const noexcept
	{
		return depth < mLevels.size() ? mLevels[mLevels.size() - depth - 1] : mRootLevel;
	}

	State MarkupWriter::endCurrentLevel()
	{
		State state;
		MarkupEvent &level = this->getCurrentLevel();
		switch (level.markup)
		{
			case Markup::Array:
				this->endArray();
				break;

			case Markup::Attribute:
				this->endAttribute();
				break;

			case Markup::Comment:
				this->endComment();
				break;

			case Markup::Document:
				this->endDocument();
				break;

			case Markup::Element:
				this->endElement();
				break;

			case Markup::Instruction:
				this->endInstruction();
				break;

			case Markup::Value:
				this->endValue();
				break;

			default:
				state.fail(Reason::Missing);
				break;
		}

		if (!state.completed())
		{
			state.succeed();
		}

		return state;
	}

	State MarkupWriter::endCurrentLevels(std::size_t depth)
	{
		State state;
		std::size_t levels = mLevels.size();
		std::size_t toEnd = std::min(levels, depth);
		for (std::size_t i = 0; i < toEnd; ++i)
		{
			this->endCurrentLevel();
		}

		if (levels < depth)
		{
			state.fail(Reason::Missing);
		}
		else
		{
			state.succeed();
		}

		return state;
	}

	State MarkupWriter::endCurrentChildLevels(Markup type)
	{
		State state;
		std::size_t depth = this->hasCurrentLevel(type);
		if (depth == mLevels.size())
		{
			state.fail(Reason::Missing);
		}
		else if (depth > 0)
		{
			state = this->endCurrentLevels(depth);
		}
		else
		{
			state.succeed();
		}
		return state;
	}
}
