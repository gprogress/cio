/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_REASON_H
#define CIO_REASON_H

#include "Types.h"

#include "Enumeration.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Reason enumeration describes the general categories of reasons for the outcome of a particular action.
	 *
	 * In most cases these are explaining a reason for failure and are mapped from system error codes;
	 * generally one does not need a reason for success.
	 *
	 * However, whether a particular reason is actually a failure is often contextual - for example
	 * Reason::Missing is an error when trying to open a file, but merely informational when attempting to remove a file.
	 */
	enum class Reason : std::uint8_t
	{
		// Default state

		/** No reason is specified */
		None,
		
		/** The reason is unknown or was mapped from an unrecognized system error */
		Unknown,
		
		// Blame the user
		
		/** The application user or an agent on their behalf made a decision */
		User,
	
		// Issues related to interruptions
		
		/** The operation was interrupted while in progress or spuriously returned */
		Interrupted,
		
		/** The operation was interrupted by a connection reset event */
		Reconnected,
		
		/** The operation took too long to complete */
		Timeout,
				
		/** The operation was canceled */
		Canceled,

		// Data availability issues not due to permissions
		
		/** The resource is currently busy */
		Busy,

		/** The operation failed due to hardware error */
		Hardware,
		
		/** The resource has never been connected, opened, or set up */
		Unopened,
		
		/** The operation tried to read more data than existed or write less data than required */
		Underflow,

		/** The operation tried to write more data than existed or read less data than required */
		Overflow,
		
		/** The operation failed because limits on native OS-level resources such as file or socket handles were reached */
		Exhausted,

		/** The operation was a request but no corresponding response was ever received within the defined timeout */
		Unanswered,

		/** The operation resulted in an infinite loop of redirection */
		Infinite,

		// User error or protocol problems

		/** The caller provided invalid or bad parameters */
		Invalid,
		
		/** The operation is possible but the code to do so is not yet implemented */
		Unimplemented,
		
		/** The operation required a particular data length alignment or total size and this was not met */
		Misaligned,
		
		/** The operation's underlying protocol packet or format used to execute the request was not understood by the receiver */
		Unparsable,
		
		/** The operation's underlying protocol packet or format used to execute the request returned an unexpected value or response */
		Unexpected,

		/** The operation's underlying protocol packet or format is in an incompatible version */
		Incompatible,
		
		// Existence and Connection Failure States
		
		/** The necessary resource already exists */
		Exists,

		/** The necessary resource did not exist */
		Missing,

		/** The hardware needed was physically disconnected */
		Unplugged,
		
		/** The logical transport layer is already connected */
		Connected,

		/** The logical transport layer unexpectedly disconnected */
		Disconnected,

		/** Some kind of network traversal was needed and no valid route was found */
		Unreachable,
		
		// Unsupported operations failure states
				
		/** The operation is not implemented or supported by the transport layer without further explanation */
		Unsupported,
			
		/** The resource exists but is not usable for the type of operation requested */	
		Unusable,
				
		/** The resource is incapable of being read regardless of authentication */
		Unreadable,
		
		/** The resource is incapable of being written regardless of authentication */
		Unwritable,
		
		/** The resource is incapable of being removed regardless of authentication */
		Unremovable,
		
		/** The resource is incapable of being created regardless of authentication */
		Uncreatable,
		
		/** The resource is incapable of seeking or reading/writing at a particular position */
		Unseekable,

		/** The resource is not a program that can be executed */
		Unexecutable,

		// Authorization Refusal Failure States

		/** The operation was generically refused without further explanation */
		Refused,
		
		/** The operation was blocked by general policy rules such as firewalls or ban lists */
		Blocked,
		
		/** The operation required authentication but none was attempted */
		Uncredentialed,
		
		/** The operation required authentication but the credentialed user is not authorized to perform it */
		Unauthorized,

		/** The operation required authentication or encryption and the user provided it, but it was not secure enough */
		Insecure,
		
		/** The operation would normally have succeeded but the user hit a policy quota on resource limits */
		Quota
	};

	/**
	 * Specializes Enumeration for the C++ Reason enum.
	 */
	template <>
	class CIO_API Enumeration<Reason> final : public Enumerants<Reason>
	{
		public:
			/** The array of enum values */
			static const Enumerant<Reason> values[];

			/** The enumeration global instance */
			static const Enumeration<Reason> instance;

			/** Constructor */
			Enumeration();

			/** Destructor */
			virtual ~Enumeration() noexcept override;
	};

	/**
	 * Prints a Reason to a C++ stream.
	 * 
	 * @param stream The stream
	 * @param reason The reason
	 * @return the stream
	 * @throw std::exception If the stream throws an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, Reason reason);
			
	/**
	 * Maps the C and POSIX standard errno codes to Reason generically using the current value of errno in the calling thread.
	 *
	 * @note This mapping may not be exact since each C or POSIX function has its own interpretation of what a particular error code means.
	 * All error codes not otherwise mapped are mapped to Reason::Unknown.
	 * 
	 * @return the best matching error reason for the current value of errno
	 */
	CIO_API Reason error() noexcept;

	/**
	 * Maps the C and POSIX standard errno codes to the Reason generically.
	 *
	 * @note This mapping may not be exact since each C or POSIX function has its own interpretation of what a particular error code means.
	 * All error codes not otherwise mapped are mapped to Reason::Unknown.
	 * 
	 * @param error The C/POSIX error code
	 * @return the best matching error reason
	 */
	CIO_API Reason error(int error) noexcept;
	
	/**
	 * Maps the Windows error codes to Reason generically using the current value of ::GetLastError().
	 *
	 * @note This mapping may not be exact since each Windows function has its own interpretation of what a particular error code means.
	 * All error codes not otherwise mapped are mapped to Reason::Unknown.
	 * On non-Windows platforms this always returns Reason::Unsupported.
	 * 
	 * @return the best matching error reason
	 */
	CIO_API Reason errorWin32() noexcept;
	
	/**
	 * Maps the Windows error codes to Reason generically.
	 * This will handle values such as those returned by ::GetLastError().
	 *
	 * @note This mapping may not be exact since each Windows function has its own interpretation of what a particular error code means.
	 * All error codes not otherwise mapped are mapped to Status::Failure.
	 * 
	 * @param error The error code
	 * @return the best matching error reason
	 */
	CIO_API Reason errorWin32(int error) noexcept;
}


#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
