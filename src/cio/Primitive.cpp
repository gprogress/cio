/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Primitive.h"

#include "Type.h"

#include <ostream>

namespace cio
{
	struct ComponentRecord
	{
		Primitive type;
		
		const char *label;
		
		Type category;
	};

	static const ComponentRecord sComponentRecords[] = {
		{ Primitive::None, "None", Type::None },
		{ Primitive::Boolean, "Boolean", Type::Boolean },
		{ Primitive::Unsigned4, "Unsigned4", Type::Unsigned },
		{ Primitive::Unsigned8, "Unsigned8", Type::Unsigned },
		{ Primitive::Unsigned16, "Unsigned16", Type::Unsigned },
		{ Primitive::Unsigned32, "Unsigned32", Type::Unsigned },
		{ Primitive::Unsigned64, "Unsigned64", Type::Unsigned },
		{ Primitive::Unsigned128, "Unsigned128", Type::Unsigned },
		{ Primitive::Unsigned256, "Unsigned256", Type::Unsigned },
		{ Primitive::Signed4, "Signed4", Type::Integer },
		{ Primitive::Signed8, "Signed8", Type::Integer },
		{ Primitive::Signed16, "Signed16", Type::Integer },
		{ Primitive::Signed32, "Signed32", Type::Integer },
		{ Primitive::Signed64, "Signed64", Type::Integer },
		{ Primitive::Signed128, "Signed128", Type::Integer },
		{ Primitive::Signed256, "Signed256", Type::Integer },
		{ Primitive::Float8, "Float8",  Type::Real },
		{ Primitive::Float16, "Float16", Type::Real },
		{ Primitive::Float32, "Float32", Type::Real },
		{ Primitive::Float64, "Float64", Type::Real },
		{ Primitive::Float128, "Float128", Type::Real },
		{ Primitive::Char8, "Char8",  Type::Text },
		{ Primitive::Char16, "Char16", Type::Text },
		{ Primitive::Char32, "Char32", Type::Text },
		{ Primitive::Bit, "Bit", Type::Blob },
		{ Primitive::Nibble, "Nibble", Type::Blob },
		{ Primitive::Byte, "Byte", Type::Blob },
		{ Primitive::Word16, "Word16", Type::Blob },
		{ Primitive::Word32, "Word32", Type::Blob },
		{ Primitive::Word64, "Word64", Type::Blob },
		{ Primitive::Word128, "Word128", Type::Blob },
		{ Primitive::Word256, "Word256", Type::Blob },
		{ Primitive::Default, "Quantized", Type::Real },
		{ Primitive::Quantized, "Default", Type::Any },
		{ Primitive::Variant, "Variant", Type::Any },
	};

	const char *print(Primitive type)
	{
		const char *text = "Unknown";
		
		for (const ComponentRecord &r : sComponentRecords)
		{
			if (r.type == type)
			{
				text = r.label;
				break;
			}
		}
	
		return text;
	}
	
	Type type(Primitive type) noexcept
	{
		Type prim = Type::Any;
		
		for (const ComponentRecord &r : sComponentRecords)
		{
			if (r.type == type)
			{
				prim = r.category;
				break;
			}
		}
		
		return prim;
	}

	std::ostream &operator<<(std::ostream &s, Primitive type)
	{
		return s << print(type);
	}
}
