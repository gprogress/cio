/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "PathTest.h"

#include <cio/test/Assert.h>

#include <cio/Path.h>
#include<iostream>
namespace cio
{
	PathTest::PathTest() :
		cio::test::Suite("cio/Path")
	{
		CIO_TEST_METHOD(cio::PathTest, hasNativeRoot);
		CIO_TEST_METHOD(cio::PathTest, hasWindowsRoot);
		CIO_TEST_METHOD(cio::PathTest, hasUnixRoot);
		CIO_TEST_METHOD(cio::PathTest, fromNativeFile);
		CIO_TEST_METHOD(cio::PathTest, fromWindowsFile);
		CIO_TEST_METHOD(cio::PathTest, fromUnixFile);
		CIO_TEST_METHOD(cio::PathTest, getNativeSeparator);
		CIO_TEST_METHOD(cio::PathTest, isNativeSeparator);
		CIO_TEST_METHOD(cio::PathTest, isWindowsSeparator);
		CIO_TEST_METHOD(cio::PathTest, isUnixSeparator);
		CIO_TEST_METHOD(cio::PathTest, validateNativeInputText);

		CIO_TEST_METHOD(cio::PathTest, setRoot);
		CIO_TEST_METHOD(cio::PathTest, setPath);
		CIO_TEST_METHOD(cio::PathTest, merge);
		CIO_TEST_METHOD(cio::PathTest, getExtension);
		CIO_TEST_METHOD(cio::PathTest, getQuery);
		CIO_TEST_METHOD(cio::PathTest, getParent);
		CIO_TEST_METHOD(cio::PathTest, getFilename);
		CIO_TEST_METHOD(cio::PathTest, getFilenameNoExtension);
		CIO_TEST_METHOD(cio::PathTest, setProtocol);
		CIO_TEST_METHOD(cio::PathTest, matchesProtocol);
		CIO_TEST_METHOD(cio::PathTest, isFileProtocol);
		CIO_TEST_METHOD(cio::PathTest, matchesExtension);
		CIO_TEST_METHOD(cio::PathTest, setExtension);
		CIO_TEST_METHOD(cio::PathTest, removeExtension);
		CIO_TEST_METHOD(cio::PathTest, isRelative);
		CIO_TEST_METHOD(cio::PathTest, isComplete);
		CIO_TEST_METHOD(cio::PathTest, isRoot);
		CIO_TEST_METHOD(cio::PathTest, complete);
		CIO_TEST_METHOD(cio::PathTest, makeCompletePath);
		CIO_TEST_METHOD(cio::PathTest, makeRelativePath);
		CIO_TEST_METHOD(cio::PathTest, makePathWithExtension);
		
		CIO_TEST_METHOD(cio::PathTest, addChild);
		CIO_TEST_METHOD(cio::PathTest, toNativeFile);
		CIO_TEST_METHOD(cio::PathTest, toWindowsFile);
		CIO_TEST_METHOD(cio::PathTest, toUnixFile);
		CIO_TEST_METHOD(cio::PathTest, validateNativeInput);

		CIO_TEST_METHOD(cio::PathTest, encode);
		CIO_TEST_METHOD(cio::PathTest, decode);
	}

	PathTest::~PathTest() noexcept = default;

	void PathTest::hasNativeRootTest()
	{
		CIO_ASSERT_EQUAL(0, cio::Path::hasNativeRoot("", 1));
		
		CIO_ASSERT_EQUAL(0, cio::Path::hasNativeRoot("root", 4));
		
		CIO_ASSERT_EQUAL(0, cio::Path::hasNativeRoot("root/path/file.png", 19));
		CIO_ASSERT_EQUAL(1, cio::Path::hasNativeRoot("/root/path/file.png", 21));
		
		CIO_ASSERT_EQUAL(0, cio::Path::hasNativeRoot("https://root/path/file.png", 27));
		CIO_ASSERT_EQUAL(0, cio::Path::hasNativeRoot("file://root/path/file.png", 26));
#ifdef _WIN32
		CIO_ASSERT_EQUAL(3, cio::Path::hasNativeRoot("C://root/path/file.png", 23));
#endif
	}

	void PathTest::hasWindowsRootTest()
	{
		CIO_ASSERT_EQUAL(0, cio::Path::hasWindowsRoot("", 1));
		
		CIO_ASSERT_EQUAL(0, cio::Path::hasWindowsRoot("root", 4));
		
		CIO_ASSERT_EQUAL(0, cio::Path::hasWindowsRoot("root/path/file.png", 19));
		CIO_ASSERT_EQUAL(1, cio::Path::hasWindowsRoot("/root/path/file.png", 20));
		CIO_ASSERT_EQUAL(1, cio::Path::hasWindowsRoot("/root/path/file.png", 21));
		
		CIO_ASSERT_EQUAL(3, cio::Path::hasWindowsRoot("C://root/path/file.png", 23));
		CIO_ASSERT_EQUAL(0, cio::Path::hasWindowsRoot("https://root/path/file.png", 27));
		CIO_ASSERT_EQUAL(0, cio::Path::hasWindowsRoot("file://root/path/file.png", 26));
	}
	
	void PathTest::hasUnixRootTest()
	{
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("", 1));
		
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("root", 4));
		
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("root/path/file.png", 19));
		CIO_ASSERT_EQUAL(true, cio::Path::hasUnixRoot("/root/path/file.png", 20));
		CIO_ASSERT_EQUAL(true, cio::Path::hasUnixRoot("//root/path/file.png", 21));
		
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("C://root/path/file.png", 23));
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("https://root/path/file.png", 27));
		CIO_ASSERT_EQUAL(false, cio::Path::hasUnixRoot("file://root/path/file.png", 26));
	}

	void PathTest::fromNativeFileTest()
	{
#ifdef _WIN32
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromNativeFile("C://root/path/file.png", 22));
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromNativeFile("C://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromNativeFile(std::string("C://root/path/file.png")));
#endif
		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromNativeFile("", 0));
		CIO_ASSERT_EQUAL("root", cio::Path::fromNativeFile("root", 4));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromNativeFile("https://root/path/file.png", 26));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromNativeFile("file://root/path/file.png", 25));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromNativeFile("root/path/file.png", 18));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromNativeFile("/root/path/file.png", 19).c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromNativeFile("//root/path/file.png", 20).c_str());

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromNativeFile(""));
		CIO_ASSERT_EQUAL("root", cio::Path::fromNativeFile("root"));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromNativeFile("https://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromNativeFile("file://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromNativeFile("root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromNativeFile("/root/path/file.png").c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromNativeFile("//root/path/file.png").c_str());

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromNativeFile(std::string("")));
		CIO_ASSERT_EQUAL("root", cio::Path::fromNativeFile(std::string("root")));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromNativeFile(std::string("https://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromNativeFile(std::string("file://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromNativeFile(std::string("root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromNativeFile(std::string("/root/path/file.png")).c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromNativeFile(std::string("//root/path/file.png")).c_str());
	}
	
	void PathTest::fromWindowsFileTest()
	{
		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromWindowsFile("", 0));
		CIO_ASSERT_EQUAL("root", cio::Path::fromWindowsFile("root", 4));
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromWindowsFile("C://root/path/file.png", 22));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromWindowsFile("https://root/path/file.png", 26));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromWindowsFile("file://root/path/file.png", 25));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromWindowsFile("root/path/file.png", 18));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromWindowsFile("/root/path/file.png", 19).c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromWindowsFile("//root/path/file.png", 20).c_str());

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromWindowsFile(""));
		CIO_ASSERT_EQUAL("root", cio::Path::fromWindowsFile("root"));
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromWindowsFile("C://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromWindowsFile("https://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromWindowsFile("file://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromWindowsFile("root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromWindowsFile("/root/path/file.png").c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromWindowsFile("//root/path/file.png").c_str());

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromWindowsFile(std::string("")));
		CIO_ASSERT_EQUAL("root", cio::Path::fromWindowsFile(std::string("root")));
		CIO_ASSERT_EQUAL(cio::Path("file:///C://root/path/file.png"), cio::Path::fromWindowsFile(std::string("C://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromWindowsFile(std::string("https://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromWindowsFile(std::string("file://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromWindowsFile(std::string("root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Text("file:///root/path/file.png"), cio::Path::fromWindowsFile(std::string("/root/path/file.png")).c_str());
		CIO_ASSERT_EQUAL(cio::Text("file:////root/path/file.png"), cio::Path::fromWindowsFile(std::string("//root/path/file.png")).c_str());
	}
	
	void PathTest::fromUnixFileTest()
	{
		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromUnixFile("", 0));
		CIO_ASSERT_EQUAL("root", cio::Path::fromUnixFile("root", 4));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromUnixFile("https://root/path/file.png", 26));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromUnixFile("file://root/path/file.png", 25));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromUnixFile("root/path/file.png", 18));
		CIO_ASSERT_EQUAL(cio::Path("file:///root/path/file.png"), cio::Path::fromUnixFile("/root/path/file.png", 19));
		CIO_ASSERT_EQUAL(cio::Path("file:////root/path/file.png"), cio::Path::fromUnixFile("//root/path/file.png", 20));

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromUnixFile(""));
		CIO_ASSERT_EQUAL("root", cio::Path::fromUnixFile("root"));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromUnixFile("https://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromUnixFile("file://root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromUnixFile("root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file:///root/path/file.png"), cio::Path::fromUnixFile("/root/path/file.png"));
		CIO_ASSERT_EQUAL(cio::Path("file:////root/path/file.png"), cio::Path::fromUnixFile("//root/path/file.png"));

		CIO_ASSERT_EQUAL(cio::Path(), cio::Path::fromUnixFile(std::string("")));
		CIO_ASSERT_EQUAL("root", cio::Path::fromUnixFile(std::string("root")));
		CIO_ASSERT_EQUAL(cio::Path("https://root/path/file.png"), cio::Path::fromUnixFile(std::string("https://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("file://root/path/file.png"), cio::Path::fromUnixFile(std::string("file://root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("root/path/file.png"), cio::Path::fromUnixFile(std::string("root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("file:///root/path/file.png"), cio::Path::fromUnixFile(std::string("/root/path/file.png")));
		CIO_ASSERT_EQUAL(cio::Path("file:////root/path/file.png"), cio::Path::fromUnixFile(std::string("//root/path/file.png")));
	}
	
	void PathTest::getNativeSeparatorTest()
	{
#ifdef _WIN32
		CIO_ASSERT_EQUAL('\\', cio::Path::getNativeSeparator());// line 209
#else
		CIO_ASSERT_EQUAL('/', cio::Path::getNativeSeparator());
#endif
	}
	
	void PathTest::isNativeSeparatorTest()
	{
		CIO_ASSERT_EQUAL(true, cio::Path::isNativeSeparator('/'));
#ifdef _WIN32
		CIO_ASSERT_EQUAL(true, cio::Path::isNativeSeparator('\\'));// line 215
#endif
	}
	
	void PathTest::isWindowsSeparatorTest()
	{
		CIO_ASSERT_EQUAL(true, cio::Path::isWindowsSeparator('/'));
		CIO_ASSERT_EQUAL(true, cio::Path::isWindowsSeparator('\\'));
	}
	
	void PathTest::isUnixSeparatorTest()
	{
		CIO_ASSERT_EQUAL(true, cio::Path::isUnixSeparator('/'));
		CIO_ASSERT_EQUAL(false, cio::Path::isUnixSeparator('\\'));
	}
	
	void PathTest::validateNativeInputTextTest()
	{
#ifdef _WIN32
		CIO_ASSERT_EQUAL("C://root/path/file.png", cio::Path::validateNativeInputText("C://root/path/file.png"));
		CIO_ASSERT_EQUAL("C://root/path/file.png", cio::Path::validateNativeInputText("C:\\\\root\\path\\file.png"));
#endif

		CIO_ASSERT_EQUAL("", cio::Path::validateNativeInputText(""));
		CIO_ASSERT_EQUAL("root", cio::Path::validateNativeInputText("root"));
		CIO_ASSERT_EQUAL("https://root/path/file.png", cio::Path::validateNativeInputText("https://root/path/file.png"));
		CIO_ASSERT_EQUAL("file://root/path/file.png", cio::Path::validateNativeInputText("file://root/path/file.png"));
		CIO_ASSERT_EQUAL("root/path/file.png", cio::Path::validateNativeInputText("root/path/file.png"));
		CIO_ASSERT_EQUAL("/root/path/file.png", cio::Path::validateNativeInputText("/root/path/file.png"));
		CIO_ASSERT_EQUAL("//root/path/file.png", cio::Path::validateNativeInputText("//root/path/file.png"));
		
		CIO_ASSERT_EQUAL("https:\\\\root\\path\\file.png", cio::Path::validateNativeInputText("https:\\\\root\\path\\file.png"));
		CIO_ASSERT_EQUAL("file:\\\\root\\path\\file.png", cio::Path::validateNativeInputText("file:\\\\root\\path\\file.png"));
		CIO_ASSERT_EQUAL("root\\path\\file.png", cio::Path::validateNativeInputText("root\\path\\file.png"));
		CIO_ASSERT_EQUAL("/root/path/file.png", cio::Path::validateNativeInputText("\\root\\path\\file.png"));
		CIO_ASSERT_EQUAL("//root/path/file.png", cio::Path::validateNativeInputText("\\\\root\\path\\file.png"));
	}

	void PathTest::setRootTest()
	{
		Path filepath("");
		filepath.setRoot("");
		CIO_ASSERT_EQUAL("", filepath.getRoot());
		
		filepath = "bar.cpp";
		filepath.setRoot("root");
		CIO_ASSERT_EQUAL("", filepath.getRoot());
		CIO_ASSERT_EQUAL("rootbar.cpp", filepath.getPath());
		
		filepath = "bar.cpp";
		filepath.setRoot("C://root/");
		CIO_ASSERT_EQUAL("root/", filepath.getRoot());
		CIO_ASSERT_EQUAL("root/bar.cpp", filepath.getPath());
		
		filepath = "/foo/bar";
		filepath.setRoot("/root/");
		CIO_ASSERT_EQUAL("/", filepath.getRoot());
		CIO_ASSERT_EQUAL("/root/foo/bar", filepath.getPath());
		
		filepath = "C://foo/bar";
		filepath.setRoot("root/");
		CIO_ASSERT_EQUAL("root/", filepath.getRoot());
		CIO_ASSERT_EQUAL("C://root/foo/bar", filepath.str());
		
		filepath = "user://foo/bar";
		filepath.setRoot("root/");
		CIO_ASSERT_EQUAL("root/", filepath.getRoot());
		CIO_ASSERT_EQUAL("user://root/foo/bar", filepath.str());
		
		filepath = "https://foo.com/bar";
		filepath.setRoot("root/");
		CIO_ASSERT_EQUAL("https://root/foo.com/bar", filepath.str());
	}

	void PathTest::setPathTest()
	{
		Path filepath("");
		filepath.setPath("");
		CIO_ASSERT_EQUAL("", filepath.getPath());
		
		filepath = "";
		filepath.setPath("set/this/path");
		CIO_ASSERT_EQUAL("set/this/path", filepath.getPath());
		
		filepath = "set/this/path";
		filepath.setPath("with/a/new/one");
		CIO_ASSERT_EQUAL("with/a/new/one", filepath.getPath());
		
		filepath = "set/this/path";
		filepath.setPath("");
		CIO_ASSERT_EQUAL("", filepath.getPath());
		
		filepath = "";
		filepath.setPath("set\\this\\path");
		CIO_ASSERT_EQUAL("set\\this\\path", filepath.getPath());
		
		filepath = "set\\this\\path";
		filepath.setPath("with\\a\\new\\one");
		CIO_ASSERT_EQUAL("with\\a\\new\\one", filepath.getPath());
		
		filepath = "set\\this\\path";
		filepath.setPath("");
		CIO_ASSERT_EQUAL("", filepath.getPath());
	}

	void PathTest::mergeTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.merge(""));
		
		filepath = "";
		CIO_ASSERT_EQUAL("merge/this/path", filepath.merge("merge/this/path"));
		
		filepath = "merge/this/path";
		CIO_ASSERT_EQUAL("merge/this/path", filepath.merge("with/a/new/one"));
		
		filepath = "merge/this/path";
		CIO_ASSERT_EQUAL("merge/this/path", filepath.merge(""));

		filepath = "merge/this/path";
		const cio::Path mergePath = "with/a/new/one";
		CIO_ASSERT_EQUAL("merge/this/path", filepath.merge(mergePath));

		filepath = "merge/this/path";
		const cio::Path mergePathBlank = "";
		CIO_ASSERT_EQUAL("merge/this/path", filepath.merge(mergePathBlank));

	}

	void PathTest::getExtensionTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.getExtension());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL("txt", filepath.getExtension());

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL("", filepath.getExtension());

		filepath = "://path/file.jpeg";
		CIO_ASSERT_EQUAL("jpeg", filepath.getExtension());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL("", filepath.getExtension());

		filepath = "path/file.extend";
		CIO_ASSERT_EQUAL("extend", filepath.getExtension());

		filepath = "file.png";
		CIO_ASSERT_EQUAL("png", filepath.getExtension());

		filepath = "https://foo.com/file.png";
		CIO_ASSERT_EQUAL("png", filepath.getExtension());

		filepath = "file://foo/file.png";
		CIO_ASSERT_EQUAL("png", filepath.getExtension());
	}

	void PathTest::getQueryTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.getQuery());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL("", filepath.getQuery());

		filepath = "C://path/file/file?sample=\"query\"&or=\"something\"";
		CIO_ASSERT_EQUAL("sample=\"query\"&or=\"something\"", filepath.getQuery());

		filepath = "C://path/file/test";
		CIO_ASSERT_EQUAL("", filepath.getQuery());

		filepath = "/path/file?samplequery";
		CIO_ASSERT_EQUAL("samplequery", filepath.getQuery());

		filepath = "path/file?{query}&other";
		CIO_ASSERT_EQUAL("{query}&other", filepath.getQuery());

		filepath = "://path/file.jpeg?sample=query";
		CIO_ASSERT_EQUAL("sample=query", filepath.getQuery());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL("", filepath.getQuery());

		filepath = "path/file.extend";
		CIO_ASSERT_EQUAL("", filepath.getQuery());
		
		filepath = "?file.txt";
		CIO_ASSERT_EQUAL("file.txt", filepath.getQuery());
	}

	void PathTest::getParentTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.getParent());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL("C://path", filepath.getParent());

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL("C://path", filepath.getParent());

		filepath = "C://path/file/test";
		CIO_ASSERT_EQUAL("C://path/file", filepath.getParent());

		filepath = "C://path/file/test/test2";
		CIO_ASSERT_EQUAL("C://path/file/test", filepath.getParent());

		filepath = "C://path/file/test/test2/test3";
		CIO_ASSERT_EQUAL("C://path/file/test/test2", filepath.getParent());

		filepath = "://path/file.jpeg";
		CIO_ASSERT_EQUAL("://path", filepath.getParent());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL("/path", filepath.getParent());

		filepath = "path/file.extend";
		CIO_ASSERT_EQUAL("path", filepath.getParent());
		
		filepath = "file.txt";
		CIO_ASSERT_EQUAL("", filepath.getParent());

		filepath = "file://path/file.txt";
		CIO_ASSERT_EQUAL("file://path", filepath.getParent());
	}

	void PathTest::getFilenameTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.getFilename());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL("file.txt", filepath.getFilename());

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL("file", filepath.getFilename());

		filepath = "://path/file.jpeg";
		CIO_ASSERT_EQUAL("file.jpeg", filepath.getFilename());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL("file", filepath.getFilename());

		filepath = "path/file.extend";
		CIO_ASSERT_EQUAL("file.extend", filepath.getFilename());
		
		filepath = "file.txt";
		CIO_ASSERT_EQUAL("file.txt", filepath.getFilename());
	}

	void PathTest::getFilenameNoExtensionTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL("", filepath.getFilenameNoExtension());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());

		filepath = "://path/file.jpeg";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());

		filepath = "path/file.extend";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());
		
		filepath = "file.txt";
		CIO_ASSERT_EQUAL("file", filepath.getFilenameNoExtension());

	}

	void PathTest::setProtocolTest()
	{
		Path filepath("");
		filepath.setProtocol("");
		CIO_ASSERT_EQUAL("", filepath.getProtocol().str());

		filepath = "C://path/file.txt";
		filepath.setProtocol("");
		CIO_ASSERT_EQUAL(cio::Text("C://path/file.txt"), filepath.c_str());
		CIO_ASSERT_EQUAL("C", filepath.getProtocol());

		std::string pro = "D";
		filepath = "C://path/file";
		filepath.setProtocol("D");
		CIO_ASSERT_EQUAL(cio::Text("D://path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("D", filepath.getProtocol());

		filepath = "file://path/file";
		filepath.setProtocol("C");
		CIO_ASSERT_EQUAL(cio::Text("C://path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("C", filepath.getProtocol());

		filepath = "://path/file";
		filepath.setProtocol("file");
		CIO_ASSERT_EQUAL(cio::Text("file://://path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("file", filepath.getProtocol());

		filepath = "//path/file";
		filepath.setProtocol("A");
		CIO_ASSERT_EQUAL(cio::Text("A:////path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("A", filepath.getProtocol());

		filepath = "/path/file";
		filepath.setProtocol("C");
		CIO_ASSERT_EQUAL(cio::Text("C:///path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("C", filepath.getProtocol());

		filepath = "path/file";
		filepath.setProtocol("proto");
		CIO_ASSERT_EQUAL(cio::Text("proto://path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("proto", filepath.getProtocol());

		filepath = "\\path\\file";
		filepath.setProtocol("C");
		CIO_ASSERT_EQUAL(cio::Text("C:///path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("C", filepath.getProtocol());

		filepath = "\\\\path\\file";
		filepath.setProtocol("C");
		CIO_ASSERT_EQUAL(cio::Text("C:////path/file"), filepath.c_str());
		CIO_ASSERT_EQUAL("C", filepath.getProtocol());
	}

	void PathTest::matchesProtocolTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol("C"));

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol("C"));

		filepath = "file://path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol("file"));

		filepath = "http://path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol("http"));

		filepath = "://path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "//path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "/path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "\\path\\file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));

		filepath = "\\\\path\\file";
		CIO_ASSERT_EQUAL(true, filepath.matchesProtocol(""));
	}

	void PathTest::isFileProtocolTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(false, filepath.isFileProtocol());

		filepath = "C://path/file.txt";
		CIO_ASSERT_EQUAL(false, filepath.isFileProtocol());

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL(false, filepath.isFileProtocol());

		filepath = "file://path/file";
		CIO_ASSERT_EQUAL(true, filepath.isFileProtocol());

		filepath = "://path/file";
		CIO_ASSERT_EQUAL(false, filepath.isFileProtocol());

		filepath = "//path/file";
		CIO_ASSERT_EQUAL(true, filepath.isFileProtocol());

		filepath = "/path/file";
		CIO_ASSERT_EQUAL(true, filepath.isFileProtocol());

		filepath = "path/file";
		CIO_ASSERT_EQUAL(false, filepath.isFileProtocol());

		filepath = "\\path\\file";
		CIO_ASSERT_EQUAL(true, filepath.isFileProtocol());

		filepath = "\\\\path\\file";
		CIO_ASSERT_EQUAL(true, filepath.isFileProtocol());
	}

	void PathTest::matchesExtensionTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension(""));

		filepath = "/";
		CIO_ASSERT_EQUAL(false, filepath.matchesExtension("/"));

		filepath = ".";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension(Text(".")));

		filepath = ".png";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension(std::string(".png")));

		filepath = "file.jpeg";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension(".jpeg"));

		filepath = "path/file.psd";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension("psd"));

		filepath = "/path/file.heh";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension("heh"));

		filepath = "C://path/file.hah";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension("hah"));

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL(true, filepath.matchesExtension(""));

		filepath = "C://path/file";
		CIO_ASSERT_EQUAL(false, filepath.matchesExtension("ext"));
	}

	void PathTest::setExtensionTest()
	{
		Path filepath("");
		filepath.setExtension("");
		CIO_ASSERT_EQUAL(".", filepath.getPath());

		filepath = "/";
		filepath.setExtension("");
		CIO_ASSERT_EQUAL("/.", filepath.getPath());

		filepath = ".png";
		filepath.setExtension("file");
		CIO_ASSERT_EQUAL(".file", filepath.getPath());

		filepath = "file.jpg";
		filepath.setExtension("png");
		CIO_ASSERT_EQUAL("file.png", filepath.getPath());

		filepath = "path/file.psd";
		filepath.setExtension("");
		CIO_ASSERT_EQUAL("path/file", filepath.getPath());

		filepath = "/path/file.heh";
		filepath.setExtension("extend");
		CIO_ASSERT_EQUAL("/path/file.extend", filepath.getPath());

		filepath = "C://path/file.heh";
		filepath.setExtension("");
		CIO_ASSERT_EQUAL("path/file", filepath.getPath());

		filepath = "C://path/file";
		filepath.setExtension("end");
		CIO_ASSERT_EQUAL("path/file.end", filepath.getPath());

		filepath = "https://foo.com/file";
		filepath.setExtension("end");
		CIO_ASSERT_EQUAL("foo.com/file.end", filepath.getPath());

		filepath = "file://foo/file";
		filepath.setExtension("end");
		CIO_ASSERT_EQUAL("foo/file.end", filepath.getPath());
	}

	void PathTest::removeExtensionTest()
	{
		Path filepath("");
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("", filepath);

		filepath = "/";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("/", filepath);

		filepath = ".png";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL(".png", filepath);

		filepath = "file.jpeg";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("file", filepath);

		filepath = "path/file.psd";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("path/file", filepath);

		filepath = "/path/file.heh";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("/path/file", filepath);

		filepath = "C://path/file.heh";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("C://path/file", filepath);

		filepath = "C://path/file";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("C://path/file", filepath);

		filepath = "https://foo.com/file.psd";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("https://foo.com/file", filepath);

		filepath = "file://path/file.txt";
		filepath.removeExtension();
		CIO_ASSERT_EQUAL("file://path/file", filepath);
	}

	void PathTest::isRelativeTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "/";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());
		
		filepath = "//";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());

		filepath = "C://";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());
		
		filepath = ".";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());

		filepath = "C://foo/bar";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());

		filepath = "/foo/bar";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "./foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());

		filepath = "../foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isRelative());

		filepath = "///foo/bar";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "C:/foo/bar";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "C://foo/bar/";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());

		filepath = "C://foo/bar//";
		CIO_ASSERT_EQUAL(false, filepath.isRelative());
	}

	void PathTest::isCompleteTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(false, filepath.isComplete());

		filepath = "/";
		CIO_ASSERT_EQUAL(false, filepath.isComplete());
		
		filepath = "//";
		CIO_ASSERT_EQUAL(false, filepath.isComplete());

		filepath = "C://";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = ".";
		CIO_ASSERT_EQUAL(false, filepath.isComplete());

		filepath = "C://foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = "foo/bar";
		CIO_ASSERT_EQUAL(false, filepath.isComplete());

		filepath = "/foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = "///foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = "C:/foo/bar";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = "C://foo/bar/";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());

		filepath = "C://foo/bar//";
		CIO_ASSERT_EQUAL(true, filepath.isComplete());
	}

	void PathTest::isRootTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "C://root";
		CIO_ASSERT_EQUAL(true, filepath.isRoot());
				
		filepath = "C://root/root2/root3/root4";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "/root";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "root";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "root/";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "/root/";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "//root";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
		
		filepath = "/root/root2";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());

		filepath = "C:/root/file.png";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());

		filepath = "C://root/root2/file.ext";
		CIO_ASSERT_EQUAL(false, filepath.isRoot());
	}

	void PathTest::completeTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(filepath, filepath.complete(""));

		filepath = "C://Make/Path";
		CIO_ASSERT_EQUAL("C://Make/Path", filepath.complete(""));

		filepath = "Test";
		CIO_ASSERT_EQUAL("Make/Path/Test", filepath.complete("Make/Path/"));
		filepath = "Test";
		CIO_ASSERT_EQUAL("/Make/Path/./Test", filepath.complete("/Make/Path/."));
		filepath = "Test";
		CIO_ASSERT_EQUAL("/Make/Path/../Test", filepath.complete("/Make/Path/.."));
		filepath = "Test";
		CIO_ASSERT_EQUAL("Make/Path//Test", filepath.complete("Make/Path//"));
		filepath = "Test";
		CIO_ASSERT_EQUAL("/Make/Path//./Test", filepath.complete("/Make/Path//."));
		filepath = "Test";
		CIO_ASSERT_EQUAL("/Make/Path//../Test", filepath.complete("/Make/Path//.."));
		filepath = "Test";
		CIO_ASSERT_EQUAL("C://Make/Path/Test", filepath.complete("C://Make/Path"));
		filepath = "Test";
		CIO_ASSERT_EQUAL("C://Make/Path/Test", filepath.complete("C://Make/Path/"));

		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("Make/Path"));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("/Make/Path/."));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("/Make/Path/.."));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("Make/Path//"));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("/Make/Path//."));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("/Make/Path//.."));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("C://Make/Path"));
		filepath = "/Test";
		CIO_ASSERT_EQUAL("/Test", filepath.complete("C://Make/Path/"));
		
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("Make/Path"));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("/Make/Path/."));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("/Make/Path/.."));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("Make/Path//"));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("/Make/Path//."));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("/Make/Path//.."));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("C://Make/Path"));
		filepath = "//Test";
		CIO_ASSERT_EQUAL("//Test", filepath.complete("C://Make/Path/"));
		
		filepath = ".Test";
		CIO_ASSERT_EQUAL("Make/Path/.Test", filepath.complete("Make/Path"));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("Make/Path./.Test", filepath.complete("Make/Path."));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("/Make/Path/./.Test", filepath.complete("/Make/Path/."));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("/Make/Path/../.Test", filepath.complete("/Make/Path/.."));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("Make/Path//.Test", filepath.complete("Make/Path//"));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("/Make/Path//./.Test", filepath.complete("/Make/Path//."));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("/Make/Path//../.Test", filepath.complete("/Make/Path//.."));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("C://Make/Path/.Test", filepath.complete("C://Make/Path"));
		filepath = ".Test";
		CIO_ASSERT_EQUAL("C://Make/Path/.Test", filepath.complete("C://Make/Path/"));
		
		filepath = "..Test";
		CIO_ASSERT_EQUAL("Make/Path/..Test", filepath.complete("Make/Path"));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("Make/Path./..Test", filepath.complete("Make/Path."));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("/Make/Path/./..Test", filepath.complete("/Make/Path/."));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("/Make/Path/../..Test", filepath.complete("/Make/Path/.."));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("Make/Path//..Test", filepath.complete("Make/Path//"));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("/Make/Path//./..Test", filepath.complete("/Make/Path//."));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("/Make/Path//../..Test", filepath.complete("/Make/Path//.."));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("C://Make/Path/..Test", filepath.complete("C://Make/Path"));
		filepath = "..Test";
		CIO_ASSERT_EQUAL("C://Make/Path/..Test", filepath.complete("C://Make/Path/"));
	}

	void PathTest::makeCompletePathTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(filepath, filepath.makeCompletePath(""));

		filepath = "path";
		CIO_ASSERT_EQUAL("path", filepath.makeCompletePath(""));
		
		CIO_ASSERT_EQUAL("/path", filepath.makeCompletePath("/"));
		
		CIO_ASSERT_EQUAL("./path", filepath.makeCompletePath("."));

		CIO_ASSERT_EQUAL("file/path", filepath.makeCompletePath("file"));

		filepath = "/path";
		CIO_ASSERT_EQUAL("/path", filepath.makeCompletePath("file"));
		
		filepath = "//path";
		CIO_ASSERT_EQUAL("//path", filepath.makeCompletePath("file"));

		filepath = "./path";
		CIO_ASSERT_EQUAL("file/./path", filepath.makeCompletePath("file"));

		filepath = ".path";
		CIO_ASSERT_EQUAL("/file/.path", filepath.makeCompletePath("/file"));
	}

	void PathTest::makeRelativePathTest()
	{
		Path filepath("");
		CIO_ASSERT_EQUAL(filepath, filepath.makeRelativePath(""));
		
		filepath = "C://Make/Path/Test";
		CIO_ASSERT_EQUAL("", filepath.makeRelativePath(""));
		
		CIO_ASSERT_EQUAL("../new", filepath.makeRelativePath("C://Make/Path/new"));
		CIO_ASSERT_EQUAL("../new/", filepath.makeRelativePath("C://Make/Path/new/"));
		
		CIO_ASSERT_EQUAL("C://A/New/location", filepath.makeRelativePath("C://A/New/location"));
		CIO_ASSERT_EQUAL("C://A/New/location/", filepath.makeRelativePath("C://A/New/location/"));
		
		CIO_ASSERT_EQUAL("../../This/Relative/Path", filepath.makeRelativePath("C://Make/This/Relative/Path"));
		CIO_ASSERT_EQUAL("../../This/Relative/Path/", filepath.makeRelativePath("C://Make/This/Relative/Path/"));

		filepath = "C://Make/Path/Test/";
		CIO_ASSERT_EQUAL("", filepath.makeRelativePath(""));
		
		CIO_ASSERT_EQUAL("../new", filepath.makeRelativePath("C://Make/Path/new"));
		CIO_ASSERT_EQUAL("../new/", filepath.makeRelativePath("C://Make/Path/new/"));
		
		CIO_ASSERT_EQUAL("C://A/New/location", filepath.makeRelativePath("C://A/New/location"));
		CIO_ASSERT_EQUAL("C://A/New/location/", filepath.makeRelativePath("C://A/New/location/"));
		
		CIO_ASSERT_EQUAL("../../This/Relative/Path", filepath.makeRelativePath("C://Make/This/Relative/Path"));
		CIO_ASSERT_EQUAL("../../This/Relative/Path/", filepath.makeRelativePath("C://Make/This/Relative/Path/"));
	}

	void PathTest::makePathWithExtensionTest()
	{
		Path empty;
		CIO_ASSERT_EQUAL(empty, empty.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(empty, empty.makePathWithExtension("prj"));
		CIO_ASSERT_EQUAL(empty, empty.makePathWithExtension(".prj"));

		Path filename("sample.txt");
		CIO_ASSERT_EQUAL(Path("sample"), filename.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(Path("sample.bin"), filename.makePathWithExtension("bin"));
		CIO_ASSERT_EQUAL(Path("sample.bin"), filename.makePathWithExtension(".bin"));

		Path filenameNoExt("sample");
		CIO_ASSERT_EQUAL(Path("sample"), filenameNoExt.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(Path("sample.bin"), filenameNoExt.makePathWithExtension("bin"));
		CIO_ASSERT_EQUAL(Path("sample.bin"), filenameNoExt.makePathWithExtension(".bin"));

		Path relative("foo/bar/baz.gltf");
		CIO_ASSERT_EQUAL(Path("foo/bar/baz"), relative.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(Path("foo/bar/baz.png"), relative.makePathWithExtension("png"));
		CIO_ASSERT_EQUAL(Path("foo/bar/baz.png"), relative.makePathWithExtension(".png"));

		Path absolute("http://foo.com/baz.tgz");
		CIO_ASSERT_EQUAL(Path("http://foo.com/baz"), absolute.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(Path("http://foo.com/baz.gz"), absolute.makePathWithExtension("gz"));
		CIO_ASSERT_EQUAL(Path("http://foo.com/baz.gz"), absolute.makePathWithExtension(".gz"));

		Path multiext("file:///woot/item.tar.bz2");
		CIO_ASSERT_EQUAL(Path("file:///woot/item.tar"), multiext.makePathWithExtension(""));
		CIO_ASSERT_EQUAL(Path("file:///woot/item.tar.xz"), multiext.makePathWithExtension("xz"));
		CIO_ASSERT_EQUAL(Path("file:///woot/item.tar.xz"), multiext.makePathWithExtension(".xz"));
	}

	void PathTest::addChildTest()
	{
		Path filepath;
		CIO_ASSERT_EQUAL("", filepath.addChild(""));

		filepath = "/root";
		CIO_ASSERT_EQUAL("/root/path/file.cpp", filepath.addChild("path/file.cpp"));
		
		filepath = "file://root";
		CIO_ASSERT_EQUAL("file://root/path/file.cpp", filepath.addChild("path/file.cpp"));
		
		filepath = "http://root";
		CIO_ASSERT_EQUAL("http://root/path/file.cpp", filepath.addChild("path/file.cpp"));
		
		filepath = "http://root";
		CIO_ASSERT_EQUAL("http://root/path/file", filepath.addChild("path/file"));
		
		filepath = "http://root";
		CIO_ASSERT_EQUAL("http://root", filepath.addChild("/path/file"));
	}

	void PathTest::toNativeFileTest()
	{
		Path filepath;
		char actual[30] = "\0";
#if defined _WIN32
		CIO_ASSERT_EQUAL(0, filepath.toNativeFile(actual, 0));
		CIO_ASSERT_EQUAL("", filepath);

		std::memset(actual, '\0', 30);
		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL(19, filepath.toNativeFile(actual, 19));
		CIO_ASSERT_EQUAL(cio::Text("\\root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toNativeFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toNativeFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL(14, filepath.toNativeFile(actual, 21));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file"), actual);
		
		filepath = "";
		CIO_ASSERT_EQUAL("", filepath.toNativeFile());

		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL("\\root\\path\\file.cpp", filepath.toNativeFile());
		
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root\\path\\file.cpp", filepath.toNativeFile());
		
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root\\path\\file.cpp", filepath.toNativeFile());
		
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL("root\\path\\file", filepath.toNativeFile());
#else
		CIO_ASSERT_EQUAL(0, filepath.toNativeFile(actual, 0));
		CIO_ASSERT_EQUAL("", filepath);

		std::memset(actual, '\0', 30);
		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL(19, filepath.toNativeFile(actual, 19));
		CIO_ASSERT_EQUAL(cio::Text("/root/path/file.cpp"), actual);

		std::memset(actual, '\0', 30);
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toNativeFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file.cpp"), actual);

		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toNativeFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file.cpp"), actual);

		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL(14, filepath.toNativeFile(actual, 21));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file"), actual);

		filepath = "";
		CIO_ASSERT_EQUAL("", filepath.toNativeFile());

		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL("/root/path/file.cpp", filepath.toNativeFile());

		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root/path/file.cpp", filepath.toNativeFile());

		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root/path/file.cpp", filepath.toNativeFile());

		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL("root/path/file", filepath.toNativeFile());
#endif
	}

	void PathTest::toWindowsFileTest()
	{
		Path filepath;
		char actual[30] = "\0";
		CIO_ASSERT_EQUAL(0, filepath.toWindowsFile(actual, 0));
		CIO_ASSERT_EQUAL("", filepath);

		std::memset(actual, '\0', 30);
		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL(19, filepath.toWindowsFile(actual, 19));
		CIO_ASSERT_EQUAL(cio::Text("\\root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toWindowsFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toWindowsFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL(14, filepath.toWindowsFile(actual, 21));
		CIO_ASSERT_EQUAL(cio::Text("root\\path\\file"), actual);
		
		filepath = "";
		CIO_ASSERT_EQUAL("", filepath.toWindowsFile());

		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL("\\root\\path\\file.cpp", filepath.toWindowsFile());
		
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root\\path\\file.cpp", filepath.toWindowsFile());
		
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root\\path\\file.cpp", filepath.toWindowsFile());
		
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL("root\\path\\file", filepath.toWindowsFile());
	}

	void PathTest::toUnixFileTest()
	{
		Path filepath;
		char actual[30] = "\0";
		CIO_ASSERT_EQUAL(0, filepath.toUnixFile(actual, 0));
		CIO_ASSERT_EQUAL("", filepath);

		std::memset(actual, '\0', 30);
		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL(19, filepath.toUnixFile(actual, 19));
		CIO_ASSERT_EQUAL(cio::Text("/root/path/file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toUnixFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL(18, filepath.toUnixFile(actual, 25));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file.cpp"), actual);
		
		std::memset(actual, '\0', 30);
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL(14, filepath.toUnixFile(actual, 21));
		CIO_ASSERT_EQUAL(cio::Text("root/path/file"), actual);
		
		filepath = "";
		CIO_ASSERT_EQUAL("", filepath.toUnixFile());

		filepath = "/root/path/file.cpp";
		CIO_ASSERT_EQUAL("/root/path/file.cpp", filepath.toUnixFile());
		
		filepath = "file://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root/path/file.cpp", filepath.toUnixFile());
		
		filepath = "http://root/path/file.cpp";
		CIO_ASSERT_EQUAL("root/path/file.cpp", filepath.toUnixFile());
		
		filepath = "http://root/path/file";
		CIO_ASSERT_EQUAL("root/path/file", filepath.toUnixFile());
	}

	void PathTest::validateNativeInputTest()
	{
		cio::Path filepath;
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("", filepath);

		filepath = "root";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("root", filepath);
		
		filepath = "https://root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("https://root/path/file.png", filepath);
		
		filepath = "file://root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("file://root/path/file.png", filepath);
		
		filepath = "root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("root/path/file.png", filepath);
		
		filepath = "/root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("/root/path/file.png", filepath);
		
		filepath = "//root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("//root/path/file.png", filepath);

#ifdef _WIN32

		filepath = "C://root/path/file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("C://root/path/file.png", filepath);

		filepath = "C:\\\\root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("C://root/path/file.png", filepath);
		
		filepath = "https:\\\\root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("https:\\\\root\\path\\file.png", filepath);
		
		filepath = "file:\\\\root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("file:\\\\root\\path\\file.png", filepath);
		
		filepath = "root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("root\\path\\file.png", filepath);
		
		filepath = "\\root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("/root/path/file.png", filepath);
		
		filepath = "\\\\root\\path\\file.png";
		filepath.validateNativeInput();
		CIO_ASSERT_EQUAL("//root/path/file.png", filepath);
#endif
	}

	void PathTest::encodeTest()
	{
		std::string decoded = " oi\n_d~\x01_39%0G1.-ef\t!.sd\x0F";
		std::string encoded = "%20oi%0A_d~%01_39%250G1.-ef%09%21.sd%0F";

		bool success = Path::encode(decoded);
		CIO_ASSERT(success);
		CIO_ASSERT_EQUAL(encoded, decoded);
		CIO_ASSERT_EQUAL(encoded.size(), decoded.size());
	}

	void PathTest::decodeTest()
	{
		std::string decoded = " oi\n_d~\x01_39%0G1.-ef\t!.sd\x0F";
		std::string encoded = "%20oi%0A_d~%01_39%250G1.-ef%09%21.sd%0F";

		bool success = Path::decode(encoded);

		CIO_ASSERT(success);
		CIO_ASSERT_EQUAL(decoded, encoded);
		CIO_ASSERT_EQUAL(decoded.size(), encoded.size());

		std::string undecodableInputs[3] =
			{
				"Aa9%2G_", // %2G is not hex
				"~.d%0", // %0 is not in %NN format
				"gh%8Foi%0A" // %8F does not fit in 8 bits
			};

		for (std::string input : undecodableInputs)
		{
			std::string inputCopy = input;
			success = Path::decode(inputCopy);
			CIO_ASSERT(!success);

			// ensure we didn't modify the input on failure
			CIO_ASSERT_EQUAL(input, inputCopy);
			CIO_ASSERT_EQUAL(input.size(), inputCopy.size());
		}
	}
}