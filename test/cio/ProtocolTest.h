/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_PROTOCOLTEST_H
#define CIO_PROTOCOLTEST_H

#include <cio/test/Suite.h>

#include <set>

namespace cio
{
	/**
	 * Conducts a unit test of the base cio::Protocol class.
	 * This class is effectively a mock empty directory, so we confirm the base methods work as advertised.
	 */
	class ProtocolTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructor.
			 */
			ProtocolTest();

			/**
			 * Destructor.
			 */
			virtual ~ProtocolTest() noexcept override;

			/**
			 * Tests that Directory::open() always returns Underflow whether the input is empty or not.
			 */
			void openTest();

			/**
			 * Tests that Directory::filename() always returns the empty string.
			 */
			void filenameTest();

			/**
			 * Tests that Directory::current() always returns default metadata.
			 */
			void currentTest();

			/**
			 * Tests that Directory::next() always fails with reason Underflow.
			 */
			void nextTest();

			/**
			 * Tests that Directory::openCurrentInput() returns the nullptr.
			 */
			void openCurrentInputTest();
			
			/**
			 * Tests that Directory::openInput() returns the nullptr.
			 */
			void openInputTest();

			/**
			 * Tests that Directory::valid() always returns false.
			 */
			void traversableTest();

			/**
			 * Tests that Directory::checkForSpecialDirectories() considers '.' and '..' special directories
			 * and various other inputs are not considered special.
			 */
			void checkForSpecialDirectoriesTest();

			/**
			 * Tests that Directory::checkForSpecial() considers '.' and '..' special directories
			 * and various other inputs are not considered special.
			 */
			void checkForSpecialFilenameTest();

			/**
			 * Tests that Directory::special() considers '.' and '..' special directories
			 * and various other inputs are not considered special.
			 * This version tests all overloads to make sure they compile and get the same answers.
			 */
			void specialTest();
			
			/**
			 * Tests that Directory::clear() can be called without crashing.
			 */
			void clearTest();
	};
}

#endif

