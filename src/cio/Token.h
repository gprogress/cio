/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TOKEN_H
#define CIO_TOKEN_H

#include "Types.h"

#include "Exception.h"
#include "Parse.h"
#include "Type.h"
#include "Source.h"
#include "Text.h"
#include "Validation.h"

#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Token class represents a chunk of significant text being a token.  
	 * 
	 */
	class CIO_API Token
	{
		public:
			/**
			* Construct a new empty token.
			*/
			Token() noexcept;

			/**
			* Construct a new token with the given source and text.
			* 			 
			* @param src The input source
			* @param txt The text of the token
			*/
			Token(const Source& src, const Text &txt) noexcept;

			/**
			 * Construct a copy of a token.
			 *
			 * @param in The token to copy
			 */
			Token(const Token &in) noexcept;

			/**
			* Move token underlying data into a new instance of this class.
			*
			* @param in The device to move
			*/
			Token(Token &&in) noexcept;

			/**
			 * Copy a token.
			 *
			 * @param in The token to copy
			 * @return this token
			 */
			Token &operator=(const Token &in);

			/**
			* Move token underlying data into this object.
			*
			* @param in The token to move
			* @return this token
			*/
			Token &operator=(Token &&in) noexcept;

			/**
			* Destructor.
			*/
			inline ~Token() noexcept = default;
			
			/**
			 * Gets the underlying input for the token.
			 *
			 * @return the underlying input used to get the data
			 */
			inline const Text& text() const noexcept;

			/**
			 * Sets the underlying input for the token.
			 *
			 * @param txt the underlying text
			 */
			void setText(const Text& txt) noexcept;

			/**
			 * References the given text, creates a Text parameter using a null allocator.
			 *
			 * @param text The character string being referenced
			 * @param len The length of the character string
			 */
			void referenceText(const char* text, std::size_t len) noexcept;

			/**
			 * Gets the detected value type from parsing.
			 *
			 * @return The detected parse type
			 */
			Type type() const noexcept;

			/**
			 * Parses the token to get the given type
			 * 
			 * @tparam T The desired type
			 * @throw cio::Exception If the conversion failed
			 * @return the templated type result
			 */
			template <typename T> 
			inline T to() const;

			/**
			 * Parses the token to get the given type
			 *
			 * @tparam T The desired type
			 * @return the templated type parse result
			 */
			template <typename T>
			inline Parse<T> decode() const noexcept;

			/**
			 * Input source line number, column number, and file name reference
			 *
			 * @return the source
			 */
			inline Source source() const noexcept;

			/**
			 * Sets the input source
			 *
			 * @param src the source to set
			 */
			void setSource(const Source& src) noexcept;

			/**
			 * Evaluate whether the token is complete
			 *
			 * @return the boolean that determines if the token is complete
			 */
			inline bool partial() const noexcept;

			/**
			 * Sets whether the token is complete
			 *
			 * @param part the boolean that determines if the token is complete
			 */
			inline void setPartial(bool part) noexcept;

			/**
			 * The number of text characters in token
			 *
			 * @return the number of text characters in the token
			 */
			inline std::size_t size() const noexcept;

			/**
			 * Evaluate whether the token is empty or not
			 *
			 * @return whether the token has any whitespace or characters
			 */
			inline bool empty() const noexcept;

			/**
			 * Evaluate whether the token is whitespace or text
			 *
			 * @return whether the token is made up of whitespace
			 */
			inline bool isWhitespace() const noexcept;

			/**
			 * Sets whether the token is whitespace or text
			 *
			 * @param whitespace true if the token is whitespace
			 */
			inline void setWhitespace(bool whitespace) noexcept;

			/**
			 * Determine if the token itself exists
			 *
			 * @return whether token exists
			 */
			inline explicit operator bool() const noexcept;

		private:
			/** The Text for this class */
			Text mText;

			/** The input source for this class*/
			Source mSource;

			/** Whether the token is partially complete*/
			bool mPartial;

			/** Whether the token is a whitespace token*/
			bool mIsWhitespace;
	};
}

/* Inline implementation */

namespace cio
{
	inline const Text& Token::text() const noexcept
	{
		return mText;
	}

	inline Source Token::source() const noexcept
	{
		return mSource;
	}

	inline bool Token::partial() const noexcept
	{
		return mPartial;
	}

	inline void Token::setPartial(bool part) noexcept
	{
		mPartial = part;
	}

	inline std::size_t Token::size() const noexcept
	{
		return mText.size();
	}

	inline bool Token::empty() const noexcept
	{
		return mText.empty();
	}

	inline Token::operator bool() const noexcept
	{
		return !mText.empty();

	}

	inline bool Token::isWhitespace() const noexcept
	{
		return mIsWhitespace;
	}

	inline void Token::setWhitespace(bool whitespace) noexcept
	{
		mIsWhitespace = whitespace;
	}

	template <typename T>
	inline T Token::to() const
	{
		Parse<T> p;
		parseType(mText, p);
		if (failed(p.status))
		{
			throw cio::Exception();
		}
		return p.value;
	}

	template <typename T>
	inline Parse<T> Token::decode() const noexcept
	{
		Parse<T> p;
		parseType(mText, p);
		return p;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
