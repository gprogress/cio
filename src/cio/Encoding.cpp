/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Encoding.h"

#include "Primitive.h"
#include "Type.h"

#include <climits>
#include <sstream>

namespace cio
{
	Encoding Encoding::createNormalized(std::size_t mantissa) noexcept
	{
		return Encoding(mantissa, 0, -static_cast<int>(mantissa), Signedness::None, Normalized::Yes);
	}

	Encoding Encoding::createSignedNormalized(std::size_t mantissa) noexcept
	{
		return Encoding(mantissa, 0, -static_cast<int>(mantissa), Signedness::Negation, Normalized::Yes);
	}

	std::int32_t Encoding::calculateULPDistanceFromZero(float value) noexcept
	{
		std::int32_t distFromZero;
		if (value == 0.0f)
		{
			distFromZero = 0;
		}
		else
		{
			std::int32_t integerRepresentation;
			std::memcpy(&integerRepresentation, &value, 4);

			if (value < 0.0f)
			{
				distFromZero = INT_MIN - integerRepresentation;
			}
			else
			{
				distFromZero = integerRepresentation;
			}
		}
		return distFromZero;
	}

	std::int64_t Encoding::calculateULPDistanceFromZero(double value) noexcept
	{
		std::int64_t distFromZero;
		if (value == 0.0)
		{
			distFromZero = 0;
		}
		else
		{
			std::int64_t integerRepresentation;
			std::memcpy(&integerRepresentation, &value, 8);

			if (value < 0.0)
			{
				distFromZero = LLONG_MIN - integerRepresentation;
			}
			else
			{
				distFromZero = integerRepresentation;
			}
		}
		return distFromZero;
	}


	std::int32_t Encoding::calculateDifferenceULP(float a, float b) noexcept
	{
		std::int32_t distAFromZero = calculateULPDistanceFromZero(a);
		std::int32_t distBFromZero = calculateULPDistanceFromZero(b);
		return distAFromZero - distBFromZero;
	}

	std::int64_t Encoding::calculateDifferenceULP(double a, double b) noexcept
	{
		std::int64_t distAFromZero = calculateULPDistanceFromZero(a);
		std::int64_t distBFromZero = calculateULPDistanceFromZero(b);
		return distAFromZero - distBFromZero;
	}

	Encoding::Encoding(std::size_t mantissa) noexcept :
		mantissa(static_cast<std::uint16_t>(mantissa))
	{
		// default values handle everything else
	}

	Encoding::Encoding(std::size_t mantissa, Signedness signedness) noexcept :
		mantissa(static_cast<std::uint16_t>(mantissa)),
		signedness(signedness)
	{
		// default values handle everything else
	}

	Encoding::Encoding(std::size_t mantissa, unsigned exponent, int bias) noexcept :
		mantissa(static_cast<std::uint16_t>(mantissa)),
		bias(bias),
		exponent(exponent)
	{
		// default values handle everything else
	}

	Encoding::Encoding(std::size_t mantissa, unsigned exponent, int bias, Signedness signedness, Normalized normalized, Domain domain) noexcept :
		mantissa(static_cast<std::uint16_t>(mantissa)),
		bias(bias),
		exponent(exponent),
		signedness(signedness),
		normalized(normalized),
		domain(domain)
	{
		// nothing more to do
	}

	Encoding::Encoding(std::size_t mantissa, unsigned exponent, int bias, Signedness signedness, bool normalized, bool nan, bool infinity) noexcept :
		mantissa(static_cast<std::uint16_t>(mantissa)),
		bias(bias),
		exponent(exponent),
		signedness(signedness),
		normalized(static_cast<Normalized>(static_cast<unsigned>(normalized) | (static_cast<unsigned>(nan) << 1) | (static_cast<unsigned>(infinity) << 2)))
	{
		// nothing more to do
	}

	Encoding::Encoding(Type prim) noexcept
	{
		this->setEncoding(prim);
	}
	
	Encoding::Encoding(Primitive type, Domain domain) noexcept
	{
		this->setEncoding(type, domain);
	}
	
	Encoding::Encoding(Type prim, Primitive type) noexcept
	{
		this->setEncoding(prim, type);
	}
	
	void Encoding::setEncoding(Type prim, Domain domain) noexcept
	{
		this->setEncoding(prim, primitive(prim), domain);
	}
	
	void Encoding::setEncoding(Type prim, Primitive type, Domain domain) noexcept
	{
		if (type >= Primitive::Default)
		{
			this->setEncoding(primitive(prim), domain);
		}
		else
		{
			this->setEncoding(type, domain);
		}
	}
	
	void Encoding::setEncoding(Primitive type, Domain domain) noexcept
	{
		this->clear();
	
		// special values are not supported by this method
		if (type > Primitive::None && type < Primitive::Default)
		{
			// Use prefix byte to decide general category
			unsigned prefix = static_cast<unsigned>(type) & 0xF0;
			unsigned bitlog = static_cast<unsigned>(type) & 0x0F;
			
			if (bitlog > 0)
			{
				switch (prefix)
				{
					case 0x10: // signed integer
						this->mantissa = (1 << (bitlog - 1u));
						this->signedness = Signedness::Negation;
						break;
						
					case 0x20: // floating point
					{
						this->signedness = Signedness::Prefix;
						this->normalized = Normalized::YesWithInfinityNaN;
					
						switch (bitlog)
						{
							case 0x04: // "quarter" mini-float 
							{
								this->mantissa = 3;
								this->exponent = 4;
								this->bias = -2;
								break;
							}
							
							case 0x05: // half minifloat
							{
								this->mantissa = 10;
								this->exponent = 5;
								this->bias = -15;
								break;
							}
							
							case 0x06: // single float
							{
								this->mantissa = 23;
								this->exponent = 8;
								this->bias = -127;
								break;
							}
							
							case 0x07: // double float
							{
								this->mantissa = 52;
								this->exponent = 11;
								this->bias = -1023; 
								break;
							}
							
							case 0x08: // quad float
							{
								this->mantissa = 112;
								this->exponent = 15;
								this->bias = -16383;
								break;
							}
							
							default: // unsupported type
							{
								// nothign to do
								break;
							}
						}
						break;
					}
					
					// All other types shared std::size_t mantissa-only layout
					default:
					{
						this->mantissa = (1 << (bitlog - 1u));
						break;
					}
				}
			}
		}

		this->domain = domain;
	}

	bool Encoding::isSigned() const noexcept
	{
		return this->signedness != Signedness::None;
	}

	void Encoding::setSigned(bool value) noexcept
	{
		if (value)
		{
			if (this->signedness == Signedness::None)
			{
				this->signedness = Signedness::Negation;
			}
		}
		else
		{
			this->signedness = Signedness::None;
		}
	}

	bool Encoding::hasSignBit() const noexcept
	{
		return this->signedness == Signedness::Prefix;
	}

	void Encoding::setSignBit(bool value) noexcept
	{
		if (value)
		{
			this->signedness = Signedness::Prefix;
		}
		else if (this->signedness == Signedness::Prefix)
		{
			this->signedness = Signedness::None;
		}
	}

	bool Encoding::isTwosComplement() const noexcept
	{
		return this->signedness == Signedness::Negation;
	}

	void Encoding::setTwosComplement(bool value) noexcept
	{
		if (value)
		{
			this->signedness = Signedness::Negation;
		}
		else if (this->signedness == Signedness::Negation)
		{
			this->signedness = Signedness::None;
		}
	}

	bool Encoding::isNormalized() const noexcept
	{
		return (static_cast<unsigned>(this->normalized) & 1) != 0;
	}

	void Encoding::setNormalized(bool value) noexcept
	{
		this->normalized = static_cast<Normalized>(static_cast<unsigned>(value) | (static_cast<unsigned>(this->normalized) & 0x6));
	}

	bool Encoding::hasNoData() const noexcept
	{
		return (static_cast<unsigned>(this->normalized) & 2) != 0;
	}

	void Encoding::setNoData(bool enabled) noexcept
	{
		this->normalized = static_cast<Normalized>((static_cast<unsigned>(enabled) << 1) | (static_cast<unsigned>(this->normalized) & 0x5));
	}

	bool Encoding::hasInfinity() const noexcept
	{
		return (static_cast<unsigned>(this->normalized) & 4) != 0;
	}

	void Encoding::setInfinity(bool enabled) noexcept
	{
		this->normalized = static_cast<Normalized>((static_cast<unsigned>(enabled) << 2) | (static_cast<unsigned>(this->normalized) & 0x3));
	}

	std::size_t Encoding::getMantissaBits() const noexcept
	{
		return this->mantissa;
	}

	void Encoding::setMantissaBits(std::size_t value) noexcept
	{
		this->mantissa = static_cast<std::uint16_t>(value);
	}

	unsigned Encoding::getExponentBits() const noexcept
	{
		return this->exponent;
	}

	void Encoding::setExponentBits(unsigned value) noexcept
	{
		this->exponent = static_cast<std::uint8_t>(value);
	}

	int Encoding::getExponentBias() const noexcept
	{
		return static_cast<int>(this->bias);
	}

	void Encoding::setExponentBias(int bias) noexcept
	{
		this->bias = static_cast<std::int16_t>(bias);
	}

	void Encoding::setFloatingPoint(std::size_t mantissa, unsigned exponent, int bias) noexcept
	{
		this->mantissa = static_cast<std::uint16_t>(mantissa);
		this->exponent = static_cast<std::uint8_t>(exponent);
		this->bias = static_cast<std::int16_t>(bias);
		this->signedness = Signedness::Prefix;
		this->normalized = Normalized::YesWithInfinityNaN;
	}

	bool Encoding::isFloatingPoint() const noexcept
	{
		return this->mantissa > 0 && this->exponent > 0;
	}

	void Encoding::unpack(std::uint64_t packed) noexcept
	{
		std::memcpy(this, &packed, 8u);
	}

	std::uint64_t Encoding::pack() const noexcept
	{
		std::uint64_t value = 0;
		std::memcpy(&value, this, 8u);
		return value;
	}

	std::size_t Encoding::computeRequiredBits() const noexcept
	{
		unsigned elements = this->hasReal() + this->hasImaginary();
		std::size_t bits = static_cast<std::size_t>(this->mantissa) + static_cast<std::size_t>(this->exponent) + static_cast<std::size_t>(this->signedness == Signedness::Prefix);
		return bits * elements;
	}

	std::size_t Encoding::computeRequiredBits(std::size_t count) const noexcept
	{
		return this->computeRequiredBits() * count;
	}

	std::size_t Encoding::computeRequiredBytes() const noexcept
	{
		return (this->computeRequiredBits() + 7u) / 8u;
	}

	std::size_t Encoding::computeRequiredBytes(std::size_t count) const noexcept
	{
		return (this->computeRequiredBits(count) + 7u) / 8u;
	}

	void Encoding::clear() noexcept
	{
		this->mantissa = 0;
		this->exponent = 0;
		this->bias = 0;
		this->signedness = Signedness::None;
		this->normalized = Normalized::No;
		this->domain = Domain::Real;
	}

	bool Encoding::hasReal() const noexcept
	{
		return (static_cast<unsigned>(this->domain) & 1) != 0;
	}

	bool Encoding::hasImaginary() const noexcept
	{
		return (static_cast<unsigned>(this->domain) & 2) != 0;
	}

	void Encoding::setReal(bool enabled) noexcept
	{
		this->domain = static_cast<Domain>(static_cast<unsigned>(this->domain) & 2 | static_cast<unsigned>(enabled));
	}

	void Encoding::setImaginary(bool enabled) noexcept
	{
		this->domain = static_cast<Domain>(static_cast<unsigned>(this->domain) & 1 | (static_cast<unsigned>(enabled) << 1));
	}

	bool Encoding::isUnsignedInteger() const noexcept
	{
		return this->mantissa > 0 && this->exponent == 0 && this->bias == 0 && this->signedness == Signedness::None && this->normalized == Normalized::No;
	}

	std::size_t Encoding::isStandardInteger() const noexcept
	{
		std::size_t bytes = 0u;

		if (this->mantissa > 0 && this->exponent == 0 && this->bias == 0 && this->normalized == Normalized::No)
		{
			std::size_t bits = this->getMantissaBits();
			bool matches = (bits == 8u || bits == 16u || bits == 32u ||bits == 64u);
			if (matches && this->signedness == Signedness::None || this->signedness == Signedness::Negation)
			{
				bytes = bits / 8u;
			}
		}

		return bytes;
	}

	std::size_t Encoding::isStandardUnsigned() const noexcept
	{
		std::size_t bytes = 0u;
		if (this->signedness == Signedness::None)
		{
			bytes = this->isStandardInteger();
		}
		return bytes;
	}

	std::size_t Encoding::isStandardSigned() const noexcept
	{
		std::size_t bytes = 0u;
		if (this->signedness == Signedness::Negation)
		{
			bytes = this->isStandardInteger();
		}
		return bytes;
	}

	std::size_t Encoding::isStandardFloat() const noexcept
	{
		return this->is<float>() ? 4 : (this->is<double>() ? 8 : 0);
	}

	bool Encoding::isCopyableTo(const Encoding &encoding) const noexcept
	{
		return this->mantissa == encoding.mantissa &&
			this->exponent == encoding.exponent &&
			this->bias == encoding.bias &&
			this->normalized == encoding.normalized &&
			(this->signedness == Signedness::Prefix) == (encoding.signedness == Signedness::Prefix);
	}
	
	std::pair<std::size_t, Signedness> Encoding::isPrimitive() const noexcept
	{
		std::pair<std::size_t, Signedness> result(0, Signedness::None);
		std::size_t floatBytes = this->isStandardFloat();

		if (floatBytes > 0)
		{
			result.first = floatBytes;
			result.second = Signedness::Prefix;
		}
		else
		{
			std::size_t intBytes = this->isStandardInteger();
			if (intBytes > 0)
			{
				result.first = intBytes;
				result.second = this->signedness;
			}
		}

		return result;
	}
	
	std::string print(Encoding layout)
	{
		std::ostringstream s;
		s << layout;
		return s.str();
	}

	Encoding::operator bool() const noexcept
	{
		return this->mantissa || this->exponent;
	}

	bool operator==(Encoding left, Encoding right) noexcept
	{
		return left.pack() == right.pack();
	}

	bool operator!=(Encoding left, Encoding right) noexcept
	{
		return left.pack() != right.pack();
	}

	bool operator<(Encoding left, Encoding right) noexcept
	{
		return left.pack() < right.pack();
	}

	bool operator<=(Encoding left, Encoding right) noexcept
	{
		return left.pack() <= right.pack();
	}

	bool operator>(Encoding left, Encoding right) noexcept
	{
		return left.pack() > right.pack();
	}

	bool operator>=(Encoding left, Encoding right) noexcept
	{
		return left.pack() >= right.pack();
	}

	std::ostream &operator<<(std::ostream &s, Encoding layout)
	{
		if (layout.isSigned())
		{
			if (layout.hasSignBit())
			{
				s << "S";
			}
			else
			{
				s << "+-";
			}
		}

		if (layout.isNormalized())
		{
			s << "1.";
		}

		s << layout.getMantissaBits() << "X";

		if (layout.getExponentBits() > 0 || layout.getExponentBias() != 0)
		{
			s << "*2^(";

			if (layout.getExponentBits() > 0)
			{
				s << layout.getExponentBits() << "Y";
			}

			if (layout.getExponentBias() > 0)
			{
				s << "+" << layout.getExponentBias();
			}
			else if (layout.getExponentBias() < 0)
			{
				s << layout.getExponentBias();
			}

			s << ")";
		}

		if (layout.hasInfinity())
		{
			s << "[inf]";
		}

		if (layout.hasNoData())
		{
			s << "[nan]";
		}

		return s;
	}
}
