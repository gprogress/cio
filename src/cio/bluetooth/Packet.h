/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_PACKET_H
#define CIO_BLUETOOTH_PACKET_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Packet enumeration describes the overall type of controller packets that may be sent or received.
		 */
		enum class Packet : std::uint8_t
		{
			/** No packet type was set */
			None = 0x00,
			
			/** The packet is a command to the controller */
			Command = 0x01,
			
			/* The packet is asynchronous data read or written */
			AsynchronousData = 0x02,
			
			/** The packet is synchronous data read or written (classic only, not for low energy) */
			SynchronousData = 0x03,
			
			/** The packet is an event read from the controller */
			Event = 0x04,
			
			/** The packet is an extended command to the controller */
			ExtendedCommand = 0x09
		};

		/**
		 * Gets a human readable text representation of an HCI Packet type.
		 *
		 * @param type The HCI packet type
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(Packet type) noexcept;

		/**
		 * Prints a human readable text representation of an HCI Packet type to a C++ stream.
		 *
		 * @param s The stream
		 * @param type The HCI Packet type
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, Packet type);
	}
}

#endif

