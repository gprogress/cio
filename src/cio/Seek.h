/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_SEEK_H
#define CIO_SEEK_H

#include "Types.h"

#include "Length.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * Enumeration of seek directions relative to current position.
	 */
	enum class Seek : std::uint8_t
	{
		/** Seek is disabled */
		None,

		/** Seek is absolute position from the beginning of a resource */
		Begin,

		/** Seek is relative position to current position, if known */
		Current,

		/** Seek is relative position to the resource size, if known */
		End,

		/** Seek is unknown */
		Unknown
	};
	
	CIO_API Length position(Length offset, Seek mode, Length current, Length size);

	CIO_API Length position(Length offset, Seek mode, std::size_t current, std::size_t size);
	
	/**
	 * Gets a text representation of the Seek enumeration.
	 *
	 * @param mode The seek type
	 * @return the text for the seek type
	 */
	CIO_API const char *print(Seek mode) noexcept;

	/**
	 * Parses a null-terminated C string to get the matching Seek type.
	 * If the text does not match any known type, Seek::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed seek type
	 */
	CIO_API Seek parseSeek(const char *text) noexcept;

	/**
	 * Parses a text string to get the matching seek type.
	 * If the text does not match any known seek type, Seek::Unknown is returned.
	 *
	 * @param text The text to parse
	 * @return the parsed seek type
	 */
	CIO_API Seek parseSeek(const std::string &text) noexcept;

	/**
	 * Prints the text for Seek type to a C++ stream.
	 *
	 * @param stream The C++ stream
	 * @param mode The seek type to print
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, Seek mode);

	/**
	 * Parses text from a C++ stream to obtain the matching Seek type.
	 * If the text does not match any known seek type, Seek::None is set.
	 *
	 * @param stream The C++ stream
	 * @param mode The reference to the Seek type to store the parse results into
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::istream &operator>>(std::istream &stream, Seek &mode);
}

#endif
