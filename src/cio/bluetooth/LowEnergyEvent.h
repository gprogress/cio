/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_LOWENERGYEVENT_H
#define CIO_BLUETOOTH_LOWENERGYEVENT_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Low Energy Event enumeration represents the standard codes for the sub-event types
		 * for Bluetooth Low Energy events.
		 */
		enum class LowEnergyEvent : std::uint8_t
		{
			/** No event was set */
			None = 0x00,
		
			/** Low energy connection is established */
			Connected = 0x01,
			
			/** An advertising report was received */
			Advertisement = 0x02,
			
			/** Low energy connection parameters were updated */
			ConnectionUpdated = 0x03,
			
			/** The request to read the remote used features completed */
			RemoteFeaturesRead = 0x04,
			
			/** A long term key was requested */
			LongTermKeyRequested = 0x05,
			
			/** A remote connection parameter was requested */
			ParameterRequested = 0x06,
			
			/** The data length changed */
			DataLengthChanged = 0x07,
			
			/** The local P256 public key read request was completed */
			LocalPublicKeyRead = 0x08,
			
			/** The DHKey generation request was completed */
			DHKeyGenerated = 0x09,
			
			/** The enhanced connection request was completed */
			EnhancedConnection = 0x0A,
			
			/** The direct advertisement report was received */
			DirectAdvertisement = 0x0B
		};
		
		/**
		 * Gets a human readable text representation of an HCI Low Energy Event subcode.
		 *
		 * @param type The HCI Low Energy event subcode
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(LowEnergyEvent type) noexcept;

		/**
		 * Prints a human readable text representation of an HCI Low Energy Event subcode to a C++ stream.
		 *
		 * @param s The stream
		 * @param type The HCI Low Energy Event subcode
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, LowEnergyEvent type);
	}
}

#endif

