/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Type.h"

#include "Primitive.h"
#include "Print.h"
#include "ParseStatus.h"
#include "Text.h"

#include <ostream>

namespace cio
{
	struct PrimitiveRecord
	{
		/** Type type */
		Type type;
		
		/** Label */
		const char *label;
		
		/** Default component type */
		Primitive component;
	};
	
	const PrimitiveRecord sPrimitiveRecords[] =
	{
		{ Type::None, 	"None", 	Primitive::None },
		{ Type::Boolean, 	"Boolean", 	Primitive::Boolean },
		{ Type::Integer, 	"Integer", 	Primitive::Signed32},
		{ Type::Unsigned, 	"Unsigned", 	Primitive::Unsigned32 },
		{ Type::Real, 	"Real", 	Primitive::Float32 },
		{ Type::Enum, 	"Enum", 	Primitive::Signed32 },
		{ Type::Text, 	"Text", 	Primitive::Char8 }, 
		{ Type::Blob, 	"Blob", 	Primitive::Byte },
		{ Type::Object, 	"Object", 		Primitive::Variant },
		{ Type::Any, 	"Any", 		Primitive::Variant }
	};
	
	Primitive primitive(Type prim) noexcept
	{
		Primitive type = Primitive::None;
		
		if (prim <= Type::Any)
		{
			type = sPrimitiveRecords[static_cast<std::size_t>(prim)].component;
		}
		
		return type;
	}
	
	const char *print(Type prim) noexcept
	{
		const char *text = "Unknown";
		if (prim <= Type::Any)
		{
			text = sPrimitiveRecords[static_cast<std::size_t>(prim)].label;
		}
	
		return text;
	}

	std::size_t print(Type prim, char *buffer, std::size_t capacity) noexcept
	{
		return print(print(prim), buffer, capacity);
	}

	TextParseStatus parse(const cio::Text &text, Type &prim) noexcept
	{
		TextParseStatus status;

		for (std::size_t i = 0; i < sizeof(sPrimitiveRecords) / sizeof(PrimitiveRecord); ++i)
		{
			if (text.equalInsensitive(sPrimitiveRecords[i].label))
			{
				prim = sPrimitiveRecords[i].type;
				status.status = Validation::Exact;
				status.parsed = text.size();
			}
		}

		if (!status.valid())
		{
			status.status = Validation::InvalidValue;
			prim = Type::None;
		}

		return status;
	}

	std::ostream &operator<<(std::ostream &s, Type prim)
	{
		return s << print(prim);
	}
}
