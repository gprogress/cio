/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LOCKABLE_H
#define CIO_LOCKABLE_H

#include <mutex>

namespace cio
{
	/**
	 * The Lockable mix-in base class provides a mutex lock intended to be used by a derived class
	 * for synchronization of its members. It allows it or any of its derived classes to be used
	 * as the template paramter for a std::unique_lock, std::lock_guard, and so on.
	 * 
	 * Unlike the std::mutex used to implement it, the Lockable is both copyable and movable, but both operations
	 * do nothing since the mutex itself is neither. This enables derived classes to assume default copy and move
	 * semantics if the mutex would have otherwise been the only non-copyable or non-movable member.
	 * 
	 * Also unlike std::mutex, the lock methods are const since locking is assumed to be transient state
	 * rather than an actual object property.
	 */
	class Lockable
	{
		public:
			/**
			 * Construct a lockable in the unlocked state.
			 */
			inline Lockable() noexcept;
			
			/**
			 * Copy constructor. Does nothing since the mutex is not copyable.
			 * 
			 * @param in The lockable to copy
			 */
			inline Lockable(const Lockable &in) noexcept;

			/**
			 * Move constructor. Does nothing since the mutex is not movable.
			 *
			 * @param in The lockable to move
			 */
			inline Lockable(Lockable &&in) noexcept;

			/**
			 * Copy assignment. Does nothing since the mutex is not copyable.
			 *
			 * @param in The lockable to copy
			 * @return this lockable
			 */
			inline Lockable &operator=(const Lockable &in) noexcept;

			/**
			 * Move assignment. Does nothing since the mutex is not movable.
			 *
			 * @param in The lockable to move
			 * @return this lockable
			 */
			inline Lockable &operator=(Lockable &&in) noexcept;

			/**
			 * Destructor.
			 */
			inline ~Lockable() noexcept = default;

			/**
			 * Locks the underlying mutex, waiting indefinitely until this succeeds.
			 * 
			 * @throw std::system_error If the underlying mutex could not be locked
			 */
			inline void lock() const;

			/**
			 * Unlocks the underlying mutex.
			 * 
			 * @throw std::system_error If the underlying mutex could not be locked
			 */
			inline void unlock() const;

			/**
			 * Locks the underlying mutex, returning immediately with either success or failure.
			 * 
			 * @return whether the mutex was locked by this call
			 * @throw std::system_error If the underlying mutex could not be locked due to any reason other than
			 * it being currently locked
			 */
			inline bool try_lock() const;

			/**
			 * Gets the underlying mutex for direct use with (for example) condition variables.
			 * 
			 * @return the underlying mutex
			 */
			inline std::mutex &mutex() const noexcept;

			/**
			 * Implicitly cast the lockable to a non-const mutex so it can be used with std::unique_lock
			 * and other types.
			 *
			 * @return the underlying mutex
			 */
			inline operator std::mutex &() const noexcept;

		private:
			/** The mutex implementing the lock state */
			mutable std::mutex mLockableMutex;
	};
}

/* Inline implementation */

namespace cio
{
	inline Lockable::Lockable() noexcept
	{
		// nothing more to do
	}

	inline Lockable::Lockable(const Lockable &in) noexcept
	{
		// nothing more to do
	}

	inline Lockable::Lockable(Lockable &&in) noexcept
	{
		// nothing more to do
	}

	inline Lockable &Lockable::operator=(const Lockable &in) noexcept
	{
		// nothing more to do
		return *this;
	}

	inline Lockable &Lockable::operator=(Lockable &&in) noexcept
	{
		// nothing more to do
		return *this;
	}

	inline void Lockable::lock() const
	{
		mLockableMutex.lock();
	}

	inline void Lockable::unlock() const
	{
		mLockableMutex.unlock();
	}

	inline bool Lockable::try_lock() const
	{
		return mLockableMutex.try_lock();
	}

	inline std::mutex &Lockable::mutex() const noexcept
	{
		return mLockableMutex;
	}

	inline Lockable::operator std::mutex &() const noexcept
	{
		return mLockableMutex;
	}
}

#endif