/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_EVENT_H
#define CIO_XML_EVENT_H

#include "Types.h"

#include <cio/Text.h>

#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		class CIO_XML_API Event
		{
			public:
				Event();

				Event(Type dataType, EventType eventType);

				Event(Text value, Type dataType, EventType eventType);

				Event(std::string value, Type dataType, EventType eventType);

				Event(const Event &in);

				Event(Event &&in) noexcept;

				Event &operator=(const Event &in);

				Event &operator=(Event &&in) noexcept;

				~Event();

				inline const std::string &getValue() const;

				std::string takeValue();

				void setValue(std::string value);

				void appendValue(Text value);

				inline Type getDataType() const;

				inline void setDataType(Type type);

				inline EventType getEventType() const;

				inline void setEventType(EventType type);

				inline bool matches(Type type, EventType etype) const;

				inline explicit operator bool() const;

			private:
				std::string mValue;
				Type mType;
				EventType mEventType;
		};
	}
}

/* Inline implementation */

namespace cio
{
	namespace xml
	{
		inline const std::string &Event::getValue() const
		{
			return mValue;
		}

		inline Type Event::getDataType() const
		{
			return mType;
		}

		inline void Event::setDataType(Type type)
		{
			mType = type;
		}

		inline EventType Event::getEventType() const
		{
			return mEventType;
		}

		inline void Event::setEventType(EventType type)
		{
			mEventType = type;
		}

		inline bool Event::matches(Type type, EventType etype) const
		{
			return mType == type && mEventType == etype;
		}

		inline Event::operator bool() const
		{
			return mType != Type::None;
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
