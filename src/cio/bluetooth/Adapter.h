/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ADAPTER_H
#define CIO_BLUETOOTH_ADAPTER_H

#include "Types.h"

#include "Controller.h"

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Adapter provides a user-friendly implementation of the interface for a local bluetooth adapter.
		 * It implements the core Controller interface by internally allocating and using the best available platform specific technology.
		 *
		 * On Linux the D-Bus Controller is preferred, with a fallback to the HCI Controller if D-Bus is not available.
		 * Currently other platforms are not yet supported.
		 */
		class CIO_BLUETOOTH_API Adapter final : public Controller
		{
			public:							
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Gets the metaclass for the best subclass of Controller for the current platform.
				 * On Linux this will return DBusController if available, falling back to HCIController.
				 * On all other platforms this currently will use HCIController.
				 *
				 * @return the metaclass for the best controller class for this platform
				 */
				static const Metaclass *getBestControllerType() noexcept;
				
				/**
				 * Allocates a new instance of the best subclass of Controller for the current platform.
				 *
				 * @return the best controller for the current platform
				 * @throw std::bad_alloc If the memory for the controller could not be allocated
				 */
				static std::unique_ptr<Controller> allocateBestController();
				
				/**
				 * Constructs a new instance of the best subclass of Controller for the current platform
				 * onto the given memory chunk.
				 *
				 * @param buffer The memory chunk
				 * @param capacity The number of bytes available
				 * @return the constructed best controller for the current platform
				 */
				static Controller *constructBestController(void *buffer, std::size_t capacity);

				/**
				 * Construct an unconnected Adapter.
				 */
				Adapter() noexcept;
				
				/**
				 * Construct a Controller and immediately connect it to the given local device.
				 *
				 * @param device The device
				 */
				explicit Adapter(const Device &device);
				
				/**
				 * Constructs a Bluetooth Low Energy Client by adopting the state of a given client.
				 * 
				 * @param in The controller to move
				 */
				Adapter(Adapter &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Controller.
				 *
				 * @param in The controller to move
				 * @return this controller
				 */
				Adapter &operator=(Adapter &&in) noexcept;
				
				/**
				 * Destructor. Disconnects the socket.
				 */
				virtual ~Adapter() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the adapter, deallocating the controller.
				 */
				virtual void clear() noexcept override;

				/**
				 * Performs validation checks to determine if the Bluetooth controller is usable and if not, attempt to determine why.
				 * The returned Exception object contains the success state, and if marked as a failure, a general reason why and a specific error message.
				 *
				 * The Adapter implementation will instantiate the best matching controller for the current platform, then ask it to perform the same validation.
				 *
				 * @return an exception object indicating whether Bluetooth support is usable and, if not, why not
				 */
				virtual Exception validate() override;
				
				/**
				 * Gets the current controller used to implement this adapter.
				 * If none was set, allocates a new one using the best available technology for the current platform.
				 *
				 * @return the controller
				 * @throw std::bad_alloc If the memory for the controller could not be allocated
				 */
				std::shared_ptr<Controller> getOrCreateController();
				
				/**
				 * Gets the current controller used to implement this adapter.
				 *
				 * @return the controller
				 */
				std::shared_ptr<Controller> getController() noexcept;
				
				/**
				 * Gets the current controller used to implement this adapter.
				 *
				 * @return the controller
				 */
				std::shared_ptr<const Controller> getController() const noexcept;
				
				/**
				 * Manually overrides this adapter to use the given controller.
				 * This can be used to prefer a different controller technology than the adapter would recommend.
				 *
				 * @param controller The controller
				 */
				void setController(std::shared_ptr<Controller> controller) noexcept;
				
				/**
				 * Ensures that this adapter has a controller and opens it to the currently associated device.
				 */
				virtual void openCurrentDevice() override;
				
				/**
				 * Checks to see if this adapter has a controller and if so whether it is open.
				 *
				 * @return whether the controller exists and is open
				 */
				virtual bool isOpen() const noexcept override;
				
				/**
				 * Closes the currently open controller, if any.
				 * The controller is not deallocated.
				 */
				virtual void close() noexcept override;
				
				// Device discovery
				
				/**
				 * Discovers the default device for the system.
				 * Typically this is device index 0 but it's entirely up to the native system platform.
				 * This method may be used even if the controller was not opened to a specific device.
				 * This delegates to the currently set controller.
				 *
				 * @return the default device, or none if not found
				 */
				virtual Device findDefaultDevice() override;
							
				/**
				 * Scans the local system to find all active Bluetooth devices.
				 * This delegates to the currently set controller.
				 *
				 * @return the list of known devices
				 * @throw cio::Exception If there was a failure in setting up the HCI connection to scan for local devices
				 * @throw std::bad_alloc If the memory for the list could not be allocated
				 */
				virtual std::vector<Device> discoverLocalDevices() override;
				
				/**
				 * Reads the local address from the currently open device.
				 * This returns the empty address if no device is open.
				 * This delegates to the currently set controller.
				 *
				 * @return the local device address
				 * @throw cio::Exception If the local device is open but a low-level hardware error occurred reading the address
				 */
				virtual std::size_t readLocalName(char *text, std::size_t length) override;
				
				/**
				 * Reads the local device name from the currently open device.
				 * This prints the empty string if no device is open.
				 * This delegates to the currently set controller.
				 *
				 * @param text The buffer to store the device name into
				 * @param length the maximum number of text bytes available to store
				 * @return the total number of bytes needed for the name (which may be more or less than the buffer)
				 */
				virtual DeviceAddress readLocalAddress() override;
				
				/**
				 * Uses the controller to start the Bluetooth Low Energy discovery process.
				 * This is a mode setting that enables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 * Scanning is inherently asynchronous so use listenForAdvertisements method to actually receive the scan results.
				 *
				 * If scanning is already in progress, this method should do nothing and return State::Exists.
				 *
				 * This delegates to the currently set controller.
				 * 
				 * @return the status which should either be Success if scanning wasstarted or Exists if scanning was already in progress
				 */
				virtual State startLowEnergyScan() override;
				
				/**
				 * Uses the controller to recieve advertisements for peripherals for an execution period.
				 * The receiver callback function is called once for each device advertisement until the execution's timer has elapsed or the execution
				 * has been asynchronously canceled by another thread.
				 *
				 * This delegates to the currently set controller.
				 *
				 * The same peripheral may be discovered more than once if more than advertisement was seen during the execution time period.
				 * The result of this method is the number of advertisements recieved and the overall status, which should be Success if the full
				 * execution time elapsed, Abandoned if the scanning was canceled, or some other error if scanning was either not enabled or a hardware error occurred.
				 *
				 * The final result of the scan will also be stored into the executor itself to mark execution as complete, abandoned, etc.
				 * 
				 * @param listener the advertisement listener to specify task parameters and receive peripherals
				 * @return the result of the scanning in terms of number of advertisements and final status
				 */
				virtual Progress<std::size_t> listenForAdvertisements(AdvertisementListener &listener) noexcept override;
				
				/**
				 * Uses the controller to stop the Bluetooth Low Energy discovery process if it was enabled.
				 * This is a mode setting that disables the scanning mode. Note that some controller implementations may require administrative privileges to do so.
				 * This delegates to the currently set controller.
				 * 
				 * @return the status which should either be Success if scanning was stopped or Missing if scanning was not enabled at all
				 */
				virtual State stopLowEnergyScan() override;
				
				/**
				 * Uses the current device controller to asynchronously establish a connection to the given remote peripheral.
				 * This should preferentially negotiate a low energy connection, but may fall back to a classic connection if necessary.
				 * If such a connection already exists, it should be reused and this method should return State::Connected.
				 *
				 * This method may (and likely will) return before a connection is actually established.
				 * Bluetooth connection methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * Note that it is not an error if the peripheral is not currently visible to the device.
				 * It is not unusual for Bluetooth devices to appear and disappear depending on local conditions.
				 *
				 * @param peripheral The peripheral to connect to
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Connected if the peripheral was already connected, State::Requested if the request was made
				 */ 
				virtual State startConnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Uses the current device controller to asynchronously disconnect a connection to the given remote peripheral.
				 * If the peripheral is known but no such connection already exists, this method should return State::Disconnected.
				 * If the peripheral itself is not known, then this method should return State::Missing.
				 *
				 * This method may (and likely will) return before the connection is actually disconnected.
				 * Bluetooth connection methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * @param peripheral The peripheral to disconnect from
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return State::Missing if no such peripheral is known, State::Disconnected if the peripheral is already disconnected, or State::Requested if the request was made
				 */ 
				virtual State startDisconnection(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Waits for the next connection State from the device relevant to the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the status of the connection (Connected for connected, Disconnected for disconnection, Timeout if this method timed out, error state for failure to connect)
				 */
				virtual State waitForConnectionState(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Requests the current connection state for the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the status of the connection (Connected for connected, Disconnected for exists but not connected, Missing if no such peripheral is known)
				 */
				virtual State requestConnectionState(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Uses the current device controller to asynchronously pair with the given remote peripheral for authentication.
				 * If the current device and peripheral are already paired, this should reuse the pairing and return State::Exists.
				 *
				 * This method may (and likely will) return before the devices are actually paired.
				 * Bluetooth pairing methodology is inherently asynchronous and also extremely slow - on the order of seconds.
				 * The task controller provided for this method is used for timeouts and cancellation for even making the request itself,
				 * since on some platforms the underlying technology may involve blocking requests to a message bus.
				 *
				 * @note This method currently does not provide for advanced agent-based pairing to negotiate input/output capabilities.
				 * Future versions of CIO will enable that.
				 *
				 * @param peripheral The peripheral to pair with
				 * @return State::Success upon completion or State::Exists if the peripheral was already paired
				 * @throw cio::Exception If the process of pairing failed
				 */ 
				virtual State startPairing(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Unpairs the current device with the given peripheral.
				 * If the devices are not paired, this method should return State::Missing but not otherwise fail.
				 *
				 * @param peripheral The peripheral to unpair from
				 * @return State::Success upon completion or State::Missing if the peripheral was not actually paired
				 * @throw cio::Exception If no local device is selected or if the process of unpairing failed
				 */
				virtual State startUnpairing(const Peripheral &peripheral, Task &exec) override;
					
				/**
				 * Waits for the next pairing State from the device relevant to the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the status of the connection (Connected for paired, Disconnected for unpaired, Timeout if this method timed out, error state for failure to pair)
				 */
				virtual State waitForPairingState(const Peripheral &peripheral, Task &exec) override;
				
				/**
				 * Requests the current pairing state for the given peripheral.
				 *
				 * @param peripheral The peripheral
				 * @param exec The task to control this operation's timeout and cancellation
				 * @return the status of the pairing (Connected for paired, Disconnected for exists but not paired, Missing if no such peripheral is known)
				 */
				virtual State requestPairingState(const Peripheral &peripheral, Task &exec) override;
				
			private:
				/** The metaclass for this class */
				static Class<Adapter> sMetaclass;

				/**
				 * Sets up the controller implementation using the best available technology.
				 *
				 * @return the controller
				 */
				Controller &setupImplementation();

				/** The device */
				Device mDevice;
				
				/** The platform-specific controller */
				std::shared_ptr<Controller> mController;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

