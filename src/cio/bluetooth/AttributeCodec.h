/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ATTRIBUTECODEC_H
#define CIO_BLUETOOTH_ATTRIBUTECODEC_H

#include "Types.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Codec class handles the low-level binary serialization and deserialization details of the Bluetooth Low Energy (BLE)
		 * General Attribute (GATT) protocol so that higher-level APIs can focus on logical operations. Due to the very small packet size (MTU) limit
		 * for BLE, many logical operations may end up requiring multiple Codec requests and responses to execute.
		 *
		 * The actual ATT protocol used to implement GATT is simple yet very general and many different kinds of queries can be represented
		 * with the same basic primitives.
		 *
		 * The Codec class assumes that you want to manage your own buffers preallocated to the maximum packet size (by default 23 bytes, though this can be negotiated
		 * higher up to 251 bytes).
		 */
		class CIO_BLUETOOTH_API AttributeCodec
		{
			public:				
				// Error response
													
				/**
				 * Parses the error packet format from the packet's available data into a CIO Exception.
				 * The exception's status is set to the CIO status most closely matching the BLE ATT error code.
				 *
				 * @param packet The packet to process
				 * @throw cio::Exception If the packet itself is malformed for representing an error
				 * @return a cio::Exception representing the error decoded from the packet
				 */
				cio::Exception decodeErrorResponse(Buffer &packet) const;
		
				// Packet size request/response
											
				/**
				 * Encode a request to get the server's maximum packet size (maximum transmission unit aka MTU) and provide
				 * client's maximum packet size for the server's information.
				 *
				 * @note This is not a negotiation, both max packet sizes are constant and must be honored.
				 * The client and server can always choose to send smaller packets than the max though.
				 *
				 * @param packet The packet to use for the request
				 * @param clientMax the client maximum packet size, defaults to the protocol default maximum of 23 bytes 
				 */
				void encodeMaxPacketSizeRequest(Buffer &packet, std::size_t clientMax) const;
				
				/**
				 * Decodes the response to get the server's maximum packet size (maximum transmission unit aka MTU).
				 *
				 * @param packet The packet containing the response
				 * @return the parsed server max packet size (MTU) in bytes
				 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a max packet size response, or could not be parsed
				 */
				std::size_t decodeMaxPacketSizeResponse(Buffer &packet) const;		
								
				// Discover all services in range
						
				/**
				 * Encode a request to discover all primary services in a given attribute handle range, defaulting to the entire server range.
				 * This is a higher-level abstraction of the primary use case for the Read Value By Group Type request, which is to discover the
				 * primary service handles, type UUIDs, and attribute handle ranges.
				 *
				 * @param packet The packet to configure for the request
				 * @param low The lowest attribute handle value to search, defaults to 0x0001 to start at the first handle
				 * @param high The highest attribute handle value to search, defaults to 0xFFFF to end at the last possible handle
				 */
				void encodeDiscoverServicesRequest(Buffer &packet, std::uint16_t low = 0x0001u, std::uint16_t high = 0xFFFFu) const;				
				
				/**
				 * Decodes the response to a request to discover services by group type. This can handle any variant of the Read By Group Type response
				 * including with short 16-bit UUIDs and full 128-bit UUIDs. The provided services list is updated to place the discovered services at the end
				 * which will have their type UUID, attribute handle, and end attribute handle properly configured.
				 *
				 * @note It's possible not all services in the request range were included in the response due to packet size limitations.
				 * If there might still be more services to discover, the given CIO Progress will have Status::Partial.
				 * Otherwise if we know that all possible services have been discovered, the CIO Progress will have Status::Success.
				 * If the result is partial, then another discover services request needs to be made with the start attribute handle being one more than
				 * the end handle of the last service in the output service list.
				 *
				 * @param packet The link layer packet to process for the response
				 * @param services The list of Service definitions to append any discovered service groups to
				 * @return a result with a status of either Success if service discovery was complete or Partial if more services might be available but were
				 * not included in this response
				 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually an appropriate response, or could not be parsed
				 */
				Progress<std::uint16_t> decodeDiscoverServicesResponse(Buffer &packet, std::vector<Service> &services) const;
				
				/**
				 * Decodes the initial portion of the response to a request to discover services by group type. This detects and returns
				 * the size of an individual service response record, which is 6 for short 16-bit UUIDs and 20 for full 128-bit UUIDs.
				 *
				 * The returned result will have Status::Success if this is the last packet for discovering services or Status::Partial if more might exist.
				 * The Progress's count field will be set to the size of each individual response item: 6 bytes for short responses and 20 bytes for long responses,
				 * or 0 bytes if no data is present (should only happen on the last response which will also have Status::Success).
				 *
				 * @param packet The link layer packet to process for the response
				 * @return a result with a status of either Success if service discovery was complete or Partial if more services might be available and the size of
				 * each individual service response which is either 6 or 20
				 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually an appropriate response type, or could not be parsed
				 */
				Progress<std::uint8_t> decodeDiscoverServicesPreamble(Buffer &packet) const;
				
				/**
				 * Decodes the next service declaration in a Read By Group Type response after the initial preamble has been parsed.
				 * This method processes services declared with 16-bit short UUIDs.
				 * The packet current position will be advanced by the data Progress and the returned Service reflects the UUID, handle, and end handle that was parsed.
				 *
				 * @param packet The packet being Progress which must be a Read By Group Type response advanced to the first byte of the service value.
				 * @return the parsed service
				 */
				Service decodeNextShortServiceDiscovery(Buffer &packet) const;
									
				/**
				 * Decodes the next service declaration in a Read By Group Type response after the initial preamble has been parsed.
				 * This method processes services declared with 16-bit short UUIDs.
				 * The packet current position will be advanced by the data Progress and the returned Service reflects the UUID, handle, and end handle that was parsed.
				 *
				 * @param packet The packet being Progress which must be a Read By Group Type response advanced to the first byte of the service value.
				 * @return the parsed service
				 */
				Service decodeNextServiceDiscovery(Buffer &packet) const;
				
				// Discover service by type
		
				/**
				 * Encodes a packet to request the attribute handle range for a particular primary service given its 128-bit definition UUID.
 		 		 * This is a higher-level abstraction that uses the Find By Type request to get the first handle range for a primary service declaration
 		 		 * whose value (the UUID definition) matches the given 128-bit UUID.
 		 		 *
 		 		 *
 		 		 * @param packet The packet to configure for the find by type request to locate the service
 		 		 * @param serviceId The unique ID definition value of the service
				 * @param low The starting attribute handle for the range to search for the service
				 * @param high The ending attribute handle for the range to search for service
				 */
				void encodeFindServiceRequest(Buffer &packet, const UniqueId &serviceId, std::uint16_t low = 0x0001u, std::uint16_t high = 0xFFFFu) const;
								
				/**
				 * Decodes the response for a find by type request used to locate a particular service.
				 *
				 * @note Strictly speaking a find by type request could find multiple results, and nothing prevents a server from having multiple services of the same type.
				 * In such cases, this method will just return the first (lowest attribute range) service located. In most cases there should only be one service for
				 * a particular type though.
				 *
				 * @note The returned service will not have not have the definition UUID set because this packet does not actually contain that information. The client
				 * who originally made the request will need to set that field.
				 *
				 * @return the located service, if found
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a find by type response, or had malformed data that wasn't parseable
			 	 */
				Service decodeFindServiceResponse(Buffer &packet) const;
				
				// Discover characteristics in range
				
				/**
				 * Encode a request to discover all characteristics in the given attribute handle range.
				 * This is a higher-level abstraction that uses the Read By Type request to get the Characteristic attribute's data.
				 * Normally you would want to set the attribute handle range to the range defined by a particular Service.
				 * However, nothing stops you from providing any range you want and the default is to obtain all characteristics on the server.
				 *
				 * @param packet The packet to configure for the read by type request to get characteristics
				 * @param low The starting attribute handle for the range to search for characteristics
				 * @param high The ending attribute handle for the range to search for characteristics
				 */
				void encodeDiscoverCharacteristicsRequest(Buffer &packet, std::uint16_t low = 0x0001u, std::uint16_t high = 0xFFFFu) const;
					
				/**
				 * Decodes a read by type response to parse the discovered characteristics in the response.
 				 * This can handle any variant of the Read By Type response including with short 16-bit UUIDs and full 128-bit UUIDs.
 				 * The provided characteristics list is updated to place the discovered characteristics at the end
				 * which will have their type UUID, attribute handle, properties, and attribute value handle properly configured.
				 *
				 * @note It's possible not all characteristics in the request range were included in the response due to packet size limitations.
				 * If there might still be more characteristics to discover, the given CIO Progress will have Status::Partial.
				 * Otherwise if we know that all possible characteristics have been discovered, the CIO Progress will have Status::Success.
				 * If the result is partial, then another discover characteristics request needs to be made with the start attribute handle being one more than
				 * the value handle of the last characteristic added to the output.
				 *
				 * @param packet The packet to process
				 * @param characteristics The characteristics list to update with any found characteristics
				 * @return a result with status of Success if all characteristics were discovered or Partial if more might need to be found
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a read by type response, or had malformed data that wasn't parseable
				 */
				Progress<std::uint16_t> decodeDiscoverCharacteristicsResponse(Buffer &packet, std::vector<Characteristic> &characteristics) const;
				
				/**
				 * Decodes the initial portion of a Read By Type response needed for service discovery.
			 	 * This validates that the response is the correct type, correctly formatted, processes any errors in an error response, and then determines
				 * the item length of each item if there is data. The item length is 7 for 16-bit short UUIDs and 21 for 128-bit full UUIDs.
				 * The packet buffer will be configured to represent all of the characteristics in the response.
				 *
				 * @param packet The packet as received by the link layer
				 * @return a result indicating the size of each characteristic item (7 or 21) and whether the response indicated all characteristics were Progress
				 * or more requests may be needed
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a read by type response, or had malformed data that wasn't parseable
				 */
				Progress<std::uint8_t> decodeDiscoverCharacteristicsPreamble(Buffer &packet) const;
								
				/**
				 * Decodes the next characteristic declaration in a Read By Type response after the initial preamble has been parsed.
				 * This method processes characteristics declared with 16-bit short UUIDs.
				 * The packet current position will be advanced by the data Progress and the returned Characteristic reflects the UUID, handle, properties, and value handle
				 * that was parsed.
				 *
				 * @param packet The packet being Progress which must be a Read By Type response advanced to the first byte of the characteristic value.
				 * @return the parsed characteristic
				 */
				Characteristic decodeNextShortCharacteristic(Buffer &packet) const;
				
				/**
				 * Decodes the next characteristic declaration in a Read By Type response after the initial preamble has been parsed.
				 * This method processes characteristics declared with 128-bit UUIDs.
				 * The packet current position will be advanced by the data Progress and the returned Characteristic reflects the UUID, handle, properties, and value handle
				 * that was parsed.
				 *
				 * @param packet The packet being Progress which must be a Read By Type response advanced to the first byte of the characteristic value.
				 * @return the parsed characteristic
				 */
				Characteristic decodeNextCharacteristic(Buffer &packet) const;
				
				// Find Information
				
				/**
				 * Encodes a request to find information about attributes in a handle range.
				 * This will get attribute handles and types.
				 *
				 * @param packet The packet to configure for the find information request
				 * @param start The start handle of the range to search
				 * @param end The end handle of the range to search
				 */
				void encodeFindInformationRequest(Buffer &packet, std::uint16_t start = 0x0001u, std::uint16_t end = 0xFFFFu) const;
				
				/**
				 * Decodes a response to a find information request to append discovered Attribute handles and types to the given list.
				 *
				 * @param packet The received packet to decode
				 * @param attrs The attribute list to append discovered attributes to
				 * @return a result with status either Success if all attributes have been discovered or Partial if more might exist, and a count of how many were added this time
				 */
				Progress<std::uint16_t> decodeFindInformationResponse(Buffer &packet, std::vector<Attribute> &attrs) const;
				
				/**
				 * Decodes the initial information about a find information response.
				 * This validates that the response is the correct type, correctly formatted, processes any errors in an error response, and then determines
				 * the item length of each item if there is data.
				 * 
				 * The returned result will have Status::Success if this is the last packet for discovering attributes or Status::Partial if more might exist.
				 * The Progress's count field will be set to the size of each individual response item: 4 bytes for short responses and 18 bytes for long responses,
				 * or 0 bytes if no data is present (should only happen on the last response).
				 *
				 * @return the result specifying whether this is the last find information response packet and how big each individual item is
				 * @throw cio::Exception If the packet was empty, not a find information response, or malformed
				 */
				Progress<std::uint8_t> decodeFindInformationPreamble(Buffer &packet) const;
				
				/**
				 * Decodes the next entry in a find information response packet, returning an attribute reflecting the parsed handle and type.
				 * This method handles long attribute types using 128-bit UUIDs.
				 * This method must be called on a packet where decodeFindInformationPreamble returned successfully with a count field of 18.
				 *
				 * @param packet The packet to decode
				 * @return the attribute decoded from the packet
				 */
				Attribute decodeNextAttributeResponse(Buffer &packet) const;
				
				/**
				 * Decodes the next entry in a find information response packet, returning an attribute reflecting the parsed handle and type.
				 * This method handles short attribute types using 16-bit UUIDs.
				 * This method must be called on a packet where decodeFindInformationPreamble returned successfully with a count field of 4.
				 *
				 * @param packet The packet to decode
				 * @return the attribute decoded from the packet
				 */
				Attribute decodeNextShortAttributeResponse(Buffer &packet) const;

				// Read
				
				/**
				 * Encodes a request to read the value of an attribute given its handle.
				 * For long attributes, this will only read the value up to the maximum packet size.
				 *
				 * @param packet The packet to configure for the read request
				 * @param handle The handle of the attribute value to read
				 */
				void encodeReadRequest(Buffer &packet, std::uint16_t handle) const;
				
				/**
				 * Decodes a response with the value read from an attribute read request.
				 * The packet buffer is configured so that the attribute data is the data from current to limit and can be directly read by the user
				 * after this call completes.
				 *
				 * @param packet The packet received from the link layer
				 * @return a reference to the given packet after configuring it to read the value
				 */
				Buffer &decodeReadResponse(Buffer &packet) const;
				
				// Read By Type
				
				/**
				 * Encodes a request to read the value of an attribute given a possible handle range and its type.
				 * This can in theory result in multiple values being returned if more than attribute has the same type in the range.
				 * However, typically it is used to find well known attributes in a service range where only value will be the result.
				 *
				 * @param packet The packet to configure for the request
				 * @param type The type UUID of the attribute to read
				 * @param low The starting attribute handle for the range to search for the attribute to read
				 * @param high The ending attribute handle for the range to search for the attribute to read
				 */
				void encodeReadByTypeRequest(Buffer &packet, const UniqueId &type, std::uint16_t low = 0x0001u, std::uint16_t high = 0xFFFFu) const;

				/**
				 * Decodes the response to a read by type request, presuming that only a single response is received.
				 * The packet buffer is configured so that the attribute data is the data from current to limit and can be directly read by the user
				 * after this call completes.
				 *
				 * There can be more than one attribute response to the same request, either in the current packet or requiring a future request.
				 * This method will not handle that case and is optimized for the simpler case where only one attribute is expected to match the type.
				 *
				 * @param packet The packet as received by the link layer
				 * @return the attribute handle that has the value in the packet buffer
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a read by type response, or was malformed
				 */ 
				std::uint16_t decodeReadByTypeResponse(Buffer &packet) const;
				
				/**
				 * Decodes the initial portion of a read by type response, determining and returning the element size.
				 * This validates that the response is the correct type, correctly formatted, processes any errors in an error response, and then determines
				 * the item length of each item if there is data. The item length of a read by type response may be of any length of at least 3 bytes since
				 * it must include the 2 byte handle and at least 1 byte of data.
				 *
				 * The packet buffer will be configured to represent all of the data items present in the response, so the item count can be determined
				 * by taking the remaining length divided by the item length.
				 *
				 * @param packet The packet as received by the link layer
				 * @return the status of the read by type response and the length of each response item
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a read by type response, or was malformed
				 */
				Progress<std::uint8_t> decodeReadByTypePreamble(Buffer &packet) const;
				
				/**
				 * Decodes the response to a read by type request, providing each response to the given listener.
				 * This will fully process the given packet if possible.
				 *
				 * Due to packet size limitations, this may not be all of the possible responses for the original request.
				 * If that may be the case, this will return Status::Partial.
				 * If no more responses are possible, this will return Status::Success.
				 *
				 * @param packet The packet as received by the link layer
				 * @param listener The listener to receive each read attribute handle and value
				 * @return the status indicating whether the request was fully satisfied or only partial results may have been returned and a count of attributes received this time
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a read by type response, or was malformed
				 */ 
				Progress<std::uint16_t> decodeReadByTypeResponse(Buffer &packet, const AttributeCallback &listener) const;
				
				// Write (with response)
				
				/**
				 * Configures the given buffer to represent a write request for an attribute given its handle.
				 * This writes the Request::Write opcode and handle appropriately to the packet, then configures the packet
				 * so that the remaining data (current to limit) can be used for whatever data the user actually wants to send.
				 * The user can directly write their data to the packet then flip it to send the PDU to the link layer.
				 * 
				 * This version is slightly more efficient since it avoids a buffer copy if the user is writing dynamic data to the packet.
				 *
				 * After sending a write request, the next response from the server should be a write response.
				 *
				 * @param packet The packet being created
				 * @param handle The handle of the attribute to write
				 * @return a reference to packet after it has been configured to let the user write the data for the attribute value
				 */
				Buffer &initiateWriteRequest(Buffer &packet, std::uint16_t handle) const;
								
				/**
				 * Configures the given buffer to represent a write request for an attribute given its handle and the given binary data.
				 * This writes the Request::Write opcode and handle appropriately to the packet, then copies the given input data to the packet.
				 * 
				 * This version is useful if you are writing buffer data that you already have in place.
				 *
				 * After sending a write request, the next response from the server should be a write response.
				 *
				 * @param packet The packet being created
				 * @param handle The handle of the attribute to write
				 * @param data The data to write to the attribute value
				 * @param length The number of bytes to write to the attribute value
				 */
				void encodeWriteRequest(Buffer &packet, std::uint16_t handle, const void *data, std::size_t length) const;
				
				/**
				 * Decodes the packet for a write response.
				 * Write responses are purely a confirmation that a write occurred so the packet does not actually have any data.
				 * 
				 * @param packet The packet as received by the link layer
			 	 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a write response, or unexpectedly had data content
			 	 */
				void decodeWriteResponse(Buffer &packet) const;
				
				// Write (without response)
								
				/**
				 * Configures the given buffer to represent a write command for an attribute given its handle.
				 * This writes the Command::Write opcode and handle appropriately to the packet, then configures the packet
				 * so that the remaining data (current to limit) can be used for whatever data the user actually wants to send.
				 * The user can directly write their data to the packet then flip it to send the PDU to the link layer.
				 * 
				 * This version is slightly more efficient since it avoids a buffer copy if the user is writing dynamic data to the packet.
				 *
				 * Write commands do not generate responses so there is no way to determine if the write command actually succeeded.
				 *
				 * @param packet The packet being created
				 * @param handle The handle of the attribute to write
				 * @return a reference to packet after it has been configured to let the user write the data for the attribute value
				 */
				Buffer &initiateWriteCommand(Buffer &packet, std::uint16_t handle) const;
				
				/**
				 * Configures the given buffer to represent a write command for an attribute given its handle and the given binary data.
				 * This writes the Command::Write opcode and handle appropriately to the packet, then copies the given input data to the packet.
				 * 
				 * This version is useful if you are writing buffer data that you already have in place.
				 *
				 * Write commands do not generate responses so there is no way to determine if the write command actually succeeded.
				 *
				 * @param packet The packet being created
				 * @param handle The handle of the attribute to write
				 * @param data The data to write to the attribute value
				 * @param length The number of bytes to write to the attribute value
				 */
				void encodeWriteCommand(Buffer &packet, std::uint16_t handle, const void *data, std::size_t length) const;
				
				// Notify - receive data
		
				/**
				 * Decodes a notify packet.
				 * The attribute handle that has an updated value is returned.
				 * The packet is configured so that the active range (current to limit) is the attribute value.
				 *
				 * @note Notify packets can be received at any time if notifications have been enabled for any attributes, even if a request has been submitted
				 * and a response has not yet been received, so the higher level connection logic should always be prepared to handle them.
				 * The client does not send any repsponse to notify packets so the server has no way to know if the client received them.
				 *
				 * @param packet The packet as received by the link layer
				 * @return the attribute handle of the attribute that sent new data
				 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually a notification, or could not be parsed
				 */		
				std::uint16_t decodeNotify(Buffer &packet) const;
				
				// Indicate - receive data and server wants confirmation
				
				/**
				 * Decodes an indicate request packet.
				 * The attribute handle that has an updated value is returned.
				 * The packet is configured so that the active range (current to limit) is the attribute value.
				 * The next packet sent to the server should be an indicate response confirming that the indication was received.
				 *
				 * @note Indicate request packets can be received at any time if indications have been enabled for any attributes, even if a request has been submitted
				 * and a response has not yet been received, so the higher level connection logic should always be prepared to handle them.
				 *
				 * @param packet The packet as received by the link layer
				 * @return the attribute handle of the attribute that sent new data
				 * @throw cio::Exception If the packet represented a BLE ATT error, was not actually an indicate request, or could not be parsed
				 */		
				std::uint16_t decodeIndicateRequest(Buffer &packet) const;
				
				/**
				 * Encodes an indicate response (confirmation) packet.
				 * The client should send such a response after receiving an indication request.
				 * The response does not actually have any state or data, it just is a single byte stating that it is an indication response.
				 *
				 * @param packet The packet to configure for the indication response
				 */	
				void encodeIndicateResponse(Buffer &packet) const;
				
			private:
				/** Class logger */
				static Class<AttributeCodec> sMetaclass;
		};	
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

