/*==============================================================================
 * Copyright 2017-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TYPES_H
#define CIO_TYPES_H

#if defined _WIN32
#if defined CIO_BUILD
#if defined CIO_BUILD_SHARED
#define CIO_API __declspec(dllexport)
#if defined _MSC_VER
#define CIO_TEMPLATE_EXTERN
#define CIO_TEMPLATE_INSTANCE CIO_API
#else
#define CIO_TEMPLATE_EXTERN CIO_API
#define CIO_TEMPLATE_INSTANCE 
#endif
#else
#define CIO_API
#endif
#else
#define CIO_API __declspec(dllimport)
#define CIO_TEMPLATE_EXTERN CIO_API
#endif
#elif defined __ELF__
#define CIO_API __attribute__ ((visibility ("default")))
#define CIO_TEMPLATE_EXTERN CIO_API
#define CIO_TEMPLATE_INSTANCE
#else
#define CIO_API
#define CIO_TEMPLATE_EXTERN
#define CIO_TEMPLATE_INSTANCE
#endif

#include <cstdint>
#include <functional>
#include <type_traits>

/** 
 * Constant representing an unknown memory size.
 * This is the largest std::size_t value.
 * All valid memory sizes are non-negative and less than this value.
 */
#define CIO_UNKNOWN_SIZE SIZE_MAX

/** 
 * Constant representing an unknown content length.
 * This is the largest signed 64-bit integer.
 * All valid resource lengths are non-negative and less than this value.
 */
#define CIO_UNKNOWN_LENGTH cio::Length(INT64_MAX)

namespace cio
{
	// Forward declarations
	enum class Action : std::uint8_t;
	class Allocator;
	class Big; // Order.h
	class Buffer;
	class BufferedInput;
	enum class Case : std::uint8_t;
	class Channel;
	class Character;
	template <typename> class Chunk;
	template <typename> class Class;
	class Device;
	class DeviceAddress;
	class Directory;
	enum class Domain : std::uint8_t;
	class Encoding;
	template <typename> class Enumerant;
	template <typename> class Enumerants;
	template <typename> class Enumeration;
	class Exception;
	template <typename> class Factory;
	class File;
	class Filesystem;
	template <std::size_t> class FixedText;
	class Host; // Order.h
	class Input;
	class JsonWriter;
	template <typename K, typename V, typename F> class KeyFactory;
	class Length;
	class Library;
	template <typename> class LibraryFunction;
	template <typename... > class Listener;
	class Lockable;
	class Little; // Order.h
	class Logger;
	template <typename = Logger, typename = std::true_type> struct LogMessageStream;
	class Malloc;
	enum class Markup : std::uint8_t;
	enum class MarkupAction : std::uint8_t;
	class MarkupEvent;
	class MarkupWriter;
	class MemoryChannel;
	class MemoryInput;
	class Metaclass;
	class Metadata;
	enum class Mode : std::uint8_t;
	class ModeSet;
	class New;
	enum class Newline : std::uint8_t;
	enum class Normalized : std::uint8_t;
	enum class Order : std::uint8_t;
	enum class Outcome : std::uint8_t;
	class Output;
	enum class Ownership : std::uint8_t;
	template <typename, typename = char, typename = std::size_t> class Parse;
	template <typename = char, typename = std::size_t> class ParseStatus;
	class Path;
	class Pipe;
	template <typename> class Pointer;
	enum class Primitive : std::uint8_t;
	template <typename, typename> class PrintProperties;
	class Program;
	template <typename> class Progress;
	class Protocol;
	class ProtocolFactory;
	class ProtocolFilter;
	template <typename> class ProtocolRegistrar; // ProtocolFactory.h
	class ReadBuffer;
	enum class Reason : std::uint8_t;
	class RelativeInput;
	enum class Resize : std::uint8_t;
	enum class Resource : std::uint8_t;
	template <typename> class Response;
	enum class Seek : std::uint8_t;
	class Serial;
	enum class Severity : std::uint8_t;
	enum class Signedness : std::uint8_t;
	class Source;
	enum class StandardPath : std::uint8_t;
	class State;
	enum class Status : std::uint8_t;
	class Task;
	class TemporaryFile;
	class Text;
	template <typename T> using TextParse = Parse<T, char, std::size_t>; // Parse.h
	using TextParseStatus = ParseStatus<char, std::size_t>; // ParseSTatus.h 
	class TextReader;
	class Token;
	class Traversal;
	enum class Type : std::uint8_t;
	class UniqueId;
	enum class Validation : std::uint8_t;
	class Variant;
	class Version;

	/**
	 * Function callback to receive indirectly or asynchronously provided data arguments.
	 * Success is indicated by returning normally. Failure is indicated by throwing any exception (preferably cio::Exception with a status and byte count).
	 */
	template <typename... A>
	using Callback = std::function<void (A...)>;
}

#endif
