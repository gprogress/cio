/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Program.h"

#include <cio/Directory.h>
#include <cio/Filesystem.h>
#include <cio/Input.h>
#include <cio/Class.h>
#include <cio/Exception.h>
#include <cio/Metadata.h>
#include <cio/Output.h>
#include <cio/ProtocolFactory.h>
#include <cio/State.h>

#include <iostream>

namespace cio
{
	namespace zip
	{
		Class<Program> Program::sMetaclass("cio::zip::Program");

		const Metaclass &Program::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		Program::Program() noexcept :
			mAction(Action::None),
			mFactory(ProtocolFactory::getDefault())
		{
			// nothing more to do
		}

		Program::Program(int argc, char **argv) :
			mAction(Action::None),
			mFactory(ProtocolFactory::getDefault())
		{
			this->parse(argc, argv);
		}

		Program::Program(const Program &in) = default;

		Program::Program(Program &&in) noexcept = default;

		Program &Program::operator=(const Program &in) = default;

		Program &Program::operator=(Program &&in) noexcept = default;

		Program::~Program() noexcept = default;
		
		const Metaclass &Program::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Program::clear() noexcept
		{
			mAction = Action::None;
			mArchivePath.clear();
			mArchive.reset();
			mWorkingDirectory.clear();
			mSelectedEntries.clear();
			mFactory = ProtocolFactory::getDefault();
		}

		void Program::parse(int argc, char **argv)
		{
			if (argc >= 2)
			{
				const char *current = argv[1];

				Path working;
				std::vector<Path> entries;
				bool needOutput = false;
				bool needWorking = false;

				int i = 1;
				while (i < argc)
				{
					const char *arg = argv[i];
					std::size_t len = std::strlen(arg);

					// Valid action/modifier arguments require at least '-' and one more character
					if (len >= 2 && arg[0] == '-')
					{
						// Long form argument
						if (arg[1] == '-')
						{
							// see if we have an '='
							const char *split = std::strchr(arg + 2, '=');
							std::size_t arglen = split - arg - 2;

							if (std::strncmp("extract", arg + 2, arglen) == 0)
							{
								mAction = Action::Read;

								if (*split == '=')
								{
									sMetaclass.warning() << "Unexpected value provided to --extract";
								}
							}
							else if (std::strncmp("get", arg + 2, arglen) == 0)
							{
								mAction = Action::Read;

								if (*split == '=')
								{
									sMetaclass.warning() << "Unexpected value provided to --get";
								}
							}
							else if (std::strncmp("list", arg + 2, arglen) == 0)
							{
								mAction = Action::Scan;

								if (*split == '=')
								{
									sMetaclass.warning() << "Unexpected value provided to --list";
								}
							}
							else if (std::strncmp("file", arg + 2, arglen) == 0)
							{
								if (*split == '=')
								{
									mArchivePath.set(split + 1);
								}
								else
								{
									needOutput = true;
								}
							}
							else if (std::strncmp("directory", arg + 2, arglen) == 0)
							{
								if (*split == '=')
								{
									// Changing working directory after entries, go ahead and capture them
									if (!entries.empty())
									{
										mSelectedEntries.emplace_back(working, std::move(entries));
									}

									working.set(split + 1);
								}
								else
								{
									needWorking = true;
								}
							}
						}
						// short form argument
						else
						{
							std::size_t j = 1;
							while (j < len)
							{
								switch (arg[j])
								{
									// next non-action argument is a filename
									case 'f':
										needOutput = true;
										break;

									// Marker to extract
									case 'x':
										mAction = Action::Read;
										break;

									// Marker to list
									case 't':
										mAction = Action::Scan;
										break;

									case 'C':
										needWorking = true;
										break;

									default:
										sMetaclass.error() << "Unrecognized short argument " << arg[j];
										break;
								}

								++j;
							}
						}
					}
					// Top level filename argument
					else
					{
						if (needOutput)
						{
							mArchivePath.set(arg);
							needOutput = false;
						}
						else if (needWorking)
						{
							// Changing working directory after entries, go ahead and capture them
							if (!entries.empty())
							{
								mSelectedEntries.emplace_back(working, std::move(entries));
							}

							mWorkingDirectory.set(arg);
							needWorking = false;
						}
						else
						{
							entries.emplace_back(arg);
						}
					}

					++i;
				}

				// Final cleanup after parsing

				if (!entries.empty())
				{
					mSelectedEntries.emplace_back(working, std::move(entries));
				}

				if (!working.empty())
				{
					mWorkingDirectory = std::move(working);
				}
			}
			else
			{
				this->help(std::cout);
			}
		}

		void Program::help(std::ostream & s)
		{
			s << "cio-zip: command line archive tool intended to emulate a subset of tar\n";
			s << "\tUsage: cio-zip <-x|-t> <-f INPUT> <-C DIR> <ENTRY1 ENTRY2 ...>\n\n";
			s << "\tAction Modes\n";
			s << "\t\t-t --list\tDisplay list of entries in archive\n";
			s << "\t\t-x --extract --get\tExtract named entries from archive to current or specified directory\n";
			s << "\t\t-h --help\tShow this help message";
			s << "\n";
			s << "\tResource Modifiers\n";
			s << "\t\t-f --file\tUse named file or URI as archive location\n";
			s << "\t\t-C --directory\tChange to named directory prior to following operations\n";
			s << "\n";
			s << "\tShort options may be combined. Options with parameters may use or omit the '='\n\n";
			s << "\tUnlike tar, this tool will work on any input that cio::ProtocolFactory considers a directory.\n";
			s << "\tIt currently supports .zip archives and native filesystem directories.\n";
		}

		void Program::execute()
		{
			switch (mAction)
			{
				case Action::Read:
					sMetaclass.info() << "Extracting from archive";
					this->open();
					this->extract();
					break;

				case Action::Scan:
					sMetaclass.info() << "Listing archive";
					this->open();
					this->list(std::cout);
					break;

				default:
					sMetaclass.error() << "Unrecognized action " << mAction;
					break;
			}
		}

		void Program::open()
		{
			if (mArchivePath.empty())
			{
				sMetaclass.error() << "No input archive path specified - standard input not yet supported";
			}
			else
			{
				if (!mFactory)
				{
					throw Exception(Reason::Unreachable, "No protocol factory set");
				}

				mArchive = mFactory->readDirectory(mArchivePath);
				sMetaclass.info() << "Opened archive using " << mArchive->getMetaclass().getQualifiedName();
			}
		}

		void Program::extract()
		{
			if (mSelectedEntries.empty())
			{
				this->extractAll();
			}
			else
			{
				this->extractSelected();
			}
		}

		void Program::extractSelected()
		{
			if (mArchive)
			{
				try
				{
					for (const std::pair<Path, std::vector<Path>> &item : mSelectedEntries)
					{
						Path working = item.first;
						Filesystem fs;

						if (working.empty())
						{
							working = fs.getCurrentDirectory();
						}

						if (!mFactory)
						{
							throw Exception(Reason::Unreachable, "No protocol factory set");
						}

						std::unique_ptr<Protocol> protocol = mFactory->getProtocol(working, Resource::Directory);

						for (const Path &entry : item.second)
						{
							Path target(working, entry);
							Metadata md = mArchive->current();

							if (md.isDirectory())
							{
								protocol->createDirectory(target);
							}
							else if (md.isFile())
							{
								std::unique_ptr<Input> input = mArchive->openCurrentInput();
								std::unique_ptr<Output> output = protocol->openOutput(target);
								input->copyTo(*output);
							}
						}
					}
				}
				catch (const Exception &e)
				{
					sMetaclass.error() << "Archive I/O error " << e.reason() << " " << e.what();
				}
			}
			else
			{
				sMetaclass.error() << "Cannot extract entries - no open archive";
			}
		}

		void Program::extractAll()
		{
			if (mArchive)
			{
				try
				{
					Filesystem fs;
					cio::Path working;
					if (mWorkingDirectory)
					{
						working = mWorkingDirectory;
					}
					else
					{
						working = fs.getCurrentDirectory();
					}
					
					if (!mFactory)
					{
						throw Exception(Reason::Unreachable, "No protocol factory set");
					}

					std::unique_ptr<Protocol> protocol = mFactory->getProtocol(working, Resource::Directory);

					while (mArchive->traversable())
					{
						Metadata md = mArchive->current();

						Path target(working, mArchive->filename());
						
						if (md.isDirectory())
						{
							protocol->createDirectory(target);
						}
						else if (md.isFile())
						{
							std::unique_ptr<Input> input = mArchive->openCurrentInput();
							std::unique_ptr<Output> output = protocol->openOutput(target);
							input->copyTo(*output);
						}

						mArchive->next();
					}
				}
				catch (const Exception &e)
				{
					sMetaclass.error() << "Archive I/O error " << e.reason() << " " << e.what();
				}
			}
			else
			{
				sMetaclass.error() << "Cannot extract entries - no open archive";
			}
		}

		void Program::list(std::ostream &printer)
		{
			if (mArchive)
			{
				try
				{
					while (mArchive->traversable())
					{
						Metadata md = mArchive->current();

						if (md.isFile())
						{ 
							printer << mArchive->filename() << " " << md.getType() << " " << md.getLength() << '\n';
						}
						else if (md.isDirectory())
						{
							printer << mArchive->filename() << " " << md.getType() << '\n';
						}

						mArchive->next();
					}
				}
				catch (const Exception &e)
				{
					sMetaclass.error() << "Archive I/O error " << e;
				}
			}
			else
			{
				sMetaclass.error() << "Cannot list entries - no open archive";
			}
		}

		const ProtocolFactory *Program::getProtocolFactory() const noexcept
		{
			return mFactory;
		}

		void Program::setProtocolFactory(const ProtocolFactory *factory) noexcept
		{
			mFactory = factory;
		}
	}
}
