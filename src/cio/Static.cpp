/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/

// This file is necessary to control order of initialization for static variable that depend on each other
// This also handles the various build configurations of separate shared library (where load order is honored)
// vs. all-in-integrated builds and static library builds (where everything needs to be here)
// 
// C++ primitive constants are guaranteed to be baked into the program image so those can be skipped
// Anything with actual constructors needs to be handled here though

#if defined CIO_BUILD

#include <cio/Buffer.h>
#include <cio/Class.h>
#include <cio/Malloc.h>
#include <cio/New.h>
#include <cio/ProtocolFactory.h>

namespace cio
{
	// Global allocators first, they are used by everything 
	Allocator Allocator::sNone;

	New Allocator::sNew;

	Malloc Allocator::sMalloc;

	std::atomic<Allocator *> Allocator::sGlobalDefault(&sNew);

	// Metaclasses second, they reference global allocators but may be used by other statics

	Class<ProtocolFilter> ProtocolFilter::sMetaclass("cio::ProtocolFilter");

	// Protocol filter information

	const Text Protocol::sWildcard("*");

	const ProtocolFilter Protocol::sEmptyFilter = { nullptr, Protocol::sWildcard, Protocol::sWildcard, Resource::None };

	ProtocolFactory ProtocolFactory::sDefault;
}

#include <cio/Filesystem.h>
#include <cio/Class.h>
#include <cio/ProtocolRegistrar.h>

namespace cio
{
	const Text Filesystem::sFilePrefix("file");

	ProtocolFilter Filesystem::sDefaultFilter[1] = {
		{ &sMetaclass, sFilePrefix, sWildcard, Resource::Unknown }
	};

	cio::ProtocolRegistrar<cio::Filesystem> sProtocolRegistrar;
}

#endif

#if defined CIO_ZIP_BUILD

#include <cio/zip/Protocol.h>
#include <cio/Class.h>
#include <cio/ProtocolRegistrar.h>

namespace cio
{
	namespace zip
	{
		const Text Protocol::sJarPrefix("jar");

		const Text Protocol::sArchiveExtensions("zip;jar;3tz");

		ProtocolFilter Protocol::sDefaultFilter[2] = {
			{ &sMetaclass, sJarPrefix, sWildcard, Resource::Unknown },
			{ &sMetaclass, sWildcard, sArchiveExtensions, Resource::Directory }
		};

		cio::ProtocolRegistrar<cio::zip::Protocol> sZipProtocolRegistrar;
	}
}

#endif
