/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Directory.h"

#include "Exception.h"
#include "cio/Metadata.h"
#include "cio/Class.h"
#include "cio/ModeSet.h"
#include "cio/Path.h"
#include "cio/Status.h"
#include "cio/Text.h"
#include "cio/Resource.h"

#include "File.h"

#if defined CIO_HAVE_WINDOWS_H

#include <windows.h>

#define CIO_HAVE_WINDOWS_DIR

#endif

#if defined CIO_HAVE_DIRENT_H && defined CIO_HAVE_SYS_TYPES_H && defined CIO_HAVE_FCNTL_H && CIO_HAVE_SYS_STAT_H && CIO_HAVE_UNISTD_H

#include <cerrno>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define CIO_HAVE_POSIX_DIR

#endif

#include <cctype>

namespace cio
{
	Class<Directory> Directory::sMetaclass("cio::Directory");

	std::vector<std::string> Directory::listNativeEntries(const Path &path)
	{
		std::vector<std::string> entries;
		Directory dir;

		dir.open(path);
		State e;
		while (dir)
		{
			Text current = dir.filename();
			if (dir.checkForSpecialFilename(current) == Resource::None)
			{
				entries.emplace_back(current);
			}

			e = dir.next();
		}

		if (e.reason != Reason::Underflow)
		{
			throw Exception(e);
		}

		return entries;
	}

	std::vector<Path> Directory::list(const Path &path)
	{
		std::vector<Path> children;
		Directory dir;

		dir.open(path);
		State e;
		while (dir)
		{
			Text current = dir.filename();
			if (dir.checkForSpecialFilename(current) == Resource::None)
			{
				children.emplace_back(path, current);
			}

			e = dir.next();
		}

		if (e.reason != Reason::Underflow)
		{
			throw Exception(e);
		}

		return children;
	}

	std::vector<Path> Directory::list(const Path &path, const std::vector<std::string> &extensions)
	{
		std::vector<Path> children;
		Directory dir;

		if (extensions.empty())
		{
			children = list(path);
		}
		else
		{
			dir.open(path);
			State e;
			while (dir)
			{
				Text current = dir.filename();
				if (dir.checkForSpecialFilename(current) == Resource::None)
				{
					for (const Text &s : extensions)
					{
						if (s.endsWith(current.splitAfter('.')))
						{
							children.emplace_back(path, current);
							break;
						}
					}
				}

				e = dir.next();
			}

			if (e.reason != Reason::Underflow)
			{
				throw Exception(e);
			}
		}

		return children;
	}

	const Metaclass &Directory::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Directory::Directory() noexcept :
		mDirectory(nullptr),
		mEntry(nullptr)
	{
		// nothing more to do
	}

	Directory::Directory(const Path &path) :
		mDirectory(nullptr),
		mEntry(nullptr)
	{
		Text file = path.toNativeFile();
		State e = this->openNativeHandle(file.data(), ModeSet::readonly());
		if (e.failed() && e.reason != Reason::Underflow)
		{
			throw Exception(e, "Failed to open directory");
		}
	}

	Directory::Directory(const Path &path, ModeSet modes) :
		mDirectory(nullptr),
		mEntry(nullptr)
	{
		Text file = path.toNativeFile();
		State e = this->openNativeHandle(file.data(), modes);
		if (e.failed() && e.reason != Reason::Underflow)
		{
			throw Exception(e, "Failed to open directory");
		}
	}

	Directory::Directory(Directory &&in) noexcept :
		mDirectory(in.mDirectory),
		mEntry(in.mEntry)
	{
		in.mDirectory = nullptr;
		in.mEntry = nullptr;
	}

	Directory &Directory::operator=(Directory &&in) noexcept
	{
		if (this != &in)
		{
			this->disposeNativeHandle();

			mDirectory = in.mDirectory;
			in.mDirectory = nullptr;

			mEntry = in.mEntry;
			in.mEntry = nullptr;
		}

		return *this;
	}

	Directory::~Directory() noexcept
	{
		this->disposeNativeHandle();
	}

	const Metaclass &Directory::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Directory::clear() noexcept
	{
		this->disposeNativeHandle();
	}

	void *Directory::getNativeHandle() noexcept
	{
		return mDirectory;
	}

	void *Directory::getNativeEntry() noexcept
	{
		return mEntry;
	}

	ModeSet Directory::openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory)
	{
		Text file = resource.toNativeFile();

		State e = this->openNativeHandle(file.data(), modes);
		if (e.failed() && e.reason != Reason::Underflow)
		{
			throw Exception(e, "Failed to open directory");
		}

		return ModeSet::readonly();
	}

#if defined CIO_HAVE_WINDOWS_DIR

	struct WindowsSearch
	{
		WIN32_FIND_DATA entry;
		char path[MAX_PATH];
	};

	bool Directory::traversable() const noexcept
	{
		return mEntry != nullptr && static_cast<const WindowsSearch *>(mEntry)->entry.cFileName[0] != 0;
	}

	State Directory::openNativeHandle(const char *path, ModeSet modes) noexcept
	{
		this->clear();

		State e;
		if (path)
		{
			char tmp[MAX_PATH] = { };
			std::size_t length = std::strlen(path);
			if (length > 0)
			{
				std::memcpy(tmp, path, length);
				tmp[length] = '\\';
				tmp[length + 1] = '*';
				tmp[length + 2] = 0;

				WindowsSearch *search = new WindowsSearch();
				std::memcpy(search->path, path, length + 1);
				HANDLE handle = ::FindFirstFile(tmp, &search->entry);

				if (handle == INVALID_HANDLE_VALUE)
				{
					DWORD error = ::GetLastError();

					delete search;

					if (error == ERROR_FILE_NOT_FOUND)
					{
						e.fail(Reason::Underflow);
					}
					else
					{
						e.fail(errorWin32(error));
					}
				}
				else
				{
					mDirectory = handle;
					mEntry = search;
					e.succeed();
				}
			}
			else
			{
				e.fail(Reason::Invalid);
			}
		}
		else
		{
			e.fail(Reason::Invalid);
		}

		return e;
	}

	Metadata Directory::current() const
	{
		Metadata metadata;

		if (mEntry)
		{
			const WindowsSearch *data = static_cast<const WindowsSearch *>(mEntry);
			if (data->entry.cFileName[0] != 0)
			{
				metadata.setLocation(Path::fromNativeFile(data->entry.cFileName));

				if ((data->entry.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				{
					metadata.setType(Resource::Directory);
				}
				else
				{
					metadata.setType(Resource::File);

					std::uint64_t length = static_cast<std::uint64_t>(data->entry.nFileSizeLow) +
						(static_cast<std::uint64_t>(data->entry.nFileSizeHigh) << 32);
					metadata.setLength(length);
				}

				if ((data->entry.dwFileAttributes & FILE_ATTRIBUTE_READONLY) != 0)
				{
					metadata.setMode(ModeSet::readonly());
				}
				else
				{
					metadata.setMode(ModeSet::general());
				}
			}
		}

		return metadata;
	}

	Text Directory::filename() const
	{
		Text name;
		if (mEntry)
		{
			const WIN32_FIND_DATA *data = static_cast<const WIN32_FIND_DATA *>(mEntry);
			name.bind(data->cFileName, sizeof(data->cFileName));
		}
		return name;
	}

	State Directory::next()
	{
		State e(Action::Discover);
		if (mEntry)
		{
			WindowsSearch *search = static_cast<WindowsSearch *>(mEntry);

			BOOL success = ::FindNextFile(mDirectory, &search->entry);
			if (success)
			{
				e.succeed();
			}
			else
			{
				// Clear out entry, this is how we know we're in this state
				std::memset(&search->entry, 0, sizeof(search->entry));
				DWORD error = ::GetLastError();
				if (error == ERROR_NO_MORE_FILES)
				{
					e.fail(Reason::Underflow);
				}
				else
				{
					throw Exception(errorWin32(error), "Could not get next directory entry");
				}
			}
		}
		else
		{
			e.fail(Reason::Unopened);
		}

		return e;
	}

	std::unique_ptr<Input> Directory::openCurrentInput(ModeSet modes) const
	{
		std::unique_ptr<Device> input;

		if (!mEntry)
		{
			throw Exception(Reason::Unopened);
		}

		// We need original directory path to construct filename
		const WindowsSearch *search = static_cast<const WindowsSearch *>(mEntry);
		if (search->entry.cFileName[0] == 0)
		{
			throw Exception(Reason::Underflow, "Cannot open input at end of directory");
		}

		// Make sure it's not a directory
		if ((search->entry.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
		{
			throw Exception(Reason::Unusable, "Cannot open file input for subdirectory entry");
		}

		// Construct an absolute native path
		char fullpath[MAX_PATH];
		std::size_t length = std::strlen(search->path);
		std::memcpy(fullpath, search->path, length);
		fullpath[length] = '\\';

		std::size_t length2 = std::strlen(search->entry.cFileName);
		std::memcpy(fullpath + length + 1, search->entry.cFileName, length2);
		fullpath[length + 1 + length2] = 0;

		input.reset(new File(fullpath, modes));

		return input;
	}

	std::unique_ptr<Input> Directory::openInput(const Path &path, ModeSet modes) const
	{
		std::unique_ptr<Input> input;

		if (path.isRelative())
		{
			if (!mEntry)
			{
				throw Exception(Reason::Unopened);
			}

			if (path.empty())
			{
				throw Exception(Reason::Invalid, "Cannot open empty relative path");
			}

			// We need original directory path to construct filename
			const WindowsSearch *search = static_cast<const WindowsSearch *>(mEntry);


			// Construct an absolute native path
			char fullpath[MAX_PATH];
			std::size_t length = std::strlen(search->path);
			std::memcpy(fullpath, search->path, length);
			fullpath[length] = '\\';

			std::string converted = path.toWindowsFile();
			std::memcpy(fullpath + length + 1, converted.c_str(), converted.size() + 1);

			// Might fail if file doesn't exist or is wrong type
			input.reset(new File(fullpath, modes));
		}
		else
		{
			input = Filesystem::openInput(path, modes);
		}

		return input;
	}

	State Directory::rewind()
	{
		// Need to reopen search handle with search path
		State state(Action::Seek);

		if (mEntry)
		{
			HANDLE handle = reinterpret_cast<HANDLE>(mDirectory);
			WindowsSearch *search = static_cast<WindowsSearch *>(mEntry);

			std::size_t length = std::strlen(search->path);

			char tmp[MAX_PATH];
			std::memcpy(tmp, search->path, length);
			tmp[length] = '\\';
			tmp[length + 1] = '*';
			tmp[length + 2] = 0;

			::FindClose(handle);
			mDirectory = nullptr;

			handle = ::FindFirstFile(tmp, &search->entry);

			if (handle == INVALID_HANDLE_VALUE)
			{
				DWORD error = ::GetLastError();

				delete search;
				mEntry = nullptr;

				if (error == ERROR_FILE_NOT_FOUND)
				{
					state.fail(Reason::Missing);
				}
				else
				{
					state.fail(errorWin32(error));
					throw Exception(state);
				}
			}
			else
			{
				mDirectory = handle;
				state.succeed();
			}
		}
		else
		{
			state.fail(Reason::Unopened);
			throw Exception(state);
		}

		return state;
	}

	State Directory::seekToEntry(const Path &relative)
	{
		State state(Action::Seek);

		if (relative.empty())
		{
			throw Exception(Reason::Invalid, "Relative path to seek was empty");
		}

		if (!relative.isRelative())
		{
			throw Exception(Reason::Unusable, "Absolute path provided for relative seek");
		}

		if (mEntry)
		{
			Text path = relative.toWindowsFile();
			WindowsSearch *search = static_cast<WindowsSearch *>(mEntry);

			// If we're already on the current path, we don't need to do anything
			if (search->entry.cFileName == path)
			{
				state.succeed();
			}
			// Otherwise we have to restart the search
			else
			{
				HANDLE handle = reinterpret_cast<HANDLE>(mDirectory);
				std::size_t length = std::strlen(search->path);

				char tmp[MAX_PATH];
				std::memcpy(tmp, search->path, length);
				tmp[length] = '\\';
				tmp[length + 1] = '*';
				tmp[length + 2] = 0;

				::FindClose(handle);
				mDirectory = nullptr;

				handle = ::FindFirstFile(tmp, &search->entry);
				if (handle == INVALID_HANDLE_VALUE)
				{
					DWORD error = ::GetLastError();

					delete search;
					mEntry = nullptr;

					if (error == ERROR_FILE_NOT_FOUND)
					{
						state.fail(Reason::Underflow);
					}
					else
					{
						state.fail(errorWin32(error));
						throw Exception(state);
					}
				}
				else
				{
					mDirectory = handle;

					if (search->entry.cFileName == path)
					{
						state.succeed();
					}
					else
					{
						while (::FindNextFile(handle, &search->entry))
						{
							if (search->entry.cFileName == path)
							{
								state.succeed();
								break;
							}
						}

						if (!state.succeeded())
						{
							int error = ::GetLastError();
							if (error == ERROR_NO_MORE_FILES)
							{
								std::memset(&search->entry, 0, sizeof(search->entry));
								state.fail(Reason::Underflow);
							}
							else
							{
								state.fail(errorWin32());
								throw Exception(state);
							}
						}
					}
				}
			}
		}
		else
		{
			state.fail(Reason::Unopened);
			throw Exception(state);
		}

		return state;
	}

	void Directory::disposeNativeHandle() noexcept
	{
		if (mEntry)
		{
			delete static_cast<WindowsSearch *>(mEntry);
			mEntry = nullptr;
		}

		if (mDirectory)
		{
			::FindClose(mDirectory);
			mDirectory = nullptr;
		}
	}

#elif defined CIO_HAVE_POSIX_DIR
	bool Directory::traversable() const noexcept
	{
		return mEntry != nullptr;
	}

	State Directory::openNativeHandle(const char *path, ModeSet modes) noexcept
	{
		State state;
		this->clear();

		DIR *dirp = ::opendir(path);
		if (dirp)
		{
			mDirectory = dirp;
			struct dirent *ent = ::readdir(dirp);
			if (ent)
			{
				mEntry = ent;
				state.succeed();
			}
			else
			{
				int error = errno;

				if (error == 0)
				{
					state.fail(Reason::Underflow);
				}
				else
				{

					state.fail(cio::error(error));
				}
			}
		}
		else
		{
			int error = errno;
			state.fail(cio::error(error));
		}

		return state;
	}

	State Directory::next()
	{
		State e(Action::Discover);
		if (mDirectory)
		{
			errno = 0;
			DIR *dirp = static_cast<DIR *>(mDirectory);
			mEntry = nullptr;

			struct dirent *ent = ::readdir(dirp);
			if (ent)
			{
				mEntry = ent;
				e.succeed();
			}
			else
			{
				int error = errno;

				if (error == 0)
				{
					e.fail(Reason::Underflow);
				}
				else
				{
					throw Exception(cio::error(error), "Failed to read next directory entry");
				}
			}
		}
		else
		{
			e.fail(Reason::Unopened);
		}

		return e;
	}

	State Directory::rewind()
	{
		State state(Action::Seek);
		if (mDirectory)
		{
			errno = 0;
			DIR *dirp = static_cast<DIR *>(mDirectory);
			mEntry = nullptr;

			::rewinddir(dirp); // can't fail

			struct dirent *ent = ::readdir(dirp);
			if (ent)
			{
				mEntry = ent;
				state.succeed();
			}
			else
			{
				int error = errno;

				if (error == 0)
				{
					state.fail(Reason::Underflow);
				}
				else
				{
					throw Exception(cio::error(error), "Failed to read next directory entry");
				}
			}
		}
		else
		{
			state.fail(Reason::Unopened);
			throw Exception(state);
		}

		return state;
	}

	State Directory::seekToEntry(const Path &relative)
	{
		State state(Action::Seek);

		if (relative.empty())
		{
			throw Exception(Reason::Invalid, "Relative path to seek was empty");
		}

		if (!relative.isRelative())
		{
			throw Exception(Reason::Unusable, "Absolute path provided for relative seek");
		}

		Text path = relative.toUnixFile();

		if (mDirectory)
		{
			// Check to see if current entry is what we need, if so nothing needs to be done
			struct dirent *ent = static_cast<struct dirent *>(mEntry);
			if (ent && ent->d_name == path)
			{
				state.succeed();
			}
			else
			{
				DIR *dirp = reinterpret_cast<DIR *>(mDirectory);
				mEntry = nullptr;

				::rewinddir(dirp); // can't fail

				ent = ::readdir(dirp);
				while (ent)
				{
					if (ent && ent->d_name == path)
					{
						state.succeed();
						mEntry = ent;
						break;
					}

					ent = ::readdir(dirp);
				}

				if (!state.succeeded())
				{
					int error = errno;
					if (error == 0)
					{
						state.fail(Reason::Missing);
					}
					else
					{
						state.fail(cio::error(error));
						throw Exception(state);
					}
				}
			}
		}
		else
		{
			state.fail(Reason::Unopened);
			throw Exception(state);
		}

		return state;
	}

	Metadata Directory::current() const
	{
		Metadata metadata;

		if (mEntry)
		{
			const struct dirent *ent = static_cast<const struct dirent *>(mEntry);
			metadata.setLocation(ent->d_name);

			// it's a bit convoluted to get info here
			struct stat buffer = { };
			DIR *dirp = static_cast<DIR *>(mDirectory);
			int dir = ::dirfd(dirp);
			int found = ::fstatat(dir, ent->d_name, &buffer, 0);
			if (found != -1)
			{
				Resource type = Resource::Unknown;

				if (S_ISDIR(buffer.st_mode))
				{
					type = Resource::Directory;
				}
				else if (S_ISBLK(buffer.st_mode) ||
					S_ISCHR(buffer.st_mode) ||
					S_ISFIFO(buffer.st_mode))
				{
					type = Resource::Device;
				}
				else if (S_ISREG(buffer.st_mode))
				{
					type = Resource::File;
					metadata.setLength(buffer.st_size);
					metadata.setPhysicalSize(buffer.st_blocks * 512u);
				}

				metadata.setType(type);

				// TODO user permission mapping
				metadata.setMode(ModeSet::general());
			}
		}

		return metadata;
	}

	Text Directory::filename() const
	{
		Text filename;

		if (mEntry)
		{
			const struct dirent *ent = static_cast<const struct dirent *>(mEntry);
			filename.bind(ent->d_name);
		}

		return filename;
	}

	std::unique_ptr<Input> Directory::openCurrentInput(ModeSet modes) const
	{
		std::unique_ptr<Device> input;

		if (!mEntry)
		{
			throw Exception(Reason::Unopened);
		}

		const struct dirent *ent = static_cast<const struct dirent *>(mEntry);
		DIR *dirp = static_cast<DIR *>(mDirectory);
		int dir = ::dirfd(dirp);

		int fd = ::openat(dir, ent->d_name, O_RDONLY);

		// On Linux, this will open directories too, which is not what we want
		// Check flags to validate and close if it's not a regular file
		if (fd == -1)
		{
			throw Exception(error(), "Could not open current directory entry");
		}

		struct stat buffer = { };
		int found = ::fstat(fd, &buffer);

		if (found == -1)
		{
			int error = errno;
			::close(fd);
			throw Exception(cio::error(error), "Could not get opened directory entry properties");
		}

		if (S_ISDIR(buffer.st_mode))
		{
			::close(fd);
			throw Exception(Reason::Unusable, "Could not open input for current directory entry because it was a subdirectory");
		}
		// If it's a regular file, open a file::File
		else if (S_ISREG(buffer.st_mode))
		{
			input.reset(new File(fd));
		}
		// If it's anything else, open a file::Device
		else
		{
			input.reset(new Device(fd));
		}

		return input;
	}

	std::unique_ptr<Input> Directory::openInput(const Path &path, ModeSet modes) const
	{
		std::unique_ptr<Device> input;

		if (!mEntry)
		{
			throw Exception(Reason::Unopened);
		}

		if (path.empty())
		{
			throw Exception(Reason::Invalid, "Cannot open empty relative path");
		}

		if (!path.isRelative())
		{
			throw Exception(Reason::Invalid, "Cannot open absolute path relative to directory");
		}

		const struct dirent *ent = static_cast<const struct dirent *>(mEntry);
		DIR *dirp = static_cast<DIR *>(mDirectory);
		int dir = ::dirfd(dirp);

		std::string native = path.toUnixFile();

		int fd = ::openat(dir, native.c_str(), O_RDONLY);

		// On Linux, this will open directories too, which is not what we want
		// However, there is a race condition if we check and then open, so we go ahead and open first
		// Then check flags to validate and close if it's not a regular file
		if (fd == -1)
		{
			throw Exception(error(), "Could not open directory entry");
		}

		struct stat buffer = { };
		int found = ::fstat(fd, &buffer);

		if (found == -1)
		{
			int error = errno;
			::close(fd);
			throw Exception(cio::error(error), "Could not get opened directory entry properties");
		}

		// If it's a directory,
		if (S_ISDIR(buffer.st_mode))
		{
			::close(fd);
			throw Exception(Reason::Unusable, "Could not open input for directory entry because it was a subdirectory");
		}
		// If it's a regular file, open a file::File
		else if (S_ISREG(buffer.st_mode))
		{
			input.reset(new File(fd));
		}
		// If it's anything else, open a file::Device
		else
		{
			input.reset(new Device(fd));
		}

		return input;
	}

	void Directory::disposeNativeHandle() noexcept
	{
		if (mDirectory)
		{
			::closedir(static_cast<DIR *>(mDirectory));
			mDirectory = nullptr;
			mEntry = nullptr;
		}
	}

#else
#error "No cio::Directory implementation on this platform"
#endif
}

