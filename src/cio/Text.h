/*==============================================================================
 * Copyright 2021-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TEXT_H
#define CIO_TEXT_H

#include "Types.h"

#include "Case.h"
#include "Factory.h"

#include <algorithm>
#include <string>
#include <utility>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Text class is a representation of UTF-8 text that may either be an allocated and owned character array
	 * or a reference to a character sequence owned by some other object such as a std::string.
	 *
	 * Unlike C strings or std::string, Text objects are not necessarily null-terminated to enable working effectively
	 * with text substrings without copying. This class also provides a variety of methods for obtaining substrings while
	 * minimizing copying and memory allocation.
	 *
	 * This class includes an implicit cast to std::string so it can be used with any APIs that expect a std::string.
	 * However, it does not include an implicit cast to const char * since the text may not necessarily be null-terminated.
	 */
	class CIO_API Text
	{
		public:
			/**
			 * Gets the pointer to the default empty text data.
			 * This pointer is to static memory shared by all instances of the empty text.
			 * 
			 * @return The empty text data
			 */
			static const char *getEmptyText() noexcept;

			/**
			 * Transcodes UTF-8 text from the given UTF-16 text.
			 * This will process up to the given number of UTF-16 characters, stopping at the null terminator if present.
			 * 
			 * @param text The UTF-16 text
			 * @param count The number of UTF-16 characters (not bytes) to consider
			 * @return The transcoded UTF-8 text
			 */
			static Text fromUtf16(const char16_t *text, std::size_t count = SIZE_MAX);

			/**
			 * Transcodes UTF-8 text from the given Unicode text.
			 *
			 * @param text The Unicode text
			 * @param count The number of Unicode codepoints (not bytes) to consider
			 * @return The transcoded UTF-8 text
			 */
			static Text fromUnicode(const char32_t *text, std::size_t count = SIZE_MAX);

			/**
			 * Creates a non-owning view of the given C string.
			 *
			 * @param s The string
			 * @return The view
			 */
			static Text view(const char *s) noexcept;

			/**
			 * Creates a non-owning view of the given C++ string.
			 * 
			 * @param s The string
			 * @return The view
			 */
			static Text view(const std::string &s) noexcept;

			/**
			 * Create a null-terminated text view of the given buffer. If the buffer has a null terminator, it is viewed directly.
			 * Otherwise, a copy of the string is allocated with an extra byte for the null terminator.
			 * 
			 * @param buffer The text buffer
			 * @param len The length of the text buffer
			 */
			static Text viewNullTerminated(const char *buffer, std::size_t len);

			/**
			 * Creates an allocated copy of the given null-terminated C string.
			 *
			 * @param text The text to copy
			 * @return The duplicated text
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			static Text duplicate(const char *text);

			/**
			 * Creates an allocated copy of the given text buffer.
			 *
			 * @param text The text to copy
			 * @param length The number of text characters to copy
			 * @return The duplicated text
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			static Text duplicate(const char *text, std::size_t length);

			/**
			 * Creates an allocated copy of the given C++ string.
			 * 
			 * @param text The text to copy
			 * @return The duplicated text
			 * @throw std::bad_alloc If the memory could not be allocated
			 */
			static Text duplicate(const std::string &text);

			/**
			 * Gets the given text without any leading or trailing whitespace or unprintable characters.
			 * 
			 * @param text The text to trim
			 * @return The trimmed text
			 */
			static std::string trim(const char *text);

			/**
			 * Gets the given text without any leading or trailing whitespace or unprintable characters.
			 *
			 * @param text The text to trim
			 * @return The trimmed text
			 */
			static std::string trim(const std::string &text);

			/**
			 * Gets the given text without any leading or trailing whitespace or unprintable characters.
			 *
			 * @param text The text to trim
			 * @return The trimmed text
			 */
			static std::string trim(Text text) noexcept;

			/**
			 * Construct the null text.
			 */
			Text() noexcept;

			/**
			 * Constructs a text that views the given null-terminated C string.
			 * The length will include the null terminator.
			 * This does not take ownership of the text memory.
			 *
			 * @param cstr The C string to view
			 */
			Text(const char *cstr) noexcept;

			/**
			 * Constructs a text that references the given null-terminated C string literal.
			 * The length will include the null terminator.
			 * This does not take ownership of the text memory.
			 *
			 * @param cstr The C string to view
			 */
			template <std::size_t N>
			Text(const char (&cstr)[N]) noexcept :
				mElements(cstr),
				mCapacity(N),
				mFactory(nullptr)
			{
				// nothing more to do
			}			

			/**
			 * Constructs a text that views the given character sequence.
			 * The sequence does not need to be null terminated.
			 * This does not take ownership of the text memory.
			 *
			 * @param str The pointer to the character sequence start to duplicate
			 * @param length The number of characters to reference
			 */
			Text(const char *str, std::size_t length);

			/**
			 * Construct a copy of the given std::string.
			 * This does not affect the std::string in any way, and creates a copy unique to this Text.
			 *
			 * @param text The text to reference
			 */
			Text(const std::string &text) noexcept;

			/**
			 * Construct a copy of the givenFixed Text.
			 * This does not affect the input text in any way, but this method will have its own allocated copy.
			 *
			 * @tparam N The text length
			 * @param text The text to copy
			 */
			template <std::size_t N>
			Text(const FixedText<N> &in) :
				mElements(in.data()),
				mCapacity(N),
				mFactory(nullptr)
			{
				this->internalize();
			}

			/**
			 * Adopts null-terminated text that was allocated using the given allocator.
			 * The null terminator byte is included in the adopted size.
			 * If the allocator is empty, the text is viewed without owning it.
			 * 
			 * @param cstr The C string to adopt
			 * @param allocator The allocator to use, or nullptr to just view the text
			 */
			Text(char *cstr, Factory<char> allocator) noexcept;

			/**
			 * Adopts the given character sequence as this text that was allocated using the given allocator.
			 * The sequence does not need to be null terminated.
			 * If the allocator is empty, the text is viewed without owning it.
			 *
			 * @param str the pointer to the character sequence start to duplicate
			 * @param length The number of characters to reference
			 * @param allocator The allocator to use, or nullptr to just view the text
			 */
			Text(char *str, std::size_t length, Factory<char> allocator) noexcept;

			/**
			 * Construct a copy of text. This creates a view of the text and
			 * does not duplicate it.
			 *
			 * @param in The text to reference
			 */
			Text(const Text &in);

			/**
			 * Construct text by move transfer.
			 *
			 * @param in The text to move
			 */
			Text(Text &&in) noexcept;

			/**
			 * Copies text. This duplicates text into new memory.
 			 * This creates a view of the text and does not duplicate it.
			 *
			 * @param in The text to reference
			 * @return This text
			 */
			Text &operator=(const Text &in);

			/**
			 * Moves text.
			 *
			 * @param in The text to move
			 * @erturn This text
			 */
			Text &operator=(Text &&in) noexcept;

			/**
			 * Destructor. Deallocates the text memory if owned.
			 */
			~Text() noexcept;

			/**
			 * Clears the text. Deallocates the text memory if owned.
			 */
			void clear() noexcept;

			/**
			 * Checks to see whether the text is currently null-terminated.
			 * If true, the text data pointer can be used as a C-style string.
			 *
			 * @return Whether the text is null-terminated
			 */
			bool isNullTerminated() const noexcept;

			/**
			 * Gets the number of character bytes allocated to the text, including any null terminators if present.
			 * This represents how much memory is viewed or allocated.
			 *
			 * @return The text size in character bytes
			 */
			std::size_t capacity() const noexcept;

			/**
			 * Checks whether the text is the empty string.
			 * This is true both if it is null (no bytes) or if the first byte is the C-style null terminator.
			 * 
			 * @return Whether the text is null or empty by C-style string rules
			 */
			bool empty() const noexcept;

			/**
			 * Checks whether the text has zero bytes and is just the null pointer.
			 * 
			 * @return Whether the text is null 
			*/
			bool null() const noexcept;

			/**
			 * Gets the size of the text.
			 * This is the number of characters in the text as conventionally interpreted up to the first null terminator,
			 * or the allocated text capacity if no null terminator is present.
			 *
			 * @return The length of the text
			 */
			std::size_t size() const noexcept;

			/**
			 * Gets the character at the given offset.
			 *
			 * @param offset The offset
			 * @return The character
			 */
			const char &operator[](std::size_t offset) const noexcept;

			/**
			 * Gets the codepoint for the UTF-8 sequence that starts at the given offset.
			 * This will return 0 if the UTF-8 sequence is invalid or incomplete.
			 * 
			 * @param offset The offset in the text to start parsing the codepoint
			 * @return The Unicode codepoint or 0 if invalid or incomplete
			 */
			Character codepoint(std::size_t offset) const noexcept;

			/**
	 		 * Gets the first character.
			 *
			 * @return The first character
			 */
			const char &front() const noexcept;

			/**
			 * Gets the last character.
			 * This is often the C null terminator.
			 *
			 * @return The final character
			 */
			const char &back() const noexcept;

			/**
			 * Gets the data pointer to the text.
			 * Note that the text might not actually be null-terminated so use nullTerminate() first if you want to ensure the data pointer
			 * is usable with C-style text APIs. This method returns the data pointer regardless of null termination.
			 *
			 * @return The text data pointer
			 */
			const char *data() const noexcept;

			/**
			 * Gets the C-style pointer to the text.
			 * Note that the text might not actually be null-terminated so use nullTerminate() first if you want to ensure the data pointer
			 * is usable with C-style text APIs. If the text is not null-terminator, this returns nullptr for safety.
			 *
			 * @return The text data pointer or nullptr if not null-terminator
			 */
			const char *c_str() const noexcept;

			/**
			 * Gets the iterator to the beginning of the text, which also happens to be the data pointer.
			 *
			 * @return The begin iterator
			 */
			const char *begin() const noexcept;

			/**
			 * Gets the iterator at the first null terminator in the text, or the allocation limit iterator if none was present.
			 *
			 * @return The iterator to the end of the C-style string
			 */
			const char *end() const noexcept;

			/**
			 * Gets the iterator to the allocation limit of the text, which also happens to be a pointer to after the end of the data.
			 *
			 * @return The end iterator
			 */
			const char *limit() const noexcept;

			/**
			 * Sets up this Text object to reference the given null-terminated C-style text without copying.
			 * The size of the Text will include the null-terminator.
			 *
			 * @param text The text to reference
			 */
			void bind(const char *text) noexcept;

			/**
			 * Sets up this Text object to reference the given std::string without copying.
			 * The size of the Text will include the null-terminator.
			 *
			 * @param text The text to reference
			 */
			void bind(const std::string &text) noexcept;

			/**
			 * Sets up this Text object to reference the given temporary std::string by copying it.
			 * The size of the Text will include the null-terminator.
			 *
			 * @param text The text to copy
			 */
			void bind(std::string &&text);

			/**
			 * Sets up this Text object to reference the given text without copying.
			 *
			 * @param text The text to reference
			 * @param length The number of character bytes in the text
			 */
			void bind(const char *text, std::size_t length) noexcept;

			/**
			 * Sets up this Text object to reference the given text without copying and to use the given allocator to deallocate it.
			 *
			 * @param text The text to reference
			 * @param length The number of character bytes in the text
			 * @param allocator The allocator to use to deallocate
			 */
			void bind(char *text, std::size_t length, Factory<char> allocator) noexcept;

			/**
			 * Allocates the given number of bytes for the text.
			 * The returned allocated character pointer may be used to initialize the text.
			 * Note that the capacity must explicitly include the null terminator if one is desired,
			 * and that an allocation request of 0 will deallocate the text.
			 * 
			 * @param capacity The requested number of bytes
			 * @return The allocated character array
			 */
			char *allocate(std::size_t capacity);

			/**
			 * Gets the allocator currently in use if any.
			 *
			 * @return The allocator currently in use
			 */
			Factory<char> allocator() const noexcept;

			/**
			 * Ensures that this text is null-terminated appropriately for C-style APIs.
			 * If the text is not currently null-terminated, a new copy is allocated with one additional byte for the null terminator.
			 *
			 * @return True if a new null terminator was added, false if one was already present
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			bool nullTerminate();

			/**
			 * Creates a view of the text that is null terminator.
			 * If the current text is already null terminator, a non-owning view of it is returned.
			 * Otherwise, a new text is allocated with the null terminator.
			 * 
			 * @return The viewed null-terminated text
			 */
			Text viewNullTerminated() const;

			/**
			 * Ensures that this Text has its own unique copy of the text data.
			 * If this Text is not currently the owner of the data, a new copy is allocated and the existing string is copied into it.
			 *
			 * @return True if a new text data buffer was allocated, false if this text is already the owner of the text data
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			bool internalize();

			/**
			 * Sets up this Text to own a copy of the given C-style null-terminated text data.
			 * The Text size will include the null terminator byte.
			 *
			 * @param text The C-style null-terminated text to copy
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			Text &internalize(const char *text);

			/**
			 * Sets up this Text to own a copy of the given text data.
			 *
			 * @param text The text data pointer
			 * @param length The number of bytes in the text data
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			Text &internalize(const char *text, std::size_t length);

			/**
			 * Sets up this Text to own a copy of the given text data.
			 * The Text size will include the null terminator byte.
			 *
			 * @param text The text to copy
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			Text &internalize(const std::string &text);

			/**
			 * Sets up this Text to own a copy of the given text data.
			 *
			 * @param text The text to copy
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 */
			Text &internalize(const Text &text);

			/**
			 * Sets up this Text to own the given moved text data.
			 * If the input text is allocated already, then allocator and text is simply moved.
			 * Otherwise it is duplicated with a new allocator.
			 *
			 * @param text The text to copy
			 * @throw std::bad_alloc If memory was needed but could not be allocated
			 * @return A reference to this text for convenience
			 */
			Text &internalize(Text &&text);
			
			/**
			 * Sets the current text to an internalized copy of the given text if it is not currently set.
			 * This does nothing if the current text already has a value.
			 *
			 * @param text The text to merge
			 * @return A reference to this text for convenience
			 */
			Text &merge(const Text &text);
		
			/**
			 * Sets the current text to internalize the given moved text if it is not currently set.
			 * This does nothing if the current text already has a value.
			 *
			 * @param text The text to merge
			 * @return A reference to this text for convenience
			 */
			Text &merge(Text &&text);

			/**
			 * Gets a Text object that is a reference to this current Text object without copying it.
			 *
			 * @return The Text referencing this Text
			 */
			Text view() const noexcept;

			/**
			 * Gets a Text object that is a copy of this current Text object.
			 *
			 * @return The Text copy
			 */
			Text duplicate() const;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compare(const char *text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compare(const std::string &text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compare(const Text &text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compareInsensitive(const char *text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compareInsensitive(const std::string &text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The text to compare
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int compareInsensitive(const Text &text) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order with specified case rules, returned as a three-way comparison.
			 *
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param mode The case rule to use for this comparison
			 * @return The three-way comparison result of the two texts following the given case rule
			 */
			int compare(const char *text, Case mode) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order with specified case rules, returned as a three-way comparison.
			 *
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param mode The case rule to use for this comparison
			 * @return The three-way comparison result of the two texts following the given case rule
			 */
			int compare(const std::string &text, Case mode) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order with specified case rules, returned as a three-way comparison.
			 *
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param mode The case rule to use for this comparison
			 * @return The three-way comparison result of the two texts following the given case rule
			 */
			int compare(const Text &text, Case mode) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const char *text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const std::string &text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order preserving text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const Text &text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefixInsensitive(const char *text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefixInsensitive(const std::string &text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order ignoring text case, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefixInsensitive(const Text &text, std::size_t maxLength) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order using the given case rules, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *			 
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @param mode The case mode to do the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const char *text, std::size_t maxLength, Case mode) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order using the given case rules, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *			 
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @param mode The case mode to do the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const std::string &text, std::size_t maxLength, Case mode) const noexcept;
			
			/**
			 * Compare two text strings for alphabetic sort order using the given case rules, returned as a three-way comparison.
			 * This method only considers the first maxLength bytes prefix.
			 *			 
			 * If the case is Case::Sensitive, the comparison is done exactly with case.
			 * Otherwise for all other Case values the comparison ignores case.
			 *
			 * A negative value indicates the this text is before the given text.
			 * A positive value indicates the this text is after the given text.
			 * A zero value indicates the two texts are equal.
			 *
			 * @param text The second text
			 * @param maxLength The maximum number of bytes to consider in the comparison
			 * @param mode The case mode to do the comparison
			 * @return The three-way comparison result of the two texts ignoring case
			 */
			int comparePrefix(const Text &text, std::size_t maxLength, Case mode) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const char *text) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const std::string &text) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const Text &text) const noexcept;

			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The UTF-16 text to compare
			 * @param length The maximum UTF-16 character count to consider
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const char16_t *text, std::size_t length = SIZE_MAX) const noexcept;

			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The Unicode text to compare
			 * @param length The maximum Unicode character count to consider
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const char32_t *text, std::size_t length = SIZE_MAX) const noexcept;

			/**
			 * Checks to see whether two text strings are equal while ignoring case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equalInsensitive(const char *text) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while ignoring case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equalInsensitive(const std::string &text) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while ignoring case.
			 *
			 * @param text The text to compare
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equalInsensitive(const Text &text) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal according to given case rules.
			 *
			 * @param text The text to compare
			 * @param mode The case mode
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const char *text, Case mode) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The text to compare
			 * @param mode The case mode
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const std::string &text, Case mode) const noexcept;
			
			/**
			 * Checks to see whether two text strings are equal while preserving case.
			 *
			 * @param text The text to compare
			 * @param mode The case mode
			 * @return Whether the two texts are equal while ignoring case
			 */
			bool equal(const Text &text, Case mode) const noexcept;
			
			/**
			 * Gets a Text object that is a reference to the first off bytes prefix of this Text without copying it.
			 *
			 * @param off The number of bytes to include in the prefix
			 * @return The Text reference to the prefix
			 */
			Text prefix(std::size_t off) const noexcept;

			/**
			 * Gets a Text object that is a reference to the last off bytes suffix of this Text without copying it.
			 *
			 * @param off The number of bytes to include in the suffix
			 * @return The Text reference to the suffix
			 */
			Text suffix(std::size_t off) const noexcept;

			/**
			 * Gets a Text object that is a reference to count bytes starting at off from this Text without coyping it.
			 *
			 * @param off The first byte to include in the reference
			 * @param count The number of bytes ot include in the reference
			 * @return The Text reference to the substring
			 */
			Text substr(std::size_t off, std::size_t count) const noexcept;
			
			/**
			 * Gets the portion of this text without leading or trailing
			 * white space. A reference to this text is returned.
			 *
			 * @return A reference to this text without leading/trailing spaces
			 */
			Text trim() const noexcept;
			
			/**
			 * Gets the portion of this text which omits a prefix of the given character, if present.
			 *
			 * @param c The character to skip
			 * @param count The number of instances to skip, or SIZE_MAX to skip all
			 * @return A reference to this text without the character prefix
			 */
			Text trimPrefix(char c, std::size_t count = SIZE_MAX) const noexcept;
			
			/**
			 * Gets the portion of this text which omits a suffix of the given character, if present.
			 *
			 * @param c The character to skip
			 * @param count The number of instances to skip, or SIZE_MAX to skip all
			 * @return A reference to this text without the character suffix
			 */
			Text trimSuffix(char c, std::size_t count = SIZE_MAX) const noexcept;
			
			/**
			 * Gets a lower-case representation of the same text.
			 *
			 * @return The lowest case text
			 * @throw std::bad_alloc If memory was needed but couldn't be allocated
			 */
			std::string toLower() const;
			
			/**
			 * Gets an upper-case representation of the same text.
			 *
			 * @return The upper case text
			 * @throw std::bad_alloc If memory was needed but couldn't be allocated
			 */
			std::string toUpper() const;
			
			/**
			 * Gets a representation of the same text with the given case.
			 *
			 * @param c The desired case of the text
			 * @return The text converted to the given case
			 * @throw std::bad_alloc If memory was needed but couldn't be allocated
			 */
			std::string toCase(Case c) const;

			/**
			 * Finds the position index of the first instance of the given character relative
			 * to the beginning of the text starting at the given offset.
			 *
			 * @param c The character to find
			 * @param off The first position to search
			 * @return The first position of the found character at or after the search position
			 * or std::string::npos if not found
			 */
			std::size_t find(char c, std::size_t off = 0) const noexcept;

			/**
			 * Finds the position index of the first instance of the given text relative
			 * to the beginning of this text starting at the given offset.
			 *
			 * @param text The text to find
			 * @param off The first position to search
			 * @return The first position of the found text at or after the search position
			 * or std::string::npos if not found
			 */
			std::size_t find(const Text &text, std::size_t off = 0) const noexcept;

			/**
			 * Finds the position index of the last instance of the given character relative
			 * to the beginning of the text.
			 *
			 * @param c The character to find
			 * @param off The maximum position to search
			 * @return The last position of the found character,
			 * or std::string::npos if not found
			 */
			std::size_t rfind(char c, std::size_t off = std::string::npos) const noexcept;

			/**
			 * Checks to see if the text contains the given character.
			 *
			 * @param c The character
			 * @return Whether it was found
			 */
			bool contains(char c) const noexcept;
			
			/**
			 * Checks to see if the text contains the given character ignoring case.
			 *
			 * @param c The character
			 * @return Whether it was found
			 */
			bool containsInsensitive(char c) const noexcept;
		
			/**
			 * Checks to see if the text contains the given character using the given case rules.
			 *
			 * @param c The character
			 * @param mode The case rules
			 * @return Whether it was found
			 */
			bool contains(char c, Case mode) const noexcept;

			/**
			 * Gets a referenced view of the prefix of this string up to the first instance of the given character.
			 * If the character was not found, the entire string is considered the prefix.
			 * 
			 * @param c The character to find
			 * @return A reference to the text portion before the found character, or the whole string if it was not found
			 */
			Text splitBefore(char c) const noexcept;

			/**
			 * Gets a referenced view of the prefix of this string up to the first instance of the given text.
			 * If the text was not found, the entire string is considered the prefix.
			 * If the text was empty, the prefix is also empty.
			 *
			 * @param t The text to find
			 * @return A reference to the text portion before the found character, or the whole string if it was not found
			 */
			Text splitBefore(const Text &t) const noexcept;

			/**
			 * Gets a referenced view of the prefix of this string up to the given suffix.
			 * If the given character is not the suffix, the empty string is returned indicate it wasn't matched.
			 *
			 * @param c The character to find as the suffix
			 * @return A reference to the text portion before the suffix, or the empty string if the character wasn't the suffix
			 */
			Text splitBeforeSuffix(char c) const noexcept;

			/**
			 * Gets a referenced view of the prefix of this string up to the given suffix.
			 * If the given text is not the suffix, the empty string is returned indicate it wasn't matched.
			 * If the given text is empty, the entire string is returned.
			 *
			 * @param t The text to find as the suffix
			 * @return A reference to the text portion before the suffix, or the empty string if the Text wasn't the suffix
			 */
			Text splitBeforeSuffix(const Text &t) const noexcept;

			/**
			 * Gets a referenced view of the suffix of this string after the first instance of the given character.
			 * If the character was not found, the suffix is considered empty.
			 *
			 * @param c The character to find
			 * @return A reference to the text portion before the found character, or the empty string if it was not found
			 */
			Text splitAfter(char c) const noexcept;

			/**
			 * Gets a referenced view of the suffix of this string after the given prefix.
			 * If the given character is not the prefix, the empty string is returned indicate it wasn't matched.
			 *
			 * @param c The character to find as the prefix
			 * @return A reference to the text portion after the prefix, or the empty string if the character wasn't the prefix
			 */
			Text splitAfterPrefix(char c) const noexcept;

			/**
			 * Gets a referenced view of the suffix of this string after the given prefix.
			 * If the given text is not the prefix, the empty string is returned indicate it wasn't matched.
			 * If the given text is empty, the entire string is returned.
			 *
			 * @param t The text to find as the prefix
			 * @return A reference to the text portion before the prefix, or the empty string if the Text wasn't the prefix
			 */
			Text splitAfterPrefix(const Text &t) const noexcept;

			/**
			 * Gets a referenced view of the suffix of this string after the end of the first instance of the given text.
			 * If the text was not found, the suffix is considered empty.
			 * If the text is empty, the entire string is considered the suffix.
			 *
			 * @param t The text to find
			 * @return A reference to the text portion before the found character, or the empty string if it was not found
			 */
			Text splitAfter(const Text &t) const noexcept;

			/**
			 * Gets two referenced views of the string before and after the first instance of the given character.
			 * If the character is not found, the entire string is considered before and after is empty.
			 *
			 * @param c The character to find
			 * @return A pair containing the text before the split and the text after the spit
			 */
			std::pair<Text, Text> split(char c) const noexcept;

			/**
			 * Gets two referenced views of the string before and after the first instance of the given text.
			 * If the text is not found, the entire string is considered before and after is empty.
			 *
			 * @param t The text to find
			 * @return A pair containing the text before the split and the text after the spit
			 */
			std::pair<Text, Text> split(const Text &t) const noexcept;

			/**
			 * Replaces all instances of the input character with the output character.
			 * If the input character is not found, a read-only view of the current text is returned.
			 * Otherwise new text is allocated to perform the replacement.
			 * 
			 * @param search The character to find
			 * @param replace The character to replace the found character with
			 * @return The text with replacement
			 */
			Text replace(char search, char replace) const;
			
			/**
			 * Creates a std::string copy of this text.
			 *
			 * @return The string copy
			 * @throw std::bad_alloc If memory for the string could not be allocated
			 */
			std::string str() const;

			/**
			 * Gets the printed length of the text.
			 * 
			 * @return The number of UTF-8 characters in the text up to the first null terminator if present
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints to a returned UTF-8 string by returning itself.
			 * 
			 * @return This text
			 */
			const Text &print() const noexcept;

			/**
			 * Prints to a UTF-8 string buffer.
			 *
			 * @param buffer The buffer to print to
			 * @param capacity THe capacity of the buffer
			 * @return The length of this text to be printed
			 */
			std::size_t print(char *buffer, std::size_t count) const noexcept;

			/**
			 * Gets a byte swapped view of this text. Since this text is UTF-8 no bytes are actually
			 * swapped and a view of this unmodified text is returned.
			 * 
			 * @return This text
			 */
			const Text &swapBytes() const noexcept;

			/**
			 * Applies a byte swap to this text. This does nothing since the text is UTF-8.
			 * 
			 * @return This text
			 */
			Text &applyByteSwap() noexcept;

			/**
			 * Checks to see if this text starts with the given character.
			 *
			 * @param prefix The desired prefix
			 * @return Whether this text starts with the given prefix
			 */
			bool startsWith(char prefix) const noexcept;

			/**
			 * Checks to see if this text starts with the given prefix.
			 * This is always true if the prefix is empty.
			 * 
			 * @param prefix The desired prefix
			 * @return Whether this text starts with the given prefix
			 */
			bool startsWith(const Text &prefix) const noexcept;

			/**
			 * Checks to see if this text ends with the given character.
			 * This does not considered a trailing null terminator if present.
			 *
			 * @param suffix The desired suffix
			 * @return Whether this text ends with the given suffix
			 */
			bool endsWith(char suffix) const noexcept;

			/**
			 * Checks to see if this text starts with the given suffix.
			 * This is always true if the suffix is empty.
			 *
			 * @param suffix The desired suffix
			 * @return Whether this text ends with the given suffix
			 */
			bool endsWith(const Text &suffix) const noexcept;

			/**
			 * Casts to a std::string copy of this text.
			 *
			 * @return The string copy
			 * @throw std::bad_alloc If memory for the string could not be allocated
			 */
			operator std::string() const;

			/**
			 * Casts to a character pointer to this text.
			 *
			 * @warning the text might not be null-terminated, call
			 * nullTerminate() first to ensure it is usable with non-CIO
			 * users
			 *
			 * @return The character pointer
			 */
			explicit operator const char *() const noexcept;

			/**
			 * Casts to FixedText<N>.
			 *
			 * @return The fixed text
			 */
			template <std::size_t N>
			inline explicit operator FixedText<N>() const noexcept
			{
				return FixedText<N>(mElements, mCapacity);
			}

			/**
			 * Interprets the text in a Boolean context.
			 * It is considered true if it is not empty.
			 *
			 * @return Whether the text is not empty
			 */
			explicit operator bool() const noexcept;

		private:
			/** Metaclass for this class */
			static Class<Text> sMetaclass;
		
			/** Preallocated space for the empty null-terminated text */
			static const char sEmptyText;

			/** The character bytes **/
			const char *mElements = nullptr;

			/** The number of character byte elements **/
			std::size_t mCapacity = 0;

			/** The allocator used if any */
			Factory<char> mFactory;
	};

	/**
	 * Concatenates two Text instances into a new Text instance that owns the allocated concatenation.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return The Text containing the sequence of left followed by right
	 * @throw std::bad_alloc If memory was needed but could not be allocated
	 */
	CIO_API Text operator+(const Text &left, const Text &right);

	/**
	 * Concatenates two Text instances, storing the result in the first Text instance.
	 *
	 * @param left The first text, which is replaced with the concatenation
	 * @param right The second text
	 * @return A reference to the first text object
	 * @throw std::bad_alloc If memory was needed but could not be allocated
	 */
	CIO_API Text &operator+=(Text &left, const Text &right);

	/**
	 * Prints a Text object to a C++ stream. This will work correctly regardless of whether the text is null-terminated.
	 *
	 * @param s The stream
	 * @param text The text to print
	 * @return The stream
	 * @throw std::exception if the stream threw any exceptions on failure
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, const Text &text);

	/**
	 * Compare two text strings for equality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	CIO_API bool operator==(const Text &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for equality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	CIO_API bool operator==(const char *left, const Text &right) noexcept;

	/**
	 * Compare two text strings for equality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	CIO_API bool operator==(const Text &left, const char *right) noexcept;

	/**
	 * Compare two text strings for equality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	CIO_API bool operator==(const std::string &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for equality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	CIO_API bool operator==(const Text &left, const std::string &right) noexcept;

	/**
	 * Compare two text strings for inequality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	CIO_API bool operator!=(const Text &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for inequality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	CIO_API bool operator!=(const char *left, const Text &right) noexcept;

	/**
	 * Compare two text strings for inequality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	CIO_API bool operator!=(const Text &left, const char *right) noexcept;

	/**
	 * Compare two text strings for inequality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	CIO_API bool operator!=(const std::string &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for inequality.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	CIO_API bool operator!=(const Text &left, const std::string &right) noexcept;

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is before the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically before the second text
	 */
	CIO_API bool operator<(const Text &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is before or equal to the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically before or equal to the second text
	 */
	CIO_API bool operator<=(const Text &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is after the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically after the second text
	 */
	CIO_API bool operator>(const Text &left, const Text &right) noexcept;

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is after or equal to the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically after or equal to the second text
	 */
	CIO_API bool operator>=(const Text &left, const Text &right) noexcept;
	
	/**
	 * The SortTextCaseSensitive class is a functor object that can be used as a template parameter
	 * for things like std::map and std::set to perform string comparison with case sensitivity.
	 */
	class CIO_API SortTextCaseSensitive
	{
		public:
			/**
			 * Checks whether two C-style null-terminated strings are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const char *left, const char *right) const noexcept;

			/**
			 * Checks whether two C++ strings are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const std::string &left, const std::string &right) const noexcept;

			/**
			 * Checks whether two texts are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const Text &left, const Text &right) const noexcept;
	};

	/**
	 * The SortTextCaseInsensitive class is a functor object that can be used as a template parameter
	 * for things like std::map and std::set to perform string comparison without case sensitivity.
	 */
	class CIO_API SortTextCaseInsensitive
	{
		public:
			/**
			 * Checks whether two C-style null-terminated strings are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const char *left, const char *right) const noexcept;

			/**
			 * Checks whether two texts are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const Text &left, const Text &right) const noexcept;
	};

	/**
	 * The SortTextCaseOptional class is a functor object that can be used as a template parameter
	 * for things like std::map and std::set to perform string comparison with case sensitivity being a configurable runtime option.
	 */
	class CIO_API SortTextCaseOptional
	{
		public:
			/**
			 * Construct the sorter with the default mode of Case::Sensitive.
			 */
			SortTextCaseOptional() noexcept;

			/**
			 * Construct the sorter with the given case mode.
			 *
			 * @param mode The case mode
			 */
			SortTextCaseOptional(Case mode) noexcept;

			/**
			 * Checks whether two C-style null-terminated strings are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const char *left, const char *right) const noexcept;

			/**
			 * Checks whether two texts are in the right sort order.
			 *
			 * @param left The first string
			 * @param right The second string
			 * @return Whether the first string is before the second string
			 */
			bool operator()(const Text &left, const Text &right) const noexcept;

			/**
			 * Gets the current case mode.
			 *
			 * @return The current case mode
			 */
			Case getCaseMode() const noexcept;

			/**
			 * Sets the current case mode.
			 *
			 * @param mode The case mode to set
			 */
			void setCaseMode(Case mode) noexcept;

		private:
			/** The current case mode */
			Case mMode;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
