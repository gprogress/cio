/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ORDER_H
#define CIO_ORDER_H

#include "Types.h"
#include "Metatypes.h"

#include <cstring>
#include <iosfwd>

/** Define value to specify big-endian system */
#define CIO_BIG_ENDIAN 0x00010203

/** Define value to specify little-endian system */
#define CIO_LITTLE_ENDIAN 0x0302010

/** Define value to specify that system endianness is unknown and must be detected at runtime */
#define CIO_RUNTIME_ENDIAN 0

// Endianness is a CPU property
// Little-endian: check for common X86 macros
#if defined(__X86__) || defined(__i386__) || defined (_M_IX86)
#define CIO_ENDIAN CIO_LITTLE_ENDIAN

// Little-endian: check for common X86-64 macros
#elif defined(__x86_64__) || defined(_M_X64)
#define CIO_ENDIAN CIO_LITTLE_ENDIAN

// Little-endian: check for ARM and MIPS EL variants
#elif defined(__ARMEL__) || defined(__MIPSEL__)
#define CIO_ENDIAN CIO_LITTLE_ENDIAN

// Big-endian: check for ARM and MIPS EB variants
#elif defined(__ARMEB__) || defined (__MIPSEB__)
#define CIO_ENDIAN CIO_BIG_ENDIAN

// Big-endian: check for common PowerPC macros
#elif defined (__ppc__)
#define CIO_ENDIAN CIO_BIG_ENDIAN

// If it's something we don't know about, see if __BYTE_ORDER__ is set
// Newer GCC versions (and hopefully) other compilers may set this
#elif defined __BYTE_ORDER__
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define CIO_ENDIAN CIO_LITTLE_ENDIAN
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define CIO_ENDIAN CIO_BIG_ENDIAN
#else
#error "No host byte order detection available on this platform"
#endif

// No recognized information on CPU, punt to runtime detection
#else
#define CIO_ENDIAN CIO_RUNTIME_ENDIAN

#endif

#if CIO_ENDIAN == CIO_BIG_ENDIAN
#define CIO_NATIVE_BYTE_ORDER Big
#elif  CIO_ENDIAN == CIO_LITTLE_ENDIAN
#define CIO_NATIVE_BYTE_ORDER Little
#else
#error "No host byte order detection available on this platform"
#endif

#if defined __GNUC__
/** Define that marks that we have the GCC (and compatible) family of _builtin_bswap intrinsics */
#define CIO_HAVE_BUILTIN_BSWAP
#endif

#if defined _MSC_VER
#define CIO_HAVE_UCRT_BYTESWAP
#endif

#ifndef CIO_GLOBAL_SWAPBYTES
/**
 * To work with argument-dependent namespace lookup in template contexts such as cio::Buffer, the overloads for 
 * swapBytes and applyByteSwap for primitives need to be in the global namespace and will be aliased into the cio namespace.
 * Howver, this can conflict with any other library or application which may have done the same thing.
 * To disable this functionality and only have swapBytes and applyByteSwap in the cio namespace, define CIO_GLOBAL_SWAPBYTES to 0.
 */
#define CIO_GLOBAL_SWAPBYTES 1
#endif

namespace cio
{
	/**
	 * The Order enumeration specifies the order for multi-byte primitives quantities on a CPU architecture.
	 */
	enum class Order : std::uint8_t
	{
		/** The byte order is unknown or not yet specified */
		Unknown = 0,

		/** The byte order is big-endian */
		Big = 1,

		/** The byte order is little-endian */
		Little = 2,

		/** The native host byte order is an alias to whichever byte order was detected at compile time */
		Host = CIO_NATIVE_BYTE_ORDER
	};

	/**
	 * Gets the text string representation of a byte order. Note that Host is an alias and will actually get either "Big" or "Little" as appropriate for the system.
	 *
	 * @param order The byte order
	 * @return the text string for the byte order
	 */
	CIO_API const char *print(Order order) noexcept;

	/**
	 * Prints the text string representation of a byte order. Note that Host is an alias and will actually get either "Big" or "Little" as appropriate for the system.
	 *
	 * @param order The byte order
	 * @param buffer The text buffer to print to
	 * @param capacity The capacity of the buffer
	 * @return the number of bytes actually needed to print
	 */
	CIO_API std::size_t print(Order order, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the text string representation of a byte order to a C++ standard output stream.
	 * Note that Host is an alias and will actually get either "Big" or "Little" as appropriate for the system.
	 *
	 * @param s The stream
	 * @param order The byte order
	 * @return the stream after printing
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Order order);

	/**
	 * The Big order compile time type is a compile-time constant for Big endian byte order.
	 *
	 * It provides a variety of methods for converting values and arrays to other byte orders in the most optimized way.
	 */
	class Big : public std::integral_constant<Order, Order::Big>
	{
		public:
		/**
		 * Changes the given value between host order and Big order.
		 *
		 * @tparam <T> The value type
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T order(T value) noexcept;

		/**
		 * Changes the given value between Big and Big order.
		 * It just returns the input.
		 *
		 * @tparam <T> The value type
		 * @param big The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Big big, T value) noexcept;

		/**
		 * Changes the given value between Big and Little order.
		 * It swaps the input bytes.
		 *
		 * @tparam <T> The value type
		 * @param little The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Little little, T value) noexcept;

		/**
		 * Changes the given value between Big and Host order.
		 *
		 * @tparam <T> The value type
		 * @param host The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Host host, T value) noexcept;

		/**
		 * Changes the given value between Big and given order.
		 *
		 * @tparam <T> The value type
		 * @param order The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Order order, T value) noexcept;

		/**
		 * Changes the given value between host order and Big order.
		 *
		 * @tparam <T> The value type
		 * @param value The value to reorder
		 */
		template <typename T>
		inline static void reorder(T &value) noexcept;

		/**
		 * Changes all array elements between Big and Host order.
		 * This does nothing if the host order is Big or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Big and Big order.
		 * This does nothing.
		 *
		 * @tparam <T> The array element type
		 * @param big The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Big big, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Big and Little order.
		 * This does nothing if the data type is not multi-byte, otherwise it swaps every array element.
		 *
		 * @tparam <T> The array element type
		 * @param little The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Little little, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Big and Host order.
		 * This does nothing if the host order is Big or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param host The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Host host, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Big and given order.
		 * This does nothing if the given order is Big or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param order The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Order order, T *data, std::size_t count) noexcept;

		/**
		 * Changes the given value between host order and Big order.
		 * This enables the Big order to be used as a functor.
		 *
		 * @tparam <T> The value type
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline T operator()(T value) const noexcept;
	};

	/**
	 * The Little compile time type is a compile-time constant for Little Endian byte order.
	 *
	 * It provides a variety of methods for converting values and arrays to other byte orders in the most optimized way.
	 */
	class Little : public std::integral_constant<Order, Order::Little>
	{
		public:
		/**
		 * Gets the given value converted between host order and Little order.
		 *
		 * @tparam <T> The value type
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T order(T value) noexcept;

		/**
		 * Changes the given value between Little and Big order.
		 * It swaps the input bytes.
		 *
		 * @tparam <T> The value type
		 * @param big The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Big big, T value) noexcept;

		/**
		 * Changes the given value between Little and Little order.
		 * It returns the input value.
		 *
		 * @tparam <T> The value type
		 * @param little The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Little little, T value) noexcept;

		/**
		 * Changes the given value between Little and Host order.
		 *
		 * @tparam <T> The value type
		 * @param host The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Host host, T value) noexcept;

		/**
		 * Changes the given value between Little and given order.
		 *
		 * @tparam <T> The value type
		 * @param order The other order
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline static T to(Order order, T value) noexcept;

		/**
		 * Changes the given value between host order and Little order.
		 *
		 * @tparam <T> The value type
		 * @param value The value to reorder
		 */
		template <typename T>
		inline static void reorder(T &value) noexcept;

		/**
		 * Changes all array elements between Little and Host order.
		 * This does nothing if the host order is Little or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Little and Big order.
		 * This does nothing if the data type is not multi-byte, otherwise it swaps every array element.
		 *
		 * @tparam <T> The array element type
		 * @param big The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Big big, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Little and Little order.
		 * This does nothing.
		 *
		 * @tparam <T> The array element type
		 * @param little The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Little little, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Little and Host order.
		 * This does nothing if the host order is Little or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param host The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Host host, T *data, std::size_t count) noexcept;

		/**
		 * Changes all array elements between Little and given order.
		 * This does nothing if the given order is Little or the data type is not multi-byte.
		 *
		 * @tparam <T> The array element type
		 * @param order The other byte order
		 * @param data The array
		 * @param count The number of array elements
		 */
		template <typename T>
		inline static void reorder(Order order, T *data, std::size_t count) noexcept;

		/**
		 * Changes the given value between host order and Little order.
		 *
		 * @tparam <T> The value type
		 * @param value The value
		 * @return the reordered value
		 */
		template <typename T>
		inline T operator()(T value) const noexcept;
	};

	/**
	 * The Host Byte order is a type that inherits from either Big or Little byte order
	 * depending on the host CPU.
	 */
	class Host : public CIO_NATIVE_BYTE_ORDER
	{
		// nothing to add - pull in everything from either Big or Little
	};

	/**
	 * Performs a runtime detection check of the actual byte order of the currently executing CPU.
	 *
	 * @return the detected runtime byte order
	 */
	inline Order detectRuntimeByteOrder() noexcept;

	/**
	 * Swaps the CPU byte order of the given type.
	 * 
	 * The general implementation will attempt to call a swapBytes method on type T.
	 * If this fails to compile, this overload is not considered but others might be due to SFINAE.
	 * 
	 * @tparam T the type to byte swap
	 * @return the swapped value
	 * @throw std::bad_alloc If the class requires allocating memory for a member but that failed
	 */
	template <typename T>
	inline auto swapBytes(const T &value) -> decltype(value.swapBytes());

	/**
	 * Applies a byte swap to the given value T in place without creating a copy.
	 * If this fails to compile, this overload is not considered but others might be due to SFINAE.
	 *
	 * @tparam T the type to byte swap
	 * @param value The value or object to swap in place
	 * @return a reference to the value after swapping it
	 */
	template <typename T>
	inline auto applyByteSwap(T &value) noexcept -> decltype(value.applyByteSwap());

	/**
	 * Swaps a 1 byte integer quantity.
	 * This simply returns the input value.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the value
	 */
	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 1> size) noexcept;

	/**
	 * Swaps a 2 byte integer quantity.
	 * This uses the best intrinsic on the target CPU, or inline shifts if none are available.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 2> size) noexcept;

	/**
	 * Swaps a 4 byte integer quantity.
	 * This uses the best intrinsic on the target CPU, or inline shifts if none are available.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 4> size) noexcept;

	/**
	 * Swaps a 4 byte float quantity.
	 * This copies the float to an integer, swaps that, then copies back.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	inline float swapBytesBuiltin(float value, std::integral_constant<std::size_t, 4> size) noexcept;

	/**
	 * Swaps an 8 byte integer quantity.
	 * This uses the best intrinsic on the target CPU, or inline shifts if none are available.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 8> size) noexcept;

	/**
	 * Swaps an 8 byte float quantity.
	 * This copies the float to an integer, swaps that, then copies back.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	inline double swapBytesBuiltin(double value, std::integral_constant<std::size_t, 4> size) noexcept;

	/**
	 * Swaps a binary quantity not covered by any of the built-in swaps.
	 * This uses a loop to reverse the bytes.
	 *
	 * @param value The value
	 * @param size The size to select this overload
	 * @return the swapped value
	 */
	template <typename T>
	inline T swapBytesBuiltin(T value, std::size_t size) noexcept;

	/**
	 * Swaps a primitive value in place.
	 * This delegates to swapBytesN and stores the result back in the input.
	 *
	 * @param value The value to swap in place
	 * @param size The size of the value
	 * @return a reference to the value after swapping
	 */
	template <typename T, typename S>
	inline T &applyByteSwapBuiltin(T &value, S size) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 *
	 * @tparam T The element type
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T, typename L>
	inline void swapArrayBytes(T *data, L count) noexcept;

	/**
	 * Byte swaps an opaque data array in place.
	 * This is overloaded to do nothing.
	 *
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename L>
	inline void swapArrayBytes(void *data, L count) noexcept;

	/**
	 * Byte swaps a bool data array in place.
	 * This is overloaded to do nothing.
	 *
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename L>
	inline void swapArrayBytes(bool *data, L count) noexcept;

	/**
	 * Byte swaps a char data array in place.
	 * This is overloaded to do nothing.
	 *
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename L>
	inline void swapArrayBytes(char *data, L count) noexcept;

	/**
	 * Byte swaps an unsigned char data array in place.
	 * This is overloaded to do nothing.
	 *
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename L>
	inline void swapArrayBytes(unsigned char *data, L count) noexcept;

	/**
	 * Byte swaps a signed char data array in place.
	 * This is overloaded to do nothing.
	 *
	 * @tparam L The count type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename L>
	inline void swapArrayBytes(signed char *data, L count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of no elements by doing nothing.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 0> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of a single element for performance.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 1> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of two elements for performance.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 2> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of three elements for performance.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 3> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of two elements for performance.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 4> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 *
	 * @tparam T The element type
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, SIZE_MAX> count) noexcept;

	/**
	 * Byte swaps an array of values in place.
	 * This overload implements a direct swap of a fixed number of elements by
	 * subdividing to other fixed count calls.
	 *
	 * @tparam T The element type
	 * @tparam N The number of array elements to swap
	 * @param data The array to swap element by element
	 * @param count The number of array elements to swap
	 */
	template <typename T, std::size_t N>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, N> count) noexcept;
}

// Items that need to be in global namespace for argument-dependent lookup to work right

#if !CIO_GLOBAL_SWAPBYTES
namespace cio
{
#endif
	/**
	 * Peforms a byte swap of a boolean value.
	 * These are always 1 byte so this returns the input.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr bool swapBytes(bool value) noexcept;

	/**
	 * Peforms a byte swap of a char value.
	 * These are always 1 byte so this returns the input.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr char swapBytes(char value) noexcept;

	/**
	 * Peforms a byte swap of an unsigned char value.
	 * These are always 1 byte so this returns the input.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr unsigned char swapBytes(unsigned char value) noexcept;

	/**
	 * Peforms a byte swap of an signed char value.
	 * These are always 1 byte so this returns the input.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value (usually the same as the input)
	 */
	constexpr signed char swapBytes(signed char value) noexcept;

	/**
	 * Peforms a byte swap of a short value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr short swapBytes(short value) noexcept;

	/**
	 * Peforms a byte swap of an unsigned short value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr unsigned short swapBytes(unsigned short value) noexcept;

	/**
	 * Peforms a byte swap of an int value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr int swapBytes(int value) noexcept;

	/**
	 * Peforms a byte swap of an unsigned int value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr unsigned swapBytes(unsigned value) noexcept;

	/**
	 * Peforms a byte swap of a long value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr long swapBytes(long value) noexcept;

	/**
	 * Peforms a byte swap of an unsigned long value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr unsigned long swapBytes(unsigned long value) noexcept;

	/**
	 * Peforms a byte swap of a long long value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr long long swapBytes(long long value) noexcept;

	/**
	 * Peforms a byte swap of an unsigned long long value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	constexpr unsigned long long swapBytes(unsigned long long value) noexcept;

	/**
	 * Peforms a byte swap of a float value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	inline float swapBytes(float value) noexcept;

	/**
	 * Peforms a byte swap of a double value.
	 *
	 * @param value The value to byte swap
	 * @return the byte swapped value
	 */
	inline double swapBytes(double value) noexcept;

	/**
	 * Performs a byte swap of a Boolean in place.
	 * 
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline bool &applyByteSwap(bool &value) noexcept;

	/**
	 * Performs a byte swap of a char in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline char &applyByteSwap(char &value) noexcept;

	/**
	 * Performs a byte swap of a signed char in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline signed char &applyByteSwap(signed char &value) noexcept;

	/**
	 * Performs a byte swap of an unsigned char in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline unsigned char &applyByteSwap(unsigned char &value) noexcept;

	/**
	 * Performs a byte swap of a signed short in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline short &applyByteSwap(short &value) noexcept;

	/**
	 * Performs a byte swap of an unsigned short in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline unsigned short &applyByteSwap(unsigned short &value) noexcept;

	/**
	 * Performs a byte swap of a signed int in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline int &applyByteSwap(int &value) noexcept;

	/**
	 * Performs a byte swap of an unsigned int in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline unsigned &applyByteSwap(unsigned &value) noexcept;

	/**
	 * Performs a byte swap of a signed long in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline long &applyByteSwap(long &value) noexcept;

	/**
	 * Performs a byte swap of an unsigned long in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline unsigned long &applyByteSwap(unsigned long &value) noexcept;

	/**
	 * Performs a byte swap of a signed long long in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline long long &applyByteSwap(long long &value) noexcept;

	/**
	 * Performs a byte swap of an unsigned long long in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline unsigned long long &applyByteSwap(unsigned long long &value) noexcept;

	/**
	 * Performs a byte swap of a float in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline float &applyByteSwap(float &value) noexcept;

	/**
	 * Performs a byte swap of a double in place.
	 *
	 * @param value The value to swap
	 * @return a reference to the swapped value
	 */
	inline double &applyByteSwap(double &value) noexcept;

#if !CIO_GLOBAL_SWAPBYTES
}
#else
namespace cio
{
	using ::swapBytes;
	using ::applyByteSwap;
}
#endif

/* Inline implementation */

namespace cio
{
	inline Order detectRuntimeByteOrder() noexcept
	{
		Order order;
		const char signature[4] = { 1, 2, 3, 4 };
		std::uint32_t value;
		std::memcpy(&value, signature, 4);

		switch (value)
		{
			case 0x01020304:
				order = Order::Big;
				break;

			case 0x04030201:
				order = Order::Little;
				break;

			default:
				order = Order::Unknown;
				break;
		}

		return order;
	}
}

// Built-in byte swap intrinsics, done using cio::std::integral_constant<std::size_t, N> to select width

namespace cio
{
	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 1> size) noexcept
	{
		return value;
	}

	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 2> size) noexcept
	{
#if defined CIO_HAVE_BUILTIN_BSWAP
		return static_cast<T>(__builtin_bswap16(static_cast<MatchingUnsigned<T>>(value)));
#elif defined CIO_HAVE_UCRT_BYTESWAP
		return static_cast<T>(_byteswap_ushort(static_cast<MatchingUnsigned<T>>(value)));
#else
		return ((value >> 8) & 0xFF) | (value << 8);
#endif
	}

	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 4> size) noexcept
	{
#if defined CIO_HAVE_BUILTIN_BSWAP
		return static_cast<T>(__builtin_bswap32(static_cast<MatchingUnsigned<T>>(value)));
#elif defined CIO_HAVE_UCRT_BYTESWAP
		return static_cast<T>(_byteswap_ulong(static_cast<MatchingUnsigned<T>>(value)));
#else
		return (((value >> 24) & 0x000000FF) |
			((value >> 8) & 0x0000FF00) |
			((value << 8) & 0x00FF0000) |
			(value << 24));
#endif
	}

	inline float swapBytesBuiltin(float value, std::integral_constant<std::size_t, 4> size) noexcept
	{
		std::uint32_t in;
		std::memcpy(&in, &value, 4);
		std::uint32_t swapped = swapBytesBuiltin(in, size);

		float out;
		std::memcpy(&out, &swapped, 4);
		return out;
	}

	template <typename T>
	constexpr T swapBytesBuiltin(T value, std::integral_constant<std::size_t, 8> size) noexcept
	{
#if defined CIO_HAVE_BUILTIN_BSWAP
		return static_cast<T>(__builtin_bswap64(static_cast<MatchingUnsigned<T>>(value)));
#elif defined CIO_HAVE_UCRT_BYTESWAP
		return static_cast<T>(_byteswap_uint64(static_cast<MatchingUnsigned<T>>(value)));
#else
		return (
			(value >> 56) & 0x00000000000000FF) |
			(value >> 40) & 0x000000000000FF00) |
			(value >> 24) & 0x0000000000FF0000) |
			(value >> 8) & 0x00000000FF000000) |
			(value << 8) & 0x000000FF00000000) |
			(value << 24) & 0x0000FF0000000000) |
			(value << 40) & 0x00FF000000000000) |
			(value << 56));
#endif
	}

	inline double swapBytesBuiltin(double value, std::integral_constant<std::size_t, 8> size) noexcept
	{
		std::uint64_t in;
		std::memcpy(&in, &value, 8);
		std::uint64_t swapped = swapBytesBuiltin(in, size);

		double out;
		std::memcpy(&out, &swapped, 8);
		return out;
	}

	template <typename T>
	inline T swapBytesBuiltin(T value, std::size_t size) noexcept
	{
		T swapped;
		std::uint8_t *output = reinterpret_cast<std::uint8_t *>(&swapped) + sizeof(T) - 1;
		const std::uint8_t *input = reinterpret_cast<const std::uint8_t *>(&input);
		for (std::size_t i = 0; i < sizeof(T); ++i)
		{
			*output-- = *input++;
		}
		return swapped;
	}

	template <typename T, typename S>
	inline T &applyByteSwapBuiltin(T &value, S size) noexcept
	{
		return (value = swapBytesBuiltin(value, size));
	}
}

#if !CIO_GLOBAL_SWAPBYTES
namespace cio
{
#endif

	constexpr bool swapBytes(bool value) noexcept
	{
		return value;
	}

	constexpr char swapBytes(char value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(char)>());
	}

	constexpr unsigned char swapBytes(unsigned char value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned char)>());
	}

	constexpr signed char swapBytes(signed char value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(signed char)>());
	}

	constexpr short swapBytes(short value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(short)>());
	}

	constexpr unsigned short swapBytes(unsigned short value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned short)>());
	}

	constexpr int swapBytes(int value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(int)>());
	}

	constexpr unsigned swapBytes(unsigned value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned)>());
	}

	constexpr long swapBytes(long value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(long)>());
	}

	constexpr unsigned long swapBytes(unsigned long value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned long)>());
	}

	constexpr long long swapBytes(long long value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(long long)>());
	}

	constexpr unsigned long long swapBytes(unsigned long long value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned long long)>());
	}

	inline float swapBytes(float value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(float)>());
	}

	inline double swapBytes(double value) noexcept
	{
		return cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(double)>());
	}

	inline bool &applyByteSwap(bool &value) noexcept
	{
		return value;
	}

	inline char &applyByteSwap(char &value) noexcept
	{
		return value;
	}

	inline signed char &applyByteSwap(signed char &value) noexcept
	{
		return value;
	}

	inline unsigned char &applyByteSwap(unsigned char &value) noexcept
	{
		return value;
	}

	inline short &applyByteSwap(short &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(short)>()));
	}

	inline unsigned short &applyByteSwap(unsigned short &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned short)>()));
	}

	inline int &applyByteSwap(int &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(int)>()));
	}

	inline unsigned &applyByteSwap(unsigned &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned)>()));
	}

	inline long &applyByteSwap(long &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(long)>()));
	}

	inline unsigned long &applyByteSwap(unsigned long &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned long)>()));
	}

	inline long long &applyByteSwap(long long &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(long long)>()));
	}

	inline unsigned long long &applyByteSwap(unsigned long long &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(unsigned long long)>()));
	}

	inline float &applyByteSwap(float &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(float)>()));
	}

	inline double &applyByteSwap(double &value) noexcept
	{
		return (value = cio::swapBytesBuiltin(value, std::integral_constant<std::size_t, sizeof(double)>()));
	}

#if !CIO_GLOBAL_SWAPBYTES
}
#endif

namespace cio
{
	// Generic methods - use SFINAE to detect if we can call the desired methods

	template <typename T>
	inline auto swapBytes(const T &value) -> decltype(value.swapBytes())
	{
		return value.swapBytes();
	}

	template <typename T>
	inline auto applyByteSwap(T &value) noexcept -> decltype(value.applyByteSwap())
	{
		return value.applyByteSwap();
	}

	// Big methods - host independent

	template <typename T>
	inline T Big::to(Big big, T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline T Big::to(Little little, T value) noexcept
	{
		return swapBytes(value);
	}
	
	template <typename T>
	inline T Big::to(Order order, T value) noexcept
	{
		return order == Order::Big ? value : swapBytes(value);
	}	
	
	template <typename T>
	inline void Big::reorder(Big big, T *data, std::size_t count) noexcept
	{
		// Big to big - do nothing
	}
	
	template <typename T>
	inline void Big::reorder(Little little, T *data, std::size_t count) noexcept
	{
		// Big to little - swap array
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline void Big::reorder(Order order, T *data, std::size_t count) noexcept
	{
		if (order != Order::Big)
		{
			swapArrayBytes(data, count);
		}
	}
	
	// Little methods - host independent
	
	template <typename T>
	inline T Little::to(Big big, T value) noexcept
	{
		return swapBytes(value);
	}
	
	template <typename T>
	inline T Little::to(Little little, T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline T Little::to(Order order, T value) noexcept
	{
		return order == Order::Little ? value : swapBytes(value);
	}
	
	template <typename T>
	inline void Little::reorder(Big big, T *data, std::size_t count) noexcept
	{
		// Little to big - swap
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline void Little::reorder(Little little, T *data, std::size_t count) noexcept
	{
		// Little to little - do nothing
	}
	
	template <typename T>
	inline void Little::reorder(Order order, T *data, std::size_t count) noexcept
	{
		if (order != Order::Little)
		{
			swapArrayBytes(data, count);
		}
	}
	
// Host order methods depend on whether we know if host is Big, host is Little, or if we need to detect
#if CIO_ENDIAN == CIO_BIG_ENDIAN

	// Big methods - host dependent, host is Big

	template <typename T>
	inline T Big::order(T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline T Big::to(Host host, T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline void Big::reorder(T &value) noexcept
	{
		// big to host big - do nothing
	}
	
	template <typename T>
	inline void Big::reorder(T *data, std::size_t count) noexcept
	{
		// big to host big - do nothing
	}
	
	template <typename T>
	inline static void Big::reorder(Host host, T *data, std::size_t count) noexcept
	{
		// big to host big - do nothing
	}
	
	template <typename T>
	inline T Big::operator()(T value) const noexcept
	{
		return value;
	}	
	
	// Little methods - host dependent, host is Big
	
	template <typename T>
	inline T Little::order(T value) noexcept
	{
		return swapBytes(value);
	}
	
	template <typename T>
	inline T Little::to(Host host, T value) noexcept
	{
		return swapBytes(value);
	}
	
	template <typename T>
	inline void Little::reorder(T &value) noexcept
	{
		// little to host big - swap in place
		value = swapBytes(value);
	}
	
	template <typename T>
	inline void Little::reorder(T *data, std::size_t count) noexcept
	{
		// little to host big - swap bytes
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline static void Little::reorder(Host host, T *data, std::size_t count) noexcept
	{
		// little to host big - swap bytes
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline T Little::operator()(T value) const noexcept
	{
		return swapBytes(value);
	}	
	
#elif CIO_ENDIAN == CIO_LITTLE_ENDIAN

	// Big methods, host dependent - host is Little

	template <typename T>
	inline T Big::order(T value) noexcept
	{
		return swapBytes(value);
	}

	template <typename T>
	inline void reorder(T *data, std::size_t count) noexcept
	{
		// big to host little - swap
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline T Big::to(Host host, T value) noexcept
	{
		return swapBytes(value);
	}
	
	template <typename T>
	inline void Big::reorder(T &value) noexcept
	{
		// big to host little - swap in place
		value = swapBytes(value);
	}
	
	template <typename T>
	inline void Big::reorder(T *data, std::size_t count) noexcept
	{
		// big to host little - swap
		swapArrayBytes(data, count);
	}
	
	template <typename T>
	inline void Big::reorder(Host host, T *data, std::size_t count) noexcept
	{
		// big to host little - swap
		swapArrayBytes(data, count);
	}
		
	template <typename T>
	inline T Big::operator()(T value) const noexcept
	{
		return swapBytes(value);
	}	
	
	// Little methods, host dependent - host is Little
	
	template <typename T>
	inline T Little::order(T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline T Little::to(Host host, T value) noexcept
	{
		return value;
	}
	
	template <typename T>
	inline void Little::reorder(T &value) noexcept
	{
		// little to host little - do nothing
	}
		
	template <typename T>
	inline void Little::reorder(T *data, std::size_t count) noexcept
	{
		// little to host little - do nothing
	}
	
	template <typename T>
	inline void Little::reorder(Host host, T *data, std::size_t count) noexcept
	{
		// little to host little - do nothing
	}
	
	template <typename T>
	inline T Little::operator()(T value) const noexcept
	{
		return value;
	}	
	
#else
#error "Host byte order is unknown"
#endif

	template <typename T, typename L>
	inline void swapArrayBytes(T *data, L count) noexcept
	{
		T *current = data;
		T *end = data + count;
		while (current < end)
		{
			applyByteSwap(*current++);
		}
	}

	template <typename L>
	inline void swapArrayBytes(void *data, L count) noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline void swapArrayBytes(bool *data, L count) noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline void swapArrayBytes(unsigned char *data, L count) noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline void swapArrayBytes(signed char *data, L count) noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline void swapArrayBytes(char *data, L count) noexcept
	{
		// nothing to do
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 0> count) noexcept
	{
		// nothing to do
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 1> count) noexcept
	{
		applyByteSwap(*data);
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 2> count) noexcept
	{
		applyByteSwap(data[0]);
		applyByteSwap(data[1]);
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 3> count) noexcept
	{
		applyByteSwap(data[0]);
		applyByteSwap(data[1]);
		applyByteSwap(data[2]);
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, 4> count) noexcept
	{
		applyByteSwap(data[0]);
		applyByteSwap(data[1]);
		applyByteSwap(data[2]);
		applyByteSwap(data[3]);
	}

	template <typename T>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, SIZE_MAX> count) noexcept
	{
		swapArrayBytes(data, count.value);
	}

	template <typename T, std::size_t N>
	inline void swapArrayBytes(T *data, std::integral_constant<std::size_t, N> count) noexcept
	{
		swapArrayBytes(data, std::integral_constant<std::size_t, N / 2>());
		swapArrayBytes(data + N / 2, std::integral_constant<std::size_t, N - N / 2>());
	}
}

#endif
