/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "BufferedInput.h"

#include "Class.h"
#include "Input.h"
#include "Metadata.h"
#include "Output.h"
#include "ProtocolFactory.h"
#include "Response.h"
#include "Seek.h"

namespace cio
{
	const std::size_t BufferedInput::DefaultBufferSize = 4096;

	Class<BufferedInput> BufferedInput::sMetaclass("cio::BufferedInput");

	const Metaclass &BufferedInput::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &BufferedInput::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	BufferedInput::BufferedInput() noexcept :
		mPosition(0)
	{
		// nothing to do
	}

	BufferedInput::BufferedInput(Pointer<Input> source) :
		mSource(std::move(source)),
		mBuffer(BufferedInput::DefaultBufferSize),
		mPosition(mSource ? mSource->getReadPosition() : 0)
	{
		mBuffer.limit(0);
	}

	BufferedInput::BufferedInput(Pointer<Input> source, std::size_t bufferSize) :
		mSource(std::move(source)),
		mBuffer(bufferSize),
		mPosition(mSource ? mSource->getReadPosition() : 0)
	{
		mBuffer.limit(0);
	}

	BufferedInput::BufferedInput(Pointer<Input> source, Buffer buffer) :
		mSource(std::move(source)),
		mBuffer(std::move(buffer)),
		mPosition(mSource ? mSource->getReadPosition() : 0)
	{
		mBuffer.limit(0);
	}

	BufferedInput::BufferedInput(BufferedInput &&in) noexcept = default;

	BufferedInput &BufferedInput::operator=(BufferedInput &&in) noexcept = default;

	BufferedInput::~BufferedInput() noexcept = default;

	void BufferedInput::clear() noexcept
	{
		mSource.clear();
		mBuffer.clear();
		mPosition = 0;
		Input::clear();
	}

	bool BufferedInput::isOpen() const noexcept
	{
		return mBuffer.available() || (mSource && mSource->isOpen());
	}

	cio::ModeSet BufferedInput::openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory)
	{
		cio::ModeSet actual;
		if (factory)
		{
			mSource = factory->openInput(resource, modes);
			if (mSource)
			{
				actual = mSource->modes();
				if (mBuffer.empty())
				{
					mBuffer.allocate(DefaultBufferSize);
				}
			}
		}
		mBuffer.limit(0);
		return actual;
	}

	Metadata BufferedInput::metadata() const
	{
		Metadata md;
		if (mSource)
		{
			md = mSource->metadata();
		}
		return md;
	}

	Path BufferedInput::location() const
	{
		Path path;
		if (mSource)
		{
			path = mSource->location();
		}
		return path;
	}

	Resource BufferedInput::type() const noexcept
	{
		Resource type;
		if (mSource)
		{
			type = mSource->type();
		}
		return type;
	}

	ModeSet BufferedInput::modes() const noexcept
	{
		ModeSet modes;
		if (mSource)
		{
			modes = mSource->modes();
		}
		return modes;
	}

	Length BufferedInput::size() const noexcept
	{
		return mSource ? mSource->size() : mBuffer.size();
	}

	Length BufferedInput::readable() const noexcept
	{
		Length readable = mBuffer.remaining();
		if (mSource)
		{
			readable += mSource->readable();
		}
		return readable;
	}

	Resize BufferedInput::isReadSeekable() const noexcept
	{
		Resize policy = Resize::None;
		if (mSource)
		{
			policy = mSource->isReadSeekable();
		}
		return policy;
	}

	Progress<std::size_t> BufferedInput::requestRead(void *data, std::size_t bytes) noexcept
	{
		Progress<std::size_t> result = mBuffer.requestRead(data, bytes);
		if (result.count == 0 && bytes > 0 && mSource)
		{
			// Request is large enough we should just punt to underlying input
			if (mBuffer.size() < bytes)
			{
				result = mSource->requestRead(data, bytes);
				mPosition += result.count;
			}
			// Refill buffer then serve from it
			else
			{
				this->refillBuffer();
				result = mBuffer.requestRead(data, bytes);
			}
		}
		return result;
	}

	Progress<std::size_t> BufferedInput::requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept
	{
		Progress<std::size_t>  result(Action::Read, 0, bytes);

		Length absolute = this->calculateReadPosition(position, mode);

		if (absolute >= mPosition && absolute < mPosition + mBuffer.limit())
		{
			std::size_t offset = static_cast<std::size_t>(absolute - mPosition);
			std::size_t read = mBuffer.requestReadFromPosition(data, bytes, offset);
			result.count = read;
			result.succeed();
		}
		else if (mSource)
		{
			result = mSource->requestReadFromPosition(data, bytes, position, mode);
		}

		return result;
	}

	Length BufferedInput::getReadPosition() const noexcept
	{
		return mPosition + mBuffer.position();
	}

	Progress<Length> BufferedInput::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);

		Length absolute = this->calculateReadPosition(position, mode);
		result.total = absolute;

		if (absolute >= mPosition && absolute < mPosition + mBuffer.limit())
		{
			std::size_t offset = static_cast<std::size_t>(absolute - mPosition);
			mBuffer.position(offset);
			result.count = absolute;
			result.succeed();
		}
		else if (mSource)
		{
			result = mSource->requestSeekToRead(position, mode);
			if (result.succeeded())
			{
				mPosition = result.count;
				mBuffer.limit(0);
			}
		}
		
		return result;
	}

	Progress<Length> BufferedInput::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Read);
		
		if (bytes < 0)
		{
			result.fail(Reason::Invalid);
		}
		else
		{
			std::size_t skipped = mBuffer.skip(bytes.size());
			if (skipped < bytes)
			{
				if (mSource)
				{
					result = mSource->requestDiscard(bytes - skipped);
				}
			}
			result.succeed();
		}
			
		return result;
	}

	Response<std::size_t> BufferedInput::recommendReadAlignment() const noexcept
	{
		return Response<std::size_t>(mBuffer.size());
	}

	State BufferedInput::setReadTimeout(std::chrono::milliseconds timeout)
	{
		State state;
		if (mSource)
		{
			state = mSource->setReadTimeout(timeout);
		}
		else
		{
			state.fail(Reason::Unopened);
		}
		return state;
	}

	std::chrono::milliseconds BufferedInput::getReadTimeout() const
	{
		std::chrono::milliseconds timeout(0);
		if (mSource)
		{
			timeout = mSource->getReadTimeout();
		}
		return timeout;
	}

	Text BufferedInput::getText(std::size_t bytes)
	{
		Text text;

		if (mBuffer.available())
		{
			text = mBuffer.getText(bytes).duplicate();
		}
		else if (mSource)
		{
			this->refillBuffer();
			text = mBuffer.getText(bytes).duplicate();
		}

		return text;
	}

	Text BufferedInput::getTextLine(std::size_t bytes)
	{
		Text line = mBuffer.getTextLine(bytes);
		if ((!line || line.back() != '\n') && mSource)
		{
			std::size_t read = line.size();
			line.internalize();

			while (read < bytes && (!line || line.back() != '\n'))
			{
				this->refillBuffer();

				Text next = mBuffer.getTextLine(bytes - read);
				read += next.size();
				line += next;
			}
		}
		line.internalize();
		return line;
	}

	Progress<Length> BufferedInput::requestCopyTo(Output &output, Length length)
	{
		Progress<Length> copied(0, length);

		if (!mBuffer.available())
		{
			this->refillBuffer();
		}

		if (mBuffer.available())
		{
			copied = output.requestDrain(mBuffer);
			copied.total = length;
		}
		else if (!mSource)
		{
			copied.fail(Reason::Unopened);
		}

		return copied;
	}

	Progress<std::size_t> BufferedInput::requestReadBuffer(std::size_t bytes) noexcept
	{
		Progress<std::size_t> result(0, bytes);
		if (mBuffer.remaining() < bytes)
		{
			mBuffer.resize(bytes);
			result.succeed();
		}
		else
		{
			result.fail(Reason::Overflow);
		}
		result.count = mBuffer.size();
		return result;
	}

	Buffer &BufferedInput::getReadBuffer() noexcept
	{
		return mBuffer;
	}

	const Buffer &BufferedInput::getReadBuffer() const noexcept
	{
		return mBuffer;
	}

	void BufferedInput::setReadBuffer(Buffer buffer)
	{
		if (buffer.unused() < mBuffer.remaining())
		{
			throw Exception(Action::Copy, Reason::Underflow);
		}
		buffer.receive(mBuffer);
		mBuffer = std::move(buffer);
	}

	Input *BufferedInput::getInput() noexcept
	{
		return mSource.get();
	}

	const Input *BufferedInput::getInput() const noexcept
	{
		return mSource.get();
	}

	void BufferedInput::setInput(Pointer<Input> source)
	{
		mSource = std::move(source);
		if (mBuffer.empty())
		{
			mBuffer.allocate(BufferedInput::DefaultBufferSize);
		}
		mBuffer.limit(0);
		mPosition = mSource ? mSource->getReadPosition() : 0;
	}

	Pointer<Input> BufferedInput::takeInput() noexcept
	{
		return std::move(mSource);
	}

	void BufferedInput::refillBuffer()
	{
		if (mSource)
		{
			mPosition += mBuffer.compact();
			mBuffer.unflip();
			mSource->requestFill(mBuffer);
			mBuffer.flip();
		}
	}
}
