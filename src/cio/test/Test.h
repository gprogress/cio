/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_TEST_H
#define CIO_TEST_TEST_H

#include "Types.h"

#include <cio/Text.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace test
	{
		/**
		 * The Test class provides the core interface for automated testing.
		 *
		 * Tests can be named and/or given resource paths to form a name hierarchy. This is not used for the testing itself, but can
		 * be used by the Runner or other classes to selectively enable or disable tests.
		 *
		 * This base class does not define any execution mechanism for tests, subclasses must override the execute() method.
		 */
		class CIO_TEST_API Test
		{
			public:
				/**
				 * Gets the metaclass for Test.
				 *
				 * @return the metaclass for Test
				 */
				static const Metaclass &getDeclaredMetaclass();

				/**
				 * Construct the empty test.
				 */
				Test() noexcept;

				/**
				 * Construct a test with the given label.
				 *
				 * @param path The resource path for the test
				 */
				explicit Test(Text label);

				/**
				 * Construct a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Test(const Test &in);

				/**
				 * Construct a test by transferring the input of a given test.
				 *
				 * @param in The test to move
				 */
				Test(Test &&in) noexcept;

				/**
				 * Assign a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Test &operator=(const Test &in);

				/**
				 * Transfer the contents of a test into this one.
				 *
				 * @param in The test to move
				 */
				Test &operator=(Test &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Test() noexcept;

				/**
				 * Gets the metaclass for the runtime type of this Test class or subclass.
				 * This must be overridden by subclasses to enable polymorphic cloning and transfers.
				 *
				 * @return the metaclass for the actual runtime type of this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept;

				/**
				 * Clears all state on this object and restores it to as if it had been default constructed.
				 */
				virtual void clear() noexcept;

				/**
				 * Performs common fixture set up and construction for this test.
				 */
				virtual Severity setUp();

				/**
				 * Performs common fixture tear down and destruction for this test.
				 */
				virtual void tearDown() noexcept;

				/**
				 * Executes the given test URI, reporting faults to the given logger. The empty URI indicates to execute all tests.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed as a standalone test, so subclasses that need context will need to override this method to set it up.
				 *
				 * This base class implementation does nothing and succeeds at it.
				 *
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity execute(Reporter &reporter);

				/**
				 * Executes the given test, reporting faults to the given logger.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed in the full context of the entire stack of tests being executed.
				 * Most tests will not need the context and can just execute the test in isolation.
				 *
				 * This base class implementation just calls execute and ignores the context.
				 *
				 * @param context The test context to run this test in
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity executeInContext(Test *context, Reporter &reporter);

				/**
				 * Executes the given test, reporting faults to the given logger.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed in the full context of the entire stack of tests being executed.
				 * Most tests will not need the full context and can just execute the test in isolation.
				 *
				 * This base class implementation calls executeInContext with the last (most immediate parent) test.
				 *
				 * @param context The arary of test contexts to run this test in
				 * @param depth The test context depth
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity executeInFullContext(Test **context, std::size_t depth, Reporter &reporter);

				/**
				 * Finds the test case for the given URI. If empty, returns this test case.
				 * The base implementation returns this test if URI is empty or equals the test URI and nullptr otherwise.
				 * Subclasses that containerize multiple tests should override.
				 * 
				 * @param uri The URI of the test to find
				 * @return the found test case, or nullptr if it did not exist
				 */
				virtual Test *find(const Text &name) noexcept;

				/**
				 * Gets the test name. This should be a short human-readable label.
				 *
				 * @return the test name
				 */
				const Text &getName() const;

				/**
				*
				 * Sets the test name. This should be a short human-readable label.
				 *
				 * @param name the test name
				 */
				void setName(Text name);

			private:
				/** The metaclass for this class */
				static const Class<Test> sMetaclass;
			
				/** The short name of the test, typically the filename of the URI */
				Text mName;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
