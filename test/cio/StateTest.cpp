/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "StateTest.h"

#include <cio/State.h>
#include <cio/test/Assert.h>

#include <sstream>

namespace cio
{
	StateTest::StateTest() :
		cio::test::Suite("cio/State")
	{		
		this->addTestCase("State", &StateTest::testConstruction);
		this->addTestCase("operator=", &StateTest::testAssignment);
		this->addTestCase("clear", &StateTest::testClear);
		this->addTestCase("print",  &StateTest::testPrint);
		this->addTestCase("operator<<", &StateTest::testPrintOperator);
	}

	StateTest::~StateTest() noexcept = default;

	void StateTest::testConstruction()
	{
		// Default construction
		{
			State state;
			CIO_ASSERT_EQUAL(Action::None, state.action);
			CIO_ASSERT_EQUAL(Status::None, state.status);
			CIO_ASSERT_EQUAL(Outcome::None, state.outcome);
			CIO_ASSERT_EQUAL(Reason::None, state.reason);
		}
		
		// Construct with Status
		{
			State state(Status::Requested);
			CIO_ASSERT_EQUAL(Action::None, state.action);
			CIO_ASSERT_EQUAL(Status::Requested, state.status);
			CIO_ASSERT_EQUAL(Outcome::None, state.outcome);
			CIO_ASSERT_EQUAL(Reason::None, state.reason);
		}
		
		// Construct with Action
		{
			State state(Action::Edit);
			CIO_ASSERT_EQUAL(Action::Edit, state.action);
			CIO_ASSERT_EQUAL(Status::None, state.status);
			CIO_ASSERT_EQUAL(Outcome::None, state.outcome);
			CIO_ASSERT_EQUAL(Reason::None, state.reason);
		}
		
		// Construct with Action + Status
		{
			State state(Action::Resize, Status::Canceled);
			CIO_ASSERT_EQUAL(Action::Resize, state.action);
			CIO_ASSERT_EQUAL(Status::Canceled, state.status);
			CIO_ASSERT_EQUAL(Outcome::None, state.outcome);
			CIO_ASSERT_EQUAL(Reason::None, state.reason);
		}
		
		// Construct with Action + Status + Outcome
		{
			State state(Action::Subscribe, Status::Completed, Outcome::Succeeded);
			CIO_ASSERT_EQUAL(Action::Subscribe, state.action);
			CIO_ASSERT_EQUAL(Status::Completed, state.status);
			CIO_ASSERT_EQUAL(Outcome::Succeeded, state.outcome);
			CIO_ASSERT_EQUAL(Reason::None, state.reason);
		}
		
		// Construct with Action + Status + Outcome + Reason
		{
			State state(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
			CIO_ASSERT_EQUAL(Action::Execute, state.action);
			CIO_ASSERT_EQUAL(Status::Completed, state.status);
			CIO_ASSERT_EQUAL(Outcome::Failed, state.outcome);
			CIO_ASSERT_EQUAL(Reason::Exhausted, state.reason);
		}
		
		// Test copy construction
		{
			State input(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
			State state(input);
			CIO_ASSERT_EQUAL(Action::Execute, state.action);
			CIO_ASSERT_EQUAL(Status::Completed, state.status);
			CIO_ASSERT_EQUAL(Outcome::Failed, state.outcome);
			CIO_ASSERT_EQUAL(Reason::Exhausted, state.reason);
		}
	}

	void StateTest::testAssignment()
	{
		// Test copy assignment
		{
			State input(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
			State state;
			state = input;
			
			CIO_ASSERT_EQUAL(Action::Execute, state.action);
			CIO_ASSERT_EQUAL(Status::Completed, state.status);
			CIO_ASSERT_EQUAL(Outcome::Failed, state.outcome);
			CIO_ASSERT_EQUAL(Reason::Exhausted, state.reason);
		}
	}
	
	void StateTest::testClear()
	{
		State state(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
		
		CIO_ASSERT_EQUAL(Action::Execute, state.action);
		CIO_ASSERT_EQUAL(Status::Completed, state.status);
		CIO_ASSERT_EQUAL(Outcome::Failed, state.outcome);
		CIO_ASSERT_EQUAL(Reason::Exhausted, state.reason);
		
		state.clear();
		
		CIO_ASSERT_EQUAL(Action::None, state.action);
		CIO_ASSERT_EQUAL(Status::None, state.status);
		CIO_ASSERT_EQUAL(Outcome::None, state.outcome);
		CIO_ASSERT_EQUAL(Reason::None, state.reason);
	}
	
	void StateTest::testPrint()
	{
		// Print the empty State
		{
			State state;
			char buffer[128] = { };
			std::string expected = "Action: None, Status: None, Outcome: None, Reason: None";
			
			// Test printing to buffer
			std::size_t length = state.print(buffer, 128);
			CIO_ASSERT_EQUAL(expected, buffer);
			CIO_ASSERT_EQUAL(expected.size(), length);
		}
		
		// Print a non-empty State
		{
			State state(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
			char buffer[128] = { };
			std::string expected = "Action: Execute, Status: Completed, Outcome: Failed, Reason: Exhausted";
			
			// Test printing to buffer
			std::size_t length = state.print(buffer, 128);
			CIO_ASSERT_EQUAL(expected, buffer);
			CIO_ASSERT_EQUAL(expected.size(), length);
		}
	}
	
	void StateTest::testPrintOperator()
	{
		// Print the empty State
		{
			State state;
			std::ostringstream stream;
			std::string expected = "Action: None, Status: None, Outcome: None, Reason: None";
			
			// Test printing to buffer
			CIO_ASSERT(stream << expected);
			CIO_ASSERT_EQUAL(expected, stream.str());
		}
		
		// Print a non-empty State
		{
			State state(Action::Execute, Status::Completed, Outcome::Failed, Reason::Exhausted);
			std::ostringstream stream;
			std::string expected = "Action: Execute, Status: Completed, Outcome: Failed, Reason: Exhausted";
			
			// Test printing to buffer
			CIO_ASSERT(stream << expected);
			CIO_ASSERT_EQUAL(expected, stream.str());
		}
	}
}
