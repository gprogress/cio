/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "ProtocolFilter.h"

#include <cio/Class.h>
#include <cio/Path.h>

namespace cio
{
	// In Static.cpp becuase it is currently used for static allocation
	// Class<ProtocolFilter> ProtocolFilter::sMetaclass("cio::ProtocolFilter");

	ProtocolFilter::~ProtocolFilter() noexcept
	{
		//
	}

	const Metaclass &ProtocolFilter::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	int ProtocolFilter::matches(Resource type, const Path &path) const noexcept
	{
		return this->matches(type, path.getParent(), path.getExtension());
	}

	int ProtocolFilter::matches(Resource type, const Text &prefix, const Text &ext) const noexcept
	{
		int score = -1;

		if (this->type == Resource::Unknown) // wildcard type match
		{
			score = 0;
		}
		else if (this->type == type) // exact type match
		{
			score = 4;
		}

		// No point in scoring anything else if type didn't match
		if (score >= 0)
		{ 
			// Protocol match check
			int protocolMatch = ProtocolFilter::matchesListEntry(this->prefix, prefix);
			if (protocolMatch >= 0)
			{
				score += protocolMatch; // add either 0 or 1

				int extensionMatch = ProtocolFilter::matchesListEntry(this->extension, ext);
				if (extensionMatch >= 0)
				{
					score |= (extensionMatch << 1); // add either 0 or 2
				}
				else
				{
					score = -1;
				}
			}
			else
			{
				score = -1;
			}
		}

		return score;
	}

	int ProtocolFilter::matchesListEntry(const Text &list, const Text &text)  noexcept
	{
		int matches = -1;

		if (list.empty())
		{
			matches = text.empty();
		}
		else
		{
			std::pair<Text, Text> current = list.split(';');
			while (current.second.data() > current.first.data())
			{
				// wildcard match, note but look for exact
				if (current.first == "*")
				{
					matches = 0;
				}
				// exact match, no need to search further
				else if (current.first == text)
				{
					matches = 1;
					break;
				}

				current = current.second.split(';');
			}
		}

		return matches;
	}
}