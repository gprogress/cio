/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Resolver.h"

// Needed for initialize and deinitialize on Windows
#include "Socket.h"

#include <cio/Parse.h>
#include <cio/Print.h>

#include <mutex>

#if defined CIO_USE_UNIX_SOCKETS

#if defined CIO_HAVE_NETDB_H
#include <netdb.h>
#endif

#if defined CIO_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#elif defined CIO_USE_WINSOCK2
#if defined CIO_HAVE_WS2TCPIP_H
#include <Ws2tcpip.h>
#endif
#endif

namespace cio
{
	namespace net
	{
		namespace
		{
			std::mutex sGlobalMutex;

			void translateAddressInfo(struct addrinfo *ai, Service &result)
			{
				result.setAddress(Address(ai->ai_addr, ai->ai_addrlen));
				result.setChannelType(fromNativeSocketType(ai->ai_socktype));
				result.setProtocolFamily(ProtocolFamily(ai->ai_protocol));
			}
		}

		Resolver::Resolver() noexcept
		{
			Socket::initialize();
		}

		Resolver::Resolver(const Resolver &in)
		{
			Socket::initialize();
		}

		Resolver::~Resolver() noexcept
		{
			Socket::deinitialize();
		}

		std::pair<ProtocolFamily, std::uint16_t> Resolver::findServicePort(const Text &proto) const
		{
			std::pair<ProtocolFamily, std::uint16_t> result;

			if (proto)
			{
				std::size_t portStart = proto.rfind(':');
				std::size_t protoEnd = portStart;

				if (portStart != std::string::npos)
				{
					result.second = cio::parseUnsigned16(proto.suffix(portStart + 1));
				}
				else
				{
					protoEnd = proto.strlen();
				}

				if (protoEnd > 0)
				{
					Text protoText = proto.prefix(protoEnd);
					auto valid = result.first.parse(protoText);

					if (valid != Validation::Exact)
					{
						protoText.nullTerminate();
						std::lock_guard<std::mutex> lock(sGlobalMutex);
						struct servent *service = ::getservbyname(protoText.c_str(), nullptr);

						if (service)
						{
							result.first.parse(service->s_proto);
							if (!result.second)
							{
								result.second = ntohs(service->s_port);
							}
						}
					}
				}
				else
				{
					std::lock_guard<std::mutex> lock(sGlobalMutex);
					struct servent *service = ::getservbyport(htons(result.second), nullptr);

					if (service)
					{
						result.first.parse(service->s_proto);
					}
				}
			}

			return result;
		}

		std::string Resolver::findServiceName(ProtocolFamily type, std::uint16_t port) const
		{
			std::string service;
			char protoText[16] = { 0 };
			std::size_t protoLen = type.print(protoText, 16);

			if (port == 0)
			{
				service.assign(protoText, protoText + protoLen);
			}
			else
			{
				std::lock_guard<std::mutex> lock(sGlobalMutex);
				struct servent *entry = ::getservbyport(htons(port), protoText);

				if (entry)
				{
					service = entry->s_name;
				}
			}

			return service;
		}

		Address Resolver::lookupAddress(const Text &host) const
		{
			Address address;

			// Step 1: Attempt to parse out as an IP address without resolution
			if (!address.parse(host))
			{
				// Step 2: if that didn't succeed, use system resolution on hostname
				Text chost = host.viewNullTerminated();

				struct addrinfo *results = nullptr;
				int status = ::getaddrinfo(chost.data(), nullptr, nullptr, &results);

				if (status == 0)
				{
					address.set(results->ai_addr, results->ai_addrlen);
					::freeaddrinfo(results);
				}
			}

			return address;
		}

		std::vector<Address> Resolver::lookupAddresses(const Text &host) const
		{
			std::vector<Address> addresses;

			struct addrinfo hints = { };
			struct addrinfo *results = nullptr;

			// Linux says this is the defaults
			hints.ai_flags |= (AI_V4MAPPED | AI_ADDRCONFIG);

			// Step 1: parse as a raw IP address first to check for special cases
			Address ip;
			ip.parse(host);

			// Step 2: even if that succeeded, perform lookup to get additional addresses
			// Note if we have the special "any" address, the host needs to be nullptr and we need hints
			Text chost;
			if (ip.isAnyIP())
			{
				hints.ai_flags |= AI_PASSIVE;
			}
			else
			{
				chost = host.viewNullTerminated();
			}

			int status = ::getaddrinfo(chost.data(), nullptr, &hints, &results);

			if (status == 0)
			{
				struct addrinfo *current = results;

				do
				{
					addresses.emplace_back();
					addresses.back().set(current->ai_addr, current->ai_addrlen);
					current = current->ai_next;
				}
				while (current);

				::freeaddrinfo(results);
			}

			return addresses;
		}

		Service Resolver::lookupService(const cio::Path &path) const
		{
			Text proto = path.getProtocol();
			Text root = path.getRoot();
			Text hostname = root.view();

			// Skip up to last '@' since anything before that won't modify resolution
			for (std::size_t i = root.size(); i > 0; --i)
			{
				if (root[i - 1] == '@')
				{
					hostname = root.suffix(i);
					break;
				}
			}

			Text port;

			// See if we have a port override, regardless of protocol presence
			for (std::size_t j = hostname.size(); j > 0; --j)
			{
				if (hostname[j - 1] == ':')
				{
					port = hostname.suffix(j);
					hostname = hostname.prefix(j - 1);
				}
			}

			if (proto.empty())
			{
				port = proto.view();
			}

			std::uint16_t portOverride = cio::parseUnsigned16(port);
			return this->lookupService(hostname, proto, portOverride);
		}

		std::vector<Service> Resolver::lookupServices(const cio::Path &path) const
		{
			Text proto = path.getProtocol();
			Text root = path.getRoot();
			Text hostname = root.view();

			// Skip up to last '@' since anything before that won't modify resolution
			for (std::size_t i = root.size(); i > 0; --i)
			{
				if (root[i - 1] == '@')
				{
					hostname = root.suffix(i);
					break;
				}
			}

			Text port;

			// See if we have a port override, regardless of protocol presence
			for (std::size_t j = hostname.size(); j > 0; --j)
			{
				if (hostname[j - 1] == ':')
				{
					port = hostname.suffix(j);
					hostname = hostname.prefix(j - 1);
				}
			}

			// Attempt resolution using protocol first, falling back to port if no protocol specified
			if (proto.empty())
			{
				proto = port.view();
			}

			std::uint16_t portOverride = cio::parseUnsigned16(port);
			return this->lookupServices(hostname, proto.data(), portOverride);
		}

		Service Resolver::lookupService(const Text &host, const Text &proto, std::uint16_t portOverride) const
		{
			Service service;

			struct addrinfo *results = nullptr;
			struct addrinfo hints = { };

			// Linux says this is the defaults
			hints.ai_flags |= (AI_V4MAPPED | AI_ADDRCONFIG);

			// Step 1: parse as a raw IP address first to check for special cases
			Address ip;
			ip.parse(host);

			// Step 2: even if that succeeded, perform lookup to get additional addresses
			// Note if we have the special "any" address, the host needs to be nullptr and we need hints
			Text chost;
			if (ip.isAnyIP())
			{
				hints.ai_flags |= AI_PASSIVE;
			}
			else
			{
				chost = host.viewNullTerminated();
			}

			ProtocolFamily pf;

			Text cproto;
			if (Validation::Exact == pf.parse(proto))
			{
				hints.ai_socktype = toNativeSocketType(pf.getChannelType());
				hints.ai_protocol = pf.get();
			}
			else
			{
				cproto = proto.viewNullTerminated();
			}

			int status = ::getaddrinfo(chost.data(), cproto.data(), &hints, &results);

			if (status == 0)
			{
				translateAddressInfo(results, service);

				if (portOverride > 0)
				{
					service.setPort(portOverride);
				}

				::freeaddrinfo(results);
			}

			return service;
		}

		std::vector<Service> Resolver::lookupServices(const Text &host, const Text &proto, std::uint16_t portOverride) const
		{
			std::vector<Service> addresses;

			struct addrinfo *results = nullptr;
			struct addrinfo hints = { };

			// Linux says this is the defaults
			hints.ai_flags |= (AI_V4MAPPED | AI_ADDRCONFIG);

			// Step 1: parse as a raw IP address first to check for special cases
			Address ip;
			ip.parse(host);

			// Step 2: even if that succeeded, perform lookup to get additional addresses
			// Note if we have the special "any" address, the host needs to be nullptr and we need hints
			Text chost;
			if (ip.isAnyIP())
			{
				hints.ai_flags |= AI_PASSIVE;
			}
			else
			{
				chost = host.viewNullTerminated();
			}

			ProtocolFamily pf;

			Text cproto;
			if (Validation::Exact == pf.parse(proto))
			{
				hints.ai_socktype = toNativeSocketType(pf.getChannelType());
				hints.ai_protocol = pf.get();
			}
			else
			{
				cproto = proto.viewNullTerminated();
			}

			int status = ::getaddrinfo(chost.data(), cproto.data(), &hints, &results);

			if (status == 0 && results)
			{
				struct addrinfo *next = results;

				do
				{
					addresses.emplace_back();
					translateAddressInfo(next, addresses.back());
					next = next->ai_next;
				}
				while (next);

				::freeaddrinfo(results);

				if (portOverride > 0)
				{
					for (Service &service : addresses)
					{
						service.setPort(portOverride);
					}
				}
			}

			return addresses;
		}
	}
}
