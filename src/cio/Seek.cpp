/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Seek.h"

#include "Length.h"

#include <cio/Case.h>

#include <istream>
#include <ostream>

namespace cio
{
	namespace
	{
		const char *sSeekText[] =
		{
			"None",
			"Begin",
			"Current",
			"End",
			"Unknown"
		};
	}
	
	Length position(Length offset, Seek mode, Length current, Length size)
	{
		Length result;

		switch (mode)
		{
			case Seek::None:
				result = 0;
				break;

			case Seek::Begin:
				result = offset;
				break;

			case Seek::Current:
			{
				if (current.known())
				{
					result = current + offset;
				}
				break;
			}

			case Seek::End:
			{
				if (size.known())
				{
					result = size + offset;
				}
				break;
			}

			default:
				result = CIO_UNKNOWN_LENGTH;
				break;
		}

		return result;
	}
	
	CIO_API Length position(Length offset, Seek mode, std::size_t current, std::size_t size)
	{
		Length result;

		switch (mode)
		{
			case Seek::None:
				result = 0;
				break;

			case Seek::Begin:
				result = offset;
				break;

			case Seek::Current:
			{
				if (current != SIZE_MAX)
				{
					result = current + offset;
				}
				break;
			}

			case Seek::End:
			{
				if (size != SIZE_MAX)
				{
					result = size + offset;
				}
				break;
			}

			default:
				result = CIO_UNKNOWN_LENGTH;
				break;
		}

		return result;
	}
	
	const char *print(Seek mode) noexcept
	{
		return sSeekText[static_cast<unsigned>(mode)];
	}

	Seek parseSeek(const char *text) noexcept
	{
		Seek mode = Seek::Unknown;

		for (unsigned i = 0; i < sizeof(sSeekText) / sizeof(const char *); ++i)
		{
			if (CIO_STRCASECMP(sSeekText[i], text) == 0)
			{
				mode = static_cast<Seek>(i);
				break;
			}
		}

		return mode;
	}

	Seek parseSeek(const std::string &text) noexcept
	{
		return parseSeek(text.c_str());
	}

	std::ostream &operator<<(std::ostream &stream, Seek mode)
	{
		return (stream << print(mode));
	}

	std::istream &operator>>(std::istream &stream, Seek &mode)
	{
		std::string chunk;
		stream >> chunk;
		mode = parseSeek(chunk);
		return stream;
	}
}
