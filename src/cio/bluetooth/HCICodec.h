/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_HCICODEC_H
#define CIO_BLUETOOTH_HCICODEC_H

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

#include "Types.h"

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth HCICodec class provides the low-level method to encode Host-Controller Interface (HCI) bluetooth commands into binary packets to send to the microcontroller
		 * and then decode resulting events.
		 *
		 * It is primarily used by the HCIController but may be used independently for any reason to work with the Bluetooth HCI protocol.
		 *
		 * @warning This class is not yet fully implemented since the CIO implementation was switched to D-Bus on Linux.
		 */
		class CIO_BLUETOOTH_API HCICodec
		{
			public:
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass.
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Encodes the given Device MAC to the native format for HCI.
				 * The format requires 6 bytes.
				 *
				 * @param address The MAC
				 * @param data The buffer to receive the native MAC
				 */ 
				static void toNativeAddress(const DeviceAddress &address, void *data) noexcept;
				
				/**
				 * Decodes the given Device MAC from the native format for HCI.
				 * The format requires 6 bytes.
				 *
				 * @param data The buffer to receive the native MAC
				 * @return the MAC
				 */
				static DeviceAddress fromNativeAddress(const void *data) noexcept;
				
				/**
				 * Peeks at the given HCI packet to classify what kind of packet it is.
				 *
				 * @param packet The packet
				 * @return the packet type
				 */
				Packet classifyPacket(const Buffer &packet) const noexcept;
				
				/**
				 * Decodes a MAC address from the given packet buffer.
				 * This advances the buffer 6 bytes and returns the MAC>
				 *
				 * @param packet The packet to decode
				 * @return the decoded MAC
				 */
				DeviceAddress decodeAddress(Buffer &packet) const;
				
				/**
				 * Decodes the packet header, determining the packet type and reading the packet data length.
				 * The buffer is updated to represent the packet payload.
				 *
				 * @param packet The packet buffer
				 * @return the packet type
				 */
				Packet decodePacketHeader(Buffer &packet) const;
				
				/**
				 * Decodes an event packet to determine the event type and configure the packet to read the event data.
				 * This requires decodePacketHeader to have been done first and the result to be Packet::Event.
				 *
				 * @param packet The packet buffer
				 * @return the event type
				 */
				Event decodeEventHeader(Buffer &packet) const;
				
				/**
				 * Decodes a low-energy event packet to determine the event type and configure the packet to read the event data.
				 * This requires decodeEventHeader to have been done first and the result to be Event::LowEnergy.
				 *
				 * @param packet The packet buffer
				 * @return the low-energy event type
				 */
				LowEnergyEvent decodeLowEnergyHeader(Buffer &packet) const;
				
				/**
				 * Decodes the header information of a low-energy advertisement packet.
				 * This requires decodeLowEnergyEventHeader to have been done first and the result to be LowEnergyEvent::Advertisement.
				 *
				 * @param packet The packet buffer
				 * @return the number of advertisements
				 */
				std::size_t decodeAdvertisementHeader(Buffer &packet) const;
				
				/**
				 * Decodes a low-energy advertisement packet.
				 * This requires decodeAdvertisementHeader to have been done first.
				 *
				 * @param packet The packet buffer
				 * @return the next advertisement
				 */
				Peripheral decodeNextAdvertisement(Buffer &packet) const;
				
				/**
				 * Gets the maximum number of local Bluetooth devices that the host system can handle.
				 *
				 * @return the maximum Bluetooth device count
				 */
				std::size_t getMaxDeviceCount() const noexcept;
				
				/**
				 * Allocates enough space in the given device buffer to represent the maximum device count.
				 *
				 * @param buffer The buffer
				 * @return the buffer after this operation
				 */
				Buffer &reserveLocalDeviceList(Buffer &buffer) const;
				
				/**
				 * Allocates enough space in the given device buffer to represent at least the given count of devices.
				 *
				 * @param buffer The buffer
				 * @param count The number of devices
				 * @return the buffer after this operation
				 */
				Buffer &reserveLocalDeviceList(Buffer &buffer, std::size_t count) const;
				
				/**
				 * Decodes the header of a device buffer obtained by the HCI ioctl
				 * to determine how many devices are present.
				 *
				 * @param buffer The device buffer
				 * @return the next device in the buffer
				 */
				std::size_t decodeDeviceListHeader(Buffer &buffer) const;
				
				/**
				 * Decodes the next device from a device buffer obtained by the HCI ioctl.
				 *
				 * @param buffer The device buffer
				 * @return the next device in the buffer
				 */
				Device decodeNextDevice(Buffer &buffer) const;
				
			private:
				/** The metaclass for this class */
				static Class<HCICodec> sMetaclass;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

