/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "IPv4.h"

#include "Flow.h"

#include <cio/Print.h>
#include <cio/Parse.h>

#include <cctype>
#include <ostream>

namespace cio
{
	namespace net
	{
		IPv4 IPv4::any() noexcept
		{
			return IPv4(0u);
		}

		IPv4 IPv4::localhost() noexcept
		{
			return IPv4(0x7F000001u);
		}

		IPv4 IPv4::broadcast() noexcept
		{
			return IPv4(0xFFFFFFFFu);
		}
		
		bool IPv4::supports(Flow flow) noexcept
		{
			return flow != Flow::Anycast;
		}

		IPv4::IPv4(const char *text) noexcept :
			mBytes(0)
		{
			this->parse(text);
		}

		IPv4::IPv4(const char *text, std::size_t len) noexcept :
			mBytes(0)
		{
			this->parse(text, len);
		}

		IPv4::IPv4(const std::string &text) noexcept :
			mBytes(0)
		{
			this->parse(text);
		}
		IPv4::IPv4() noexcept :
			mBytes(0)
		{
			// nothing more to do
		}

		IPv4::IPv4(std::uint32_t bytes) noexcept :
			mBytes(bytes)
		{
			// nothing more to do
		}

		std::uint32_t IPv4::get() const noexcept
		{
			return mBytes;
		}

		void IPv4::set(std::uint32_t bytes) noexcept
		{
			mBytes = bytes;
		}

		void IPv4::clear() noexcept
		{
			mBytes = 0;
		}

		IPv4::operator bool() const noexcept
		{
			return mBytes != 0;
		}

		std::size_t IPv4::print(char *text, std::size_t length) const noexcept
		{
			char buffer[16];
			char *tmp = (length < 16) ? buffer : text;
			std::size_t off = cio::print(mBytes >> 24, tmp, 16);
			tmp[off++] = '.';
			off += cio::print((mBytes >> 16) & 0xFF, tmp + off, 16 - off);
			tmp[off++] = '.';
			off += cio::print((mBytes >> 8) & 0xFF, tmp + off, 16 - off);
			tmp[off++] = '.';
			off += cio::print(mBytes & 0xFF, tmp + off, 16 - off);

			if (length > 0 && tmp != text)
			{
				std::memcpy(text, tmp, std::min(off, length));
			}

			if (length > off)
			{
				std::memset(text + off, 0, length - off);
			}

			return off;
		}

		std::string IPv4::print() const
		{
			char tmp[16];
			std::size_t actual = this->print(tmp, 16);
			return std::string(tmp, tmp + actual);
		}

		TextParseStatus IPv4::parse(const char *text) noexcept
		{
			TextParseStatus valid(0u, 0, Validation::Missing, Reason::Missing);

			if (text)
			{
				std::size_t len = std::strlen(text);
				valid = this->parse(text, len + 1);
			}

			return valid;
		}

		TextParseStatus IPv4::parse(const std::string &text) noexcept
		{
			return this->parse(text.c_str(), text.size() + 1);
		}

		TextParseStatus IPv4::parse(const char *text, std::size_t length) noexcept
		{
			TextParseStatus valid;

			mBytes = 0;

			std::uint32_t current = 0;
			std::uint32_t shift = 24;
			bool active = false;
			const char *ptr = text;

			while (valid.parsed < length && *ptr && !std::isspace(*ptr))
			{
				char c = *ptr;

				if (c >= '0' && c <= '9')
				{
					current = current * 10 + (c - '0');
					active = true;

					if (current > 255)
					{
						valid.status = Validation::AboveMaximum;
						break;
					}
				}
				else if (c == '.')
				{
					if (active)
					{
						mBytes |= (current << shift);
						current = 0;
						active = false;

						if (shift > 0)
						{
							shift -= 8;
						}
						else
						{
							valid.status = Validation::TooMany;
							break;
						}
					}
					else
					{
						valid.status = Validation::InvalidValue;
						break;
					}
				}
				else
				{
					valid.reason = Reason::Unexpected;
					break;
				}

				++ptr;
				++valid.parsed;
			}

			if (active)
			{
				mBytes |= (current << shift);
			}

			if (valid.status == Validation::None)
			{
				if (shift == 0)
				{
					valid.status = Validation::Exact;
				}
				else
				{
					valid.status = Validation::TooFew;
				}
			}

			if (length == valid.parsed)
			{
				valid.reason = Reason::Underflow;
			}
			else
			{
				valid.terminator = *ptr;
			}

			return valid;
		}

		bool operator==(const IPv4 &left, const IPv4 &right)
		{
			return left.get() == right.get();
		}

		bool operator!=(const IPv4 &left, const IPv4 &right)
		{
			return left.get() != right.get();
		}

		bool operator<(const IPv4 &left, const IPv4 &right)
		{
			return left.get() < right.get();
		}

		bool operator<=(const IPv4 &left, const IPv4 &right)
		{
			return left.get() <= right.get();
		}

		bool operator>(const IPv4 &left, const IPv4 &right)
		{
			return left.get() > right.get();
		}

		bool operator>=(const IPv4 &left, const IPv4 &right)
		{
			return left.get() >= right.get();
		}

		std::ostream &operator<<(std::ostream &stream, const IPv4 &ip)
		{
			char tmp[16];
			ip.print(tmp, 16);
			return stream << tmp;
		}
	}
}


