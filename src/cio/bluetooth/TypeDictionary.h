/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_TYPEDICTIONARY_H
#define CIO_BLUETOOTH_TYPEDICTIONARY_H

#include "Types.h"

#include "UniqueId.h"

#include <cio/Text.h>

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Type Dictionary is a class to match up Bluetooth Unique IDs with text descriptions.
		 * 
		 * It can be set up to work with a built-in compiled set of 16-bit type ID definitions plus any custom
		 * definitions that can be added by the user at runtime.
		 */
		class CIO_BLUETOOTH_API TypeDictionary
		{
			public:
				/**
				 * Construct an empty type dictionary.
				 */
				TypeDictionary() noexcept;
				
				/**
				 * Construct a deep copy of a type dictionary.
				 *
				 * @param in The dictionary to copy
				 * @throw std::bad_alloc If memory couldn't be allocated to do the copy
				 */
				TypeDictionary(const TypeDictionary &in);
				
				/**
				 * Construct a type dictionary by transferring the contents of the given one.
				 * 
				 * @param in The type dictionary to move
				 */
				TypeDictionary(TypeDictionary &&in) noexcept;
				
				/**
				 * Performs a deep copy of a type dictionary.
				 *
				 * @param in The dictionary to copy
				 * @return this dictionary
				 * @throw std::bad_alloc If memory couldn't be allocated to do the copy
				 */
				TypeDictionary &operator=(const TypeDictionary &in);
				
				/**
				 * Moves a type dictionary.
				 * 
				 * @param in The type dictionary to move
				 * @return this type dictionary
				 */
				TypeDictionary &operator=(TypeDictionary &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				~TypeDictionary() noexcept;
				
				/**
				 * Clears all custom unique ID names and resets the dictionary to use predefined types.
				 */
				void clear() noexcept;
				
				/**
				 * Gets whether this dictionary should use predefined names and type IDs for 16-bit standard Bluetooth UUIDs.
				 * If true, this will consult the built-in names if no custom name is defined for a 16-bit UUID.
				 * The default is true.
				 *
				 * @return whether to use predefined 16-bit standard Bluetooth UUID names
				 */
				bool getUsePredefinedTypes() noexcept;
				
				/**
				 * Sets whether this dictionary should use predefined names and type IDs for 16-bit standard Bluetooth UUIDs.
				 * If true, this will consult the built-in names if no custom name is defined for a 16-bit UUID.
				 *
				 * @param value whether to use predefined 16-bit standard Bluetooth UUID names
				 */
				void setUsePredefinedTypes(bool value) noexcept;
				
				/**
				 * Adds a definition for the given unique ID to have the given text name.
				 * This can be used for custom unique IDs or to override the predefined name of a standard 16-bit UUID.
				 *
				 * @param id The UUID
				 * @param name The name for the UUID
				 * @return whether the insert added a new entry
				 */
				bool insert(const UniqueId &id, Text name);
				
				/**
				 * Gets the UUID for a given text name.
				 * If no name matches it, then the empty UUID is returned.
				 *
				 * @param name The name
				 * @return the UUID
				 */
				UniqueId id(const Text &name) const noexcept;
				
				/**
				 * Gets the text name for a given UUID.
				 * If no UUID matches it, the static string "Unknown" is returned.
				 *
				 * @param id The UUID
				 * @return the name
				 */
				Text name(const UniqueId &id) const noexcept;
				
			private:
				/**
				 * Internal structure for entries.
				 */
				struct Entry
				{	
					/** Unique ID */
					UniqueId id;
					
					/** Text name */
					Text name;
				};
			
				/** List of all defined entries. Not expected to be large so we don't optimize it into a search structure. */
				std::vector<Entry> mEntries;
				
				/** Whether to use predefined name and type IDs for 16-bit standard Bluetooth UUIDs. */
				bool mUsePredefinedTypes;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

