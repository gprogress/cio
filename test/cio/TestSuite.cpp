/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "TestSuite.h"

#include "BufferTest.h"
#include "DeviceAddressTest.h"
#include "DirectoryTest.h"
#include "EncodingTest.h"
#include "FixedTextTest.h"
#include "JsonWriterTest.h"
#include "ParseTest.h"
#include "PathTest.h"
#include "ProgramTest.h"
#include "ProtocolTest.h"
#include "SignednessTest.h"
#include "StateTest.h"
#include "VersionTest.h"
#include "TemporaryFileTest.h"
#include "TextReaderTest.h"
#include "TextTest.h"
#include "TimeTest.h"

namespace cio
{
	TestSuite::TestSuite() :
		cio::test::Suite("cio")
	{
		this->addTestCase(cio::BufferTest());
		this->addTestCase(cio::DeviceAddressTest());
		this->addTestCase(cio::DirectoryTest());
		this->addTestCase(cio::EncodingTest());
		this->addTestCase(cio::FixedTextTest());
		this->addTestCase(cio::JsonWriterTest());
		this->addTestCase(cio::ParseTest());
		this->addTestCase(cio::PathTest());
		this->addTestCase(cio::ProgramTest());
		this->addTestCase(cio::ProtocolTest());
		this->addTestCase(cio::SignednessTest());
		this->addTestCase(cio::StateTest());
		this->addTestCase(cio::VersionTest());
		this->addTestCase(cio::TemporaryFileTest());
		this->addTestCase(cio::TextReaderTest());
		this->addTestCase(cio::TextTest());
		this->addTestCase(cio::TimeTest());
	}
	
	TestSuite::~TestSuite() noexcept = default;
}

