/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "IPv6.h"

#include "Flow.h"

#include <cio/Parse.h>
#include <cio/Print.h>

#include <cctype>
#include <ostream>

namespace cio
{
	namespace net
	{
		IPv6 IPv6::any() noexcept
		{
			return IPv6();
		}

		IPv6 IPv6::localhost() noexcept
		{
			return IPv6(0, 1);
		}
		
		bool IPv6::supports(Flow flow) noexcept
		{
			return flow != Flow::Broadcast;
		}

		std::size_t IPv6::printShort(std::uint16_t value, char *text, std::size_t length) noexcept
		{
			Lowercase c;
			std::size_t needed = 0;
			std::size_t off = 0;

			std::uint8_t nibble = static_cast<std::uint8_t>(value >> 12);

			if (nibble != 0)
			{
				++needed;

				if (off < length)
				{
					text[off++] = cio::printHexDigit(nibble, c);
				}
			}

			nibble = static_cast<std::uint8_t>(value >> 8);

			if (nibble != 0 || needed > 0)
			{
				++needed;

				if (off < length)
				{
					text[off++] = cio::printHexDigit(nibble, c);
				}
			}

			nibble = static_cast<std::uint8_t>(value >> 4);

			if (nibble != 0 || needed > 0)
			{
				++needed;

				if (off < length)
				{
					text[off++] = cio::printHexDigit(nibble, c);
				}
			}

			// The last nibble always gets printed even if it's 0
			nibble = static_cast<std::uint8_t>(value & 0xF);
			++needed;

			if (off < length)
			{
				text[off++] = cio::printHexDigit(nibble, c);
			}

			return needed;
		}

		std::size_t IPv6::print(char *text, std::size_t length) const noexcept
		{
			char buffer[48];
			char *tmp = length < 48 ? buffer : text;
			std::size_t needed = 0;
			bool zeroSequence = false;
			bool zeroDone = false;

			// common case - high is all zeros, so we know it's going to start with "::"
			if (mHigh == 0)
			{
				tmp[needed++] = ':';
				zeroSequence = true;
			}
			// High is nonzero, process normally
			else
			{
				// Look at inital short and determine if we start with a zero sequence
				std::uint16_t byte = static_cast<std::uint16_t>(mHigh >> 48);

				if (byte == 0)
				{
					zeroSequence = true;
					tmp[0] = ':';
					needed = 1;
				}
				else
				{
					needed += IPv6::printShort(byte, tmp, 48);
				}

				// Do remainder of high
				unsigned shift = 48;

				do
				{
					shift -= 16;
					byte = static_cast<std::uint16_t>(mHigh >> shift);

					if (byte != 0)
					{
						if (zeroSequence)
						{
							zeroSequence = false;
							zeroDone = true;
						}

						tmp[needed++] = ':';
						needed += IPv6::printShort(byte, tmp + needed, 48 - needed);
					}
					else if (!zeroDone)
					{
						if (!zeroSequence)
						{
							tmp[needed++] = ':';
							zeroSequence = true;
						}
					}
					else
					{
						tmp[needed++] = ':';
						needed += IPv6::printShort(byte, tmp + needed, 48 - needed);
					}
				}
				while (shift > 0);
			}

			// If low is all zeros, handle specially
			if (mLow == 0)
			{
				// High already finished a zero sequence, so we just have to print out the actual zeros
				if (zeroDone)
				{
					tmp[needed++] = ':';
					tmp[needed++] = '0';
					tmp[needed++] = ':';
					tmp[needed++] = '0';
					tmp[needed++] = ':';
					tmp[needed++] = '0';
					tmp[needed++] = ':';
					tmp[needed++] = '0';
				}
				// Either high was an entire zero sequence, or we're starting one
				else if (!zeroSequence)
				{
					tmp[needed++] = ':';
					zeroSequence = true;
				}
			}
			else
			{
				// Do all of low
				unsigned shift = 64;

				do
				{
					shift -= 16;
					std::uint16_t byte = static_cast<std::uint16_t>(mLow >> shift);

					if (byte != 0)
					{
						if (zeroSequence)
						{
							zeroSequence = false;
							zeroDone = true;
						}

						tmp[needed++] = ':';
						needed += IPv6::printShort(byte, tmp + needed, 48 - needed);
					}
					else if (!zeroDone)
					{
						if (!zeroSequence)
						{
							tmp[needed++] = ':';
							zeroSequence = true;
						}
					}
					else
					{
						tmp[needed++] = ':';
						needed += IPv6::printShort(byte, tmp + needed, 48 - needed);
					}
				}
				while (shift > 0);
			}

			// If we ended with a zero sequence, cap it off with the trailing ':'
			if (zeroSequence)
			{
				tmp[needed++] = ':';
			}

			// If we were using a temporary buffer, copy the portion that fits in the real buffer
			if (tmp != text)
			{
				std::memcpy(text, tmp, std::min(length, needed));
			}

			// If the real buffer had more space than needed, null terminate it
			if (length > needed)
			{
				std::memset(text + needed, 0, length - needed);
			}

			return needed;
		}

		std::string IPv6::print() const
		{
			char tmp[48] = { };
			std::size_t len = this->print(tmp, 48);
			return std::string(tmp, tmp + len);
		}

		TextParseStatus IPv6::parse(const char *text) noexcept
		{
			TextParseStatus result(0u, 0, Validation::Missing, Reason::Missing);

			if (text)
			{
				std::size_t len = std::strlen(text);
				result = this->parse(text, len + 1);
			}

			return result;
		}

		TextParseStatus IPv6::parse(const char *text, std::size_t len) noexcept
		{
			TextParseStatus result(0u, 0, Validation::Missing, Reason::Missing);
			mHigh = 0;
			mLow = 0;

			bool zeroSequence = false;
			std::uint16_t chunks[8] = { };
			std::size_t count = 0;
			std::size_t split = 8;
			unsigned nibble = 0;

			char neededTerminator = 0;
			char foundTerminator = 0;

			if (text && len > 0)
			{
				result.status = Validation::None;
				result.reason = Reason::None;

				if (text[0] == '[')
				{
					neededTerminator = ']';
					++result.parsed;
				}

				if (text[result.parsed] == ':')
				{
					if (text[result.parsed + 1] == ':')
					{
						split = 0;
						zeroSequence = true;
						result.parsed += 2;
					}
					else
					{
						result.status = Validation::InvalidValue;
					}
				}

				while (result.parsed < len && result.status == Validation::None)
				{
					char c = text[result.parsed];

					if (c == ':')
					{
						nibble = 0;

						if (result.parsed + 1 == len)
						{
							result.status = Validation::TooShort;
						}
						else
						{
							char n = text[result.parsed + 1];
							if (n == ':')
							{
								if (zeroSequence)
								{
									result.status = Validation::InvalidValue;
									break;
								}
								else
								{
									zeroSequence = true;
									split = ++count;
									++result.parsed;
								}
							}
							else
							{
								++count;
							}

							if (count >= 8)
							{
								result.status = Validation::TooMany;
								break;
							}
						}

						++result.parsed;
					}
					else if (std::isxdigit(c))
					{
						if (nibble >= 4)
						{
							result.status = Validation::AboveMaximum;
						}
						else
						{
							std::uint16_t parsed = cio::parseNibble(c);
							chunks[count] = (chunks[count] << 4) | parsed;
							++nibble;
						}
						++result.parsed;
					}
					else if (neededTerminator)
					{
						if (c == neededTerminator)
						{
							foundTerminator = c;
							++result.parsed;
						}
						else if (!c || std::isspace(c))
						{
							result.status = Validation::TooShort;
						}
						else
						{
							result.reason = Reason::Unexpected;
						}
						break;
					}
					else if (!c || std::isspace(c))
					{
						break;
					}
					else
					{
						result.reason = Reason::Unexpected;
						break;
					}
				}

				if (result.status == Validation::None)
				{
					// We finished with a hex digit value, so we had one more item
					if (nibble > 0)
					{
						++count;
					}

					if (count < 8 && !zeroSequence)
					{
						result.status = Validation::TooFew;
					}
					else
					{
						result.status = Validation::Exact;
					}
				}

				if (result.status == Validation::Exact)
				{
					if (zeroSequence)
					{
						std::size_t skipped = 8 - count;
						std::memmove(chunks + split + skipped, chunks + split, (count - split) * 2);
						std::memset(chunks + split, 0, skipped * 2);
					}

					mHigh = (static_cast<std::uint64_t>(chunks[0]) << 48) |
							(static_cast<std::uint64_t>(chunks[1]) << 32) |
							(static_cast<std::uint64_t>(chunks[2]) << 16) |
							(static_cast<std::uint64_t>(chunks[3]));
					mLow = (static_cast<std::uint64_t>(chunks[4]) << 48) |
							(static_cast<std::uint64_t>(chunks[5]) << 32) |
							(static_cast<std::uint64_t>(chunks[6]) << 16) |
							(static_cast<std::uint64_t>(chunks[7]));

					// If we used all the input and didn't have a terminating ']' the text parse might not be done
					if (result.parsed == len && neededTerminator != foundTerminator)
					{
						result.reason = Reason::Underflow;
					}
				}

				if (result.parsed < len)
				{
					result.terminator = text[result.parsed];
				}
			}

			return result;
		}

		TextParseStatus IPv6::parse(const std::string &text) noexcept
		{
			return this->parse(text.c_str(), text.size() + 1);
		}

		CIO_API std::ostream &operator<<(std::ostream &stream, const IPv6 &ip)
		{
			char tmp[48];
			ip.print(tmp, 48);
			return stream << tmp;
		}
	}
}


