/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_LOGMESSAGESTREAM_H
#define CIO_LOGMESSAGESTREAM_H

#include "Types.h"

#include "Severity.h"

#include <string>
#include <sstream>

namespace cio
{
	/**
	 * The Log Message Stream is a helper class to allow the Logger class to operate as a target of operator<<
	 * while incrementally building up log messages.
	 *
	 * The general template conditionally checks at runtime whether to build up the log messages into an output string stream,
	 * then logs the final result to the logger upon destruction or explicitly calling the log() method.
	 *
	 * @tparam <L> The type of Logger to use, which should typically be Logger
	 * @tparam <B> The boolean type indicating whether logging has been enabled at compile time
	 */
	template <typename L /* = Logger */, typename B /*= std::true_type */>
	struct LogMessageStream
	{
		/**
		 * Constructs a Log Message Stream for a given logger and severity.
		 *
		 * @param logger the logger
		 * @param context The context to provide to the logger
		 * @param severity The severity level
		 * @param enabled Whether logging is enabled at runtime
		 */
		LogMessageStream(L &logger, const char *context, Severity severity, bool enabled);

		/**
		 * Moves a Log Message Stream into this new one to pass it along the chain of operator<<
		 *
		 * @param in The log message builder to move into this one
		 */
		LogMessageStream(LogMessageStream<L, B> &&in) noexcept;

		/**
		 * Destructor. If the logging is enabled, logs any accumulated text into the original logger provided on construction.
		 */
		~LogMessageStream();

		/**
		 * If the logging is enabled, logs any accumulated text into the original logger provided on construction.
		 *
		 * @return whether logging occurred
		 */
		B log();

		/** The logger to use */
		L &logger;

		/** The context to provide to the logger */
		const char *context;

		/** The severity level of the accumulated message */
		Severity severity;

		/** The accumulated message */
		std::ostringstream text;

		/** Whether logging is enabled at runtime */
		bool enabled;
	};

	/**
	 * The Log Message Stream is a helper class to allow the Logger class to operate as a target of operator<<
	 * while incrementally building up log messages.
	 *
	 * The specialization for when logging is disabled at compile time (B = std::false_type) is streamlined to have no data and do nothing, and
	 * thus should be optimized out of actual runtime code.
	 *
	 * @tparam <L> The logger type being used
	 */
	template <typename L>
	struct LogMessageStream<L, std::false_type>
	{
		/**
		 * Default constructor.
		 */
		inline LogMessageStream() noexcept;

		/**
		 * Construct the compile time disabled logger.
		 *
		 * @param logger the logger, which is ignored
		 * @param context The context, which is ignored
		 * @param severity The severity, which is ignored
		 * @param enabled Whether logging is enabled at runtime, which is ignored
		 */
		inline LogMessageStream(L &logger, const char *context, Severity severity, bool enabled) noexcept;

		/**
		 * Finalizes logging, to meet the same API as the main template.
		 * This always returns false.
		 *
		 * @return std::false_type
		 */
		inline std::false_type log() noexcept;
	};

	/**
	 * Incrementally logs a given object as part of a log message. If logging is enabled on the given message builder, the given value is printed to a std::ostringstream using
	 * the regular operator<< for that data type. The contents of the log message builder are then transferred into the returned object for further use.
	 *
	 * @param in The log message builder containing the incremental log message progress so far
	 * @param value The value to log
	 * @return the next log message builder containing the log message progress including the given value (if enabled)
	 */
	template <typename L, typename B, typename T>
	inline LogMessageStream<L, B> operator<<(LogMessageStream<L, B> &&in, T value);

	/**
	 * Incrementally logs a given object as part of a log message. This overload specializes for when logging is disabled at compile time (B = std::false_type) to return
	 * an empty log message builder while ignoring the inputs.
	 *
	 * @param in The log message builder to ignore
	 * @param value The logged value, which will be ignored
	 * @return an empty compile-time-disabled log message builder
	 */
	template <typename L, typename T>
	inline LogMessageStream<L, std::false_type> operator<<(LogMessageStream<L, std::false_type> &&in, T value) noexcept;
}

/* Inline implementation */

namespace cio
{
	template <typename L, typename B>
	LogMessageStream<L, B>::LogMessageStream(L &logger, const char *context, Severity severity, bool enabled) :
		logger(logger),
		context(context),
		severity(severity),
		enabled(enabled)
	{
		// nothing more to do
	}

	template <typename L, typename B>
	LogMessageStream<L, B>::LogMessageStream(LogMessageStream<L, B> &&in) noexcept :
		logger(in.logger),
		context(in.context),
		severity(in.severity),
		text(std::move(in.text)),
		enabled(in.enabled)
	{
		in.enabled = false;
	}

	template <typename L, typename B>
	LogMessageStream<L, B>::~LogMessageStream()
	{
		if (this->enabled)
		{
			std::string message = this->text.str();
			this->logger.log(this->context, this->severity, message.c_str());
		}
	}

	template <typename L, typename B>
	B LogMessageStream<L, B>::log()
	{
		if (this->enabled)
		{
			std::string message = this->text.str();
			this->logger.log(this->context, this->severity, message.c_str());
			this->enabled = false;
		}

		return this->enabled;
	}

	template <typename L>
	inline LogMessageStream<L, std::false_type>::LogMessageStream() noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline LogMessageStream<L, std::false_type>::LogMessageStream(L &, const char *, Severity, bool) noexcept
	{
		// nothing to do
	}

	template <typename L>
	inline std::false_type LogMessageStream<L, std::false_type>::log() noexcept
	{
		return std::false_type();
	}

	template <typename L, typename B, typename T>
	inline LogMessageStream<L, B> operator<<(LogMessageStream<L, B> &&in, T value)
	{
		if (in.enabled)
		{
			in.text << value;
		}

		return std::move(in);
	}

	template <typename L, typename T>
	inline LogMessageStream<L, std::false_type> operator<<(LogMessageStream<L, std::false_type> &&, T) noexcept
	{
		return LogMessageStream<L, std::false_type>();
	}
}

#endif
