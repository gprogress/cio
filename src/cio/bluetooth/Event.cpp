/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Event.h"

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sEventText[] = {
			"None",
			"Inquiry Completed",
			"Inquiry Result",
			"Connection Completed",
			"Connection Requested",
			"Disconnection Completed",
			"Authentication Completed",
			"Remote Name Received",
			"Encryption Changed",
			"Connection Link Key Changed",
			"Master Link Key Completed",
			"Remote Features Read",
			"Remove Version Read",
			"Quality of Service Set Up",
			"Command Completed",
			"Command Status",
			"Hardware Error",
			"Flush Completed",
			"Role Changed",
			"Completed Packet Count",
			"Mode Changed",
			"Link Keys Returned",
			"PIN Code Requested",
			"Link Key Requested",
			"Link Key Provided",
			"Loopback",
			"Data Buffer Overflow",
			"Max Slots Changed",
			"Clock Offset Read",
			"Connection Packet Type Changed",
			"Quality of Service Violation",
			"Page Scan Mode Changed",
			"Page Scan Repetition Mode Changed",
			"Flow Control Specified",
			"Inquiry Result With RSSI",
			"Remote Extended Features Read",
			"Unknown", // gap 0x24
			"Unknown", // gap 0x25
			"Unknown", // gap 0x26
			"Unknown", // gap 0x27
			"Unknown", // gap 0x28
			"Unknown", // gap 0x29
			"Unknown", // gap 0x2A
			"Unknown", // gap 0x2B
			"Synchronous Connection Completed",
			"Synchronous Connection Changed",
			"Sniff Subrating",
			"Extended Inquiry Result",
			"Encryption Key Refreshed",
			"I/O Capability Requested",
			"I/O Capability Provided",
			"User Confirmation Requested",
			"User Passkey Requested",
			"Out of Band Data Requested",
			"Simple Pairing Completed",
			"Unknown", // gap 0x37
			"Link Supervision Timeout Changed",
			"Enhanced Flush Completed",
			"Unknown", // gap 0x3A
			"User Passkey Provided",
			"Keypress Provided",
			"Remote Low Energy Features Provided",
			"Low Energy Meta"
		};

		const char *print(Event evt) noexcept
		{
			const char *text = "Unknown";
			if (evt <= Event::LowEnergy)
			{
				text = sEventText[static_cast<std::size_t>(evt)];
			}
			return text;
		}
		
		std::ostream &operator<<(std::ostream &s, Event evt)
		{
			return s << print(evt);
		}
	}
}

