/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_RESOLVER_H
#define CIO_NET_RESOLVER_H

#include "Types.h"

#include "Service.h"

#include <cio/Path.h>

#include <vector>

namespace cio
{
	namespace net
	{
		/**
		 * The Resolver provides an interface to the native OS domain name resolution system.
		 * It provides two main sets of functionality: resolve hostnames into addresses (typically IP addresses) and
		 * to resolve service protocols into associated protocol types and ports.
		 */
		class CIO_NET_API Resolver
		{
			public:
				/**
				 * Construct a resolver.
				 * On platforms where it is necessary, this also initializes the socket library API.
				 */
				Resolver() noexcept;

				/**
				 * Construct a resolver.
				 * On platforms where it is necessary, this also initializes the socket library API.
				 *
				 * @param in The resolver to copy
				 */
				Resolver(const Resolver &in);

				/**
				 * Destructor.
				 * On platforms where it is necessary, this also deinitializes the socket library API.
				 */
				~Resolver() noexcept;

				/**
				 * Gets the standard port and socket type for a particular service protocol.
				 * An example input might be "http" and the output would be ProtocolFamily::tcp() and port 80.
				 * As a special case, the text "tcp" and the text "udp" return the corresponding ProtocolFamily with a port of 0.
				 *
				 * If the protocol name is actually the text for a number, that is parsed to set the output port
				 * and then used to lookup the service to figure out the protocol type.
				 *
				 * @param proto The protocol name
				 * @return the service protocol type and port
				 */
				std::pair<ProtocolFamily, std::uint16_t> findServicePort(const Text &proto) const;

				/**
				 * Gets the standard protocol name for the given protocol type and port.
				 * An example input might be ProtocolFamily::tcp() and port 80 and the output would be "http".
				 * As a special case, if the input port is 0, the baseline protocol "tcp" or "udp" would be returned.
				 *
				 * @param type The protocol type
				 * @param port The protocol port
				 * @return the standard name for the protocol
				 */
				std::string findServiceName(ProtocolFamily type, std::uint16_t port) const;

				/**
				 * Performs DNS name resolution on the given host name to obtain the Internet Address for it most recommended by the operating system.
				 * Whether this is an IPv4 or IPv6 address if both are available is up to the operating system.
				 *
				 * @param host The hostname
				 * @return the "best" address for the host name
				 */
				Address lookupAddress(const Text &host) const;

				/**
				 * Performs DNS name resolution on the given host name to obtain all Internet Addresses for it in the order recommended by the operating system.
				 * This may include both IPv4 and IPv6 addresses.
				 *
				 * @param host The hostname
				 * @return the list of addresses for the host name
				 */
				std::vector<Address> lookupAddresses(const Text &host) const;

				/**
				 * Examines the given Path URI to determine the Service description that best matches it to reach the given resource.
				 * If the URI includes a hostname, that will be used to perform DNS name resolution to get the most recommended address from the operating system.
				 * If the URI includes a protocol, that will be used to determine the channel type and underlying protocol type and default port.
				 * If the URI includes a port override, that will replace the default port and be used to determine channel type if the protocol is not specified.
				 *
				 * Whether the returned service is IPv4 or IPv6 if both are available is up to the operating system.
				 *
				 * @param path The URI to consider
				 * @return the service best representing the address, protocol type, and so on for connecting to the given resource
				 */
				Service lookupService(const cio::Path &path) const;

				/**
				 * Examines the given Path URI to determine the Service descriptions that can be used to reach the given resource.
				 * If the URI includes a hostname, that will be used to perform DNS name resolution to get the addresses from the operating system.
				 * If the URI includes a protocol, that will be used to determine the channel type and underlying protocol type and default port.
				 * If the URI includes a port override, that will replace the default port and be used to determine channel type if the protocol is not specified.
				 *
				 * The returned list may include both IPv4 and IPv6 services.
				 *
				 * @param path The URI to consider
				 * @return the list of service description representing how to connect to the given resource
				 */
				std::vector<Service> lookupServices(const cio::Path &path) const;

				/**
				 * Determines the Service description that best matches the given connection parameters.
				 * If the hostname is specified, that will be used to perform DNS name resolution to get the most recommended address from the operating system.
				 * If the protocol is specified, that will be used to determine the channel type and underlying protocol type and default port. If the protocol is a number, that is interpreted as the default port.
				 * If the port override is non-zero, that will be set on the service after al other factors are considered.
				 *
				 * Whether the returned service is IPv4 or IPv6 if both are available is up to the operating system.
				 *
				 * @param host The host name
				 * @param proto The protocol name or printed text of the port number
				 * @param portOverride The port to set on the service after all other factors are considered, or 0 to use the default
				 * @return the service best representing the address, protocol type, and so on for connecting to the given resource
				 */
				Service lookupService(const Text &host, const Text &proto, std::uint16_t portOverride = 0) const;

				/**
				 * Determines the list of Service descriptions that can be used for the given connection parameters.
				 * If the hostname is specified, that will be used to perform DNS name resolution to get the most recommended address from the operating system.
				 * If the protocol is specified, that will be used to determine the channel type and underlying protocol type and default port. If the protocol is a number, that is interpreted as the default port.
				 * If the port override is non-zero, that will be set on the service after al other factors are considered.
				 *
				 * The returned list may include both IPv4 and IPv6 services.
				 *
				 * @param host The host name
				 * @param proto The protocol name or printed text of the port number
				 * @param portOverride The port to set on the service after all other factors are considered, or 0 to use the default
				 * @return the list of services representing the address, protocol type, and so on for connecting to the given resource
				 */
				std::vector<Service> lookupServices(const Text &host, const Text &proto, std::uint16_t portOverride = 0) const;
		};
	}
}

#endif

