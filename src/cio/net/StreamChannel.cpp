/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "StreamChannel.h"

#include "Address.h"
#include "Resolver.h"

#include <cio/Class.h>
#include <cio/ModeSet.h>

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#include <sys/types.h>
#endif

#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2

#if defined CIO_HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif

#if defined CIO_HAVE_WS2TCPIP_H
#include <WS2tcpip.h>
#endif
#endif

namespace cio
{
	namespace net
	{
		Class<StreamChannel> StreamChannel::sMetaclass("cio::net::StreamChannel");

		const Metaclass &StreamChannel::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		StreamChannel::StreamChannel() noexcept
		{
			// nothing more to do
		}

		StreamChannel::StreamChannel(Socket &&socket) noexcept :
			Channel(std::move(socket))
		{
			// nothing more to do
		}

		StreamChannel::StreamChannel(StreamChannel &&in) noexcept :
			Channel(std::move(in))
		{
			// nothing more to do
		}

		StreamChannel &StreamChannel::operator=(StreamChannel &&in) noexcept
		{
			Channel::operator=(std::move(in));
			return *this;
		}

		StreamChannel::~StreamChannel() noexcept
		{
			// nothing more to do
		}

		const Metaclass &StreamChannel::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void StreamChannel::clear() noexcept
		{
			Channel::clear();
		}

		cio::ModeSet StreamChannel::openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory)
		{
			cio::ModeSet actualModes;
			Resolver resolver;
			Service remoteAddr = resolver.lookupService(resource);

			if (remoteAddr)
			{
				this->connect(remoteAddr);
			}

			if (mSocket)
			{
				actualModes = ModeSet::general();
			}

			return actualModes;
		}

		Progress<std::size_t> StreamChannel::requestRead(void *buffer, std::size_t length) noexcept
		{
			Progress<std::size_t> result = Channel::requestRead(buffer, length);

			if (result.count == 0 && length > 0 && result.succeeded())
			{
				result.fail(Reason::Underflow);
			}

			return result;
		}

		Progress<std::size_t> StreamChannel::requestWrite(const void *buffer, std::size_t length) noexcept
		{
			return Channel::requestWrite(buffer, length);
		}

		void StreamChannel::connect(const Service &service)
		{
			this->clear();
			mSocket.allocate(service);
			mSocket.connect(service.getAddress());
		}

		void StreamChannel::connect(const Address &address)
		{
			this->clear();
			mSocket.allocate(address.getAddressFamily(), ChannelType::Stream);
			mSocket.connect(address);
		}

		void StreamChannel::connect(const IPv4 &ip, std::int16_t port)
		{
			this->clear();
			mSocket.allocate(AddressFamily::ipv4(), ChannelType::Stream);
			Address address(ip, port);
			mSocket.connect(address);
		}

		void StreamChannel::connect(const IPv6 &ip, std::int16_t port)
		{
			this->clear();
			mSocket.allocate(AddressFamily::ipv6(), ChannelType::Stream);
			Address address(ip, port);
			mSocket.connect(address);
		}
	}
}



