/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Output.h"

#include "Buffer.h"
#include "Class.h"
#include "Metadata.h"
#include "ModeSet.h"
#include "Path.h"
#include "ProtocolFactory.h"
#include "Resize.h"
#include "Seek.h"

namespace cio
{
	Class<Output> Output::sMetaclass("Output");

	const Metaclass &Output::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &Output::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Output::~Output() noexcept = default;

	void Output::clear() noexcept
	{
		// nothing to do
	}

	bool Output::isOpen() const noexcept
	{
		return false;
	}

	Metadata Output::metadata() const
	{
		return Metadata(this->location(), this->modes(), this->type(), this->size());
	}

	Resource Output::type() const noexcept
	{
		return Resource::Unknown;
	}

	ModeSet Output::modes() const noexcept
	{
		return ModeSet();
	}

	Path Output::location() const
	{
		return Path();
	}

	Resize Output::resizable() const noexcept
	{
		return Resize::Unknown;
	}

	Progress<Length> Output::requestResize(Length length) noexcept
	{
		return Progress<Length>(Reason::Unsupported, 0, length);
	}

	ModeSet Output::openWithFactory(const Path& resource, ModeSet modes, ProtocolFactory* factory)
	{
		return ModeSet();
	}

	ModeSet Output::openToWrite(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Load | Mode::Replace | Mode::Write, ProtocolFactory::getDefault());
	}

	ModeSet Output::createToWrite(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Create | Mode::Write, ProtocolFactory::getDefault());
	}

	ModeSet Output::openOrCreateToWrite(const Path &resource)
	{
		return this->openWithFactory(resource, Mode::Create | Mode::Load | Mode::Replace | Mode::Write, ProtocolFactory::getDefault());
	}

	Length Output::size() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	bool Output::empty() const noexcept
	{
		return this->size() == 0;
	}

	Length Output::capacity() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	bool Output::appendable() const noexcept
	{
		return true;
	}

	Length Output::calculateWritePosition(Length value, Seek mode) const noexcept
	{
		Length result = CIO_UNKNOWN_LENGTH;

		switch (mode)
		{
			case Seek::None:
				result = 0;
				break;

			case Seek::Begin:
				result = value;
				break;

			case Seek::Current:
			{
				Length current = this->getWritePosition();
				if (current.known())
				{
					result = current + value;
				}
				break;
			}

			case Seek::End:
			{
				Length base = this->size();
				if (base.known())
				{
					result = base + value;
				}
				break;
			}

			default:
				break;
		}

		return result;
	}

	Length Output::writable() const noexcept
	{
		Length available = CIO_UNKNOWN_LENGTH;
		Length current = this->getWritePosition();

		if (current.known())
		{
			Length total = this->capacity();
			if (total.known())
			{
				if (current < total)
				{
					available = total - current;
				}
				else
				{
					available = 0;
				}
			}
		}

		return available;
	}

	Resize Output::isWriteSeekable() const noexcept
	{
		return Resize::Unknown;
	}

	void Output::skip(Length bytes)
	{
		Progress<Length> current = this->requestSkip(bytes);
		while (current.count < bytes && !current.failed())
		{
			current += this->requestSkip(bytes - current.count);
		}

		if (current.count != bytes)
		{
			throw Exception(current, "Could not skip output bytes");
		}
	}

	void Output::splat(std::uint8_t value, Length bytes)
	{
		Progress<Length> current = this->requestSplat(value, bytes);
		while (current.count < bytes && !current.failed())
		{
			current += this->requestSplat(value, bytes - current.count);
		}

		if (current.count != bytes)
		{
			throw Exception(current, "Could not splat value to output bytes");
		}
	}

	void Output::splatSequence(const void *bytes, std::size_t length, Length count)
	{
		if (length > 0 && count > 0)
		{
			if (length == 1)
			{
				this->splat(*static_cast<const std::uint8_t *>(bytes), count);
			}
			else
			{
				for (std::size_t i = 0; i < count; ++i)
				{
					this->write(bytes, length);
				}
			}
		}
	}
	
	void Output::seekToWrite(Length position, Seek mode)
	{
		Progress<Length> current = this->requestSeekToWrite(position, mode);
		while (current.unfinished())
		{
			current = this->requestSeekToWrite(position, mode);
		}

		if (current.failed())
		{
			throw Exception(current, "Could not set write position");
		}
	}

	void Output::write(const void *data, std::size_t length)
	{
		if (length > 0)
		{
			Progress<std::size_t> current = this->requestWrite(data, length);
			while (current.count < length && !current.failed())
			{
				current += this->requestWrite(static_cast<const std::uint8_t *>(data) + current.count, length - current.count);
			}

			if (current.count < length)
			{
				throw Exception(current, "Could not write bytes");
			}
		}
	}

	void Output::writeToPosition(const void *data, std::size_t length, Length position, Seek mode)
	{
		if (length > 0)
		{
			Progress<std::size_t> current = this->requestWriteToPosition(data, length, position, mode);
			while (current.count < length && !current.failed())
			{
				current += this->requestWriteToPosition(static_cast<const std::uint8_t *>(data) + current.count, 
					length - current.count, position + current.count, mode);
			}

			if (current.count < length)
			{
				throw Exception(current, "Could not write bytes");
			}
		}
	}
	
	Progress<std::size_t> Output::requestWrite(const void * /*data*/, std::size_t /*count*/) noexcept
	{
		return fail(Action::Write, Reason::Unsupported);
	}

	Length Output::getWritePosition() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	Progress<Length> Output::requestSeekToWrite(Length /*pos*/, Seek /*mode*/) noexcept
	{
		return fail(Action::Seek, Reason::Unsupported);
	}

	Progress<std::size_t> Output::requestWriteToPosition(const void * /*data*/, std::size_t /*length*/, Length /*position*/, Seek /*mode*/) noexcept
	{
		return fail(Action::Write, Reason::Unsupported);
	}

	Progress<Length> Output::requestSkip(Length bytes) noexcept
	{
		Progress<Length> progress = this->requestSplat(0, bytes);
		if (progress.succeeded())
		{
			progress.reason = Reason::Unseekable;
		}
		return progress;
	}

	Progress<Length> Output::requestSplat(std::uint8_t value, Length count) noexcept
	{
		Progress<Length> result(Action::Write, 0, count);
		unsigned char tmp[512];
		Length bufsize(512);

		std::size_t toStore = std::min(count, bufsize).size();
		std::memset(tmp, value, toStore);

		Progress<std::size_t> chunkProgress = this->requestWrite(tmp, toStore);
		result += chunkProgress;

		while (!chunkProgress.failed() && result.count < count)
		{
			Length remaining = count - chunkProgress.count;
			toStore = std::min(remaining, bufsize).size();
			chunkProgress = this->requestWrite(tmp, toStore);
			result += chunkProgress;
		}

		return result;
	}

	Progress<std::size_t> Output::requestWriteBuffer(std::size_t /*bytes*/) noexcept
	{
		return fail(Action::Write, Reason::Unsupported);
	}

	Progress<Traversal> Output::flush(Traversal /*recursive*/) noexcept
	{
		return fail(Action::Write, Reason::Unsupported);
	}
	
	State Output::adviseUpcomingWrite(Length pos, Seek mode, std::size_t bytes) noexcept
	{
		return fail(Action::Write, Reason::Unsupported);
	}
	
	std::size_t Output::putText(const char *text)
	{
		std::size_t length = 0;
		
		if (text)
		{
			length = std::strlen(text);
			this->write(text, length);
		}
		
		return length;
	}
	
	std::size_t Output::putText(const char *text, std::size_t maxLength)
	{
		std::size_t length = 0;

		if (text)
		{
			length = terminator(text, maxLength);
			this->write(text, length);
		}

		return length;
	}
	
	std::size_t Output::putText(const std::string &text)
	{
		this->write(text.c_str(), text.size());
		return text.size();
	}

	std::size_t Output::putText(const Text &text)
	{
		std::size_t length = text.strlen();
		this->write(text.data(), length);
		return length;
	}

	Progress<std::size_t> Output::requestDrain(Buffer &buffer) noexcept
	{
		Progress<std::size_t> result = this->requestWrite(buffer.current(), buffer.remaining());
		buffer.skip(result.count);
		return result;
	}

	Progress<std::size_t> Output::drain(Buffer &buffer)
	{
		Progress<std::size_t> result;
		
		try
		{
			std::size_t bytes = buffer.remaining();
			this->write(buffer.current(), bytes);
			buffer.skip(bytes);
		}
		catch (const Exception &e)
		{
			buffer.skip(e.progress().count);
			throw;
		}
		
		return result;
	}

	Output::operator bool() const noexcept
	{
		return this->isOpen();
	}
}
