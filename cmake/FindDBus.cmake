#######################################################################################################################
# Copyright 2022-2023, Geometric Progress LLC
#
# This file is part of the Careful I/O Library (CIO).
#
# CIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CIO.  If not, see <https://www.gnu.org/licenses/>
#######################################################################################################################
include(ManifestFind)

# Basic content
MANIFEST_SHARED_LIBRARY(DBus "dbus/dbus.h" dbus-1)
set(DBus_INCLUDE_SUFFIXES "dbus-1.0")

MANIFEST_FIND_PACKAGE(DBus)

# DBus also has an architecture-specific include in a nonstandard location
if(TARGET DBus::DBus)
	set(_dbus_arch_hints )
	if(MINGW)	
		foreach(_dir ${CMAKE_LIBRARY_PATH} ${CMAKE_SYSTEM_LIBRARY_PATH})
			list(APPEND _dbus_arch_hints "${_dir}/../lib/dbus-1.0/include")
		endforeach()
	else()
		set(_dbus_arch_hints "/usr/lib/${CMAKE_LIBRARY_ARCHITECTURE}/dbus-1.0/include" "/usr/lib/dbus-1.0/include")
	endif()
	
	message(STATUS "Looking for DBus arch dir: ${_dbus_arch_hints}")
	find_path(DBus_ARCH_INCLUDE_DIR "dbus/dbus-arch-deps.h" HINTS ${_dbus_arch_hints})
	if (DBus_ARCH_INCLUDE_DIR)
		set_property(TARGET DBus::DBus APPEND PROPERTY
			INTERFACE_INCLUDE_DIRECTORIES "${DBus_ARCH_INCLUDE_DIR}")
		# target_include_directories doesn't work on imported targets in older versions of CMake
		# target_include_directories(DBus::DBus PUBLIC INTERFACE ${DBus_ARCH_INCLUDE_DIR})
	else()
		set(DBus_FOUND 0)
	endif()
endif()

MANIFEST_VALIDATE_FOUND_PACKAGE(DBus)
