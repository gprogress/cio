/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_DEVICE_H
#define CIO_BLUETOOTH_DEVICE_H

#include "Types.h"

#include <cio/net/Channel.h>

#include <functional>
#include <iosfwd>
#include <string>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Device provides the core representation of the identity of a Bluetooth device available on
		 * the local system.
		 *
		 * The Device class supports both classic (BR/EDR) and low-energy (BLE) devices.
		 *
		 * Most users will want to user the higher-level Adapter class to access a particular Device with a platform-appropriate Controller.
		 */
		class CIO_BLUETOOTH_API Device
		{
			public:
				/**
				 * Gets the metaclass for the cio::bluetooth::Device class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Gets the "invalid" device index indicating no device is active.
				 *
				 * @return the invalid device index
				 */
				static std::uintptr_t getInvalidIndex() noexcept;
				
				/**
				 * Construct an unconnected device.
				 */
				Device() noexcept;
				
				/**
				 * Construct a device given its index.
				 *
				 * @param idx The index
				 */
				explicit Device(std::uintptr_t idx) noexcept;
				
				/**
				 * Construct a device given its index and address type.
				 *
				 * @param idx The index
				 * @param type the address type
				 */
				Device(std::uintptr_t idx, AddressType type) noexcept;

				/**
				 * Construct a device by attempting to parse a text string for a recognized device.
				 * Currently this matches the string "HCI#%u" where %u is an unsigned integer.
				 * If parsing fails, the device is initialized to the invalid device index
				 *
				 * @param text The text to parse
				 * @param length The number of bytes in the text
				 */
				explicit Device(const Text &text) noexcept;
				
				/**
				 * Copies the state of a device.
				 * This copies the handle but not the HCI connection.
				 *
				 * @param in The device to copy
				 */
				Device(const Device &in);
				
				/**
				 * Constructs a device by adopting the given state of another device.
				 *
				 * @param in The device to move
				 */
				Device(Device &&in) noexcept;
				
				/**
				 * Copies the state of the device.
				 * The existing state is cleared first.
				 * The HCI connection is not copied.
				 *
				 * @param in The device to copy
				 * @return this device
				 */
				Device &operator=(const Device &in);
				
				/**
				 * Moves device state into this device.
				 * The existing state is cleared first.
				 *
				 * @param in The device to move
				 * @return this device
				 */
				Device &operator=(Device &&in) noexcept;
				
				/**
				 * Destructor. Cleans up all open device resources.
				 */
				~Device() noexcept;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				const Metaclass &getMetaclass() const noexcept;
				
				/**
				 * Clears the device state.
				 */				
				void clear() noexcept;
				
				/**
				 * Opens the specific local device by its index.
				 *
				 * @param index The index of the device to open
				 */
				void setIndex(std::uintptr_t index);

				// Local device state - requires device to be open
								
				/**
				 * Gets the index of the currently open device.
				 * This is just the native device handle.
				 *
				 * @return the native device index
				 */
				std::uintptr_t getIndex() const noexcept;
								
				/**
				 * Sets how scanning and connection methods report this scanner's address type.
				 * This can be Public or Random for low-energy mode or Classic for BR/EDR functionality.
				 * The default is low-energy Public address type if the device is open and None otherwise.
				 *
				 * @return the address type currently in use for the scanner
				 */
				AddressType getAddressType() const noexcept;
				
				/**
				 * Sets how scanning and connection methods report this scanner's address type.
				 * This can be Public or Random for low-energy mode or Classic for BR/EDR functionality.
				 *
				 * @param value the address type to use for the scanner
				 */
				void setAddressType(AddressType value) noexcept;
				
				/**
				 * Checks whether this device is currently set to a low energy address mode.
				 *
				 * @return whether the device is low energy
				 */
				bool isLowEnergy() const noexcept;
				
				/**
				 * Prints this device identifier to the given buffer.
				 * Currently this prints the string "HCI #%u" substituting the device index if the index is valid or "None" otherwise.
				 *
				 * @param buffer The text buffer to print into
				 * @param length The length of the text buffer
				 * @return the actual length that would be needed to fully print the value
				 */
				std::size_t print(char *buffer, std::size_t length) const noexcept;
								
				/**
				 * Attempts to parse a text string for a recognized device.
				 * Currently this matches the string "HCI#%u" where %u is an unsigned integer.
				 *
				 * @param text The text to parse
				 * @return the parsing status
				 */
				TextParseStatus parse(const Text &text) noexcept;
				
				/**
				 * Interprets whether a device index has been set.
				 * 
				 * @return whether a device index is set
				 */
				explicit operator bool() const noexcept;
				
			private:
				/** The metaclass for this class */
				static Class<Device> sMetaclass;
				
				/** The native device index */
				std::uintptr_t mDevice;
				
				/** Address type of the device */
				AddressType mAddressType;

		};
		
		/**
		 * Check to see if two devices are equal. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the devices are equal
		 */
		CIO_BLUETOOTH_API bool operator==(const Device &left, const Device &right) noexcept;
		
		/**
		 * Check to see if two devices are not equal. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the devices are not equal
		 */
		CIO_BLUETOOTH_API bool operator!=(const Device &left, const Device &right) noexcept;
		
		/**
		 * Check to see if one device sorts before another. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the first device is less than the second device
		 */		
		CIO_BLUETOOTH_API bool operator<(const Device &left, const Device &right) noexcept;
		
		/**
		 * Check to see if one device sorts before or equal to another. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the first device is less than or equal to the second device
		 */
		CIO_BLUETOOTH_API bool operator<=(const Device &left, const Device &right) noexcept;
		
		/**
		 * Check to see if one device sorts after another. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the first device is greater than the second device
		 */		
		CIO_BLUETOOTH_API bool operator>(const Device &left, const Device &right) noexcept;
		
		/**
		 * Check to see if one device sorts after than or equal to another. This is done by device index.
		 *
		 * @param left The first device
		 * @param right The second device
		 * @return whether the first device is greater than the second device
		 */
		CIO_BLUETOOTH_API bool operator>=(const Device &left, const Device &right) noexcept;
		
		/**
		 * Prints a device identifier to a C++ stream.
		 *
		 * @param stream the C++ stream
		 * @param dev the device
		 * @return the stream
		 * @throw std::exception If the stream throws an exception
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, const Device &dev);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

