/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "UniqueId.h"

#include "Class.h"
#include "Parse.h"
#include "Print.h"
#include "Status.h"
#include "Text.h"

#include <cctype>
#include <cstdlib>
#include <ostream>

namespace cio
{
	Class<UniqueId> UniqueId::sMetaclass("cio::UniqueId");
	
	const Metaclass &UniqueId::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}
	
	UniqueId UniqueId::generate() noexcept
	{
		// TODO implement actual UUID generation here
		return UniqueId::random();
	}

	UniqueId UniqueId::random() noexcept
	{
		UniqueId result;

		std::uint64_t high = (static_cast<std::uint64_t>(std::rand()) << 32) | static_cast<std::uint64_t>(std::rand());
		std::uint64_t low = (static_cast<std::uint64_t>(std::rand()) << 32) | static_cast<std::uint64_t>(std::rand());

		result.set(high, low);

		return result;
	}
		
	UniqueId::UniqueId() noexcept :
		first(0),
		second(0)
	{
		// nothing more to do
	}

	UniqueId::UniqueId(const UniqueId &in) noexcept = default;

	UniqueId &UniqueId::operator=(const UniqueId &in) noexcept = default;

	UniqueId::~UniqueId() noexcept = default;
	
#if CIO_ENDIAN == CIO_BIG_ENDIAN
	UniqueId::UniqueId(std::uint64_t high, std::uint64_t low) noexcept :
		first(high),
		second(low)
	{
		// nothing more to do
	}
	
	void UniqueId::set(std::uint64_t high, std::uint64_t low) noexcept
	{
		this->first = high;
		this->second = low;
	}
	
	std::uint64_t UniqueId::high() const noexcept
	{
		return this->first;
	}
	
	std::uint64_t UniqueId::low() const noexcept
	{
		return this->second;
	}
	
#elif CIO_ENDIAN == CIO_LITTLE_ENDIAN
	UniqueId::UniqueId(std::uint64_t high, std::uint64_t low) noexcept :
		first(low),
		second(high)
	{
		// nothing more to do
	}
	
	void UniqueId::set(std::uint64_t high, std::uint64_t low) noexcept
	{
		this->first = low;
		this->second = high;
	}
	
	std::uint64_t UniqueId::high() const noexcept
	{
		return this->second;
	}
	
	std::uint64_t UniqueId::low() const noexcept
	{
		return this->first;
	}
	
#else
	UniqueId::UniqueId(std::uint64_t high, std::uint64_t low) noexcept :
	{
		if (cio::detectRuntimeByteOrder() == ByteOrder::BigEndian())
		{
			this->first = high;
			this->second = low;
		}
		else
		{
			this->first = low;
			this->second = high;
		}
	}
	
	void UniqueId::set(std::uint64_t high, std::uint64_t low) noexcept
	{
		if (cio::detectRuntimeByteOrder() == ByteOrder::BigEndian())
		{
			this->first = high;
			this->second = low;
		}
		else
		{
			this->first = low;
			this->second = high;
		}
	}
	
	std::uint64_t UniqueId::high() const noexcept
	{
		return (cio::detectRuntimeByteOrder() == ByteOrder::BigEndian()) ? this->first : this->second;
	}
	
	std::uint64_t UniqueId::low() const noexcept
	{
		return (cio::detectRuntimeByteOrder() == ByteOrder::BigEndian()) ? this->second : this->first;
	}
#endif
	
	std::size_t UniqueId::print(char *buffer, std::size_t len) const noexcept
	{
		const std::uint8_t *bigBytes;
#if CIO_ENDIAN == CIO_BIG_ENDIAN
		bigBytes = this->data();
#else
		UniqueId bigId = Big::order(*this);
		bigBytes = bigId.data();
#endif
		char tmp[36];
		char *target = (len >= 36) ? buffer : tmp;
		char *current = target;
		
		current += cio::printBytes(bigBytes, 4, current, 8);
		*current++ = '-';
		
		current += cio::printBytes(bigBytes + 4, 2, current, 4);
		*current++ = '-';
		
		current += cio::printBytes(bigBytes + 6, 2, current, 4);
		*current++ = '-';
		
		current += cio::printBytes(bigBytes + 8, 2, current, 4);
		*current++ = '-';
		
		current += cio::printBytes(bigBytes + 10, 6, current, 12);
		
		if (target != buffer)
		{
			std::memcpy(buffer, target, len);
		}
		else if (len > 36)
		{
			std::memset(current, 0, len - 36);
		}
		
		return 36;
	}

	
	std::string UniqueId::print() const
	{
		char tmp[37];
		this->print(tmp, 37);
		return std::string(tmp);
	}

	void UniqueId::clear() noexcept
	{
		this->first = 0;
		this->second = 0;
	}

	const std::uint8_t *UniqueId::data() const noexcept
	{
		return reinterpret_cast<const std::uint8_t *>(this);
	}

	std::uint8_t *UniqueId::data() noexcept
	{
		return reinterpret_cast<std::uint8_t *>(this);
	}
	
	TextParseStatus UniqueId::parse(const Text &text) noexcept
	{
		TextParseStatus parse;
		
		this->clear();
		
		if (text)
		{
			const char *current = text.begin();
			const char *end = text.end();
			
			// Skip leading whitespace
			while (std::isspace(*current))
			{
				++current;
			}
		
			bool matched = true;
		
			// Some UUID formats have brackets around them, match them up and ignore them otherwise
			if (*current == '[')
			{
				++parse.parsed;
				matched = *(end - 1) == ']';
				++current;
				--end;
			}
			else if (*current == '{')
			{
				++parse.parsed;
				matched = (*(end - 1) == '}');
				++current;
				--end;
			}
			
			if (matched)
			{
				std::uint8_t bytes[16] = { };
			
				for (std::size_t i = 0; i < 16; ++i)
				{	
					// Skip '-' characters
					while (current < end && *current == '-')
					{
						++parse.parsed;
						++current;
					}
					
					// See if we have two input bytes for next hex digit
					if (current + 1 < end)
					{
						bool parsed = cio::parseByte(current[0], current[1], bytes + i);
						
						if (!parsed)
						{
							parse.reason = Reason::Unexpected;
							parse.status = Validation::InvalidValue;
							current = end;
							break;
						}
						
						current += 2;
						parse.parsed += 2;
					}
					else
					{
						parse.status = Validation::TooShort;
						current = end;
						break;
					}
				}
				
				if (current < end && *current != 0)
				{
					parse.status = Validation::TooLong;
				}
				
				
				std::uint64_t high = 0;
				std::uint64_t low = 0;
				
				std::memcpy(&high, bytes, 8);
				std::memcpy(&low, bytes + 8, 8);

				this->set(Big::order(high), Big::order(low));
			}
			else
			{
				parse.status = Validation::InvalidValue;
			}
		}
		else
		{
			parse.status = Validation::Missing;
		}
		
		return parse;
	}
			
	UniqueId::operator bool() const noexcept
	{
		return this->first != 0 || this->second != 0;
	}

	bool operator==(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.first == right.first && left.second == right.second;
	}

	bool operator!=(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.first != right.first || left.second != right.second;
	}

	bool operator<(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.high() < right.high() || (left.high() == right.high() && left.low() < right.low());
	}

	bool operator<=(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.high() < right.high() || (left.high() == right.high() && left.low() <= right.low());
	}

	bool operator>(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.high() > right.high() || (left.high() == right.high() && left.low() >= right.low());
	}

	bool operator>=(const UniqueId &left, const UniqueId &right) noexcept
	{
		return left.high() > right.high() || (left.high() == right.high() && left.low() >= right.low());
	}
	
	UniqueId swapBytes(const UniqueId &id) noexcept
	{
		return UniqueId(swapBytes(id.low()), swapBytes(id.high()));
	}
	
	std::ostream &operator<<(std::ostream &stream, const UniqueId &id)
	{
		char tmp[37];
		id.print(tmp, 37);
		return stream << tmp;
	}
}
