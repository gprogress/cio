/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MEMORYINPUT_H
#define CIO_MEMORYINPUT_H

#include "Types.h"

#include "Input.h"

#include "ReadBuffer.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	* The Memory Input adapts a Read Buffer to support the Input interface.
	* It is useful for presenting in-memory byte sequences as an input.
	*/
	class CIO_API MemoryInput : public cio::Input
	{
		public:
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct a stream.
			*/
			MemoryInput() noexcept;

			MemoryInput(const void *data, std::size_t length) noexcept;
			
			MemoryInput(const MemoryInput &in);

			MemoryInput(MemoryInput &&in) noexcept;

			MemoryInput &operator=(const MemoryInput &in);
			
			MemoryInput &operator=(MemoryInput &&in) noexcept;
		
			virtual ~MemoryInput() noexcept override;

			virtual const Metaclass &getMetaclass() const noexcept override;

			void bind(ReadBuffer buffer);

			void bind(const void *data, std::size_t length);

			ReadBuffer unbind();

			/**
			 * Gets direct access to the underlying current buffer.
			 * Modifications to the buffer will be reflected in the channel.
			 *
			 * @return the buffer
			 */
			ReadBuffer &buffer() noexcept;

			/**
			 * Gets direct access to the underlying current buffer.
			 *
			 * @return the buffer
			 */
			const ReadBuffer &buffer() const noexcept;

			/**
			 * Clears the memory input and buffer.
			 */
			virtual void clear() noexcept override;
				
			/**
			* Checks whether the input is open.
			* This is true if the underlying memory buffer has data.
			*
			* @return whether the channel is open
			*/
			virtual bool isOpen() const noexcept override;

			/**
			 * Gets the current size of the memory buffer backing this channel.
			 *
			 * @return the current buffer size
			 */
			virtual Length size() const noexcept override;

			/**
			 * Checks whether the resource is empty and contains zero bytes.
			 *
			 * @return whether the resource is emtpy
			 */
			virtual bool empty() const noexcept override;

			virtual Length readable() const noexcept override;

			virtual Progress<std::size_t> requestRead(void *data, std::size_t bytes) noexcept override;

			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			virtual Length getReadPosition() const noexcept override;

			virtual Progress<Length> requestSeekToRead(Length position, Seek mode) noexcept override;

			virtual Progress<Length> requestDiscard(Length bytes) noexcept override;

			/**
			 * @copydoc Input::getText
			 * 
			 * This override directly implements searching the memory buffer for the text end.
			 */
			virtual Text getText(std::size_t bytes = SIZE_MAX) override;

			/**
			 * @copydoc Input::getTextLine
			 *
			 * This override directly implements searching the memory buffer for the text line end.
			 */
			virtual Text getTextLine(std::size_t bytes = SIZE_MAX) override;

		private:
			/** The metaclass for this class */
			static Class<MemoryInput> sMetaclass;
				
			ReadBuffer mBuffer;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
