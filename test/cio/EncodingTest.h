/************************************************************************
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ENCODINGTEST_H
#define CIO_ENCODINGTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	* Tests the basic functionality of the Encoding class.
	*/
	class EncodingTest : public cio::test::Suite
	{
		public:
			/**
			 * Construct the test suite and test cases.
			 */
			EncodingTest();
			
			/**
			 * Destructor.
			 */
			virtual ~EncodingTest() noexcept override;

			/**
			 * Tests that all Encoding constructors result in correct initial values.
			 */
			void testConstruction();

			/**
			 * Tests that operator= copies values.
			 */
			void testAssignment();

			/**
			 * Tests that clear() resets valus to default state.
			 */
			void testClear();
			
			/**
			 * Tests that Encoding::create<T> results in correct values for type T for built-in C++ types.
			 */
			void testCreate();
			
			/**
			 * Tests Encoding::createNormalized.
			 */
			void testCreateNormalized();
			
			/**
			 * Tests Encoding::createSignedNormalized.
			 */
			void testCreateSignedNormalized();
			
			/**
			 * Tests Encoding::isSigned.
			 */
			void testIsSigned();
			
			/**
			 * Tests Encoding::setSigned.
			 */
			void testSetSigned();
			
			/**
			 * Tests Encoding::hasSignBit.
			 */
			void testHasSignBit();
			
			/**
			 * Tests Encoding::setSignBit.
			 */
			void testSetSignBit();
			
			/**
			 * Tests Encoding::setTwosComplement and Encoding::isTwosComplement.
			 */
			void testSetTwosComplement();
			
			/**
			 * Tests Encoding::isUnsignedInteger.
			 */
			void testIsUnsignedInteger();
			
			/**
			 * Tests Encoding::isStandardInteger.
			 */
			void testIsStandardInteger();
			
			/**
			 * Tests Encoding::isStandardUnsigned.
			 */
			void testIsStandardUnsigned();
			
			/**
			 * Tests Encoding::isStandardSigned.
			 */
			void testIsStandardSigned();
			
			/**
			 * Tests Encoding::setNormalized and Encoding::isNormalized.
			 */
			void testSetNormalized();
			
			/**
			 * Tests Encoding::setNoData and Encoding::hasNoData.
			 */
			void testSetNoData();
			
			/**
			 * Tests Encoding::setInfinity and Encoding::hasInfinity.
			 */
			void testSetInfinity();
			
			/**
			 * Tests Encoding::setMantissaBits and Encoding::getMantissaBits.
			 */
			void testSetMantissaBits();
			
			/**
			 * Tests Encoding::setExponentBits and Encoding::getExponentBits.
			 */
			void testSetExponentBits();
			
			/**
			 * Tests Encoding::setExponentBias and Encoding::getExponentBias.
			 */
			void testSetExponentBias();
			
			/**
			 * Tests Encoding::setFloatingPoint and Encoding::isFloatingPoint.
			 */
			void testSetFloatingPoint();
			
			/**
			 * Tests Encoding::isStandardFloat.
			 */
			void testIsStandardFloat();
			
			/**
			 * Tests Encoding::isBelowRepresentation.
			 */
			void testIsBelowRepresentation();
			
			/**
			 * Tests Encoding::isAboveRepresentation.
			 */
			void testIsAboveRepresentation();
			
			/**
			 * Tests Encoding::pack and Encoding::unpack.
			 */
			void testPack();

			/**
			 * Tests Encoding::setEncoding.
			 */
			void testSetEncoding();

			/**
			 * Tests Encoding::computeRequiredBits.
			 */
			void testComputeRequiredBits();

			/**
			 * Tests Encoding::computeRequiredBytes.
			 */
			void testComputeRequiredBytes();

			/**
			 * Tests Encoding::is.
			 */
			void testIs();

			/**
			 * Tests Encoding::isCopyableTo.
			 */
			void testIsCopyableTo();

			/**
			 * Tests Encoding::isPrimitive.
			 */
			void testIsPrimitive();

			/**
			 * Tests Encoding::setReal and Encoding::hasReal.
			 */
			void testSetReal();

			/**
			 * Tests Encoding::setImaginary and Encoding::hasImaginary.
			 */
			void testSetImaginary();
			
			/**
			 * Tests Encoding::equalWithinULP.
			 */
			void testEqualWithinULP();
	};
}

#endif
