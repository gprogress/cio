/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEXTTEST_H
#define CIO_TEXTTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the base cio::Directory class.
	 * This class is effectively a mock empty directory, so we confirm the base methods work as advertised.
	 */
	class TextTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructor.
			 */
			TextTest();

			/**
			 * Destructor.
			 */
			virtual ~TextTest() noexcept override;

			/**
			 * Tests that the default constructor works properly.
			 */
			void defaultConstructorTest();

			/**
			 * Tests that the constructor using std::string works properly.
			 */
			void stringConstructorTest();

			/**
			 * Tests that assignment using std::string works properly.
			 */
			void stringAssignmentTest();

			/**
			 * Tests that bind using std::string works properly.
			 */
			void stringBindTest();

			/**
			 * Tests that Text::toLower() works properly.
			 */
			void toLowerTest();

			/**
			 * Tests that Text::toUpper() works properly.
			 */
			void toUpperTest();
	};
}

#endif

