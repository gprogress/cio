/*==============================================================================
 * Copyright 2023-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MARKUPWRITER_H
#define CIO_MARKUPWRITER_H

#include "Types.h"

#include "Buffer.h"
#include "Factory.h"
#include "MarkupEvent.h"
#include "Newline.h"
#include "Output.h"
#include "Pointer.h"
#include "Print.h"
#include "Type.h"
#include "Text.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The CIO Markup Writer provides a common base class and interface for text-based markup writers so that the
	 * same logic can be reused between different formats such as JSON and XML.
	 */
	class CIO_API MarkupWriter
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Construct a JSON writer.
			 * 
			 * If a non-null allocator is specified with the pointer, the writer will own the stream and delete it when done.
			 * 
			 * @param stream The underlying output stream if any
			 * @param allocator The memory allocator for the stream if any
			 */
			explicit MarkupWriter(Pointer<Output> stream = nullptr);

			/**
			 * Construct a JSON writer with a preconfigured write buffer.
			 * 
			 * If a non-null allocator is specified with the pointer, the writer will own the stream and delete it when done.
			 *
			 * @param buffer The write buffer
			 * @param stream The underlying output stream if any
			 * @param allocator The memory allocator for the stream if any
			 */
			explicit MarkupWriter(Buffer buffer, Pointer<Output> stream = nullptr);

			/**
			 * Destructor. Attempts to flush all uncommitted writes, but will not throw if that fails.
			 */
			virtual ~MarkupWriter() noexcept;

			/**
			 * Changes the output stream for this JSON writer.
			 * All buffered writes will be flushed to the given stream after this call.
			 * Setting to null will disable buffer flushing.
			 * 
			 * This can be called with a raw pointer to use a non-owning view of that output,
			 * or an owning pointer with a non-null allocator to deallocate on close.
			 * 
			 * @param stream The new output stream
			 */
			virtual void setOutput(Pointer<Output> stream);

			/**
			 * Clears the writer.
			 * The base class attempts to flush all uncommitted writes, but will not throw if that fails.
			 * Subclasses should override to clear derived state or alter behavior.
			 */
			virtual void clear() noexcept;

			/**
			 * Flushes all uncommitted buffered writes to the output.
			 * The base class implementation simply drains the buffer to the output if any.
			 * Subclasses may override to alter behavior or perform additional steps.
			 * 
			 * @throw cio::Exception If flushing failed due to no output available or the actual output write failed
			 */
			virtual void flush();

			/**
			 * Starts a new document.
			 * This should only be done once per output.
			 * The base implementation checks to see if the level stack is empty.
			 * If so, a new markup event is pushed to represent the document.
			 * 
			 * Subclasses should override to additional print any encoding text necessary to the write buffer, or replace the
			 * check for determining whether a new document can be started.
			 * 
			 * @return the status of this operation
			 */
			virtual State startDocument();

			/**
			 * Ends the current document.
			 * This closes all open elements.
			 * 
			 * The base implementation pops each level off of the level stack and calls the correct
			 * end method to close the element.
			 * 
			 * Subclasses may override to additional print any encoding text necessary to the write buffer, or replace the
			 * check for determining whether a document can be ended.
			 * 
			 * @return the status of this operation
			 */
			virtual State endDocument();

			/**
			 * Start a new element in the document.
			 * The given element data type will be used for the element if anything other than None.
			 * Use Type::Any to indicate a complex element with fields.
			 * 
			 * The base implementation will check the current level stack.
			 * If the current level is an attribute, comment, value, or instruction, it will be ended.
			 * Then it will push a new markup event onto the level stack for the level.
			 * 
			 * Subclasses must override to encode the appropriate text to the write buffer, or to change
			 * the check for whether the element may be started.
			 * 
			 * @warning The element key text is not duplicated and needs to remain valid until the level is ended.
			 * 
			 * @param element The element to start
			 * @return the status of this operation
			 */
			virtual State startElement(Text element);

			/**
			 * End the current element.
			 * 
			 * The base implementation will check the current level stack.
			 * If the current level is an attribute, comment, value, or instruction, it will be ended.
			 * Then it will pop the markup event from the level stack.
			 * 
			 * Subclasses must override to encode the appropriate text to the write buffer, or to change
			 * the check for whether the element may be ended. Note that the subclass needs to obtain the
			 * element key text or other level traits prior to calling the base class method if it is needed.
			 * 
			 * @return the status of this operation
			 */
			virtual State endElement();

			/**
			 * Writes an empty element by calling startElement then endElement.
			 * Subclasses may override to do this more efficiently.
			 *
			 * @param name The element name
			 * @return the status of this operation
			 */
			virtual State writeEmptyElement(Text name);

			/**
			 * Starts an attribute with the given key name. Only valid after calling start element.
			 * 
			 * The base implementation will check the current level stack.
			 * If the current level is an attribute, the existing attribute will be ended.
			 * If the current level is an element or isntruction after that, a new level will be pushed
			 * to the level stack for the attribute. Otherwise, the operation will fail.
			 * 
			 * Subclasses must override to encode the appropriate text to the write buffer, or to change
			 * the check for whether the attribute may be started.
			 * 
			 * @warning The attribute key text is not duplicated and needs to remain valid until the level is ended.
			 * 
			 * @param key The attribute key name
			 * @return the status of this operation
			 */
			virtual State startAttribute(Text key);

			/**
			 * Ends the current attribute. Only valid after calling start attribute.
			 * 
			 * The base implementation will check the current level stack.
			 * If the current level is an attribute, the existing attribute will be ended.
			 * Otherwise, the operation will fail.
			 * 
			 * Subclasses must override to encode the appropriate text to the write buffer, or to change
			 * the check for whether the attribute may be ended.
			 * 
			 * @return the status of this operation
			 */
			virtual State endAttribute();

			/**
			 * Starts a new comment.
			 * 
			 * The base implementation checks the level stack to verify the current level is not already a comment.
			 * If the current level is an attribute, it will be ended first.
			 * The level stack will be updated with a markup event for the comment.
			 * 
			 * Comment text may be written using writeValue.
			 * 
			 * Subclasses must override to write encoded text to start the comment.
			 * 
			 * @return the state of this operation
			 */
			virtual State startComment();

			/**
			 * End the current comment.
			 * The base implementation checks the level stack to verify the current level is a comment.
			 * If so, it will be popped off the level stack.
			 * 
			 * Subclasses must override to write encoded text to end the comment.
			 * 
			 * @return the state of this operation
			 */
			virtual State endComment();

			/**
			 * Writes the full text of a new comment.
			 * The base implementation calls startComment, then writeValue, then endComment.
			 * Subclasses may override to implement more directly for performance.
			 * 
			 * @param text The text to write for the comment
			 * @return the state of this operation
			 */
			virtual State writeComment(Text text);

			/**
			 * Starts a markup encoding instruction with the given key.
			 * These are typically specific to the markup type and not general purpose.
			 * 
			 * @param key The instruction key
			 * @return the state of this operation
			 */
			virtual State startInstruction(Text key);

			/**
			 * Ends the current markup encoding instruction.
			 * These are typically specific to the markup type and not general purpose.
			 *
			 * @return the state of this operation
			 */
			virtual State endInstruction();

			/**
			 * Starts a value to be generated incrementally by a future call to writeValue.
			 * For markup writers that care about data type, the expected data type may be specified to improve validation.
			 * Subclasses should override to add any appropriate prefix to the encoding (typically for things like quoted strings).
			 *
			 * @param type the intended data type of this value, or None to allow any data type
			 * @return the status of this operation
			 */
			virtual State startValue(Type type = Type::None);

			/**
			 * Ends the current value, if it was being generated incrementally.
			 * Subclasses should override to add any terminator to the encoding (typically for things like quoted strings).
			 *
			 * @return the status of this operation
			 */
			virtual State endValue();

			/**
			 * Writes a UTF-8 text value for the currently active item.
			 *
			 * The base implementation calls startValue with the appropriate type, then writes the raw text,
			 * then calls endValue. If value is the null pointer, nothing is written.
			 *
			 * @param value The text value
			 * @return the status of this operation
			 */
			virtual State writeValue(const Text &value);

			/**
			 * Writes a Boolean value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * Boolean text "true" or "false", then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(bool value);

			/**
			 * Writes a signed integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(int value);

			/**
			 * Writes an unsigned integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(unsigned value);

			/**
			 * Writes a signed integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(long value);

			/**
			 * Writes an unsigned integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(unsigned long value);

			/**
			 * Writes a signed integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(long long value);

			/**
			 * Writes an unsigned integer value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the integer, then calls endValue.
			 * 
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(unsigned long long value);

			/**
			 * Writes a real value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type, then writes out the
			 * printed value of the floating point number, with special values formatted as "NaN", "Infinity", and "-Infinity",
			 * then calls endValue.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(float value);

			/**
			 * Writes a real value for the currently active item.
			 * 
			 * The base implementation calls startValue with the appropriate type then writes out the
			 * printed value of the floating point number, with special values formatted as "NaN", "Infinity", and "-Infinity",
			 * then calls end value.
			 *
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			virtual State writeValue(double value);

			/**
			 * Writes raw byte data for the currently active item.
			 *
			 * The base implementation calls startValue with the appropriate type then writes out the
			 * printed value of the bytes as a hexadecimal string.
			 *
			 * @param value The byte array
			 * @param length The number of bytes to write
			 * @return the status of this operation
			 */
			virtual State writeValue(const void *bytes, std::size_t length);

			/**
			 * Writes any datatype value not otherwise handled by the writer by printing it and
			 * wrapping it in a cio::Text view.
			 * 
			 * @tparam T The value type
			 * @param value The value
			 * @return The status of this operation
			 */
			template <typename T>
			inline State writeValue(const T &value)
			{
				return this->writeValue(cio::Text::view(cio::print(value)));
			}

			/**
			 * Writes an empty value for the current active item.
			 * 
			 * The base implementation simply calls startValue then endValue.
			 * 
			 * @return the status of this operation
			 */
			virtual State writeEmptyValue();

			/**
			 * Starts an array value. This can only be done inside elements.
			 * 
			 * @param count The number of array items, or SIZE_MAX to leave unbounded
			 * @param type The type of array element, or Type::Any to allow mixed arrays
			 * @return the status of this operation
			 */
			virtual State startArray(std::size_t count = SIZE_MAX, Type type = Type::None);

			/**
			 * Ends the current array value.
			 *
			 * @return the status of this operation
			 */
			virtual State endArray();

			// Simplifying methods - ignore default values and write entire attributes in one pass

			/**
			 * Writes a simple element with the given value.
			 *
			 * @param name The element name
			 * @param value The value
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeSimpleElement(Text name, T value);

			/**
			 * Writes a simple element with the given value if it not the default value for T.
			 *
			 * @param name The element name
			 * @param value The value
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeSimpleNondefaultElement(Text name, T value);

			/**
			 * Writes a simple element with the given value if it not the given default value.
			 *
			 * @param name The element name
			 * @param value The value
			 * @param def The default value to not write
			 * @return the status of this operation
			 */
			template <typename T, typename U>
			inline State writeSimpleNondefaultElement(Text name, T value, U def);

			/**
			 * Writes an attribute with the given value.
			 *
			 * @param name The attribute value
			 * @param value The value
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeAttribute(Text name, T value);

			/**
			 * Writes an entire attribte if it is not the default value for its type.
			 *
			 * @param key The attribute key name
			 * @param value The attribute value
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeNondefaultAttribute(Text key, T value);

			/**
			 * Writes a value if it is not the given default value.
			 *
			 * @param key The attribute name
			 * @param value The attribute value
			 * @param def The default value to select to not write the attribute
			 * @return the status of this operation
			 */
			template <typename T, typename U>
			inline State writeNondefaultAttribute(Text key, T value, U def);

			/**
			 * Writes out an array by explicitly providing values.
			 * 
			 * @tparam T The value types
			 * @param values The values to write
			 * @return the status of this operation
			 */
			template <typename... T>
			inline State writeSimpleArray(T... values);

			/**
			 * Writes out an array by explicitly providing values.
			 *
			 * @tparam T The value types
			 * @param key The element name
			 * @param values The values to write
			 * @return the status of this operation
			 */
			template <typename... T>
			inline State writeSimpleArrayElement(Text key, T... values);

			/**
			 * Writes out an array by explicitly providing values.
			 *
			 * @tparam T The value types
			 * @param data The array of values to write
			 * @param count The number of values to write
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeArray(const T *data, std::size_t count);

			/**
			 * Writes out an array by explicitly providing values.
			 *
			 * @tparam T The value types
			 * @tparam N The compile time array size
			 * @param data The array of values to write
			 * @return the status of this operation
			 */
			template <typename T, std::size_t N>
			inline State writeArray(const T(&data)[N]);

			/**
			 * Writes out an array by explicitly providing values.
			 *
			 * @tparam T The vector element type
			 * @tparam A The vector allocator type
			 * @param data The array of values to write
			 * @return the status of this operation
			 */
			template <typename T, typename A>
			inline State writeArray(const std::vector<T, A> &data);

			/**
			 * Writes out an array by explicitly providing values.
			 *
			 * @tparam T The value types
			 * @param key The element name
			 * @param data The array data values to write
			 * @param count The number of values to write
			 * @return the status of this operation
			 */
			template <typename T>
			inline State writeArrayElement(Text key, const T *data, std::size_t count);

			/**
			 * Writes the next array values and then the remaining array values
			 * after starting an array value.
			 * 
			 * @tparam T The next array value type
			 * @tparam U The remaining array value types
			 * @param value The next array value
			 * @param remainder The remaining array values
			 */
			template <typename T, typename... U>
			inline void writeArrayValues(T value, U... remainder);

			/**
			 * Finishes writing the array values since none are left.
			 */
			inline void writeArrayValues();

			// Pretty-printing settings

			/**
			 * Sets the indent character type and count.
			 * 
			 * @param c The indent character type
			 * @param amount The number of indent characters per level
			 */
			void setIndent(char c, unsigned amount);

			/**
			 * Sets the indent character type.
			 * 
			 * @param c The indent character type
			 */
			void setIndentChar(char c);

			/**
			 * Gets the indent character type.
			 *
			 * @return The indent character type
			 */
			char getIndentChar() const;

			/**
			 * Sets the indentation amount.  Each level of indent will be this amount.
			 * 
			 * @param amt The number of indent characters per level
			 */
			void setIndentAmount(unsigned amt);

			/**
			 * Gets the indentation amount.  Each level of indent will be this amount.
			 *
			 * @return The number of indent characters per level
			 */
			unsigned getIndentAmount() const;

			/**
			 * Gets the current number of indentation characters.
			 * This is the current depth multiplied by the indentation amount.
			 * 
			 * @return the current number of indentation characters
			 */
			unsigned getCurrentIndentAmount() const;

			/**
			 * Gets the newline style to use between records.
			 * None will result in records being separated by normal whitespace.
			 * All other styles will result in the proper newline character(s) for that style.
			 * 
			 * @return the newline style in use
			 */
			Newline getNewlineStyle() const noexcept;

			/**
			 * Sets the newline style to use between records.
			 * None will result in records being separated by normal whitespace.
			 * All other styles will result in the proper newline character(s) for that style.
			 *
			 * @param newline the newline style in use
			 */
			void setNewlineStyle(Newline newline) noexcept;

			// Advanced Operations - Direct Output and Buffer Management

			/**
			 * Gets direct access to the output.
			 * This returns a non-owning view of the output.
			 * 
			 * @return the output
			 */
			Pointer<Output> getOutput() noexcept;

			/**
			 * Takes ownership of this output, removing it from the writer.
			 * 
			 * @return the removed output
			 */
			Pointer<Output> takeOutput() noexcept;

			/**
			 * Gets the internal buffer used to cache formatted writes.
			 * If no output has been set, this will have the entire document written so far.
			 * 
			 * Directly modifying the buffer may be used to add, remove, or alter cached content that
			 * will be written the next time the buffer is flushed. You can also change the resize policy
			 * to affect whether the buffer has a hard write limit or prefers to cache data.
			 * 
			 * @return the write buffer
			 */
			Buffer &getWriteBuffer() noexcept;

			/**
			 * Gets the internal buffer used to cache formatted writes.
			 * If no output has been set, this will have the entire document.
			 *
			 * @return the write buffer
			 */
			const Buffer &getWriteBuffer() const noexcept;

			/**
			 * Sets the internal buffer used to cache formatted writes.
			 * The existing buffer content, if any, will be lost and replaced with the new buffer.
			 * This can be used to precisely control what memory is used for buffering, such as using
			 * statically allocated memory.
			 * 
			 * @param buffer The buffer to adopt
			 */
			void setWriteBuffer(Buffer buffer) noexcept;

			/**
			 * Takes the internal buffer used to cache formatted writes, moving it from this object to the caller.
			 * This will prevent the next flush from writing any of the content up to this point.
			 * 
			 * @return the moved buffer
			 */
			Buffer takeWriteBuffer() noexcept;

			/**
			 * Writes a separator between values.
			 * If the Newline style is None, this writes a single ' '.
			 * Otherwise, it writes a newline in the given newline style.
			 */
			void separate();

			/**
			 * Writes indentation at the current element depth.
			 */
			void indent();

			/**
			 * Writes indentation at a depth relative to the current element depth.
			 * 
			 * @param relative The relative depth to the current depth
			 */
			void indent(int relative);

			/**
			 * Writes indentation for the given element depth.
			 *
			 * @param depth The element depth
			 */
			void indentAbsolute(unsigned depth);

			/**
			 * Requests that at least the given number of bytes are writable in the write buffer.
			 * If the write buffer is full, it will be drained to the output (if any),
			 * then the remaining(needed) will be delegated to it to verify it has enough space.
			 * If the write buffer is resizable, this will resize it if needed.
			 *
			 * @param needed The number of bytes needed for a single atomic write
			 * @return the actual number of bytes available in the write buffer
			 */
			std::size_t remaining(std::size_t needed);

			/**
			 * Ensures that at least the given number of bytes are writable in the write buffer.
			 * If the write buffer is full, it will be drained to the output (if any),
			 * then the writable(needed) will be delegated to it to verify it has enough space.
			 * If the write buffer is resizable, this will resize it if needed.
			 * 
			 * @param needed The number of bytes needed for a single atomic write
			 * @return the actual number of bytes available in the write buffer
			 * @throw Exception If the write buffer total size was not enough to accomodate this request
			 * and it is not resizable.
			 */
			std::size_t writable(std::size_t needed);

			/**
			 * Writes encoded text data straight to the writer. This bypasses all markup structure checks and
			 * just directly prints the characters to the write buffer and/or output device.
			 * 
			 * If the text will fit in the current buffer size, it will be printed to the write buffer, possibly
			 * flushing the existing buffer first. If not, and an output device exists, it will be written straight
			 * to the output after flushing any existing buffer data. Finally, if no other option exists, the write
			 * buffer will be resized if allowed to accommodate the write.
			 * 
			 * @param text The text to write
			 * @param length The number of text characters to write
			 * @throw Exception If the text could not be written in its entirety
			 */
			void write(const char *text, std::size_t length);

			/**
			 * Writes a printed value straight to the writer. This bypasses all markup structure checks and
			 * just directly prints the characters to the write buffer and/or output device.
			 *
			 * @tparam T the value type
			 * @param value The value to write
			 * @throw Exception If the value could not be written in its entirety
			 */
			template <typename T>
			inline void write(T value);

			/**
			 * Writes encoded text data straight to the writer. This bypasses all markup structure checks and
			 * just directly prints the characters to the write buffer and/or output device.
			 *
			 * If the text will fit in the current buffer size, it will be printed to the write buffer, possibly
			 * flushing the existing buffer first. If not, and an output device exists, it will be written straight
			 * to the output after flushing any existing buffer data. Finally, if no other option exists, the write
			 * buffer will be resized if allowed to accommodate the write.
			 *
			 * @param c The character to splat
			 * @param length The number of text characters to write
			 * @throw Exception If the text could not be written in its entirety
			 */
			void splat(char c, std::size_t length);

			/**
			 * Gets the current markup level of any type.
			 * If none, this returns the root level.
			 * 
			 * @return the current markup level
			 */
			MarkupEvent &getCurrentLevel() noexcept;

			/**
			 * Gets the current markup level of any type.
			 * If none, this returns the root level.
			 *
			 * @return the current markup level
			 */
			const MarkupEvent &getCurrentLevel() const noexcept;

			/**
			 * Gets the current markup level of the given type.
			 * If none, this returns the root level.
			 *
			 * @param type the type of markup element to find
			 * @return the current markup level
			 */
			MarkupEvent &getCurrentLevel(Markup type) noexcept;

			/**
			 * Gets the current markup level of any type.
			 * If none, this returns the root level.
			 *
			 * @param type the type of markup element to find
			 * @return the current markup level
			 */
			const MarkupEvent &getCurrentLevel(Markup type) const noexcept;

			/**
			 * Gets the parent of the current markup level of any type.
			 * Optionally this may specify how many parent levels to get.
			 * If the depth is not valid, this returns the root level.
			 *
			 * @param depth The parent level depth
			 * @return the current markup level
			 */
			MarkupEvent &getParentLevel(std::size_t depth = 1) noexcept;

			/**
			 * Gets the parent of the current markup level of any type.
			 * Optionally this may specify how many parent levels to get.
			 * If the depth is not valid, this returns the root level.
			 *
			 * @param depth The parent level depth
			 * @return the current markup level
			 */
			const MarkupEvent &getParentLevel(std::size_t depth = 1) const noexcept;

			/**
			 * Checks whether the level stack has a current level of the given type.
			 * If so, it returns the depth relative to the root level.
			 * Otherwise, it returns 0 to indicate the root level and also to be interpreted as false.
			 * 
			 * @param type The markup type
			 * @return the depth relative to root of the deepest current level of that type
			 */
			std::size_t hasCurrentLevel(Markup type) const noexcept;

			/**
			 * Ends the current level regardless of what it is.
			 * 
			 * @return the status of this operation
			 */
			State endCurrentLevel();

			/**
			 * Ends the given number of current levels regardless of what they are.
			 *
			 * @param depth The number of levels to end with 0 being the current level
			 * @return the status of this operation
			 */
			State endCurrentLevels(std::size_t depth);

			/**
			 * Ends the the current child levels below the current level of the given type.
			 *
			 * @param type The type of markup element to find and remove children from
			 * @return the status of this operation
			 */
			State endCurrentChildLevels(Markup type);

		protected:
			/** The metaclass for this class */
			static Class<MarkupWriter> sMetaclass;

			/** Buffer to use to cache write operations */
			Buffer mWriteBuffer;

			/** The output being written */
			Pointer<Output> mOutput;

			/** Active level stack for writer */
			std::vector<MarkupEvent> mLevels;

			/** Level information for top-level items outside of a document */
			MarkupEvent mRootLevel;

			/** The current element depth, used for indentation */
			unsigned mDepth;

			/** The number of indentation characters to use at each level */
			unsigned mIndent;

			/** The indentation character to use */
			char mIndentChar;

			/** The newline style to use */
			Newline mNewline;

			/** Whether actual writes should be filtered due to the output not wanting the current level */
			bool mFilterWrites;
	};
}

/* Inline implementation */

namespace cio
{
	template <typename T>
	inline State MarkupWriter::writeAttribute(Text name, T value)
	{
		State state = this->startAttribute(std::move(name));
		if (state.succeeded())
		{
			state = this->writeValue(value);
			this->endAttribute();
		}
		return state;
	}

	template <typename T>
	inline State MarkupWriter::writeNondefaultAttribute(Text key, T value)
	{
		State state;

		if (value != T())
		{
			state = this->writeAttribute(std::move(key), value);
		}

		return state;
	}

	template <typename T, typename U>
	inline State MarkupWriter::writeNondefaultAttribute(Text key, T value, U def)
	{
		State state;

		if (value != def)
		{
			state = this->writeAttribute(std::move(key), value);
		}

		return state;
	}

	template <typename T>
	inline State MarkupWriter::writeSimpleElement(Text name, T value)
	{
		State state = this->startElement(std::move(name));
		if (state.succeeded())
		{
			state = this->writeValue(value);
			this->endElement();
		}
		return state;
	}

	template <typename T>
	inline State MarkupWriter::writeSimpleNondefaultElement(Text name, T value)
	{
		State state;
		if (value != T())
		{
			state = this->writeSimpleElement(std::move(name), value);
		}
		return state;
	}

	template <typename T, typename U>
	inline State MarkupWriter::writeSimpleNondefaultElement(Text name, T value, U def)
	{
		State state;
		if (value != def)
		{
			state = this->writeSimpleElement(std::move(name), value);
		}
		return state;
	}

	template <typename... T>
	inline State MarkupWriter::writeSimpleArray(T... values)
	{
		State state = this->startArray(sizeof...(T));
		if (state.succeeded())
		{
			this->writeArrayValues(values...);
			this->endArray();
		}
		return state;
	}

	template <typename... T>
	inline State MarkupWriter::writeSimpleArrayElement(Text key, T... values)
	{
		State state = this->startElement(std::move(key));
		if (state.succeeded())
		{
			state = this->writeSimpleArray(values...);
			this->endElement();
		}
		return state;
	}

	template <typename T>
	inline State MarkupWriter::writeArray(const T *data, std::size_t count)
	{
		State state = this->startArray(count);
		if (state.succeeded())
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				this->writeValue(data[i]);
			}
			this->endArray();
		}
		return state;
	}

	template <typename T, std::size_t N>
	inline State MarkupWriter::writeArray(const T(&data)[N])
	{
		return this->writeArray(data, N);
	}


	template <typename T, typename A>
	inline State MarkupWriter::writeArray(const std::vector<T, A> &data)
	{
		return this->writeArray(data.data(), data.size());
	}

	template <typename T>
	inline State MarkupWriter::writeArrayElement(Text key, const T *data, std::size_t count)
	{
		State state = this->startElement(std::move(key));
		if (state.succeeded())
		{
			state = this->writeArray(data, count);
			this->endElement();
		}
		return state;
	}

	template <typename T, typename... U>
	inline void MarkupWriter::writeArrayValues(T value, U... remainder)
	{
		this->writeValue(value);
		this->writeArrayValues(remainder...);
	}

	inline void MarkupWriter::writeArrayValues()
	{
		// nothing to do in base case
	}

	template <typename T>
	inline void MarkupWriter::write(T value)
	{
		if (!mFilterWrites)
		{
			std::size_t needed = cio::strlen(value);
			// First try to generate enough room in the buffer, possibly draining to the output
			std::size_t available = this->remaining(needed);
			if (available >= needed)
			{
				mWriteBuffer << value;
			}
			// If we still don't have enough room, we have to write straight to the device
			else if (mOutput)
			{
				*mOutput << value;
			}
			else
			{
				throw Exception(Action::Write, Reason::Overflow);
			}
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
