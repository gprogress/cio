/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_CONNECTION_H
#define CIO_BLUETOOTH_CONNECTION_H

#include "Types.h"

#include "Device.h"
#include "Peripheral.h"

#include <cio/Allocator.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Connection class provides a simplified high-level API to work with Bluetooth connections.
		 * It handles automatically determining the best platform-specific implementation of the local controller
		 * and the GATT attribute client and establishing connection and pairing between devices.
		 */
		class CIO_BLUETOOTH_API Connection
		{
			public:
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Gets the metaclass describing the most appropriate GATT attribute client class type for the current platform.
				 * 
				 * @note Currently this is only implemented on Linux to prefer the DBusAttributeClient if available, falling back to the
				 * L2CAPAttributeClient otherwise.
				 *
				 * @return the metaclass describing the best GATT client to use
				 */
				static const Metaclass *getBestClientType() noexcept;
				
				/**
				 * Allocates a new instance of the best GATT client for the current platform using operator new.
				 *
				 * @return the attribute client
				 * @throw std::bad_alloc If the memory could not be allocated
				 */
				static std::unique_ptr<AttributeClient> allocateBestClient();
				
				/**
				 * Constructs a new instance of the best GATT client for the current platform on the given chunk of memory.
				 *
				 * @param buffer The memory buffer
				 * @param capacity The maximum number of bytes available in the memory buffer
				 * @return the attribute client after construction
				 * @throw std::overflow_error If the memory buffer was not large enough for the class instance
				 */
				static AttributeClient *constructBestClient(void *buffer, std::size_t capacity);
			
				/**
				 * Construct an empty connection with no local or peripheral devices associated.
				 */
				Connection() noexcept;
				
				/**
				 * Construct a connection from the given local device to the given peripheral.
				 * The best platform-specific implementation for the local controller is allocated and used.
				 * The connection is established immediately, but the device pairing state is not modified.
				 *
				 * @param device The local device
				 * @param peripheral The peripheral
				 */
				Connection(const Device &device, const Peripheral &peripheral);

				/**
				 * Move constructor for Connection.
				 *
				 * @param in The connection to move
				 */
				Connection(Connection &&in) noexcept;
				
				/**
				 * Move assignment for Connection.
				 *
				 * @param in The connection to move
				 * @return this connection
				 */
				Connection &operator=(Connection &&in) noexcept;
				
				/**
				 * Destructor.
				 * If automatic unpairing is enabled, this will unpair the local and peripheral devices.
				 * If automatic disconnection is enabled, this will disconnect the local and peripheral devices.
				 */
				~Connection() noexcept;
				
				/**
				 * Clears the connection, restoring it to the empty default constructed state with no local or peripheral device.
				 *
				 * If automatic unpairing is enabled, this will unpair the local and peripheral devices.
				 * If automatic disconnection is enabled, this will disconnect the local and peripheral devices.
				 */			
				void clear() noexcept;
				
				/**
				 * Opens the current device to use for the connection, or the default device if none was set.
				 * The best platform-specific implementation for the local controller is allocated and used.
				 *
				 * If automatic disconnection is enabled, this disconnects the current connection first.
				 */
				void open();
				
				/**
				 * Opens the given device to use for the connection.
				 * The best platform-specific implementation for the local controller is allocated and used.
				 *
				 * If automatic disconnection is enabled, this disconnects the current connection first.
				 *
				 * @param device The local device to use
				 */
				void open(const Device &device);
				
				/**
				 * Establishes the connection using the currently associated local and peripheral devices.
				 * If no local device has been associated, the default local device will be used.
				 *
				 * Connection is generally intended to be long-lived with minimal radio use and thus should
				 * outlive this particular connection object or the running application. However, this behavior
				 * can be changed with setAutoDisconnect(true) to scope the connection to this object.
				 *				 
				 * If automatic disconnection is enabled, this disconnects the current connection first.
				 *
				 * The returned status indicates whether a new connection was established (Success) or
				 * an existing long-term connection was reused (Exists).
				 *
				 * @return whether the connection is new (Success) or reused (Exists)
				 * @throw cio::Exception If no peripheral has been associated with the connection or the connection fails
				 */
				State connect();
				
				/**
				 * Establishes the connection using the currently associated local device and the given peripheral device.
				 * If no local device has been associated, the default local device will be used.
				 *
				 * Connection is generally intended to be long-lived with minimal radio use and thus should
				 * outlive this particular connection object or the running application. However, this behavior
				 * can be changed with setAutoDisconnect(true) to scope the connection to this object.
				 *
				 * If automatic disconnection is enabled, this disconnects the current connection first.
				 *
				 * The returned status indicates whether a new connection was established (Success) or
				 * an existing long-term connection was reused (Exists).
				 *
				 * @param peripheral The peripheral to connect to
				 * @return whether the connection is new (Success) or reused (Exists)
				 * @throw cio::Exception If no peripheral has been associated with the connection or the connection fails
				 */
				State connect(const Peripheral &peripheral);
				
				/**
				 * Establishes the connection from the given local device to the given peripheral device.
				 * If the given device is empty, the default local device will be used instead.
				 *
				 * Connection is generally intended to be long-lived with minimal radio use and thus should
				 * outlive this particular connection object or the running application. However, this behavior
				 * can be changed with setAutoDisconnect(true) to scope the connection to this object.
				 *
				 * If automatic disconnection is enabled, this disconnects the current connection first.
				 *
				 * The returned status indicates whether a new connection was established (Success) or
				 * an existing long-term connection was reused (Exists).
				 *
				 * @param device The local device to connect from
				 * @param peripheral The peripheral to connect to
				 * @return whether the connection is new (Success) or reused (Exists)
				 * @throw cio::Exception If no peripheral has been associated with the connection or the connection fails
				 */
				State connect(const Device &device, const Peripheral &peripheral);
				
				/**
				 * Disconnects the current established connection between the local device and peripheral device.
				 *
				 * @return whether the connection was disconnected (Success) or didn't exist at all (Missing)
				 */
				State disconnect();
				
				/**
				 * Pairs the current local and peripheral devices.
				 * If the current device is not yet connected to the peripheral, the connection is established first.
				 * if the current device is not set, the default local device will be used.
				 *
				 * Pairing is generally intended to be a long term state that outlives this particular connection object
				 * or device-level connection. By default pairing is not automatically unpaired, but you can change that behavior
				 * by calling setAutoUnpair(true) to scope the pairing to this particular object.
				 *
				 * The returned status indicates whether a new pairing was established (Success) or
				 * an existing pairing was reused (Exists).
				 * @return whether the pairing is new (Success) or reused (Exists)
				 * @throw cio::Exception If no peripheral has been associated with the connection or the pairing fails
				 */
				State pair();
				
				/**
				 * Unpairs the current local and peripheral devices.
				 * 
				 * The returned status indicates whether a pairing existed (Success) or not (Missing).
				 *
				 * @return whether the pairing was canceled (Success) or didn't exist to begin with (Missing)
				 * @throw cio::Exception If no peripheral has been associated with the connection or the unpairing fails
				 */
				State unpair();

				/**
				 * Checks whether this object knows that the connection is currently established.
				 *
				 * Note that the connection may already exist between the device and peripheral but this object
				 * won't know about it until the connection is attempted.
				 *
				 * @return whether this object knows that a connection is estabilshed
				 */
				bool isConnected() const noexcept;
								
				/** 
				 * Manually marks this object as being connected or no.
				 * This is intended for advanced use to clear the connection flag when some other source (such as a GATT client)
				 * returns that a disconnection occurred asynchronously.
				 *
				 * @param value Whether to mark the object as connected
				 */
				void setConnected(bool value) noexcept;
				
				/**
				 * Gets whether this object is configured to automatically disconnect a connection on clear(), destruction, or
				 * changing peripherals or local devices. The default is false to preserve long-term persistent connections.
				 *
				 * @return whether automatic disconnection is enabled
				 */
				bool getAutoDisconnect() const noexcept;
				
				/**
				 * Sets whether this object is configured to automatically disconnect a connection on clear(), destruction, or
				 * changing peripherals or local devices. The default is false to preserve long-term persistent connections.
				 *
				 * @param value whether automatic disconnection is enabled
				 */
				void setAutoDisconnect(bool value) noexcept;
				
				/**
				 * Checks whether this object knows that the connected devices are paired.
				 *
				 * Note that the pairing may already exist between the device and peripheral but this object
				 * won't know about it until the pairing is attempted.
				 *
				 * @return whether this object knows that the devices are paired
				 */
				bool isPaired() const noexcept;
				
				/**
				 * Gets whether this object is configured to automatically unpair the connected devices on clear() or destruction.
				 * The default is false to preserve long-term persistent pairing.
				 *
				 * @return whether automatic disconnection is enabled
				 */
				bool getAutoUnpair() const noexcept;
				
				/**
				 * Sets whether this object is configured to automatically unpair the connected devices on clear() or destruction.
				 * The default is false to preserve long-term persistent pairing.
				 *
				 * @param value whether automatic disconnection is enabled
				 */
				void setAutoUnpair(bool value) noexcept;
				
				/**
				 * Checks whether this connection is known to be using Bluetooth Low Energy technology.
				 *
				 * @return whether this is a Bluetooth Low Energy connection
				 */
				bool isLowEnergy() const noexcept;
				
				/**
				 * Gets the local adapter associated with the current device.
				 * If none exists, creates one using the best available technology on the current platform.
				 *
				 * @return the local adapter
				 * @throw cio::Exception If no adapter already existed and one could not be created due to platform or technology limitations
				 * @throw std::bad_alloc If allocating memory for the adapter failed
				 */
				std::shared_ptr<Adapter> getOrCreateAdapter();
				
				/**
				 * Gets the local adapter associated with the current device.
				 *
				 * @return the local adapter
				 */
				std::shared_ptr<Adapter> getAdapter() noexcept;
				
				/**
				 * Gets the local adapter associated with the current device.
				 *
				 * @return the local adapter
				 */
				std::shared_ptr<const Adapter> getAdapter() const noexcept;
				
				/**
				 * Manually overrides the local adapter currently associated with this connection.
				 * You can use this to select a different technology than the platform default or provide your own implementation.
				 * 
				 * @param adapter The local adapter
				 */
				void setAdapter(std::shared_ptr<Adapter> adapter);
				
				/**
				 * Takes the local adapter associated with the current device, removing it so no other users will use it.
				 * After this call, no changes to this class will affect the returned adapter.
				 *
				 * @return the local adapter
				 */
				std::shared_ptr<Adapter> takeAdapter() noexcept;

				/**
				 * Manually overrides the local controller currently associated with this connection.
				 * You can use this to select a different technology than the platform default or provide your own implementation.
				 * If the controller is actually a subclass of Adapter, it will be used directly.
				 * Otherwise, the current adapter (or a new one if none was set) will be configured with the given controller.
				 * 
				 * @param controller The local controller
				 */
				void setController(std::shared_ptr<Controller> controller);
				
				/**
				 * Gets the local adapter device currently associated with the connection.
				 *
				 * @return the current device
				 */
				const Device &getDevice() const noexcept;
				
				/**
				 * Sets the local adapter device for this object without establishing a connection.
				 *
				 * @param device The local device to set
				 */
				void setDevice(const Device &device) noexcept;
				
				/**
				 * Gets the peripheral currently associated with the connection.
				 *
				 * @return the current peripheral
				 */
				const Peripheral &getPeripheral() const noexcept;
				
				/**
				 * Sets the peripheral for this object without establishing a connection.
				 *
				 * @param remote The peripheral to set
				 */
				void setPeripheral(const Peripheral &remote) noexcept;
				
				/**
				 * Gets the GATT client currently associated with this connection.
				 * If none exists, a new one is allocated using the best technology available on the current platform.
				 * Currently, on Linux this prefers the DBusAttributeClient if available and the L2CAPAttributeClient as a fallback.
				 * All other platforms favor the L2CAPAttributeClient.
				 *
				 * @note The GATT client requires a connection to be established. If the devices are not paired, GATT queries
				 * are generally limited to GAP and basic discovery. Pairing is usually needed for full GATT operation.
				 * 
				 * @return the GATT client
				 */
				std::shared_ptr<AttributeClient> getOrCreateAttributeClient();
				
				/**
				 * Gets the GATT client currently associated with this connection.
				 *
				 * @note The GATT client requires a connection to be established. If the devices are not paired, GATT queries
				 * are generally limited to GAP and basic discovery. Pairing is usually needed for full GATT operation.
				 * 
				 * @return the GATT client or nullptr if none is available
				 */
				std::shared_ptr<AttributeClient> getAttributeClient() noexcept;
				
				/**
				 * Gets the GATT client currently associated with this connection.
				 * 
				 * @note The GATT client requires a connection to be established. If the devices are not paired, GATT queries
				 * are generally limited to GAP and basic discovery. Pairing is usually needed for full GATT operation.
				 *				 
				 * @return the GATT client or nullptr if none is available
				 */
				std::shared_ptr<const AttributeClient> getAttributeClient() const noexcept;

				/**
				 * Manually overrides the GATT client currently associated with this connection.
				 * You can use this to select a different technology than the platform default or provide your own implementation.
				 * 
				 * @param client the GATT client
				 */
				void setAttributeClient(std::shared_ptr<AttributeClient> client) noexcept;

				/**
				 * Takes the GATT client currently associated with this connection and removes it so that no other user may use it.
				 * After this call, getAttributeClient will return nullptr and getOrCreateAttributeClient will allocate a new one,
				 * and no changes to this class will affect the returned client.
				 *
				 * @note The GATT client requires a connection to be established. If the devices are not paired, GATT queries
				 * are generally limited to GAP and basic discovery. Pairing is usually needed for full GATT operation.
				 *
				 * @return the GATT client or nullptr if none is available
				 */
				std::shared_ptr<AttributeClient> takeAttributeClient() noexcept;

				/**
				 * Interprets this Connection in a Boolean context.
				 * It is considered true if the local device is connected to the peripheral.
				 * The pairing and GATT client state are not considered.
				 *
				 * @return whether the connection is established
				 */
				explicit operator bool() const noexcept;
				
			private:
				/** Metaclass for this class */
				static Class<Connection> sMetaclass;
				
				/**
				 * Updates the state of the connection to select the given device and peripheral.
				 *
				 * If they are different from the current device and peripheral, the connection should no longer be marked
				 * as connected or paired.
				 *
				 * If automatic disconnection is enabled, the existing connection should be disconnected.
				 * If automatic unpairing is enabled, the existing pairing should be canceled.
				 *
				 * If an adapter is allocated, it should be updated to use the new device.
				 *
				 * @param device The device
				 * @param peripheral The peripheral
				 */
				void update(const Device &device, const Peripheral &peripheral);
			
				/** The current local controller */
				std::shared_ptr<Adapter> mAdapter;
				
				/** The current GATT client */
				std::shared_ptr<AttributeClient> mClient;
				
				/** The current local device */
				Device mDevice;
				
				/** The current peripheral */
				Peripheral mPeripheral;
				
				/** Whether a connection is known to be established */
				bool mConnected;
				
				/** Whether automatic disconnection is enabled */
				bool mAutoDisconnect;
				
				/** Whether the devices are known to be paired */
				bool mPaired;
				
				/** Whether automatic unpairing is enabled */
				bool mAutoUnpair;
				
				/** Whether the connection is known to be Bluetooth Low Energy */
				bool mLowEnergy;
								
				/** The timeout to use for auto-generated tasks */
				std::chrono::milliseconds mTimeout;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

