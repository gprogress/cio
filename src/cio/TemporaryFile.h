/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TEMPORARYFILE_H
#define CIO_TEMPORARYFILE_H

#include "Types.h"

#include "Output.h"
#include "UniqueId.h"

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The CIO Temporary File class creates a temporary file to write to and can also save the contents
	* of the temporary file to a different location.
	*/
	class CIO_API TemporaryFile
	{
		public:
			/**
			* Default constructor.
			*/
			TemporaryFile();

			/**
			* Constructor. Creates a temporary file.
			* 
			* @param subdir The sub directory in the temp folder that would hold the temporary file
			* @param ext The temporary file's type extension
			*/
			TemporaryFile(const Path &subdir, const Text &ext);

			/**
			* Destructor. Deletes the temporary file and non-empty parent directories created.
			*/
			~TemporaryFile() noexcept;
			
			/**
			* Move constructor.
			*
			* @param in The temporary file to move
			*/
			TemporaryFile(TemporaryFile &&in) noexcept;
				
			/**
			* Move assignment.
			*
			* @param in The temporary file to move
			* @return this temporary file
			*/
			TemporaryFile &operator=(TemporaryFile &&in) noexcept;

			/**
			* Clears state. Deletes the temporary file and non-empty directories created.
			*/
			void clear() noexcept;

			/**
			* Creates a temporary file using a previously set path.
			*/
			void createTempFile();

			/**
			* Creates a temporary file with the given file extension in the given sub directory.
			*
			* @param subdir The sub directory in the temp folder that would hold the temporary file
			* @param ext The temporary file's type extension
			*/
			void createTempFile(const Path &subdir, const Text &ext);

			/**
			* Opens a temporary file to read and write.
			* 
			* @return the opened temporary file.
			*/
			File open();

			/**
			* Saves the contents of the temporary file to the given path.
			* 
			* @param path The path to save the temporary file to
			*/
			void copyTo(const Path &path);

			/**
			* Gets the path to the temp file.
			* 
			* @return The path to the temp file
			*/
			const Path &getTempFilePath() noexcept;

			/**
			* Sets the path to the temp file.
			* 
			* @param path The path to the temp file
			*/
			void setTempFilePath(const Path &path);

		private:
			/** The path to the temporary file. */
			Path mTempFilePath;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
