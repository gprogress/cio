/*==============================================================================
 * Copyright 2022-2024 Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_STOPWATCH_H
#define CIO_STOPWATCH_H

#include "Types.h"

#include <chrono>
#include <iosfwd>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Stopwatch class provides a representation of execution time.
	 * Unlike the Time class, it is monotonic (always increasing) and user changes will not affect it.
	 * However it is not directly convertible to a real-world date and time.
	 */
	class CIO_API Stopwatch
	{
		public:
			/**
			 * Gets the stopwatch time representing the end of time.
			 * 
			 * @return The end of time
			 */
			inline static Stopwatch max() noexcept;

			/**
			 * Gets the current execution time from the start of program.
			 * 
			 * @return The current execution
			 */
			inline static Stopwatch now() noexcept;

			/** 
			 * The stopwatch time value implementing the time 
			 */
			std::chrono::time_point<std::chrono::steady_clock> value;

			/**
			 * Construct a stopwatch time representing the beginning of time.
			 */
			inline Stopwatch() noexcept;

			/**
			 * Construct a stopwatch time using its given C++ representation.
			 * 
			 * @param value The stopwatch time value
			 */
			inline Stopwatch(std::chrono::time_point<std::chrono::steady_clock> value) noexcept;

			/**
			 * Calculates how much time has elapsed in milliseconds since the given epoch time.
			 * 
			 * @param epoch The epoch time
			 * @return The amount of time that elapsed between the epoch and this time
			 */
			inline std::chrono::milliseconds elapsed(const Stopwatch &epoch) const noexcept;

			/**
			 * Calculates how much time has elapsed in the given duration type since the given epoch time.
			 *
			 * @tparam D The duration type
			 * @param epoch The epoch time
			 * @return The amount of time that elapsed between the epoch and this time
			 */
			template <typename D>
			inline D elapsed(const Stopwatch &epoch) const noexcept;

			/**
			 * Calculates how much time has elapsed in millseconds between the set time and now.
			 * 
			 * @return The amount of time that elapsed between the set time and now
			 */
			inline std::chrono::milliseconds elapsed() const noexcept;

			/**
			 * Calculates how much time has elapsed in the given duration type between the set time and now.
			 *
			 * @tparam D The duration type
			 * @return The amount of time that elapsed between the set time and now
			 */
			template <typename D>
			inline D elapsed() const noexcept;

			/**
			 * Resets this stopwatch time to the beginning of time.
			 */
			inline void clear() noexcept;

			/**
			 * Resets the stopwatch time to the current time.
			 */
			inline void start() noexcept;

			/**
			 * Adds a time duration to this stopwatch time to get a scheduled future stopwatch time.
			 * If the given offset is not positive, this instead returns the end of time to indicate
			 * the time will never be scheduled.
			 * 
			 * @tparam <R> The representation type of the duration offset
			 * @tparam <P> The period ratio type of the duration offset
			 * @param offset The interval to schedule the next stopwatch time
			 * @return The next scheduled stopwatch time, or the end of time if the offset was not valid
			 */
			template <typename R, typename P>
			inline Stopwatch schedule(std::chrono::duration<R, P> offset) const noexcept;

			/**
			 * Prints a stopwatch time to a UTF-8 buffer.
			 * The time is broken down into absolute fields and printed with the most appropriate time unit.
			 *
			 * @param buffer The UTF-8 text buffer
			 * @param length The length of the buffer
			 * @return The actual number of characters that would be needed
			 */
			std::size_t print(char *buffer, std::size_t length) const noexcept;

			/**
			 * Prints a stopwatch time to returned UTF-8 text string.
			 * The time is broken down into absolute fields and printed with the most appropriate time unit.
			 *
			 * @return The UTF-8 text string
			 * @throw std::bad_alloc If memory allocation failed
			 */
			std::string str() const;

			/**
			 * Interprets the stopwatch time in a Boolean context.
			 * This is true if it is not the beginning of time.
			 * 
			 * @return The stopwatch time interpreted as a Boolean
			 */
			inline explicit operator bool() const noexcept;
	};

	/**
	 * Calculates the duration between two stopwatch times.
	 * 
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return The duration between the two times in milliseconds
	 */
	inline std::chrono::milliseconds operator-(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Adds a duration offset to a stopwatch time.
	 * 
	 * @tparam <R> The representation type of the duration offset
	 * @tparam <P> The period ratio type of the duration offset
	 * @param left The stopwatch time
	 * @param offset The duration to add
	 * @return The stopwatch time after the offset is added
	 */
	template <typename R, typename P>
	inline Stopwatch &operator+=(Stopwatch &left, std::chrono::duration<R, P> offset) noexcept;

	/**
	 * Adds a duration offset to a stopwatch time to get a new stopwatch time.
	 *
	 * @tparam <R> The representation type of the duration offset
	 * @tparam <P> The period ratio type of the duration offset
	 * @param left The stopwatch time
	 * @param offset The duration to add
	 * @return The stopwatch time with the offset added
	 */
	template <typename R, typename P>
	inline Stopwatch operator+(const Stopwatch &left, std::chrono::duration<R, P> offset) noexcept;

	/**
	 * Checks to see if two stopwatch times are equal.
	 * 
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the stopwatch times are equal
	 */
	inline bool operator==(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Checks to see if two stopwatch times are not equal.
	 *
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the stopwatch times are not equal
	 */
	inline bool operator!=(const Stopwatch &left, const Stopwatch &right) noexcept;
	
	/**
	 * Checks to see if a stopwatch time is before another stopwatch time.
	 * 
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the first time is before the second
	 */
	inline bool operator<(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Checks to see if a stopwatch time is not after another stopwatch time.
	 *
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the first time is not after the second
	 */
	inline bool operator<=(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Checks to see if a stopwatch time is after another stopwatch time.
	 *
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the first time is after the second
	 */
	inline bool operator>(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Checks to see if a stopwatch time is not before another stopwatch time.
	 *
	 * @param left The first stopwatch time
	 * @param right The second stopwatch time
	 * @return Whether the first time is not before the second
	 */
	inline bool operator>=(const Stopwatch &left, const Stopwatch &right) noexcept;

	/**
	 * Prints a stopwatch time to a C++ stream.
	 * The time is broken down into absolute fields and printed with the most appropriate time unit.
	 * 
	 * @param stream The stream
	 * @param time The stopwatch time
	 * @return The stream
	 * @throw std::exception If the stream threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Stopwatch &time);
}

/* Inline implementation */

namespace cio
{
	inline Stopwatch Stopwatch::max() noexcept
	{
		return std::chrono::time_point<std::chrono::steady_clock>::max();
	}

	inline Stopwatch Stopwatch::now() noexcept
	{
		return std::chrono::steady_clock::now();
	}

	inline Stopwatch::Stopwatch() noexcept :
		value()
	{
		// nothing more to do
	}

	inline Stopwatch::Stopwatch(std::chrono::time_point<std::chrono::steady_clock> value) noexcept :
		value(value)
	{
		// nothing more to do
	}

	inline std::chrono::milliseconds Stopwatch::elapsed(const Stopwatch &epoch) const noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(this->value - epoch.value);
	}

	template <typename D>
	inline D Stopwatch::elapsed(const Stopwatch &epoch) const noexcept
	{
		return std::chrono::duration_cast<D>(this->value - epoch.value);
	}

	inline std::chrono::milliseconds Stopwatch::elapsed() const noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - this->value);
	}

	template <typename D>
	inline D Stopwatch::elapsed() const noexcept
	{
		return std::chrono::duration_cast<D>(std::chrono::steady_clock::now() - this->value);
	}

	inline void Stopwatch::clear() noexcept
	{
		this->value = std::chrono::time_point<std::chrono::steady_clock>();
	}

	inline void Stopwatch::start() noexcept
	{
		this->value = std::chrono::steady_clock::now();
	}

	template <typename R, typename P>
	inline Stopwatch Stopwatch::schedule(std::chrono::duration<R, P> offset) const noexcept
	{
		return offset.count() > 0 ? (this->value + offset) : std::chrono::time_point<std::chrono::steady_clock>::max();
	}

	inline Stopwatch::operator bool() const noexcept
	{
		return this->value != std::chrono::time_point<std::chrono::steady_clock>();
	}

	template <typename R, typename P>
	inline Stopwatch &operator+=(Stopwatch &left, std::chrono::duration<R, P> offset) noexcept
	{
		left.value += offset;
		return left;
	}

	template <typename R, typename P>
	inline Stopwatch operator+(const Stopwatch &left, std::chrono::duration<R, P> offset) noexcept
	{
		return left.value + offset;
	}

	inline std::chrono::milliseconds operator-(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(left.value - right.value);
	}

	inline bool operator==(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value == right.value;
	}

	inline bool operator!=(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value != right.value;
	}

	inline bool operator<(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value < right.value;
	}

	inline bool operator<=(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value <= right.value;
	}

	inline bool operator>(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value > right.value;
	}

	inline bool operator>=(const Stopwatch &left, const Stopwatch &right) noexcept
	{
		return left.value >= right.value;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
