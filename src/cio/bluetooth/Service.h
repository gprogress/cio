/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_SERVICE_H
#define CIO_BLUETOOTH_SERVICE_H

#include "Types.h"

#include "Attribute.h"

#include <utility>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Service class extends the baseline generic Attribute
		 * to organize groups of attributes into a service declaration.
		 *
		 * Services consist of a primary definition type, may include other services by reference,
		 * and then contain one or more Characteristic instances which themselves are organized
		 * collections of lower level attributes into a logical value.
		 *
		 * If the Service is associated with a BLE client, it can use that client to discover
		 * associated attributes and characteristics and perform other queries like reading values by type.
		 *
		 * As an attribute, the Service can also be read (its value is the service definition unique ID)
		 * and its type will always be either TypeId::PrimaryService or TypeId::SecondaryService.
		 * The service's attribute handle is for the definition itself, and the contents of the service
		 * start from the next attribute until the defined end handle.
		 */
		class CIO_BLUETOOTH_API Service : public Attribute
		{
			public:
				/** Iterator type for this container */
				using iterator = Characteristic *;
				
				/** Const Iterator type for this container */
				using const_iterator = const Characteristic *;
			
				/**
				 * Construct an empty service.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * The handle range is set to (0, 0) to indicate it is not set.
				 */					
				Service() noexcept;

				/** 
				 * Constructs a service with the given service type.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * The handle range is set to (0, 0) to indicate it is not set.
				 *
				 * @param serviceType the service type
				 */
				explicit Service(const UniqueId &serviceType) noexcept;

				/**
				 * Construct a service with the given handle range.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * 
				 * @param start The start handle
				 * @param end The end handle
				 */				
				Service(std::uint16_t start, std::uint16_t end) noexcept;
				
				/**
				 * Construct a service with the given handle range.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * 
				 * @param start The start handle
				 * @param end The end handle
				 * @param serviceType the service type
				 */				
				Service(std::uint16_t start, std::uint16_t end, const UniqueId &serviceType) noexcept;
				
				/**
				 * Construct a service with the given handle range and attribute client.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * 
				 * @param start The start handle
				 * @param end The end handle
				 * @param client The attribute client
				 */	
				Service(std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept;
				
				/**
				 * Construct a service with the given handle range and attribute client.
				 * The Attribute type is intialized to TypeId::PrimaryService.
				 * 
				 * @param start The start handle
				 * @param end The end handle
				 * @param serviceType the service type
				 * @param client The attribute client
				 */	
				Service(std::uint16_t start, std::uint16_t end, const UniqueId &serviceType, AttributeClient *client) noexcept;
				
				/**
				 * Construct a copy of a service.
				 *
				 * @param in The service to copy
				 */
				Service(const Service &in);
				
				/**
				 * Move construct a service.
				 *
				 * @param in The service to copy
				 */
				Service(Service &&in) noexcept;
				
				/**
				 * Copies a service.
				 *
				 * @param in The service to copy
				 * @return this service
				 */
				Service &operator=(const Service &in);
				
				/**
				 * Moves a service.
				 *
				 * @param in The service to move
				 * @return this service
				 */
				Service &operator=(Service &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Service() noexcept override;
				
				/**
				 * Clears the state of the service
				 */
				virtual void clear() noexcept override;
				
				/**
				 * Sets the attribute client to use for service queries.
				 * This overrides the base class to also set the client on all contained characteristics.
				 *
				 * @param client The attribute client to set
				 */
				virtual void setClient(AttributeClient *client) noexcept override;

				/**
				 * Validates the service information.
				 * This validates the service handle range is in order, that characteristics are in sorted order, and that characteristic end handles are properly set.
				 * This also validates each characteristic.
				 */
				virtual void validate() override;
				
				/**
				 * Synchronizes the service definition with the underlying attribute client.
				 * In addition to base Attribute synchronization, this also discovers all characteristics.
				 *
				 * @return the status of the synchronization (Success for updates, Exists if discovery was already complete, Unconnected if there is no client)
				 */
				virtual State synchronize() override;
				
				/**
				 * Gets the definition type of the service.
				 * This is technically the value of the service attribute, but is what most people think
				 * of when they think of the service type.
				 *
				 * @return the service definition unique ID
				 */
				const UniqueId &getServiceType() const noexcept;
				
				/**
				 * Sets the definition type of the service.
				 * This is technically the value of the service attribute, but is what most people think
				 * of when they think of the service type.
				 *
				 * @param id the service definition unique ID
				 */
				void setServiceType(const UniqueId &id) noexcept;
				
				/**
				 * Gets the name of the service's definition type.
				 * This is technically the name of the service's value UUID.
				 * This requires a BLE client to be set and will consult its type dictionary.
				 *
				 * @return the service name
				 */
				Text getServiceName() const noexcept;
				
				/**
				 * Sets the service's definition type given its name.
				 * This requires a BLE client to be set and will consult its type dictionary.
				 *
				 * @param name The type name
				 * @return the discovered type ID, or the empty ID if none matched
				 * @throw cio::Exception If no BLE client exists, or the given name did not match any type
				 */
				const UniqueId &setServiceTypeByName(const Text &name);
				
				// Read by type support - convenience API

				/**
				 * Reads the first attribute value in the service that matches the given type into a data buffer.
				 *
				 * @param type the attribute type
				 * @param data The data buffer to read into
				 * @param length the maximum number of bytes to read into the buffer
				 * @return a pair with the handle of the read attribute and the number of bytes actually read
				 * @throw cio::Exception If no BLE client exists or if the underlying protocol queries failed
				 */
				std::pair<std::uint16_t, std::size_t> readValueByType(const UniqueId &type, void *data, std::size_t length) const;
				
				/**
				 * Reads the first attribute value in the service that matches the given type returning it as a data type T,
				 * which can be either a C++ primitive value or std::string or cio::Text.
				 *
				 * This is the easiest way to read an attribute if you only know its value type and not anything else
				 * and don't care about its handle or any associated schema or metadata.
				 *
				 * @tparam <T> The data type to read
				 * @param type the attribute type
				 * @return the read value
				 * @throw cio::Exception If no BLE client exists or if the underlying protocol queries failed
				 */
				template <typename T>
				T readValueByType(const UniqueId &type) const;			
				
				// Characteristic management via STL container API
				
				/**
				 * Checks whether there are characteristics associated with this service.
				 *
				 * @return whether there are characteristics
				 */
				bool empty() const noexcept;
				
				/**
				 * Gets the number of characteristics associated with this service.
				 *
				 * @return the number of characteristics
				 */
				std::size_t size() const noexcept;
				
				/**
				 * Gets a start iterator to the list of characteristics associated with this service.
				 *
				 * @return the characteristic start iterator
				 */
				Service::iterator begin() noexcept;
				
				/**
				 * Gets an end iterator to the list of characteristics associated with this service.
				 *
				 * @return the characteristic start iterator
				 */
				Service::iterator end() noexcept;
				
				/**
				 * Gets a start iterator to the list of characteristics associated with this service.
				 *
				 * @return the characteristic start iterator
				 */
				Service::const_iterator begin() const noexcept;
								
				/**
				 * Gets an end iterator to the list of characteristics associated with this service.
				 *
				 * @return the characteristic start iterator
				 */
				Service::const_iterator end() const noexcept;
				
				/**
				 * Gets the characteristic at the given sequence index in the service.
				 * The index is NOT the handle, use getCharacteristic for that.
				 *
				 * @param idx The index
				 * @return the characteristic
				 */
				Characteristic &operator[](std::size_t idx);
				
				/**
				 * Gets the characteristic at the given sequence index in the service.
				 * The index is NOT the handle, use getCharacteristic for that.
				 *
				 * @param idx The index
				 * @return the characteristic
				 */
				const Characteristic &operator[](std::size_t idx) const;
				
				/**
				 * Finds the iterator to the characteristic that contains the given handle.
				 * If no such characteristic is found, the end iterator is returned.
				 *
				 * @return the iterator to the characteristic, or end iterator if not found
				 */ 
				Service::iterator find(std::uint16_t handle) noexcept;
				
				/**
				 * Finds the iterator to the characteristic that contains the given handle.
				 * If no such characteristic is found, the end iterator is returned.
				 *
				 * @return the iterator to the characteristic, or end iterator if not found
				 */ 
				Service::const_iterator find(std::uint16_t handle) const noexcept;
				
				/**
				 * Finds the iterator to the first characteristic with the given value type.
				 * If no such characteristic is found, the end iterator is returned.
				 *
				 * @return the iterator to the declaration, or end iterator if not found
				 */ 
				Service::iterator find(const UniqueId &type) noexcept;
				
				/**
				 * Finds the iterator to the first characteristic with the given value type.
				 * If no such characteristic is found, the end iterator is returned.
				 *
				 * @return the iterator to the declaration, or end iterator if not found
				 */
				Service::const_iterator find(const UniqueId &type) const noexcept;
				
				/**
				 * Gets the list of characteristics associated with this service.
				 *
				 * @return the characteristics
				 */
				std::vector<Characteristic> &getCharacteristics() noexcept;
				
				/**
				 * Gets the list of characteristics associated with this service.
				 *
				 * @return the characteristics
				 */
				const std::vector<Characteristic> &getCharacteristics() const noexcept;
				
				/**
				 * Gets the characteristic that contains the given handle.
				 * Normally this should be the handle of the characteristic itself, but it will also
				 * return the proper characteristic if you give any of its declaration handles.
				 *
				 * @parma handle The characteristic handle
				 * @return the characteristic
				 * @throw std::out_of_range If the handle is not part of the service range
				 * @throw cio::Exception If discovery failed
				 * @throw std::bad_alloc If allocating memory for characteristics failed
				 */
				Characteristic &getCharacteristic(std::uint16_t handle);
				
				/**
				 * Gets the characteristic that contains the given handle.
				 * Normally this should be the handle of the characteristic itself, but it will also
				 * return the proper characteristic if you give any of its declaration handles.
				 *
				 * @parma handle The characteristic handle
				 * @return the characteristic
				 * @throw std::out_of_range If the handle is not part of the service range
				 * @throw cio::Exception If discovery failed
				 * @throw std::bad_alloc If allocating memory for characteristics failed
				 */
				const Characteristic &getCharacteristic(std::uint16_t handle) const;
				
				/**
				 * Gets the first characteristic whose value type is the given UUID.
				 *
				 * @param type The value type
				 * @return the characteristic
				 * @throw std::invalid_argument If the type is the invalid UUID
				 * @throw cio::Exception If discovery failed or no such characteristic exists
				 * @throw std::bad_alloc If allocating memory for characteristics failed
				 */
				Characteristic &getCharacteristic(const UniqueId &type);
				
				/**
				 * Gets the first characteristic whose value type is the given UUID.
				 *			 *
				 * @param type The value type
				 * @return the characteristic
				 * @throw std::invalid_argument If the type is the invalid UUID
				 * @throw cio::Exception If discovery failed or no such characteristic exists
				 * @throw std::bad_alloc If allocating memory for characteristics failed
				 */
				const Characteristic &getCharacteristic(const UniqueId &type) const;
				
				/**
				 * Manually sets the list of cached characteristics and mark discovery as completed.
				 * This can be used by more general discovery mechanisms (depending on the attribute client)
				 * or to restore persisted characteristic information that was saved offline.
				 *
				 * @warning This method expects the attributes to be sorted without duplicates and within the characteristic handle range.
				 * Call validateCachedDeclarations() afterward if that might not be the case.
				 *
				 * @param chars The characteristics
				 */
				void setCharacteristics(const std::vector<Characteristic> &chars);
				
				/**
				 * Manually sets the list of cached characteristics and mark discovery as completed.
				 * This can be used by more general discovery mechanisms (depending on the attribute client)
				 * or to restore persisted characteristic information that was saved offline.
				 *
				 * @warning This method expects the attributes to be sorted without duplicates and within the characteristic handle range.
				 * Call validateCachedDeclarations() afterward if that might not be the case.
				 *
				 * @param chars The characteristics
				 */
				void setCharacteristics(std::vector<Characteristic> &&chars) noexcept;
				
				/**
				 * Validates that the list of cached characteristics are within the service handle range without duplicates and are in sorted order.
				 * Characteristics that are outside of the service range are discarded.
				 * Each characteristic's declarations are also validated and this also calls validateCharacteristicBoundaries().
				 */
				void validateCharacteristics();
				
				/**
				 * Sets the end handle for cached characteristics so that each characteristic ends one handle before the next one starts.
				 * The final characteristic end handle is set to the service end handle.
				 * This also ensures all characteristic clients are set to the service's client.
				 */
				void validateCharacteristicBoundaries();
				
				/**
				 * Clears the cached list of characteristics previously discovered and marks discovery as not completed.
				 *
				 * @return the cached list of known characteristics
				 */
				void clearCharacteristics() noexcept;

				// Discovery - used to update cache
				
				/**
				 * Discovers the list of characteristics from the attribute client using the service handle range.
				 * This does not consult the cached characteristics.
				 *
				 * @return the list of cached characteristics
				 */
				std::vector<Characteristic> discoverCharacteristics() const;
				
				/**
				 * If characteristic discovery has not yet been performed, do so.
				 *
				 * @return the status of the synchronization (Success for updates, Exists if discovery was already complete, Unconnected if there is no client)
				 */
				State synchronizeCharacteristics();
								
				/**
				 * Locates the internal index offset of the characteristic whose handle range contains the given handle.
				 * This requires characteristic synchronization to have already been performed.
				 * If no such characteristic exists, the size of the characteristic array is returned.
				 *
				 * @param handle The handle
				 * @return the index of the characteristic or the end index if not found
				 */
				std::size_t locateCharacteristicIndex(std::uint16_t handle) const noexcept;
				
				/**
				 * Locates the internal index offset of the first characteristic whose value type is the given type.
				 * This requires characteristic synchronization to have already been performed.
				 * If no such characteristic exists, the size of the characteristic array is returned.
				 *
				 * @param handle The handle
				 * @return the index of the characteristic or the end index if not found
				 */
				std::size_t locateCharacteristicIndex(const UniqueId &type) const noexcept;
								
				/**
				 * Locates the internal index offset range of the characteristics whose handle ranges overlap the given handle range.
				 * This requires characteristic synchronization to have already been performed.
				 * The handle range is inclusive of both start and end.
				 * The returned index range is exclusive of the end.
				 * If no such characteristics exist, an empty range is returned.
				 *
				 * @param start The lowest handle of the handle range to find
				 * @param end The highest handle (inclusive) of the handle range to find
				 * @return the range of index values (exclusive of end) overlapping the given handle range
				 */
				std::pair<std::size_t, std::size_t> locateCharacteristicRange(std::uint16_t start, std::uint16_t end) const noexcept;
				
			private:
				/** The logger for this class */
				static Logger sLogger;
				
				/** The list of known characteristics */
				std::vector<Characteristic> mCharacteristics;
				
				/** The service definition type (value) for this class */
				UniqueId mServiceType;
		};
	}
}

/* Inline implementation */

namespace cio
{
	namespace bluetooth
	{
		template <typename T>
		T Service::readValueByType(const UniqueId &type) const
		{
			T tmp = T();
			std::pair<std::uint16_t, std::size_t> result = this->readValueByType(type, &tmp, sizeof(T));
			if (result.second < sizeof(T))
			{
				throw cio::Exception(Reason::Underflow, "Attribute was not long enough for value");
			}
			return tmp;
		}
		
		template <>
		CIO_BLUETOOTH_API std::string Service::readValueByType<std::string>(const UniqueId &type) const;
		
		template <>
		CIO_BLUETOOTH_API cio::Text Service::readValueByType<cio::Text>(const UniqueId &type) const;
				
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

