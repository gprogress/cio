/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "ReadBuffer.h"

#include "Buffer.h"
#include "Class.h"
#include "JsonWriter.h"
#include "Text.h"

#include <algorithm>

namespace cio
{
	Class<ReadBuffer> ReadBuffer::sMetaclass("cio::ReadBuffer");

	const Metaclass &ReadBuffer::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}
		
	// Constructor implementations

	ReadBuffer::ReadBuffer() noexcept :
		mFactory(nullptr),		
		mElements(nullptr),
		mSize(0),
		mLimit(0),
		mPosition(0)
	{
		// nothing more to do
	}

	ReadBuffer::ReadBuffer(const void *data, std::size_t capacity)  noexcept :
		mFactory(nullptr),	
		mElements(static_cast<const std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0)
	{
		// nothing more to do
	}

	ReadBuffer::ReadBuffer(const void *data, std::size_t capacity, std::size_t limit, std::size_t position) noexcept :
		mFactory(nullptr),
		mElements(static_cast<const std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(std::min(limit, mSize)),
		mPosition(std::min(position, mLimit))
	{
		// nothing more to do
	}

	ReadBuffer::ReadBuffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept :
		mFactory(allocator),	
		mElements(static_cast<const std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(capacity),
		mPosition(0)
	{
		// nothing more to do
	}

	ReadBuffer::ReadBuffer(void *data, std::size_t capacity, Factory<std::uint8_t> allocator, std::size_t limit, std::size_t position) noexcept :
		mFactory(allocator),
		mElements(static_cast<const std::uint8_t *>(data)),
		mSize(capacity),
		mLimit(std::min(limit, mSize)),
		mPosition(std::min(position, mLimit))
	{
		// nothing more to do
	}

	ReadBuffer::ReadBuffer(const Buffer &in) noexcept :
		mFactory(in.mFactory),
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition)
	{
		if (mFactory)
		{
			mElements = mFactory.copy(mElements, mSize);
		}
	}
	
	ReadBuffer::ReadBuffer(Buffer &&in) noexcept :
		mFactory(in.mFactory),
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition)
	{
		// nothing more to do
	}
	
	ReadBuffer::ReadBuffer(const ReadBuffer &in) :
		mFactory(in.mFactory),	
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition)
	{
		if (mFactory)
		{
			mElements = mFactory.copy(mElements, mSize);
		}
	}	

	ReadBuffer::ReadBuffer(ReadBuffer &&in) noexcept :
		mFactory(in.mFactory),	
		mElements(in.mElements),
		mSize(in.mSize),
		mLimit(in.mLimit),
		mPosition(in.mPosition)
	{
		in.mFactory = nullptr;
		in.mElements = nullptr;
		in.mSize = 0;
		in.mPosition = 0;
		in.mLimit = 0;
	}
	
	ReadBuffer &ReadBuffer::operator=(const ReadBuffer &in)
	{
		if (this != &in)
		{
			this->clear();
			
			mFactory = nullptr;
			mElements = mFactory ? mFactory.copy(in.mElements, in.mSize) : in.mElements;
			mSize = in.mSize;
			mPosition = in.mPosition;
			mLimit = in.mLimit;
		}

		return *this;
	}
	
	ReadBuffer &ReadBuffer::operator=(ReadBuffer &&in) noexcept
	{
		if (this != &in)
		{
			mFactory = in.mFactory;
			mElements = in.mElements;
			mSize = in.mSize;
			mPosition = in.mPosition;
			mLimit = in.mLimit;

			in.mFactory = nullptr;
			in.mElements = nullptr;
			in.mSize = 0;
			in.mPosition = 0;
			in.mLimit = 0;
		}

		return *this;
	}

	ReadBuffer::~ReadBuffer() noexcept
	{
		mFactory.destroy(const_cast<std::uint8_t *>(mElements), mSize);
	}
		
	void ReadBuffer::clear() noexcept
	{
		mFactory.destroy(const_cast<std::uint8_t *>(mElements), mSize);
		mFactory = nullptr;
		
		mElements = nullptr;
		mSize = 0;
		mPosition = 0;
		mLimit = 0;
	}

	const Metaclass &ReadBuffer::getMetaclass() const noexcept
	{
		return sMetaclass;
	}
		
	void ReadBuffer::bind(void *data, std::size_t capacity, Factory<std::uint8_t> allocator) noexcept
	{
		this->clear();
		
		mElements = static_cast<std::uint8_t *>(data);
		mFactory = allocator;
		mSize = capacity;
		mLimit = mSize;
		mPosition = 0;
	}

	// Core ReadBuffer API - direct calls

	void ReadBuffer::bind(const void *data, std::size_t capacity) noexcept
	{
		this->clear();

		mElements = static_cast<const std::uint8_t *>(data);
		mSize = capacity;
		mLimit = mSize;
		mPosition = 0;
	}

	std::size_t ReadBuffer::send(Buffer &output) noexcept
	{
		std::size_t toCopy = std::min(mLimit - mPosition, output.mLimit - output.mPosition);
		std::memcpy(output.mElements + output.mPosition, mElements + mPosition, toCopy);
		mPosition += toCopy;
		output.mPosition += toCopy;
		return toCopy;
	}

	std::size_t ReadBuffer::send(Buffer &output, std::size_t maxLength) noexcept
	{
		std::size_t toCopy = std::min(maxLength, std::min(mLimit - mPosition, output.mLimit - output.mPosition));
		std::memcpy(output.mElements + output.mPosition, mElements + mPosition, toCopy);
		mPosition += toCopy;
		output.mPosition += toCopy;
		return toCopy;
	}

	const std::uint8_t *ReadBuffer::internalize()
	{
		if (mElements && !mFactory)
		{
			mFactory = Factory<std::uint8_t>();
			mElements = mFactory.copy(mElements, mSize);
		}

		return mElements;
	}

	const std::uint8_t *ReadBuffer::internalize(const void *data, std::size_t capacity)
	{
		if (mElements != data || !mFactory)
		{
			this->clear();

			if (capacity > 0)
			{
				mFactory = Factory<std::uint8_t>();
				mElements = mFactory.copy(static_cast<const std::uint8_t *>(data), capacity);
				mSize = capacity;
				mPosition = 0;
				mLimit = capacity;
			}
		}

		return mElements;
	}

	const std::uint8_t *ReadBuffer::internalize(const void *data, std::size_t capacity, Factory<std::uint8_t> allocator)
	{
		if (mElements != data || !mFactory)
		{
			this->clear();

			if (capacity > 0)
			{
				mFactory = Factory<std::uint8_t>();
				mElements = mFactory.copy(static_cast<const std::uint8_t *>(data), capacity);
				mSize = capacity;
				mPosition = 0;
				mLimit = capacity;
			}
		}

		return mElements;
	}

	Buffer ReadBuffer::duplicate() const
	{
		Buffer buffer;

		if (mElements)
		{
			Factory<std::uint8_t> allocator;
			buffer.bind(allocator.copy(mElements, mSize), mSize, allocator);
			buffer.mPosition = mPosition;
			buffer.mLimit = mLimit;
		}

		return buffer;
	}

	Text ReadBuffer::getText()
	{
		const char *current = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = mLimit - mPosition;
		std::size_t end = cio::terminator(current, mLimit - mPosition);
		std::size_t length = end + (end < capacity); // include null terminator if present
		mPosition += length;
		return Text(current, length);
	}

	Text ReadBuffer::getText(std::size_t bytes)
	{
		const char *current = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = std::min(bytes, mLimit - mPosition);
		std::size_t end = cio::terminator(current, mLimit - mPosition);
		std::size_t length = end + (end < capacity); // include null terminator if present
		mPosition += length;
		return Text(current, length);
	}

	Text ReadBuffer::getTextLine(std::size_t bytes) noexcept
	{
		const char *text = reinterpret_cast<const char *>(mElements + mPosition);
		std::size_t capacity = std::min(bytes, mLimit - mPosition);
		std::size_t length = 0;

		while (length < capacity)
		{
			char c = text[length++];
			if (!c || c == '\n')
			{
				break;
			}
		}

		mPosition += length;
		return Text(text, length);
	}

	Text ReadBuffer::viewProcessedText() const noexcept
	{
		const char *data = reinterpret_cast<const char *>(mElements);
		return Text(data, mPosition);
	}

	Text ReadBuffer::viewRemainingText() const noexcept
	{
		const char *data = reinterpret_cast<const char *>(mElements + mPosition);
		return Text(data, mLimit - mPosition);
	}

	ReadBuffer ReadBuffer::view() const noexcept
	{
		return ReadBuffer(mElements, mSize, mLimit, mPosition);
	}

	ReadBuffer ReadBuffer::viewProcessed() const noexcept
	{
		return ReadBuffer(mElements, mSize, mPosition, 0);
	}

	ReadBuffer ReadBuffer::viewRemaining() const noexcept
	{
		return ReadBuffer(mElements + mPosition, mLimit - mPosition, mLimit - mPosition, 0);
	}

	std::string ReadBuffer::print() const
	{
		JsonWriter writer;
		this->print(writer);
		return writer.getWriteBuffer().viewProcessedText();
	}

	std::size_t ReadBuffer::print(char *buffer, std::size_t capacity) const noexcept
	{
		std::size_t needed = SIZE_MAX;
		try
		{
			JsonWriter writer(buffer, capacity);
			this->print(writer);
			needed = writer.getWriteBuffer().position();
		}
		catch (...)
		{
			// printing failed, size is unknown
		}
		return needed;
	}

	void ReadBuffer::print(MarkupWriter &writer) const
	{
		writer.writeSimpleNondefaultElement("position", mPosition);
		writer.writeSimpleNondefaultElement("limit", mLimit);
		writer.writeSimpleNondefaultElement("size", mSize);
		if (mFactory)
		{
			// TODO implement allocator printing
			// writer.writeSimpleElement("allocator", mFactory.metaclass->getName());
		}
		writer.startElement("elements");
		writer.writeValue(mElements, mSize);
		writer.endValue();
	}
}
