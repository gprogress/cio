/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ATTRIBUTE_H
#define CIO_BLUETOOTH_ATTRIBUTE_H

#include "Types.h"

#include "UniqueId.h"

#include <cio/Exception.h>
#include <cio/Text.h>

#include <string>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Attribute provides a general representation of a Generic Attribute
		 * as used in Bluetooth Low Energy (BLE). Attributes are defined by a 16-bit unsigned handle unique to the
		 * server, a globally unique type ID (either 16-bit or 128-bit), and a data value that can be accessed in some way.
		 *
		 * If a Bluetooth Low Energy Client is associated with this Attribute, it can be used to read and write the attribute value
		 * and perform other queries.
		 *
		 * More specific GATT types such as Characteristic, Descriptor, and Service subclass the baseline Attribute.
		 */
		class CIO_BLUETOOTH_API Attribute
		{
			public:		
				/**
				 * Construct the empty attribute.
				 * The start and end handle is initialized to 0x0000 and the type UUID is initialized to the empty UUID.
				 */
				Attribute() noexcept;
				
				/**
				 * Construct the attribute with the given handle that is used for both start and end.
				 * The type is initialized to the empty UUID.
				 *
				 * @param handle The handle
				 */
				explicit Attribute(std::uint16_t handle) noexcept;
				
				/**
				 * Construct the attribute with the given start and end handle
				 * The type is initialized to the empty UUID.
				 *
				 * @param start The start handle
				 * @param end The end handle
				 */
				explicit Attribute(std::uint16_t start, std::uint16_t end) noexcept;	
				
				/**
				 * Construct the attribute with the given type.
				 * The handle is initialized to 0x0000
				 *
				 * @param type The type
				 */
				explicit Attribute(const UniqueId &type) noexcept;
				
				/**
				 * Construct the attribute with the given type and handle.
				 * The handle is used for both start and end handle.
				 *
				 * @param type The type
				 * @param handle The handle
				 */
				Attribute(const UniqueId &type, std::uint16_t handle) noexcept;
				
				/**
				 * Construct the attribute with the given type and start and end handle.
				 *
				 * @param type The type
				 * @param start The start handle
				 * @param end The end handle
				 */
				Attribute(const UniqueId &type, std::uint16_t start, std::uint16_t end) noexcept;
				
				/**
				 * Construct the attribute with the given type and handle with the given attribute client.
				 * The start and end handle are both initialized to the given handle.
				 *
				 * @param type The type
				 * @param handle The handle
				 * @param client The attribute client
				 */
				Attribute(const UniqueId &type, std::uint16_t handle, AttributeClient *client) noexcept;
				
				/**
				 * Construct the attribute with the given handle and type
				 * linked to the given attribute client.
				 *
				 * @param type The type
				 * @param start The start handle
				 * @param end The end handle
				 * @param client The attribute client
				 */
				Attribute(const UniqueId &type, std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept;
				
				/**
				 * Construct a copy of an attribute.
				 * 
				 * @param in The attribute to copy
				 */
				Attribute(const Attribute &in);
								
				/**
				 * Move constructs an attribute.
				 * 
				 * @param in The attribute to move
				 */
				Attribute(Attribute &&in) noexcept;
				
				/**
				 * Copy an attribute.
				 * 
				 * @param in The attribute to copy
				 * @return this attribute
				 */
				Attribute &operator=(const Attribute &in);
				
				/**
				 * Moves an attribute.
				 * 
				 * @param in The attribute to move
				 * @return this attribute
				 */
				Attribute &operator=(Attribute &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				virtual ~Attribute() noexcept;
				
				// Polymorphic API - subclasses need to override
				
				/**
				 * Clears the state of this attribute.
				 * The handle is initialized to 0x0000 and the type UUID is initialized to the empty UUID.
				 * The Client pointer is reset to nullptr.
				 */
				virtual void clear() noexcept;
				
				/**
				 * Sets the BLE client associated with the attribute.
				 * The attribute does not take ownership the client.
				 *
				 * The base class just sets the client pointer.
				 * Subclasses should override to propagate to all child attributes as well.
				 *
				 * @param client the BLE client or nullptr to disconnect the attribute
				 */
				virtual void setClient(AttributeClient *client) noexcept;

				/**
				 * Synchronizes the attribute state with the underlying attribute client.
				 *
				 * The base class will get the attribute type if not set.
				 * Subclasses should override to discover contained attributes like characteristics or declarations.
				 *
				 * @return the status of the synchronization attempt (Reason::Exists if discovery is already done, Reason::Unopened if
				 * no connection was available)
				 * @throw Exception If synchronization was attempted through a valid connection but failed
				 */
				virtual State synchronize();
				
				/**
				 * Validates the current content of the attribute, determining missing content and correcting any issues.
				 * For the base Attribute class, this checks to see if the start handle is less than or equal to the end handle.
				 * If they are not (and neither are the invalid handle 0x0000) then they are swapped.
				 *
				 * Subclasses should override to additional make sure that child attributes are sorted, within the handle range, and segmented properly.
				 */
				virtual void validate();
					
				// Basic Attribute properties - handles, types, clients
					
				/**
				 * Gets the 16-bit handle of this attribute.
				 * This is equivalent to the start handle, but with the context of intending to represent the attribute itself rather than a range.
				 *
				 * @return the attribute handle
				 */
				std::uint16_t getHandle() const noexcept;
				
				/**
				 * Sets the 16-bit handle of this attribute.
				 * This sets both the start handle and end handle to this value, which is typically what you want for declarations and other leaf attributes.
				 *
				 * @param handle the attribute handle
				 */
				void setHandle(std::uint16_t handle) noexcept;
				
				/**
				 * Gets the 16-bit start handle of the handle range covered by this attribute.
				 * This attribute should be at the start handle and all child attributes (if any) should be after it.
				 * The special value 0x0000 indicates that the start handle is unknown.
				 *
				 * @return the attribute range start handle
				 */
				std::uint16_t getStartHandle() const noexcept;
				
				/**
				 * Sets the 16-bit start handle of the handle range covered by this attribute.
				 * The special value 0x0000 indicates that the start handle is unknown.
				 *
				 * Note that this may invalidate some of the child attributes of subclasses, which should be corrected using the validate() method.
				 *
				 * @param handle the attribute start handle
				 */
				void setStartHandle(std::uint16_t start) noexcept;
				
				/**
				 * Gets the 16-bit end handle of the handle range covered by this attribute.
				 * The end handle is inclusive and all child attributes (if any) of this attribute should be at or before it.
				 * The special value 0x0000 indicates that the end handle is unknown.
				 *
				 * @return the attribute range start handle
				 */
				std::uint16_t getEndHandle() const noexcept;
				
				/**
				 * Sets the 16-bit end handle of the handle range covered by this attribute.
				 * The end handle is inclusive and all child attributes (if any) of this attribute should be at or before it.
				 * The special value 0x0000 indicates that the end handle is unknown.
				 *
				 * @param handle the attribute end handle
				 */
				void setEndHandle(std::uint16_t start) noexcept;
				
				/**
				 * Sets the 16-bit handle range of this attribute directly in one call.
				 * This is equivalent to separately calling setStartHandle and setEndHandle, except that it also validates that start <= end.
				 * If start > end and neither are the special value 0x0000 to indicate unknown, they are swapped.
				 *
				 * @param start The start handle
				 * @param end The end handle
				 */
				void setHandleRange(std::uint16_t start, std::uint16_t end) noexcept;
				
				/**
				 * Checks whether this attribute handle range contains the given handle.
				 *
				 * @param handle The handle
				 * @return whether the handle is inside of the range of this attribute
				 */
				bool containsHandle(std::uint16_t handle) const noexcept;
				
				/**
				 * Checks whether this attribute handle range intersects the given handle range.
				 *
				 * @param start The start handle
				 * @param end The end handle
				 * @return whether the handle range intersects the range of this attribute
				 */
				bool intersectsHandleRange(std::uint16_t start, std::uint16_t end) const noexcept;
				
				/**
				 * Checks whether this attribute handle range fully contains the given handle range.
				 *
				 * @param start The start handle
				 * @param end The end handle
				 * @return whether the range of this attribute fully contains the given handle range
				 */
				bool containsHandleRange(std::uint16_t start, std::uint16_t end) const noexcept;
				
				/**
				 * Checks whether this attribute's start handle is contained inside the given range.
				 *
				 * @param start The start handle
				 * @param end The end handle
				 * @return whether the given handle range contains this attribute's start handle
				 */
				bool isHandleContained(std::uint16_t start, std::uint16_t end) const noexcept;
				
				/**
				 * Clears the handle range of this attribute to set both start and end to the special value 0x0000 to indicate unknown.
				 */
				void clearHandleRange() noexcept;
				
				/**
				 * Gets the Unique ID representing the type of the attribute.
				 * For specialized attributes like services and characteristics, this is the type itself such as "Primary Service"
				 * rather than its definition value.
				 *
				 * @return the type of this attribute
				 */
				const UniqueId &getType() const noexcept;
				
				/**
				 * Sets the Unique ID representing the type of the attribute.
				 * For specialized attributes like services and characteristics, this is the type itself such as "Primary Service"
				 * rather than its definition value.
				 *
				 * @param id the type of this attribute
				 */
				void setType(const UniqueId &id) noexcept;
								
				/**
				 * Gets the name of the attribute's type.
				 * This requires a BLE Client to be set and will consult its type dictionary.
				 *
				 * @note For grouping attributes like characteristics and services, this will be the name of its actual
				 * type such as "Primary Service" or "Characteristic" and not the logical name of the service definition or value.
				 *
				 * @return the type name
				 */
				Text getName() const noexcept;
				
				/**
				 * Sets the attribute type given its name.
				 * This requires a BLE Client to be set and will consult its type dictionary.
				 *
				 * @param name The type name
				 * @return the discovered type ID, or the empty ID if none matched
				 * @throw cio::Exception If no BLE Client exists, or the given name did not match any type
				 */
				const UniqueId &setTypeByName(const Text &name);
				
				/**
				 * Gets the BLE client associated with the attribute.
				 * The attribute does not own the client.
				 *
				 * @return the BLE client or nullptr if none exists
				 */
				AttributeClient *getClient() noexcept;
				
				/**
				 * Gets the BLE Client associated with the attribute.
				 * The attribute does not own the Client.
				 *
				 * @return the BLE client or nullptr if none exists
				 */
				const AttributeClient *getClient() const noexcept;
								
				/**
				 * Checks whether discovery and synchronization has been performed on this attribute.
				 * This is true if both base discovery and content discovery have been complete.
				 *
				 * @return whether discovery has been completed
				 */
				bool isDiscoveryComplete() const noexcept;
				
				/**
				 * Manually marks whether discovery is complete.
				 * This can be used to ensure discovery is marked complete even if no information was found.
				 * This marks both base attribute and content discovery as complete.
				 *
				 * @param value Whether to mark discovery complete
				 */
				void setDiscoveryComplete(bool value) noexcept;
				
				/**
				 * Synchronizes the attribute's type and handle, filling in missing values where possible.
				 * This is only performed if the base discovery is not complete and marks base discovery complete when done.
				 * If the type is set but the start handle is missing (0x0000) the client is to discover the first attribute of the type.
				 * If the type is missing but the start handle is set, the client is used to obtain the type.
				 *
				 * @return the status of the synchronization attempt (Reason::Exists if discovery is already done, Reason::Unopened if
				 * no connection was available)
				 * @throw Exception If synchronization was attempted through a valid connection but failed
				 */
				State synchronizeTypeHandle();
				
				/**
				 * Checks whether discovery and synchronization has been performed on the base portion of this attribute.
				 * This covers the handle range and type.
				 *
				 * @return whether the handle range and type have been discovered
				 */
				bool isBaseDiscoveryComplete() const noexcept;
				
				/**
				 * Marks whether discovery and synchronization has been performed on the base portion of this attribute.
				 * This covers the handle range and type.
				 *
				 * @param value whether the handle range and type have been discovered
				 */
				void setBaseDiscoveryComplete(bool value) noexcept;
				
				/**
				 * Checks whether discovery and synchronization has been performed on the content covered by the handle range.
				 * For the base Attribute class this is purely informational. For subclasses it can control synchronization
				 * of content such as characteristics or declarations.
				 *
				 * @return whether the content in the handle range has been discovered
				 */
				bool isContentDiscoveryComplete() const noexcept;
				
				/**
				 * Sets whether discovery and synchronization has been performed on the content covered by the handle range.
				 * For the base Attribute class this is purely informational. For subclasses it can control synchronization
				 * of content such as characteristics or declarations.
				 *
				 * @param value whether the content in the handle range has been discovered
				 */
				void setContentDiscoveryComplete(bool value) noexcept;
			
				/**
				 * Interprets the attribute declaration in a Boolean context.
				 * It is considered true if its handle is set to a non-zero value.
				 *
				 * @return whether the attribute handle is set
				 */
				explicit operator bool() const noexcept;
			
			protected:			
				/** The UUID of the type of the attribute */
				UniqueId mType;
				
				/** The Bluetooth Low Energy Client used to perform requests and responses */	
				AttributeClient *mClient;
			
				/** The 16-bit handle of the attribute */
				std::uint16_t mStartHandle;
				
				/** The 16-bit bit handle of the end of the attribute's content range. */
				std::uint16_t mEndHandle;
								
				/** Whether discovery of base attribute state has been performed on the current connection */
				bool mDiscovered;
				
				/** Whether discovery of contained content has been performed on the current connection */
				bool mContentDiscovered;
				
			private:
				/** The metaclass for this class */
				static Class<Attribute> sMetaclass;
		};
		
		/**
		 * Compare two attributes for equality.
		 * This is true if they have the same start and end handle and type.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the attributes are equal
		 */
		CIO_BLUETOOTH_API bool operator==(const Attribute &left, const Attribute &right) noexcept;
		
		/**
		 * Compare two attributes for inequality.
		 * This is false if they have the same start and end handle and type.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the attributes are not equal
		 */
		CIO_BLUETOOTH_API bool operator!=(const Attribute &left, const Attribute &right) noexcept;
		
		/**
		 * Compare two attributes for less than sort order.
		 * This checks the start handles to see if the left start handle is less than the right.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the left attribute sorts before the right attribute
		 */
		CIO_BLUETOOTH_API bool operator<(const Attribute &left, const Attribute &right) noexcept;
		
		/**
		 * Compare two attributes for less than or equal to sort order.
		 * This checks the start handles to see if the left start handle is less than or equal to the right.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the left attribute does not sort after the right attribute
		 */
		CIO_BLUETOOTH_API bool operator<=(const Attribute &left, const Attribute &right) noexcept;
		
		/**
		 * Compare two attributes for greater than sort order.
		 * This checks the start handles to see if the left start handle is greater than the right.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the left attribute sorts after the right attribute
		 */
		CIO_BLUETOOTH_API bool operator>(const Attribute &left, const Attribute &right) noexcept;
		
		/**
		 * Compare two attributes for greater than or equal to sort order.
		 * This checks the start handles to see if the left start handle is greater than or equal to the right.
		 *
		 * @param left The first attribute
		 * @param right The second attribute
		 * @return whether the left attribute does not sort before the right attribute
		 */
		CIO_BLUETOOTH_API bool operator>=(const Attribute &left, const Attribute &right) noexcept;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

