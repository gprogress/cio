/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Suite.h"

#include "Fault.h"
#include "Reporter.h"

#include <cio/Class.h>
#include <cio/Severity.h>

namespace cio
{
	namespace test
	{
		const Class<Suite> Suite::sMetaclass("cio::test::Suite");

		const Metaclass &Suite::getDeclaredMetaclass()
		{
			return sMetaclass;
		}

		Suite::Suite() noexcept = default;

		Suite::Suite(Text path) :
			Test(std::move(path))
		{
			// nothing more to do
		}

		Suite::Suite(const Suite &in) :
			Test(in)
		{
			this->copyTestCases(in);
		}

		Suite::Suite(Suite &&in) noexcept :
			Test(std::move(in)),
			 mOwnedTestCases(std::move(in.mOwnedTestCases)),
			 mTestCases(std::move(in.mTestCases))
		{
			// nothing more to do
		}

		Suite &Suite::operator=(const Suite &in)
		{
			if (this != &in)
			{
				Test::operator=(in);
				this->copyTestCases(in);
			}

			return *this;
		}

		Suite &Suite::operator=(Suite &&in) noexcept
		{
			if (this != &in)
			{
				Test::operator=(std::move(in));
				mOwnedTestCases = std::move(in.mOwnedTestCases);
				mTestCases = std::move(in.mTestCases);
			}

			return *this;
		}

		Suite::~Suite() noexcept = default;

		void Suite::copyTestCases(const Suite &in)
		{
			mTestCases = in.mTestCases;
			mOwnedTestCases.resize(in.mOwnedTestCases.size());

			for (std::size_t i = 0; i < in.mOwnedTestCases.size(); ++i)
			{
				mOwnedTestCases[i] = cio::clone(*in.mOwnedTestCases[i]);

				for (std::vector<Test *> &sequence : mTestCases)
				{
					for (std::size_t j = 0; j < sequence.size(); ++j)
					{
						if (sequence[j] == in.mOwnedTestCases[i].get())
						{
							sequence[j] = mOwnedTestCases[i].get();
						}
					}
				}
			}
		}

		const Metaclass &Suite::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Suite::clear() noexcept
		{
			mTestCases.clear();
			mOwnedTestCases.clear();
			Test::clear();
		}

		Severity Suite::execute(Reporter &reporter)
		{
			Severity result = Severity::Info;
			reporter.start(*this);
			
			reporter.info() << "Executing test suite of " << mTestCases.size() << " test phases";

			std::size_t i = 0;

			for (std::vector<Test *> &sequence : mTestCases)
			{
				if (!sequence.empty())
				{
					if (result < Severity::Error)
					{
						Severity sequenceProcessed = this->executeTestCaseSetInContext(this, reporter, sequence);
						result = std::max(sequenceProcessed, result);
					}
					else
					{
						for (Test *test : sequence)
						{
							reporter.skip(*test, result);
						}
					}
				}
				
				++i;
			}

			reporter.complete(*this, result);
			return result;
		}

		Severity Suite::executeInContext(Test *context, Reporter &reporter)
		{
			Severity result = Severity::Info;
			Test *nextContext[2] = { context, this };
			reporter.start(*this);

			for (std::vector<Test *> &sequence : mTestCases)
			{
				if (!sequence.empty())
				{
					if (result < Severity::Error)
					{
						Severity sequenceProcessed = this->executeTestCaseSetInFullContext(nextContext, 2, reporter, sequence);
						result = std::max(sequenceProcessed, result);
					}
					else
					{
						for (Test *test : sequence)
						{
							reporter.skip(*test, result);
						}
					}
				}
			}

			reporter.complete(*this, result);
			return result;
		}

		Severity Suite::executeInFullContext(Test **context, std::size_t depth, Reporter &reporter)
		{
			Severity result = Severity::Info;
			reporter.start(*this);
			std::vector<Test *> nextContext(depth + 1);

			if (depth)
			{
				std::copy(context, context + depth, nextContext.begin());
			}

			nextContext.back() = this;

			for (std::vector<Test *> &sequence : mTestCases)
			{
				if (!sequence.empty())
				{
					if (result < Severity::Error)
					{
						Severity sequenceProcessed = this->executeTestCaseSetInFullContext(nextContext.data(), nextContext.size(), reporter, sequence);
						result = std::max(sequenceProcessed, result);
					}
					else
					{
						for (Test *test : sequence)
						{
							reporter.skip(*test, result);
						}
					}
				}
			}

			reporter.complete(*this, result);
			return result;
		}

		Test *Suite::find(const Text &uri) noexcept
		{
			Test *match = Test::find(uri);
			if (!match)
			{
				for (auto &sequence : mTestCases)
				{
					for (auto &test : sequence)
					{
						match = test->find(uri);
						if (match)
						{
							break;
						}
					}

					if (match)
					{
						break;
					}
				}
			}
			return match;
		}

		Test &Suite::addTestCase(Test &&test, unsigned sequenceId)
		{
			std::unique_ptr<Test> moved(cio::moveIntoNewInstance(std::move(test)));
			return this->addTestCase(std::move(moved), sequenceId);
		}

		Test &Suite::addTestCase(std::unique_ptr<Test> test, unsigned sequenceId)
		{
			this->referenceTestCase(test.get(), sequenceId);
			mOwnedTestCases.emplace_back(std::move(test));
			return *mOwnedTestCases.back();
		}

		Test &Suite::referenceTestCase(Test *test, unsigned sequenceId)
		{
			if (sequenceId >= mTestCases.size())
			{
				mTestCases.resize(sequenceId + 1);
			}

			mTestCases[sequenceId].emplace_back(test);
			return *test;
		}

		void Suite::clearTestCases()
		{
			mTestCases.clear();
			mOwnedTestCases.clear();
		}

		std::vector<std::vector<Test *>> &Suite::getTestCases()
		{
			return mTestCases;
		}

		const std::vector<std::vector<Test *>> &Suite::getTestCases() const
		{
			return mTestCases;
		}

		std::vector<Test *> &Suite::getTestCases(unsigned sequenceId)
		{
			return mTestCases.at(sequenceId);
		}

		const std::vector<Test *> &Suite::getTestCases(unsigned sequenceId) const
		{
			return mTestCases.at(sequenceId);
		}

		void Suite::clearTestCases(unsigned sequenceId)
		{
			if (mTestCases.size() > sequenceId)
			{
				mTestCases[sequenceId].clear();
			}

			// Prune empty test sequences
			while (!mTestCases.empty() && mTestCases.back().empty())
			{
				mTestCases.pop_back();
			}
		}

		std::size_t Suite::getTestCaseSequenceCount() const
		{
			return mTestCases.size();
		}

		Severity Suite::setUpTestCaseSequence(Reporter &reporter) noexcept
		{
			Severity setupProcessed = Severity::None;

			try
			{
				setupProcessed = this->setUp();
			}
			catch (const Fault &fault)
			{
				reporter.fail(*this, fault);
				setupProcessed = fault.getSeverity();
			}
			catch (const std::exception &e)
			{
				Fault fault(std::string("Test suite setUp exception ") + e.what(), CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(*this, fault);
				setupProcessed = fault.getSeverity();
			}
			catch (...)
			{
				Fault fault("Test suite setUp unexpected exception ", CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(*this, fault);
				setupProcessed = fault.getSeverity();
			}

			return setupProcessed;
		}

		Severity Suite::executeTestCase(Reporter &reporter, Test &testCase) noexcept
		{
			Severity caseProcessed = Severity::None;

			try
			{
				caseProcessed = testCase.execute(reporter);
			}
			catch (const Fault &fault)
			{
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (const std::exception &e)
			{
				Fault fault(std::string("Test case exception ") + e.what(), CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (...)
			{
				Fault fault("Unexpected test case exception", CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}

			return caseProcessed;
		}

		Severity Suite::executeTestCaseSet(Reporter &reporter, const std::vector<Test *> &testCases) noexcept
		{
			Severity setupProcessed = this->setUpTestCaseSequence(reporter);
			Severity caseSetProcessed = Severity::None;

			if (setupProcessed < Severity::Error)
			{
				for (Test *testCase : testCases)
				{
					Severity caseProcessed = this->executeTestCase(reporter, *testCase);
					caseSetProcessed = std::max(caseProcessed, caseSetProcessed);
				}
			}
			else
			{
				for (Test *testCase : testCases)
				{
					reporter.skip(*testCase, setupProcessed);
				}
			}

			this->tearDown();
			return std::max(setupProcessed, caseSetProcessed);
		}

		Severity Suite::executeTestCaseInContext(Test *context, Reporter &reporter, Test &testCase) noexcept
		{
			Severity caseProcessed = Severity::None;

			try
			{
				caseProcessed = testCase.executeInContext(context, reporter);
			}
			catch (const Fault &fault)
			{
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (const std::exception &e)
			{
				Fault fault(std::string("Test case exception ") + e.what(), CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (...)
			{
				Fault fault("Unexpected test case exception", CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}

			return caseProcessed;
		}

		Severity Suite::executeTestCaseSetInContext(Test *context, Reporter &reporter, const std::vector<Test *> &testCases) noexcept
		{
			Severity setupProcessed = this->setUpTestCaseSequence(reporter);
			Severity caseSetProcessed = Severity::None;

			if (setupProcessed < Severity::Error)
			{
				for (Test *testCase : testCases)
				{
					Severity caseProcessed = this->executeTestCaseInContext(context, reporter, *testCase);
					caseSetProcessed = std::max(caseProcessed, caseSetProcessed);
				}
			}
			else
			{
				for (Test *testCase : testCases)
				{
					reporter.skip(*testCase, setupProcessed);
				}
			}

			this->tearDown();
			return std::max(setupProcessed, caseSetProcessed);
		}

		Severity Suite::executeTestCaseInFullContext(Test **context, std::size_t depth, Reporter &reporter, Test &testCase) noexcept
		{
			Severity caseProcessed = Severity::None;

			try
			{
				caseProcessed = testCase.executeInFullContext(context, depth, reporter);
			}
			catch (const Fault &fault)
			{
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (const std::exception &e)
			{
				Fault fault(std::string("Test case exception ") + e.what(), CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}
			catch (...)
			{
				Fault fault("Unexpected test case exception", CIO_CURRENT_SOURCE(), Severity::Critical);
				reporter.fail(testCase, fault);
				caseProcessed = fault.getSeverity();
			}

			return caseProcessed;
		}

		Severity Suite::executeTestCaseSetInFullContext(Test **context, std::size_t depth, Reporter &reporter, const std::vector<Test *> &testCases) noexcept
		{
			Severity setupProcessed = this->setUpTestCaseSequence(reporter);
			Severity caseSetProcessed = Severity::None;

			if (setupProcessed < Severity::Error)
			{
				for (Test *testCase : testCases)
				{
					Severity caseProcessed = this->executeTestCaseInFullContext(context, depth, reporter, *testCase);
					caseSetProcessed = std::max(caseProcessed, caseSetProcessed);
				}
			}
			else
			{
				for (Test *testCase : testCases)
				{
					reporter.skip(*testCase, setupProcessed);
				}
			}

			this->tearDown();
			return std::max(setupProcessed, caseSetProcessed);
		}
	}
}
