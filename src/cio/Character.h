/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CHARACTER_H
#define CIO_CHARACTER_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * Represents a single Unicode character independent of text encoding.
	 */
	class CIO_API Character
	{
		public:
			/**
			 * Parses the Character from the given UTF-8 value.
			 * The parse will fail if the value is not an ASCII character due to being an incomplete UTF-8 sequence.
			 * If successful, the parse length is always 1.
			 *
			 * @param c The UTF-8 value
			 * @return The decoded Character
			 */
			static Parse<Character, char> decode(char c) noexcept;

			/**
			 * Parses the Character from the given UTF-16 value.
			 * The parse will fail if the UTF-16 is an incomplete surrogate pair.
			 * If successful, the parse length is always 1.
			 *
			 * @param c The UTF-16 value
			 * @return The decoded character
			 */
			static Parse<Character, char16_t> decode(char16_t c) noexcept;

			/**
			 * Parses the Character from the given Unicode codepoint.
			 * The parse will fail if the codepoint is out of range.
			 * If successful, the parse length is always 1.
			 *
			 * @param c The codepoint
			 * @return The character
			 */
			static Parse<Character, char32_t> decode(char32_t c) noexcept;

			/**
			 * Parses the next Character from the given UTF-8 sequence.
			 * The parse will fail if the UTF-8 sequence ends before a character is complete
			 * or if the determined codepoint is out of range.
			 *
			 * If successful, the parse length is between 1 and 4.
			 *
			 * @param text The UTF-8 text sequence
			 * @param limit The maximum number of UTF-8 values to consider
			 * @return The character
			 */
			static Parse<Character, char> decode(const char *text, std::size_t limit = SIZE_MAX) noexcept;

			/**
			 * Parses the next code point from the given UTF-16 sequence.
			 * The parse will fail if the next UTF-16 value is part of a surrogate pair but the second pair item isn't present
			 * or if the determined codepoint is out of range.
			 *
			 * If successful, the parse length is between 1 and 2.
			 *
			 * @param text The UTF-16 text sequence
			 * @param limit The maximum number of UTF-16 values to consider
			 * @return The character
			 */
			static Parse<Character, char16_t> decode(const char16_t *text, std::size_t limit = SIZE_MAX) noexcept;

			/**
			 * Parses the next code point from the given Unicode character sequence.
			 * The parse will fail if the next Unicode codepoint is out of range.
			 * If successful, the parse length is always 1.
			 *
			 * @param text The character text
			 * @param limit The maximum number of Unicode characters to consider
			 * @return The codepoint
			 */
			static Parse<Character, char32_t> decode(const char32_t *text, std::size_t limit = SIZE_MAX) noexcept;

			/**
			 * Prints the given Unicode text string to the given UTF-8 text buffer.
			 *
			 * @param input The Unicode text string
			 * @param count The number of Unicode codepoints (not bytes) to print
			 * @param output The text buffer to print to
			 * @param capacity The length of the text buffer
			 * @return The total number of UTF-8 bytes that would be needed to print the text
			 */
			static std::size_t transcode(const char32_t *input, std::size_t count, char *output, std::size_t capacity) noexcept;

			/**
			 * Prints the given UTF-16 text string to the given UTF-8 text buffer.
			 * If the last UTF-16 character is an unterminated surrogate pair, it will not be printed.
			 *
			 * @param input The UTF-16 text string
			 * @param count The number of UTF-16 characters (not bytes) to print
			 * @param output The text buffer to priint to
			 * @param capacity The length of the text buffer
			 * @return The total number of UTF-8 bytes that would be needed to print the text
			 */
			static std::size_t transcode(const char16_t *input, std::size_t count, char *output, std::size_t capacity) noexcept;
			
			/**
			 * Construct the null character.
			 */
			inline Character() noexcept
			{
				// nothing more to do
			}

			/**
			 * Construct the given character from a UTF-8 byte.
			 * This will fail and set the character to null if the UTF-8 sequence is incomplete.
			 *
			 * @param c The character
			 */
			Character(char c) noexcept;

			/**
			 * Construct the given character from a UTF-16 value.
			 * This will fail and set the character to null if the UTF-16 sequence is incomplete.
			 *
			 * @param c The character
			 */
			Character(char16_t c) noexcept;

			/**
			 * Construct the given character from its UCS-4 representation.
			 * 
			 * @param c The character
			 */
			inline Character(char32_t c) :
				mCodepoint(c)
			{
				// nothing more to do
			}

			/**
			 * Decode the first character from a UTF-8 buffer up to the null terminator or the size if known.
			 * If the UTF-8 sequence is incomplete or invalid, the null character is constructed.
			 * 
			 * @param buffer The UTF-8 buffer
			 * @param capacity The length of the UTF-8 buffer if known
			 */
			explicit Character(const char *buffer, std::size_t capacity = SIZE_MAX);

			/**
			 * Decode the first character from a UTF-16 buffer up to the null terminator or the size if known.
			 * If the UTF-16 sequence is incomplete or invalid, the null character is constructed.
			 *
			 * @param buffer The UTF-16 buffer
			 * @param capacity The length of the UTF-16 buffer if known
			 */
			explicit Character(const char16_t *buffer, std::size_t capacity = SIZE_MAX);

			/**
			 * Decode the first character from a UCS-4 buffer up to the null terminator or the size if known.
			 * If the UCS-4 value invalid, the null character is constructed.
			 *
			 * @param buffer The UCS-4 buffer
			 * @param capacity The length of the UCS-4 buffer if known
			 */
			explicit Character(const char32_t *buffer, std::size_t capacity = SIZE_MAX);

			/**
			 * Copy constructor.
			 * 
			 * @param in The character to copy
			 */
			inline Character(const Character &in) noexcept = default;

			/**
			 * Copy assignment.
			 * 
			 * @param in The character to copy
			 * @return This character
			 */
			inline Character &operator=(const Character &in) noexcept = default;

			/**
			 * Destructor.
			 */
			inline ~Character() noexcept = default;

			/**
			 * Resets the character to null.
			 */
			inline void clear() noexcept
			{
				mCodepoint = 0;
			}

			/**
			 * Checks whether the given character is an ASCII character.
			 * Only ASCII characters may be safely passed to C/C++ standard functions.
			 *
			 * @return Whether the character is ASCII
			 */
			bool isAscii() const noexcept;

			/**
			 * Checks whether the given character represents a letter in an alphabetic script.
			 * The closest ASCII equivalent is std::isalpha().
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is a word component
			 */
			bool isAlphabetic() const noexcept;

			/**
			 * Checks whether the given character represents a pronounceable phoneme such as a letter or syllable.
			 * The closest ASCII equivalent is std::isalpha().
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is a word component
			 */
			bool isPhonetic() const noexcept;

			/**
			 * Checks whether the given character is a describable concept.
			 * This is true for letters, syllables, ideographs, digits, and certain types of math symbols.
			 * It excludes whitespace and most types of punctuation.
			 * The closest ASCII equivalent is std::isalnum().
			 * 
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 * 
			 * @return Whether the character is uniquely pronounceable
			 */
			bool isConceptual() const noexcept;

			/**
			 * Checks whether the given character is punctuation.
			 * The ASCII equivalent is std::ispunct().
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is uniquely pronounceable
			 */
			bool isPunctuation() const noexcept;

			/**
			 * Checks whether the character is a whitespace character.
			 * This only considers actual whitespace characters, not substitute characters to display them or
			 * other word boundary characters not considered space.
			 * The ASCII equivalent is std::isspace().
			 *
			 * @return Whether the character is whitespace
			 */
			bool isWhitespace() const noexcept;

			/**
			 * Checks whether the given character is a visible glyph (or grapheme).
			 * This does not include whitespace.
			 * The ASCII equivalent is std::isgraph().
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is a visible glyph
			 */
			bool isGlyph() const noexcept;

			/**
			 * Checks whether the given character is visible when printing.
			 * This includes glyphs and whitespace.
			 * The ASCII equivalent is std::isprint().
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is visible when printing
			 */
			bool isVisible() const noexcept;

			/**
			 * Checks whether the given character is considered a digit.
			 * Currently only ASCII '0' through '9' are considered.
			 * The ASCII equivalent is std::isdigit().
			 * 
			 * @return Whether the character is a digit
			 */
			bool isDigit() const noexcept;

			/**
			 * Checks whether the given character is considered to be uppercase.
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is uppercase
			 */
			bool isUppercase() const noexcept;

			/**
			 * Checks whether the given character is considered to be lowercase.
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return Whether the character is lowercase
			 */
			bool isLowercase() const noexcept;

			/**
			 * Converts the character to its uppercase representation if it has one.
			 * The character is returned unmodified if it does not have case.
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return The uppercase character
			 */
			Character toUppercase() const noexcept;

			/**
			 * Converts the character to its lowercase representation if it has one.
			 * The character is returned unmodified if it does not have case.
			 *
			 * @note This is a work in progress and may not correctly handle all possible inputs outside of ASCII range.
			 *
			 * @return The lowercase character
			 */
			Character toLowercase() const noexcept;

			/**
			 * Gets the codepoint for the character.
			 * 
			 * @return The character codepoint
			 */
			inline char32_t codepoint() const noexcept
			{
				return mCodepoint;
			}

			/**
			 * Gets the number of UTF-8 characters needed to print this Unicode character.
			 * This can be from 0 to 4.
			 *
			 * @return The number of UTF-8 characters needed to print the Unicode codepoint
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints this Unicode character to a returned UTF-8 text buffer.
			 * 
			 * @return The printed character
			 */
			cio::FixedText<4> print() const noexcept;

			/**
			 * Prints this Unicode character to the given UTF-8 text buffer, returning how many UTF-8 characters would have been needed.
			 * An offset to UTF-8 output may be supplied to finish printing a character previously partially printed.
			 *
			 * @param buffer The buffer to print to
			 * @param capacity The number of bytes available in the buffer
			 * @param offset The byte offset of the UTF-8 sequence to start printing at
			 * @return The number of UTF-8 bytes that would be printed if the buffer is large enough
			 */
			std::size_t print(char *buffer, std::size_t capacity, std::size_t offset = 0) const noexcept;

			/**
			 * Inteprets the character as a Boolean.
			 * This is true if it is not the null character.
			 * 
			 * @return Whether the character is non-null
			 */
			inline explicit operator bool() const noexcept
			{
				return mCodepoint != 0;
			}

		private:
			/** The character's codepoint value */
			char32_t mCodepoint = 0;
	};

	/**
	 * Gets the number of data elements in the buffer until the NUL (0) terminator is found.
	 * For UTF-8 strings (const char *) this is also the same as the print length and number of bytes.
	 * For other string types, this gets the number of input elements without transcoding to UTF-8.
	 * 
	 * While intended for characters, this works with any element type that has a conversion to bool.
	 * 
	 * @tparam C The character type
	 * @param buffer The buffer
	 * @param limit The maximum number of values to consider
	 * @return The offset of the first null terminator
	 */
	template <typename C>
	inline std::size_t terminator(const C *buffer, std::size_t limit = SIZE_MAX) noexcept
	{
		std::size_t offset = 0;
		if (buffer)
		{
			while (offset < limit && buffer[offset])
			{
				++offset;
			}
		}
		return offset;
	}

	/**
	 * Check to see whether the given text data buffer is null-terminated within an expected number of characters.
	 *
	 * @param text The text data buffer
	 * @param length The maximum number of characters to consider in the buffer
	 * @return whether there was a null terminator in the text buffer
	 */
	template <typename C>
	inline bool terminated(const C *text, std::size_t length) noexcept
	{
		std::size_t i = 0;
		while (i < length && text[i])
		{
			++i;
		}
		return i < length;
	}

	/**
	 * Gets the number of UTF-8 characters needed to print the given UTF-8 character.
	 * This is always 1 if the character is ASCII, or 0 if it is NUL (0) or an incomplete character.
	 *
	 * @param value The UTF-8 character
	 * @return The number of UTF-8 characters needed to print the Unicode codepoint
	 */
	CIO_API std::size_t strlen(char value) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given UTF-16 character.
	 * This can be from 1 to 4, or 0 if the UTF-16 is NUL (0) or part of a surrogate pair so we can't complete it.
	 *
	 * @param value The UTF-16 character
	 * @return The number of UTF-8 characters needed to print the Unicode codepoint
	 */
	CIO_API std::size_t strlen(char16_t value) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given Unicode character.
	 * This can be from 1 to 4, or 0 if the character is NUL (0) or out of range.
	 *
	 * @param value The Unicode character
	 * @return The number of UTF-8 characters needed to print the Unicode codepoint
	 */
	CIO_API std::size_t strlen(char32_t value) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given UTF-8 character sequence.
	 * This is equivalent to std::strlen except it also considers the limit and will stop at invalid 
	 * UTF-8 sequences.
	 *
	 * @param value The UTF-8 character buffer
	 * @param limit The maximum number of UTF-8 values to consider
	 * @return The number of UTF-8 characters needed to print the UTF-8 text
	 */
	CIO_API std::size_t strlen(const char *value, std::size_t limit = SIZE_MAX) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given UTF-16 character sequence.
	 * This stops at the first null terminator, the given limit, or if an invalid UTF-16 sequence is found.
	 *
	 * @param value The UTF-16 character buffer
	 * @param limit The maximum number of UTF-16 values to consider
	 * @return The number of UTF-8 characters needed to print the UTF-16 text
	 */
	CIO_API std::size_t strlen(const char16_t *value, std::size_t limit = SIZE_MAX) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given UCS-4 character sequence.
	 * This stops at the first null terminator, the given limit, or if an invalid UCS-4 character is found.
	 *
	 * @param value The UCS-4 character buffer
	 * @param limit The maximum number of UCS-4 values to consider
	 * @return The number of UTF-8 characters needed to print the UCS-4 text
	 */
	CIO_API std::size_t strlen(const char32_t *value, std::size_t limit = SIZE_MAX) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given UTF-16 character.
	 * This can be from 1 to 4, or 0 if the UTF-16 is NUL (0) or part of a surrogate pair so we can't complete it.
	 *
	 * @param value The UTF-16 character
	 * @return The number of UTF-8 characters needed to print the Unicode codepoint
	 */
	CIO_API std::size_t strlen(char16_t value) noexcept;

	/**
	 * Gets the number of UTF-8 characters needed to print the given Unicode character.
	 * This can be from 1 to 4, or 0 if the character is NUL (0) or out of range.
	 *
	 * @param value The Unicode character
	 * @return The number of UTF-8 characters needed to print the Unicode codepoint
	 */
	CIO_API std::size_t strlen(char32_t value) noexcept;

	/**
	 * Prints the specified character value to a buffer with a maximum capacity.
	 * This will print all characters (including incomplete UTF-8 sequences) except the NUL (0) character.
	 *
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @param capacity The maximum number of characters in the buffer to print
	 * @return The actual number of characters that is needed to print the value, which is always 0 or 1
	 */
	CIO_API std::size_t print(char value, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the given UTF-16 to the given UTF-8 text buffer, returning how many UTF-8 characters would have been needed.
	 * An offset to UTF-8 output may be supplied to finish printing a character previously partially printed.
	 * Nothing is printed if the UTF-16 character is part of a surrogate pair and therefore is incomplete.
	 *
	 * @param value The Unicode codepoint to print
	 * @param buffer The buffer to print to
	 * @param capacity The number of bytes available in the buffer
	 * @param offset The byte offset of the UTF-8 sequence to start printing at
	 * @return The number of UTF-8 bytes that would be printed if the buffer is large enough
	 */
	CIO_API std::size_t print(char16_t value, char *buffer, std::size_t capacity, std::size_t offset = 0) noexcept;

	/**
	 * Prints the given Unicode codepoint to the given UTF-8 text buffer, returning how many UTF-8 characters would have been needed.
	 * An offset to UTF-8 output may be supplied to finish printing a character previously partially printed.
	 *
	 * @param value The Unicode codepoint to print
	 * @param buffer The buffer to print to
	 * @param capacity The number of bytes available in the buffer
	 * @param offset The byte offset of the UTF-8 sequence to start printing at
	 * @return The number of UTF-8 bytes that would be printed if the buffer is large enough
	 */
	CIO_API std::size_t print(char32_t value, char *buffer, std::size_t capacity, std::size_t offset = 0) noexcept;

	/**
	 * Checks to see if two characters are equal.
	 * This is true if the have the same codepoint.
	 * 
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the characters are equal
	 */
	inline bool operator==(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() == right.codepoint();
	}

	/**
	 * Checks to see if two characters are not equal.
	 * This is true if the have different codepoints.
	 *
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the characters are not equal
	 */
	inline bool operator!=(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() != right.codepoint();
	}

	/**
	 * Checks to see if one character is before another.
	 * This orders the characters by codepoint, not necessarily by any language-based sorting.
	 *
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the first character is before the second
	 */
	inline bool operator<(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() < right.codepoint();
	}

	/**
	 * Checks to see if one character is before or equal to another.
	 * This orders the characters by codepoint, not necessarily by any language-based sorting.
	 *
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the first character is before or equal to the second
	 */
	inline bool operator<=(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() <= right.codepoint();
	}

	/**
	 * Checks to see if one character is after another.
	 * This orders the characters by codepoint, not necessarily by any language-based sorting.
	 *
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the first character is after the second
	 */
	inline bool operator>(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() > right.codepoint();
	}

	/**
	 * Checks to see if one character is after or equal to another.
	 * This orders the characters by codepoint, not necessarily by any language-based sorting.
	 *
	 * @param left The first character
	 * @param right The second character
	 * @return Whether the first character is after or equal to the second
	 */
	inline bool operator>=(const Character &left, const Character &right) noexcept
	{
		return left.codepoint() >= right.codepoint();
	}

	/**
	 * Prints a character to a C++ stream by encoding it as UTF-8.
	 * 
	 * @param s The stream
	 * @param c The character
	 * @return The stream
	 * @throw std::exception If stream printing threw an exception
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, const Character &c);
}

#endif
