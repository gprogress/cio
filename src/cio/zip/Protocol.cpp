/************************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Protocol.h"

#include "Archive.h"

#include <cio/ProtocolFactory.h>
#include <cio/Metadata.h>

#include <cio/Class.h>
#include <cio/Path.h>
#include <cio/ProtocolFilter.h>
#include <cio/ProtocolRegistrar.h>

namespace cio
{
	namespace zip
	{
		Class<Protocol> Protocol::sMetaclass("cio::zip::Protocol");

		/* In Static.cpp
		const Text Protocol::sJarPrefix("jar");

		const Text Protocol::sArchiveExtensions("zip;jar");

		ProtocolFilter Protocol::sDefaultFilter[2] = {
			{ &sMetaclass, sJarPrefix, sWildcard, Resource::Unknown },
			{ &sMetaclass, sWildcard, sArchiveExtensions, Resource::Directory }
		};
	
		cio::ProtocolRegistrar<cio::file::Protocol> sZipProtocolRegistrar;
		*/
		const Metaclass &Protocol::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		const Metaclass &Protocol::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		Text Protocol::getClassProtocolPrefix() noexcept
		{
			return sJarPrefix;
		}

		Chunk<ProtocolFilter> Protocol::getClassProtocolFilter() noexcept
		{
			return Chunk<ProtocolFilter>(sDefaultFilter, 2);
		}

		Protocol::Protocol() noexcept :
			cio::Protocol(sJarPrefix, getClassProtocolFilter())
		{
			// nothing more to do
		}

		Protocol::~Protocol() noexcept = default;

		void Protocol::clear() noexcept
		{
			cio::Protocol::clear();

			mDefaultPrefix = sJarPrefix;
			mFilters.bind(sDefaultFilter, 2);
		}

		Metadata Protocol::readMetadata(const Path &input) const
		{
			Metadata read;
			cio::ProtocolFactory *factory = cio::ProtocolFactory::getDefault();

			std::pair<Path, Path> archiveEntry = this->splitArchiveEntry(input);
			if (!archiveEntry.first.empty())
			{
				Archive archive;

				// use Protocol Factory to obtain underlying file input
				std::shared_ptr<Input> zipInput = factory->openInput(archiveEntry.first);
				archive.openZipInput(zipInput);
				read = archive.readMetadata(archiveEntry.second);
			}

			return read;
		}

		std::unique_ptr<cio::Protocol> Protocol::openDirectory(const cio::Path &path, cio::ModeSet modes, cio::Traversal createMissing)
		{
			std::unique_ptr<Archive> archive;
			cio::ProtocolFactory *factory = cio::ProtocolFactory::getDefault();

			std::pair<Path, Path> archiveEntry = this->splitArchiveEntry(path);
			if (!archiveEntry.first.empty())
			{ 
				// use Protocol Factory to obtain underlying file input
				std::shared_ptr<Input> zipInput = factory->openInput(archiveEntry.first, modes);

				archive.reset(new Archive());
				archive->openZipInput(zipInput);

				if (!archiveEntry.second.empty())
				{
					// TODO we need a ArchiveDirectory implementation to make this work
				}
			}

			return archive;
		}

		std::unique_ptr<cio::Input> Protocol::openInput(const cio::Path &path, cio::ModeSet modes) const
		{
			std::unique_ptr<cio::Input> input;
			cio::ProtocolFactory *factory = cio::ProtocolFactory::getDefault();

			std::pair<Path, Path> archiveEntry = this->splitArchiveEntry(path);
			if (!archiveEntry.first.empty() && !archiveEntry.second.empty())
			{
				// use Protocol Factory to obtain underlying file input
				std::shared_ptr<Input> zipInput = factory->openInput(archiveEntry.first, modes);

				Archive archive;
				archive.openZipInput(zipInput);
				input = archive.openInput(archiveEntry.second, modes);
			}

			return input;
		}

		std::pair<Path, Path> Protocol::splitArchiveEntry(const Path &path) 
		{
			Path archive;
			Path entry;

			Text referenced;
			Text protocol = path.getProtocol();

			// Determine what part of the URI is responsible for the archive
			if (protocol == sJarPrefix)
			{
				std::size_t offset = protocol.size() + 1;
				referenced.bind(path.c_str() + offset, path.size() - offset);
				referenced = referenced.trimPrefix('/').trimPrefix('/');
			}
			else
			{
				referenced.bind(path.c_str(), path.size());
			}

			// See if we have a split point for '!'
			std::pair<Text, Text> split = referenced.split('!');
			return std::pair<Path, Path>(split.first, split.second.trimPrefix('/').trimSuffix('/'));
		}
	}
}
