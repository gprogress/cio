/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DBusController.h"

#include "AddressType.h"
#include "Peripheral.h"

#include <cio/DeviceAddress.h>
#include <cio/Exception.h>
#include <cio/Listener.h>
#include <cio/Class.h>
#include <cio/Parse.h>
#include <cio/Print.h>
#include <cio/State.h>
#include <cio/Variant.h>

#include <dbus/dbus.h>

#if defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#endif

#if defined CIO_HAVE_GRP_H
#include <grp.h>
#endif

#if defined CIO_HAVE_PWD_H
#include <pwd.h>
#endif

#include <thread>

namespace cio
{
	namespace bluetooth
	{
		Class<DBusController> DBusController::sMetaclass("cio::bluetooth::DBusController");
		
		const char *DBusController::BLUEZ_DOMAIN = "org.bluez";
		
		const char *DBusController::BLUEZ_ADAPTER_IFACE = "org.bluez.Adapter1";
		
		const char *DBusController::BLUEZ_PERIPHERAL_IFACE = "org.bluez.Device1";
		
		const Metaclass &DBusController::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
		DBusController::DBusController() noexcept
		{
			// nothing more to do
		}
			
		DBusController::DBusController(const Device &in) :
			Controller(in)
		{
			mSystemBus.open(BLUEZ_DOMAIN);
			
			if (in)
			{
				char printed[64];
				this->printAdapterPath(in, printed, 64);
				mAdapterPath = printed;
			}
		}	
		
		DBusController::DBusController(DBusController &&in) noexcept :
			Controller(std::move(in)),
			mSystemBus(std::move(in.mSystemBus)),
			mAdapterPath(std::move(in.mAdapterPath)),
			mKnownDevices(std::move(in.mKnownDevices)),
			mKnownPeripherals(std::move(in.mKnownPeripherals))
		{
			// nothing more to do
		}
			
		DBusController &DBusController::operator=(DBusController &&in) noexcept
		{
			if (this != &in)
			{
				Controller::operator=(std::move(in));
				
				mSystemBus = std::move(in.mSystemBus);
				mAdapterPath = std::move(in.mAdapterPath);
				mKnownDevices = std::move(in.mKnownDevices);
				mKnownPeripherals = std::move(in.mKnownPeripherals);
			}
			
			return *this;
		}
			
		DBusController::~DBusController() noexcept = default;
		
		const Metaclass &DBusController::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void DBusController::clear() noexcept
		{
			Controller::clear();
			
			mSystemBus.clear();
			mAdapterPath.clear();
			mKnownDevices.clear();
			mKnownPeripherals.clear();
		}
		
		Exception DBusController::validate()
		{
			Exception result;
			
#if defined CIO_HAVE_UNISTD_H && defined CIO_HAVE_GRP_H && defined CIO_HAVE_PWD_H
			
			__uid_t uid = getuid();//you can change this to be the uid that you want

			struct passwd *pw = getpwuid(uid);

			int ngroups = 256;
			__gid_t groups[256];
			getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);
			
			bool foundBluetooth = false;
			
			for (int i = 0; i < ngroups; ++i)
			{
				struct group* gr = getgrgid(groups[i]);
				if (std::strcmp("bluetooth", gr->gr_name) == 0)
				{
					foundBluetooth = true;
					break;
				}
			}
			
			if (foundBluetooth)
			{
				result.succeed();
			}
			else
			{
				result.fail(Reason::Unauthorized, "Current user is not member of 'bluetooth' group");
			}
			
#endif
			
			return result;
		}
			
		void DBusController::openCurrentDevice()
		{
			mSystemBus.open(BLUEZ_DOMAIN);
			
			mAdapterPath.clear();
		
			Device dev = this->getDevice();
			
			if (dev)
			{
				char printed[64];
				this->printAdapterPath(dev, printed, 64);
				mAdapterPath = printed;
			}
		}
					
		bool DBusController::isOpen() const noexcept
		{
			return mSystemBus.isOpen();
		}			
						
		void DBusController::close() noexcept
		{
			Controller::close();
			
			mAdapterPath.clear();
			mSystemBus.clear();
			mKnownDevices.clear();
			mKnownPeripherals.clear();
		}
		
		DBusClient &DBusController::getSystemBus() noexcept
		{
			return mSystemBus;
		}
		
		const DBusClient &DBusController::getSystemBus() const noexcept
		{
			return mSystemBus;
		}
				
		DBusClient &DBusController::getOrCreateSystemBus()
		{
			mSystemBus.open(BLUEZ_DOMAIN);
			return mSystemBus;
		}	
		
		void DBusController::setSystemBus(const DBusClient &client) noexcept
		{
			mSystemBus = client;
		}		
				
		Device DBusController::findDefaultDevice()
		{
			Device device;
			this->synchronizeDeviceCache();
			if (!mKnownDevices.empty())
			{
				device = mKnownDevices.begin()->second;
			}
			return device;
		}
		
		std::vector<Device> DBusController::discoverLocalDevices()
		{
			std::vector<Device> devices;
			this->synchronizeDeviceCache();
			
			devices.reserve(mKnownDevices.size());
			for (const std::pair<Path, Device> &item : mKnownDevices)
			{
				devices.emplace_back(item.second);
			}
			
			return devices;
		}
				
		std::size_t DBusController::readLocalName(char *text, std::size_t length)
		{
			Variant value = this->getAdapterProperty("Name");
			return value.print(text, length);
		}
		
		DeviceAddress DBusController::readLocalAddress()
		{
			DeviceAddress address;
			Variant value = this->getAdapterProperty("Address");
			address.parse(value.c_str());
			return address;
		}

		State DBusController::startLowEnergyScan()
		{
			State status;
			
			if (!mAdapterPath.empty())
			{
				status = this->invokeAdapterCommand("StartDiscovery");
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}
				
		Progress<std::size_t> DBusController::listenForAdvertisements(AdvertisementListener &listener) noexcept
		{
			Progress<std::size_t> result;
			
			if (!mAdapterPath.empty())
			{
				// Let discovery results accumulate
				Progress<std::chrono::milliseconds> timeout = listener.poll();
				
				std::this_thread::sleep_for(timeout.count);
				
				try
				{
					this->synchronizeDeviceCache();
						
					for (const Peripheral &p : mKnownPeripherals)
					{
						listener.post(p);
						++result.count;
					}
						
					result.succeed();
				}
				catch (...)
				{
					result.fail(Reason::Unknown);
				}
			}
			else
			{
				result.fail(Reason::Unopened);
			}

			return result;
		}
				
		State DBusController::stopLowEnergyScan()
		{
			State status;
			
			if (!mAdapterPath.empty())
			{
				bool scanning = mSystemBus.getProperty(mAdapterPath.c_str(), BLUEZ_ADAPTER_IFACE, "Discovering").getBoolean();
			
				if (scanning)
				{
					status = this->invokeAdapterCommand("StopDiscovery");
				}
				else
				{
					status.complete(Reason::Missing);
				}
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}
	
		State DBusController::startConnection(const Peripheral &peripheral, Task &exec)
		{
			State status;
			
			if (!mAdapterPath.empty() && peripheral)
			{
				this->synchronizeDeviceCache();
				
				for (const Peripheral &candidate : mKnownPeripherals)
				{
					if (candidate.getAddress() == peripheral.getAddress())
					{
						char peripheralObject[64];
						this->printPeripheralPath(peripheral.getAddress(), peripheralObject, 64);
						
						// Check to see if we're already connected
						bool connected = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Connected").getBoolean();
						if (connected)
						{
							status.complete(Reason::Connected);
						}
						else
						{
						
							status = mSystemBus.invokeCommand(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Connect");
							
							if (status.succeeded())
							{																
								// connection may not be established immediately, loop until it does?
								std::chrono::milliseconds timeout(100);
								std::size_t attempts = 20;

								do
								{
									std::this_thread::sleep_for(timeout);
									connected = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Connected").getBoolean();
								}
								while (!connected && --attempts > 0);
								
								if (!connected)
								{
									sMetaclass.warning() << "Peripheral " << peripheral.getAddress() << " connection timed out";
									status.fail(Reason::Timeout);
								}
							}
						}
						
						break;
					}
				}
				
				if (!status.completed())
				{
					status.fail(Reason::Missing);
				}
			}
			else
			{
				status.fail(Reason::Unopened);	
			}
		
			return status;
		}
				
		State DBusController::startDisconnection(const Peripheral &peripheral, Task &exec)
		{
			State status;
			
			if (!mAdapterPath.empty() && peripheral)
			{
				this->synchronizeDeviceCache();
				
				for (const Peripheral &candidate : mKnownPeripherals)
				{
					if (candidate.getAddress() == peripheral.getAddress())
					{
						char peripheralObject[64];
						this->printPeripheralPath(peripheral.getAddress(), peripheralObject, 64);
						
						bool connected = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Connected").getBoolean();
						if (connected)
						{
							status = mSystemBus.invokeCommand(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Disconnect");
						}
						else
						{
							status.complete(Reason::Disconnected);
							sMetaclass.info() << "Can't disconnect from " << peripheral.getAddress() << " since it was not connected";
						}
						break;
					}
				}
				
				if (!status.completed())
				{
					status.fail(Reason::Missing);
				}
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}
		
		State DBusController::startPairing(const Peripheral &peripheral, Task &exec)
		{
			State status;
			if (!mAdapterPath.empty() && peripheral)
			{
				this->synchronizeDeviceCache();
				
				for (const Peripheral &candidate : mKnownPeripherals)
				{
					if (candidate.getAddress() == peripheral.getAddress())
					{
						char peripheralObject[64];
						this->printPeripheralPath(peripheral.getAddress(), peripheralObject, 64);
					
						bool paired = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Paired").getBoolean();
						if (paired)
						{
							status.complete(Reason::Connected);
						}
						else
						{
							status = mSystemBus.invokeCommand(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Pair");
							
							if (status.started())
							{
								// pairing may not be established immediately, loop until it does?
								std::chrono::milliseconds timeout(100);
								std::size_t attempts = 20;
			
								do
								{
									std::this_thread::sleep_for(timeout);
									paired = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Paired").getBoolean();
								}
								while (!paired && --attempts > 0);
								
								if (!paired)
								{
									status.fail(Reason::Timeout);
									sMetaclass.warning() << "Peripheral " << peripheral.getAddress() << " pairing timed out";
								}
							}
						}
						
						break;
					}
				}
				
				if (!status.completed())
				{
					status.fail(Reason::Missing);
				}
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}
		
		State DBusController::startUnpairing(const Peripheral &peripheral, Task &exec) 
		{
			State status;
			if (!mAdapterPath.empty() && peripheral)
			{
				this->synchronizeDeviceCache();
				
				for (const Peripheral &candidate : mKnownPeripherals)
				{
					if (candidate.getAddress() == peripheral.getAddress())
					{
						char peripheralObject[64];
						this->printPeripheralPath(peripheral.getAddress(), peripheralObject, 64);
						
						bool paired = mSystemBus.getProperty(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "Paired").getBoolean();
						if (paired)
						{
							status = mSystemBus.invokeCommand(peripheralObject, BLUEZ_PERIPHERAL_IFACE, "CancelPairing");
						}
						else
						{
							status.complete(Reason::Disconnected);
							sMetaclass.info() << "Can't unpair from " << peripheral.getAddress() << " since it was not connected";
						}
						
						break;
					}
				}	
				
				if (!status.completed())
				{
					status.fail(Reason::Missing);
				}	
			}
			else
			{
				status.fail(Reason::Unopened);
			}
			
			return status;
		}	
		
		std::size_t DBusController::printAdapterPath(const Device &device, char *text, std::size_t length) const noexcept
		{
			char printed[64] = "/org/bluez/hci";
			std::size_t actual = 14 + cio::print(device.getIndex(), printed + 14, 40);
			std::size_t toCopy = std::min(actual, length);
			
			if (toCopy > 0)
			{
				std::memcpy(text, printed, toCopy);
			}
			
			if (toCopy < length)
			{
				std::memset(text + toCopy, 0, length - toCopy);
			}
			
			return actual;
		}
		
		// DBus adapter helpers
		
		void DBusController::populateDeviceCache()
		{
			if (mSystemBus)
			{
				auto receiver = [&](const char *object, const char *iface)
				{
					try
					{
						if (std::strcmp(iface, BLUEZ_ADAPTER_IFACE) == 0)
						{
							this->updateDeviceInfo(object);
						}
						else if (std::strcmp(iface, BLUEZ_PERIPHERAL_IFACE) == 0)
						{
							this->updatePeripheralInfo(object);
						}
					}
					catch (...)
					{
						// BlueZ tends to fail on property get a lot more than you'd expect
					}
				};
				
				const char *ifaces[] = { BLUEZ_ADAPTER_IFACE, BLUEZ_PERIPHERAL_IFACE };
				mSystemBus.findMatchingObjects(ifaces, 2, receiver);
			}
		}
		
		void DBusController::updateDeviceInfo(const char *object)
		{			
			// Look for HCI prefix - need to figure out if this naming scheme is guaranteed?
			if (std::strncmp(object, "/org/bluez/hci", 14) == 0)
			{
				Device device;
			
				const char *idxBase = object + 14;
			
				// Find end of adapter portion
				const char *delimit = idxBase;
				while (*delimit != '/' && *delimit != 0)
				{
					++delimit;
				}
				
				std::uintptr_t idx = cio::parseUnsigned(Text(idxBase, delimit - idxBase));
				device.setIndex(idx);

				// Determine proper address type
				try
				{
					Variant addressType = mSystemBus.getProperty(object, BLUEZ_ADAPTER_IFACE, "AddressType");
		
					if (addressType.matches("public"))
					{
						device.setAddressType(AddressType::Public);
					}
					else if (addressType.matches("random"))
					{
						device.setAddressType(AddressType::Random);
					}
				}
				catch (...)
				{
					// BlueZ tends to just fail if the device state isn't right
				}
				
				cio::Path key = object;
				mKnownDevices[key] = device;
			}
			else
			{
				sMetaclass.error() << "Device path doesn't match expected pattern, code needs to be updated to handle this";
			}
		}
		
		void DBusController::updatePeripheralInfo(const char *object)
		{
			// No point in tracking peripherals if we're not connected to a local adapter		
			if (!mAdapterPath.empty())
			{
				// check that object is a peripheral that is linked to our current adapter
				if (std::strncmp(mAdapterPath.c_str(), object, mAdapterPath.size()) == 0)
				{
					const char *peripheralText = object + mAdapterPath.size();
					if (std::strncmp(peripheralText, "/dev_", 5) == 0)
					{
						std::size_t remainder = std::strlen(peripheralText + 5);
						if (remainder == 17)
						{
							char macText[18];
							std::strcpy(macText, peripheralText + 5);
							macText[2] = ':'; // replace '_' with ':'
							macText[5] = ':';
							macText[8] = ':';
							macText[11] = ':';
							macText[14] = ':';

							DeviceAddress mac;
							mac.parse(macText);
							
							Peripheral *peripheral = nullptr;
														
							// See if we are updating an existing record
							for (Peripheral &p : mKnownPeripherals)
							{
								if (p.getAddress() == mac)
								{
									peripheral = &p;
									break;
								}
							}
							
							if (!peripheral)
							{
								mKnownPeripherals.emplace_back(mac);
								peripheral = &mKnownPeripherals.back();
							}

							try
							{
								Variant name = mSystemBus.getProperty(object, BLUEZ_PERIPHERAL_IFACE, "Name");
								peripheral->setShortName(name.getString().c_str());
							}
							catch (...)
							{
								// This should only happen if permissions are bad or maybe if the device is offline
							}
			
							try
							{
								Variant addressType = mSystemBus.getProperty(object, BLUEZ_PERIPHERAL_IFACE, "AddressType");
								if (addressType.matches("public"))
								{
									peripheral->setAddressType(AddressType::Public);
								}
								else if (addressType.matches("random"))
								{
									peripheral->setAddressType(AddressType::Random);
								}
								else
								{
									sMetaclass.warning() << "Unhandled address type " << addressType;
								}
							}
							catch (...)
							{
								// BlueZ tends to just fail if anything is out of whack
							}
							
							try
							{
								cio::Variant rssi = mSystemBus.getProperty(object, BLUEZ_PERIPHERAL_IFACE, "RSSI");
								peripheral->setSignalStrength(rssi.getInteger());
							}
							catch (const std::exception &e)
							{	
								// This happens when DBus has cached a device that is actually out of range or offline
								peripheral->clearSignalStrength();
							}
						}
					}
				}
			}
		}

		void DBusController::synchronizeDeviceCache()
		{
			mSystemBus.open(BLUEZ_DOMAIN);
		
			// if (mKnownDevices.empty() || mKnownPeripherals.empty())
			{
				// TODO subscribe for updates
				this->populateDeviceCache();
			}
			
			// TODO process pending updates
		}
	
		Variant DBusController::getAdapterProperty(const char *property) const
		{
			return mSystemBus.getProperty(mAdapterPath.c_str(), BLUEZ_ADAPTER_IFACE, property);
		}
		
		State DBusController::invokeAdapterCommand(const char *command)
		{
			return mSystemBus.invokeCommand(mAdapterPath.c_str(), BLUEZ_ADAPTER_IFACE, command);
		}
		
		// D-Bus Peripheral helpers
		
		std::size_t DBusController::printPeripheralPath(const DeviceAddress &peripheral, char *text, std::size_t length) const noexcept
		{
			std::size_t actual = 0;
			std::size_t printed = 0;
			
			if (!mAdapterPath.empty())
			{
				actual += mAdapterPath.size();
				
				std::size_t toCopy = std::min(actual, length);
				if (toCopy > 0)
				{
					std::memcpy(text, mAdapterPath.c_str(), toCopy);
				}
				printed = toCopy;
				
				if (actual < length)
				{
					char field[23] = "/dev_";
					peripheral.printMac(field + 5, 18);
					
					field[7] = '_'; // replace ':' with '_"
					field[10] = '_';
					field[13] = '_';
					field[16] = '_';
					field[19] = '_';
					
					toCopy = std::min<std::size_t>(22u, length - printed);
					if (toCopy > 0)
					{
						std::memcpy(text + printed, field, toCopy);
					}
					printed += toCopy;
				}
				
				actual += 22; // "/dev_" plus MAC (12 chars, 5 separators)	
			}
			
			if (printed < length)
			{
				std::memset(text + printed, 0, length - printed);
			}
			
			return actual;
		}
	}
}
