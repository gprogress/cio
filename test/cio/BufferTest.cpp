#include "BufferTest.h"

#include <cio/Buffer.h>
#include <cio/Resize.h>
#include <cio/test/Assert.h>

namespace cio
{
	BufferTest::BufferTest() :
		cio::test::Suite("cio/Buffer")
	{
		this->addTestCase("Buffer", &BufferTest::testConstruction);
		this->addTestCase("operator=", &BufferTest::testAssignment);
		this->addTestCase("reallocate", &BufferTest::testReallocate);
		this->addTestCase("limit", &BufferTest::testSetLimit);
		this->addTestCase("position", &BufferTest::testSetPosition);
		this->addTestCase("write", &BufferTest::testWrite);
		this->addTestCase("put", &BufferTest::testPut);
		this->addTestCase("read", &BufferTest::testRead);
		this->addTestCase("get", &BufferTest::testGet);
		this->addTestCase("flip", &BufferTest::testFlip);
		this->addTestCase("reset", &BufferTest::testReset);
		this->addTestCase("clear", &BufferTest::testClear);
		this->addTestCase("compact", &BufferTest::testCompact);
	}

	BufferTest::~BufferTest() noexcept = default;

	void BufferTest::testConstruction()
	{
		// Default construction
		{
			Buffer buffer;
			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());
		}

		// Explicit capacity construction
		{
			Buffer buffer(0);
			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());

			Buffer buffer2(14);
			CIO_ASSERT_EQUAL(14u, buffer2.size());
			CIO_ASSERT_EQUAL(14u, buffer2.limit());
			CIO_ASSERT_EQUAL(0u, buffer2.position());
			CIO_ASSERT(buffer2.data());
		}

		// Copy construction
		{
			Buffer buffer;
			Buffer buffer2(buffer);

			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());

			CIO_ASSERT_EQUAL(0u, buffer2.size());
			CIO_ASSERT_EQUAL(0u, buffer2.limit());
			CIO_ASSERT_EQUAL(0u, buffer2.position());
			CIO_ASSERT(!buffer2.data());

			std::uint8_t bytes[14] = { 0 };
			for (unsigned i = 0; i < 14; ++i)
			{
				bytes[i] = static_cast<std::uint8_t>(i);
			}

			Buffer buffer3(14);
			buffer3.limit(12);
			buffer3.position(3);
			std::memcpy(buffer3.data(), bytes, 14);

			Buffer buffer4(buffer3);

			CIO_ASSERT_EQUAL(14u, buffer3.size());
			CIO_ASSERT_EQUAL(12u, buffer3.limit());
			CIO_ASSERT_EQUAL(3u, buffer3.position());
			CIO_ASSERT(buffer3.data());

			CIO_ASSERT_EQUAL(14u, buffer4.size());
			CIO_ASSERT_EQUAL(12u, buffer4.limit());
			CIO_ASSERT_EQUAL(3u, buffer4.position());
			CIO_ASSERT(buffer4.data());

			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer3.data(), buffer3.size());
			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer4.data(), buffer4.size());
		}

		// Move construction
		{
			Buffer buffer;
			Buffer buffer2(std::move(buffer));

			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());

			CIO_ASSERT_EQUAL(0u, buffer2.size());
			CIO_ASSERT_EQUAL(0u, buffer2.limit());
			CIO_ASSERT_EQUAL(0u, buffer2.position());
			CIO_ASSERT(!buffer2.data());

			std::uint8_t bytes[14] = { 0 };
			for (unsigned i = 0; i < 14; ++i)
			{
				bytes[i] = static_cast<std::uint8_t>(i);
			}

			Buffer buffer3(14);
			buffer3.limit(12);
			buffer3.position(3);
			std::memcpy(buffer3.data(), bytes, 14);

			Buffer buffer4(std::move(buffer3));

			CIO_ASSERT_EQUAL(0u, buffer3.size());
			CIO_ASSERT_EQUAL(0u, buffer3.limit());
			CIO_ASSERT_EQUAL(0u, buffer3.position());
			CIO_ASSERT(!buffer3.data());

			CIO_ASSERT_EQUAL(14u, buffer4.size());
			CIO_ASSERT_EQUAL(12u, buffer4.limit());
			CIO_ASSERT_EQUAL(3u, buffer4.position());
			CIO_ASSERT(buffer4.data());

			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer4.data(), buffer4.size());
		}
	}

	void BufferTest::testAssignment()
	{
		// Copy assignment
		{
			Buffer buffer;
			Buffer buffer2;

			buffer2 = buffer;

			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());

			CIO_ASSERT_EQUAL(0u, buffer2.size());
			CIO_ASSERT_EQUAL(0u, buffer2.limit());
			CIO_ASSERT_EQUAL(0u, buffer2.position());
			CIO_ASSERT(!buffer2.data());

			std::uint8_t bytes[14] = { 0 };
			for (unsigned i = 0; i < 14; ++i)
			{
				bytes[i] = static_cast<std::uint8_t>(i);
			}

			Buffer buffer3(14);
			buffer3.limit(12);
			buffer3.position(3);
			std::memcpy(buffer3.data(), bytes, 14);

			Buffer buffer4;

			buffer4 = buffer3;

			CIO_ASSERT_EQUAL(14u, buffer3.size());
			CIO_ASSERT_EQUAL(12u, buffer3.limit());
			CIO_ASSERT_EQUAL(3u, buffer3.position());
			CIO_ASSERT(buffer3.data());

			CIO_ASSERT_EQUAL(14u, buffer4.size());
			CIO_ASSERT_EQUAL(12u, buffer4.limit());
			CIO_ASSERT_EQUAL(3u, buffer4.position());
			CIO_ASSERT(buffer4.data());

			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer3.data(), buffer3.size());
			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer4.data(), buffer4.size());
		}

		// Move assignment
		{
			Buffer buffer;
			Buffer buffer2;

			buffer2 = std::move(buffer);

			CIO_ASSERT_EQUAL(0u, buffer.size());
			CIO_ASSERT_EQUAL(0u, buffer.limit());
			CIO_ASSERT_EQUAL(0u, buffer.position());
			CIO_ASSERT(!buffer.data());

			CIO_ASSERT_EQUAL(0u, buffer2.size());
			CIO_ASSERT_EQUAL(0u, buffer2.limit());
			CIO_ASSERT_EQUAL(0u, buffer2.position());
			CIO_ASSERT(!buffer2.data());

			std::uint8_t bytes[14] = { 0 };
			for (unsigned i = 0; i < 14; ++i)
			{
				bytes[i] = static_cast<std::uint8_t>(i);
			}

			Buffer buffer3(14);
			buffer3.limit(12);
			buffer3.position(3);
			std::memcpy(buffer3.data(), bytes, 14);

			Buffer buffer4;
			buffer4 = std::move(buffer3);

			CIO_ASSERT_EQUAL(0u, buffer3.size());
			CIO_ASSERT_EQUAL(0u, buffer3.limit());
			CIO_ASSERT_EQUAL(0u, buffer3.position());
			CIO_ASSERT(!buffer3.data());

			CIO_ASSERT_EQUAL(14u, buffer4.size());
			CIO_ASSERT_EQUAL(12u, buffer4.limit());
			CIO_ASSERT_EQUAL(3u, buffer4.position());
			CIO_ASSERT(buffer4.data());

			CIO_ASSERT_BYTES_EQUAL(bytes, 14u, buffer4.data(), buffer4.size());
		}
	}

	void BufferTest::testWrite()
	{
		Buffer buffer;
		std::uint8_t bytes[4] = { 0xDE, 0xAD, 0xBE, 0xEF };

		buffer.setResizePolicy(Resize::None);

		buffer.write(bytes, 0);
		CIO_ASSERT_EQUAL(0u, buffer.position());

		CIO_ASSERT_THROW(buffer.write(bytes, 4), cio::Exception);
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.reallocate(2);
		buffer.write(bytes, 0);
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.limit(2);
		CIO_ASSERT_THROW(buffer.write(bytes, 4), cio::Exception);
		CIO_ASSERT_EQUAL(0u, buffer.position());

		// Update capacity but not limit
		buffer.reallocate(9);
		CIO_ASSERT_THROW(buffer.write(bytes, 4), cio::Exception);
		CIO_ASSERT_EQUAL(0u, buffer.position());

		// Update limit
		buffer.limit(6);
		buffer.write(bytes, 4);
		CIO_ASSERT_EQUAL(4u, buffer.position());
		std::uint8_t expected[4] = { 0xDE, 0xAD, 0xBE, 0xEF };
		CIO_ASSERT_BYTES_EQUAL(expected, 4u, buffer.data(), buffer.position());
	}

	void BufferTest::testPut()
	{
		Buffer buffer;

		std::uint16_t data = 0xDEAD;
		std::uint8_t bytes[4] = { 0xAD, 0xDE, 0xDE, 0xAD };

		buffer.setResizePolicy(Resize::None);

		CIO_ASSERT_THROW(buffer.putLittle(data), cio::Exception);

		buffer.reallocate(5);
		CIO_ASSERT_THROW(buffer.putLittle(data), cio::Exception);

		buffer.limit(2);
		buffer.putLittle(data);
		CIO_ASSERT_EQUAL(2u, buffer.position());
		CIO_ASSERT_BYTES_EQUAL(bytes, 2, buffer.data(), 2);

		CIO_ASSERT_THROW(buffer.putBig(data), cio::Exception);
		buffer.limit(4);
		buffer.putBig(data);
		CIO_ASSERT_BYTES_EQUAL(bytes, 4u, buffer.data(), buffer.position());

		// TODO test thoroughly
	}

	void BufferTest::testRead()
	{
		// TODO test thoroughly
	}

	void BufferTest::testGet()
	{
		// TODO test thoroughly
	}

	void BufferTest::testSetPosition()
	{
		Buffer buffer;

		buffer.position(0);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.position(5);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.reallocate(7);
		buffer.position(5);
		CIO_ASSERT_EQUAL(7u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.limit(4);
		buffer.position(5);
		CIO_ASSERT_EQUAL(7u, buffer.size());
		CIO_ASSERT_EQUAL(4u, buffer.limit());
		CIO_ASSERT_EQUAL(4u, buffer.position());

		buffer.position(3);
		CIO_ASSERT_EQUAL(7u, buffer.size());
		CIO_ASSERT_EQUAL(4u, buffer.limit());
		CIO_ASSERT_EQUAL(3u, buffer.position());
	}

	void BufferTest::testSetLimit()
	{
		Buffer buffer;

		buffer.limit(0);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		buffer.limit(7);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		// increase capacity and limit
		buffer.reallocate(5);
		buffer.limit(7);
		buffer.position(4);
		CIO_ASSERT_EQUAL(5u, buffer.size());
		CIO_ASSERT_EQUAL(5u, buffer.limit());
		CIO_ASSERT_EQUAL(4u, buffer.position());

		// decrease limit
		buffer.limit(3);
		CIO_ASSERT_EQUAL(5u, buffer.size());
		CIO_ASSERT_EQUAL(3u, buffer.limit());
		CIO_ASSERT_EQUAL(3u, buffer.position());

		buffer.limit(5);
		buffer.position(3);
		buffer.reallocate(4);
		CIO_ASSERT_EQUAL(4u, buffer.size());
		CIO_ASSERT_EQUAL(4u, buffer.limit());
		CIO_ASSERT_EQUAL(3u, buffer.position());
		CIO_ASSERT(buffer.data());
	}

	void BufferTest::testReallocate()
	{
		Buffer buffer;

		buffer.reallocate(0);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT(!buffer.data());

		buffer.reallocate(7);
		CIO_ASSERT_EQUAL(7u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT(buffer.data());

		buffer.limit(5);
		buffer.position(3);
		buffer.reallocate(4);
		CIO_ASSERT_EQUAL(4u, buffer.size());
		CIO_ASSERT_EQUAL(4u, buffer.limit());
		CIO_ASSERT_EQUAL(3u, buffer.position());
		CIO_ASSERT(buffer.data());

		buffer.reallocate(2);
		CIO_ASSERT_EQUAL(2u, buffer.size());
		CIO_ASSERT_EQUAL(2u, buffer.limit());
		CIO_ASSERT_EQUAL(2u, buffer.position());
		CIO_ASSERT(buffer.data());

		buffer.reallocate(0);
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT(!buffer.data());
	}

	void BufferTest::testReset()
	{
		Buffer buffer;

		buffer.reset();
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		std::uint8_t bytes[5] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x0A };

		buffer.reallocate(5);
		buffer.limit(4);
		buffer.position(2);
		std::memcpy(buffer.data(), bytes, 5);

		buffer.reset();
		CIO_ASSERT_EQUAL(5u, buffer.size());
		CIO_ASSERT_EQUAL(5u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT_BYTES_EQUAL(bytes, 5, buffer.data(), 5);
	}

	void BufferTest::testFlip()
	{
		Buffer buffer;

		buffer.flip();
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		std::uint8_t bytes[5] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x0A };

		buffer.reallocate(5);
		buffer.limit(4);
		buffer.position(2);
		std::memcpy(buffer.data(), bytes, 5);

		buffer.flip();
		CIO_ASSERT_EQUAL(5u, buffer.size());
		CIO_ASSERT_EQUAL(2u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT_BYTES_EQUAL(bytes, 5, buffer.data(), 5);
	}

	void BufferTest::testClear()
	{
		Buffer buffer;

		buffer.clear();
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT(!buffer.data());

		std::uint8_t bytes[5] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x0A };

		buffer.reallocate(5);
		buffer.limit(4);
		buffer.position(2);
		std::memcpy(buffer.data(), bytes, 5);

		buffer.clear();
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT(!buffer.data());
	}

	void BufferTest::testCompact()
	{
		Buffer buffer;

		buffer.compact();
		CIO_ASSERT_EQUAL(0u, buffer.size());
		CIO_ASSERT_EQUAL(0u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());

		std::uint8_t bytes[5] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x0A };

		buffer.reallocate(6);
		buffer.limit(5);
		buffer.position(3);
		std::memcpy(buffer.data(), bytes, 5);

		buffer.compact();
		CIO_ASSERT_EQUAL(6u, buffer.size());
		CIO_ASSERT_EQUAL(2u, buffer.limit());
		CIO_ASSERT_EQUAL(0u, buffer.position());
		CIO_ASSERT_BYTES_EQUAL(bytes + 3, 2, buffer.data(), 2);
	}
}
