/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_TYPES_H
#define CIO_NET_TYPES_H

#if defined _WIN32
#if defined CIO_NET_BUILD
#if defined CIO_NET_BUILD_SHARED
#define CIO_NET_API __declspec(dllexport)
#else
#define CIO_NET_API
#endif
#else
#define CIO_NET_API __declspec(dllimport)
#endif
#elif defined __ELF__
#define CIO_NET_API __attribute__ ((visibility ("default")))
#else
#define CIO_NET_API
#endif

#include <cstdint>

namespace cio
{
	/**
	 * The CIO net namespace provides classes useful for IP-based networking
	 * such as TCP/IP and UDP/IP channels, DNS name resolution, and similar services.
	 * It also provides for device discovery and management for network devices such as ethernet and 802.11 Wifi.
	 */
	namespace net
	{
		class Address;
		class AddressFamily;
		class Channel;
		enum class ChannelType : std::uint8_t;
		class Datagram;
		class DatagramChannel;
		class Device;
		enum class Flow : std::uint8_t;
		class IPv4;
		class IPv6;
		class Protocol;
		class ProtocolFamily;
		class Resolver;
		class Service;
		class Socket;
		class StreamChannel;
		class StreamServer;

		/** Native socket ID wrapped in a pointer-sized integer */
		using SocketId = std::uintptr_t;
	}
}

#endif



