/***********************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_PROTOCOL_H
#define CIO_ZIP_PROTOCOL_H

#include "Types.h"

#include <cio/Protocol.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace zip
	{
		/**
		 * The CIO Zip Protocol class provides the adapter to use Zip and other archive formats as if they were
		 * directories containing file contents. For historical reasons, the primary URI scheme is "jar".
		 * 
		 * The protocol also supports opening up files with compatible extensions such as .zip and .jar as directories
		 * even if the appropriate jar: protocol is not specified.
		 */
		class CIO_ZIP_ABI Protocol : public cio::Protocol
		{
			public:
				/**
				 * Gets the metaclass for this class.
				 * 
				 * @return the metaclass
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Gets the primary protocol prefix for this class.
				 * For historical reasons this is "jar".
				 * 
				 * @return the primary class protocol
				 */
				static Text getClassProtocolPrefix() noexcept;

				/**
				 * Gets the class protocol filter.
				 * This includes the following entries:
				 * <ol>
				 * <li>The general jar protocol scheme</li>
				 * <li>Wildcard protocol match for any Directory paths with extension .zip or .jar</li>
				 * </ol>
				 */
				static Chunk<ProtocolFilter> getClassProtocolFilter() noexcept;

				/**
				 * Splits the given path into two separate paths: the path to the underlying archive file,
				 * and the path to the entry within the file.
				 */
				static std::pair<Path, Path> splitArchiveEntry(const Path &path);

				/**
				 * Construct the Zip Protocol with the default prefix and filter.
				 */
				Protocol() noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Protocol() noexcept override;

				/**
				 * Clears the zip protocol to the default state.
				 */
				virtual void clear() noexcept override;

				/**
				 * Gets the metaclass for this object.
				 * 
				 * @return the metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Gets the metadata about the given resource.
				 * The metadata includes the modes and permissions, overall category, and content length.
				 * It also includes the canonical URI for the resource, following any links or redirects.
				 * If the resource does not actually exist, the returned metadata will reflect that.
				 *
				 * @param input the URI to the resource
				 * @return the metadata about the resource
				 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
				 */
				virtual Metadata readMetadata(const Path &input) const override;

				/**
				 * Creates the CIO Directory implementation most suitable for the given path, and then returns
				 * an opened directory suitable for scanning directory entries.
				 *
				 * The Zip Protocol implementation determines if the referenced path is to a supported archive format, and if
				 * so opens an appropriate Archive for it.
				 *
				 * @param path The path to the directory
				 * @param modes The modes to open the directory in
				 * @return the opened directory
				 * @throw cio::Exception If the directory could not be opened for any reason
				 * @throw std::bad_alloc If memory allocation failed
				 */
				virtual std::unique_ptr<cio::Protocol> openDirectory(const cio::Path &path, cio::ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::None) override;
				
				/**
				 * Creates the CIO Input implementation most suitable for the given path, and then returns an opened Input suitable
				 * reading file content.
				 * 
				 * The Zip Protocol implementation determines if the referenced path is a "jar" URI with an entry specified.
				 * If so, the archive is opened and then scanned to locate the specific path.
				 * 
				 * @param path The path to the archive entry
				 * @param modes The modes to open the file in
				 * @return the opened input
				 * @throw cio::Exception If the directory could not be opened for any reason
				 * @throw std::bad_alloc If memory allocation failed
				 */
				virtual std::unique_ptr<cio::Input> openInput(const cio::Path &path, cio::ModeSet modes = ModeSet::readonly()) const override;

			private:
				/** The metaclass for this class */
				static Class<Protocol> sMetaclass;

				/** The default filter for this class */
				static ProtocolFilter sDefaultFilter[2];
				
				/** Zip protocol prefix */
				static const Text sJarPrefix;

				/** Archive file extensions supported by this class */
				static const Text sArchiveExtensions;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

