/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MALLOC_H
#define CIO_MALLOC_H

#include "Types.h"

#include "Allocator.h"

namespace cio
{
	/**
	 * The Malloc Allocator implements the Allocator interface using C global malloc, realloc, and free functions.
	 * The class and methods are final for maximum performance.
	 */
	class CIO_API Malloc final : public Allocator
	{
		public:
			/**
			 * Gets the allocator for global C malloc/realloc/free.
			 * Under normal circumstances this is also the default allocator.
			 *
			 * @return the global C malloc/realloc/free allocator
			 */
			static constexpr Malloc *get() noexcept { return &Allocator::sMalloc; }

			/** 
			 * Construct the malloc allocator.
			 */
			inline Malloc() noexcept = default;
			
			/**
			 * Copies an allocator.
			 * 
			 * @param in The allocator to copy
			 * @return this allocator
			 */
			inline Malloc &operator=(const Malloc &in) = default;

			/**
			 * Destructor.
			 */
			virtual ~Malloc() noexcept override final = default;

			/**
			 * Allocates bytes using global ::malloc.
			 * If count is 0, this returns nullptr.
			 * This zeroes out the allocated bytes.
			 * 
			 * @param bytes The number of bytes desired
			 * @return the allocated byte array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *allocate(std::size_t bytes) override final;

			/**
			 * Allocates bytes that is a direct copy of the give bytes using global ::malloc.
			 * If count is 0, this returns nullptr.
			 * If the data is null, the bytes are allocated but not copied.
			 * This zeroes out any allocated bytes that are not copied from the input.
			 *
			 * @param data The data to copy
			 * @param bytes The number of bytes desired
			 * @return the allocated array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *duplicate(const void *data, std::size_t bytes) override final;

			/**
			 * Allocates bytes that is a direct copy of the give bytes using global ::malloc, copying a different number of bytes.
			 * If allocated count is 0, this returns nullptr.
			 * If the data is null, the bytes are allocated but not copied.
			 * This zeroes out any allocated bytes that are not copied from the input.
			 *
			 * @param data The data to copy
			 * @param copied The number of bytes to copy
			 * @param allocated The desired number of bytes in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *duplicate(const void *data, std::size_t copied, std::size_t allocated) override final;

			/**
			 * Deallocates the given bytes using global ::free.
			 * This does not perform object destruction or any other action.
			 * 
			 * @param data The existing data previously allocated by the same metaclass
			 * @param bytes The number of bytes to deallocate
			 * @return nullptr for convenience
			 */
			virtual std::nullptr_t deallocate(void *data, std::size_t bytes) noexcept override final;

			/**
			 * Reallocates bytes using a purely binary memory copy using global ::realloc.
			 * If the desired count is 0, this deallocates the input and returns nullptr.
			 * Any bytes not copied from the input are zeroed out.
			 *
			 * @param data The existing data previously allocated by the same metaclass
			 * @param current The current number of bytes
			 * @param requested The desired number of bytes
			 * @return the reallocated array, or nullptr if desired is 0
			 * @throw std::bad_alloc If new memory needed to be allocated and doing so failed
			 */
			virtual void *reallocate(void *data, std::size_t current, std::size_t requested) override final;

			/**
			 * Allocates bytes for objects using global ::malloc and constructs them.
			 * If count is 0, this returns nullptr.
			 * This uses the given metaclass object construction and destruction for objects.
			 *
			 * @param type The metaclass describing the type to allocate
			 * @param count The number of array elements desired
			 * @return the allocated object array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *create(const Metaclass *type, std::size_t count = 1) override final;

			/**
			 * Destroys objects and deallocates the given bytes using global ::free.
			 * Since this is often used in destructors and move operations, it cannot throw.
			 *
			 * @param data The existing data previously allocated by the same metaclass
			 * @param bytes The number of bytes to deallocate as a hint, or SIZE_MAX if not known
			 * @return nullptr for convenience
			 */
			virtual std::nullptr_t destroy(const Metaclass *type, void *data, std::size_t count = 1) noexcept override final;

			/**
			 * Duplicates an array of count elements using the given metaclass and global ::malloc.
			 * If the metaclass is null, this returns null.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param count The number of data elements to copy
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *copy(const Metaclass *type, const void *data, std::size_t count = 1) override final;

			/**
			 * Duplicates an array of count elements using the metaclass and global ::malloc, copying a different number of elements.
			 * If allocated is greater than copied, the additional elements not present in the input are default constructed.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param copied The number of data elements to copy
			 * @param allocated The desired number of elements in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *copy(const Metaclass *type, const void *data, std::size_t copied, std::size_t allocated) override final;
			
			/**
			 * Creates a copy of an array of count elements using the metaclass using global ::malloc, moving a different number of elements.
			 * If allocated is greater than moved, the additional elements not present in the input are default constructed.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param moved The number of data elements to move
			 * @param allocated The desired number of elements in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *move(const Metaclass *type, void *data, std::size_t moved, std::size_t allocated) override final;

			/**
			 * Reallocates an object array to have a new size using global ::malloc and ::free as needed.
			 * If the desired count is 0, this deallocates the input and returns nullptr.
			 * If the current size and new size are the same, this returns the data pointer without
			 * doing anything.
			 * 
			 * Since C ::realloc is not aware of object construction and destruction, we can't use it for this operation.
			 *
			 * @param data The existing data previously allocated by the same metaclass
			 * @param current The current number of elements
			 * @param desired The desired number of elements
			 * @return the reallocated array, or nullptr if desired is 0
			 * @throw std::bad_alloc If new memory needed to be allocated and doing so failed
			 */
			virtual void *resize(const Metaclass *type, void *data, std::size_t current, std::size_t desired) override final;
	};
}

#endif
