/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_DEVICEADDRESS_H
#define CIO_DEVICEADDRESS_H

#include "Types.h"

#include "Validation.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * The Device Address indicates the physical address of a networking or network-capable device.
	 * This is most commonly used for network adapters like ethernet, Wifi, and Bluetooth and any other adapters in the 802 networking family.
	 * There are three main device address formats - the classic Media Access Control (MAC, 48 bits) and
	 * the Extended Unique Identifier (EUI, 48-bit and 64-bit forms).
	 */
	class CIO_API DeviceAddress
	{
		public:
			/**
			 * Creates a device address from a 48-bit MAC address.
			 * This is a historical name for a 48-bit Extended Unique ID (EUI-48) address.
			 *
			 * @param mac The MAC address
			 * @return the device address
			 */
			static DeviceAddress fromMac(std::uint64_t mac) noexcept;

			/**
			 * Creates a device address from a 48-bit Extended Unique ID (EUI-48) address.
			 * This is the newer name for a MAC address.
			 *
			 * @param mac The EUI-48 address
			 * @return the device address
			 */
			static DeviceAddress fromExtended48(std::uint64_t eui48) noexcept;

			/**
			 * Creates a device address from a 64-bit Extended Unique ID (EUI-64) address.
			 *
			 * @param mac The EUI-64 address
			 * @return the device address
			 */
			static DeviceAddress fromExtended64(std::uint64_t eui64) noexcept;

			/**
			 * Parses a null-terminated text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the parsed device address
			 */
			static DeviceAddress fromText(const char *text) noexcept;

			/**
			 * Parses a text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @param length The number of bytes in the text to consider
			 * @return the parsed device address
			 */
			static DeviceAddress fromText(const char *text, std::size_t length) noexcept;

			/**
			 * Parses a text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the parsed device address
			 */
			static DeviceAddress fromText(const std::string &text) noexcept;

			/**
			 * Constructs an empty device address with all zero bytes.
			 */
			inline DeviceAddress() noexcept;
			
			/**
			 * Constructs a device address using the given data value.
			 * The byte value represents a 64-bit Extended Unique ID (EUI-64).
			 * Use DeviceAddress::fromMac if you explicitly need to use a 48-bit MAC or EUI-48.
			 *
			 * @param data The data value
			 */
			inline explicit DeviceAddress(std::uint64_t data) noexcept;
			
			/**
			 * Constructs a device address by parsing a text string.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the validation results of the parsing
			 */
			explicit DeviceAddress(const char *text) noexcept;
			
			/**
			 * Constructs a device address by parsing a text string.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @param length The number of bytes in the text
			 * @return the validation results of the parsing
			 */
			DeviceAddress(const char *text, std::size_t length) noexcept;
			
			/**
			 * Constructs a device address by parsing a text string.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the validation results of the parsing
			 */
			explicit DeviceAddress(const std::string &text) noexcept;
			
			/**
			 * Clears the device address to have the empty address with all zero bytes.
			 */
			inline void clear() noexcept;

			/**
			 * Gets the 48-bit MAC address.
			 * This will repack the address such that the low 6 bytes are the MAC and the high 2 bytes are zero.
			 * If the address is actually an EUI-64 address, the empty (0) MAC is returned.
			 * This is a historical name for a 48-bit Extended Unique ID (EUI-48) address.
			 *
			 * @return the MAC address
			 */
			std::uint64_t getMac() const noexcept;

			/**
			 * Sets this device address to present the given 48-bit MAC address.
			 * This will ignore the top two bytes of the given MAC address (which should be zero) and repack into this device address
			 * to match EUI-64 format with bytes 5 and 6 being set to zero.
			 * This is a historical name for a 48-bit Extended Unique ID (EUI-48) address.
			 *
			 * @param mac The MAC address to set
			 */
			void setMac(std::uint64_t mac) noexcept;

			/**
			 * Checks whether this address is a 48-bit MAC address (aka a 48-bit Extended Unique ID / EUI-48 address).
			 * This is true if bytes 5 and 6 are zero.
			 *
			 * @return whether this is a MAC address
			 */
			bool isMac() const noexcept;

			/**
			 * Gets the 48-bit Extended Unique ID (EUI-48) address.
			 * This will repack the address such that the low 6 bytes are the EUI-48 and the high 2 bytes are zero.
			 * If the address is actually an EUI-64 address, the empty (0) address is returned.
			 * This is the newer name for a 48-bit MAC address.
			 *
			 * @return the EUI-48 address
			 */
			std::uint64_t getExtended48() const noexcept;

			/**
			 * Sets this device address to present the given 48-bit Extended Unique ID (EUI-48) address.
			 * This will ignore the top two bytes of the given EUI-48 address (which should be zero) and repack into this device address
			 * to match EUI-64 format with bytes 5 and 6 being set to zero.
			 * This is the newer name for a 48-bit MAC address.
			 *
			 * @param eui48 The EUI-48 address to set
			 */
			void setExtended48(std::uint64_t eui48) noexcept;

			/**
			 * Checks whether this address is a 48-bit Extended Unique ID / EUI-48 address (aka a 48-bit MAC address).
			 * This is true if bytes 5 and 6 are zero.
			 *
			 * @return whether this is an EUI-48 address
			 */
			bool isExtended48() const noexcept;

			/**
			 * Gets the 64-bit Extended Unique ID (EUI-64) integer representation underlying this device address.
			 * If the device is actually a MAC / EUI-48 address, it will still be valid but bytes 5 and 6 will be zero.
			 *
			 * @return the EUI-64 value
			 */
			inline std::uint64_t getExtended64() const noexcept;

			/**
			 * Sets the 64-bit Extended Unique ID (EUI-64) integer representation underlying this device address.
			 * If the device is actually a MAC / EUI-48 address, bytes 5 and 6 on the input should be zero.
			 *
			 * @param eui64 the EUI-64 value
			 */
			inline void setExtended64(std::uint64_t eui64) noexcept;

			/**
			 * Gets the organization ID (OUI) portion of the device address.
			 * This is the top 3 bytes of the address.
			 *
			 * @return the organization ID
			 */
			std::uint32_t getOrganizationId() const noexcept;

			/**
			 * Gets the local ID portion of the device address.
			 * This is the low 5 bytes of the address for EUI-64 addresses or the low 3 bytes of the address for MAC / EUI-48 addresses.
			 *
			 * @return the local ID
			 */
			std::uint64_t getLocalId() const noexcept;

			/**
			 * Gets whether the universal bit is set on the address.
			 *
			 * @return whether the universal bit is set
			 */
			bool isUniversal() const noexcept;

			/**
			 * Interprets this device address in a Boolean context.
			 * The address is considered true if any bytes are nonzero.
			 *
			 * @return whether the address has any bytes set
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Prints the address to a text buffer if it is a 48-bit MAC (aka a 48-bit Extended Unique ID / EUI-48).
			 * If it is not a MAC, nothing is printed.
			 * If the buffer is not large enough for the output, the output will be truncated but the return value will specify how many bytes were needed.
			 * If the buffer is larger than the needed bytes, the remaining bytes are null terminated.
			 *
			 * @param data The text buffer to print to
			 * @param len The number of bytes in the text buffer
			 * @return the number of bytes needed to print the MAC
			 */
			std::size_t printMac(char *data, std::size_t len) const noexcept;

			/**
			  * Prints the address to a text buffer if it is a 48-bit Extended Unique ID / EUI-48 (aka a MAC).
			  * If it is not an EUI-48, nothing is printed.
			  * If the buffer is not large enough for the output, the output will be truncated but the return value will specify how many bytes were needed.
			  * If the buffer is larger than the needed bytes, the remaining bytes are null terminated.
			  *
			  * @param data The text buffer to print to
			  * @param len The number of bytes in the text buffer
			  * @return the number of bytes needed to print the EUI-48
			  */
			std::size_t printExtended48(char *data, std::size_t len) const noexcept;

			/**
			 * Prints the address to a text buffer formatted as a 64-bit Extended Unique ID / EUI-64.
			 * If it is actually a MAC / EUI-48, it is still printed and bytes 5 and 6 will be zero.
			  * If the buffer is not large enough for the output, the output will be truncated but the return value will specify how many bytes were needed.
			  * If the buffer is larger than the needed bytes, the remaining bytes are null terminated.
			  *
			  * @param data The text buffer to print to
			  * @param len The number of bytes in the text buffer
			  * @return the number of bytes needed to print the EUI-64
			  */
			std::size_t printExtended64(char *data, std::size_t len) const noexcept;

			/**
			 * Prints the address to a text buffer in the shortest format possible.
			 * If the address is a MAC / EUI-48, it will be printed in the shorter format for those.
			 * Otherwise it will be printed as an EUI-64.
			 * If the buffer is not large enough for the output, the output will be truncated but the return value will specify how many bytes were needed.
			 * If the buffer is larger than the needed bytes, the remaining bytes are null terminated.
			 *
			 * @param data The text buffer to print to
			 * @param len The number of bytes in the text buffer
			 * @return the number of bytes needed to print the address
			 */
			std::size_t print(char *data, std::size_t len) const noexcept;

			/**
			 * Prints the address to a returned string.
			 * The shortest format will be selected based on whether the address is a MAC / EUI-48 or not.
			 *
			 * @return the printed string
			 * @throw std::bad_alloc If not enough memory could be allocated for the string
			 */
			std::string print() const;

			/**
			 * Parses a null-terminated text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the validation results of the parsing
			 */
			TextParseStatus parse(const char *text) noexcept;

			/**
			 * Parses a text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @param len The number of bytes to consider in the text
			 * @return the validation results of the parsing
			 */
			TextParseStatus parse(const char *text, std::size_t len) noexcept;

			/**
			 * Parses a text string to get a device address.
			 * The string should be pairs of hexadecimal digits separated by ':' fields.
			 * If 6 such pairs are defined, the address is interpreted as a MAC/EUI-48 address.
			 * If 8 such pairs are defined, the address is interpreted as an EUI-64 address.
			 *
			 * @param text The text
			 * @return the validation results of the parsing
			 */
			TextParseStatus parse(const std::string &text) noexcept;
			
			/**
			 * Directly gets the data value in host byte order.
			 * The byte value represents a 64-bit Extended Unique ID (EUI-64).
			 * Use getMac or getExtended48 if you explicitly need to use a 48-bit MAC or EUI-48.
			 *
			 * @return the data value
			 */
			inline std::uint64_t value() const noexcept;
			
			/**
			 * Directly gets the data value in host byte order.
			 * The byte value represents a 64-bit Extended Unique ID (EUI-64).
			 * Use setMac or setExtended48 if you explicitly need to use a 48-bit MAC or EUI-48.
			 *
			 * @param value The data value
			 */
			inline void value(std::uint64_t value) noexcept;

		private:
			/** The underlying EUI-64 byte representation */
			std::uint64_t mExtended64;
	};

	/**
	 * Prints the address to a C++ stream.
	 * The shortest format will be selected based on whether the address is a MAC / EUI-48 or not.
	 *
	 * @param stream The stream
	 * @param address The address
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const DeviceAddress &address);

	/**
	 * Checks whether two device addresses are equal.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the addresses are equal
	 */
	inline bool operator==(const DeviceAddress &left, const DeviceAddress &right);

	/**
	 * Checks whether two device addresses are not equal.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the addresses are not equal
	*/
	inline bool operator!=(const DeviceAddress &left, const DeviceAddress &right);

	/**
	 * Checks whether a device address is less than another device address.
	 * The order is determined using the integer value of the EUI-64.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the first address is less than the second address
	 */
	inline bool operator<(const DeviceAddress &left, const DeviceAddress &right);

	/**
	 * Checks whether a device address is less than or equal to another device address.
	 * The order is determined using the integer value of the EUI-64.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the first address is less than or equal to the second address
	 */
	inline bool operator<=(const DeviceAddress &left, const DeviceAddress &right);

	/**
	 * Checks whether a device address is greater than another device address.
	 * The order is determined using the integer value of the EUI-64.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the first address is greater than the second address
	 */
	inline bool operator>(const DeviceAddress &left, const DeviceAddress &right);

	/**
	 * Checks whether a device address is greater than or equal to another device address.
	 * The order is determined using the integer value of the EUI-64.
	 *
	 * @param left The first address
	 * @param right The second address
	 * @return whether the first address is greater than or equal to the second address
	 */
	inline bool operator>=(const DeviceAddress &left, const DeviceAddress &right);
}

/* Inline implementation */

namespace cio
{
	inline DeviceAddress::DeviceAddress() noexcept :
		mExtended64(0)
	{
		// nothing more to do
	}
	
	inline DeviceAddress::DeviceAddress(std::uint64_t value) noexcept :
		mExtended64(value)
	{
		// nothing more to do
	}
	
	inline void DeviceAddress::clear() noexcept
	{
		mExtended64 = 0;
	}

	inline std::uint64_t DeviceAddress::getExtended64() const noexcept
	{
		return mExtended64;
	}

	inline void DeviceAddress::setExtended64(std::uint64_t eui64) noexcept
	{
		mExtended64 = eui64;
	}
	
	inline std::uint64_t DeviceAddress::value() const noexcept
	{
		return mExtended64;
	}
	
	inline void DeviceAddress::value(std::uint64_t value) noexcept
	{
		mExtended64 = value;
	}

	inline DeviceAddress::operator bool() const noexcept
	{
		return mExtended64 != 0;
	}

	inline bool operator==(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() == right.getExtended64();
	}

	inline bool operator!=(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() != right.getExtended64();
	}

	inline bool operator<(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() < right.getExtended64();
	}

	inline bool operator<=(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() <= right.getExtended64();
	}

	inline bool operator>(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() > right.getExtended64();
	}

	inline bool operator>=(const DeviceAddress &left, const DeviceAddress &right)
	{
		return left.getExtended64() >= right.getExtended64();
	}
}

#endif

