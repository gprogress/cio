/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Connection.h"

#include "Adapter.h"
#include "AttributeClient.h"

#include <cio/Class.h>
#include <cio/Task.h>

#if defined CIO_BLUETOOTH_HAVE_DBUS
#include "DBusAttributeClient.h"
#endif

#if defined CIO_BLUETOOTH_HAVE_NET
#include "L2CAPAttributeClient.h"
#endif

#include <cio/Class.h>

namespace cio
{
	namespace bluetooth
	{
		Class<Connection> Connection::sMetaclass("cio::bluetooth::Connection");
		
		const Metaclass &Connection::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
	
		const Metaclass *Connection::getBestClientType() noexcept
		{
#if defined CIO_BLUETOOTH_HAVE_DBUS
			return &DBusAttributeClient::getDeclaredMetaclass();
#elif defined CIO_BLUETOOTH_HAVE_NET
			return &L2CAPAttributeClient::getDeclaredMetaclass();
#else
			return &AttributeClient::getDeclaredMetaclass();
#endif
		}
				
		std::unique_ptr<AttributeClient> Connection::allocateBestClient()
		{
			std::unique_ptr<AttributeClient> client;
			const Metaclass *mc = getBestClientType();
		
			if (mc)
			{
				client.reset(static_cast<AttributeClient *>(mc->newInstance()));
			}
			
			return client;
		}
				
		AttributeClient *Connection::constructBestClient(void *buffer, std::size_t capacity)
		{
			AttributeClient *client = nullptr;
			const Metaclass *mc = getBestClientType();
			if (mc->size() < capacity)
			{
				mc->construct(buffer, 1);
				client = static_cast<AttributeClient *>(buffer);
			}
			
			return client;
		}
	
		Connection::Connection() noexcept :
			mConnected(false),
			mAutoDisconnect(false),
			mPaired(false),
			mAutoUnpair(false),
			mLowEnergy(false),
			mTimeout(10000)
		{
			// nothing more to do
		}
				
		Connection::Connection(const Device &device, const Peripheral &peripheral) :
			mConnected(false),
			mAutoDisconnect(false),
			mPaired(false),
			mAutoUnpair(false),
			mLowEnergy(false),
			mTimeout(10000)
		{
			this->connect(device, peripheral);
		}

		Connection::Connection(Connection &&in) noexcept :
			mAdapter(std::move(in.mAdapter)),
			mClient(std::move(in.mClient)),
			mDevice(std::move(in.mDevice)),
			mPeripheral(std::move(in.mPeripheral)),
			mConnected(in.mConnected),
			mAutoDisconnect(in.mAutoDisconnect),
			mPaired(in.mPaired),
			mAutoUnpair(in.mAutoUnpair),
			mLowEnergy(in.mLowEnergy),
			mTimeout(in.mTimeout)
		{
			in.mConnected = false;
			in.mAutoDisconnect = false;
			in.mPaired = false;
			in.mAutoUnpair = false;
		}
				
		Connection &Connection::operator=(Connection &&in) noexcept
		{
			if (this != &in)
			{
				this->clear();
				
				mAdapter = std::move(in.mAdapter);
				mClient = std::move(in.mClient);
				mDevice = std::move(in.mDevice);
				mPeripheral = std::move(in.mPeripheral);
				mConnected = in.mConnected;
				mAutoDisconnect = in.mAutoDisconnect;
				mPaired = in.mPaired;
				mAutoUnpair = in.mAutoUnpair;
				mLowEnergy = in.mLowEnergy;
				mTimeout = in.mTimeout;
				
				in.mConnected = false;
				in.mAutoDisconnect = false;
				in.mPaired = false;
				in.mAutoUnpair = false;
			}
			
			return *this;
		}
		
		Connection::~Connection() noexcept
		{
			try
			{
				if (mPaired && mAutoUnpair)
				{
					Task task(mTimeout);
					mAdapter->unpair(mPeripheral, task);
				}
			}
			catch (...)
			{
				// nothrow
			}
		
			try
			{
				if (mConnected && mAutoDisconnect)
				{
					Task task(mTimeout);
					mAdapter->disconnect(mPeripheral, task);
				}
			}
			catch (...)
			{
				// nothrow
			}
		}
						
		void Connection::clear() noexcept
		{
			try
			{
				if (mPaired && mAutoUnpair)
				{
					Task task(mTimeout);
					mAdapter->unpair(mPeripheral, task);
				}
			}
			catch (...)
			{
				// nothrow
			}
			
			mPaired = false;
			mAutoUnpair = false;
		
			try
			{
				if (mConnected && mAutoDisconnect)
				{
					Task task(mTimeout);
					mAdapter->disconnect(mPeripheral, task);
				}
			}
			catch (...)
			{
				// nothrow
			}
			
			mConnected = false;			
			mAutoDisconnect = false;
			
			mLowEnergy = false;
			mDevice.clear();
			mPeripheral.clear();
			mAdapter.reset();
			mClient.reset();
		}
		
		void Connection::open()
		{
			std::shared_ptr<Adapter> adapter = this->getOrCreateAdapter();
			
			if (!mDevice)
			{
				adapter->openDefaultDevice();
				mDevice = adapter->getDevice();
			}
		}
		
		void Connection::open(const Device &device)
		{
			this->update(device, mPeripheral);
			this->open();
		}
		
		State Connection::connect()
		{
			State status;
		
			if (!mConnected)
			{
				if (!mPeripheral)
				{
					throw cio::Exception(Reason::Missing, "No peripheral to connect to");
				}
				
				// No device or adapter set up, take care of those first
				if (!mDevice || !mAdapter)
				{
					this->open();
				}
				
				Task task(mTimeout);
				status = mAdapter->connect(mPeripheral, task);
				mConnected = status.succeeded();
				mLowEnergy = mPeripheral.isLowEnergy() && mAdapter->isLowEnergy();
				
				if (mClient && mConnected)
				{
					mClient->connect(mAdapter->getDevice(), mPeripheral);
				}
			}
			else
			{
				status.succeed(Reason::Exists);
			}

			return status;
		}
		
		State Connection::connect(const Peripheral &peripheral)
		{
			this->update(mDevice, peripheral);
			
			return this->connect();
		}
		
		State Connection::connect(const Device &device, const Peripheral &peripheral)
		{
			this->update(device, peripheral);
			
			return this->connect();
		}
		
		State Connection::pair()
		{
			Task task(mTimeout);
			State status = mAdapter->pair(mPeripheral, task);
			mPaired = status.succeeded();
			return status;
		}
		
		State Connection::unpair()
		{
			Task task(mTimeout);
			State status = mAdapter->unpair(mPeripheral, task);
			mPaired = false;		
			return status;
		}
		
		State Connection::disconnect()
		{
			State status;
			
			if (mClient)
			{
				mClient->disconnect();
			}
		
			if (mPaired)
			{
				if (mAutoUnpair)
				{
					Task task(mTimeout);
					mAdapter->unpair(mPeripheral, task);
				}
				
				mPaired = false;
			}
		
			if (mConnected)
			{
				Task task(mTimeout);
				status = mAdapter->disconnect(mPeripheral, task);
				mConnected = false;
			}
			
			return status;
		}

		bool Connection::isConnected() const noexcept
		{
			return mConnected;
		}
		
		void Connection::setConnected(bool value) noexcept
		{
			mConnected = value;
		}

		bool Connection::getAutoDisconnect() const noexcept
		{
			return mAutoDisconnect;
		}

		void Connection::setAutoDisconnect(bool value) noexcept
		{
			mAutoDisconnect = value;
		}

		bool Connection::isPaired() const noexcept
		{
			return mPaired;
		}

		bool Connection::getAutoUnpair() const noexcept
		{
			return mAutoUnpair;
		}

		void Connection::setAutoUnpair(bool value) noexcept
		{
			mAutoUnpair = value;
		}

		bool Connection::isLowEnergy() const noexcept
		{
			return mLowEnergy;
		}
		
		std::shared_ptr<Adapter> Connection::getOrCreateAdapter()
		{
			if (!mAdapter)
			{
				mAdapter.reset(new Adapter());
				
				if (mDevice)
				{
					mAdapter->open(mDevice);
				}
			}
			
			return mAdapter;
		}
		
		std::shared_ptr<Adapter> Connection::getAdapter() noexcept
		{
			return mAdapter;
		}
		
		std::shared_ptr<const Adapter> Connection::getAdapter() const noexcept
		{
			return mAdapter;
		}
		
		void Connection::setAdapter(std::shared_ptr<Adapter> adapter)
		{
			mAdapter = std::move(adapter);
			
			if (mAdapter && mDevice)
			{
				mAdapter->open(mDevice);
			}
		}

		std::shared_ptr<Adapter> Connection::takeAdapter() noexcept
		{
			return std::move(mAdapter);
		}

		void Connection::setController(std::shared_ptr<Controller> controller)
		{			
			std::shared_ptr<Adapter> adapter = std::dynamic_pointer_cast<Adapter>(controller);

			if (adapter)
			{
				mAdapter = std::move(adapter);
			}
			else
			{
				mAdapter = this->getOrCreateAdapter();
				mAdapter->setController(std::move(controller));
			}
			
			if (mAdapter && mDevice)
			{
				mAdapter->open(mDevice);
			}
		}

		const Device &Connection::getDevice() const noexcept
		{
			return mDevice;
		}
				
		void Connection::setDevice(const Device &device) noexcept
		{
			mDevice = device;
		}
				
		const Peripheral &Connection::getPeripheral() const noexcept
		{
			return mPeripheral;
		}
		
		void Connection::setPeripheral(const Peripheral &remote) noexcept
		{
			mPeripheral = remote;
		}
		
		std::shared_ptr<AttributeClient> Connection::getOrCreateAttributeClient()
		{
			if (!mClient)
			{
				mClient = Connection::allocateBestClient();
				
				if (mConnected)
				{
					mClient->connect(mAdapter->getDevice(), mPeripheral);
				}
			}
			
			return mClient;
		}
		
		std::shared_ptr<AttributeClient> Connection::getAttributeClient() noexcept
		{
			return mClient;
		}
		
		std::shared_ptr<const AttributeClient> Connection::getAttributeClient() const noexcept
		{
			return mClient;
		}
		
		void Connection::setAttributeClient(std::shared_ptr<AttributeClient> client) noexcept
		{
			mClient = std::move(client);
		}	

		std::shared_ptr<AttributeClient> Connection::takeAttributeClient() noexcept
		{
			return std::move(mClient);
		}

		void Connection::update(const Device &device, const Peripheral &peripheral)
		{
			if (device != mDevice || peripheral.getAddress() != mPeripheral.getAddress())
			{
				if (mPaired)
				{
					if (mAutoUnpair)
					{
						Task task(mTimeout);
						mAdapter->unpair(peripheral, task);
					}
					
					mPaired = false;
				}
			
				if (mConnected)
				{
					if (mClient)
					{
						mClient->disconnect();
					}
				
					if (mAutoDisconnect)
					{
						Task task(mTimeout);
						mAdapter->disconnect(peripheral, task);
					}
					
					mConnected = false;
				}
				
				mDevice = device;
				mPeripheral = peripheral;
				
				if (mAdapter)
				{
					if (mDevice)
					{
						mAdapter->open(mDevice);
					}
					else
					{
						mAdapter->close();
					}
				}
			}
		}		
		
		Connection::operator bool() const noexcept
		{
			return mAdapter && mPeripheral;
		}
	}
}
