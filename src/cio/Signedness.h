/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_SIGNEDNESS_H
#define CIO_SIGNEDNESS_H

#include "Types.h"

#include <type_traits>
#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Signedness enumeration describes the different encoding methodologies that can be used
	 * to represent negative numbers.
	 */
	enum class Signedness : std::uint8_t
	{
		/** The value is not signed, negative values are not possible */
		None = 0,
		
		/** The value is signed with one's complement bitwise-not used for the mantissa */
		Bitwise = 1,
		
		/** The value is signed with two's complement integer method used for the mantissa such as for conventional C++ signed integers */
		Negation = 2,
		
		/** The value is signed using a leading prefix bit before the exponent such as IEEE-754 floating point */
		Prefix = 3
	};
	
	/**
	 * Gets the appropriate signedness value for data type T. The value is determined as compile time constant.
	 * For unsigned integers, bool, and on some platforms char or wchar_t, this will return Signedness::None.
	 * For signed integers and on some platforms char or wchar_t, this will return Signedness::Negation.
	 * For float, double, and long double, this will return Signedness::Prefix.
	 * This relies on C++11 std::is_signed and std::is_float templates so it will also work with any other types that those handle.
	 *
	 * @tparam <T> The data type
	 * @return the signedness of that data type
	 */
	template <typename T>
	inline std::integral_constant<Signedness, (std::is_floating_point<T>::value ? Signedness::Prefix : (std::is_signed<T>::value ? Signedness::Negation : Signedness::None))>
	signedness() noexcept;
	
	/**
	 * Gets the label representing a signedness type.
	 *
	 * @param type The type
	 * @return the label
	 */
	CIO_API const char *print(Signedness type) noexcept;
	
	/**
	 * Prints the label representing a signedness type to a C++ stream.
	 *
	 * @param s The C++ stream
	 * @param type The signedness type
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Signedness type);
}

/* Inline implementation */

namespace cio
{
	template <typename T>
	inline std::integral_constant<Signedness, (std::is_floating_point<T>::value ? Signedness::Prefix : (std::is_signed<T>::value ? Signedness::Negation : Signedness::None))>
	signedness() noexcept
	{
		return std::integral_constant<Signedness, std::is_floating_point<T>::value ? Signedness::Prefix : (std::is_signed<T>::value ? Signedness::Negation : Signedness::None)>();
	}
}

#endif

