/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_ZIP_DEFLATOR_H
#define CIO_ZIP_DEFLATOR_H

#include "Types.h"

#include <cio/Buffer.h>
#include <cio/Output.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace zip
	{
		/**
		 * The Deflator class provides an Output overlay for transparently compressing stored bytes
		 * using the Deflate (aka Zip or Zlib) algorithm then storing the compressed bytes to another Output.
		 */
		class CIO_ZIP_ABI Deflator : public Output
		{
			public:
				/**
				 * Gets the static metaclass for the Deflator.
				 *
				 * @return the static metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct an unopened deflator.
				 * The compression level will be the default.
				 */
				Deflator() noexcept;

				/**
				 * Construt an unopened deflator with a desired buffer size and compression level.
				 *
				 * @param buffer The size of the compression buffer in bytes
				 * @param level The target compression level from 1 to 9
				 */
				explicit Deflator(std::size_t buffer, int level = 9);

				/**
				 * Construct a Deflator by transferring the contents of an existing one.
				 *
				 * @param in The deflator to move
				 */
				Deflator(Deflator &&in) noexcept;

				/**
				 * Moves a Deflator.
				 *
				 * @param in The deflator to move
				 * @return this deflator
				 */
				Deflator &operator=(Deflator &&in) noexcept;

				/**
				* Destructor.
				*/
				virtual ~Deflator() noexcept override;

				/**
				 * Gets the runtime metaclass for this object.
				 *
				 * @return the metaclass for this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Opens the resource using the given open modes.
				 * This method will open the underlying resource with the factory, then set up Deflator compression on it.
				 * This method will do nothing if the output modes do not include Write and either Load or Create.
				 *
				 * @param resource The resource to open
				 * @param modes The desired resource modes to use to open the resource
				 * @param factory The factory to use to construct underlying transport and codec protocols
				 * @return the actual resource modes that the resource could be opened with
				 * @throw cio::Exception If the resource could not be opened
				 */
				virtual cio::ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;

				/**
				 * Associates the given Output to receive compressed data from this Deflator.
				 * The output will not be owned or deallocated by this Deflator.
				 *
				 * @param output The output to use for compressed data
				 */
				void openWithUnownedOutput(Output *output);

				/**
				 * Associates the given Output to receive compressed data from this Deflator.
				 * The ownership semantics of the given Metapointer will be transferred to this Deflator.
				 *
				 * @param output The output to use for compressed data
				 */
				void openWithOutput(std::shared_ptr<Output> output);

				/**
				 * Gets the underlying output stream that compressed data is written to.
				 * 
				 * @return the underlying output
				 */
				std::shared_ptr<Output> getOutput() noexcept;

				/**
				 * Gets the underlying output stream that compressed data is written to.
				 *
				 * @return the underlying output
				 */
				std::shared_ptr<const Output> getOutput() const noexcept;

				/**
				 * Finalizes pending compression and takes ownership of the referenced Output from the Deflator.
				 * This can be used to embed compressed data inside of a stream then reclaim ownership of the stream to write uncompressed data or switch codecs.
				 * 
				 * @return the underlying output after compression is finalized
				 */
				std::shared_ptr<Output> takeOutput();

				/**
				 * Clears the Deflator.
				 * If any buffered compressed data hasn't been flushed to the underlying Output yet, that is done now.
				 * Removes all Zlib state and disassociates from the underlying Output.
				 */
				virtual void clear() noexcept override;

				/**
				 * Checks whether the Deflator is open.
				 * This is true if the deflator has underlying Output and that output is also open.
				 *
				 * @return whether the deflator is open
				 */
				virtual bool isOpen() const noexcept override;

				/**
				 * Stores the given data to the output, compressing using Deflate.
				 * The bytes are stored initially into an intermediate compressed data buffer, which is emptied when full or flushed
				 * into the associated Output.
				 *
				 * @param buffer The buffer of data to store
				 * @param bufferLen The number of bytes to store
				 * @return the result describing the status of the store operation and how many of the input bytes were actually stored
				 */
				virtual Progress<std::size_t> requestWrite(const void *buffer, std::size_t bufferLen) noexcept override;

				/**
				 * Configures the output compressed data buffer to have the given size in bytes.
				 * The larger the buffer is, the more efficient compression should be due to fewer iterations and fewer
				 * calls to the underlying Output; however, it will also increase latency of storing compressed output.
				 *
				 * @param bytes The number of bytes for the compressed data buffer
				 * @return the result describing whether this could be done and the actual size of the compressed data buffer
				 */
				virtual Progress<std::size_t> requestWriteBuffer(std::size_t bytes) noexcept override;

				/**
				 * Flushes all compressed data buffer output to the underlying Output, terminating the current Deflate block.
				 * 
				 * The depth parameter controls whether the flush is performed at all and if so how deeply to flush chained outputs.
				 *
				 * @param recursive If Recursive, also flush the associated Output too
				 * @return the status of the flush operation and the depth performed
				 */
				virtual Progress<Traversal> flush(Traversal recursive) noexcept override;

				/**
				 * Gets the compression level curently in use for this Deflator.
				 *
				 * @return the compression level from 1 to 9
				 */
				int getCompressionLevel() const;

				/**
				 * Sets the compression level for this Deflator.
				 * This only takes effect the next time an Output is associated.
				 *
				 * @param value The compression level from 1 to 9
				 */
				void setCompressionLevel(int value);

			private:
				/** The metaclass for this class */
				static Class<Deflator> sMetaclass;
				
				/**
				 * Configures this Deflator to work with a new Output.
				 */
				void configureNewOutput();

				/**
				 * Flushes any buffered compressed data to the underlying Output, then
				 * removes all Zlib state and disassociates from the underlying Output.
				 */
				void dispose();

				/**
				 * Flushes the internal Zlib buffer to the compressed data buffer.
				 *
				 * @return the result with the status and number of bytes flushed
				 */
				Progress<std::size_t> flushEncoderBuffer();

				/**
				 * Flushes the accumulated compressed data buffer to the underlying Output.
				 *
				 * @return the result with the status and number of bytes flushed
				 */
				Progress<std::size_t> flushStoreBuffer();

				/**
				 * The Output to receive compressed data.
				 */
				std::shared_ptr<Output> mSink;

				/**
				 * The handle to the Zlib encoder.
				 */
				void *mEncoder;

				/**
				 * The compressed data buffer.
				 */
				Buffer mStoreBuffer;

				/**
				 * The Zlib compression level from 1 to 9.
				 */
				int mLevel;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
