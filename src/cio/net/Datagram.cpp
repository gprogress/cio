/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Datagram.h"

namespace cio
{
	namespace net
	{
		Datagram::Datagram() noexcept
		{
			// nothing more to do
		}

		Datagram::Datagram(const Datagram &in) = default;

		Datagram::Datagram(Datagram &&in) noexcept :
			mSender(std::move(in.mSender)),
			mRecipient(std::move(in.mRecipient)),
			mPacket(std::move(in.mPacket)),
			mProgress(in.mProgress)
		{
			// nothing more to do
		}

		Datagram &Datagram::operator=(const Datagram &in) = default;

		Datagram &Datagram::operator=(Datagram &&in) noexcept
		{
			if (this != &in)
			{
				mSender = std::move(in.mSender);
				mRecipient = std::move(in.mRecipient);
				mPacket = std::move(in.mPacket);
				mProgress = in.mProgress;
			}

			return *this;
		}

		Datagram::~Datagram() noexcept
		{
			// nothing more to do
		}

		void Datagram::clear() noexcept
		{
			mSender.clear();
			mRecipient.clear();
			mPacket.clear();
			mProgress.clear();
		}

		void Datagram::setSender(Address sender) noexcept
		{
			mSender = std::move(sender);
		}

		void Datagram::setRecipient(Address recipient) noexcept
		{
			mRecipient = std::move(recipient);
		}

		void Datagram::setPacket(Buffer &&packet) noexcept
		{
			mPacket = std::move(packet);
		}

		std::size_t Datagram::size() const noexcept
		{
			return mPacket.size();
		}

		bool Datagram::empty() const noexcept
		{
			return mPacket.empty();
		}

		std::size_t Datagram::resize(std::size_t bytes)
		{
			return mPacket.resize(bytes);
		}

		std::size_t Datagram::reserve(std::size_t bytes)
		{
			return mPacket.reserve(bytes);
		}

		std::size_t Datagram::compact() noexcept
		{
			mPacket.compact();
			return mPacket.remaining();
		}

		std::size_t Datagram::flip() noexcept
		{
			mPacket.flip();
			return mPacket.remaining();
		}

		std::size_t Datagram::unflip() noexcept
		{
			mPacket.unflip();
			return mPacket.remaining();
		}

		std::size_t Datagram::reset() noexcept
		{
			mPacket.reset();
			return mPacket.limit();
		}

		std::size_t Datagram::skip(std::size_t bytes) noexcept
		{
			return mPacket.skip(bytes);
		}

		std::size_t Datagram::remaining() const noexcept
		{
			return mPacket.remaining();
		}

		std::size_t Datagram::remaining(std::size_t requested)
		{
			return mPacket.remaining(requested);
		}

		void Datagram::reply()
		{
			std::swap(mSender, mRecipient);

			if (mPacket.size() == 0)
			{
				mPacket.allocate(65535);
			}

			mPacket.reset();
		}

		Datagram::operator bool() const noexcept
		{
			return static_cast<bool>(mPacket);
		}
	}
}

