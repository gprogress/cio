/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_READER_H
#define CAQ_READER_H

#include "Types.h"

#include "Abstract.h"

namespace caq
{
	/**
	 * The Reader interface provides core methods for retrieving elements from an accessible logical array.
	 * If T is a volatile type, the reader will also operate on volatile array buffers.
	 * Whether T is const or not does not affect the interface.
	 * 
	 * @tparam T The data element type
	 */
	template <typename T>
	class Reader : public Abstract
	{
		public:
			/** The component type */
			using Component = T;

			/**
			 * Destructor.
			 */
			inline virtual ~Reader() noexcept override = default;

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * Subclasses must override this, and at bare minimum should return themselves if no other optimization is possible.
			 * 
			 * @return The optimized accessor achieving the same effect
			 */
			virtual Accessor<T> optimize() const = 0;

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const = 0;

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 * The Reader<T> base implementation calls the single element retrieve once for each index.
			 * Subclasses should override to improve performance.
			 *
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const
			{
				std::size_t cs = this->components();
				std::remove_const_t<T> *next = buffer;
				for (std::size_t c = 0; c < cs; ++c)
				{
					*next++ = this->get(n, c);
				}
				return next;
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 * The Reader<T> base implementation calls the single element retrieve once for each index.
			 * Subclasses should override to improve performance.
			 *
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const
			{
				std::remove_const_t<T> *next = buffer;
				for (unsigned long long n = 0; n < count; ++n)
				{
					next = this->retrieve(next, noff + n);
				}
				return next;
			}
	};
}

#endif
