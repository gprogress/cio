/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_METADATA_H
#define CIO_METADATA_H

#include "Types.h"

#include "Length.h"
#include "ModeSet.h"
#include "Path.h"
#include "Resource.h"

#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Metadata class represents the information about a particular resource without loading its actual content.
	 */
	class CIO_API Metadata
	{
		public:
			/**
			 * Construct empty metadata.
			 */
			Metadata() noexcept;

			/**
			 * Constructs metadata with the given text used to initialize the location URI.
			 *
			 * @param location the text for the location URI
			 */
			Metadata(const char *location);

			/**
			 * Constructs metadata with the given text used to initialize the location URI.
			 *
			 * @param location the text for the location URI
			 */
			Metadata(std::string location) noexcept;

			/**
			 * Constructs metadata with the given location URI.
			 *
			 * @param location the text for the location URI
			 */
			Metadata(Path location) noexcept;

			/**
			 * Constructs metadata with the given location URI and modes.
			 *
			 * @param location the text for the location URI
			 * @param openMode The modes
			 */
			Metadata(Path location, ModeSet openMode) noexcept;

			/**
			 * Constructs metadata with the given location URI, modes, and overallycategory.
			 *
			 * @param location the text for the location URI
			 * @param openMode The modes
			 * @param type The overall category
			 */
			Metadata(Path location, ModeSet openMode, Resource type) noexcept;
			
			/**
			 * Constructs metadata with the given location URI, modes, overall category, and logical length.
			 *
			 * @param location the text for the location URI
			 * @param openMode The modes
			 * @param type The overall category
			 * @param length The logical content length
			 */
			Metadata(Path location, ModeSet openMode, Resource type, Length length) noexcept;
			
			/**
			 * Constructs metadata with the given location URI, modes, overall category, logical length, and physical size.
			 *
			 * @param location the text for the location URI
			 * @param openMode The modes
			 * @param type The overall category
			 * @param length The logical content length
			 * @param physical The physical content size in bytes
			 */
			Metadata(Path location, ModeSet openMode, Resource type, Length length, Length physical) noexcept;
			
			/**
			 * Construct a deep copy of the metadata.
			 *
			 * @param in The metadata to copy
			 */
			Metadata(const Metadata &in);

			/**
			 * Construct metadata by transferring the contents of existing metadata.
			 *
			 * @param in The metadata to move
			 */
			Metadata(Metadata &&in) noexcept;

			/**
			 * Copy metadata.
			 *
			 * @param in The metadata to copy
			 * @return this metadata
			 */
			Metadata &operator=(const Metadata &in);

			/**
			 * Move metadata.
			 *
			 * @param in The metadata to move
			 * @return this metadata
			 */
			Metadata &operator=(Metadata &&in) noexcept;

			/**
			 * Destructor.
			 */
			~Metadata() noexcept;

			/**
			 * Clears the metadata and sets all fields to empty.
			 */
			void clear() noexcept;

			/**
			 * Checks to see whether the metadata is empty.
			 *
			 * @return whether the metadata is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Gets metadata representing the parent of the current resource.
			 * This obtains the parent URI of the location and sets the overall category to Directory.
			 * The modes and length of the parent will be unknown.
			 *
			 * @return the parent resource
			 */
			Metadata getParent() const;

			/**
			 * Clears the location URI.
			 */
			void clearLocation() noexcept;

			/**
			 * Gets the location URI.
			 *
			 * @return the location URI
			 */
			inline const Path &getLocation() const noexcept;

			/**
			 * Sets the location URI.
			 *
			 * @param location The location URI
			 */
			void setLocation(Path location) noexcept;

			/**
			 * Gets the modes and permissions of the resource.
			 *
			 * @return the modes
			 */
			inline ModeSet getMode() const noexcept;

			/**
			 * Sets whether particular mode(s) are enabled or disabled.
			 *
			 * @param mode The mode(s) to modify
			 * @param value Whether to enable or disable the modes
			 */
			inline void setMode(ModeSet mode, bool value = true) noexcept;

			/**
			 * Disables the given mode(s) from the resource.
			 *
			 * @param mode The mode(s) to disable
			 */
			inline void removeMode(ModeSet mode) noexcept;

			/**
			 * Checks to see whether the resource has the given mode enabled
			 *
			 * @param mode The mode of interest
			 * @return whether the mode is enabled
			 */
			inline bool hasMode(Mode mode) const noexcept;

			/**
			 * Gets the overall category of the resource.
			 *
			 * @return the overall category
			 */
			inline Resource getType() const noexcept;

			/**
			 * Sets the overall category of the resource.
			 *
			 * @param type The overall category
			 */
			inline void setType(Resource type) noexcept;

			/**
			 * Checks whether the resource is marked as readable.
			 *
			 * @return whether the resource is marked as readable
			 */
			inline bool isReadable() const noexcept;

			/**
			 * Checks whether the resource is marked as writable.
			 *
			 * @return whether the resource is marked as writable
			 */
			inline bool isWritable() const noexcept;

			/**
			 * Gets the logical content length of the resource, if known.
			 * For files and similar resources, this the number of bytes that can be read from and/or written to the resource.
			 * For directories this would be the number of directory entries.
			 * For links and shortcuts, this is the logical length of the linked resource.
			 * If the resource is compressed, encrypted, or transformed, this should be the original length prior to compression and so on.
			 *
			 * @return the logical content length, or CIO_CHANNEL_SIZE_UNKNOWN if unknown
			 */
			inline Length getLength() const noexcept;

			/**
			 * Sets the logical content length of the resource.
			 * For files and similar resources, this the number of bytes that can be read from and/or written to the resource.
			 * For directories this would be the number of directory entries.
			 * For links and shortcuts, this is the logical length of the linked resource.
			 * If the resource is compressed, encrypted, or transformed, this should be the original length prior to compression and so on.
			 * 
			 * If not known, the constant CIO_UNKNOWN_LENGTH should be set.
			 *
			 * @param size The logical content length
			 */
			inline void setLength(Length size) noexcept;

			/**
			 * Gets the physical size of the resource in bytes, if known.
			 * For files, directories, and most other resources, this the number of bytes actually occupied on storage medium.
			 * For links and shortcuts, this is the size of the link itself.
			 * If the resource is compressed, encrypted, or transformed, this should be the actual size after compression and so on.
			 *
			 * @return the content length in bytes, or CIO_CHANNEL_SIZE_UNKNOWN if unknown
			 */
			inline Length getPhysicalSize() const noexcept;

			/**
			 * Sets the physical size of the resource in bytes, if known.
			 * For files, directories, and most other resources, this the number of bytes actually occupied on storage medium.
			 * For links and shortcuts, this is the size of the link itself.
			 * If the resource is compressed, encrypted, or transformed, this should be the actual size after compression and so on.
			 * *
			 * If not known, the constant CIO_CHANNEL_SIZE_UNKNOWN should be set.
			 *
			 * @param size The content length
			 */
			inline void setPhysicalSize(Length size) noexcept;

			/**
			 * Validates this metadata against the given metadata, setting fields that are empty
			 * and retaining the most compatible modes.
			 *
			 * @param resource The metadata to validate against
			 * @return whether the result is usable
			 */
			bool validate(const Metadata &resource);


			// Convenience methods

			/**
			 * Check to see if a path exists and is a regular file.
			 *
			 * @param path The path
			 * @return whether the path exists and is a regular file
			 */
			inline bool isFile() const noexcept;

			/**
			 * Check to see if a path exists and is a directory file.
			 *
			 * @param path The path
			 * @return whether the path exists and is a directory file
			 */
			inline bool isDirectory() const noexcept;

			/**
			 * Check to see if a path exists and is a link.
			 *
			 * @param path The path
			 * @return whether the path exists and is a link
			 */
			inline bool isLink() const noexcept;

			/**
			 * Check to see if a path exists regardless of type.
			 *
			 * @param path The path
			 * @return whether the path exists
			 */
			inline bool exists() const noexcept;

			/**
			 * Interprets the metadata in a Boolean context.
			 * It is considered true if location URI is set.
			 *
			 * @return whether the metadata is considered true
			 */
			inline explicit operator bool() const noexcept;

		private:
			/** The URI location of the resource */
			Path mLocation;

			/** The modes and permissions of the source */
			ModeSet mMode;

			/** The overall category of the resource */
			Resource mType;

			/** The content length of the resource if known */
			Length mLength;
			
			/** The physical size of the resource if known */
			Length mPhysicalSize;
	};

	/**
	 * Checks whether two metadata objects are equal.
	 *
	 * @param left The first metadata object
	 * @param right The second metadata object
	 * @return whether the objects are equal
	 */
	CIO_API bool operator==(const Metadata &left, const Metadata &right) noexcept;

	/**
	 * Checks whether two metadata objects are not equal.
	 *
	 * @param left The first metadata object
	 * @param right The second metadata object
	 * @return whether the objects are not equal
	 */
	CIO_API bool operator!=(const Metadata &left, const Metadata &right) noexcept;

	/**
	 * Checks whether a metadata object sorts before another metadata object.
	 * The sort order is determined by the URI location.
	 *
	 * @param left The first metadata object
	 * @param right The second metadata object
	 * @return whether first object sorts before the second
	 */
	CIO_API bool operator<(const Metadata &left, const Metadata &right) noexcept;

	/**
	 * Prints a metadata object to a C++ stream.
	 *
	 * @param stream The C++ stream
	 * @param resource The metadata object
	 * @return the C++ stream
	 * @throw std::exception If the C++ stream throws an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Metadata &resource);
}

namespace cio
{
	inline bool Metadata::empty() const noexcept
	{
		return mLocation.empty();
	}

	inline const Path &Metadata::getLocation() const noexcept
	{
		return mLocation;
	}

	inline ModeSet Metadata::getMode() const noexcept
	{
		return mMode;
	}

	inline void Metadata::setMode(ModeSet mode, bool value) noexcept
	{
		mMode.set(mode, value);
	}

	inline void Metadata::removeMode(ModeSet mode) noexcept
	{
		mMode.erase(mode);
	}

	inline bool Metadata::hasMode(Mode mode) const noexcept
	{
		return mMode.count(mode);
	}

	inline Resource Metadata::getType() const noexcept
	{
		return mType;
	}

	inline void Metadata::setType(Resource type) noexcept
	{
		mType = type;
	}

	inline Length Metadata::getLength() const noexcept
	{
		return mLength;
	}

	inline void Metadata::setLength(Length length) noexcept
	{
		mLength = length;
	}

	inline Length Metadata::getPhysicalSize() const noexcept
	{
		return mPhysicalSize;
	}

	inline void Metadata::setPhysicalSize(Length size) noexcept
	{
		mPhysicalSize = size;
	}

	inline Metadata::operator bool() const noexcept
	{
		return !mLocation.empty();
	}

	inline bool Metadata::isReadable() const noexcept
	{
		return mMode.count(Mode::Load) && mMode.count(Mode::Read);
	}

	inline bool Metadata::isWritable() const noexcept
	{
		return (mMode.count(Mode::Load) || mMode.count(Mode::Create)) && mMode.count(Mode::Write);
	}

	inline bool Metadata::isFile() const noexcept
	{
		return mType == Resource::File;
	}

	inline bool Metadata::isDirectory() const noexcept
	{
		return mType == Resource::Directory;
	}

	inline bool Metadata::isLink() const noexcept
	{
		return mType == Resource::Link;
	}

	inline bool Metadata::exists() const noexcept
	{
		return !mLocation.empty() && mType != Resource::None && mMode.count(Mode::Load);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
