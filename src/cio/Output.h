/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_OUTPUT_H
#define CIO_OUTPUT_H

#include "Types.h"

#include "CharacterView.h"
#include "Exception.h"
#include "Length.h"
#include "ModeSet.h"
#include "Order.h"
#include "Path.h"
#include "Progress.h"
#include "Print.h"
#include "Progress.h"

#include <cstring>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Output interface represents the base interface for streaming output to a Resource over a transport layer.
	 * It is defined in terms of a core set of polymorphic virtual methods and then a larger set of helper methods
	 * for common output workflows.
	 *
	 * The class is designed so that you can override only the methods that are immediately needed, and the rest will have
	 * sensible base class implementations as much as possible.
	 *
	 * The only method that absolutely must be implemented is requestWrite.
	 *
	 * The core API includes the following items:
	 * Resource API - metadata and core resource management inherited from Resource
	 * get methods - core interface for obtaining information about and capabilities of the output
	 * set methods - simplified API for modifying output metadata or state
	 * write methods - simplified API for writing binary data with optional byte order conversion
	 * request methods - core polymorphic interface for writing, seeking, and setting parameters on the output which return a status result
	 */
	class CIO_API Output
	{
		public:
			/**
			 * Gets the metaclass for the Output class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Output() noexcept;

			/**
			 * Gets the metaclass for the runtime type of this object.
			 *
			 * @return the metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept;

			/**
			 * Clears the current state of the Output. This should close any files, handles, or other resources
			 * and free any memory used, but should not remove the underlying storage or data if not exclusively
			 * owned by this Output.
			 */
			virtual void clear() noexcept;

			// Open methods

			/*
			 * Checks whether the output is currently open and usable in any way.
			 *
			 * @return whether the output is open
			 */
			virtual bool isOpen() const noexcept;

			/**
			 * Opens the resource using the given open modes.
			 * The given protocol factory is used to traverse parent resources and construct
			 * underlying transport and codec mechanisms if necessary.
			 *
			 * This is the fully general open method that subclasses must override.
			 * The base class simply returns the empty mode set.
			 *
			 * @param resource The resource to open
			 * @param modes The desired resource modes to use to open the resource
			 * @param factory The factory to use to construct underlying transport and codec protocols
			 * @return the actual resource modes that the resource could be opened with
			 * @throw Exception If the resource could not be opened
			 */
			virtual ModeSet openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory);

			/**
			 * Opens the output to work with the given resource using the default modes for writing
			 * to a resource, failing if it is not present.
			 *
			 * The base class implements this method using openWithFactory which should always be sufficient.
			 * Subclasses may override to improve performance.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resourced could be opened with
			 */
			virtual ModeSet openToWrite(const Path &resource);

			/**
			 * Opens the output to work with the given resource using the default modes for writing
			 * to a resource, failing if it is already present.
			 *
			 * The base class implements this method using openWithFactory which should always be sufficient.
			 * Subclasses may override to improve performance.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resourced could be opened with
			 */
			virtual ModeSet createToWrite(const Path &resource);

			/**
			 * Opens the output to work with the given resource using the default modes for writing
			 * to a resource, creating it if not present.
			 *
			 * The base class implements this method using openWithFactory which should always be sufficient.
			 * Subclasses may override to improve performance.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resourced could be opened with
			 */
			virtual ModeSet openOrCreateToWrite(const Path &resource);

			// Polymorphic get metadata

			/**
			 * Requests the metadata for the currently opened resource if possible to obtain.
			 * Only the fields that are known are returned.
			 *
			 * The base implementation constructs metadata using type(), size(), location(), and modes().
			 * Subclasses may override for performance if desired.
			 *
			 * @return all known metadata for the open channel
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Metadata metadata() const;

			/**
			 * Gets the most specific path location of the resource, if possible.
			 *
			 * The base implementation returns an empty path.
			 *
			 * @return the resource location of the open channel if known
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Path location() const;

			/**
			 * Gets the overall resource type if known.
			 *
			 * The base implementation returns Resource::Unknown.
			 *
			 * @return the resource type of the open channel if known
			 */
			virtual Resource type() const noexcept;

			/**
			 * Gets the current modes of the resource to the extent known.
			 *
			 * The base implementation returns an empty mode set.
			 *
			 * @return the modes of the open channel if known
			 */
			virtual ModeSet modes() const noexcept;

			/**
			 * Gets the total size in bytes of the current output if known.
			 * If unknown or potentially unbounded, the constant CIO_UNKNOWN_LENGTH is returned, which is
			 * also the largest possible std::uint64_t size value.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH. Subclasses should always override it.
			 *
			 * @return the total resource size of the open channel in bytes
			 */
			virtual Length size() const noexcept;

			/**
			 * Checks whether the resource is empty and contains zero bytes.
			 *
			 * @return whether the resource is emtpy
			 */
			virtual bool empty() const noexcept;

			/**
			 * Gets whether this resource is resizable and if so how.
			 *
			 * @return the resizability
			 */
			virtual Resize resizable() const noexcept;
			
			/**
			 * Gets the theoretical maximum size of the resource.
			 *
			 * @return the maximum size
			 */
			virtual Length capacity() const noexcept;

			/**
			 * Requests to resize the output to the given length.
			 * 
			 * @param length The length
			 * @return the result of the resize request
			 */
			virtual Progress<Length> requestResize(Length length) noexcept;

			/**
			 * Gets the total number of bytes available to be written sequentially from the current write position.
			 * If unknown or potentially unbounded, the constant CIO_UNKNOWN_OUTPUT_SIZE is returned, which is
			 * also the largest possible std::uint64_t size value.
			 *
			 * The base class implements this using capacity() and getWritePosition() to calculate if both are known.
			 * Subclasses should override if they can do this more efficiently.
			 *
			 * @return the total number of bytes read
			 */
			virtual Length writable() const noexcept;

			/**
			 * Determines whether this output can be written to past the end of the write limit to automatically grow the output.
			 * This is typically true for things like files (which do have a size) as well as pipes, network streams, and other unbounded
			 * outputs. If the answer is unknown, this should return true and failures can be handled when actually trying to write past the end.
			 *
			 * Since the base class resizability is unknown, it returns true.
			 *
			 * @return whether writing past the end of this output grows the output
			 */
			virtual bool appendable() const noexcept;

			/**
			 * Gets the current sequential write position as a byte offset relative to the beginning of the resource, if known.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH.
			 *
			 * @return the current write position
			 */
			virtual Length getWritePosition() const noexcept;

			/**
			 * Calculates the absolute write position for the given relative position and Seek mode.
			 * This method does not modify the current write position or any other state.
			 *
			 * The base implementation uses getWritePosition() and size() where necessary.
			 * By definition Seek::Begin returns the input value.
			 * Seek::None returns 0 and Seek::Unknown returns CIO_UNKNOWN_LENGTH.
			 * Subclasses may override for peformance.
			 *
			 * @param value The relative byte offset of interest
			 * @param mode The seek mode
			 * @return the absolute position of that relative value
			 */
			virtual Length calculateWritePosition(Length value, Seek mode) const noexcept;

			/**
			 * Gets whether the write position is directly seekable on this output and if so in which direction.
			 * Currently this also specifies whether requestWriteToPosition and associated methods may succeed,
			 * based on the requested position vs. the current position.
			 *
			 * If this returns Resize::None, the output is not seekable and all writes and skips must be done
			 * by sequentially processing input.
			 *
			 * If this returns Resize::Grow, the write position may be moved forward but not backward.
			 * If this returns Resize::Shrink, the write position may be moved backward but not forward.
			 * If this returns Resize::Any, the output is fully random access and any seek should succeed.
			 *
			 * The base class returns Resize::Unknown.
			 *
			 * @return the write seekability status
			 */
			virtual Resize isWriteSeekable() const noexcept;

			// Simplified metadata set API - inherit from Resource, methods either succeed or throw

			/**
			 * Sets the current write position to a given absolute position relative to the given seek mode.
			 *
			 * This is implemented by calling requestSeekToWrite until it completes.
			 * Subclasses may override to improve performance.
			 *
			 * @param pos The desired write position to seek to
			 * @param seek The seek mode
			 * @throw Exception If the write seek failed
			 */
			virtual void seekToWrite(Length pos, Seek mode);

			// Write API - simplified methods to write or skip data

			/**
			 * Skips writing the given number of bytes in the output relative to the current write position.
			 * Unlike splat, skip does not modify any existing content in the skipped bytes.
			 *
			 * This is implemented by calling requestSkip until the given number of bytes are processed or it fails.
			 * Subclasses can override to improve performance.
			 *
			 * @param bytes The number of bytes to skip
			 * @throw Exception If not enough bytes could be skipped
			 */
			virtual void skip(Length bytes);

			/**
			 * Writes a constant byte value (typically 0) for a given number of bytes starting at the current write position.
			 * 
			  * This is implemented by calling requestSplat until the given number of bytes are processed or it fails.
			 * Subclasses can override to improve performance.
			 *
			 * @param value The constant byte value to store
			 * @param count The number of bytes to store with the provided value
			 * @throw Exception If not enough bytes could be written
			 */
			virtual void splat(std::uint8_t value, Length count);

			/**
			 * Writes a constant byte sequence a given number of times starting at the current write position.
			 *
			 * This is implemented by calling requestSplat until the given number of bytes are processed or it fails.
			 * Subclasses can override to improve performance.
			 *
			 * @param bytes The byte sequence to write
			 * @param length The length of the byte sequence
			 * @param count The number of times to store the given byte sequence
			 * @throw Exception If not enough bytes could be written
			 */
			virtual void splatSequence(const void *bytes, std::size_t length, Length count);

			/**
			 * Writes a byte array to the output.
			 *
			 * This is implemented by calling requestWrite until the given number of bytes are written or it fails.
			 * Subclasses can override to improve performance.
			 *
			 * @param data The data bytes to write
			 * @param length The number of bytes to write
			 * @throw Exception If not all of the bytes were written
			 */
			virtual void write(const void *data, std::size_t length);

			/**
			 * Writes a byte array to the output starting at the given position.
			 *
			 * This is implemented by calling requestWriteToPosition until the given number of bytes are written or it fails.
			 * Subclasses can override to improve performance.
			 *
			 * @param data The data bytes to write
			 * @param length The number of bytes to write
			 * @param position The position to write to
			 * @param seek mode The seek mode for the position
			 * @throw Exception If not all of the bytes were written
			 */
			virtual void writeToPosition(const void *data, std::size_t length, Length position, Seek mode);

			// Convenience API - writing UTF-8 text

			/**
			 * Writes the given C-style null-terminated string as binary data.
			 * The content of the string, but NOT the null-terminator, are stored to the output as binary data.
			 *
			 * This is implemented by calling write().
			 *
			 * @param text The text to store
			 * @return the actual number of bytes written
			 */
			std::size_t putText(const char *text);
			
			/**
			 * Writes the given C-style null-terminated string as binary data, up to the given number of input bytes.
			 * The content of the string, but NOT the null-terminator, are stored to the output as binary data.
			 *
			 * This is implemented by calling write().
			 *
			 * @param text The text to store
			 * @param maxLength The maximum number of input bytes
			 * @return the actual number of bytes written
			 */
			std::size_t putText(const char *text, std::size_t maxLength);
			
			/**
			 * Writes the given C++ string as binary data.
			 * The content of the string as provided by size(), but NOT a null-terminator, are stored to the output as binary data.
			 *
			 * @param text The text to write
			 * @return the actual number of bytes written
			 * @throw Exception If the text could not be stored
			 */
			std::size_t putText(const std::string &text);

			/**
			 * Stores the given text as binary data.
			 * The content of the string as provided by strlen(), up to but NOT including a null-terminator, are stored to the output as binary data.
			 *
			 * @param text The text to store
			 * @return the actual number of bytes written
			 * @throw Exception If the text could not be stored
			 */
			std::size_t putText(const Text &text);

			/**
			 * Prints the given fixed-size text to the buffer up to the fixed text length.
			 * If the actual text is shorter, this will print null terminators up to the length.
			 *
			 * @tparam <N> The fixed text length
			 * @param text The fixed text
			 * @return the number of bytes printed which is always N
			 * @throw Exception If not enough space remained in the buffer
			 */
			template <std::size_t N>
			inline std::size_t putText(const FixedText<N> &text);

			/**
			 * For value types not otherwise handled, determine if it has a reasonable fixed size print buffer and use that.
			 * Otherwise, use the dynamic printing method.
			 * 
			 * @tparam T The value type to print
			 * @param value The value to print
			 * @return the number of bytes actually printed
			 * @throw Exception If the text could not be stored
			 */
			template <typename T>
			inline std::size_t putText(const T &value);

			// Convenience API - writing binary data

			/**
			 * Writes machine-level numeric data to the output in host byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param data The data to write
			 * @throw Exception If not enough bytes could be written to the device
			 */
			template <typename T>
			inline void put(T data);

			/**
			 * Writes machine-level numeric data to the buffer, converted from Host to Big byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param data The data to write
			 * @param endian The byte order
			 * @throw Exception If not enough bytes could be written to the buffer
			 */
			template <typename T>
			inline void putBig(T data);

			/**
			 * Writes machine-level numeric data to the buffer, converted from Host to Little byte order.
			 *
			 * @tparam <T> The type of number, e.g. int, long, float, double
			 *
			 * @param data The data to write
			 * @param endian The byte order
			 * @throw Exception If not enough bytes could be written to the buffer
			 */
			template <typename T>
			inline void putLittle(T data);

			/**
			 * Writes an array of primitive machine-level values in native host byte order.
			 *
			 * @tparam <T> The data type to write
			 *
			 * @param data The array to write
			 * @param count The number of elements to write
			 */
			template <typename T>
			inline void putArray(const T *data, std::size_t count);

			/**
			 * Writes an array of primitive machine-level values ensuring that element is converted from Host to Big byte order.
			 *
			 * @tparam <T> The data type to write
			 *
			 * @param data The array to write
			 * @param count The number of elements to write
			 * @param endian The byte order
			 */
			template <typename T>
			inline void putBigArray(const T *data, std::size_t count);

			/**
			 * Writes an array of primitive machine-level values ensuring that element is converted from Host to Little byte order.
			 *
			 * @tparam <T> The data type to write
			 *
			 * @param data The array to write
			 * @param count The number of elements to write
			 * @param endian The byte order
			 */
			template <typename T>
			inline void putLittleArray(const T *data, std::size_t count);

			/**
			 * Writes an array of primitive machine-level values, performing a per-element byte swap while writing.
			 *
			 * @tparam <T> The data type to write
			 *
			 * @param data The array to write
			 * @param count The number of elements to write
			 */
			template <typename T>
			inline void putSwappedArray(const T *data, std::size_t count);

			// Polymorphic Write API - virtual non-throwing methods that may perform partial actions

			/**
			 * Sets the current write position to a given absolute position relative to the given seek position in the output.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH with Status::Unsupported.
			 * Subclasses should override if they provide seeking support.
			 *
			 * @param pos The desired write position to seek to
			 * @param mode The seek mode for the position
			 * @return the resulting write position and outcome
			 */
			virtual Progress<Length> requestSeekToWrite(Length pos, Seek mode) noexcept;

			/**
			 * Writes up to the given number of bytes from the provided data buffer into the output.
			 * The write is performed from the current sequential write position up to the end of the output
			 * and is performed synchronously in the calling thread.
			 *
			 * The returned state describes the outcome of this write request including number of bytes actually written.
			 *
			 * The base implementation fails with Reason::Unsupported.
			 * Subclasses must override to provide write behavior.
			 *
			 * @param data The data buffer to write from
			 * @param length The maximum number of bytes to write from the buffer to the output
			 * @return a result describing the outcome and actual number of bytes written
			 */
			virtual Progress<std::size_t> requestWrite(const void *data, std::size_t length) noexcept;

			/**
			 * Write up to the given number of bytes from the provided data buffer into the output.
			 * The write is started at the given write position relative to the given seek mode.
			 * Even if the seek mode is Seek::Current, this does not modify the current write position.
			 *
			 * The returned state describes the outcome of this write request including number of bytes actually written.
			 *
			 * The base implementation fails with Reason::Unsupported.
			 * Subclasses should override if they support this capability.
			 *
			 * @param data The data buffer to write from
			 * @param length The maximum number of bytes to write from the buffer to the output
			 * @param position The  byte offset in the output to write to
			 * @param mode The seek mode for the position
			 * @return a result describing the outcome and actual number of bytes written
			 */
			virtual Progress<std::size_t> requestWriteToPosition(const void *data, std::size_t length, Length position, Seek mode) noexcept;

			/**
			 * Skips writing the given number of bytes in the output relative to the current write position.
			 * Unlike splat, skip does not necessarily modify any existing content in the skipped bytes - if it is more efficient, their value
			 * is left indeterminate.
			 *
			 * The base implementation implements this using requestSplat.
			 *
			 * @param bytes The number of bytes to skip
			 * @return a result describing the outcome and actual number of bytes skipped
			 */
			virtual Progress<Length> requestSkip(Length bytes) noexcept;

			/**
			 * Writes a constant byte value (typically 0) for a given number of bytes starting at the current write position.
			 *
			 * The base implementation implements this using requestWrite for a buffer of up to 512 bytes and keeps writing
			 * until the count is met or the write itself returns less than expected or fails.
			 *
			 * @param value The constant byte value to store
			 * @param count The number of bytes to store with the provided value
			 * @return a result describing the outcome and actual number of bytes stored
			 */
			virtual Progress<Length> requestSplat(std::uint8_t value, Length count) noexcept;

			/**
			 * Instructs the Output that a write buffer consisting of at least the specified number of bytes should be used where relevant.
			 * This is purely an advisory call that may not actually do anything depending on the output type and hardware.
			 * It is primarily useful for compressors and other types of outputs that build up a buffered output to send to another Output.
			 *
			 * If this method succeeds with a non-zero byte count, then the Output supports
			 * buffering and the flush() method is available to control when the buffer is flushed.
			 *
			 * The base implementation fails with Reason::Unsupported.
			 *
			 * @param bytes The requested number of bytes for the store buffer
			 * @return the outcome describing whether this request could be honored and the actual store buffer size
			 */
			virtual Progress<std::size_t> requestWriteBuffer(std::size_t bytes) noexcept;

			/**
			 * Instructs the Output to flush any deferred or buffered output to the underlying device and otherwise synchronize the state.
			 * This is purely an advisory call that may not actually do anything depending on the output type and hardware.
			 * It is primarily useful for compressors and other types of outputs that build up a buffered output to send to another Output.
			 *
			 * The recursive parameter controls whether the flush should be recursively propagated to any underlying Outputs -
			 * If Traversal::None is specified, no flush is performed.
			 * If Traversal::Immediate is specified, this Output is flushed.
			 * If Traversal::Recursive is specified, this flushes all levels of chained output and ensures (to the extent possible)
			 * that all data has been fully specified to the final target device.
			 * Other values may be used to specify specific flush depths.
			 *
			 * @note The base implementation fails with Reason::Unsupported indicating no operation was attempted.
			 * Subclasses that guarantee synchronous store operations should override this to return success instead,
			 * and subclasses that have actual flush semantics should implement them as an override with the actual depth flushed.
			 *
			 * @param depth The traversal model specifying how to apply the flush operation if the Output has referenced chained Outputs,
			 * has no effect if the Output doesn't support the flush operation or operates directly on a hardware device.
			 * @return the outcome describing the actual depth flushed (if known) and the status of the flush operation
			 */
			virtual Progress<Traversal> flush(Traversal depth) noexcept;
			
			/**
			 * Advises the input that some sequence of future write requests will be performed at the given byte range.
			 * The base implementation does nothing and fails with Reason::Unsupported.
			 * Subclasses may override this to preallocate data, resize internal buffers, inform the OS, or any other potentially useful
			 * behavior to improve performance is done.
			 *
			 * @param pos The intended write start position, relative to the seek mode
			 * @param mode The seek mode of the write position
			 * @param bytes The number of bytes in range that might be written
			 * @return the acknowledgment of what this advice did, if anything
			 */
			virtual State adviseUpcomingWrite(Length pos, Seek mode, std::size_t bytes) noexcept;
			
			// Buffer management
						
			/**
			 * Requests to write the contents of the buffer into the output starting at its current position until its limit.
			 * Fewer bytes may be written and this is not necessarily an error.
			 * The buffer's position will be moved by the number of bytes actually written.
			 *
			 * @param buffer The buffer to drain
			 * @return the result showing the number of bytes requested, actually written, and outcome
			 */
			Progress<std::size_t> requestDrain(Buffer &buffer) noexcept;
			
			/**
			 * Writes the contents of the buffer into the output starting at its current position until its limit.
			 * If not enough bytes could be written, this throws an exception.
			 * The buffer's position will be moved by the number of bytes actually written.
			 *
			 * @note Depending on when the output detects the error, some bytes may be written even if an exception is thrown.
			 * The buffer will be updated to reflect the partial write even in this case.
			 *
			 * @param buffer The buffer to drain
			 * @return the result showing the number of bytes requested, actually written, and outcome
			 * @throw Exception If not enough bytes could be written
			 */
			Progress<std::size_t> drain(Buffer &buffer);

			/**
			 * Interprets whether the output is currently open when used in a Boolean context.
			 *
			 * @return whether the output is currently open
			 */
			explicit operator bool() const noexcept;

		private:
			/** The metaclass for this class */
			static Class<Output> sMetaclass;
	};

	/**
	 * Print a value using the Output as the target for any value.
	 *
	 * @tparam T The value type being printed
	 * @param value The value to print
	 * @param buffer The buffer to print to
	 * @return the number of bytes needed to print
	 */
	template <typename T>
	inline std::size_t print(const T &value, Output &output) noexcept
	{
		auto printed = cio::print(value);
		auto text = cio::text(printed);
		output.requestWrite(text.data(), text.values());
		return text.values();
	}

	/**
	 * Provides an overloaded operator<< print operator to print value strings as UTF-8 to an Output similar
	 * to a std::ostream.
	 *
	 * @tparam T The value type being printed
	 * @param buffer The buffer to print to
	 * @param value The value to print
	 * @return the buffer after printing
	 * @throw Exception If not enough space was available to print the value
	 */
	template <typename T>
	inline Output &operator<<(Output &stream, const T &value)
	{
		print(value, stream);
		return stream;
	}
}

/* Inline implementation */

namespace cio
{	
	template <std::size_t N>
	inline std::size_t Output::putText(const FixedText<N> &text)
	{
		std::size_t length = text.strlen();
		this->write(text.data(), length);
		return length;
	}

	template <typename T>
	inline std::size_t Output::putText(const T &value)
	{
		return this->putText(print(value));
	}

	template <typename T>
	inline void Output::put(T data)
	{
		this->write(&data, sizeof(T));
	}
	
	template <typename T>
	inline void Output::putBig(T data)
	{
		this->put(Big::order(data));
	}

	template <typename T>
	inline void Output::putLittle(T data)
	{
		this->put(Little::order(data));
	}

	template <typename T>
	inline void Output::putArray(const T *data, std::size_t count)
	{
		this->write(data, sizeof(Value<T>) * count);
	}

	template <typename T>
	inline void Output::putBigArray(const T *data, std::size_t count)
	{
#if CIO_ENDIAN == CIO_BIG_ENDIAN
		this->write(data, sizeof(Value<T>) * count);
#else
		this->putSwappedArray(data, count);
#endif
	}

	template <typename T>
	inline void Output::putLittleArray(const T *data, std::size_t count)
	{
#if CIO_ENDIAN == CIO_LITTLE_ENDIAN
		this->write(data, sizeof(Value<T>) * count);
#else
		this->putSwappedArray(data, count);
#endif
	}

	template <typename T>
	inline void Output::putSwappedArray(const T *data, std::size_t count)
	{
		if (count > 0)
		{
			if (sizeof(T) > 1)
			{
				switch (count)
				{
					case 0:
						// nothing to do
						break;

					case 1:
					{
						T tmp = swapBytes(*data);
						this->write(&tmp, sizeof(T));
						break;
					}

					case 2:
					{
						T tmp[2] = { swapBytes(data[0]), swapBytes(data[1]) };
						this->write(tmp, 2 * sizeof(T));
						break;
					}

					case 3:
					{
						T tmp[3] = { swapBytes(data[0]), swapBytes(data[1]), swapBytes(data[2]) };
						this->write(tmp, 3 * sizeof(T));
						break;
					}

					case 4:
					{
						T tmp[4] = { swapBytes(data[0]), swapBytes(data[1]), swapBytes(data[2]),  swapBytes(data[3]) };
						this->write(tmp, 4 * sizeof(T));
						break;
					}

					default:
					{
						Progress<std::size_t> written;
						const T *current = data;
						const T *end = data + count;
						while (current < end)
						{
							T tmp = swapBytes(*current);
							Progress<std::size_t> result = this->requestWrite(&tmp, sizeof(T));
							while (result.count < sizeof(T) && !result.failed())
							{
								result += this->requestWrite(reinterpret_cast<const char *>(&tmp) + result.count, sizeof(T) - result.count);
							}
						
							written += result;
							if (result.count < sizeof(T))
							{
								throw Exception(written);
							}

							++current;
						}

						break;
					}
				}
			}
			else
			{
				this->write(data, count);
			}
		}
	}

	template <>
	inline void Output::putSwappedArray<void>(const void *data, std::size_t count)
	{
		this->write(data, count);
	}
	
	template <>
	inline void Output::putSwappedArray<bool>(const bool *data, std::size_t count)
	{
		this->write(data, count);
	}
	
	template <>
	inline void Output::putSwappedArray<char>(const char *data, std::size_t count)
	{
		this->write(data, count);
	}
	
	template <>
	inline void Output::putSwappedArray<signed char>(const signed char *data, std::size_t count)
	{
		this->write(data, count);
	}
	
	template <>
	inline void Output::putSwappedArray<unsigned char>(const unsigned char *data, std::size_t count)
	{
		this->write(data, count);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
