/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_TEST_TYPES_H
#define CIO_TEST_TYPES_H

#if defined _WIN32
#if defined CIO_TEST_BUILD
#if defined CIO_TEST_BUILD_SHARED
#define CIO_TEST_API __declspec(dllexport)
#else
#define CIO_TEST_API
#endif
#else
#define CIO_TEST_API __declspec(dllimport)
#endif
#elif defined __ELF__
#define CIO_TEST_API __attribute__ ((visibility ("default")))
#else
#define CIO_TEST_API
#endif

#include <cstdint>

namespace cio
{
	class Metaclass;
	class ResourcePath;
	enum class Severity : std::uint8_t;

	/**
	 * The CIO test namespace provides classes suitable for robust and high-performance automated testing such
	 * as needed for unit test frameworks.
	 */
	namespace test
	{
		class Fault;
		class Fixture;
		template <typename F = void (*)()> class Function;
		template <typename T, typename F = void (T::*)()> class Method;
		class Reporter;
		class Suite;
		class Test;
	}
}

#endif
