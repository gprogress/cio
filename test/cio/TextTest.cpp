/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TextTest.h"

#include <cio/test/Assert.h>

#include <cio/Text.h>

namespace cio
{
	TextTest::TextTest() :
		cio::test::Suite("cio::Text")
	{
		this->addTestCase("Text()", &TextTest::defaultConstructorTest);
		this->addTestCase("Text(std::string)", &TextTest::stringConstructorTest);
		this->addTestCase("operator=(std::string)", &TextTest::stringAssignmentTest);
		this->addTestCase("bind(std::string)", &TextTest::stringBindTest);
		CIO_TEST_METHOD(cio::TextTest, toUpper);
		CIO_TEST_METHOD(cio::TextTest, toLower);
	}

	TextTest::~TextTest() noexcept = default;

	void TextTest::defaultConstructorTest()
	{
		Text text;
		CIO_ASSERT_EQUAL(0u, text.capacity());
		CIO_ASSERT_EQUAL((void *) nullptr, (void *)text.data());
		CIO_ASSERT(!text.allocator());
	}

	void TextTest::stringConstructorTest()
	{
		// Test case 1 - empty std::string
		{
			std::string empty;
			
			Text clvalue(empty);
			CIO_ASSERT_EQUAL(1u, clvalue.capacity());
			CIO_ASSERT_EQUAL((void *) Text::getEmptyText(), (void *) clvalue.data());
			CIO_ASSERT(!clvalue.allocator());

			Text rvalue(std::move(empty));
			CIO_ASSERT_EQUAL(1u, rvalue.capacity());
			CIO_ASSERT_EQUAL((void *)Text::getEmptyText(), (void *) rvalue.data());
			CIO_ASSERT(!rvalue.allocator());
		}

		// Test case 2 - non-empty std::string
		{
			std::string value("Test Value");
			std::size_t length = value.size() + 1;
			const char *data = value.data();

			Text clvalue(value);
			CIO_ASSERT_EQUAL(length, clvalue.capacity());
			CIO_ASSERT_DIFFERENT((void *) data, (void *) clvalue.data());
			CIO_ASSERT(clvalue.allocator());

			Text rvalue(std::move(value));
			CIO_ASSERT_EQUAL(length, rvalue.capacity());
			CIO_ASSERT_DIFFERENT((void *) data, (void *) rvalue.data());
			CIO_ASSERT(rvalue.allocator());
		}
	}	
	
	void TextTest::stringAssignmentTest()
	{
		// Test case 1 - empty std::string
		{
			std::string empty;

			Text clvalue;
			clvalue = empty;
			CIO_ASSERT_EQUAL(1u, clvalue.capacity());
			CIO_ASSERT_EQUAL((void *)Text::getEmptyText(), (void *)clvalue.data());
			CIO_ASSERT(!clvalue.allocator());

			Text rvalue;
			rvalue = std::move(empty);
			CIO_ASSERT_EQUAL(1u, rvalue.capacity());
			CIO_ASSERT_EQUAL((void *)Text::getEmptyText(), (void *)rvalue.data());
			CIO_ASSERT(!rvalue.allocator());
		}

		// Test case 2 - non-empty std::string
		{
			std::string value("Test Value");
			std::size_t length = value.size() + 1;
			const char *data = value.data();

			Text clvalue;
			clvalue = value;
			CIO_ASSERT_EQUAL(length, clvalue.capacity());
			CIO_ASSERT_DIFFERENT((void *)data, (void *)clvalue.data());
			CIO_ASSERT(clvalue.allocator());

			Text rvalue;
			rvalue = std::move(value);
			CIO_ASSERT_EQUAL(length, rvalue.capacity());
			CIO_ASSERT_DIFFERENT((void *)data, (void *)rvalue.data());
			CIO_ASSERT(rvalue.allocator());
		}
	}

	void TextTest::stringBindTest()
	{
		// Test case 1 - empty std::string
		{
			std::string empty;

			Text clvalue;
			clvalue.bind(empty);
			CIO_ASSERT_EQUAL(1u, clvalue.capacity());
			CIO_ASSERT_EQUAL((void *)Text::getEmptyText(), (void *)clvalue.data());
			CIO_ASSERT(!clvalue.allocator());

			Text rvalue;
			rvalue.bind(std::move(empty));
			CIO_ASSERT_EQUAL(1u, rvalue.capacity());
			CIO_ASSERT_EQUAL((void *)Text::getEmptyText(), (void *)rvalue.data());
			CIO_ASSERT(!rvalue.allocator());
		}

		// Test case 2 - non-empty std::string
		{
			std::string value("Test Value");
			std::size_t length = value.size() + 1;
			const char *data = value.data();

			Text clvalue;
			clvalue.bind(value);
			CIO_ASSERT_EQUAL(length, clvalue.capacity());
			CIO_ASSERT_EQUAL((void *)data, (void *)clvalue.data());
			CIO_ASSERT(!clvalue.allocator());

			Text rvalue;
			rvalue.bind(std::move(value));
			CIO_ASSERT_EQUAL(length, rvalue.capacity());
			CIO_ASSERT_DIFFERENT((void *)data, (void *)rvalue.data());
			CIO_ASSERT(rvalue.allocator());
		}
	}

	void TextTest::toUpperTest()
	{
		Text empty;
		Text emptyUpper = empty.toUpper();
		CIO_ASSERT_EQUAL(0u, emptyUpper.capacity());
		CIO_ASSERT(emptyUpper.data() == nullptr);

		Text null("");
		Text nullUpper = null.toUpper();
		CIO_ASSERT_EQUAL(1u, nullUpper.capacity());
		CIO_ASSERT_EQUAL((void *) null.data(), (void *) nullUpper.data());
		CIO_ASSERT(!nullUpper.allocator());

		Text allUpperInput("ABCDEF");
		Text allUpperActual = allUpperInput.toUpper();
		CIO_ASSERT_EQUAL(allUpperInput.data(), allUpperActual.data());
		CIO_ASSERT(!allUpperActual.allocator());

		Text allLowerInput("abcdef");
		Text allLowerActual = allLowerInput.toUpper();
		CIO_ASSERT_BYTES_EQUAL("ABCDEF", 7u, allLowerActual.data(), allLowerActual.capacity());
		CIO_ASSERT(allLowerInput.data() != allLowerActual.data());
		CIO_ASSERT(allLowerActual.allocator());

		Text mixedInput("a.Bc$De FgH");
		Text mixedActual = mixedInput.toUpper();
		CIO_ASSERT_BYTES_EQUAL("A.BC$DE FGH", 12u, mixedActual.data(), mixedActual.capacity());
		CIO_ASSERT(mixedInput.data() != mixedActual.data());
		CIO_ASSERT(mixedActual.allocator());

		Text mixedInput2("a.Bc$De fGh");
		Text mixedActual2 = mixedInput2.toUpper();
		CIO_ASSERT_BYTES_EQUAL("A.BC$DE FGH", 12u, mixedActual2.data(), mixedActual2.capacity());
		CIO_ASSERT(mixedInput2.data() != mixedActual2.data());
		CIO_ASSERT(mixedActual2.allocator());
	}

	void TextTest::toLowerTest()
	{
		Text empty;
		Text emptyUpper = empty.toLower();
		CIO_ASSERT_EQUAL(0u, emptyUpper.capacity());
		CIO_ASSERT(emptyUpper.data() == nullptr);

		Text null("");
		Text nullUpper = null.toLower();
		CIO_ASSERT_EQUAL(1u, nullUpper.capacity());
		CIO_ASSERT_EQUAL((void *)null.data(), (void *)nullUpper.data());
		CIO_ASSERT(!nullUpper.allocator());

		Text allUpperInput("ABCDEF");
		Text allUpperActual = allUpperInput.toLower();
		CIO_ASSERT_BYTES_EQUAL("abcdef", 7u, allUpperActual.data(), allUpperActual.capacity());
		CIO_ASSERT(allUpperInput.data() != allUpperActual.data());
		CIO_ASSERT(allUpperActual.allocator());

		Text allLowerInput("abcdef");
		Text allLowerActual = allLowerInput.toLower();
		CIO_ASSERT_EQUAL((void *) allLowerInput.data(), (void *) allLowerActual.data());
		CIO_ASSERT(!allLowerActual.allocator());

		Text mixedInput("a.Bc$De FgH");
		Text mixedActual = mixedInput.toLower();
		CIO_ASSERT_BYTES_EQUAL("a.bc$de fgh", 12u, mixedActual.data(), mixedActual.capacity());
		CIO_ASSERT(mixedInput.data() != mixedActual.data());
		CIO_ASSERT(mixedActual.allocator());

		Text mixedInput2("a.Bc$De fGh");
		Text mixedActual2 = mixedInput2.toLower();
		CIO_ASSERT_BYTES_EQUAL("a.bc$de fgh", 12u, mixedActual2.data(), mixedActual2.capacity());
		CIO_ASSERT(mixedInput2.data() != mixedActual2.data());
		CIO_ASSERT(mixedActual2.allocator());
	}
}

