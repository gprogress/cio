#######################################################################################################################
# Copyright 2020-2023, Geometric Progress LLC
#
# This file is part of the Careful I/O Library (CIO).
#
# CIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CIO.  If not, see <https://www.gnu.org/licenses/>
#######################################################################################################################
set(CMAKE_DEFAULT_STAGE_DIR "${CMAKE_BINARY_DIR}")

set(CMAKE_STAGE_DIR "${CMAKE_DEFAULT_STAGE_DIR}" CACHE PATH "Staging directory for intermediate build")
option(CMAKE_STAGE_IMPORTED_TARGETS "Whether to stage imported targets in the build directory" "${WIN32}")
option(CMAKE_INSTALL_IMPORTED_TARGETS "Whether to install imported targets in the final install step" "${WIN32}")

# Set default variables that affect targets
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_STAGE_DIR}/${CMAKE_INSTALL_BINDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_STAGE_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_STAGE_DIR}/${CMAKE_INSTALL_LIBDIR})

# Suppress per-configuration folders in generators like Visual Studio
foreach(_CONFIG ${CMAKE_CONFIGURATION_TYPES})	
	string(TOUPPER "${_CONFIG}" _config_upper)
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${_config_upper} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${_config_upper} "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}")
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${_config_upper} "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}")	
endforeach()

function(COPY_FOUND_FILE INPUT DESTINATION OUTPUT)
	file(MAKE_DIRECTORY "${OUTPUT}")
	if(NOT CMAKE_STAGE_QUIET)
		message(STATUS "Copying ${INPUT} to ${OUTPUT}")
	endif()
	configure_file("${INPUT}" "${OUTPUT}" COPYONLY)
endfunction()

function(STAGE_FILES_RUNTIME RELATIVE_PATH FILES)
	if(CMAKE_STAGE_IMPORTED_TARGETS)
		if("${RELATIVE_PATH}")
			set(_path "${CMAKE_STAGE_DIR}/${RELATIVE_PATH}")
		else()
			set(_path "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
		endif()	
	
		foreach(_file ${FILES} ${ARGN})
			COPY_FOUND_FILE("${_file}" DESTINATION "${_path}")
		endforeach()
	endif()
endfunction()

function(STAGE_AND_INSTALL_FILES_RUNTIME RELATIVE_PATH FILES)
	STAGE_FILES_RUNTIME("${RELATIVE_PATH}" ${FILES} ${ARGN})
	if(CMAKE_INSTALL_IMPORTED_TARGETS)
		install(FILES ${FILES} ${ARGN} DESTINATION "${CMAKE_INSTALL_BINDIR}/${RELATIVE_PATH}")
	endif()
endfunction()

function(STAGE_AND_INSTALL_RUNTIME TARGETS)
	STAGE_RUNTIME(${TARGETS} ${ARGN})
	INSTALL_RUNTIME(${TARGETS} ${ARGN})
endfunction()

function(STAGE_RUNTIME TARGETS)
	if(CMAKE_STAGE_IMPORTED_TARGETS)
# Step one - collect targets and see if we have a destination
		set(_targets )
		set(_dest )
		set(_current _targets)
		set(_optional )
		foreach(_target ${TARGETS} ${ARGN})
			if("${_target}" STREQUAL "DESTINATION")
				set(_current _dest)
			elseif("${_target}" STREQUAL "OPTIONAL")
				set(_optional OPTIONAL)
			else()
				list(APPEND ${_current} ${_target})
			endif()
		endforeach()

# Step two - copy to runtime dir
		foreach(_target ${_targets})
			STAGE_RUNTIME_TO_DIR(${_target} ${_dest} ${_optional})
		endforeach()
	endif()
endfunction()

function(INSTALL_RUNTIME TARGETS)
	if(CMAKE_INSTALL_IMPORTED_TARGETS)
# Step one - collect targets and see if we have a destination
		set(_targets )
		set(_dest )
		set(_current _targets)
		set(_optional )
		foreach(_target ${TARGETS} ${ARGN})
			if("${_target}" STREQUAL "DESTINATION")
				set(_current _dest)
			elseif("${_target}" STREQUAL "OPTIONAL")
				set(_optional OPTIONAL)
			else()
				list(APPEND ${_current} ${_target})
			endif()
		endforeach()

# Step two - actually install each target
		foreach(_target ${_targets})
			INSTALL_RUNTIME_TO_DIR(${_target} ${_dest} ${_optional})
		endforeach()
	endif()
endfunction()

# Function to copy DLLs and PDBs into build directory at configure time
# This is primarily useful on Windows to enable launching and debugging inside of MVSC
# But can be done on any platform
function(STAGE_RUNTIME_TO_DIR TARGET)
	if(TARGET ${TARGET})
		get_target_property(_DLL_PATH ${TARGET} IMPORTED_LOCATION)
		get_target_property(_DLL_PATH_RELEASE ${TARGET} IMPORTED_LOCATION_RELEASE)
		get_target_property(_DLL_PATH_DEBUG ${TARGET} IMPORTED_LOCATION_DEBUG)
		get_target_property(_PDB_PATH ${TARGET} IMPORTED_PDB)
		get_target_property(_PDB_PATH_RELEASE ${TARGET} IMPORTED_PDB_RELEASE)
		get_target_property(_PDB_PATH_DEBUG ${TARGET} IMPORTED_PDB_DEBUG)
		
		if(ARGN)
			get_filename_component(_dest ${ARGN} ABSOLUTE BASE_DIR ${CMAKE_STAGE_DIR})
			list(REMOVE_ITEM _dest OPTIONAL)
		endif()

		if (NOT _dest)
			set(_dest "${CMAKE_STAGE_DIR}/${CMAKE_INSTALL_BINDIR}")
		elseif(NOT IS_ABSOLUTE _dest)
			set(_dest ${CMAKE_STAGE_DIR}/${_dest})
		endif()
		
		if(CMAKE_CONFIGURATION_TYPES)
			foreach(_CONFIG ${CMAKE_CONFIGURATION_TYPES})
				string(TOUPPER "${_CONFIG}" _SUFFIX)
				if(_DLL_PATH_${_SUFFIX})
					COPY_FOUND_FILE("${_DLL_PATH_${_SUFFIX}}" DESTINATION "${_dest}" )
				elseif(_DLL_PATH)
					COPY_FOUND_FILE("${_DLL_PATH}" DESTINATION "${_dest}")
				endif()

				if(_PDB_PATH_${_SUFFIX})
					COPY_FOUND_FILE("${_PDB_PATH_${_SUFFIX}}" DESTINATION "${_dest}")
				elseif(_PDB_PATH)
					COPY_FOUND_FILE("${_PDB_PATH}" DESTINATION "${_dest}")
				endif()
			endforeach()
		else()
			string(TOUPPER "${CMAKE_BUILD_TYPE}" _SUFFIX)
			if(NOT _SUFFIX)
				set(_SUFFIX "RELEASE")
			endif()

			if(_DLL_PATH_${_SUFFIX})
				COPY_FOUND_FILE("${_DLL_PATH_${_SUFFIX}}" DESTINATION "${_dest}")
			elseif(_DLL_PATH)
				COPY_FOUND_FILE("${_DLL_PATH}" DESTINATION "${_dest}")
			endif()

			if(_PDB_PATH_${_SUFFIX})
				COPY_FOUND_FILE("${_PDB_PATH_${_SUFFIX}}" DESTINATION "${_dest}")
			elseif(_PDB_PATH)
				COPY_FOUND_FILE("${_PDB_PATH}" DESTINATION "${_dest}")
			endif()
		endif()
	else()
		set(_optional )
		foreach(_arg ${ARGN})
			if (_arg STREQUAL "OPTIONAL")
				set(_optional 1)
			endif()
		endforeach()
		if (NOT _optional)
			message(SEND_ERROR "Stage runtime failed: ${TARGET} does not exist")
		endif()
	endif()
endfunction()

# Function to copy DLLs and PDBs into install directory at install time
# This is primarily useful for deploying imported targets found by find_package
function(INSTALL_RUNTIME_TO_DIR TARGET)
	if(TARGET ${TARGET})
		set(_definition ${ARGN})
		list(REMOVE_ITEM _definition OPTIONAL)
		
		if(_definition)
			set(_libdir ${_definition})
			set(_bindir ${_definition})
		else()
			set(_libdir ${CMAKE_INSTALL_LIBDIR})
			set(_bindir ${CMAKE_INSTALL_BINDIR})
		endif()

		if(WIN32)
			get_target_property(_TYPE ${TARGET} TYPE)
			if("${_TYPE}" STREQUAL "STATIC_LIBRARY")
				get_target_property(_LIB_PATH ${TARGET} IMPORTED_LOCATION)
				get_target_property(_LIB_PATH_RELEASE ${TARGET} IMPORTED_LOCATION_RELEASE)
				get_target_property(_LIB_PATH_DEBUG ${TARGET} IMPORTED_LOCATION_DEBUG)
			else()
				get_target_property(_RUN_PATH ${TARGET} IMPORTED_LOCATION)
				get_target_property(_RUN_PATH_RELEASE ${TARGET} IMPORTED_LOCATION_RELEASE)
				get_target_property(_RUN_PATH_DEBUG ${TARGET} IMPORTED_LOCATION_DEBUG)
				
				get_target_property(_LIB_PATH ${TARGET} IMPORTED_IMPLIB)
				get_target_property(_LIB_PATH_RELEASE ${TARGET} IMPORTED_IMPLIB_RELEASE)
				get_target_property(_LIB_PATH_DEBUG ${TARGET} IMPORTED_IMPLIB_DEBUG)

				get_target_property(_PDB_PATH ${TARGET} IMPORTED_PDB)
				get_target_property(_PDB_PATH_RELEASE ${TARGET} IMPORTED_PDB_RELEASE)
				get_target_property(_PDB_PATH_DEBUG ${TARGET} IMPORTED_PDB_DEBUG)
			endif()
		else()
			get_target_property(_TYPE ${TARGET} TYPE)
			if("${_TYPE}" STREQUAL "SHARED_LIBRARY" OR "${_TYPE}" STREQUAL "STATIC_LIBRARY")
				get_target_property(_LIB_PATH ${TARGET} IMPORTED_LOCATION)
				get_target_property(_LIB_PATH_RELEASE ${TARGET} IMPORTED_LOCATION_RELEASE)
				get_target_property(_LIB_PATH_DEBUG ${TARGET} IMPORTED_LOCATION_DEBUG)
				
			elseif("${_TYPE}" STREQUAL "EXECUTABLE")
				get_target_property(_RUN_PATH ${TARGET} IMPORTED_LOCATION)
				get_target_property(_RUN_PATH_RELEASE ${TARGET} IMPORTED_LOCATION_RELEASE)
				get_target_property(_RUN_PATH_DEBUG ${TARGET} IMPORTED_LOCATION_DEBUG)
				
			endif()
		endif()

		# Multi-configuration version
		if(CMAKE_CONFIGURATION_TYPES)
			foreach(_CONFIG ${CMAKE_CONFIGURATION_TYPES})
				string(TOUPPER "${_CONFIG}" _SUFFIX)
				if(_RUN_PATH_${_SUFFIX})
					install(FILES "${_RUN_PATH_${_SUFFIX}}" DESTINATION "${_bindir}" CONFIGURATIONS ${_CONFIG})
				elseif(_RUN_PATH)
					install(FILES "${_RUN_PATH}" DESTINATION "${_bindir}"  CONFIGURATIONS ${_CONFIG})
				endif()

				if(_LIB_PATH_${_SUFFIX})
					install(FILES "${_LIB_PATH_${_SUFFIX}}" DESTINATION "${_libdir}"  CONFIGURATIONS ${_CONFIG})
				elseif(_LIB_PATH)
					install(FILES "${_LIB_PATH}" DESTINATION "${_libdir}"  CONFIGURATIONS ${_CONFIG})
				endif()

				if(_PDB_PATH_${_SUFFIX})
					install(FILES "${_RUN_PATH_${_SUFFIX}}" DESTINATION "${_bindir}" CONFIGURATIONS ${_CONFIG})
				elseif(_PDB_PATH)
					install(FILES "${_RUN_PATH}" DESTINATION "${_libdir}" CONFIGURATIONS ${_CONFIG})
				endif()
			endforeach()
		# Single-configuration version
		else()
			string(TOUPPER "${CMAKE_BUILD_TYPE}" _SUFFIX)
			if(NOT _SUFFIX)
				set(_SUFFIX "RELEASE")
			endif()

			if(_RUN_PATH_${_SUFFIX})
				install(FILES "${_RUN_PATH_${_SUFFIX}}" DESTINATION "${_bindir}")
			elseif(_RUN_PATH)
				install(FILES "${_RUN_PATH}" DESTINATION "${_bindir}")
			endif()
			
			if(_LIB_PATH_${_SUFFIX})
				install(FILES "${_LIB_PATH_${_SUFFIX}}" DESTINATION "${_libdir}")
			elseif(_LIB_PATH)
				install(FILES "${_LIB_PATH}" DESTINATION "${_libdir}")
			endif()

			if(_PDB_PATH_${_SUFFIX})
				install(FILES "${_PDB_PATH_${_SUFFIX}}" DESTINATION "${_bindir}")
			elseif(_PDB_PATH)
				install(FILES "${_PDB_PATH}" DESTINATION "${_bindir}")
			endif()
		endif()
	else()
		set(_optional )
		foreach(_arg ${ARGN})
			if (_arg STREQUAL "OPTIONAL")
				set(_optional 1)
			endif()
		endforeach()
		if (NOT _optional)
			message(SEND_ERROR "Install runtime failed: ${TARGET} does not exist")
		endif()
	endif()
endfunction()

function(FIND_SYSTEM_LIBRARIES OUTPUT_VAR)
	if(MINGW)
		if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
			find_program(LIBSTDCPP_PROGRAM "libstdc++-6.dll")
			find_program(LIBGCC_PROGRAM NAMES "libgcc_s_seh-1.dll" "libgcc_s_dw2-1.dll")
			find_program(LIBWINPTHREAD_PROGRAM "libwinpthread-1.dll")
			set("${OUTPUT_VAR}" "${LIBSTDCPP_PROGRAM}" "${LIBGCC_PROGRAM}" "${LIBWINPTHREAD_PROGRAM}" PARENT_SCOPE)
		elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
			find_program(LIBCPP_PROGRAM "libc++.dll")
			set("${OUTPUT_VAR}" "${LIBCPP_PROGRAM}" PARENT_SCOPE)
		endif()
	endif()
endfunction()

function(STAGE_SYSTEM_LIBRARIES)
	FIND_SYSTEM_LIBRARIES(SYSTEM_LIBS)
	if(SYSTEM_LIBS)
		STAGE_FILES_RUNTIME("" ${SYSTEM_LIBS})
	endif()
endfunction()

function(INSTALL_SYSTEM_LIBRARIES)
	FIND_SYSTEM_LIBRARIES(SYSTEM_LIBS)
	if(SYSTEM_LIBS AND CMAKE_INSTALL_IMPORTED_TARGETS)
		install(FILES ${SYSTEM_LIBS} ${ARGN} DESTINATION "${CMAKE_INSTALL_BINDIR}")
	endif()
endfunction()

function(STAGE_AND_INSTALL_SYSTEM_LIBRARIES)
	FIND_SYSTEM_LIBRARIES(SYSTEM_LIBS)
	if(SYSTEM_LIBS)
		STAGE_FILES_RUNTIME("" ${SYSTEM_LIBS})
		if(CMAKE_INSTALL_IMPORTED_TARGETS)
			install(FILES ${SYSTEM_LIBS} ${ARGN} DESTINATION "${CMAKE_INSTALL_BINDIR}")
		endif()
	endif()
endfunction()