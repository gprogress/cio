/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "LowEnergyEvent.h"

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sLowEnergyEventText[] = {
			"None",
			"Connection Completed",
			"Advertisement",
			"Connection Updated",
			"Remote Features Read",
			"Long Term Key Requested",
			"Parameter Requested",
			"Data Length Changed",
			"Local Public Key Read",
			"DHKey Generated",
			"Enhanced Connection Completed",
			"Direct Advertisement"
		};

		const char *print(LowEnergyEvent code) noexcept
		{
			const char *text = "Unknown";
			if (code <= LowEnergyEvent::DirectAdvertisement)
			{
				text = sLowEnergyEventText[static_cast<std::size_t>(code)];
			}

			return text;
		}

		std::ostream &operator<<(std::ostream &s, LowEnergyEvent type)
		{
			return s << print(type);
		}
	}
}