/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_DESCRIPTOR_H
#define CIO_BLUETOOTH_DESCRIPTOR_H

#include "Types.h"

#include "Attribute.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO Bluetooth Descriptor refines the GATT Attribute base class to implement concrete descriptors
		 * used to read and write actual data values.
		 *
		 * By definition descriptors are only a single descriptor handle rather than a range.
		 *
		 * Characteristic values and Characteristic declarations are the main use cases for descriptors.
		 */
		class CIO_BLUETOOTH_API Descriptor : public Attribute
		{
			public:		
				/**
				 * Construct the empty descriptor.
				 * The handle is initialized to 0x0000 and the type UUID is initialized to the empty UUID.
				 */
				Descriptor() noexcept;
				
				/**
				 * Construct the descriptor with the given handle.
				 * The type is initialized to the empty UUID.
				 *
				 * @param handle The handle
				 */
				explicit Descriptor(std::uint16_t handle) noexcept;
				
				/**
				 * Construct the descriptor with the given type.
				 * The handle is initialized to 0x0000
				 *
				 * @param type The type
				 */
				explicit Descriptor(const UniqueId &type) noexcept;
				
				/**
				 * Construct the descriptor with the given type and handle.
				 * The handle is initialized to the given handle.
				 *
				 * @param type The type
				 * @param handle The handle
				 */
				Descriptor(const UniqueId &type, std::uint16_t handle) noexcept;
				
				/**
				 * Construct the descriptor with the given type and handle with the given descriptor client.
				 * The handle is initialized to the given handle.
				 *
				 * @param type The type
				 * @param handle The handle
				 * @param client The attribute client
				 */
				Descriptor(const UniqueId &type, std::uint16_t handle, AttributeClient *client) noexcept;
				
				/**
				 * Construct a descriptor from a generic attribute.
				 *
				 * @param in The attribute
				 */
				explicit Descriptor(const Attribute &in);
				
				/**
				 * Construct a descriptor from a generic attribute.
				 *
				 * @param in The attribute
				 */
				explicit Descriptor(Attribute &&in) noexcept;
				
				/**
				 * Construct a copy of an descriptor.
				 * 
				 * @param in The descriptor to copy
				 */
				Descriptor(const Descriptor &in);
								
				/**
				 * Move constructs an descriptor.
				 * 
				 * @param in The descriptor to move
				 */
				Descriptor(Descriptor &&in) noexcept;
				
				/**
				 * Assign a descriptor from a generic attribute.
				 *
				 * @param in The attribute
				 * @return this descriptor
				 */
				Descriptor &operator=(const Attribute &in);
				
				/**
				 * Assign a descriptor from a generic attribute.
				 *
				 * @param in The attribute
				 * @return this descriptor
				 */
				Descriptor &operator=(Attribute &&in) noexcept;
				
				/**
				 * Copy an descriptor.
				 * 
				 * @param in The descriptor to copy
				 * @return this descriptor
				 */
				Descriptor &operator=(const Descriptor &in);
				
				/**
				 * Moves an descriptor.
				 * 
				 * @param in The descriptor to move
				 * @return this descriptor
				 */
				Descriptor &operator=(Descriptor &&in) noexcept;
				
				/**
				 * Destructor.
				 */
				virtual ~Descriptor() noexcept;

				/**
				 * Validates the current content of the descriptor, determining missing content and correcting any issues.
				 * For the Descriptor, in addition to the base descriptor checks, this also makes sure that the start and end handle are the same,
				 * and if they are different will set them to the lower of the two handle values.
				 */
				virtual void validate();
			
				// Read API - uses descriptor Client to perform operation
				
				/**
				  * Uses the associated BLE Client to read the value of the descriptor using a callback listener.
				  * The attribute handle must be set and the descriptor must be readable.
				  *
				  * @param listener The listener to receive read data
				  * @return the actual number of bytes read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				std::size_t read(const AttributeCallback &listener) const;
				
				/**
				  * Uses the associated BLE Client to read the value of the descriptor.
				  * The attribute handle must be set and the descriptor must be readable.
				  *
				  * @param data The data buffer to read the descriptor value into
				  * @param length The maximum length to read into the data buffer
				  * @return the actual number of bytes read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				std::size_t read(void *data, std::size_t length) const;
				
				/**
				  * Uses the associated BLE Client to read a single value of the descriptor as the given type T.
				  * The descriptor handle must be set and the descriptor must be readable.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to read a text value.
				  *
				  * @tparam <T> The data type of the value
				  * @return the value that was read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				template <typename T>
				T read() const;
				
				// Write API - uses LE Client to encode request and decode response
				
				/**
				  * Uses the associated BLE Client to write the value of the descriptor.
				  * The attribute handle must be set and the descriptor must be writable with response.
				  *
				  * @param data The data buffer containing the data to write to the descriptor
				  * @param length The length to write from the data buffer
				  * @return the actual number of bytes written
				  * @throw cio::Exception If there was no BLE Client or if the write request failed for any reason
				  */
				std::size_t write(const void *data, std::size_t length);
				
				/**
				  * Uses the associated BLE Client to write the value of the descriptor without waiting for confirmation.
				  * The attribute handle must be set and the descriptor must be writable without response.
				  *
				  * @param data The data buffer containing the data to write to the descriptor
				  * @param length The length to write from the data buffer
				  * @return the actual number of bytes submitted to be written
				  * @throw cio::Exception If there was no BLE Client or if the write command failed to be submitted
				  */
				std::size_t send(const void *data, std::size_t length);
				
				/**
				  * Uses the associated BLE Client to write the value of the descriptor from an input of type T.
				  * The attribute handle must be set and the descriptor must be writable with response.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to write a text value.
				  *
				  * @note This method will acquire a lock on the Client for thread safety, then
				  * use the transmit buffer to submit a write request to the L2CAP socket. After it will process
				  * received packets until it receives a write confirmation response.
				  *
				  * @warning this method is currently not capable of writing descriptors longer than the max packet size
				  *
				  * @tparam <T> The data type of the value
				  * @param value The value to write
				  * @throw cio::Exception If there was no BLE Client or if the write request failed for any reason
				  */
				template <typename T>
				void write(const T &value);
				
				/**
				  * Uses the associated BLE Client to write the value of the descriptor from an input of type T without waiting for confirmation.
				  * The attribute handle must be set and the descriptor must be writable without response.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to write a text value.
				  *
				  * @note This method will acquire a lock on the Client for thread safety, then
				  * use the transmit buffer to submit a write command to the L2CAP socket. It will not attempt to process
				  * any responses.
				  *
				  * @warning this method is currently not capable of writing descriptors longer than the max packet size
				  *
				  * @tparam <T> The data type of the value
				  * @param value The value to write
				  * @throw cio::Exception If there was no BLE Client or if the write command failed to be submitted
				  */
				template <typename T>
				void send(const T &value);
				
			private:
				/** The metaclass for this class */
				static Class<Descriptor> sMetaclass;
		};
	}
}

/* Inline implementation */
namespace cio
{
	namespace bluetooth
	{
		template <typename T>
		inline T Descriptor::read() const
		{
			T tmp = T();
			std::size_t actual = this->read(&tmp, sizeof(T));
			if (actual < sizeof(T))
			{
				throw cio::Exception(Reason::Underflow, "Not enough descriptor data for requested value type");
			}
			return tmp;
		}
		
		template <>
		CIO_BLUETOOTH_API std::string Descriptor::read<std::string>() const;
		
		template <>
		CIO_BLUETOOTH_API cio::Text Descriptor::read<cio::Text>() const;
		
		template <typename T>
		inline void Descriptor::write(const T &value)
		{
			std::size_t actual = this->write(&value, sizeof(T));
			if (actual < sizeof(T))
			{
				throw cio::Exception(Reason::Overflow, "Not enough descriptor data for requested value type");
			}
		}
		
		template <>
		CIO_BLUETOOTH_API void Descriptor::write<std::string>(const std::string &value);
					
		template <>
		CIO_BLUETOOTH_API void Descriptor::write<Text>(const Text &value);
			
		template <typename T>
		inline void Descriptor::send(const T &value)
		{
			std::size_t actual = this->send(&value, sizeof(T));
			if (actual < sizeof(T))
			{
				throw cio::Exception(Reason::Overflow, "Not enough descriptor data for requested value type");
			}
		}
		
		template <>
		CIO_BLUETOOTH_API void Descriptor::send<std::string>(const std::string &value);
					
		template <>
		CIO_BLUETOOTH_API void Descriptor::send<Text>(const Text &value);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

