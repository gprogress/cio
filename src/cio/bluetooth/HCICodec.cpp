/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "HCICodec.h"

#include "AddressType.h"
#include "Device.h"
#include "Event.h"
#include "LowEnergyEvent.h"
#include "Packet.h"
#include "Peripheral.h"

#include <cio/Buffer.h>
#include <cio/Order.h>
#include <cio/DeviceAddress.h>
#include <cio/Class.h>

#include <cstring>

namespace cio
{
	namespace bluetooth
	{
		Class<HCICodec> HCICodec::sMetaclass("cio::bluetooth::HCICodec");
		
		const Metaclass &HCICodec::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
		
// Windows native BTH_ADDR is just a 64-bit integer MAC
#if defined CIO_USE_WINSOCK2
		void HCICodec::toNativeAddress(const DeviceAddress &address, void *bdaddr) noexcept
		{
			std::uint64_t mac48little = Little::order(address.getMac());
			std::memcpy(bdaddr, &mac48little, 8);
		}

		DeviceAddress HCICodec::fromNativeAddress(const void *bdaddr) noexcept
		{
			std::uint64_t mac48little = 0;
			std::memcpy(&mac48little, bdaddr, 8);
			std::uint64_t mac48 = Little::order(mac48little);
			return DeviceAddress::fromMac(mac48);
		}

// Other platforms (e.g. Linux) use a little-endian six byte MAC
#else
		void HCICodec::toNativeAddress(const DeviceAddress &address, void *bdaddr) noexcept
		{
			std::memset(bdaddr, 0, 6);
			std::uint64_t mac48little = Little::order(address.getMac());
			std::memcpy(bdaddr, reinterpret_cast<const std::uint8_t *>(&mac48little), 6);
		}
		
		DeviceAddress HCICodec::fromNativeAddress(const void *bdaddr) noexcept
		{
			std::uint64_t mac48little = 0;
			std::memcpy(reinterpret_cast<std::uint8_t *>(&mac48little), bdaddr, 6);
			std::uint64_t mac48 = Little::order(mac48little);
			return DeviceAddress::fromMac(mac48);			
		}
#endif
		DeviceAddress HCICodec::decodeAddress(Buffer &packet) const
		{
			std::uint64_t mac48little = 0;
			packet.read(&mac48little, 6);
			std::uint64_t mac48 = Little::order(mac48little);
			return DeviceAddress::fromMac(mac48);	
		}
		
		Packet HCICodec::classifyPacket(const Buffer &packet) const noexcept
		{
			Packet type = Packet::None;
			if (packet.available())
			{
				type = static_cast<Packet>(*packet.current());
			}
			return type;
		}
		
		Packet HCICodec::decodePacketHeader(Buffer &packet) const
		{
			std::uint8_t type = packet.get();
			// TODO validate it is an expected type
			return static_cast<Packet>(type);
		}
		
		Event HCICodec::decodeEventHeader(Buffer &packet) const
		{
			Event e = Event::None;
			
			return e;
		}
		
		LowEnergyEvent HCICodec::decodeLowEnergyHeader(Buffer &packet) const
		{
			LowEnergyEvent e = LowEnergyEvent::None;
			
			return e;
		}
		
		std::size_t HCICodec::decodeAdvertisementHeader(Buffer &packet) const
		{
			std::size_t length = 0;
			
			return length;
		}
		
		Peripheral HCICodec::decodeNextAdvertisement(Buffer &packet) const
		{
			Peripheral ad;

			// Okay, we have advertising info, let's use it!
			std::uint8_t adType = packet.get();
		    	sMetaclass.info() << "BLE Advertising data type: " << static_cast<unsigned>(adType);
			
			std::uint8_t addrType = packet.get();
			sMetaclass.info() << "Remote address type: " << static_cast<unsigned>(addrType);			
			ad.setAddressType(addrType == 0 ? AddressType::Public : AddressType::Random);

			ad.setAddress(this->decodeAddress(packet));
			sMetaclass.info() << "Found nearby BLE device " <<  ad.getAddress();
			
			std::uint8_t reportLength = packet.get();
			
			cio::Buffer shenanigans(packet.current(), reportLength, nullptr);
			packet.skip(reportLength);

			while (shenanigans.remaining())
			{
				std::uint8_t nextLength = shenanigans.get();
				std::uint8_t nextType = shenanigans.get();

				switch (nextType)
				{
					case 0x01:
					{
						std::uint8_t flags = shenanigans.get();
						if (nextLength > 2)
						{
							shenanigans.skip(nextLength - 2);
						}
						sMetaclass.info() << "BLE AD: Flags = " << static_cast<unsigned>(flags);
						break;
					}
						
					case 0x08:
					{
						ad.setShortName(reinterpret_cast<const char *>(shenanigans.current()), nextLength - 2);
						shenanigans.skip(nextLength - 2);
						sMetaclass.info() << "BLE AD: Partial Name = " << ad.getShortName();	
						break;
					}
						
					case 0x09:
					{
	
						ad.setShortName(reinterpret_cast<const char *>(shenanigans.current()), nextLength - 2);
						shenanigans.skip(nextLength - 2);					  		
						sMetaclass.info() << "BLE AD: Full Name = " << ad.getShortName();
						break;
					}
					
					case 0x12:
					{
						ad.setMinConnectionInterval(shenanigans.getLittle<std::uint16_t>());
						ad.setMaxConnectionInterval(shenanigans.getLittle<std::uint16_t>());
						if (nextLength > 4)
						{
							shenanigans.skip(nextLength - 4);
						}
						
						sMetaclass.info() << "BLE AD: Peripheral Connection Interval Range " << ad.getMinConnectionInterval() << " : " << ad.getMaxConnectionInterval();
						break;
					}

					default:
					{
						sMetaclass.info() << "BLE AD: " << static_cast<unsigned>(nextType) << " with " << (nextLength - 1) << " data bytes";
						shenanigans.skip(nextLength - 1);
						break;
					}
				}
			}	
					
			return ad;		
		}
		
		std::size_t HCICodec::getMaxDeviceCount() const noexcept
		{
			// TODO determine if this is actually a real limitation or just BlueZ being silly
			return 16;
		}
		
		Buffer &HCICodec::reserveLocalDeviceList(Buffer &buffer) const
		{
			return this->reserveLocalDeviceList(buffer, this->getMaxDeviceCount());
		}
		
		Buffer &HCICodec::reserveLocalDeviceList(Buffer &buffer, std::size_t count) const
		{		
			std::uint16_t allocated = static_cast<std::uint16_t>(std::min<std::size_t>(65535, count));
		
			// 2 byte header for count, plus 6 bytes per device
			buffer.reserve(2 + 6 * allocated);
			buffer.reset();
			std::memcpy(buffer.current(), &allocated, 2); // initialize to maximum count
			
			return buffer;
		}
		
		std::size_t HCICodec::decodeDeviceListHeader(Buffer &buffer) const
		{
			return buffer.get<std::uint16_t>();
		}
				
		Device HCICodec::decodeNextDevice(Buffer &buffer) const
		{
			Device device;
			
			std::uint16_t index = buffer.get<std::uint16_t>();
			std::uint32_t opts = buffer.get<std::uint32_t>();
			device.setIndex(index);
			
			return device;	
		}		
	}
}
