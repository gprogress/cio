/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_RESPONSE_H
#define CIO_RESPONSE_H

#include "Types.h"

#include "State.h"
#include "Metatypes.h"

namespace cio
{
	/**
	 * The Response template provides a representation of a response of some operation that may incude a value and the status of the operation.
	 *
	 * While the status can be used in any way desired, typically the following conventions are used:
	 * <ul>
	 * <li>None = value is not requested from remote resource and is unknown</li>
	 * <li>Requested = value is requested from remote resource and is unknown</li>
	 * <li>Submitted = value is set locally and requested to be synced to remove resource</li>
	 * <li>Read = value has been read from remote source</li>
	 * <li>Written = value has been modified locally and written to the remote resource</li>
	 * <li>Removed = value has been removed from the remote source</li>
	 * </ul>
	 *
	 * Generally any other success status should be treated the same as Written.
	 *
	 * The value type is left uninitialized on default construction.
	 * If the type T supports efficient move construction or assignment, this object will too.
	 *
	 * @tparam <T> The value type,
	 */
	template <typename T>
	class Response : public State
	{
		public:
			/** The response value */
			T value;

			/**
			 * Construct an empty Response object.
			 * The value is default constructed.
			 */
			inline Response() noexcept;

			/**
			 * Construct a Response object with the given state.
			 * The value is default constructed.
			 *
			 * @param state The state
			 */
			inline Response(State state) noexcept;

			/**
			 * Construct a Response object with the given value.
			 * The state is initialized to Requested.
			 *
			 * @param status The status
			 * @param count The number of objects (typically bytes) Response
			 */
			inline Response(T value) noexcept;

			/**
			 * Construct a Response object with the given value and state.
			 *
			 * @param state The state
			 * @param count The number of objects (typically bytes) Response
			 */
			inline Response(State status, T value) noexcept;
			
			/**
			 * Construct a copy of a Response object.
			 *
			 * @param in The Response object to copy
			 */
			inline Response(const Response<T> &in);
			
			/**
			 * Move constructor.
			 *
			 * @param in The object to move
			 */
			inline Response(Response<T> &&in) noexcept;

			/**
			 * Construct a copy of a Response object.
			 *
			 * @tparam <U> The other Response object's numeric type
			 * @param in The Response object to copy
			 */
			template <typename U>
			inline Response(const Response<U> &in);

			/**
			 * Copy a Response object.
			 *
			 * @param in The Response object to copy
			 * @return this Response object
			 */
			inline Response<T> &operator=(const Response<T> &in);
			
			/**
			 * Move a Response object.
			 *
			 * @param in The Response object to copy
			 * @return this Response object
			 */
			inline Response<T> &operator=(Response<T> &&in) noexcept;

			/**
			 * Copy a Response object.
			 *
			 * @tparam <U> The other Response object's numeric type
			 * @param in The Response object to copy
			 * @return this Response object
			 */
			template <typename U>
			inline Response<T> &operator=(const Response<U> &in);
			
			/**
			 * Assign to a response value.
			 * Requests a new write if the new value is different from the current value or the current value
			 * is not synchronized with the remote value. Sets the completed action to write if the new value is the same
			 * as the known read remote value.
			 *
			 * @param value the value
			 * @return this response
			 */
			inline Response<T> &operator=(T value) noexcept;

			/**
			 * Receives a value from remote resource.
			 * This sets the status to completed and the Action to Read if
			 * the value has not been locally overridden.
			 *
			 * @param value The value
			 * @return the status after this update
			 */
			inline Status receive(const T &value);

			/**
			 * Receives a value from remote resource.
			 * This sets the status to completed and the Action to Read if
			 * the value has not been locally overridden.
			 *
			 * @param value The value
			 * @return the status after this update
			 */
			inline Status receive(T &&value) noexcept;

			/**
			 * Submits a moved value to be applied to the remote resource.
			 * Requests a new write if the new value is different from the current value or the current value
			 * is not synchronized with the remote value. Sets the completed action to write if the new value is the same
			 * as the known read remote value.
			 *
			 * @param value The value
			 * @return the status after this update
			 */
			inline Status submit(const T &value);

			/**
			 * Submits a moved value to be applied to the remote resource.
			 * Requests a new write if the new value is different from the current value or the current value
			 * is not synchronized with the remote value. Sets the completed action to write if the new value is the same
			 * as the known read remote value.
			 *
			 * @param value The value
			 * @return the status after this update
			 */
			inline Status submit(T &&value) noexcept;
			
			/**
			 * Interprets the Response object in a Boolean context.
			 *
			 * @return whether the status indicates the object is known
			 */
			inline explicit operator bool() const noexcept;
			
			/**
			 * Uses the Response value automatically when expected.
			 *
			 * @return the response value
			 */
			inline operator T &() noexcept;
			
			/**
			 * Uses the Response value automatically when expected.
			 *
			 * @return the response value
			 */
			inline operator const T &() const noexcept;
			
			/**
			 * Casts a Response object to a different value type.
			 *
			 * @tparam <U> The desired value type
			 * @return the converted Response object
			 */
			template <typename U>
			inline explicit operator Response<U>() const;

			/**
			 * Clears the Response object.
			 */
			inline void clear() noexcept;
			
			/**
			 * Checks whether the value's status is considered known.
			 * This is true if the status is synchronized with the remote resource or
			 * the Action is Write.
			 *
			 * @return whether the value is known
			 */
			inline bool known() const noexcept;
			
			/**
			 * Checks whether the value's status is considered synchronized with the remote resource.
			 * This is true if the state has succeeded.
			 *
			 * @return whether the sync is complete
			 */
			inline bool synced() const noexcept;
			
			/**
			 * Checks whether the value's status is considered to be locally overridden.
			 * This is true if the Action is Write.
			 *
			 * @return whether the status is considered locally overridden
			 */
			inline bool overridden() const noexcept;
			
			/**
			 * Updates the status to remove synchronization.
			 * This downgrades status to Requested if it is in a higher state.
			 *
			 * @return the status resulting from this operation
			 */
			inline Status unsync() noexcept;
	};
}

/* Inline implementation */

namespace cio
{
	// General template for Response<T>
	
	template <typename T>
	inline Response<T>::Response() noexcept :
		value()
	{
		// nothing more to do
	}

	template <typename T>
	inline Response<T>::Response(State state) noexcept :
		State(state),
		value()
	{
		// nothing more to do
	}

	template <typename T>
	inline Response<T>::Response(T value) noexcept :
		State(Status::Requested),
		value(std::move(value))
	{
		// nothing more to do
	}

	template <typename T>
	inline Response<T>::Response(State state, T value) noexcept :
		State(state),
		value(std::move(value))
	{
		// nothing more to do
	}
	
	template <typename T>
	inline Response<T>::Response(const Response<T> &in) = default;
	
	template <typename T>
	inline Response<T>::Response(Response<T> &&in) noexcept :
		State(in),
		value(std::move(in.value))
	{
		// nothing more to do
	}
	
	template <typename T>
	template <typename U>
	inline Response<T>::Response(const Response<U> &in) :
		State(in),
		value(in.value)
	{
		// nothing more to do
	}

	template <typename T>
	inline Response<T> &Response<T>::operator=(const Response<T> &in) = default;
	
	template <typename T>
	inline Response<T> &Response<T>::operator=(Response<T> &&in) noexcept
	{
		State::operator=(in);
		this->value = std::move(in.value);
		return *this;
	}
	
	template <typename T>
	template <typename U>
	inline Response<T> &Response<T>::operator=(const Response<U> &in)
	{
		State::operator=(in);
		this->value = in.value;
		return *this;
	}

			
	template <typename T>		
	inline Response<T> &Response<T>::operator=(T value) noexcept
	{
		this->submit(std::move(value));
		return *this;
	}
	
	template <typename T>
	inline Status Response<T>::receive(const T &value)
	{
		if (!this->overridden())
		{
			this->value = value;
			this->action = Action::Read;
			this->status = Status::Completed;
			this->outcome = Outcome::Succeeded;
			this->reason = Reason::None;
		}
		
		return this->status;
	}
	
	template <typename T>
	inline Status Response<T>::receive(T &&value) noexcept
	{
		if (!this->overridden())
		{
			this->value = std::move(value);
			this->action = Action::Read;
			this->status = Status::Completed;
			this->outcome = Outcome::Succeeded;
			this->reason = Reason::None;
		}
		
		return this->status;
	}
	
	template <typename T>
	inline Status Response<T>::submit(const T &value)
	{
		if (this->value != value)
		{
			this->value = value;
			this->request(Action::Write);
		}
		else if (this->status < Status::Requested)
		{
			this->request(Action::Write);
		}
		else if (this->status == Status::Completed)
		{
			this->action = Action::Write;
		}
		
		return this->status;
	}
	
	template <typename T>
	inline Status Response<T>::submit(T &&value) noexcept
	{
		if (this->value != value)
		{
			this->value = std::move(value);
			this->request(Action::Write);
		}
		else if (this->status < Status::Requested)
		{
			this->request(Action::Write);
		}
		else if (this->status == Status::Completed)
		{
			this->action = Action::Write;
		}
		
		return this->status;
	}
	
	template <typename T>
	inline Response<T>::operator T &() noexcept
	{
		return this->value;
	}
			
	template <typename T>
	inline Response<T>::operator const T &() const noexcept
	{
		return this->value;
	}
			
	template <typename T>
	inline Response<T>::operator bool() const noexcept
	{
		return this->succeeded() || this->action == Action::Write;
	}

	template <typename T>
	template <typename U>
	inline Response<T>::operator Response<U>() const
	{
		return Response<U>(static_cast<const State &>(*this), static_cast<U>(this->value));
	}

	template <typename T>
	inline void Response<T>::clear() noexcept
	{
		State::clear();
		this->value = T();
	}
				
	template <typename T>
	inline bool Response<T>::known() const noexcept
	{
		return this->succeeded() || this->action == Action::Write;
	}
	
	template <typename T>
	inline bool Response<T>::synced() const noexcept
	{
		return this->succeeded();
	}
	
	template <typename T>
	inline bool Response<T>::overridden() const noexcept
	{
		return this->action == Action::Write;
	}
	
	template <typename T>
	inline Status Response<T>::unsync() noexcept
	{
		if (this->status > Status::Requested)
		{
			this->status = Status::Requested;
			this->outcome = Outcome::None;
			this->reason = Reason::None;
		}
		
		return this->status;
	}
}



#endif
