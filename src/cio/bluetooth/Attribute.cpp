/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Attribute.h"

#include "AttributeClient.h"
#include "TypeDictionary.h"
#include "TypeId.h"

#include <cio/Class.h>

#include <algorithm>

namespace cio
{
	namespace bluetooth
	{
		Class<Attribute> Attribute::sMetaclass("cio::bluetooth::Attribute");
	
		Attribute::Attribute() noexcept :
			mClient(nullptr),
			mStartHandle(0),
			mEndHandle(0),
			mDiscovered(false),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
		
		Attribute::Attribute(std::uint16_t handle) noexcept :
			mClient(nullptr),
			mStartHandle(handle),
			mEndHandle(handle),
			mDiscovered(false),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
		
		Attribute::Attribute(std::uint16_t start, std::uint16_t end) noexcept :
			mClient(nullptr),
			mStartHandle(start),
			mEndHandle(end),
			mDiscovered(false),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
		
		Attribute::Attribute(const UniqueId &type) noexcept :
			mType(type),
			mClient(nullptr),
			mStartHandle(0),
			mEndHandle(0),
			mDiscovered(false),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
				
		Attribute::Attribute(const UniqueId &type, std::uint16_t handle) noexcept :
			mType(type),
			mClient(nullptr),
			mStartHandle(handle),
			mEndHandle(handle),
			mDiscovered(type && handle),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
		
		Attribute::Attribute(const UniqueId &type, std::uint16_t start, std::uint16_t end) noexcept :
			mType(type),
			mClient(nullptr),
			mStartHandle(start),
			mEndHandle(end),
			mDiscovered(type && start),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
				
		Attribute::Attribute(const UniqueId &type, std::uint16_t handle, AttributeClient *client) noexcept :
			mType(type),
			mClient(client),
			mStartHandle(handle),
			mEndHandle(handle),
			mDiscovered(type && handle),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
		
		Attribute::Attribute(const UniqueId &type, std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept :
			mType(type),
			mClient(client),
			mStartHandle(start),
			mEndHandle(end),
			mDiscovered(type && start),
			mContentDiscovered(false)
		{
			// nothing more to do
		}
			
		Attribute::Attribute(Attribute &&in) noexcept :
			mType(in.mType),
			mClient(in.mClient),		
			mStartHandle(in.mStartHandle),
			mEndHandle(in.mEndHandle),
			mDiscovered(in.mDiscovered),
			mContentDiscovered(in.mContentDiscovered)
		{
			// nothing more to do
		}
				
		Attribute::Attribute(const Attribute &in) = default;
			
		Attribute &Attribute::operator=(Attribute &&in) noexcept
		{
			mClient = in.mClient;
			mType = in.mType;
			mStartHandle = in.mStartHandle;
			mEndHandle = in.mEndHandle;
			mDiscovered = in.mDiscovered;
			mContentDiscovered = in.mContentDiscovered;
			return *this;
		}
			
		Attribute &Attribute::operator=(const Attribute &in) = default;
			
		Attribute::~Attribute() noexcept = default;
				
		void Attribute::clear() noexcept
		{
			mClient = nullptr;
			mType.clear();
			mStartHandle = 0;
			mEndHandle = 0;	
			mDiscovered = false;
			mContentDiscovered = false;
		}
						
		void Attribute::validate()
		{
			if (mStartHandle && mEndHandle && mStartHandle > mEndHandle)
			{
				std::swap(mStartHandle, mEndHandle);
			}
		}	
		
		State Attribute::synchronize()
		{
			State e = this->synchronizeTypeHandle();
			mContentDiscovered = true; // no content in base class
			return e;
		}				
			
		State Attribute::synchronizeTypeHandle()
		{
			State e;
			if (!mDiscovered)
			{
				if (mClient)
				{
					if (!mType && mStartHandle)
					{
						mType = mClient->getAttributeType(mStartHandle);
					}
					else if (mType && !mStartHandle)
					{
						mStartHandle = mClient->findAttributeHandle(mType);
					}
					
					mDiscovered = true;
					e.succeed();
				}
				else
				{
					e.fail(Reason::Unopened);
				}
			}
			else
			{
				e.succeed(Reason::Exists);
			}
			
			return e;
		}
				
		void Attribute::setClient(AttributeClient *Client) noexcept
		{
			mClient = Client;
		}								
		
		const UniqueId &Attribute::getType() const noexcept
		{
			return mType;
		}
	
		void Attribute::setType(const UniqueId &id) noexcept
		{
			mType = id;
			mDiscovered = (mStartHandle && mType);
		}
		
		Text Attribute::getName() const noexcept
		{
			Text result("Unknown");
			if (mClient)
			{
				result = mClient->getTypes().name(mType);
			}
			return result;
		}
				
		const UniqueId &Attribute::setTypeByName(const Text &name)
		{
			if (!mClient)
			{
				throw Exception(Reason::Unopened, "Can't set attribute type by name: no BLE Client");
			}
			
			mType = mClient->getTypes().id(name);
			mDiscovered = (mStartHandle && mType);
			
			if (!mType)
			{
				throw Exception(Reason::Invalid, "Can't set attribute type by name: no type matched the given name");
			}
			
			return mType;
		}
						
		std::uint16_t Attribute::getHandle() const noexcept
		{
			return mStartHandle;
		}
				
		void Attribute::setHandle(std::uint16_t handle) noexcept
		{
			mStartHandle = handle;
			mEndHandle = handle;
			
			mDiscovered = (mStartHandle && mType);
		}
		
		std::uint16_t Attribute::getStartHandle() const noexcept
		{
			return mStartHandle;
		}
				
		void Attribute::setStartHandle(std::uint16_t handle) noexcept
		{
			mStartHandle = handle;
			mDiscovered = (mStartHandle && mType);
		}
		
		std::uint16_t Attribute::getEndHandle() const noexcept
		{
			return mEndHandle;
		}
				
		void Attribute::setEndHandle(std::uint16_t handle) noexcept
		{
			mEndHandle = handle;
		}

		void Attribute::setHandleRange(std::uint16_t start, std::uint16_t end) noexcept
		{
			mStartHandle = start;
			mEndHandle = end;
		}
		
		bool Attribute::containsHandle(std::uint16_t handle) const noexcept
		{
			return mStartHandle <= handle && handle <= mEndHandle;
		}
		
		bool Attribute::intersectsHandleRange(std::uint16_t start, std::uint16_t end) const noexcept
		{
			return start <= mEndHandle && end >= mStartHandle;
		}
				
		bool Attribute::containsHandleRange(std::uint16_t start, std::uint16_t end) const noexcept
		{
			return start >= mStartHandle && end <= mEndHandle;
		}
		
		bool Attribute::isHandleContained(std::uint16_t start, std::uint16_t end) const noexcept
		{
			return start <= mStartHandle && mStartHandle <= end;
		}
		
		void Attribute::clearHandleRange() noexcept
		{
			mStartHandle = 0;
			mEndHandle = 0;
		}
	
		AttributeClient *Attribute::getClient() noexcept
		{
			return mClient;
		}
				
		const AttributeClient *Attribute::getClient() const noexcept
		{
			return mClient;
		}
				
		bool Attribute::isDiscoveryComplete() const noexcept
		{
			return mDiscovered && mContentDiscovered;
		}
		
		void Attribute::setDiscoveryComplete(bool value) noexcept
		{
			mDiscovered = value;
			mContentDiscovered = value;
		}
				
		bool Attribute::isBaseDiscoveryComplete() const noexcept
		{
			return mDiscovered;
		}
		
		void Attribute::setBaseDiscoveryComplete(bool value) noexcept
		{
			mDiscovered = value;
		}
		
		bool Attribute::isContentDiscoveryComplete() const noexcept
		{
			return mContentDiscovered;
		}
		
		void Attribute::setContentDiscoveryComplete(bool value) noexcept
		{
			mContentDiscovered = value;
		}
	
		Attribute::operator bool() const noexcept
		{
			return mStartHandle != 0;
		}
		
		bool operator==(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() == right.getHandle();
		}
		
		bool operator!=(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() != right.getHandle();
		}
		
		bool operator<(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() < right.getHandle();
		}
		
		bool operator<=(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() <= right.getHandle();
		}
		
		bool operator>(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() > right.getHandle();
		}
		
		bool operator>=(const Attribute &left, const Attribute &right) noexcept
		{
			return left.getHandle() >= right.getHandle();
		}
	}
}

