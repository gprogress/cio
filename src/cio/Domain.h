/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_DOMAIN_H
#define CIO_DOMAIN_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Domain enumeration describes the numeric domain for a particular encoding in the sense of whether
	 * it is real, imaginary, or complex. It is an overlay on a bit flag for independently specifying real and each imaginary component.
	 * 
	 * Currently this only supports the first imaginary number i.
	 */
	enum class Domain : std::uint8_t
	{
		/** No numeric domain is specified */
		None = 0,
		
		/** The value is part of the real number domain. */
		Real = 1,
		
		/** The value is part of the imaginary domain for imaginary number i. */
		Imaginary = 2,
		
		/** The value is part of the complex number domain with both a real and imaginary component. */
		Complex = 3,
	};
	
	/**
	 * Gets the label representing a numeric domain type.
	 *
	 * @param type The type
	 * @return the label
	 */
	CIO_API const char *print(Domain type) noexcept;
	
	/**
	 * Prints the label representing a numeric domain type to a C++ stream.
	 *
	 * @param s The C++ stream
	 * @param type The signedness type
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Domain type);
}

#endif

