/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_METATYPES_H
#define CIO_METATYPES_H

#include "Types.h"

#include <cstring>

// Forward declarations and aliases
namespace cio
{
	// Types for identifying physical values and propagating reference / pointer / const / volatile qualifiers

	/**
	 * The ValueType template obtains an actual copyable value for a given input type.
	 * In general, this is equivalent to std::decay<T>::type to remove references, const, and so on.
	 * Specializations exist to present void as a single byte and to get the underlying "physical" value for delegate classes.
	 *
	 * @tparam <T> The type of object to use as an array element
	 */
	template <typename T> struct ValueType;

	/**
	 * The Value template alias obtains an actual copyable value for a given input type.
	 * In general, this is equivalent to std::decay<T>::type to remove references, const, and so on.
	 * Specializations exist to present void as a single byte.
	 *
	 * @tparam <T> The type of object to use as a value
	 */
	template <typename T> using Value = typename ValueType<T>::type;

	// ValueType specializations

	template <typename T>
	struct ValueType
	{
		/** The actual value type, which for non-void types is the provided type T without references or const/volatile */
		using type = typename std::decay<T>::type;
	};

	template <>
	struct ValueType<void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = std::uint8_t;
	};

	template <>
	struct ValueType<const void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = std::uint8_t;
	};


	/**
	 * The ValuePointerType template obtains a dereferenceable pointer for a given input type.
	 * In general, this is equivalent to just a pointer to T.
	 * For void and const void, this instead obtains a pointer to std::uint_t and const std::uint_t respectively.
	 * Unlike ValueType, it preserves const and volatile qualifiers.
	 *
	 * @tparam <T> The type of object to use as an array element
	 */
	template <typename T> struct ValuePointerType;

	/**
	 * The Value template alias is a dereferenceable pointer for a given input type.
	 * In general, this is equivalent to just a pointer to T.
	 * For void and const void, this instead obtains a pointer to std::uint_t and const std::uint_t respectively.
	 * Unlike Value, it preserves const and volatile qualifiers.
	 *
	 * @tparam <T> The type of object to use as an array element
	 */
	template <typename T> using ValuePointer = typename ValuePointerType<T>::type;

	/**
	 * The CopyConstType template obtains a type that has the same value semantics as T but with the constness of U.
	 * This can be used to propagate const through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the constness
	 */
	template <typename T, typename U> struct CopyConstType;
	
	/**
	 * The CopyConst template alias is a type that has the same value semantics as T but with the constness of U.
	 * This can be used to propagate const through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the constness
	 */
	template <typename T, typename U>
	using CopyConst = typename CopyConstType<T, U>::type;
	
	/**
	 * The CopyVolatileType template obtains a type that has the same value semantics as T but with the volatility of U.
	 * This can be used to propagate volatile through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the volatility
	 */
	template <typename T, typename U> struct CopyVolatileType;
	
	/**
	 * The CopyVolatile template alias is a type that has the same value semantics as T but with the volatility of U.
	 * This can be used to propagate volatile through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the volatility
	 */
	template <typename T, typename U>
	using CopyVolatile = typename CopyVolatileType<T, U>::type;
	
	/**
	 * The CopyConstVolatileType template obtains a type that has the same value semantics as T but with the constness and volatility (CV) of U.
	 * This can be used to propagate const and volatile through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the constness and volatility
	 */
	template <typename T, typename U> struct CopyConstVolatileType;
	
	/**
	 * The CopyConstVolatile template alias is a type that has the same value semantics as T but with the constness and volatility (CV) of U.
	 * This can be used to propagate const and volatile through type conversions.
	 *
	 * @tparam <T> The type of object that provides the value type
	 * @tparam <U> The type of object that provides the constness and volatility
	 */
	template <typename T, typename U>
	using CopyConstVolatile = typename CopyConstVolatileType<T, U>::type;

	/**
	  * The ReferenceType template obtains a reference value for a given input type.
	  * In general, this is equivalent to adding a reference to T while preserving const, volatile, etc.
	  *
	  * @tparam <T> The type of object to use as an array element
	  */
	template <typename T> struct ReferenceType;

	/**
	 * The Reference template alias obtains a reference value for a given input type.
	 * In general, this is equivalent to just adding a reference to the type.
	 * Specializations exist to present void as a single byte reference and to pass through existing reference types as-is.
	 *
	 * @tparam <T> The type of object to use as an array element
	 */
	template <typename T> using Reference = typename ReferenceType<T>::type;

	/**
	 * The LogicalBitsType template obtains a compile-time integer type used to represent the number of actually meaningful bits
	 * in a particular primitive data type. For most types, this is the same as the physical bit size obtained from SizeofBits.
	 * As a special case, the logical bit size for bool is 1 (since it can only meaningfully use 1 bit regardless of physical layout) and the logical bit size for void is 8.
	 *
	 * @tparam <T> The type of object of interest
	 */
	template <typename T>
	struct LogicalBitsType;

	/**
	 * The LogicalBits template alias obtains a compile-time integer type used to represent sizes of the given type.
	 * The given type is always an instantiation of std::integral_constant.
	 * As a special case, the logical bit size for bool is 1 (since it can only meaningfully use 1 bit regardless of physical layout) and the logical bit size for void is 8.
	 *
	 * @tparam <T> The type of object of interest
	 */
	template <typename T>
	using LogicalBits = typename LogicalBitsType<T>::type;

	/**
	 * The ElementType template is a selector to get the value type that results applying the index operator[] for the argument type L.
	 * In general this is just performed by taking the declared type of such an operation.
	 * However, this can be specialized for types that do not provide that particular operator, and in particular is specialized for void * to present it as a byte array.
	 *
	 * In most cases this will be a reference type, but is not required to be.
	 * T may be const to get the const element type.
	 *
	 * @tparam <T> The data type to use for indexing operation
	 * @tparam <L> The data type to use for the index argument, by default std::size_t
	 */
	template <typename T, typename L = std::size_t>
	struct ElementType;

	/**
	 * The Element template alias is the value type that results applying the index operator[] for the argument type L.
	 * In general this is just performed by taking the declared type of such an operation.
	 * However, this can be specialized for types that do not provide that particular operator, and in particular is specialized for void * to present it as a byte array.
	 *
	 * In most cases this will be a reference type, but is not required to be.
	 * T may be const to get the const element type.
	 *
	 * @tparam <T> The data type to use for indexing operation
	 * @tparam <L> The data type to use for the index argument, by default std::size_t
	 */
	template <typename T, typename L = std::size_t>
	using Element = typename ElementType<T, L>::type;

	/**
	 * The BoolType template is a selector for the data type of the given number of N bytes.
	 * For N = 1, this is bool. For all other values, it is the same as the unsigned type.
	 */
	template <unsigned N> struct BoolType;

	/**
	 * The Bool template alias for the Boolean data type of the given number of N bytes.
	 * For N = 1, this is bool. For all other values, it is the same as the unsigned type.
	 */
	template <unsigned N> using Bool = typename BoolType<N>::type;

	/**
	 * The Matching Bool template alias is a selector to choose the standard Boolean type that has the same size as the given type.
	 * For 1-byte types, this will match up to bool. For all other types, this will match up to the unsigned integer type.
	 *
	 * @tparam <T> The data type to use to locate a signed integer type of equal size
	 */
	template <typename T>
	using MatchingBool = Bool<sizeof(T)>;

	/**
	 * The IntType template is a selector to choose the proper C++ signed two's-complement integer type for a given byte size.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is cio::quarter_float
	 * For N = 2, the result is cio::half_float
	 * For N = 4, the result is float
	 * For N = 8, the result is double
	 *
	 * @tparam <N> The number of bytes to use for the integer type
	 */
	template <unsigned N> struct IntType;

	/**
	 * Alias for the signed two's-complement integer data type of the given number of N bytes.
	 * For N = 1, the result is std::int8_t.
	 * For N = 2, the result is std::int16_t.
	 * For N = 4, the result is std::int32_t.
	 * For N = 8, the result is std::int64_t.
	 */
	template <unsigned N> using Int = typename IntType<N>::type;

	/**
	 * The Matching Int template alias is a selector to choose the standard (i.e. from <cstdint> header) C++ signed integer
	 * type with the same size as the given type. The provided template type may be any type but is most useful
	 * for matching up signed integers where more than one type has the same size (i.e. long vs. int or long long)
	 * or finding the signed integer type with the same binary size as an unsigned integer or floating point type.
	 *
	 * @tparam <T> The data type to use to locate a signed integer type of equal size
	 */
	template <typename T>
	using MatchingInt = Int<sizeof(T)>;

	/**
	 * The UnsignedType template is a selector to choose the proper C++ unsigned integer type for a given byte size.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is std::uint8_t.
	 * For N = 2, the result is std::uint16_t.
	 * For N = 4, the result is std::uint32_t.
	 * For N = 8, the result is std::uint64_t
	 *
	 * @tparam <N> The number of bytes to use for the integer type
	 */
	template <unsigned> struct UnsignedType;

	/**
	 * Alias for the unsigned integer data type of the given number of N bytes.
	 * For N = 1, the result is std::uint8_t.
	 * For N = 2, the result is std::uint16_t.
	 * For N = 4, the result is std::uint32_t.
	 * For N = 8, the result is std::uint64_t.
	 */
	template <unsigned N> using Unsigned = typename UnsignedType<N>::type;

	/**
	 * The Matching Unsigned template alias is a selector to choose the standard (i.e. from <cstdint> header) C++ signed integer
	 * type with the same size as the given type. The provided template type may be any type but is most useful
	 * for matching up unsigned integers where more than one type has the same size (i.e. long vs. int or long long)
	 * or finding the unsigned integer type with the same binary size as an signed integer or floating point type.
	 *
	 * @tparam <T> The data type to use to locate an unsigned integer type of equal size
	 */
	template <typename T>
	using MatchingUnsigned = Unsigned<sizeof(T)>;

	/**
	 * The FloatType template is a selector to choose the proper C++ floating point type for a given byte size.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is cio::quarter_float
	 * For N = 2, the result is cio::half_float
	 * For N = 4, the result is float
	 * For N = 8, the result is double
	 *
	 * @tparam <N> The number of bytes to use for the float type
	 */
	template <unsigned> struct FloatType;

	/**
	 * The Float template alias is a selector to choose the proper C++ floating point type for a given byte size.
	 * For sizes where no C++ float exists, the CIO emulation class of that type is used instead.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is cio::quarter_float
	 * For N = 2, the result is cio::half_float
	 * For N = 4, the result is float
	 * For N = 8, the result is double
	 *
	 * @tparam <N> The number of bytes to use for the float type
	 */
	template <unsigned N> using Float = typename FloatType<N>::type;

	/**
	 * The Matching Float template alias is a selector to choose the standard (i.e. from <cstdint> header) C++ floating point
	 * type with the same size as the given type. The provided template type may be any type but is most useful
	 * for matching up signed integers where more than one type has the same size (i.e. long vs. int or long long)
	 * or finding the signed integer type with the same binary size as an unsigned integer or floating point type.
	 *
	 * @tparam <T> The data type to use to locate a floating point type of equal size
	 */
	template <typename T>
	using MatchingFloat = Float<sizeof(T)>;

	/**
	 * The CharType template is a selector to choose the proper C++ text character type for a given byte size.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is char
	 * For N = 2, the result is wchar_t
	 *
	 * @tparam <N> The number of bytes to use for the text character type
	 */
	template <unsigned> struct CharType;

	/**
	 * The Char template alias is a selector to choose the proper C++ text character type for a given byte size.
	 *
	 * The type is undefined (thus triggering a compile error) if no such type is available.
	 * For N = 1, the result is char
	 * For N = 2, the result is wchar_t
	 *
	 * @tparam <N> The number of bytes to use for the text character type
	 */
	template <unsigned N> using Char = typename CharType<N>::type;

	/**
	 *
	 * The Matching Char template alias is a selector to choose the standard (i.e. from <cstdint> header) C++ text character
	 * type with the same size as the given type. The provided template type may be any type but is most useful
	 * for matching up characters to integer types of the same size.
	 *
	 * @tparam <T> The data type to use to locate a text character type of equal size
	 */
	template <typename T>
	using MatchingChar = Char<sizeof(T)>;

	/**
	 * The SumType is a traits template to get the most specific type that represents the sums of the given type.
	 * For the empty type list, std::false_type is used to represent the constant empty sum.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> + Value<U>
	 * For three or more types, it is recursively defined to be the SumType of the first type and the SumType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow or range.
	 *
	 * @tparam<T> The data types to find the sum type for
	 */
	template <typename... T> struct SumType;

	/**
	 * The Sum template alias is the most specific type that represents the sums of the given type.
	 * For the empty type list, std::false_type is used to represent the constant empty sum.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> + Value<U>
	 * For three or more types, it is recursively defined to be the SumType of the first type and the SumType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow or range.
	 *
	 * @tparam<T> The data types to find the sum type for
	 */
	template <typename... T> using Sum = typename SumType<T...>::type;

	/**
	 * The DifferenceType is a traits template to get the most specific type that represents the sequential differences of the given type.
	 * For the empty type list, std::false_type is used to represent the empty difference.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> - Value<U>
	 * For three or more types, it is recursively defined to be the DifferenceType of the first type and the DifferenceType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, or range.
	 * In particular, the result is unsigned if all the inputs are unsigned, even though theoretically a negative difference could happen.
	 *
	 * @tparam<T> The data types to find the difference type for
	 */
	template <typename... T> struct DifferenceType;

	/**
	 * The Difference template alias is the most specific type that represents the sequential differences of the given types.
	 * For the empty type list, std::false_type is used to represent the empty difference.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> - Value<U>
	 * For three or more types, it is recursively defined to be the DifferenceType of the first type and the DifferenceType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, or range.
	 * In particular, the result is unsigned if all the inputs are unsigned, even though theoretically a negative difference could happen.
	 *
	 * @tparam<T> The data types to find the difference type for
	 */
	template <typename... T> using Difference = typename DifferenceType<T...>::type;

	/**
	 * The SignedDifference template alias is the most specific type that represents the sequential signed differences of the given types.
	 * This takes the regular Difference and then makes it signed if it is not already.
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, or range.
	 *
	 * @tparam<T> The data types to find the signed difference type for
	 */
	template <typename... T> using SignedDifference = Difference<int, T...>;

	/**
	 * The ProductType template is a selector to get the most specific type that represents the products of the given types.
	 * For the empty type list, std::true_type is used to represent the constant base product 1.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> * Value<U>
	 * For three or more types, it is recursively defined to be the ProductType of the first type and the ProductType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, or range.
	 *
	 * @tparam<T> The data types to find the product type for
	 */
	template <typename... T> struct ProductType;

	/**
	 * The Product template alias is the most specific type that represents the products of the given types.
	 * For the empty type list, std::true_type is used to represent the constant base product 1.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> * Value<U>
	 * For three or more types, it is recursively defined to be the ProductType of the first type and the ProductType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, or range.
	 *
	 * @tparam<T> The data types to find the product type for
	 */
	template <typename... T> using Product = typename ProductType<T...>::type;

	/**
	 * The QuotientType template is a selector to get the most specific type that represents the sequential quotients of the given types.
	 * For the empty type list, std::true_type is used to represent the constant base quotient 1.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> / Value<U>
	 * For three or more types, it is recursively defined to be the QuotientType of the first type and the QuotientType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, range, or fractional division.
	 * In particular, the result remains an integer type if all input types are integers.
	 *
	 * @tparam<T> The data types to find the quotient type for
	 */
	template <typename... T> struct QuotientType;

	/**
	 * The Quotient template alias is the most specific type that represents the quotients of the given types.
	 * For the empty type list, std::true_type is used to represent the constant base quotient 1.
	 * For a single type T, this is equivalent to Value<T>.
	 * For two types T and U, this is equivalent to the declared type of Value<T> / Value<U>
	 * For three or more types, it is recursively defined to be the QuotientType of the first type and the QuotientType of the remainder.
	 *
	 * This version simply finds the narrowest machine native type that can be used, without consideration of overflow, underflow, range, or fractional division.
	 * In particular, the result remains an integer type if all input types are integers.
	 *
	 * @tparam<T> The data types to find the quotient type for
	 */
	template <typename... T> using Quotient = typename QuotientType<T...>::type;

	/**
	 * The RealQuotient template alias is a selector to get the most specific type that that represents the sequential quotients of the given types, but forced to at minimum a floating point value.
	 * This is essentially Quotient<float, T...>.
	 *
	 * @tparam <T> The data types to find the real quotient type for
	 */
	template <typename... T>
	using RealQuotient = Quotient<float, T...>;

	/**
	 * The Average template alias is a selector to get the most specific type that represents the average (mean) of the given types.
	 * This is essentially RealQuotient<T...> to ensure sufficient precision exists in the average.
	 *
	 * @tparam <T> The data types to find the average type for
	 */
	template <typename... T>
	using Average = RealQuotient<T...>;
}

namespace cio
{
	// ValuePointerType specializations

	template <typename T>
	struct ValuePointerType
	{
		/** The actual value type, which for non-void types is the provided type T without references or const/volatile */
		using type = T *;
	};

	template <>
	struct ValuePointerType<void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = std::uint8_t *;
	};

	template <>
	struct ValuePointerType<const void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = const std::uint8_t *;
	};

	template <typename T>
	struct ValuePointerType<T &> : public ValueType<T>
	{
		// pull in the the non-reference type as the pointer type
	};
	
	// CopyConstType specialization
	
	template <typename T, typename U>
	struct CopyConstType
	{
		using type = typename std::remove_const<T>::type;
	};
	
	template <typename T, typename U>
	struct CopyConstType<T, const U>
	{
		using type = typename std::add_const<T>::type;
	};
	
	template <typename T, typename U>
	struct CopyVolatileType
	{
		using type = typename std::remove_volatile<T>::type;
	};
	
	template <typename T, typename U>
	struct CopyVolatileType<T, volatile U>
	{
		using type = typename std::add_volatile<T>::type;
	};
	
	template <typename T, typename U>
	struct CopyConstVolatileType
	{
		using type = CopyVolatileType<CopyConstType<T, U>, U>;
	};


	// ReferenceType specializations

	template <typename T>
	struct ReferenceType
	{
		/** The actual value type, which for non-void types is the provided type T without references or const/volatile */
		using type = T &;
	};

	template <typename T>
	struct ReferenceType<T &>
	{
		using type = T &;
	};

	template <>
	struct ReferenceType<void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = std::uint8_t &;
	};

	template <>
	struct ReferenceType<const void>
	{
		/** The actual value type, which for void is std::uint8_t */
		using type = const std::uint8_t &;
	};

	// LogicalBitsType specialization

	template <typename T>
	struct LogicalBitsType
	{
		using type = std::integral_constant<std::size_t, sizeof(Value<T>) * 8u>;
	};

	template <>
	struct LogicalBitsType<bool>
	{
		using type = std::integral_constant<std::size_t, 1>;
	};

	template <>
	struct LogicalBitsType<const bool>
	{
		using type = std::integral_constant<std::size_t, 1>;
	};

	// ElementType specializations

	// general template, already documented
	template <typename T, typename L>
	struct ElementType
	{
		/** The element type applied by T::operator[](L) */
		using type = decltype(std::declval<T>()[std::declval<L>()]);

		typename ElementType<T, L>::type &operator()(T object, L value) const
		{
			return object[value];
		}
	};

	template <typename L>
	struct ElementType<void *, L>
	{
		using type = std::uint8_t &;

		std::uint8_t &operator()(void *ptr, L value) const
		{
			return static_cast<std::uint8_t *>(ptr)[value];
		}
	};

	template <typename L>
	struct ElementType<const void *, L>
	{
		using type = const std::uint8_t &;

		const std::uint8_t &operator()(const void *ptr, L value) const
		{
			return static_cast<const std::uint8_t *>(ptr)[value];
		}
	};

	// BoolType specializations

	// General template, already documented
	template <unsigned N>
	struct BoolType
	{
		/** The standard Boolean of size N */
		using type = Unsigned<N>;
	};

	/**
	 * The BoolType template is a selector to choose the proper C++ Boolean type for a given byte size.
	 *
	 * For the specialization N = 1, the type is bool.
	 */
	template <>
	struct BoolType<1>
	{
		/** The standard Boolean of size 1 */
		using type = bool;
	};

	// IntType specializations

	/**
	 * The IntType template is a selector to choose the proper C++ signed integer type for a given byte size.
	 *
	 * For the specialization N = 1, the type is std::int8_t.
	 */
	template <>
	struct IntType<1>
	{
		/** The standard signed integer of size 1 */
		using type = std::int8_t;
	};

	/**
	 * The IntType template is a selector to choose the proper C++ signed integer type for a given byte size.
	 *
	 * For the specialization N = 2, the type is std::int16_t.
	 */
	template <>
	struct IntType<2>
	{
		/** The standard signed integer of size 2 */
		using type = std::int16_t;
	};

	/**
	 * The IntType template is a selector to choose the proper C++ signed integer type for a given byte size.
	 *
	 * For the specialization N = 4, the type is std::int32_t.
	 */
	template <>
	struct IntType<4>
	{
		/** The standard signed integer of size 4 */
		using type = std::int32_t;
	};

	/**
	 * The IntType template is a selector to choose the proper C++ signed integer type for a given byte size.
	 *
	 * For the specialization N = 4, the type is std::int64_t.
	 */
	template <>
	struct IntType<8>
	{
		/** The standard signed integer of size 8 */
		using type = std::int64_t;
	};

	// UnsignedType specializations

	/**
	 * The UnsignedType template is a selector to choose the proper C++ unsigned integer type for a given byte size.
	 *
	 * For the specialization N = 1, the type is std::uint8_t.
	 */
	template <>
	struct UnsignedType<1>
	{
		/** The standard unsigned integer of size 1 */
		using type = std::uint8_t;
	};

	/**
	 * The UnsignedType template is a selector to choose the proper C++ unsigned integer type for a given byte size.
	 *
	 * For the specialization N = 2, the type is std::uint16_t.
	 */
	template <>
	struct UnsignedType<2>
	{
		/** The standard unsigned integer of size 2 */
		using type = std::uint16_t;
	};

	/**
	 * The UnsignedType template is a selector to choose the proper C++ unsigned integer type for a given byte size.
	 *
	 * For the specialization N = 4, the type is std::uint32_t.
	 */
	template <>
	struct UnsignedType<4>
	{
		/** The standard unsigned integer of size 4 */
		using type = std::uint32_t;
	};

	/**
	 * The UnsignedType template is a selector to choose the proper C++ unsigned integer type for a given byte size.
	 *
	 * For the specialization N = 8, the type is std::uint64_t.
	 */
	template <>
	struct UnsignedType<8>
	{
		/** The standard unsigned integer of size 8 */
		using type = std::uint64_t;
	};

	// FloatType specializations

	/**
	 * The FloatType template is a selector to choose the proper C++ floating point type for a given byte size.
	 *
	 * For the specialization N = 4, the type is float.
	 */
	template <>
	struct FloatType<4>
	{
		/** The standard floating point type of size 4 */
		using type = float;
	};

	/**
	 * The FloatType template is a selector to choose the proper C++ floating point type for a given byte size.
	 *
	 * For the specialization N = 8, the type is double.
	 */
	template <>
	struct FloatType<8>
	{
		/** The standard floating point type of size 8 */
		using type = double;
	};

	// CharType specializations

	/**
	 * The CharType template is a selector to choose the proper C++ text character type for a given byte size.
	 *
	 * For the specialization N = 1, the type is char for UTF-8 data.
	 */
	template <>
	struct CharType<1>
	{
		/** The standard text character of size 1 */
		using type = char;
	};

	/**
	 * The CharType template is a selector to choose the proper C++ text character type for a given byte size.
	 *
	 * For the specialization N = 2, the type is wchar_t for UTF-16 data.
	 */
	template <>
	struct CharType<2>
	{
		/** The standard text character of size 2 */
		using type = wchar_t;
	};

	// SumType specializations
	template <>
	struct SumType<>
	{
		using type = std::false_type;
	};

	template <typename T>
	struct SumType<T>
	{
		using type = Value<T>;
	};

	template <typename T, typename U>
	struct SumType<T, U>
	{
		using type = decltype(Value<T>() + Value<U>());
	};

	template <typename T, typename U, typename... R>
	struct SumType<T, U, R...>
	{
		using type = Sum<T, Sum<U, R...>>;
	};


	// DifferenceType specializations
	template <>
	struct DifferenceType<>
	{
		using type = std::false_type;
	};

	template <typename T>
	struct DifferenceType<T>
	{
		using type = Value<T>;
	};

	template <typename T, typename U>
	struct DifferenceType<T, U>
	{
		using type = decltype(Value<T>() - Value<U>());
	};

	template <typename T, typename U, typename... R>
	struct DifferenceType<T, U, R...>
	{
		using type = Difference<T, Difference<U, R...>>;
	};

	// ProductType specializations
	template <>
	struct ProductType<>
	{
		using type = std::true_type;
	};

	template <typename T>
	struct ProductType<T>
	{
		using type = Value<T>;
	};

	template <typename T, typename U>
	struct ProductType<T, U>
	{
		using type = decltype(Value<T>() * Value<U>());
	};

	template <typename T, typename U, typename... R>
	struct ProductType<T, U, R...>
	{
		using type = Product<T, Product<U, R...>>;
	};

	// QuotientType specializations
	template <>
	struct QuotientType<>
	{
		using type = std::true_type;
	};

	template <typename T>
	struct QuotientType<T>
	{
		using type = Value<T>;
	};

	template <typename T, typename U>
	struct QuotientType<T, U>
	{
		using type = decltype(Value<T>() / Value<U>());
	};

	template <typename T, typename U, typename... R>
	struct QuotientType<T, U, R...>
	{
		using type = Quotient<T, Quotient<U, R...>>;
	};


	/**
	 * Transfer the content of one text buffer to another of potentially different size.
	 * If the source is longer than the destination, it is truncated.
	 * If the source is shorter than the destination, the destination is null-terminated as a courtesy.
	 *
	 * This method is primarily used as a backend for higher level text and print methods.
	 *
	 * @param source The input text
	 * @param length The input text length
	 * @param dest The output buffer
	 * @param capacity The output buffer capacity
	 * @return the actual number of bytes transferred, not counting any null terminator
	 */
	inline std::size_t reprint(const char *source, std::size_t length, char *dest, std::size_t capacity) noexcept
	{
		std::size_t toCopy = 0;

		if (dest)
		{
			if (source)
			{
				toCopy = length < capacity ? length : capacity;

				if (toCopy > 0)
				{
					std::memcpy(dest, source, toCopy);
				}

				if (toCopy < capacity)
				{
					dest[toCopy] = 0;
				}
			}
			else if (capacity > 0)
			{
				dest[0] = 0;
			}
		}

		return toCopy;
	}
}

#endif
