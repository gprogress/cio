/*==============================================================================
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Validation.h"

#include "Severity.h"

#include <ostream>

namespace cio
{
	namespace
	{
		const char *sValidationText[] =
		{
			"None",

			"Value requires no conversion",

			"Value conversion is lossless",

			"Value conversion loses information",

			"Value has too much precision",

			"Value below minimum",

			"Value above maximum",

			"Value too short",

			"Value too long",

			"Too few values",

			"Too many values",

			"Invalid value",

			"Missing value",

			"Multiple problems with value",

			"Value type not yet supported"
		};
	}

	const char *print(Validation val) noexcept
	{
		return sValidationText[static_cast<std::size_t>(val)];
	}

	std::ostream &operator<<(std::ostream &s, Validation val)
	{
		return s << sValidationText[static_cast<std::size_t>(val)];
	}

	Severity mapToSeverity(Validation val) noexcept
	{
		Severity severity;

		switch (val)
		{
			case Validation::Exact:
			case Validation::None:
				severity = Severity::None;
				break;

			case Validation::LosslessConversion:
				severity = Severity::Debug;
				break;

			case Validation::LossyConversion:
			case Validation::TooMuchPrecision:
				severity = Severity::Warning;
				break;

			default:
				severity = Severity::Error;
		}

		return severity;
	}
}
