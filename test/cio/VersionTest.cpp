/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "VersionTest.h"

#include <cio/test/Assert.h>

#include <cio/Version.h>

namespace cio
{
	VersionTest::VersionTest() :
		cio::test::Suite("cio/Version")
	{
		this->addTestCase("getMajor", &VersionTest::testGetMajor);
		this->addTestCase("getMinor", &VersionTest::testGetMinor);
		this->addTestCase("getPatch", &VersionTest::testGetPatch);
		this->addTestCase("getTweak", &VersionTest::testGetTweak);
		this->addTestCase("operator[]", &VersionTest::testBracket);
		this->addTestCase("operator<", &VersionTest::testLess);
		this->addTestCase("operator>", &VersionTest::testGreater);
		this->addTestCase("operator==", &VersionTest::testEqual);
	}

	VersionTest::~VersionTest() noexcept = default;
	
	void VersionTest::testGetMajor()
	{
		Version version;
		CIO_ASSERT_EQUAL(0u, version.getMajor());
		
		version = "5";
		CIO_ASSERT_EQUAL(5u, version.getMajor());
		
		version = "5.32";
		CIO_ASSERT_EQUAL(5u, version.getMajor());
		
		version = "5.32.1";
		CIO_ASSERT_EQUAL(5u, version.getMajor());
		
		version = "5.32.1.19";
		CIO_ASSERT_EQUAL(5u, version.getMajor());
	}
			
	void VersionTest::testGetMinor()
	{
		Version version;
		CIO_ASSERT_EQUAL(0u, version.getMinor());
		
		version = "5";
		CIO_ASSERT_EQUAL(0u, version.getMinor());
		
		version = "5.32";
		CIO_ASSERT_EQUAL(32u, version.getMinor());
		
		version = "5.32.1";
		CIO_ASSERT_EQUAL(32u, version.getMinor());
		
		version = "5.32.1.19";
		CIO_ASSERT_EQUAL(32u, version.getMinor());
	}
			
	void VersionTest::testGetPatch()
	{
		Version version;
		CIO_ASSERT_EQUAL(0u, version.getPatch());
		
		version = "5";
		CIO_ASSERT_EQUAL(0u, version.getPatch());
		
		version = "5.32";
		CIO_ASSERT_EQUAL(0u, version.getPatch());
		
		version = "5.32.1";
		CIO_ASSERT_EQUAL(1u, version.getPatch());
		
		version = "5.32.1.19";
		CIO_ASSERT_EQUAL(1u, version.getPatch());
	}
			
	void VersionTest::testGetTweak()
	{
		Version version;
		CIO_ASSERT_EQUAL(0u, version.getTweak());
		
		version = "5";
		CIO_ASSERT_EQUAL(0u, version.getTweak());
		
		version = "5.32";
		CIO_ASSERT_EQUAL(0u, version.getTweak());
		
		version = "5.32.1";
		CIO_ASSERT_EQUAL(0u, version.getTweak());
		
		version = "5.32.1.19";
		CIO_ASSERT_EQUAL(19u, version.getTweak());
	}
			
	void VersionTest::testBracket()
	{
		Version version;
		CIO_ASSERT_EQUAL(0u, version[0]);
		CIO_ASSERT_EQUAL(0u, version[1]);		
		CIO_ASSERT_EQUAL(0u, version[2]);
		CIO_ASSERT_EQUAL(0u, version[3]);
		CIO_ASSERT_EQUAL(0u, version[4]);
		
		version = "5";
		CIO_ASSERT_EQUAL(5u, version[0]);
		CIO_ASSERT_EQUAL(0u, version[1]);		
		CIO_ASSERT_EQUAL(0u, version[2]);
		CIO_ASSERT_EQUAL(0u, version[3]);
		CIO_ASSERT_EQUAL(0u, version[4]);
		
		version = "5.32";
		CIO_ASSERT_EQUAL(5u, version[0]);
		CIO_ASSERT_EQUAL(32u, version[1]);		
		CIO_ASSERT_EQUAL(0u, version[2]);
		CIO_ASSERT_EQUAL(0u, version[3]);
		CIO_ASSERT_EQUAL(0u, version[4]);
		
		version = "5.32.1";
		CIO_ASSERT_EQUAL(5u, version[0]);
		CIO_ASSERT_EQUAL(32u, version[1]);		
		CIO_ASSERT_EQUAL(1u, version[2]);
		CIO_ASSERT_EQUAL(0u, version[3]);
		CIO_ASSERT_EQUAL(0u, version[4]);
		
		version = "5.32.1.19";
		CIO_ASSERT_EQUAL(5u, version[0]);
		CIO_ASSERT_EQUAL(32u, version[1]);		
		CIO_ASSERT_EQUAL(1u, version[2]);
		CIO_ASSERT_EQUAL(19u, version[3]);
		CIO_ASSERT_EQUAL(0u, version[4]);		
	}
			
	void VersionTest::testLess()
	{
		Version empty;
		Version v5 = "5";
		Version v52 = "5.2";
		Version v520 = "5.2.0";
		Version v511 = "5.11";
		Version v61 = "6.1";
		
		CIO_ASSERT(!(empty < empty));
		CIO_ASSERT(empty < v5);
		CIO_ASSERT(empty < v52);
		CIO_ASSERT(empty < v520);
		CIO_ASSERT(empty < v511);
		CIO_ASSERT(empty < v61);
		
		CIO_ASSERT(!(v5 < empty));
		CIO_ASSERT(!(v5 < v5));
		CIO_ASSERT(v5 < v52);
		CIO_ASSERT(v5 < v520);
		CIO_ASSERT(v5 < v511);
		CIO_ASSERT(v5 < v61);
		
		CIO_ASSERT(!(v52 < empty));
		CIO_ASSERT(!(v52 < v5));
		CIO_ASSERT(!(v52 < v52));
		CIO_ASSERT(!(v52 < v520));
		CIO_ASSERT(v52 < v511);
		CIO_ASSERT(v52 < v61);
		
		CIO_ASSERT(!(v520 < empty));
		CIO_ASSERT(!(v520 < v5));
		CIO_ASSERT(!(v520 < v52));
		CIO_ASSERT(!(v520 < v520));
		CIO_ASSERT(v520 < v511);
		CIO_ASSERT(v520 < v61);
		
		CIO_ASSERT(!(v511 < empty));
		CIO_ASSERT(!(v511 < v5));
		CIO_ASSERT(!(v511 < v52));
		CIO_ASSERT(!(v511 < v520));
		CIO_ASSERT(!(v511 < v511));
		CIO_ASSERT(v511 < v61);
		
		CIO_ASSERT(!(v61 < empty));
		CIO_ASSERT(!(v61 < v5));
		CIO_ASSERT(!(v61 < v52));
		CIO_ASSERT(!(v61 < v520));
		CIO_ASSERT(!(v61 < v511));
		CIO_ASSERT(!(v61 < v61));
	}
			
	void VersionTest::testGreater()
	{
		Version empty;
		Version v5 = "5";
		Version v52 = "5.2";
		Version v520 = "5.2.0";
		Version v511 = "5.11";
		Version v61 = "6.1";
		
		CIO_ASSERT(!(empty > empty));
		CIO_ASSERT(!(empty > v5));
		CIO_ASSERT(!(empty > v52));
		CIO_ASSERT(!(empty > v520));
		CIO_ASSERT(!(empty > v511));
		CIO_ASSERT(!(empty > v61));
		
		CIO_ASSERT(v5 > empty);
		CIO_ASSERT(!(v5 > v5));
		CIO_ASSERT(!(v5 > v52));
		CIO_ASSERT(!(v5 > v520));
		CIO_ASSERT(!(v5 > v511));
		CIO_ASSERT(!(v5 > v61));
		
		CIO_ASSERT(v52 > empty);
		CIO_ASSERT(v52 > v5);
		CIO_ASSERT(!(v52 > v52));
		CIO_ASSERT(!(v52 > v520));
		CIO_ASSERT(!(v52 > v511));
		CIO_ASSERT(!(v52 > v61));
		
		CIO_ASSERT(v520 > empty);
		CIO_ASSERT(v520 > v5);
		CIO_ASSERT(!(v520 > v52));
		CIO_ASSERT(!(v520 > v520));
		CIO_ASSERT(!(v520 > v511));
		CIO_ASSERT(!(v520 > v61));
		
		CIO_ASSERT(v511 > empty);
		CIO_ASSERT(v511 > v5);
		CIO_ASSERT(v511 > v52);
		CIO_ASSERT(v511 > v520);
		CIO_ASSERT(!(v511 > v511));
		CIO_ASSERT(!(v511 > v61));
		
		CIO_ASSERT(v61 > empty);
		CIO_ASSERT(v61 > v5);
		CIO_ASSERT(v61 > v52);
		CIO_ASSERT(v61 > v520);
		CIO_ASSERT(v61 > v511);
		CIO_ASSERT(!(v61 > v61));
	}
			
	void VersionTest::testEqual()
	{
		Version empty;
		Version v5 = "5";
		Version v52 = "5.2";
		Version v520 = "5.2.0";
		Version v511 = "5.11";
		Version v61 = "6.1";
		
		CIO_ASSERT(empty == empty);
		CIO_ASSERT(!(empty > v5));
		CIO_ASSERT(!(empty > v52));
		CIO_ASSERT(!(empty > v520));
		CIO_ASSERT(!(empty > v511));
		CIO_ASSERT(!(empty > v61));
		
		CIO_ASSERT(!(v5 == empty));
		CIO_ASSERT(v5 == v5);
		CIO_ASSERT(!(v5 == v52));
		CIO_ASSERT(!(v5 == v520));
		CIO_ASSERT(!(v5 == v511));
		CIO_ASSERT(!(v5 == v61));
		
		CIO_ASSERT(!(v52 == empty));
		CIO_ASSERT(!(v52 == v5));
		CIO_ASSERT(v52 == v52);
		CIO_ASSERT(v52 == v520);
		CIO_ASSERT(!(v52 == v511));
		CIO_ASSERT(!(v52 == v61));
		
		CIO_ASSERT(!(v520 == empty));
		CIO_ASSERT(!(v520 == v5));
		CIO_ASSERT(v520 == v52);
		CIO_ASSERT(v520 == v520);
		CIO_ASSERT(!(v520 == v511));
		CIO_ASSERT(!(v520 == v61));
		
		CIO_ASSERT(!(v511 == empty));
		CIO_ASSERT(!(v511 == v5));
		CIO_ASSERT(!(v511 == v52));
		CIO_ASSERT(!(v511 == v520));
		CIO_ASSERT(v511 == v511);
		CIO_ASSERT(!(v511 == v61));
		
		CIO_ASSERT(!(v61 == empty));
		CIO_ASSERT(!(v61 == v5));
		CIO_ASSERT(!(v61 == v52));
		CIO_ASSERT(!(v61 == v520));
		CIO_ASSERT(!(v61 == v511));
		CIO_ASSERT(v61 == v61);
	}
}

