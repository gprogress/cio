/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_STATE_H
#define CIO_STATE_H

#include "Types.h"

#include "Action.h"
#include "Outcome.h"
#include "Reason.h"
#include "Status.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * The State template provides information about the type, status, outcome, and reason for the outcome of a particular action.
	 * It is intended to be a lightweight class to communicate status information in a compact 4-byte structure.
	 */
	class CIO_API State
	{
		public:
			/** The action that is being taken */
			Action action;
					
			/** The current execution status of operation */
			Status status;
			
			/** The outcome of the action */
			Outcome outcome;
			
			/** The reason for the outcome */
			Reason reason;

			/**
			 * Construct an empty State object with Action::None, Outcome::None, Reason::None, and Status::None.
			 */
			inline State() noexcept;
			
			/**
			 * Construct a State object for the given Status with Action::None, Outcome::None, and Reason::None.
			 *
			 * @param status The status
			 */			
			inline State(Status status) noexcept;
			
			/**
			 * Construct a State object for the given Action with Outcome::None, Reason::None, and Status::None.
			 *
			 * @param action the action
			 */			
			inline State(Action action) noexcept;
			
			/**
			 * Construct a State object for the given Reason assuming if set it is a reason for success or failure.
			 * Action is initialized to None and Status is initialized to Completed.
			 * If reason is anything other than Reason::None, then the state is initialized with Outcome::Failure.
			 * 
			 * @param reason The reason
			 */
			inline State(Reason reason) noexcept;
			
			/**
			 * Construct a State object for the given Action with the given parameters.
			 *
			 * @param action The action
			 * @param status The status
			 * @param outcome The outcome
			 * @param reason The reason for the outcome
			 */			
			inline State(Action action, Status status, Outcome outcome = Outcome::None, Reason reason = Reason::None) noexcept;
			
			/**
			 * Construct a copy of a State object.
			 *
			 * @param in The State object to copy
			 */
			inline State(const State &in) = default;

			/**
			 * Copy a State object.
			 *
			 * @param in The State object to copy
			 * @return this State object
			 */
			inline State &operator=(const State &in) = default;
			
			/**
			 * Checks whether the operation has been requested
			 * This is true if the status is any status other than None.
			 *
			 * @return whether the operation was requested
			 */
			inline bool requested() const noexcept;
			
			/**
			 * Checks whether the operation has been requested but not yet finished.
			 * This is true if the status is not None, Canceled, Completed.
			 *
			 * @return whether the operation was unfinished
			 */
			inline bool unfinished() const noexcept;
	
			/**
			 * Checks whether the operation has started.
			 * This is true if the status is either running or completed.
			 *
			 * @return whether the operation was started
			 */
			inline bool started() const noexcept;
			
			/**
			 * Checks whether the operation is running.
			 * This validates that the status is at least Started and not a completion state.
			 *
			 * @return whether the operation is currently running
			 */
			inline bool running() const noexcept;
			
			/**
			 * Checks whether the operation has completed.
			 * This is true if the status is completed.
			 *
			 * @return whether the operation has completed
			 */
			inline bool completed() const noexcept;
			
			/**
			 * Checks whether the operation has succeeded.
			 * This is true if the status is Completed and the outcome is Completed or Succeeded.
			 *
			 * @return whether the operation has suceeded
			 */
			inline bool succeeded() const noexcept;
			
			/**
			 * Checks whether the operation has failed.
			 * This is true if the outcome is Failed.
			 *
			 * @return whether the operation has failed
			 */			
			inline bool failed() const noexcept;
			
			/**
			 * Checks whether the operation's current status is considered interrupting.
			 * This is true for all interrupted statuses Interrupted and Paused, and all completion statuses.
			 *
			 * @return whether the status is considered interrupting
			 */
			inline bool interrupting() const noexcept;
			
			/**
			 * Copies the state of the given state.
			 *
			 * @param e The state
			 */
			inline void set(State e) noexcept;
			
			/**
			 * Resets the state of the state to the given settings.
			 *
			 * @param action The action
			 * @param status The status
			 * @param outcome The outcome
			 * @param reason The reason
			 */
			inline void reset(Action action, Status status = Status::None, Outcome outcome = Outcome::None, Reason reason = Reason::None) noexcept;
			
			/**
			 * Marks the state to request a new action.
			 * This sets the action as provided, the status to Requested, and the Outcome and Reason to none.
			 *
			 * @param action The action to request
			 */
			inline void request(Action action) noexcept;
			
			/**
			 * Marks the state to start execution if it in an initial state of None, Requested, or Submitted.
			 * The state will not change state otherwise.
			 *
			 * @return the status after this change
			 */
			inline Status start() noexcept;
		
			/**
			 * Marks the state as paused for the given reason.
			 * If the status is not running() this does nothing.
			 * Otherwise it sets the status to Paused.
			 *
			 * @param reason The reason for pausing
			 * @return the status after this change
			 */
			inline Status pause(Reason reason = Reason::User) noexcept;
			
			/**
			 * Marks the state to resume execution if it was interrupted.
			 * If the status is not running() this does nothing.
			 * Otherwise it sets the status to Resumed and clears the reason.
			 *
			 * @return the status after this change
			 */
			inline Status resume() noexcept;

			/**
			 * Marks the state as completed.
			 * This sets both status and outcome to Completed and reason as provided.
			 *
			 * @param reason The reason for completion
			 * @return the outcome set by this change
			 */
			inline Outcome complete(Reason reason = Reason::None) noexcept;
			
			/**
			 * Marks the state as completed.
			 * This sets status to Completed and outcome and reason as provided.
			 *
			 * @param outcome The outcome
			 * @param reason The reason for completion
			 * @return the outcome set by this change
			 */
			inline Outcome complete(Outcome outcome, Reason reason = Reason::None) noexcept;
			
			/**
			 * Marks the state as succeeded.
			 * This sets the status to Completed, outcome to Succeeded, and reason as provided.
			 *
			 * @param reason The reason for success
			 * @return the outcome set by this change
			 */
			inline Outcome succeed(Reason reason = Reason::None) noexcept;
			
			/**
			 * Marks the state as immediately succeeding for the given action.
			 * This sets the ation and reason as provided, status to Completed, and outcome to Succeeded.
			 *
			 * @param action The action to immediately mark as succeeded
			 * @param reason The reason for success
			 * @return the outcome set by this change
			 */
			inline Outcome succeed(Action action, Reason reason = Reason::None) noexcept;
			
			/**
			 * Marks the state as failed.
			 * This sets the status to Completed, outcome to Failed, and reason as provided.
			 * The default reason is Unknown to indicate the error is not known.
			 *
			 * @param reason The reason for success
			 * @return the outcome set by this change
			 */
			inline Outcome fail(Reason reason = Reason::Unknown) noexcept;
			
			/**
			 * Marks the state as immediately failing for the given action.
			 * This sets the status to Completed, outcome to Failed, and action and reason as provided.
			 * The default reason is Unknown to indicate the error is not known.
			 *
			 * @param action The action to immediately mark as failed
			 * @param reason The reason for success
			 * @return the outcome set by this change
			 */
			inline Outcome fail(Action action, Reason reason = Reason::Unknown) noexcept;
			
			/**
			 * Marks the state as abandoned.
			 * This sets the status to Completed, outcome to Abandoned, and reason as provided.
			 * The default reason is Canceled since this is the main reason other than timeout.
			 *
			 * @param reason The reason for success
			 * @return the outcome set by this change
			 */
			inline Outcome abandon(Reason reason = Reason::Canceled) noexcept;
			
			/**
			 * Interprets the State object in a Boolean context.
			 * Current this is true if the outcome is a success state.
			 *
			 * @return whether the count of objects State was nonzero
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Clears the State object to 0 objects processsed and a status of None.
			 */
			inline void clear() noexcept;
			
			/**
			 * Prints the state to a text buffer for display.
			 *
			 * @param buffer The text buffer
			 * @param length The maximum length of the text
			 * @return the actual length of the text
			 */
			std::size_t print(char *buffer, std::size_t length) const noexcept;
			
			/**
			 * Prints the state to a text buffer for display.
			 *
			 * @return the printed text
			 * @throw std::bad_alloc If the text could not be printed
			 */
			std::string print() const;
	};
	
	/**
	 * Creates an state representing requesting the given action.
	 *
	 * @param action The action
	 * @return the state
	 */
	inline State request(Action action) noexcept;
	
	/**
	 * Creates an state representing starting the given action.
	 *
	 * @param action The action
	 * @return the state
	 */
	inline State start(Action action) noexcept;
	
	/**
	 * Creates an state representing succeeding at the given action.
	 *
	 * @param action The action
	 * @param reason The reason
	 * @return the state
	 */
	inline State succeed(Action action = Action::None, Reason reason = Reason::None) noexcept;
	
	/**
	 * Creates an state representing failing at the given action.
	 *
	 * @param action The action
	 * @param reason The reason
	 * @return the state
	 */
	inline State fail(Action action = Action::None, Reason reason = Reason::None) noexcept;
	
	/**
	 * Creates an state representing failing.
	 *
	 * @param reason The reason for failure
	 * @return the state
	 */
	inline State fail(Reason reason) noexcept;
	
	/**
	 * Creates an state representing completing the given action with the default Outcome::Completed and given reason.
	 *
	 * @param action The action
	 * @param reason The reason
	 * @return the state
	 */
	inline State complete(Action action = Action::None, Reason reason = Reason::None) noexcept;
	
	/**
	 * Creates an state representing completing the given action with the given outcome and reason.
	 *
	 * @param action The action
	 * @param outcome The outcome
	 * @param reason The reason
	 * @return the state
	 */
	inline State complete(Action action, Outcome outcome, Reason reason = Reason::None) noexcept;
	
	/**
	 * Checks to see if two states are equal.
	 *
	 * @param left The first state
	 * @param right The second state
	 * @return whether the states are equal
	 */
	inline bool operator==(const State &left, const State &right) noexcept;
	
	/**
	 * Checks to see if two states are not equal.
	 *
	 * @param left The first state
	 * @param right The second state
	 * @return whether the states are not equal
	 */
	inline bool operator!=(const State &left, const State &right) noexcept;
	
	/**
	 * Prints a state to a C++ stream.
	 * 
	 * @param stream The stream
	 * @param state The state
	 * @return the stream
	 * @throw std::exception If the stream throws an exception while printing
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const State &state);
}

/* Inline implementation */

namespace cio
{
	inline State::State() noexcept :
		action(Action::None),
		status(Status::None),
		outcome(Outcome::None),
		reason(Reason::None)
	{
		// nothing more to do
	}
	
	inline State::State(Status status) noexcept :
		action(Action::None),
		status(status),
		outcome(Outcome::None),
		reason(Reason::None)
	{
		// nothing more to do
	}
	
	inline State::State(Action action) noexcept :
		action(action),
		status(Status::None),
		outcome(Outcome::None),
		reason(Reason::None)
	{
		// nothing more to do
	}
	
	inline State::State(Reason reason) noexcept :
		action(Action::None),
		status(Status::Completed),
		outcome(reason != Reason::None ? Outcome::Failed : Outcome::Succeeded),
		reason(reason)
	{
		// nothing more to do
	}
	
	
	inline State::State(Action action, Status status, Outcome outcome, Reason reason) noexcept :
		action(action),
		status(status),
		outcome(outcome),
		reason(reason)
	{
		// nothing more to do
	}

	inline State::operator bool() const noexcept
	{
		return this->status == Status::Completed && this->outcome <= Outcome::Succeeded;
	}

	inline bool State::requested() const noexcept
	{
		return this->status >= Status::Requested;
	}
	
	inline bool State::unfinished() const noexcept
	{
		return this->status >= Status::Requested && this->status < Status::Canceled;
	}
			
	inline bool State::started() const noexcept
	{
		return this->status >= Status::Started;
	}
		
	inline bool State::running() const noexcept
	{
		return this->status >= Status::Started && this->status < Status::Canceled;
	}
			
	inline bool State::completed() const noexcept
	{
		return this->status == Status::Completed;
	}
		
	inline bool State::succeeded() const noexcept
	{
		return this->status == Status::Completed && this->outcome < Outcome::Failed;
	}
	
	inline bool State::failed() const noexcept
	{
		return this->status == Status::Completed && this->outcome >= Outcome::Failed;
	}
	
	inline bool State::interrupting() const noexcept
	{
		return status >= Status::Interrupted;
	}
				
	inline void State::set(State e) noexcept
	{
		this->action = e.action;
		this->status = e.status;
		this->outcome = e.outcome;
		this->reason = e.reason;
	}				
	
	inline void State::reset(Action action, Status status, Outcome outcome, Reason reason) noexcept
	{
		this->action = action;
		this->status = status;
		this->outcome = outcome;
		this->reason = reason;
	}
	
	inline void State::request(Action action) noexcept
	{
		this->action = action;
		this->status = Status::Requested;
		this->outcome = Outcome::None;
		this->reason = Reason::None;
	}
				
	inline Status State::start() noexcept
	{
		if (this->status < Status::Started)
		{
			this->status = Status::Started;
			this->outcome = Outcome::None;
			this->reason = Reason::None;
		}
		
		return this->status;
	}
	
	inline Status State::pause(Reason reason) noexcept
	{
		if (this->status >= Status::Started && this->status < Status::Canceled)
		{
			this->status = Status::Paused;
			this->reason = reason;
		}
		
		return this->status;
	}
	
	inline Status State::resume() noexcept
	{
		if (this->status > Status::Updated && this->status < Status::Canceled)
		{
			this->status = Status::Resumed;
			this->reason = Reason::None;
		}
		
		return this->status;
	}
	
	inline Outcome State::complete(Reason reason) noexcept
	{
		this->status = Status::Completed;
		this->outcome = Outcome::Completed;
		this->reason = reason;
		return this->outcome;
	}
	
	inline Outcome State::complete(Outcome outcome, Reason reason) noexcept
	{
		this->status = Status::Completed;
		this->outcome = outcome;
		this->reason = reason;
		return this->outcome;
	}
				
	inline Outcome State::succeed(Reason reason) noexcept
	{
		this->status = Status::Completed;
		this->outcome = Outcome::Succeeded;
		this->reason = reason;
		return this->outcome;
	}
	
	inline Outcome State::succeed(Action action, Reason reason) noexcept
	{
		this->action = action;
		this->status = Status::Completed;
		this->outcome = Outcome::Succeeded;
		this->reason = reason;
		return this->outcome;
	}	
		
	inline Outcome State::fail(Reason reason) noexcept
	{
		this->status = Status::Completed;
		this->outcome = Outcome::Failed;
		this->reason = reason;
		return this->outcome;
	}
				
	inline Outcome State::fail(Action action, Reason reason) noexcept
	{
		this->action = action;
		this->status = Status::Completed;
		this->outcome = Outcome::Failed;
		this->reason = reason;
		return this->outcome;
	}
				
	inline Outcome State::abandon(Reason reason) noexcept
	{
		this->status = Status::Completed;
		this->outcome = Outcome::Abandoned;
		this->reason = reason;
		return this->outcome;
	}
	
	inline void State::clear() noexcept
	{
		this->action = Action::None;
		this->status = Status::None;
		this->outcome = Outcome::None;
		this->reason = Reason::None;
	}
	
	inline State request(Action action) noexcept
	{
		return State(action, Status::Requested);
	}

	inline State start(Action action) noexcept
	{
		return State(action, Status::Started);
	}

	inline State succeed(Action action, Reason reason) noexcept
	{
		return State(action, Status::Completed, Outcome::Succeeded, reason);
	}

	inline State fail(Action action, Reason reason) noexcept
	{
		return State(action, Status::Completed, Outcome::Failed, reason);
	}
	
	inline State fail(Reason reason) noexcept
	{
		return State(Action::None, Status::Completed, Outcome::Failed, reason);
	}
	
	inline State complete(Action action, Reason reason) noexcept
	{
		return State(action, Status::Completed, Outcome::Completed, reason);
	}
	
	inline State complete(Action action, Outcome outcome, Reason reason) noexcept
	{
		return State(action, Status::Completed, outcome, reason);
	}
	
	inline bool operator==(const State &left, const State &right) noexcept
	{
		return left.action == right.action && left.status == right.status && left.outcome == right.outcome && left.reason == right.reason;
	}
	
	inline bool operator!=(const State &left, const State &right) noexcept
	{
		return !(left == right);
	}
}

#endif
