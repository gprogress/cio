/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PROTOCOLFILTER_H
#define CIO_PROTOCOLFILTER_H

#include "Types.h"

#include <cio/Text.h>
#include <cio/Resource.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Protocol Filter class is a match record class used to decide which particular class handles resource requests.
	 * It can be configured to match on any combination of resource type, protocol prefix, and file extension.
	 * 
	 * The class is set up to be easy to use with static initialization and without memory allocation since its primary
	 * use is static construction.
	 */
	class CIO_API ProtocolFilter
	{
		public:
			/**
			 * Gets the declared metaclass of this class.
			 * 
			 * @return the metaclass
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/** The metaclass intended to handle resources matching this filter */
			const Metaclass *metaclass = nullptr;

			/** The prefix(es) this filter applies (empty matches all) */
			Text prefix;

			/** The file extension(s) this filter applies to (empty matches no extension) */
			Text extension;

			/** The resource type this filter applies to (Resource::Unknown matches all) */
			Resource type = Resource::Unknown;

			/**
			 * Checks whether this filter matches the given path for the given resource type.
			 * The returned value is a score indicating how good the match is.
			 * -1 - not a match
			 * 0 - wildcard match on all fields
			 * 1 - wildcard match on type and extension, exact match on protocol
			 * 2 - wildcard match on type and protocol, exact match on extension
			 * 3 - wildcard match on type, exact match on extension and protocol
			 * 4 - exact match on type, wildcard match on protocol and extension
			 * 5 - exact match on type and extension, wildcard match on protocol
			 * 6 - exact match on type and protocol, wildcard match on extension
			 * 7 - exact match on all fields
			 * 
			 * @param type The resource type of interest
			 * @param path The path of interest
			 * @return whether this filter matches this type and path
			 */
			int matches(Resource type, const Path &path) const noexcept;

			/**
			 * Checks whether this filter matches the given prefix and extension for the given resource type.
			 * The returned value is a score indicating how good the match is.
			 * -1 - not a match
			 * 0 - wildcard match on all fields
			 * 1 - wildcard match on type and extension, exact match on protocol
			 * 2 - wildcard match on type and protocol, exact match on extension
			 * 3 - wildcard match on type, exact match on extension and protocol
			 * 4 - exact match on type, wildcard match on protocol and extension
			 * 5 - exact match on type and extension, wildcard match on protocol
			 * 6 - exact match on type and protocol, wildcard match on extension
			 * 7 - exact match on all fields
			 *
			 * @param type The resource type of interest
			 * @param prefix The protocol prefix of interest
			 * @param ext The extension of interest
			 * @return whether this filter matches this type and path
			 */
			int matches(Resource type, const Text &prefix, const Text &ext) const noexcept;

			/**
			 * Checks whether this filter is considered valid.
			 * This is true if it has a metaclass assigned.
			 * 
			 * @return whether the filter is valid
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Checks whether the given semicolon-delimited list contains the given text entry.
			 * The returned value is a score indicating how good the match is.
			 * -1 - not a match
			 * 0 - pure wildcard match
			 * 1 - exact match
			 */
			static int matchesListEntry(const Text &list, const Text &text) noexcept;

			~ProtocolFilter() noexcept;

		private:
			/** The metaclass for this class */
			static Class<ProtocolFilter> sMetaclass;
	};
}

/* Inline implementation */

namespace cio
{
	inline ProtocolFilter::operator bool() const noexcept
	{
		return this->metaclass != nullptr;
	}
}


#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
