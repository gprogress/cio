/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_TYPES_H
#define CIO_XML_TYPES_H

#if defined _WIN32
#if defined CIO_XML_BUILD
#if defined CIO_XML_BUILD_SHARED
#define CIO_XML_API __declspec(dllexport)
#else
#define CIO_XML_API
#endif
#else
#define CIO_XML_API __declspec(dllimport)
#endif
#elif defined __ELF__
#define CIO_XML_API __attribute__ ((visibility ("default")))
#else
#define CIO_XML_API
#endif


namespace cio
{
	/**
	 * The CIO xml namespace provides classes suitable for generic XML processing on any input source or output sink.
	 * Support includes stream-based pull and push parsing as well as a Document Object Model (DOM) tree parsing view.
	 */
	namespace xml
	{
		class Handler;
		class Parser;
		class Node;
		class Reader;
		class TreeBuilder;
		class Writer;

		enum class Type
		{
			None,
			Document,
			Element,
			Attribute,
			Text,
			Comment
		};

		enum class EventType
		{
			None,
			Start,
			End,
			Key,
			Value
		};
	}
}

#endif
