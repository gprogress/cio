/*==============================================================================
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_SEVERITY_H
#define CIO_SEVERITY_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The Severity enumeration describes various levels of notice related to logging, testing,
	 * and user interaction.
	 */
	enum class Severity : std::uint8_t
	{
		/** Default placeholder for not specifying a severity level, for thresholds means enable all severities */
		None = 0,

		/** Debugging information generally only useful for developers */
		Debug = 1,

		/** Informational messages that may be useful to users but not actually a problem */
		Info = 2,

		/** Warning messages about potential issues that may be a problem but not necessarily an error */
		Warning = 3,

		/** Error messages about a known but non-critial problem */
		Error = 4,

		/** Error messages about critical problems that should generally halt execution */
		Critical = 5,

		/** Fatal error messages which typically means crashes or other major problems are about to happen */
		Fatal = 6,

		/** Placeholder for specifying that all severity levels are disabled in a threshold */
		Disabled = 7
	};

	/**
	 * Gets the text string representing a severity.
	 *
	 * @param severity The severity level
	 * @return the text string for that severity level
	 */
	CIO_API const char *print(Severity severity) noexcept;

	/**
	 * Prints the severity level to a standard C++ output stream.
	 *
	 * @param s The output stream
	 * @param severity The severity level
	 * @return the stream after printing
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Severity severity);
}

#endif
