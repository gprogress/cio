/*==============================================================================
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "TextReader.h"

#include "Path.h"

#include <cctype>

namespace cio
{
	Class<TextReader> TextReader::sMetaclass("cio::TextReader");

	const Metaclass &TextReader::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}
		// Constructor implementations

	TextReader::TextReader() noexcept :
		mInput(nullptr)	
	{
		// nothing more to do
	}

	TextReader::TextReader(Input *input, std::size_t capacity) noexcept :
		mInput(input),
		mSource(input ? Source(input->location().data()) : Source()),
		mBuffer(capacity)
	{
		mBuffer.limit(0);
		this->reset();
	}

	TextReader::TextReader(Input *input, const Source& source, std::size_t capacity) noexcept :
		mInput(input),
		mSource(source), 
		mBuffer(capacity)
	{
		mBuffer.limit(0);
	}

	TextReader::TextReader(const TextReader &in) :
		mInput(in.mInput),
		mBuffer(in.mBuffer),
		mSource(in.mSource)
	{
		// nothing more to do
	}	

	TextReader::TextReader(TextReader &&in) noexcept:
		mInput(in.mInput),
		mBuffer(std::move(in.mBuffer)),
		mSource(std::move(in.mSource))
	{
		in.clear();
	}
	
	TextReader &TextReader::operator=(const TextReader &in)
	{
		if (this != &in)
		{
			mInput = in.mInput;
			mBuffer = in.mBuffer;
			mSource = in.mSource;
		}
		return *this;
	}
	
	TextReader &TextReader::operator=(TextReader &&in) noexcept
	{
		if (this != &in)
		{
			mInput = in.mInput;
			mBuffer = std::move(in.mBuffer);
			mSource = std::move(in.mSource);
		}
		in.clear();
		return *this;
	}

	const Metaclass& TextReader::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void TextReader::setSource(const Source& source) noexcept
	{
		mSource = source;
	}

	void TextReader::setBuffer(Buffer buffer) noexcept
	{
		mBuffer = std::move(buffer);
		this->reset();
	}

	void TextReader::setInput(Input* input, std::size_t capacity) noexcept
	{
		if (input && mInput != input)
		{
			mInput = input;
			mSource.setFile(input->location().data());
			this->reset();
			mBuffer.limit(0);
		}
		
		if (capacity != mBuffer.capacity())
		{
			mBuffer.resize(capacity);
		}
	}

	TextReader& TextReader::reset() noexcept
	{
		mSource.setLine(0);
		mSource.setColumn(0);
		return *this;
	}

	Progress<std::size_t> TextReader::refill() noexcept
	{
		Progress<std::size_t> progress;
		if (mInput)
		{
			mBuffer.compact();
			mBuffer.unflip();
			progress = mInput->requestFill(mBuffer);
			mBuffer.flip();
		}
		return progress;
	}

	Token TextReader::nextToken(bool discardWhitespace) noexcept
	{
		Token tok;
		Progress<std::size_t> progress;		

		// Keep reading tokens as long as readToken is true
		bool readToken = true;

		//Flag set to false for a whitespace token, true for a non-whitespace token
		bool significantText = false;
		
		//Fill the buffer if this is the first time through and it hasn't been done yet.
		if (!mBuffer.available() && mSource.getColumn() <= 0 && mSource.getLine() <=0)
		{
			readToken = false;
			if (mInput)
			{
				this->reset();
				progress = this->refill();

				if (progress.succeeded())
				{ 
					readToken = true;
				}
			}
		}

		// The buffer position will track the most recent token split point
		// We will peek ahead in the buffer until we find the next delimiter
		std::size_t nextOffset = 0;

		while (readToken)
		{
			//Check if there are bytes to read between the current position and the limit, if not compact
			if (nextOffset >= mBuffer.remaining())
			{
				if (mBuffer.position() > 0 || mBuffer.limit() < mBuffer.size())
				{
					try
					{
						progress = this->refill();

						if (!progress.succeeded())
						{
							readToken = false;
							tok.setPartial(true);
						}
					}
					catch (const std::exception &)
					{
						//Error refilling the buffer
						readToken = false;
						tok.setPartial(true);
					}
				}
				else if (mBuffer.position() == 0 && mBuffer.limit() == mBuffer.size())
				{
					readToken = false;
					tok.setPartial(true);
					mBuffer.position(mBuffer.limit());
				}
			}
			else
			{
				// Get each character in the next token
				char c = static_cast<char>(mBuffer.current()[nextOffset]);

				//Read in the beginning character and determine whether it's a whitespace or significant data token
				if (!std::isspace(c) && c != '\0' )
				{
					if (!significantText && nextOffset > 0)
					{
						if (discardWhitespace)
						{
							mBuffer.skip(nextOffset);
							nextOffset = 0;
							significantText = true;

							//Each byte makes the column in source advance, except a newline
							mSource.advanceColumn();
						}
						else
						{
							readToken = false;
						}
					}
					else
					{
						significantText = true;

						//Each byte makes the column in source advance, except a newline
						mSource.advanceColumn();
					}
				}
				else
				{
					if (significantText)
					{
						readToken = false;
					}
					else 
					{
						if (c == '\n')
						{
							//Newlines skip lines
							mSource.advanceLine();
						}
						else
						{
							//Each byte makes the column in source advance, except a newline
							mSource.advanceColumn();
						}
					}
				}

				nextOffset += readToken;
			}
		}
		//Set the token data from the poisition in the buffer
		tok.setWhitespace(!significantText);
		tok.setSource(mSource);

		// Capture the token then advance the buffer
		if (nextOffset > 0)
		{
			tok.referenceText(reinterpret_cast<const char *>(mBuffer.current()), nextOffset);
			mBuffer.skip(nextOffset);
		}

		return tok;
	}
}
