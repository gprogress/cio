/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Properties.h"

#include "Property.h"

#include <cio/Print.h>

#include <cstring>
#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		Properties::Properties() noexcept :
			mBits(0)
		{
			// nothing more to do
		}
		
		Properties::Properties(std::uint8_t value) noexcept :
			mBits(value)
		{
			// nothing more to do
		}
		
		Properties::Properties(Property value) noexcept :
			mBits(1u << static_cast<unsigned>(value))
		{
			// nothing more to do
		}
		
		bool Properties::get(Property value) const noexcept
		{
			return (mBits & (1u << static_cast<unsigned>(value))) != 0;
		}

		void Properties::set(Property value, bool enabled /* = true */) noexcept
		{
			mBits = (mBits & ~(1u << static_cast<unsigned>(value))) | (static_cast<std::uint8_t>(enabled) << static_cast<unsigned>(value));
		}

		void Properties::clear() noexcept
		{
			mBits = 0;
		}

		std::uint8_t Properties::getValue() const noexcept
		{
			return mBits;
		}

		void Properties::setValue(std::uint8_t value) noexcept
		{
			mBits = value;
		}

		std::size_t Properties::print(char *buffer, std::size_t length) const noexcept
		{
			std::size_t needed = 0;
			std::size_t printed = 0;
			
			for (unsigned i = 0; i < 8; ++i)
			{
				if ((mBits & (1u << i)) != 0)
				{
					Property type = static_cast<Property>(i);
				
					if (needed > 0)
					{
						needed += 2;
						if (printed < length)
						{
							buffer[printed++] = ',';
							if (printed < length)
							{
								buffer[printed++] = ' ';
							}
						}
					}
				
					const char *item = cio::bluetooth::print(type);
					std::size_t ilen = std::strlen(item);
					
					needed += ilen;
					
					if (printed < length)
					{
						std::size_t toPrint = std::min(length - printed, ilen);
						std::memcpy(buffer + printed, item, toPrint);
						printed += toPrint;
					}
				}
			}
			
			// No properties were set, print none in that case
			if (needed == 0)
			{
				const char *item = "None";
				std::size_t ilen = 4;
				
				needed += ilen;
				
				if (printed < length)
				{
					std::size_t toPrint = std::min(length - printed, ilen);
					std::memcpy(buffer + printed, item, toPrint);
					printed += toPrint;
				}
			}
			
			needed += 5;
			if (printed + 5 < length)
			{
				buffer[printed++] = ' ';
				buffer[printed++] = '(';
				cio::printByte(mBits, buffer + printed);
				printed += 2;
				buffer[printed++] = ')';
			}
			
			if (printed < length)
			{
				std::memset(buffer + printed, 0, length - printed);
			}
			
			return needed;		
		}

		std::string Properties::print() const
		{
			char tmp[128];
			std::size_t actual = this->print(tmp, 128);
			return std::string(tmp, tmp + actual);
		}
		
		std::ostream &operator<<(std::ostream &stream, const Properties &value)
		{
			char tmp[128];
			std::size_t actual = value.print(tmp, 128);
			return stream.write(tmp, actual);
		}
	}
}

