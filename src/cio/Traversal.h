/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TRAVERSAL_H
#define CIO_TRAVERSAL_H

#include "Types.h"

#include <algorithm>
#include <iosfwd>

namespace cio
{
	/**
	 * The Traversal enumeration describes how to perform a traversal or search relative to a specified target object.
	 */
	class Traversal
	{
		public:
			/** Only the target itself should be considered */
			static const std::size_t None = 0;

			/** The target and its immediate children should be considered */
			static const std::size_t Immediate = 1;

			/** The target and all of its children recursively should be considered */
			static const std::size_t Recursive = SIZE_MAX;
			
			/** Traversal depth */
			std::size_t value;
			
			/**
			 * Creates a traversal with default depth of Recursive.
			 */
			inline Traversal() noexcept;
			
			/**
			 * Creates at reversal with the given depth.
			 *
			 * @param depth The depth of the traversal
			 */
			inline Traversal(std::size_t depth) noexcept;

			/**
			 * Adds to a traversal.
			 * 
			 * @param added The amount added
			 * @return this traversal
			 */
			inline Traversal &operator+=(const Traversal &added) noexcept;

			/**
			 * Subtracts from a traversal.
			 *
			 * @param added The amount added
			 * @return this traversal
			 */
			inline Traversal &operator-=(const Traversal &removed) noexcept;

			/**
			 * Checks whether the traversal depth is non-zero.
			 *
			 * @return whether the traversal depth is non-zero
			 */
			inline explicit operator bool() const noexcept;
	};
	
	/**
	 * Makes the traversal recursion depth less.
	 *
	 * @param depth the recursion value to update
	 * @return a reference to value
	 */
	inline Traversal &operator--(Traversal &depth) noexcept;
	
	/**
	 * Makes the traversal recursion depth less.
	 *
	 * @param depth the recursion value to update
	 * @param count The depth to subtract
	 * @return a reference to value
	 */
	inline Traversal operator-(Traversal depth, Traversal count) noexcept;

	/**
	 * Makes the traversal recursion depth less.
	 *
	 * @param depth the recursion value to update
	 * @param count The depth to subtract
	 * @return a reference to value
	 */
	inline Traversal operator-(Traversal depth, std::size_t count) noexcept;

	/**
	 * Makes the traversal recursion depth greater.
	 *
	 * @param depth the recursion value to update
	 * @return a reference to value
	 */
	inline Traversal &operator++(Traversal &depth) noexcept;
	
	/**
	 * Makes the traversal recursion depth greater.
	 *
	 * @param depth the recursion value to update
	 * @param count The depth to add
	 * @return a reference to value
	 */
	inline Traversal operator+(Traversal depth, Traversal count) noexcept;

	/**
	 * Makes the traversal recursion depth greater.
	 *
	 * @param depth the recursion value to update
	 * @param count The depth to add
	 * @return a reference to value
	 */
	inline Traversal operator+(Traversal depth, std::size_t count) noexcept;

	/**
	 * Checks to see if two Traversals are equal.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the traversals are equal
	 */
	inline bool operator==(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Checks to see if two Traversals are not equal.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the traversals are not equal
	 */
	inline bool operator!=(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Checks to see if one traversal is less than another.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the first traversal is less than the second traversal
	 */
	inline bool operator<(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Checks to see if one traversal is less than or equal to another.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the first traversal is less than or equal to the second traversal
	 */
	inline bool operator<=(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Checks to see if one traversal is greater than another.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the first traversal is greater than the second traversal
	 */
	inline bool operator>(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Checks to see if one traversal is greater than or equal to another.
	 *
	 * @param left The first traversal
	 * @param right The second traversal
	 * @return whether the first traversal is greater than or equal to the second traversal
	 */
	inline bool operator>=(const Traversal &left, const Traversal &right) noexcept;

	/**
	 * Gets the text value for a Traversal value.
	 *
	 * @param depth The traversal value
	 * @return the text value
	 */
	CIO_API const char *print(Traversal depth) noexcept;

	/**
	 * Prints the text value for a Traversal value to a C++ standard output stream.
	 *
	 * @param s The stream
	 * @param depth The traversal value
	 * @return the stream after printing
	 * @throw std::exception If the C++ stream threw an exception to indicate an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Traversal depth);
}

/* Inline implementation */

namespace cio
{
	inline Traversal::Traversal() noexcept :
		value(SIZE_MAX)
	{
		// nothing more to do
	}
			
	inline Traversal::Traversal(std::size_t depth) noexcept :
		value(depth)
	{
		// nothing more to do
	}
	
	inline Traversal &Traversal::operator+=(const Traversal &added) noexcept
	{
		return *this = *this + added;
	}

	inline Traversal &Traversal::operator-=(const Traversal &removed) noexcept
	{
		return *this = *this - removed;
	}

	inline Traversal::operator bool() const noexcept
	{
		return this->value > 0;
	}

	inline Traversal &operator--(Traversal &depth) noexcept
	{
		depth.value -= (depth.value > 0 && depth.value < SIZE_MAX);
		return depth;
	}
	
	inline Traversal operator-(Traversal depth, Traversal count) noexcept
	{
		return Traversal(depth.value < SIZE_MAX ? depth.value - std::min(depth.value, count.value) : SIZE_MAX);
	}

	inline Traversal operator-(Traversal depth, std::size_t count) noexcept
	{
		return Traversal(depth.value < SIZE_MAX ? depth.value - std::min(depth.value, count) : SIZE_MAX);
	}
	
	inline Traversal &operator++(Traversal &depth) noexcept
	{
		depth.value += (depth.value < SIZE_MAX);
		return depth;
	}
	
	inline Traversal operator+(Traversal depth, Traversal count) noexcept
	{
		return Traversal(depth.value < (SIZE_MAX - count.value) ? (depth.value + count.value) : SIZE_MAX);
	}

	inline Traversal operator+(Traversal depth, std::size_t count) noexcept
	{
		return Traversal(depth.value < (SIZE_MAX - count) ? (depth.value + count) : SIZE_MAX);
	}

	inline bool operator==(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value == right.value;
	}

	inline bool operator!=(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value != right.value;
	}

	inline bool operator<(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value < right.value;
	}

	inline bool operator<=(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value <= right.value;
	}

	inline bool operator>(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value > right.value;
	}

	inline bool operator>=(const Traversal &left, const Traversal &right) noexcept
	{
		return left.value >= right.value;
	}
}

#endif
