/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Order.h"

#include "Print.h"

#include <ostream>

namespace cio
{
	namespace
	{
		const char *sByteOrderText[] =
		{
			"Unknown",
			"Big",
			"Little"
		};
	}

	const char *print(Order order) noexcept
	{
		return order <= Order::Little ? sByteOrderText[static_cast<unsigned>(order)] : "Unknown";
	}

	std::size_t print(Order order, char *buffer, std::size_t capacity) noexcept
	{
		return cio::print(print(order), buffer, capacity);
	}

	std::ostream &operator<<(std::ostream &s, Order order)
	{
		return s << sByteOrderText[static_cast<unsigned>(order)];
	}
}
