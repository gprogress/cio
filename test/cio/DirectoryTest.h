/************************************************************************
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_DIRECTORYTEST_H
#define CIO_DIRECTORYTEST_H

#include <cio/test/Suite.h>
#include <cio/Directory.h>
#include <cio/Path.h>

#include <map>

namespace cio
{
	/**
	* The CIO Directory test validates that native host filesystem directories function as intended
	* using a specially crafted temporary directory with known file content.
	*/
	class DirectoryTest : public cio::test::Suite
	{
		public:
			/**
			* Constructor. Sets up the test suite.
			*/
			DirectoryTest();

			/**
			* Destructor.
			*/
			virtual ~DirectoryTest() noexcept override;

			/**
			* Sets up the sample directory for traversals.
			* @return whether set up succeeded
			*/
			virtual cio::Severity setUp() override;

			/**
			* Tears down the sample directory, if it was set.
			*/
			virtual void tearDown() noexcept override;

			void openTest();
				
			/**
			* Tests that Directory::current() returns correct metadata.
			*/
			void currentTest();
				
			/**
			* Tests that Directory::filename() returns correct filename entries.
			*/
			void filenameTest();

			/**
			* Tests that Directory::next() correctly advances through entries.
			*/
			void nextTest();

			/**
			* Tests that Directory::valid() correctly returns whether current entry is valid.
			*/
			void traversableTest();
				
			/**
			* Tests that Directory::openCurrentInput() returns a valid input that can read files.
			*/
			void openCurrentInputTest();
				
			/**
			* Tests that Directory::openInput() returns valid input that can read files.
			*/
			void openInputTest();

			/**
			* Tests that Directory::checkForSpecialWindowsDevices() correctly identifies filenames which
			* match Microsoft Windows special device names.
			*/
			void checkForSpecialWindowsDevicesTest();
				
			/**
			* Tests that Directory::checkForSpecial() correctly identifies filenames which
			* match special/reserved filenames on the current host platform.
			*/
			void checkForSpecialFilenameTest();

			/**
			* Tests that Directory::clear() properly resets state.
			*/
			void clearTest();

		private:
			/** Protocol used for creating and removing test data */
			cio::Filesystem mProtocol;
				
			/** Path to created sample directory used for testing */
			cio::Path mSampleDir;
				
			/** Map of directory entries to expected metadata */
			std::map<std::string, Metadata> mExpectedEntries;
				
			/** Map of directory entries to expected binary content */
			std::map<std::string, Buffer> mExpectedContent;
	};
}

#endif

