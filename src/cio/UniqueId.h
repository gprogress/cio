/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_UNIQUEID_H
#define CIO_UNIQUEID_H

#include "Types.h"

#include "Order.h"

#include <cstring>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Unique ID describes a way to uniquely identify a single data element using a 16-byte data value.
	 * This can be a Universally Unique ID (UUID) or an application may interpret it some other more specific way.
	 */
	class CIO_API UniqueId
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;
			
			/**
			 * Generates a Unique ID using the canonical generation process.
			 *
			 * Currently this just creates a random Unique ID but in the future
			 * it may be modified to use a canonical generation process to ensure global uniqueness.
			 *
			 * @return a generated Unique ID
			 */
			static UniqueId generate() noexcept;

			/**
			 * Generates a random Unique ID using a local random process.
			 * The ID is likely (but not guaranteed) to be unique.
			 *
			 * @return a random Unique ID
			 */
			static UniqueId random() noexcept;

			/**
			 * Creates the empty Unique ID of all zero bytes.
			 */
			UniqueId() noexcept;

			/**
			 * Construct a Unique ID from the high 8 bytes and low 8 bytes.
			 * Note that on a little endian system, the low bytes will actually be first in memory.
			 *
			 * @param high The high 8 bytes
			 * @param low The low 8 bytes
			 */
			UniqueId(std::uint64_t high, std::uint64_t low) noexcept;

			/**
			 * Construct a copy of a unique ID.
			 *
			 * @param in The unique ID to copy
			 */
			UniqueId(const UniqueId &in) noexcept;

			/**
			 * Copy a unique ID.
			 *
			 * @param in The unique ID to copy
			 * @return this ID
			 */
			UniqueId &operator=(const UniqueId &in) noexcept;

			/**
			 * Destructor.
			 */
			~UniqueId() noexcept;

			/**
			 * Clears the Unique ID and sets it to all zero bytes.
			 */
			void clear() noexcept;

			/**
			 * Gets the 16 byte binary data array for the Unique ID.
			 *
			 * @return the Unique ID data
			 */
			const std::uint8_t *data() const noexcept;

			/**
			 * Gets the 16 byte binary data array for the Unique ID.
			 *
			 * @return the Unique ID data
			 */
			std::uint8_t *data() noexcept;

			/**
			 * Gets the "low" 8 bytes of the data formatted as 64-bit unsigned integer.
			 * Note that "high" and "low" reflect the logical position of the bytes.
			 * On little-endian platforms, "low" actually comes first in the memory layout.
			 *
			 * @return the low 8 bytes of the data
			 */
			std::uint64_t low() const noexcept;

			/**
			 * Gets the "high" 8 bytes of the data formatted as 64-bit unsigned integer.
			 * Note that "high" and "low" reflect the logical position of the bytes.
			 * On little-endian platforms, "high" actually comes second in the memory layout.
			 *
			 * @return the high 8 bytes of the data
			 */
			std::uint64_t high() const noexcept;

			/**
			 * Sets the unique ID using two sets of 8 bytes formatted as a 64-bit unsigned integer.
			 * Note that "high" and "low" reflect the logical position of the bytes.
			 * On little-endian platforms, "low" actually comes first in the memory layout.
			 *
			 * @param first The first 8 bytes to set
			 * @param second The second 8 bytes to set
			 */
			void set(std::uint64_t high, std::uint64_t low) noexcept;

			/**
			 * Interprets the Unique ID in a Boolean context.
			 * It is considered true if the bytes are not all zero.
			 *
			 * @return whether the Unique ID has any nonzero bytes
			 */
			explicit operator bool() const noexcept;
			
			/**
			 * Prints the Unique ID in the standard text format of 8-4-4-4-12 (hex digits separated by hyphens).
			 * This format always requires 36 bytes to print.
			 * If the provided buffer has fewer bytes, it will be truncated.
			 * if the provided buffer has extra bytes, they will be set to null.
			 *
			 * @param buffer The buffer to print to
			 * @param len The available bytes in the buffer
			 * @return the actual number of bytes to print, which is 36
			 */
			std::size_t print(char *buffer, std::size_t len) const noexcept;

			/**
			 * Prints a UUID to a string.
			 * 
			 * @return the printed UUID
			 */
			std::string print() const;
			
			/** 
			* Parses the given text for a UUID.
			* This will effectively parse 16 bytes of hex values, possibly surrounded by a { } or [ ] bracket pair
			* and possibly separated by '-' delimiters.
			*
			* @param text The text to parse
			* @param length The length of the text
			* @return the parse status
			*/
			TextParseStatus parse(const Text &text) noexcept;
			
			/** 
			 * First 8 bytes formatted as a 64-bit unsigned integer 
			 */
			std::uint64_t first;
					
			/** 
			 * Second 8 bytes formatted as a 64-bit unsigned integer 
			 */	
			std::uint64_t second;
			
		private:
			/** The metaclass for this class */
			static Class<UniqueId> sMetaclass;
	};

	/**
	 * Checks to see if two Unique IDs are equal.
	 * This is true if they have identical byte sequences.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the IDs are equal
	 */
	CIO_API bool operator==(const UniqueId &left, const UniqueId &right) noexcept;

	/**
	 * Checks to see if two Unique IDs are not equal.
	 * This is true if they have different byte sequences.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the IDs are not equal
	 */
	CIO_API bool operator!=(const UniqueId &left, const UniqueId &right) noexcept;

	/**
	 * Checks to see if one Unique ID is less than another.
	 * Sorting is done by an integer comparison using high and low to ensure the
	 * comparison is the same on both little- and big-endian architectures.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the first ID is less than the second ID
	 */
	CIO_API bool operator<(const UniqueId &left, const UniqueId &right) noexcept;

	/**
	 * Checks to see if one Unique ID is less than or equal to another.
	 * Sorting is done by an integer comparison using high and low to ensure the
	 * comparison is the same on both little- and big-endian architectures.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the first ID is less than or equal to the second ID
	 */
	CIO_API bool operator<=(const UniqueId &left, const UniqueId &right) noexcept;

	/**
	 * Checks to see if one Unique ID is greater than another.
	 * Sorting is done by an integer comparison using high and low to ensure the
	 * comparison is the same on both little- and big-endian architectures.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the first ID is greater than the second ID
	 */
	CIO_API bool operator>(const UniqueId &left, const UniqueId &right) noexcept;

	/**
	 * Checks to see if one Unique ID is greater than or equal to another.
	 * Sorting is done by an integer comparison using high and low to ensure the
	 * comparison is the same on both little- and big-endian architectures.
	 *
	 * @param left The first ID
	 * @param right The second ID
	 * @return whether the first ID is greater than or equal to the second ID
	 */
	CIO_API bool operator>=(const UniqueId &left, const UniqueId &right) noexcept;
	
	/**
	 * Performs a byte swap of the Unique ID.
	 *
	 * @param id The ID to byte swap
	 * @return the swapped ID
	 */
	CIO_API UniqueId swapBytes(const UniqueId &id) noexcept;
	
	/**
	 * Prints a Unique ID to a C++ stream.
	 *
	 * @param stream The stream
	 * @param id The ID
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const UniqueId &id);
}

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

#endif
