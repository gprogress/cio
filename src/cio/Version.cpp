/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Version.h"

#include "Buffer.h"
#include "Parse.h"
#include "Print.h"

namespace cio
{
	Version Version::max() noexcept
	{
		return Version(UINT16_MAX, UINT16_MAX, UINT16_MAX, UINT16_MAX);
	}

	Version Version::unpack(std::uint64_t value) noexcept
	{
		Version version;
		version.setValue(value);
		return version;
	}

	Version::Version() noexcept :
		mElements() // zero initialize
	{
		// nothing more to do
	}

	Version::Version(std::uint16_t major, std::uint16_t minor, std::uint16_t patch, std::uint16_t tweak) noexcept
	{
		mElements[0] = major;
		mElements[1] = minor;
		mElements[2] = patch;
		mElements[3] = tweak;
	}

	Version::Version(const char *text) :
		mElements()
	{
		this->parse(text);
	}

	Version::Version(const std::string &text) :
		mElements()
	{
		this->parse(text);
	}

	Version::Version(const Text &text) :
		mElements()
	{
		this->parse(text);
	}
	
	Version::Version(const Version &in) = default;

	Version::Version(Version &&in) noexcept = default;

	Version &Version::operator=(const Version &in) = default;

	Version &Version::operator=(Version &&in) noexcept = default;

	Version::~Version() noexcept = default;

	void Version::clear() noexcept
	{
		mElements[0] = 0;
		mElements[1] = 0;
		mElements[2] = 0;
		mElements[3] = 0;
		mSuffix.clear();
	}

	std::uint16_t Version::getMajor() const noexcept
	{
		return mElements[0];
	}

	void Version::setMajor(std::uint16_t major) noexcept
	{
		mElements[0] = major;
	}
			
	std::uint16_t Version::getMinor() const noexcept
	{
		return mElements[1];
	}
			
	void Version::setMinor(std::uint16_t minor) noexcept
	{
		mElements[1] = minor;
	}

	std::uint16_t Version::getPatch() const noexcept
	{
		return mElements[2];
	}

	void Version::setPatch(std::uint16_t patch) noexcept
	{
		mElements[2] = patch;
	}
			
	std::uint16_t Version::getTweak() const noexcept
	{
		return mElements[3];
	}

	void Version::setTweak(std::uint16_t tweak) noexcept
	{
		mElements[3] = tweak;
	}

	std::uint32_t Version::getMajorMinor() const noexcept
	{
		return (static_cast<std::uint32_t>(mElements[0]) << 16) | static_cast<std::uint32_t>(mElements[1]);
	}

	void Version::setMajorMinor(std::uint32_t value)
	{
		mElements[0] = static_cast<std::uint16_t>(value >> 16);
		mElements[1] = static_cast<std::uint16_t>(value);
	}

	std::uint64_t Version::getMajorMinorPatch() const noexcept
	{
		return (static_cast<std::uint64_t>(mElements[0]) << 32) |
			(static_cast<std::uint64_t>(mElements[1]) << 16) |
			static_cast<std::uint64_t>(mElements[2]);
	}

	void Version::setMajorMinorPatch(std::uint64_t value) noexcept
	{
		mElements[0] = static_cast<std::uint16_t>(value >> 32);
		mElements[1] = static_cast<std::uint16_t>(value >> 16);
		mElements[2] = static_cast<std::uint16_t>(value);
	}

	std::uint64_t Version::getValue() const noexcept
	{
		return (static_cast<std::uint64_t>(mElements[0]) << 48) |
			(static_cast<std::uint64_t>(mElements[1]) << 32) |
			(static_cast<std::uint64_t>(mElements[2]) << 16) |
			static_cast<std::uint64_t>(mElements[3]);
	}

	void Version::setValue(std::uint64_t value)
	{
		mElements[0] = static_cast<std::uint16_t>(value >> 48);
		mElements[1] = static_cast<std::uint16_t>(value >> 32);
		mElements[2] = static_cast<std::uint16_t>(value >> 16);
		mElements[3] = static_cast<std::uint16_t>(value);
	}

	void Version::set(std::uint16_t major, std::uint16_t minor, std::uint16_t patch, std::uint16_t tweak)
	{
		mElements[0] = major;
		mElements[1] = minor;
		mElements[2] = patch;
		mElements[3] = tweak;
	}

	std::uint16_t &Version::operator[](std::size_t idx) noexcept
	{
		return mElements[idx];
	}

	const std::uint16_t &Version::operator[](std::size_t idx) const noexcept
	{
		return mElements[idx];
	}

	std::size_t Version::size() const noexcept
	{
		std::size_t count = 0;
		if (mElements[3])
		{
			count = 4;
		}
		else if (mElements[2])
		{
			count = 3;
		}
		else if (mElements[1])
		{
			count = 2;
		}
		else if (mElements[0])
		{
			count = 1;
		}
		return count;
	}

	void Version::setSuffix(Text text)
	{
		mSuffix = std::move(text);
	}

	Text Version::getSuffix() const noexcept
	{
		return mSuffix;
	}

	void Version::clearSuffix() noexcept
	{
		mSuffix.clear();
	}

	TextParseStatus Version::parse(Text text)
	{
		TextParseStatus result;

		this->clear();

		std::pair<Text, Text> split = text.split('.');
		if (split.first)
		{
			std::size_t len = text.strlen();

			for (std::size_t i = 0; i < 4; ++i)
			{
				std::size_t splitLen = split.first.strlen();
				auto item = cio::parseUnsigned16(split.first);
				if (!item.valid())
				{
					break;
				}

				mElements[i] = item.value;
				result.parsed += item.parsed;
				result.status = Validation::Exact;

				if (item.parsed == splitLen && split.second)
				{
					result.parsed += 1; // add split '.'
					split = split.second.split('.');
				}
				else
				{
					break;
				}
			}

			if (split.first)
			{
				if (split.first.front() == '-')
				{
					++result.parsed; // add suffix '-'

					std::size_t suffix = 0;
					std::size_t splitLen = split.first.strlen();
					while (suffix < splitLen && std::isgraph(text[suffix]))
					{
						++suffix;
					}
					mSuffix.internalize(split.first.substr(1, suffix - 1));
				}
			}

			if (result.parsed < len)
			{
				result.terminator = text[result.parsed];
			}
		}
		else
		{
			result.reason = Reason::Missing;
			result.status = Validation::None;
		}

		return result;
	}

	std::size_t Version::strlen() const noexcept
	{
		std::size_t needed = 0;
		std::size_t count = this->size();
		if (count > 0)
		{
			needed += (count - 1); // 1 for each '.'
			needed += cio::strlen(mElements[0]);
			for (std::size_t i = 1; i < count; ++i)
			{
				needed += cio::strlen(mElements[i]);
			}
		}

		if (mSuffix)
		{
			// '-' plus actual suffix
			needed += (1 + mSuffix.strlen());
		}

		return needed;
	}

	Text Version::print() const
	{
		Text result;

		char temp[24] = { };
		std::size_t offset = this->printWithoutSuffix(temp, 24);

		if (mSuffix)
		{
			temp[offset++] = '-';
			result = Text(temp, offset) + mSuffix;
		}
		else
		{
			result = Text::duplicate(temp, offset);
		}

		return result;
	}

	std::size_t Version::print(char *buffer, std::size_t length) const noexcept
	{
		std::size_t needed = this->printWithoutSuffix(buffer, length);
		std::size_t suffix = mSuffix.strlen();

		if (suffix > 0)
		{
			if (needed < length)
			{
				buffer[needed] = '-';
			}

			if (needed + 1 < length)
			{
				std::memcpy(buffer + needed + 1, mSuffix.data(), std::min(length - needed - 1, suffix));
			}

			needed += (1 + suffix);
		}

		if (needed < length)
		{
			std::memset(buffer + needed, 0, length - needed);
		}
		return needed;
	}

	std::size_t Version::printWithoutSuffix(char *buffer, std::size_t length) const noexcept
	{
		std::size_t needed = 0;
		std::size_t actual = 0;
		std::size_t count = this->size();
		if (count > 0)
		{
			needed += cio::print(mElements[0], buffer, length);
			actual = std::min(needed, length);

			for (std::size_t i = 1; i < count; ++i)
			{
				needed += cio::print('.', buffer + actual, length - actual);
				actual = std::min(needed, length);

				needed += cio::print(mElements[i], buffer + actual, length - actual);
				actual = std::min(needed, length);
			}
		}

		if (actual < length)
		{
			std::memset(buffer + actual, 0, length - actual);
		}
		return needed;
	}

	bool Version::empty() const noexcept
	{
		return mElements[0] == 0 && mElements[1] == 0 && mElements[2] == 0 && mElements[3] == 0;
	}
	
	Version::operator bool() const noexcept
	{
		return !this->empty();
	}
	
	bool operator==(const Version &left, const Version &right) noexcept
	{
		return left.getValue() == right.getValue() && left.getSuffix() == right.getSuffix();
	}
	
	bool operator!=(const Version &left, const Version &right) noexcept
	{
		return !(left == right);
	}
	
	bool operator<(const Version &left, const Version &right) noexcept
	{
		bool less = false;

		std::uint64_t leftFields = left.getValue();
		std::uint64_t rightFields = right.getValue();

		if (leftFields == rightFields)
		{
			less = (left.getSuffix() < right.getSuffix());
		}
		else
		{
			less = (leftFields < rightFields);
		}
		
		return less;
	}
	
	bool operator<=(const Version &left, const Version &right) noexcept
	{
		return !(left > right);
	}
	
	bool operator>(const Version &left, const Version &right) noexcept
	{
		bool greater = false;

		std::uint64_t leftFields = left.getValue();
		std::uint64_t rightFields = right.getValue();

		if (leftFields == rightFields)
		{
			greater = (left.getSuffix() > right.getSuffix());
		}
		else
		{
			greater = (leftFields > rightFields);
		}

		return greater;
	}
	
	bool operator>=(const Version &left, const Version &right) noexcept
	{
		return !(left < right);
	}
	
	std::ostream &operator<<(std::ostream &s, const Version &version)
	{
		char temp[24] = { };
		version.printWithoutSuffix(temp, 24);
		s << temp;
		
		const Text &suffix = version.getSuffix();
		if (suffix)
		{
			s << '-' << suffix;
		}

		return s;
	}
}
