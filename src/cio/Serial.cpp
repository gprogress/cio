/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/

#include "Serial.h"

#include "Class.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>
#define CIO_INVALID_DEVICE reinterpret_cast<std::uintptr_t>(INVALID_HANDLE_VALUE)

#elif defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#define CIO_INVALID_DEVICE static_cast<std::uintptr_t>(-1)
#else

#error "No platform implementation for acsl::Serial"

#endif

#include <cerrno>
#include <system_error>

namespace cio
{
	Class<Serial> Serial::sMetaclass("cio::Serial");

	const Metaclass &Serial::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Serial::Serial()
	{
		// nothing to do
	}

	Serial::Serial(Serial &&in) noexcept :
		Device(std::move(in))
	{
		// nothing more to do
	}

	Serial &Serial::operator=(Serial &&in) noexcept
	{
		Device::operator=(std::move(in));
		return *this;
	}

	Serial::~Serial()  noexcept
	{
		// nothing more to do
	}

	const Metaclass &Serial::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	// Platform-specific code
#if defined CIO_HAVE_WINDOWS_H

	std::string Serial::detectOpenSerialPort()
	{
		// TODO: Figure out if there's a more general way to do this in Windows
		char name[5] = "COM ";

		for (unsigned i = 1; i <= 4; ++i)
		{
			name[3] = static_cast<char>('0' + i);
			HANDLE hComm;
			hComm = ::CreateFile(name,
									GENERIC_READ | GENERIC_WRITE,
									0,
									0,
									OPEN_EXISTING,
									FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH,
									0);

			if (hComm != INVALID_HANDLE_VALUE)
			{
				mHandle = reinterpret_cast<std::uintptr_t>(hComm);
				break;
			}
		}

		if (mHandle == CIO_INVALID_DEVICE)
		{
			throw std::runtime_error("No available serial ports");
		}

		return name;
	}

	void Serial::configureSerialPort(unsigned baud, unsigned bitsPerByte, Parity parity, StopBits stopBits)
	{
		if (mHandle != CIO_INVALID_DEVICE)
		{
			DCB dcb = { 0 };
			dcb.DCBlength = sizeof(DCB);

			if (::GetCommState(reinterpret_cast<HANDLE>(mHandle), &dcb))
			{
				// TODO validate parameters
				dcb.BaudRate = baud;
				dcb.ByteSize = bitsPerByte;
				dcb.Parity = static_cast<BYTE>(parity);
				dcb.StopBits = static_cast<BYTE>(stopBits);
				dcb.fParity = 0;
				dcb.fRtsControl = 0;
				dcb.fDtrControl = 0;

				if (!::SetCommState(reinterpret_cast<HANDLE>(mHandle), &dcb))
				{
					throw std::system_error(::GetLastError(), std::system_category());
				}
			}
		}
		else
		{
			throw std::invalid_argument("Serial is not open");
		}
	}

#elif defined CIO_HAVE_UNISTD_H
	void Serial::configureSerialPort(unsigned baud, unsigned bitsPerByte, Parity parity, StopBits stopBits)
	{
		if (mHandle != CIO_INVALID_DEVICE)
		{
			struct termios tty;
			std::memset(&tty, 0, sizeof(tty));
			tty.c_cflag |= CREAD | CLOCAL | CRTSCTS;
			tty.c_cc[VMIN] = 128;
			tty.c_cc[VTIME] = 5;
			// TODO figure out whether it is possible to set custom baud rate, otherwise set to nearest valid value
			::cfsetospeed(&tty, B115200);

			switch (parity)
			{
				case Parity::None:
					tty.c_cflag &= ~PARENB;
					tty.c_cflag &= ~PARODD;
					tty.c_cflag &= ~CMSPAR;
					break;

				case Parity::Odd:
					tty.c_cflag |= PARENB;
					tty.c_cflag |= PARODD;
					tty.c_cflag &= ~CMSPAR;
					break;

				case Parity::Even:
					tty.c_cflag |= PARENB;
					tty.c_cflag &= ~PARODD;
					tty.c_cflag &= ~CMSPAR;
					break;

				case Parity::Mark:
					tty.c_cflag |= PARENB;
					tty.c_cflag |= CMSPAR;
					tty.c_cflag |= PARODD;
					break;

				case Parity::Space:
					tty.c_cflag |= PARENB;
					tty.c_cflag |= CMSPAR;
					tty.c_cflag &= ~PARODD;
			}

			tty.c_cflag &=  ~CSIZE;

			switch (bitsPerByte)
			{
				case 5:
					tty.c_cflag |= CS5;
					break;

				case 6:
					tty.c_cflag |= CS6;
					break;

				case 7:
					tty.c_cflag |= CS7;
					break;

				case 8:
					tty.c_cflag |= CS8;
					break;

				default:
					throw std::invalid_argument("The given bits per byte value not allowed on this platform");
			}

			switch (stopBits)
			{
				case StopBits::One:
					tty.c_cflag &= ~CSTOPB;
					break;

				case StopBits::OnePlusHalf:
				case StopBits::Two:
					tty.c_cflag |= CSTOPB;
					break;
			}

			::tcflush(mHandle, TCIOFLUSH);

			if (::tcsetattr(mHandle, TCSANOW, &tty) != 0)
			{
				throw std::system_error(errno, std::system_category());
			}
		}
		else
		{
			throw std::invalid_argument("Serial is not open");
		}
	}

	std::string Serial::detectOpenSerialPort()
	{
		std::string path;
		// TODO better search
		//this->openSerialPort(i);
		this->open("/dev/ttyUSB0");
		path = "/dev/ttyUSB0";

		if (mHandle == CIO_INVALID_DEVICE)
		{
			throw std::runtime_error("No available serial ports");
		}

		return path;
	}

#else
#error "No cio::file::Serial backend available on your platform, please implement one"
#endif
}

