/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Exception.h"

#include "Print.h"

#include <ostream>

namespace cio
{
	Exception::Exception() noexcept :
		mStaticMessage(nullptr)
	{
		// nothing more to do
	}
	
	Exception::Exception(Action action, Reason reason) noexcept :
		mStaticMessage(nullptr)
	{
		mProgress.fail(action, reason);
	}
	
	Exception::Exception(State status) noexcept :
		mStaticMessage(nullptr),
		mProgress(status)
	{
		// nothing more to do
	}
	
	Exception::Exception(State status, std::uint64_t bytes) noexcept :
		mStaticMessage(nullptr),
		mProgress(status, bytes)
	{
		// nothing more to do
	}
	
	Exception::Exception(State status, const char *message) noexcept :
		mStaticMessage(message),
		mProgress(status)
	{
		// nothing more to do
	}
	
	Exception::Exception(Reason reason, const char *message) noexcept :
		mStaticMessage(message)
	{
		mProgress.fail(reason);
	}

	Exception::Exception(Action action, Reason reason, const char *message) noexcept :
		mStaticMessage(message)
	{
		mProgress.fail(action, reason);
	}

	Exception::Exception(Progress<std::uint64_t> result) noexcept :
		mStaticMessage(nullptr),
		mProgress(result)
	{
		// nothing more to do
	}

	Exception::Exception(Progress<std::uint64_t> result, const char *message) noexcept :
		mStaticMessage(message),
		mProgress(result)
	{
		// nothing more to do
	}

	Exception::Exception(const Exception &e) = default;

	Exception::Exception(Exception &&e) noexcept :
		mStaticMessage(e.mStaticMessage),
		mDynamicMessage(std::move(e.mDynamicMessage)),
		mProgress(e.mProgress)
	{
		e.clear();
	}

	Exception &Exception::operator=(const Exception &e) = default;

	Exception &Exception::operator=(Exception &&e) noexcept
	{
		if (this != &e)
		{
			mStaticMessage = e.mStaticMessage;
			mDynamicMessage = std::move(e.mDynamicMessage);
			mProgress = e.mProgress;
			e.clear();
		}
		
		return *this;
	}

	Exception::~Exception() noexcept
	{
		// nothing to do
	}

	void Exception::clear() noexcept
	{
		mStaticMessage = nullptr;
		mDynamicMessage.clear();
		mProgress.clear();
	}

	const char *Exception::what() const noexcept
	{
		return mDynamicMessage.empty() ? (mStaticMessage ? mStaticMessage : cio::getDescription(mProgress.status)) : mDynamicMessage.c_str();
	}

	Reason Exception::reason() const noexcept
	{
		return mProgress.reason;
	}

	void Exception::reason(Reason r) noexcept
	{
		mProgress.reason = r;
	}

	Status Exception::status() const noexcept
	{
		return mProgress.status;
	}

	void Exception::status(Status status) noexcept
	{
		mProgress.status = status;
	}

	State Exception::state() const noexcept
	{
		return mProgress;
	}

	void Exception::state(State state) noexcept
	{
		mProgress = state;
	}

	const Progress<std::uint64_t> &Exception::progress() const noexcept
	{
		return mProgress;
	}

	void Exception::progress(const Progress<std::uint64_t> &result) noexcept
	{
		mProgress = result;
	}

	const std::string &Exception::getMessage() const noexcept
	{
		return mDynamicMessage;
	}

	void Exception::setMessage(std::string message) noexcept
	{
		mDynamicMessage = std::move(message);
	}

	const char *Exception::getStaticMessage() const noexcept
	{
		return mStaticMessage;
	}

	void Exception::setStaticMessage(const char *message) noexcept
	{
		mStaticMessage = message;
	}

	void Exception::succeed() noexcept
	{
		mProgress.succeed();
		mStaticMessage = nullptr;
		mDynamicMessage.clear();
	}

	void Exception::fail(Reason reason, std::string message) noexcept
	{
		mProgress.fail(reason);
		mStaticMessage = nullptr;
		mDynamicMessage = std::move(message);
	}

	Exception::operator bool() const noexcept
	{
		return mProgress.failed();
	}

	std::size_t Exception::print(char *buffer, std::size_t len) const noexcept
	{
		std::size_t needed = 0;
		std::size_t offset = 0;

		if (mStaticMessage)
		{
			std::size_t staticLen = std::strlen(mStaticMessage);
			needed += staticLen;
			std::size_t toCopy = std::min(len, staticLen);

			if (toCopy > 0)
			{
				std::memcpy(buffer, mStaticMessage, toCopy);
				offset += toCopy;
			}
		}

		if (mProgress.status != Status::None)
		{
			bool hadStatic = (needed > 0);

			if (hadStatic)
			{
				if (offset < len)
				{
					buffer[offset++] = ' ';

					if (offset < len)
					{
						buffer[offset++] = '(';
					}
				}

				needed += 2;
			}

			const char *statusText = cio::print(mProgress.status);
			std::size_t statusLen = std::strlen(statusText);
			needed += statusLen;
			std::size_t toCopy = std::min(len - offset, statusLen);

			if (toCopy > 0)
			{
				std::memcpy(buffer + offset, statusText, toCopy);
				offset += toCopy;
			}

			if (hadStatic)
			{
				if (offset < len)
				{
					buffer[offset++] = ')';
				}

				needed += 2;
			}
		}

		if (mProgress.status != Status::None || mProgress.count > 0 || mDynamicMessage.c_str())
		{
			if (needed > 0)
			{
				if (offset < len)
				{
					buffer[offset++] = ':';
				}

				++needed;
			}

			bool processedBytes = (mProgress.count > 0);

			if (processedBytes)
			{
				std::size_t printed = cio::print(mProgress.count, buffer + offset, len - offset);
				needed += printed;
				offset = std::min(offset + printed, len);
				const char *processedText = " bytes processed";
				std::size_t processedTextLen = sizeof(processedText) - 1;
				needed += processedTextLen;
				std::size_t toCopy = std::min(len - offset, processedTextLen);

				if (toCopy > 0)
				{
					std::memcpy(buffer + offset, processedText, toCopy);
					offset += toCopy;
				}
			}

			if (!mDynamicMessage.empty())
			{
				if (processedBytes)
				{
					if (offset < len)
					{
						buffer[offset++] = ' ';
					}

					needed += 1;
				}

				needed += mDynamicMessage.size();
				std::size_t toCopy = std::min(mDynamicMessage.size(), len - offset);

				if (toCopy > 0)
				{
					std::memcpy(buffer + offset, mDynamicMessage.c_str(), toCopy);
					offset += toCopy;
				}
			}
		}

		// Finally processed all items, pad remainder of buffer with null if needed
		if (offset < len)
		{
			std::memset(buffer, 0, len - offset);
		}

		return needed;
	}

	std::string Exception::print() const
	{
		std::size_t needed = this->print(nullptr, 0);
		std::string text;
		text.resize(needed);
		this->print(&(text[0]), text.size());
		return text;
	}

	std::ostream &operator<<(std::ostream &stream, const Exception &e)
	{
		return stream << e.print();
	}
}
