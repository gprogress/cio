/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Outcome.h"

#include "Enumeration.h"

#include <istream>
#include <ostream>

namespace cio
{
	const Enumerant<Outcome> sOutcomeEntries[] =
	{
		{ Outcome::None, "None", "No outcome occurred" },
		{ Outcome::Completed, "Completed", "The operation completed" },
		{ Outcome::Succeeded, "Succeeded", "The operation completed successfully" },
		{ Outcome::Failed, "Failed", "The operation failed to complete" },
		{ Outcome::Abandoned, "Abandoned", "The operation was abandoned before completion" }
	};

	const char *print(Outcome outcome) noexcept
	{
		return sOutcomeEntries[static_cast<unsigned>(outcome)].label;
	}
	
	const char *getDescription(Outcome outcome) noexcept
	{
		return sOutcomeEntries[static_cast<unsigned>(outcome)].description;
	}

	std::ostream &operator<<(std::ostream &s, Outcome outcome)
	{
		return s << sOutcomeEntries[static_cast<unsigned>(outcome)].label;
	}
}
