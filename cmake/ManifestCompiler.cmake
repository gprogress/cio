#######################################################################################################################
# Copyright 2023, Geometric Progress LLC
#
# This file is part of the Careful I/O Library (CIO).
#
# CIO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CIO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CIO.  If not, see <https://www.gnu.org/licenses/>
#######################################################################################################################
function(MANIFEST_COMPILER_ID OUTPUT_VAR)
	set(MANIFEST_OS )
	set(MANIFEST_ARCH )
	set(MANIFEST_COMPILER ) 

	if(WIN32)
		set(MANIFEST_OS "windows")

		# Determine processor type and pointer size
		if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "ARM")
			if(CMAKE_SIZEOF_VOID_P EQUAL 4)
				set(MANIFEST_ARCH "arm")
			elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
				set(MANIFEST_ARCH "arm64")
			endif()
		else()
			if(CMAKE_SIZEOF_VOID_P EQUAL 4)
				set(MANIFEST_ARCH "x86")
			elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
				set(MANIFEST_ARCH "x64")
			endif()
		endif()
		
		set(MANIFEST_SYSTEM ${MANIFEST_OS}-${MANIFEST_ARCH})
		
		# Match VS versions specifically
		if(MSVC_TOOLSET_VERSION EQUAL 141)
			set(MANIFEST_COMPILER "vs2017")
		elseif(MSVC_TOOLSET_VERSION EQUAL 142)
			set(MANIFEST_COMPILER "vs2019")
		elseif(MSVC_TOOLSET_VERSION GREATER 142)
			set(MANIFEST_COMPILER "vs2022")
		elseif(CMAKE_CXX_COMPILER_ID)
			# Determine if it's clang, GCC, or something else
			if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "clang")
				set(MANIFEST_CXX "clang")
			elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
				set(MANIFEST_CXX "gcc")
			else()
				string(TOLOWER ${CMAKE_CXX_COMPILER_ID} MANIFEST_CXX)
			endif()

			# Determine ABI and subsystem
			if(MINGW)
				set(MANIFEST_ABI "mingw64")
				set(MANIFEST_COMPILER "mingw64-${MANIFEST_CXX}")
			elseif(CYGWIN)
				set(MANIFEST_ABI "cygwin")
				set(MANIFEST_COMPILER "cygwin-${MANIFEST_CXX}")
			elseif(MSYS)
				set(MANIFEST_ABI "msys")
				set(MANIFEST_COMPILER "msys-${MANIFEST_CXX}")
			else()
				set(MANIFEST_ABI "ucrt")
				set(MANIFEST_COMPILER "${MANIFEST_CXX}")
			endif()
			message(STATUS "Detected Compiler ABI: ${MANIFEST_COMPILER}")
		endif()	

		set("${OUTPUT_VAR}" "${MANIFEST_OS}-${MANIFEST_ARCH}/${MANIFEST_COMPILER}" PARENT_SCOPE)
	else()
		set("${OUTPUT_VAR}" "${CMAKE_LIBRARY_ARCHITECTURE}" PARENT_SCOPE)
	endif()
endfunction()

function(MANIFEST_DETECT_ROOT)
	set(MANIFEST_DEFAULT )
	set(MANIFEST_SYSTEM )
	MANIFEST_COMPILER_ID(MANIFEST_SYSTEM)
	message(STATUS "Manifest System: ${MANIFEST_SYSTEM}")

	if(WIN32)
		set(MANIFEST_DEFAULT "C:/Packages")
	else()
		set(MANIFEST_DEFAULT "~/Packages")
	endif()
	
	# Default value of root
	set(MANIFEST_DEFAULT_ROOT "${MANIFEST_DEFAULT}/${MANIFEST_SYSTEM}")

	# Set variable, appending force is passed as argument
	set(MANIFEST_ROOT "${MANIFEST_DEFAULT_ROOT}" CACHE PATH "Path to manifest packages root folder" ${ARGN})
endfunction()

macro(MANIFEST_PACKAGE_ROOT PACKAGE NAME VERSION)
	if(MANIFEST_ROOT)
		set(${PACKAGE}_ROOT "${MANIFEST_ROOT}/${NAME}/${VERSION}" CACHE PATH "Path to ${PACKAGE} root folder")
	endif()
	set(${PACKAGE}_MANIFEST_FOLDER ${NAME})
	set(${PACKAGE}_MANIFEST_VERSION ${VERSION})
endmacro()
