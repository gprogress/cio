/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Characteristic.h"

#include "AttributeClient.h"
#include "Property.h"
#include "Subscription.h"
#include "TypeId.h"

#include <cio/Class.h>

#include <algorithm>

namespace cio
{
	namespace bluetooth
	{		
		Class<Characteristic> Characteristic::sMetaclass("cio::bluetooth::Characteristic");
		
		Characteristic::Characteristic() noexcept :
			Attribute(TypeId::Characteristic)
		{
			// nothing more to do
		}
		
		Characteristic::Characteristic(std::uint16_t handle) noexcept :
			Attribute(TypeId::Characteristic, handle)
		{
			// nothing more to do
		}
				
		Characteristic::Characteristic(std::uint16_t handle, AttributeClient *client) noexcept :
			Attribute(TypeId::Characteristic, handle, client)
		{
			// nothing more to do
		}
				
		Characteristic::Characteristic(std::uint16_t start, std::uint16_t end) noexcept :
			Attribute(TypeId::Characteristic, start, end)
		{
			// nothing more to do
		}
				
		Characteristic::Characteristic(std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept :
			Attribute(TypeId::Characteristic, start, end, client)
		{
			// nothing more to do
		}
				
		Characteristic::Characteristic(const Characteristic &in) = default;
	
		Characteristic::Characteristic(Characteristic &&in) noexcept :
			Attribute(std::move(in)),
			mDescriptors(std::move(in.mDescriptors)),
			mValue(std::move(in.mValue)),
			mProperties(in.mProperties)
		{
			// nothing more to do
		}
	
		Characteristic &Characteristic::operator=(const Characteristic &in) = default;
	
		Characteristic &Characteristic::operator=(Characteristic &&in) noexcept
		{
			if (this != &in)
			{
				Attribute::operator=(std::move(in));
				mDescriptors = std::move(in.mDescriptors);
				mValue = std::move(in.mValue);
				mProperties = in.mProperties;
			}
			
			return *this;
		}
	
		Characteristic::~Characteristic() noexcept = default;
		
		void Characteristic::clear() noexcept
		{
			mDescriptors.clear();
			mValue.clear();
			mProperties.clear();
			
			Attribute::clear();
		}
		
		State Characteristic::synchronize()
		{
			State typeStatus = this->synchronizeTypeHandle();
			State declStatus = this->synchronizeDescriptors();
			
			typeStatus.outcome = std::max(typeStatus.outcome, declStatus.outcome);
			typeStatus.reason = std::min(typeStatus.reason, declStatus.reason);
			
			return typeStatus;
		}
		
		void Characteristic::validate()
		{
			Attribute::validate();
			this->validateDescriptors();
		}
		
		void Characteristic::setClient(AttributeClient *client) noexcept
		{
			Attribute::setClient(client);
			
			for (Attribute &attr : mDescriptors)
			{
				attr.setClient(client);
			}
			
			mValue.setClient(client);
		}

		std::uint16_t Characteristic::getValueHandle() const noexcept
		{
			return mValue.getHandle();
		}
						
		void Characteristic::setValueHandle(std::uint16_t value) noexcept
		{
			mValue.setHandle(value);
		}

		const UniqueId &Characteristic::getValueType() const noexcept
		{
			return mValue.getType();	
		}
		
		void Characteristic::setValueType(const UniqueId &valueType) noexcept
		{
			mValue.setType(valueType);
		}

		Text Characteristic::getValueName() const noexcept
		{
			return mValue.getName();
		}
				
		const UniqueId &Characteristic::setValueTypeByName(const Text &name)
		{
			return mValue.setTypeByName(name);
		}
				
		bool Characteristic::readable() const noexcept
		{
			return mProperties.get(Property::Read);
		}
		
		bool Characteristic::writable(bool responseNeeded, bool signatureNeeded) const noexcept
		{
			bool state = false;
			if (responseNeeded)
			{
				if (!signatureNeeded)
				{
					state = mProperties.get(Property::Write);
				}
			}
			else if (signatureNeeded)
			{
				state = mProperties.get(Property::CommandSignedWrite);
			}
			else
			{
				state = mProperties.get(Property::CommandWrite);
			}
		
			return state;
		}
		
		const Subscription *Characteristic::subscribe(AttributeCallback &&listener, std::chrono::milliseconds interval)
		{
			const Subscription *sub = nullptr;
			
			if (mClient)
			{
				sub = mClient->subscribe(mValue.getHandle(), std::move(listener), interval);
			}
			
			return sub;
		}
				
		State Characteristic::unsubscribe(const Subscription *subscription)
		{
			State status(Action::Remove);
			if (subscription && subscription->getHandle() == mValue.getHandle())
			{
				if (mClient)
				{
					status = mClient->unsubscribe(subscription);
				}
				else
				{
					status.fail(Reason::Unopened);
				}
			}
			return status;
		}
				
		Progress<std::size_t> Characteristic::unsubscribe()
		{
			Progress<std::size_t> status;
			
			if (mClient)
			{
				status = mClient->clearSubscriptions(mValue.getHandle());
			}
			
			return status;
		}
				
		bool Characteristic::notifiable() const noexcept
		{
			return mProperties.get(Property::Notify);
		}
		
		bool Characteristic::notifying() const
		{
			std::uint8_t cccd = this->getClientConfiguration();
			return (cccd & 0x01) != 0;
		}
		
		void Characteristic::requestNotification(bool value /* = true */)
		{
			std::uint8_t cccd = this->getClientConfiguration();

			if (value)
			{
				if ((cccd & 0x01) == 0)
				{
					cccd |= 0x01;
					this->setClientConfiguration(cccd);
				}
			}
			else
			{
				if ((cccd & 0x01) != 0)
				{
					cccd &= ~0x01;
					this->setClientConfiguration(cccd);
				}
			}
		}
		
		bool Characteristic::indicatable() const noexcept
		{
			return mProperties.get(Property::Indicate);
		}
				
		bool Characteristic::indicating() const
		{
			std::uint8_t cccd = this->getClientConfiguration();
			return (cccd & 0x02) != 0;
		}
		
		void Characteristic::requestIndication(bool value /* = true */)
		{
			std::uint8_t cccd = this->getClientConfiguration();
			if (value)
			{
				if ((cccd & 0x02) == 0)
				{
					cccd |= 0x02;
					this->setClientConfiguration(cccd);
				}
			}
			else
			{
				if ((cccd & 0x02) != 0)
				{
					cccd &= ~0x02;
					this->setClientConfiguration(cccd);
				}
			}
		}
		
		bool Characteristic::broadcastable() const noexcept
		{
			return mProperties.get(Property::Broadcast);
		}
		
		bool Characteristic::broadcasting() const
		{
			std::uint8_t sccd = this->getServerConfiguration();
			return (sccd & 0x01) != 0;
		}
			
		void Characteristic::requestBroadcast(bool value /* = true */)
		{
			std::uint8_t sccd = this->getClientConfiguration();
			if (value)
			{
				if ((sccd & 0x01) == 0)
				{
					sccd |= 0x01;
					this->setServerConfiguration(sccd);
				}
			}
			else
			{
				if ((sccd & 0x01) != 0)
				{
					sccd &= ~0x01;
					this->setServerConfiguration(sccd);
				}
			}
		}
		
		bool Characteristic::extended() const noexcept
		{
			return mProperties.get(Property::Extended);
		}
				
		Properties Characteristic::getProperties() const noexcept
		{
			return mProperties;
		}
		
		void Characteristic::setProperties(Properties properties) noexcept
		{
			mProperties = properties;
		}

		std::uint8_t Characteristic::getExtendedProperties() const
		{
			std::uint8_t value = 0;
			std::size_t idx = this->locateDescriptorIndex(TypeId::CharacteristicExtendedProperties);
			
			if (idx < mDescriptors.size())
			{
				value = mDescriptors[idx].read<std::uint8_t>();
			}
		
			return value;
		}
		
		std::uint8_t Characteristic::getClientConfiguration() const
		{
			std::uint8_t value = 0;
			std::size_t idx = this->locateDescriptorIndex(TypeId::ClientCharacteristicConfiguration);
			
			if (idx < mDescriptors.size())
			{
				value = mDescriptors[idx].read<std::uint8_t>();
			}
		
			return value;
		}

		void Characteristic::setClientConfiguration(std::uint8_t value)
		{
			std::size_t idx = this->locateDescriptorIndex(TypeId::ClientCharacteristicConfiguration);
			if (idx >= mDescriptors.size() && value != 0)
			{
				throw cio::Exception(Reason::Refused, "Could not update client configuration for characteristic because no descriptor for it exists");
			}
			
			mDescriptors[idx].write(value);	
		}

		std::uint8_t Characteristic::getServerConfiguration() const
		{
			std::uint8_t value = 0;
			std::size_t idx = this->locateDescriptorIndex(TypeId::ServerCharacteristicConfiguration);
			
			if (idx < mDescriptors.size())
			{
				value = mDescriptors[idx].read<std::uint8_t>();
			}
		
			return value;
		}

		void Characteristic::setServerConfiguration(std::uint8_t value)
		{
			std::size_t idx = this->locateDescriptorIndex(TypeId::ServerCharacteristicConfiguration);
			if (idx >= mDescriptors.size() && value != 0)
			{
				throw cio::Exception(Reason::Refused, "Could not update server configuration for characteristic because no descriptor for it exists");
			}
			
			mDescriptors[idx].write(value);	
		}
	 
		Text Characteristic::getUserDescription() const
		{
			Text value;
			std::size_t idx = this->locateDescriptorIndex(TypeId::CharacteristicUserDescription);
			
			if (idx < mDescriptors.size())
			{
				value = mDescriptors[idx].read<Text>();
			}
			
			return value;
		}
		
		void Characteristic::setUserDescription(const Text &text)
		{
			std::size_t idx = this->locateDescriptorIndex(TypeId::CharacteristicUserDescription);
			if (idx >= mDescriptors.size() && text.strlen() > 0)
			{
				throw cio::Exception(Reason::Refused, "Could not update user description for characteristic because no descriptor for it exists");
			}
			
			mDescriptors[idx].write(text);	
		}
		
		bool Characteristic::empty() const noexcept
		{
			return mDescriptors.empty();
		}
				
		std::size_t Characteristic::size() const noexcept
		{
			return mDescriptors.size();
		}
				
		Characteristic::iterator Characteristic::begin() noexcept
		{
			return mDescriptors.data();
		}
				
		Characteristic::iterator Characteristic::end() noexcept
		{
			return mDescriptors.data() + mDescriptors.size();
		}
				
		Characteristic::const_iterator Characteristic::begin() const noexcept
		{
			return mDescriptors.data();
		}
				
		Characteristic::const_iterator Characteristic::end() const noexcept
		{
			return mDescriptors.data() + mDescriptors.size();
		}	
		
		Descriptor &Characteristic::operator[](std::size_t idx)
		{
			return mDescriptors[idx];
		}
				
		const Descriptor &Characteristic::operator[](std::size_t idx) const
		{
			return mDescriptors[idx];
		}
		
		std::vector<Descriptor> &Characteristic::getDescriptors() noexcept
		{		
			return mDescriptors;
		}
				
		const std::vector<Descriptor> &Characteristic::getDescriptors() const noexcept
		{			
			return mDescriptors;
		}
				
		Descriptor &Characteristic::getDescriptor(std::uint16_t handle)
		{
			if (handle == mValue.getHandle())
			{
				return mValue;
			}
			else
			{
				if (mDescriptors.empty() && mClient)
				{
					mDescriptors = this->discoverDescriptors();
				}
			
				std::size_t idx = this->locateDescriptorIndex(handle);
				
				if (idx >= mDescriptors.size())
				{
					throw cio::Exception(Reason::Missing, "No attribute with specified handle in characteristic");
				}
				
				return mDescriptors[idx];
			}
		}
							
		const Descriptor &Characteristic::getDescriptor(std::uint16_t handle) const
		{
			if (handle == mValue.getHandle())
			{
				return mValue;
			}
			else
			{
				std::size_t idx = this->locateDescriptorIndex(handle);
				
				if (idx >= mDescriptors.size())
				{
					throw cio::Exception(Reason::Missing, "No descriptor with specified handle in characteristic");
				}
				
				return mDescriptors[idx];
			}
		}
		
		Descriptor &Characteristic::getDescriptor(const UniqueId &type) 
		{
			Descriptor *result = nullptr;
		
			// Special cases
			if (type == mValue.getType())
			{
				result = &mValue;
			}
			else if (!type.isGroupType())
			{
				std::size_t idx = this->locateDescriptorIndex(type);
				
				if (idx >= mDescriptors.size())
				{
					throw cio::Exception(Reason::Missing, "No descriptor with specified type in characteristic");
				}
				
				result = &mDescriptors[idx];
			}
			else
			{
					throw cio::Exception(Reason::Invalid, "Non-descriptor group type specified for descriptor");
			}
			
			return *result;
		}
		
		const Descriptor &Characteristic::getDescriptor(const UniqueId &type) const
		{
			const Descriptor *result = nullptr;
			
			// Special cases			
			if (type == mValue.getType())
			{
				result = &mValue;
			}
			else if (!type.isGroupType())
			{	
				std::size_t idx = this->locateDescriptorIndex(type);
				
				if (idx >= mDescriptors.size())
				{
					throw cio::Exception(Reason::Missing, "No descriptor with specified type in characteristic");
				}
				
				result = &mDescriptors[idx];
			}
			else
			{
				throw cio::Exception(Reason::Invalid, "Non-descriptor group type specified for descriptor");
			}
				
			return *result;
		}
		
		Characteristic::iterator Characteristic::find(const UniqueId &type) noexcept
		{
			return mDescriptors.data() + this->locateDescriptorIndex(type);
		}
				
		Characteristic::const_iterator Characteristic::find(const UniqueId &type) const noexcept
		{
			return mDescriptors.data() + this->locateDescriptorIndex(type);
		}
								
		Characteristic::iterator Characteristic::find(std::uint16_t handle) noexcept
		{
			return mDescriptors.data() + this->locateDescriptorIndex(handle);
		}
				
		Characteristic::const_iterator Characteristic::find(std::uint16_t handle) const noexcept
		{	
			return mDescriptors.data() + this->locateDescriptorIndex(handle);		
		}
				
		std::size_t Characteristic::locateDescriptorIndex(std::uint16_t handle) const noexcept
		{
			std::size_t idx = mDescriptors.size();
			
			if (handle > mStartHandle && handle <= mEndHandle)
			{
				std::size_t low = 0;
				std::size_t high = mDescriptors.size();
				std::size_t current = (low + high) / 2;
				
				while (current < high)
				{
					const Descriptor &candidate = mDescriptors[current];
					std::uint16_t test = candidate.getHandle();
					if (test == handle)
					{
						idx = current;
						break;
					}
					else if (test < handle)
					{
						low = current + 1;
					}
					else
					{
						high = current;
					}
					
					current = (low + high) / 2;
				}
			}
			
			return idx;
		}
		
		std::pair<std::size_t, std::size_t> Characteristic::locateDescriptorRange(std::uint16_t start, std::uint16_t end) const noexcept
		{
			std::pair<std::size_t, std::size_t> range(0, 0);
			
			// Make sure range interects at all
			if (end > mStartHandle && start <= mEndHandle)
			{
				// Do a binary search, but biased at start to assume contiguous handles
				// Normally this will find it on the first loop iteration
				std::size_t low = 0;
				std::size_t high = mDescriptors.size();
				std::size_t current = (low + high) / 2;
				
				while (current < high)
				{
					const Descriptor &candidate = mDescriptors[current];
					std::uint16_t handle = candidate.getHandle();
					if (start == handle)
					{
						break;
					}
					else if (handle < start)
					{
						low = current + 1;
					}
					else
					{
						high = current;
					}
					
					current = (low + high) / 2;
				}
				
				range.first = current;
				
				low = high;
				high = mDescriptors.size();
				current = (low + high) / 2;
				
				while (current < high)
				{
					const Descriptor &candidate = mDescriptors[current];
					std::uint16_t handle = candidate.getHandle();
					if (end == handle)
					{
						++current;
						break;
					}
					else if (handle < end)
					{
						low = current + 1;
					}
					else
					{
						high = current;
					}
					
					current = (low + high) / 2;
				}
				
				range.second = current;
			}
			
			return range;
		}
		
		std::size_t Characteristic::locateDescriptorIndex(const UniqueId &type) const noexcept
		{
			std::size_t i = 0;
		
			while (i < mDescriptors.size() && mDescriptors[i].getType() != type)
			{
				++i;
			}

			return i;
		}
		
		void Characteristic::setDescriptors(const std::vector<Descriptor> &attrs)
		{
			mDescriptors = attrs;
			
			for (Descriptor &a : mDescriptors)
			{
				a.setClient(mClient);
			}	
			
			mContentDiscovered = true;
		}
				
		void Characteristic::setDescriptors(std::vector<Descriptor> &&attrs) noexcept
		{
			mDescriptors = std::move(attrs);
			
			for (Descriptor &a : mDescriptors)
			{
				a.setClient(mClient);
			}
			
			mContentDiscovered = true;
		}
		
		void Characteristic::validateDescriptors()
		{
			// Ensure list is sorted
			std::sort(mDescriptors.begin(), mDescriptors.end());
			
			// Remove out of range before characteristic start handle
			for (std::size_t i = 0; i < mDescriptors.size(); ++i)
			{
				if (mDescriptors[i].getHandle() > mStartHandle)
				{
					mDescriptors.erase(mDescriptors.begin(), mDescriptors.begin() + i);
					break;
				}
			}
			
			// make sure we have no group types in this list
			for (std::size_t g = 0; g < mDescriptors.size(); ++g)
			{
				// Found another service or characteristic in here, clip off everything starting with it
				if (mDescriptors[g].getType().isGroupType())
				{
					mDescriptors.erase(mDescriptors.begin() + g, mDescriptors.end());
					break;
				}
			}
			
			// set end handle if not set from last descriptor, or value handle if none
			if (!mEndHandle)
			{
				if (!mDescriptors.empty())
				{
					mEndHandle = mDescriptors.back().getHandle();
				}
				else
				{
					mEndHandle = mValue.getHandle();
				}
			}
			else
			{
				// Remove out of range after previously set end handle
				for (std::size_t j = mDescriptors.size(); j > 0; --j)
				{
					if (mDescriptors[j - 1].getHandle() < mEndHandle)
					{
						mDescriptors.erase(mDescriptors.begin() + j, mDescriptors.end());
						break;
					}
				}
			}
		}
				
		void Characteristic::clearDescriptors() noexcept
		{
			mDescriptors.clear();
			mContentDiscovered = false;
		}		
			
		std::uint16_t Characteristic::discoverEndHandle() const
		{	
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "No client available to discover characteristic end handle");
			}
			
			std::uint16_t endHandle = 0xFFFFu;
			std::uint16_t previousEnd = mValue.getHandle();
			State found;
			
			do
			{
				std::vector<Attribute> attrs = mClient->discoverAttributes(previousEnd + 1, 0xFFFFu);
				
				if (!attrs.empty())
				{
					previousEnd = attrs.back().getHandle();

					for (const Attribute &att : attrs)
					{
						if (att.getType().isGroupType())
						{
							endHandle = att.getHandle() - 1;
							found.succeed();
							break;
						}
					}
				}
				else
				{
					found.succeed();
				}

			} while (!found.completed() && previousEnd < 0xFFFFu);
			
			return endHandle;
		}
		
		std::vector<Descriptor> Characteristic::discoverDescriptors() const
		{	
			std::vector<Descriptor> attributes;
		
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "No client available to discover characteristic descriptors");
			}
			
			// The end handle may or may not have been set yet
			std::uint16_t end = mEndHandle ? mEndHandle : 0xFFFFu;
			std::uint16_t previousEnd = mStartHandle;
			State found;
			
			do
			{
				std::vector<Descriptor> current = mClient->discoverDescriptors(previousEnd + 1, end);
							
				if (!current.empty())
				{
					previousEnd = current.back().getHandle();

					for (std::size_t i = 0; i < current.size(); ++i)
					{
						Descriptor &att = current[i];
					
						if (att.getType().isGroupType())
						{
							found.succeed();
							current.erase(current.begin() + i, current.end());
							break;
						}
						
						att.setClient(mClient);
					}
					
					if (attributes.empty())
					{
						attributes = std::move(current);
					}
					else
					{
						attributes.reserve(attributes.size() + current.size());
						for (Descriptor &att : current)
						{
							attributes.emplace_back(std::move(att));
						}
					}
				}
				else
				{
					found.succeed();
				}
				
			} while (!found.completed() && previousEnd < end);

			return attributes;
		}			
		
		State Characteristic::synchronizeDescriptors()
		{
			State status;
			if (!mContentDiscovered)
			{
				if (mClient)
				{
					this->synchronizeTypeHandle();
					mDescriptors = this->discoverDescriptors();
					mContentDiscovered = true;
					
					// Set end handle from descriptors if not already set
					if (!mEndHandle)
					{
						if (!mDescriptors.empty())
						{
							mEndHandle = mDescriptors.back().getHandle();
						}
						else
						{
							mEndHandle = mValue.getHandle();
						}
					}
					
					status.succeed();
				}
				else
				{
					status.fail(Reason::Unopened);
				}
			}
			else
			{
				status.succeed(Reason::Exists);
			}
			
			return status;
		}

		Descriptor &Characteristic::getValueDescriptor() noexcept
		{
			return mValue;
		}
		
		const Descriptor &Characteristic::getValueDescriptor() const noexcept
		{
			return mValue;
		}
		
		void Characteristic::setValueDescriptor(const Descriptor &handle)
		{
			mValue = handle;
		}
		
		std::size_t Characteristic::readValue(const AttributeCallback &listener) const
		{
			return mValue.read(listener);
		}
				
		std::size_t Characteristic::readValue(void *data, std::size_t length) const
		{
			return mValue.read(data, length);
		}
		
		std::size_t Characteristic::writeValue(const void *data, std::size_t length)
		{
			return mValue.write(data, length);
		}
				
		std::size_t Characteristic::sendValue(const void *data, std::size_t length)
		{
			return mValue.send(data, length);
		}
	}
}

