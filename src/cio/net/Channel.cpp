/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Channel.h"

#include <cio/Class.h>

namespace cio
{
	namespace net
	{
		Class<Channel> Channel::sMetaclass("cio::net::Channel");

		const Metaclass &Channel::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		Channel::Channel() noexcept
		{
			// nothing more to do
		}

		Channel::Channel(Channel &&in) noexcept :
			cio::Channel(std::move(in))
		{
			mSocket = std::move(in.mSocket);
		}

		Channel::Channel(Socket &&socket) noexcept :
			mSocket(std::move(socket))
		{
			// nothing more to do
		}

		Channel &Channel::operator=(Channel &&in) noexcept
		{
			cio::Channel::operator=(std::move(in));
			mSocket = std::move(in.mSocket);
			return *this;
		}

		Channel::~Channel() noexcept
		{
			// nothing more to do
		}

		const Metaclass &Channel::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Channel::clear() noexcept
		{
			mSocket.clear();
		}

		bool Channel::isOpen() const noexcept
		{
			return static_cast<bool>(mSocket);
		}

		std::size_t Channel::getMaxDatagramSize() const noexcept
		{
			return 65535;
		}

		Progress<std::size_t> Channel::requestRead(void *buffer, std::size_t length) noexcept
		{
			return mSocket.receive(buffer, length);
		}

		Progress<std::size_t> Channel::requestWrite(const void *buffer, std::size_t length) noexcept
		{
			return mSocket.send(buffer, length);
		}

		Datagram Channel::receive()
		{
			Datagram datagram;
			Buffer &buffer = datagram.getPacket();
			buffer.allocate(this->getMaxDatagramSize());
			buffer.reset();
			this->receive(datagram);
			return datagram;
		}

		Progress<std::size_t> Channel::receive(Datagram &datagram) noexcept
		{
			Buffer &buffer = datagram.getPacket();
			std::size_t mark = buffer.position();
			Progress<std::size_t> result = mSocket.receiveFrom(datagram);
			std::size_t limit = buffer.position();
			buffer.reslice(mark, limit);
			return result;
		}

		Progress<std::size_t> Channel::send(Datagram &datagram) noexcept
		{
			Buffer &buffer = datagram.getPacket();
			Progress<std::size_t> result = mSocket.sendTo(datagram);
			return result;
		}
	
		State Channel::setReadTimeout(std::chrono::milliseconds timeout)
		{
			mSocket.setReceiveTimeout(timeout);
			return cio::succeed(Action::Metadata);
		}
				
		std::chrono::milliseconds Channel::getReadTimeout() const
		{
			return mSocket.getReceiveTimeout();
		}
				
		void Channel::setWriteTimeout(std::chrono::milliseconds timeout)
		{
			mSocket.setSendTimeout(timeout);
		}
				
		std::chrono::milliseconds Channel::getWriteTimeout() const
		{
			return mSocket.getSendTimeout();	
		}		
	}
}




