/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_FLOW_H
#define CIO_NET_FLOW_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace net
	{
		/**
		* The CIO Net Flow enumeration describes whether network connectivity is 1:1 (Unicast), 1:Many (Multicast),
		* or everything on the IPv4 subnet (Broadcast). Modes other than Unicast require additional socket
		* configuration to be useful.
		*/
		enum class Flow : std::uint8_t
		{
			/** No flow type */
			None,
			
			/** Unicast flow - direct peer to peer */
			Unicast,
			
			/** Broadcast flow - mediated by entire local subnetwork for distribution to all endpoints */
			Broadcast,
			
			/** Multicast flow - mediated by a multicast group for distribution to all endpoints */
			Multicast,

			/** Anycast flow - mediated by multicast group for distribution to first reachable endpoint */
			Anycast
		};


		/**
		* Gets a text string representing the socket type.
		*
		* @param type The socket type
		* @return the text string
		*/
		CIO_NET_API const char *print(Flow type) noexcept;

		/**
		* Prints the flow type to a C++ stream.
		*
		* @param stream The stream
		* @param type The flow type
		* @return the stream
		*/
		CIO_NET_API  std::ostream &operator<<(std::ostream &stream, Flow type);
	}
}

#endif

