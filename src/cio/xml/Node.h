/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_XML_NODE_H
#define CIO_XML_NODE_H

#include "Types.h"

#include <cio/Text.h>

#include <memory>
#include <string>
#include <vector>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace xml
	{
		/**
		* XML Tree Node.  This is a node in an XML parse tree.
		* The root node is always of type Type::Document.
		*/
		class CIO_XML_API Node
		{
			public:
				Node() noexcept;

				/**
				* Construct a tree node of a given type.
				* @param type The type of tree node to construct
				*/
				explicit Node(Type type) noexcept;

				Node(Type type, std::string text);

				/**
				* Construct a copy of a tree node.
				*
				* @param in The node to copy
				* @param deep Whether to do a deep copy
				*/
				Node(const Node &in);

				Node(Node &&in) noexcept;

				Node &operator=(const Node &in);

				Node &operator=(Node &&in) noexcept;

				/**
				* Destructor.
				*/
				~Node() noexcept;

				void clear() noexcept;

				/**
				* Gets the text value of this node.  For element nodes, this is the element name.
				* For text nodes, this is the text value.
				*
				* @return the text value
				*/
				const std::string &getText() const noexcept;

				/**
				* Sets the text value of this node.  For element nodes, this is the element name.
				* For text nodes, this is the text value.
				*
				* @param value The new text value
				*/
				void setText(std::string value);

				/**
				* Sets the text value of this node from a non-null terminated chunk of text.
				* For element nodes, this is the element name.
				* For text nodes, this is the text value.
				*
				* @param text The text
				* @param len The text length
				*/
				void setText(Text value);

				/**
				* Appends text to the text value of this node.
				* @param value The text to append
				*/
				void appendText(Text value);

				Node &addChild();

				/**
				* Appends the given node to the end of this node's children.
				* @param child The child to add
				*/
				Node &addChild(Node child);

				Node &addChild(Type type);

				Node &addChild(Type type, std::string value);

				/**
				* Removes and returns a child from this node.
				* @param idx The index of the child
				* @return the removed child
				*/
				bool removeChild(unsigned idx);

				/**
				* Clear this node of all children.
				*/
				void clearChildren() noexcept;

				/**
				* Gets the child at the given position.
				* @param idx The child's index
				* @return the child
				*/
				Node &getChild(unsigned idx);

				/**
				* Gets the child at the given position.
				* @param idx The child's index
				* @return the child
				*/
				const Node &getChild(unsigned idx) const;

				/**
				* @param name of the child to be returned
				* @return the child if it exists, NULL if it does not
				*/
				Node &getChild(Text name);

				Node &getFirstChild();

				const Node &getFirstChild() const;

				Node &getLastChild();

				const Node &getLastChild() const;

				/**
				* Gets the number of children.
				* @return the number of children
				*/
				std::size_t getChildCount() const noexcept;

				std::size_t getAttributeCount() const noexcept;

				std::pair<std::string, std::string> &getAttribute(std::size_t i);

				const std::pair<std::string, std::string> &getAttribute(std::size_t i) const;

				Text getAttributeValue(Text key) const;

				void addAttribute(Text key, Text value);

				void addAttribute(std::string key, std::string value);

				void addAttribute(std::pair<std::string, std::string> attribute);

				void removeAttribute(std::size_t i);

				void clearAttributes() noexcept;

				/**
				* Gets the value type of this child.
				* @return the value type
				*/
				inline Type getType() const noexcept;

				inline void setType(Type type) noexcept;

				/**
				* Checks whether this node is an element node.
				* @return whether this node is an element node
				*/
				inline bool isElement() const noexcept;

				/**
				* Checks whether this node is a text node.
				* @return whether this node is a text node
				*/
				inline bool isText() const noexcept;

				/**
				* Merges text child nodes that are adjacent.
				*/
				void mergeAdjacentTextChildren();

			private:
				std::string mValue;
				std::vector<std::pair<std::string, std::string>> mAttrs;
				std::vector<Node> mChildren;
				Type mType;
		};
	}
}

/* Inline implementation */

namespace cio
{
	namespace xml
	{
		inline Type Node::getType() const noexcept
		{
			return mType;
		}

		inline void Node::setType(Type type) noexcept
		{
			mType = type;
		}

		inline bool Node::isElement() const noexcept
		{
			return mType == Type::Element;
		}

		inline bool Node::isText() const noexcept
		{
			return mType == Type::Text;
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
