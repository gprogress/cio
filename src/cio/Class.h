/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CLASS_H
#define CIO_CLASS_H

#include "Metaclass.h"

#include "Metatypes.h"

#include <cstring>

namespace cio
{
	/**
	 * The Class template extends the Metaclass polymorphically to set up the base class members by compile-time detection of class features.
	 * It can automatically detect the size, alignment, type info, constructor methods, assignment methods, and destructor method.
	 *
	 * If the qualified name is provided at construction, it can be used to determine class name, namespace list, and template parameter list
	 * (although note there is no easy way to generate the template paramter list at compile time without dynamic memory allocation).
	 *
	 * Generally each polymorphic class should define one static instance of Class<T> for that class type, and expose it via a static method
	 * to obtain it based on the class name and by a virtual method to obtain it polymorphically at runtime given an object instance.
	 *
	 * This implementation shoudl be suitable for the vast majority of classes, structs, and primitive types, but it can be subclassed to further
	 * extend it or override default behaviors.
	 *
	 * @tparam<T> The type of class to configure a Metaclass for
	 */
	template <typename T>
	class Class : public Metaclass
	{
		public:
			/**
			 * Construct the metaclass instance with the proper values for all things that can be automatically derived.
			 * Note that this cannot automatically populate the class name, namespaces, superclasses, or template parameters (yet).
			 */
			Class();

			/**
			 * Construct the metaclass instance with the proper values for all things that can be automatically derived
			 * plus all of the specified reflection information.
			 *
			 * This constructor can be used to provide the fully qualified class name which will configure all fields that can
			 * be derived frmo it.
			 *
			 * @param name The C++ name of the class
			 */
			explicit Class(const char *qname);
			
			/**
			 * Destructor.
			 */
			virtual ~Class() noexcept override;
			
			/** Method to perform default construction of this class on a sufficiently large buffer */
			virtual bool construct(void *buffer, std::size_t count) const override;

			/** Method to perform copy construction of this class on a sufficiently large buffer */
			virtual bool copy(const void *input, void *output, std::size_t count) const override;

			/** Method to perform move construction of this class on a sufficiently large buffer */
			virtual bool move(void *input, void *output, std::size_t count) const noexcept override;

			/** Method to perform copy assignment of this class on previously constructed buffers */
			virtual bool assign(const void *input, void *output, std::size_t count) const override;

			/** Method to perform move assignment of this class on previously constructed buffers */
			virtual bool transfer(void *input, void *output, std::size_t count) const noexcept override;

			/** Method to perform desruction of this class on a previously constructed buffer */
			virtual bool destroy(void *buffer, std::size_t count) const noexcept override;			
	};
	
	/**
	 * Specialization for Class<void> to bypass detection logic involving void and just directly use Metaclass operations
	 * with compile-time values.
	 */
	template <>
	class Class<void> : public Metaclass
	{
		public:
			/**
			 * Construct the metaclass instance for void.
			 */
			inline Class(const char *name);
			
			/**
			 * Destructor.
			 */
			inline virtual ~Class() noexcept override = default;		
	};
	
	/**
	 * The reflect namespace provides the reflection machinery necessary to detect whether constructors, assignment operators, and the destructor
	 * are accessible and if so bind them properly to a Metaclass. These are detail methods that most users will not need to use directly but instead can
	 * call via configured Class objects.
	 */
	namespace reflect
	{
		/**
		 * Perform default construction of the given class on a sufficiently large uninitialized buffer.
		 *
		 * @tparam <T> The class being constructed
		 * @param buffer The uninitialized buffer to construct a T object into
		 * @param count The number of objects to construct
		 * @param possible The tag indicating that this type is default constructable
		 * @param trivial The tag indicating that this type is trivial and can use std::memset
		 * @return the Created bool indicating success
		 */
		template <typename T>
		inline std::true_type construct(void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
					
		/**
		 * Perform default construction of the given class on a sufficiently large uninitialized buffer.
		 *
		 * @tparam <T> The class being constructed
		 * @param buffer The uninitialized buffer to construct a T object into
		 * @param count The number of objects to construct
		 * @param possible The tag indicating that this type is default constructable
		 * @param nontrivial The tag indicating that this type is nontrival and must use the constructor
		 * @return the Created bool indicating success
		 */
		template <typename T>
		inline std::true_type construct(void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial);
		
		/**
		 * Placeholder to do nothing if type T does not support default construction.
		 *
		 * @tparam <T> The class being constructed
		 * @param buffer The uninitialized buffer to construct a T object into
		 * @param count The number of objects to construct
		 * @param impossible The tag indicating this type is not default constructible
		 * @param nontrivial The tag indicating that this type is nontrival
		 * @return the Unsupported bool indicating failure
		 */
		template <typename T>
		inline std::false_type construct(void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
		
		/**
		 * Perform trivial copy construction of the given class on a sufficiently large uninitialized buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to copy
		 * @param possible The tag to select copying
		 * @param trivial The tag indicating this operation is trivial and can go through std::memcpy.
		 * @return the Copied bool indicating success
		 */
		template <typename T>
		inline std::true_type copy(const void *input, void *output, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
		
		/**
		 * Perform copy construction of the given class on a sufficiently large uninitialized buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to copy
		 * @param possible The tag to select copying
		 * @param nontrivial The tag indicating this operation is nontrivial
		 * @return the Copied bool indicating success
		 */
		template <typename T>
		inline std::true_type copy(const void *input, void *output, std::size_t count, std::true_type possible, std::false_type nontrivial);
		
		/**
		 * Placeholder to do nothing if type T does not support copy construction.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to copy
		 * @param impossible The tag to select non-copying
		 * @param nontrivial The tag indicating this operation is nontrivial
		 * @return the Unsupported bool indicating failure
		 */
		template <typename T>
		inline std::false_type copy(const void *input, void *output, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
		
		/**
		 * Perform trivial move construction of the given class on a sufficiently large uninitialized buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to move
		 * @param possible The tag to select moving
		 * @param trivial The tag indicating this operation is trivial and can go through std::memcpy
		 * @return the Moved bool indicating success
		 */
		template <typename T>
		inline std::true_type move(void *input, void *output, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
		
		/**
		 * Perform move construction of the given class on a sufficiently large uninitialized buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to move
		 * @param possible The tag to select moving
		 * @param nontrivial The tag indicating this operation is nontrivial
		 * @return the Moved bool indicating success
		 */
		template <typename T>
		inline std::true_type move(void *input, void *output, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept;
		
		/**
		 * Placeholder to do nothing if type T does not support move construction.
		 *
		 * @tparam <T> The class being constructed
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The uninitialized buffer to construct a T object into
		 * @param count The number of objects to move
		 * @param impossible The tag to select non-moving
		 * @param nontrivial The tag indicating this operation is nontrivial
		 * @return the Unsupported bool indicating failure
		 */
		template <typename T>
		inline std::false_type move(void *input, void *output, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
		
		/**
		 * Perform trivial copy assignment using std::memmove on an initialized object buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The output buffer containing the object of type T to copy to
		 * @param count The number of objects to assign
		 * @param possible The tag to select copying
		 * @param trivial The tag to indicate this operation is trivial and can go through std::memmove
		 * @return the Copied bool indicating success
		 */
		template <typename T>
		inline std::true_type assign(const void *input, void *output, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
		
		/**
		 * Perform copy assignment using operator= of the given class on an initialized object buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The output buffer containing the object of type T to copy to
		 * @param count The number of objects to assign
		 * @param possible The tag to select copying
		 * @param nontrivial The tag to indicate this operation is nontrivial
		 * @return true for sccess
		 */
		template <typename T>
		inline std::true_type assign(const void *input, void *output, std::size_t count, std::true_type possible, std::false_type nontrivial);
		
		/**
		 * Placeholder to do nothing if the type T does not support copy assignment.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to copy from
		 * @param output The output buffer containing the object of type T to copy to
		 * @param count The number of objects to assign
		 * @param impossible The tag to select non-copying
		 * @param nontrivial The tag to indicate this operation is nontrivial
		 * @return false for failure
		 */
		template <typename T>
		inline std::false_type assign(const void *input, void *output, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
		
		/**
		 * Perform trivial move assignment using std::memmove an initialized object buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The output buffer containing the object of type T to move to
		 * @param count the number of objects to move
		 * @param possible The tag to select moving
		 * @param trivial The tag to indicate this is trivial and can go through std::memmove
		 * @return true for success
		 */
		template <typename T>
		inline std::true_type transfer(void *input, void *output, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
		
		/**
		 * Perform move assignment using operator= of the given class on an initialized object buffer given a buffer of an initialized object of the same type.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The output buffer containing the object of type T to move to
		 * @param count the number of objects to move
		 * @param possible The tag to select moving
		 * @param nontrivial The tag to indicate this is nontrivial and must go through the move assignment
		 * @return true for success
		 */
		template <typename T>
		inline std::true_type transfer(void *input, void *output, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept;
		
		/**
		 * Placeholder to do nothing if the type T does not support move assignment.
		 *
		 * @tparam <T> The class being assigned
		 * @param input the input buffer containing the object of type T to move from
		 * @param output The output buffer containing the object of type T to move to
		 * @param count the number of objects to move
		 * @param impossible The tag to select non-moving
		 * @param nontrivial The tag indicating this is nontrivial
		 * @return the Unsupported bool indicating failure
		 */
		template <typename T>
		inline std::false_type transfer(void *input, void *output, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
		
		/**
		 * Perform trivial object destruction on an initialized object buffer by doing nothing.
		 *
		 * @tparam <T> The class being destroyed
		 * @param buffer The buffer containing an initialized object of type T to destroy
		 * @param count The number of objects to destroy
		 * @param possible The tag to select destruction
		 * @param trivial The tag to indicate destruction is trivial therefore nothing needs to be done
		 * @return the Removed bool indicating success
		 */
		template <typename T>
		inline std::true_type destroy(void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept;
		
		/**
		 * Perform object destruction on an initialized object buffer using ~T().
		 *
		 * @tparam <T> The class being destroyed
		 * @param buffer The buffer containing an initialized object of type T to destroy
		 * @param count The number of objects to destroy
		 * @param possible The tag to select destruction
		 * @param nontrivial The tag to indicate destruction is nontrivial
		 * @return the Removed bool indicating success
		 */
		template <typename T>
		inline std::true_type destroy(void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept;
		
		/**
		 * Placeholder to do nothing if T does not support destruction.
		 *
		 * @tparam <T> The class being destroyed
		 * @param buffer The buffer containing an initialized object of type T to destroy
		 * @param count The number of objects to destroy
		 * @param impossible The tag to select non-destruction
		 * @param nontrivial The tag to indicate destruction is nontrivial
		 * @return the Unsupported bool indicating failure
		 */
		template <typename T>
		inline std::false_type destroy(void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept;
	}
}

/* Inline implementaiton */

namespace cio
{	
	template <typename T>
	Class<T>::Class() :
		Metaclass(nullptr, typeid(T), sizeof(T), alignof(T))
	{
		// nothing more to do
	}

	template <typename T>
	Class<T>::Class(const char *name) :
		Metaclass(name, typeid(T), sizeof(T), alignof(T))
	{
		// nothing more to do
	}
	
	template <typename T>
	Class<T>::~Class() noexcept = default;
			
	template <typename T>			
	bool Class<T>::construct(void *buffer, std::size_t count) const
	{
		return cio::reflect::construct<T>(buffer, count, std::is_default_constructible<T>(), std::is_trivially_default_constructible<T>());
	}

	template <typename T>
	bool Class<T>::copy(const void *input, void *output, std::size_t count) const
	{
		return cio::reflect::copy<T>(input, output, count, std::is_copy_constructible<T>(), std::is_trivially_copy_constructible<T>());
	}

	template <typename T>
	bool Class<T>::move(void *input, void *output, std::size_t count) const noexcept
	{
		return cio::reflect::move<T>(input, output, count, std::is_move_constructible<T>(), std::is_trivially_move_constructible<T>());
	}

	template <typename T>
	bool Class<T>::assign(const void *input, void *output, std::size_t count) const
	{
		return cio::reflect::assign<T>(input, output, count, std::is_copy_assignable<T>(), std::is_trivially_copy_assignable<T>());
	}

	template <typename T>
	bool Class<T>::transfer(void *input, void *output, std::size_t count) const noexcept
	{
		return cio::reflect::transfer<T>(input, output, count, std::is_move_assignable<T>(), std::is_trivially_move_assignable<T>());
	}

	template <typename T>
	bool Class<T>::destroy(void *buffer, std::size_t count) const noexcept
	{
		return cio::reflect::destroy<T>(buffer, count, std::is_destructible<T>(), std::is_trivially_destructible<T>());
	}
	
	inline Class<void>::Class(const char *name) :
		Metaclass(name, typeid(void), 1, 1)
	{
		// nothing more to do
	}
	
	namespace reflect
	{
		template <typename T>
		inline std::true_type construct(void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept
		{
			std::memset(buffer, 0, sizeof(T) * count);
			return std::true_type();
		}
	
		template <typename T>
		inline std::true_type construct(void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial)
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				new (static_cast<T *>(buffer) + i) T();
			}
			return std::true_type();
		}
		
		template <typename T>
		inline std::false_type construct(void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept
		{
			return std::false_type();
		}
		
		template <typename T>
		inline std::true_type copy(const void *input, void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept
		{
			std::memcpy(buffer, input, sizeof(T) * count);
			return std::true_type();
		}
		
		template <typename T>
		inline std::true_type copy(const void *input, void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial)
		{
			const T *values = reinterpret_cast<const T *>(input);

			for (std::size_t i = 0; i < count; ++i)
			{
				const T &toCopy = values[i];
				new (static_cast<T *>(buffer) + i) T(toCopy);
			}
			
			return std::true_type();
		}

		template <typename T>
		inline std::false_type copy(const void *input, void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept
		{
			return std::false_type();
		}
		
		template <typename T>
		inline std::true_type move(void *input, void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept 
		{
			std::memcpy(buffer, input, sizeof(T) * count);
			return std::true_type();
		}
		
		template <typename T>
		inline std::true_type move(void *input, void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept 
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				T &toMove = static_cast<T *>(input)[i];
				new (static_cast<T *>(buffer) + i) T(std::move(toMove));
			}
			
			return std::true_type();
		}
		
		template <typename T>
		inline std::false_type move(void *input, void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept 
		{
			return std::false_type();
		}
		
		template <typename T>
		inline std::true_type assign(const void *input, void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept
		{
			std::memmove(buffer, input, sizeof(T) * count);
			return std::true_type();
		}
		
		template <typename T>
		inline std::true_type assign(const void *input, void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial)
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				static_cast<T *>(buffer)[i] = static_cast<const T *>(input)[i];
			}
			
			return std::true_type();
		}

		template <typename T>
		inline std::false_type assign(const void *input, void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept
		{
			return std::false_type();
		}
		
		template <typename T>
		inline std::true_type transfer(void *input, void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept 
		{
			std::memmove(buffer, input, sizeof(T) * count);
			return std::true_type();
		}
		
		template <typename T>
		inline std::true_type transfer(void *input, void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept 
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				static_cast<T *>(buffer)[i] = std::move(static_cast<T *>(input)[i]);
			}
			
			return std::true_type();
		}
		
		template <typename T>
		inline std::false_type transfer(void *input, void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept 
		{
			return std::false_type();
		}
		
		template <typename T>
		inline std::true_type destroy(void *buffer, std::size_t count, std::true_type possible, std::true_type trivial) noexcept 
		{
			// nothing to do in this case
			return std::true_type();
		}
		
		template <typename T>
		inline std::true_type destroy(void *buffer, std::size_t count, std::true_type possible, std::false_type nontrivial) noexcept 
		{
			for (std::size_t i = 0; i < count; ++i)
			{
				T &object = static_cast<T *>(buffer)[i];
				object.~T();
			}
			
			return std::true_type();
		}
		
		template <typename T>
		inline std::false_type destroy(void *buffer, std::size_t count, std::false_type impossible, std::false_type nontrivial) noexcept 
		{
			return std::false_type();
		}
	}

}

#endif
