/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DatagramChannel.h"

#include <cio/Class.h>
#include <cio/ModeSet.h>
#include <cio/Parse.h>
#include <cio/Path.h>

#include "Resolver.h"

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#include <sys/types.h>
#endif

#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2

#if defined CIO_HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif

#if defined CIO_HAVE_WS2TCPIP_H
#include <WS2tcpip.h>
#endif
#endif

namespace cio
{
	namespace net
	{
		Class<DatagramChannel> DatagramChannel::sMetaclass("cio::net::DatagramChannel");

		const Metaclass &DatagramChannel::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		DatagramChannel::DatagramChannel() noexcept
		{
			// nothing more to do
		}

		DatagramChannel::DatagramChannel(Socket &&socket) noexcept :
			Channel(std::move(socket))
		{
			// nothing more to do
		}

		DatagramChannel::DatagramChannel(DatagramChannel &&in) noexcept :
			Channel(std::move(in))
		{
			// nothing more to do
		}

		DatagramChannel &DatagramChannel::operator=(DatagramChannel &&in) noexcept
		{
			Channel::operator=(std::move(in));
			return *this;
		}

		DatagramChannel::~DatagramChannel() noexcept
		{
			// nothing more to do
		}

		const Metaclass &DatagramChannel::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void DatagramChannel::clear() noexcept
		{
			Channel::clear();
		}

		cio::ModeSet DatagramChannel::openWithFactory(const cio::Path &path, cio::ModeSet modes, cio::ProtocolFactory *factory)
		{
			this->clear();
			cio::ModeSet actualModes;
			// Check for local port
			Text proto = path.getProtocol();
			Text fullhost = path.getRoot();
			Text local;
			Text remote = fullhost.view();
			Text remotePort;

			for (std::size_t i = 0; i < fullhost.size(); ++i)
			{
				if (fullhost[i] == '@')
				{
					local = fullhost.prefix(i);
					remote = fullhost.suffix(i + 1);
					break;
				}
			}

			// See if remote address overrides default port for protocol
			for (std::size_t i = remote.size(); i > 0; --i)
			{
				if (remote[i - 1] == ':')
				{
					remotePort = remote.suffix(i);
					remote = remote.prefix(i - 1);
					break;
				}
			}

			std::uint16_t remotePortOverride = cio::parseUnsigned16(remotePort);
			Text localPort;

			// See if local address overrides default port for protocol
			for (std::size_t i = local.size(); i > 0; --i)
			{
				if (local[i - 1] == ':')
				{
					localPort = local.suffix(i);
					local = local.prefix(i - 1);
					break;
				}
			}

			std::uint16_t localPortOverride = cio::parseUnsigned16(localPort);

			if (proto.empty())
			{
				if (!remotePort.empty())
				{
					proto = remotePort.view();
				}
				else if (!localPort.empty())
				{
					proto = localPort.view();
				}
				else
				{
					proto = Text("udp", 4);
				}
			}

			// TODO more intelligent IPv4 vs. IPv6 negotiation if both local and remote are provided
			Resolver resolver;
			Service remoteAddr = resolver.lookupService(remote, proto, remotePortOverride);
			Service localAddr = resolver.lookupService(local, proto, localPortOverride);

			// Found server binding parameters
			if (localAddr)
			{
				this->connectThroughInterface(localAddr.getAddress());
				this->listenToInterface(localAddr);
			}
			else if (remoteAddr)
			{
				mSocket.allocate(remoteAddr.getAddressFamily(), ChannelType::Datagram, remoteAddr.getProtocolFamily());
			}
			else
			{
				this->connectThroughAnyInterface();
			}

			if (remoteAddr)
			{
				this->connectToRecipient(remoteAddr);
			}

			if (mSocket)
			{
				actualModes = ModeSet::general();
			}

			return actualModes;
		}

		State DatagramChannel::connectThroughAnyInterface()
		{
			this->clear();
			// TODO handle mixed IPv4 and IPv6 setup?
			return mSocket.allocate(AddressFamily::ipv4(), ChannelType::Datagram);
		}

		State DatagramChannel::connectThroughInterface(const Address &address)
		{
			this->clear();
			return mSocket.allocate(address.getAddressFamily(), ChannelType::Datagram);
		}

		State DatagramChannel::connectThroughInterface(const Service &service)
		{
			this->clear();
			return mSocket.allocate(service);
		}

		State DatagramChannel::connectToRecipient(const Service &service)
		{
			if (!mSocket)
			{
				mSocket.allocate(service);
			}

			return mSocket.connect(service);
		}

		State DatagramChannel::connectToRecipient(const Address &address)
		{
			if (!mSocket)
			{
				mSocket.allocate(address.getAddressFamily(), ChannelType::Datagram);
			}

			return mSocket.connect(address);
		}

		State DatagramChannel::listenToAllInterfaces(std::uint16_t port)
		{
			if (!mSocket)
			{
				this->connectThroughAnyInterface();
			}

			Address address(IPv4::any(), port);
			return mSocket.bind(address);
		}

		State DatagramChannel::listenToInterface(const Service &service)
		{
			if (!mSocket)
			{
				this->connectThroughInterface(service);
			}

			return mSocket.bind(service);
		}

		State DatagramChannel::listenToInterface(const Address &address)
		{
			if (!mSocket)
			{
				this->connectThroughInterface(address);
			}

			return mSocket.bind(address);
		}

		State DatagramChannel::listenToInterface(const IPv4 &ip, std::uint16_t port)
		{
			Address address(ip, port);

			if (!mSocket)
			{
				this->connectThroughInterface(address);
			}

			return mSocket.bind(address);
		}

		State DatagramChannel::listenToInterface(const IPv6 &ip, std::uint16_t port)
		{
			Address address(ip, port);

			if (!mSocket)
			{
				this->connectThroughInterface(address);
			}

			return mSocket.bind(address);
		}
	}
}



