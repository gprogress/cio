/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_STREAMCHANNEL_H
#define CIO_NET_STREAMCHANNEL_H

#include "Types.h"

#include "Channel.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace net
	{
		/**
		 * The Stream Channel is a Channel optimized for use with stream-based network sockets such as TCP/IP.
		 */
		class CIO_NET_API StreamChannel : public Channel
		{
			public:
				/**
				 * Gets the declared metaclass for this class.
				 *
				 * @return the decalred metaclass
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct an unconnected stream channel.
				 */
				StreamChannel() noexcept;

				/**
				 * Constructs a stream channel by adopting the given socket.
				 *
				 * @param socket The socket to adopt
				 */
				explicit StreamChannel(Socket &&socket) noexcept;

				/**
				 * Constructs a stream channel by transferring the state of an existing stream channel.
				 *
				 * @param in The stream channel to move
				 */
				StreamChannel(StreamChannel &&in) noexcept;

				/**
				 * Moves a stream channel.
				 *
				 * @param in The stream channel to move
				 * @return this stream channel
				 */
				StreamChannel &operator=(StreamChannel &&in) noexcept;

				/**
				 * Destructor. Closes the underlying socket.
				 */
				virtual ~StreamChannel() noexcept override;

				/**
				 * Gets the runtime metaclass for this object.
				 *
				 * @return the metaclass for this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears the stream channel. This also closes the underlying socket.
				 */
				virtual void clear() noexcept override;

				/**
				 * Opens this Stream Channel to work with the given resource location.
				 * The resource format is protocol://recipicent:port where
				 * <ul>
				 * <li>protocol is the protocol, which may be omitted if the port is specified and is the default port for the protocol</li>
				 * <li>recipient:port is the hostname, IPv4, or IPv6 address of the recipient with port</li>
				 * </ul>
				 *
				 * The Resolver is used to translate the URI into Service descriptions that can actually be used to connect.
				 * The port may be omitted if it is the default port for the protocol.
				 *
				 * Stream sockets by nature are full duplex read/write and are always newly created, so the returned ModeSet will always indicate that.
				 *
				 * @param path The resource path to connect to
				 * @param modes The desired modes
				 * @return the actual modes
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				virtual cio::ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;

				/**
				 * Loads data from a TCP socket stream, correctly detecting end of stream and closing the connection when this happens.
				 *
				 * @param buffer The buffer to load into
				 * @param length The number of bytes to load into the buffer
				 * @return the result describing the success of the operation and how many bytes were actually loaded
				 */
				virtual Progress<std::size_t> requestRead(void *buffer, std::size_t length) noexcept override;

				/**
				 * Stores data to a TCP socket stream.
				 *
				 * @param buffer The buffer to store from
				 * @param length The number of bytes to store from the buffer
				 * @return the result describing the success of the operation and how many bytes were actually stored
				 */
				virtual Progress<std::size_t> requestWrite(const void *buffer, std::size_t length) noexcept override;

				/**
				 * Connects the stream channel to the given remote service.
				 * This will allocate the necessary socket for the service then connect it.
				 *
				 * @param service The service to connect to
				 * @throw cio::Exception If the socket setup or connection failed
				 */
				void connect(const Service &service);

				/**
				 * Connects the stream channel to the given remote address.
				 * This will allocate the necessary socket then connect.
				 *
				 * @param address The address to connect to
				 * @throw cio::Exception If the socket setup or connection failed
				 */
				void connect(const Address &address);

				/**
				 * Connects the stream channel to the given remote IPv4 address.
				 * This will allocate the necessary socket then connect.
				 *
				 * @param ip The IPv4 address to connect to
				 * @param port The port to connect to
				 * @throw cio::Exception If the socket setup or connection failed
				 */
				void connect(const IPv4 &ip, std::int16_t port);

				/**
				 * Connects the stream channel to the given remote IPv6 address.
				 * This will allocate the necessary socket then connect.
				 *
				 * @param ip The IPv6 address to connect to
				 * @param port The port to connect to
				 * @throw cio::Exception If the socket setup or connection failed
				 */
				void connect(const IPv6 &ip, std::int16_t port);
				
			private:
				/** The metaclass for this class */
				static Class<StreamChannel> sMetaclass;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

