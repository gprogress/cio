/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MASK_H
#define CIO_MASK_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Mask class is a lightweight class for tracking byte masks of which byte values are included or excluded in a set.
	 * It is primarily used for text parsing to specify enabled or disabled characters for various aspects of tokenization, but may be used for any kind of octet.
	 * 
	 * @note Because it is focused on individual bytes, it is not semantically aware of multibyte sequences such as UTF-8 encodings.
	 * However, it can be tuned to match either ASCII bytes or UTF-8 prefixes since that can be determined at the byte level.
	 */
	class CIO_API Mask
	{
		public:
			/**
			 * Creates a byte mask that accepts all ASCII whitespace characters.
			 *
			 * @return the whitespace mask
			 */
			static Mask whitespace() noexcept;

			/**
			 * Creates a byte mask that accepts all ASCII uppercase letter characters.
			 *
			 * @return the uppercase mask
			 */
			static Mask uppercase() noexcept;

			/**
			 * Creates a byte mask that accepts all ASCII lowercase letter characters.
			 *
			 * @return the lowercase mask
			 */
			static Mask lowercase() noexcept;

			/**
			 * Creates a byte mask that accepts all ASCII letter characters of any case.
			 *
			 * @return the letter mask
			 */
			static Mask alpha() noexcept;

			/**
			 * Creates a byte mask that accepts ASCII base 10 numeric digit 0-9.
			 *
			 * @return the digits mask
			 */
			static Mask digits() noexcept;

			/**
			 * Creates a byte mask that accepts ASCII characters needed for representing
			 * a number with the given base. If base is greater than 10, letters of any case
			 * are accepted for those digits.
			 *
			 * @param base The desired base
			 * @return the digits mask for the given base
			 */
			static Mask digits(unsigned base) noexcept;

			/**
			 * Creates a byte mask that accepts ASCII characters that are letters or base 10 digits.
			 *
			 * @return the alphanumeric mask
			 */
			static Mask alphanumeric() noexcept;

			/**
			 * Creates a byte mask that accepts ASCII characters that are printable symbols but not letters or digits.
			 *
			 * @return the symbols mask
			 */
			static Mask symbols() noexcept;

			/**
			 * Creates a byte mask that accepts ASCII characters that are printable glyphs - all printable characters except whitespace.
			 *
			 * @return the glyphs mask
			 */
			static Mask glyphs() noexcept;

			/**
			 * Creates a byte mask that accepts all ASCII printable characters.
			 *
			 * @return the printable mask
			 */
			static Mask printable() noexcept;

			/**
			 * Creates a byte mask that accepts all ASCII characters.
			 *
			 * @return the ASCII mask
			 */
			static Mask ascii() noexcept;

			/**
			 * Creates a byte mask that accepts all characters indicating the start of a UTF-8 multibyte sequence.
			 *
			 * @return the UTF-8 prefix mask
			 */
			static Mask unicode() noexcept;

			/**
			 * Creates a byte mask that accepts all potentially printable characters, including printable ASCII and UTF-8 prefix.
			 *
			 * @return the text mask
			 */
			static Mask text() noexcept;

			/**
			 * Creates a byte mask that accepts characters that can form a label or identifier - this includes letters of any case, base 10 digits,
			 * and the underscore '_' character.
			 *
			 * @return the label mask
			 */
			static Mask label() noexcept;

			/**
			 * Construct an empty mask that matches no bytes.
			 */
			inline Mask() noexcept;

			/**
			 * Construct a mask with the given bits indicating matches.
			 * 
			 * @param bits64 The match bits for character bytes 0-63
			 * @param bits128 The match bits for character bytes 64-127
			 * @param bits192 The match bits for character bytes 128-193
			 * @param bits256 The match bits for character bytes 194-255
			 */
			explicit Mask(unsigned long long bits64, unsigned long long bits128 = 0, unsigned long long bits192 = 0, unsigned long long bits256 = 0);

			/**
			 * Constructs a mask that accepts the given character.
			 *
			 * @param c The character to accept
			 */
			Mask(char c) noexcept;

			/**
			 * Constructs a mask that accepts the given range of characters.
			 *
			 * @param begin The first character to accept
			 * @param end The last character to accept
			 */
			Mask(char begin, char end) noexcept;

			/**
			 * Constructs a mask that accepts any of the characters in the given string, except the null terminator.
			 *
			 * @param any The string containing all of the characters to accept
			 */
			Mask(const char *any) noexcept;

			/**
			 * Constructs a mask that accepts any of the given sequence of characters.
			 *
			 * @param any The string containing all of the characters to accept
			 * @param count The number of character bytes to accept
			 */
			Mask(const char *any, std::size_t count) noexcept;

			/**
			 * Copy constructor.
			 * 
			 * @param mask The mask to copy
			 */
			inline Mask(const Mask &mask) = default;

			/**
			 * Copy assignment.
			 * 
			 * @param mask The mask to copy
			 * @return this mask
			 */
			inline Mask &operator=(const Mask &mask) = default;

			/**
			 * Destructor.
			 */
			inline ~Mask() noexcept = default;

			/**
			 * Gets the number of character bytes that are matched.
			 * 
			 * @return the number character bytes that are matched
			 */
			std::size_t size() const noexcept;

			/**
			 * Checks whether no character bytes are matched.
			 */
			bool empty() const noexcept;

			/**
			 * Checks whether the given character byte is matched.
			 * This method exists for STL compatibility.
			 * 
			 * @param value The character value
			 * @return whether the byte is matched
			 */
			inline bool count(char value) const noexcept;

			/**
			 * Checks whether the given character byte is matched.
			 *
			 * @param value The character value
			 * @return whether the byte is matched
			 */
			inline bool get(char value) const noexcept;

			/**
			 * Checks whether the given character byte is matched.
			 *
			 * @param value The character value
			 * @return whether the byte is matched
			 */
			inline bool operator[](char value) const noexcept;

			/**
			 * Sets the mask to match the given character byte.
			 * 
			 * @param value The character byte
			 */
			void set(char value) noexcept;

			/**
			 * Sets the mask to match the given range of character bytes.
			 *
			 * @param begin The first character byte
			 * @param end The last character byte
			 */
			void set(char begin, char end) noexcept;

			/**
			 * Sets whether the mask should match the given character byte.
			 *
			 * @param value The character byte
			 * @param enabled Whether the character should be matched
			 */
			void set(char value, bool enabled) noexcept;

			/**
			 * Sets the mask to match any of the characters in the given string, except the null terminator.
			 *
			 * @param any The string containing all of the characters to accept
			 */
			void set(const char *any) noexcept;

			/**
			 * Sets teh mask to match any of the given sequence of characters.
			 *
			 * @param any The string containing all of the characters to accept
			 * @param count The number of character bytes to accept
			 */
			void set(const char *any, std::size_t count) noexcept;

			/**
			 * Clears the mask so that no character bytes are matched.
			 */
			void clear() noexcept;

			/**
			 * Sets the mask to not match the given character byte.
			 * 
			 * @param value The character byte
			 */
			void clear(char value) noexcept;

			/**
			 * Sets the mask to not match the given range of character bytes.
			 *
			 * @param begin The first character byte
			 * @param end The last character byte
			 */
			void clear(char begin, char end) noexcept;

			/**
			 * Sets the mask to not match any of the characters in the given string, stopping at and ignoring the null terminator.
			 *
			 * @param any The string containing all of the characters to not match
			 */
			void clear(const char *value) noexcept;

			/**
			 * Constructs a mask to not matcha any of the given sequence of characters.
			 *
			 * @param any The string containing all of the characters to not match
			 * @param count The number of character bytes to not match
			 */
			void clear(const char *value, std::size_t count) noexcept;

			/**
			 * Sets this mask to match all bytes that are considered whitespace.
			 * This will match the result of std::isspace for ASCII characters.
			 */
			void setWhitespace() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered whitespace.
			 *
			 * @param enabled Whether to match whitespace
			 */
			void setWhitespace(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered whitespace.
			 */
			void clearWhitespace() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered uppercase letters.
			 * This will match the result of std::isupper for ASCII characters.
			 */
			void setUppercase() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered uppercase letters.
			 *
			 * @param enabled Whether matching uppercase letters is enabled
			 */
			void setUppercase(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered uppercase letters.
			 */
			void clearUppercase() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered lowercase letters.
			 * This will match the result of std::islower for ASCII characters.
			 */
			void setLowercase() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered lowercase letters.
			 *
			 * @param enabled Whether matching lowercase letters is enabled
			 */
			void setLowercase(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered lowercase letters.
			 */
			void clearLowercase() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered letters regardless of case.
			 * This will match the result of std::isalpha for ASCII characters. 
			 */
			void setAlpha() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered letters in the given case.
			 * If anything other than Case::Lower or Case::Upper is specified, all cases are matched.
			 *
			 * @param c The case to match
			 */
			void setAlpha(Case c) noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered letters regardless of case.
			 *
			 * @param enabled Whether matching letters of any case is enabled
			 */
			void setAlpha(bool enabled) noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered letters in the given case.
			 * If anything other than Case::Lower or Case::Upper is specified, all cases are affected.
			 *
			 * @param c The case to match
			 * @param enabled Whether matching letters in the given case is enabled
			 */
			void setAlpha(Case c, bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered letters regardless of case.
			 */
			void clearAlpha() noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered letters in the given case.
			 *
			 * @param c The case to not match
			 */
			void clearAlpha(Case c) noexcept;

			/**
			 * Sets this mask to match all bytes that are considered decimal digits.
			 * This will match the result of std::isdigit for ASCII characters.
			 */
			void setDigits() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered digits in the given base.
			 * This will match the result of std::isdigit plus any letters of any case needed for digits above 9 for ASCII characters.
			 *
			 * @param base The numeric base to match
			 */
			void setDigits(unsigned base) noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered decimal digits.
			 *
			 * @param enabled Whether matching base 10 digits is enabled
			 */
			void setDigits(bool enabled) noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered digits in the given base.
			 *
			 * @param base The numeric base to match
			 * @param enabled Whether to match numeric digits in the given base
			 */
			void setDigits(unsigned base, bool enabled) noexcept;

			/**
			 * Sets this mask to match not all bytes that are considered decimal digits.
			 */
			void clearDigits() noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered digits in the given base.
			 *
			 * @param base The numeric base to not match
			 */
			void clearDigits(unsigned base) noexcept;

			/**
			 * Sets this mask to match all bytes that are considered letters or numbers.
			 * This will match the result of std::isalnum for ASCII characters.
			 */
			void setAlphanumeric() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered letters or numbers.
			 *
			 * @param enabled Whether to match alphanumeric characters
			 */
			void setAlphanumeric(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered letters or numbers.
			 */
			void clearAlphanumeric() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered printable symbols but not letters, numbers, or whitespace.
			 * This will match the result of std::ispunct for ASCII characters.
			 */
			void setSymbols() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered printable symbols but not letters, numbers, or whitespace.
			 *
			 * @param enabled Whether the mask will match symbols
			 */
			void setSymbols(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered printable symbols, not affecting letters, numbers, or whitespace.
			 */
			void clearSymbols() noexcept;

			/**
			 * Sets this mask to match all bytes that are printable glyphs. This is all ASCII printable characters except whitespace.
			 */
			void setGlyphs() noexcept;

			/**
			 * Sets whetyher this mask will match all bytes that are printable glyphs. This is all ASCII printable characters except whitespace.
			 *
			 * @param enabled Whether to match printable glyphs
			 */
			void setGlyphs(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are printable glyphs. This is all ASCII printable characters except whitespace.
			 */
			void clearGlyphs() noexcept;

			/**
			 * Sets this mask to match all bytes that are considered printable ASCII characters.
			 * This will match the result of std::isprint for ASCII characters.
			 */
			void setPrintable() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered printable ASCII characters.
			 * 
			 * @param enabled Whether to match printable characters
			 */
			void setPrintable(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are considered printable ASCII characters.
			 */
			void clearPrintable() noexcept;

			/**
			 * Sets this mask to match all bytes that are valid ASCII bytes.
			 * This is true for all bytes where the high bit is not set.
			 */
			void setAscii() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are valid ASCII bytes.
			 * 
			 * @param enabled Whether to match ASCII vbytes
			 */
			void setAscii(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are valid ASCII bytes.
			 */
			void clearAscii() noexcept;

			/**
			 * Sets this mask to match all bytes that are UTF-8 bytes indicating that more bytes follow to form a multibyte character.
			 * This is true for all bytes where the high bit is set.
			 */
			void setUnicodePrefix() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are UTF-8 bytes indicating that more bytes follow to form a multibyte character.
			 *
			 * @param enabled Whether to enable matching UTF-8 prefixes
			 */
			void setUnicodePrefix(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are UTF-8 bytes indicating that more bytes follow to form a multibyte character.
			 */
			void clearUnicodePrefix() noexcept;

			/**
			 * Sets this mask to match all bytes that are potentially printable UTF-8 text.
			 * This includes all printable ASCII characters plus the unicode prefixed bytes.
			 */
			void setText() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are potentially printable UTF-8 text.
			 * 
			 * @param enabled Whether to enable printable text bytes
			 */
			void setText(bool enabled) noexcept;

			/**
			 * Sets this mask to not match all bytes that are potentially printable UTF-8 text.
			 * This includes all printable ASCII characters plus the unicode prefixed bytes.
			 */
			void clearText() noexcept;

			// Specific use cases for parsing

			/**
			 * Sets this mask to match all bytes that are considered valid identifier characters in a label.
			 * This matches all letters, numbers, and the symbol '_'.
			 */
			void setLabel() noexcept;

			/**
			 * Sets whether this mask will match all bytes that are considered valid identifier characters in a label.
			 * This affects all letters, numbers, and the symbol '_'.
			 *
			 * @param enabled Whether to match label characters
			 */
			void setLabel(bool enabled) noexcept;

			/**
			 * Sets this mask to notmatch all bytes that are considered valid identifier characters in a label.
			 * This will stop matching all letters, numbers, and the symbol '_'.
			 */
			void clearLabel() noexcept;

			// Direct mask access

			/**
			 * Gets direct access to the 64-bit integer implementing the enabled mask for the given group of 64 bits
			 * specified from to 0 to 3. 
			 *
			 * @param group The group index from 0 to 3
			 * @return the mask indicating which of the 64 character bytes in that group are enabeld
			 */
			inline std::uint64_t enabled(unsigned group) const noexcept;

			/**
			 * Directly sets the 64-bit integer implementing the enabled mask for the given group of 64 bits
			 * specified from to 0 to 3.
			 *
			 * @param group The group index from 0 to 3
			 * @param mask the new mask indicating which of the 64 character bytes in that group are enabeld
			 */
			inline void enabled(unsigned group, std::uint64_t mask) noexcept;

			/**
			 * Gets direct access to the underlying bit mask data as an array of four 64-bit integers.
			 *
			 * @return the mask data
			 */
			inline std::uint64_t *data() noexcept;

			/**
			 * Gets direct access to the underlying bit mask data as an array of four 64-bit integers.
			 *
			 * @return the mask data
			 */
			inline const std::uint64_t *data() const noexcept;

			// Logical operators

			/**
			 * Updates this mask to also enable all character bytes enabled in the given mask.
			 *
			 * @param right The mask with characters to also enable
			 * @return this mask
			 */
			Mask &operator|=(const Mask &right) noexcept;

			/**
			 * Updates this mask to disable any characters not enabled in the given mask.
			 *
			 * @param right The mask with characters to keep enabled
			 * @return this mask
			 */
			Mask &operator &=(const Mask &right) noexcept;

			/**
			 * Updates this mask to toggle character bytes enabled in the given mask.
			 *
			 * @param right The mask with characters to toggle
			 * @return this mask
			 */
			Mask &operator^=(const Mask &right) noexcept;

			/**
			 * Updates this mask to enable all disabled bytes and disable all enabled bytes to invert the match.
			 *
			 * @return this mask
			 */
			Mask &invert() noexcept;

			/**
			 * Creates a mask that matches all characters not matched by the given mask.
			 *
			 * @param left The input mask
			 * @return a mask that matches all characters not enabled in the input mask
			 */
			friend CIO_API Mask operator~(const Mask &left) noexcept;

			/**
			 * Creates a mask that matches all characters matched by either of two input masks.
			 *
			 * @param left The first input mask
			 * @param right The second input mask
			 * @return a mask that matches all characters enabled in either input mask
			 */
			friend CIO_API Mask operator|(const Mask &left, const Mask &right) noexcept;

			/**
			 * Creates a mask that matches all characters matched by both of two input masks.
			 *
			 * @param left The first input mask
			 * @param right The second input mask
			 * @return a mask that matches all characters enabled in both input masks
			 */
			friend CIO_API Mask operator&(const Mask &left, const Mask &right) noexcept;

			/**
			 * Creates a mask that matches all characters matched by exactly one of two input masks.
			 *
			 * @param left The first input mask
			 * @param right The second input mask
			 * @return a mask that matches all characters enabled in exactly one input mask
			 */
			friend CIO_API Mask operator^(const Mask &left, const Mask &right) noexcept;

		private:
			/** The mask of which characters are enabled as four groups of 64 bits each */
			std::uint64_t mEnabled[4];
	};

	/**
	 * Checks whether two byte masks are equal.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return the masks are equal
	 */
	CIO_API bool operator==(const Mask &left, const Mask &right) noexcept;

	/**
	 * Checks whether two byte masks are not equal.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return the masks are not equal
	 */
	CIO_API bool operator!=(const Mask &left, const Mask &right) noexcept;

	/**
	 * Checks whether one byte mask sorts before another.
	 * The sort order is determined by which one has the first distinct (lowest) byte enabled.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return whether the first mask sorts before the second
	 */
	CIO_API bool operator<(const Mask &left, const Mask &right) noexcept;

	/**
	 * Checks whether one byte mask sorts before or equal to another.
	 * The sort order is determined by which one has the first distinct (lowest) byte enabled.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return whether the first mask sorts before or equal to the second
	 */
	CIO_API bool operator<=(const Mask &left, const Mask &right) noexcept;

	/**
	 * Checks whether one byte mask sorts after another.
	 * The sort order is determined by which one has the first distinct (lowest) byte enabled.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return whether the first mask sorts before the second
	 */
	CIO_API bool operator>(const Mask &left, const Mask &right) noexcept;

	/**
	 * Checks whether one byte mask sorts after or equal to another.
	 * The sort order is determined by which one has the first distinct (lowest) byte enabled.
	 *
	 * @param left The first mask
	 * @param right The second mask
	 * @return whether the first mask sorts before the second
	 */
	CIO_API bool operator>=(const Mask &left, const Mask &right) noexcept;

	/**
	 * Prints a mask to a C++ stream. The mask is printed as a sequence of four hexadecimal 64-bit integers separated by '-'.
	 *
	 * @param stream The stream to print to
	 * @param mask The mask to print
	 * @return the stream
	 * @throw std::exception If the stream threw an exception while printing
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Mask &mask);
}

/* Inline implementation */

namespace cio
{
	inline Mask::Mask() noexcept
	{
		mEnabled[0] = 0;
		mEnabled[1] = 0;
		mEnabled[2] = 0;
		mEnabled[3] = 0;
	}

	inline bool Mask::count(char value) const noexcept
	{
		std::uint64_t idx = static_cast<std::uint64_t>(value);
		return (mEnabled[idx >> 6] & (1ull << (idx & 0x3F))) != 0;
	}

	inline bool Mask::get(char value) const noexcept
	{
		std::uint64_t idx = static_cast<std::uint64_t>(value);
		return (mEnabled[idx >> 6] & (1ull << (idx & 0x3F))) != 0;
	}

	inline bool Mask::operator[](char value) const noexcept
	{
		std::uint64_t idx = static_cast<std::uint64_t>(value);
		return (mEnabled[idx >> 6] & (1ull << (idx & 0x3F))) != 0;
	}

	inline std::uint64_t Mask::enabled(unsigned group) const noexcept
	{
		return mEnabled[group];
	}

	inline void Mask::enabled(unsigned group, std::uint64_t mask) noexcept
	{
		mEnabled[group] = mask;
	}

	inline std::uint64_t *Mask::data() noexcept
	{
		return mEnabled;
	}

	inline const std::uint64_t *Mask::data() const noexcept
	{
		return mEnabled;
	}
}

#endif
