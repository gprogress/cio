/*==============================================================================
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_VALIDATION_H
#define CIO_VALIDATION_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	/**
	 * The Validation enumeration describes the validation results and possible failures for encoding or decoding
	 * values from text or binary data.
	 */
	enum class Validation : std::uint8_t
	{
		/** Validation has not been performed yet */
		None,

		/** Datatype is an exact match, no conversion was necessary */
		Exact,

		/** Datatype is converted without loss of data */
		LosslessConversion,

		/** Datatype is converted with loss of data */
		LossyConversion,

		/** The value has more decimal/floating point precision than is allowed for the data type */
		TooMuchPrecision,

		/** The value is below the minimum range of the datatype */
		BelowMinimum,

		/** The value is above the maximum range of the data type */
		AboveMaximum,

		/** The value length is below the minimum for the data type */
		TooShort,

		/** The value length is above the maximum for the data type */
		TooLong,

		/** The number of values was below the minimum cardinality */
		TooFew,

		/** The number of values was above the maximum cardinality */
		TooMany,

		/** Validation failure not otherwise described */
		InvalidValue,

		/** The value was entirely missing */
		Missing,

		/** More than one failure occurred at this level */
		MultipleFailures,

		/** Validation failed because the data type is not implemented by code */
		NotSupported
	};

	/**
	 * Checks whether the Validation result is considered passing.
	 * This is true if the result is either an exact match or if it involves a lossy or lossless conversion.
	 *
	 * @param val The validation result
	 * @return whether the validation result is considered a passing success
	 */
	inline bool passed(Validation val) noexcept;

	/**
	 * Checks whether the Validation result is considered failing.
	 * This is true if the result was out of range, violated range or cardinality constraints, or is otherwise invalid or missing.
	 *
	 * @param val The validation result
	 * @return whether the validation result is considered a failure
	 */
	inline bool failed(Validation val) noexcept;

	/**
	 * Maps a Validation result to a Severity enumeration, usually for logging or testing purposes.
	 * Exact matches map to Severity::None.
	 * Lossless conversions map to Severity::Debug.
	 * Lossy conversions map to Severity::Warning.
	 * All failure modes map to Severity::Error.
	 *
	 * @param val The validation value
	 * @return the mapped Severity for that value
	 */
	CIO_API Severity mapToSeverity(Validation val) noexcept;

	/**
	 * Gets the text representation of a validation value as a null-terminated C string.
	 *
	 * @param val The validation value
	 * @return the text for the validation value
	 */
	CIO_API const char *print(Validation val) noexcept;

	/**
	 * Prints the text representation of a validation value to a C++ stream.
	 *
	 * @param s The C++ stream
	 * @param val The validation value
	 * @return the C++ stream
	 * @throw std::exception If the underlying C++ stream throws an exception for an error
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Validation val);
}

/* Inline implementation */

namespace cio
{
	inline bool passed(Validation val) noexcept
	{
		return val <= Validation::TooMuchPrecision;
	}

	inline bool failed(Validation val) noexcept
	{
		return val > Validation::TooMuchPrecision;
	}
}

#endif
