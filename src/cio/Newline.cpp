/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Newline.h"

#include <algorithm>
#include <cstring>
#include <ostream>

#if defined _MSC_VER
#pragma warning(disable:4996)
#endif

namespace cio
{
	namespace
	{
#if defined _WIN32
		const char sNativeNewline[] = "\r\n";
		const Newline sNativeNewlineEnum = Newline::Windows;
		const std::size_t sNativeNewlineLength = 2;
#else
		const char *sNativeNewline = "\n";
		const Newline sNativeNewlineEnum = Newline::Unix;
		const std::size_t sNativeNewlineLength = 1;
#endif

		const char *sNewlines[] =
		{
			"",
			"\n",
			"\r",
			"\r\n",
			sNativeNewline,
			sNativeNewline
		};

		const char *sNewlineText[] =
		{
			"None",
			"Unix",
			"Apple",
			"Windows",
			"Native",
			"Preserve"
		};

		std::size_t sNewlineLength[] =
		{
			0,
			1,
			1,
			2,
			sNativeNewlineLength,
			sNativeNewlineLength
		};
	}

	Newline getNativeNewline() noexcept
	{
		return sNativeNewlineEnum;
	}

	Newline getNativeNewline(Newline style) noexcept
	{
		return style >= Newline::Native ? sNativeNewlineEnum : style;
	}

	const char *print(Newline style) noexcept
	{
		return sNewlines[static_cast<std::size_t>(style)];
	}

	std::size_t print(Newline style, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = 0;
		Newline actual = getNativeNewline(style);
		if (actual != Newline::None)
		{
			const char *text = print(style);

			if (actual != Newline::Windows)
			{
				needed = 1;
				if (buffer && capacity)
				{
					buffer[0] = text[0];
				}
			}
			else
			{
				needed = 2;
				if (buffer && capacity)
				{
					buffer[0] = text[0];
					if (capacity > 1)
					{
						buffer[1] = text[1];
					}
				}
			}
		}

		if (needed < capacity)
		{
			buffer[needed] = 0;
		}

		return needed;
	}

	std::size_t strlen(Newline style) noexcept
	{
		return sNewlineLength[static_cast<std::size_t>(style)];
	}

	const char *label(Newline style) noexcept
	{
		return sNewlineText[static_cast<std::size_t>(style)];
	}

	std::ostream &operator<<(std::ostream &s, Newline style)
	{
		return s << print(style);
	}

	std::pair<std::size_t, std::size_t> convertNewlines(const char *input, std::size_t inputLen, char *output, std::size_t outputLen, Newline outputStyle)
	{
		std::pair<std::size_t, std::size_t> processed(0u, 0u);

		if (outputStyle == Newline::Preserve)
		{
			// No newline processing, just strcpy
			std::size_t toCopy = std::min(inputLen, outputLen);
			std::size_t copied = 0;

			while (copied < toCopy && input[copied])
			{
				output[copied] = input[copied];
				++copied;
			}

			processed.first = processed.second = copied;
		}
		else
		{
			const char *newline = cio::print(outputStyle);
			std::size_t newlineBytes = std::strlen(newline);

			while (processed.first < inputLen && input[processed.first] && processed.second < outputLen)
			{
				char c = input[processed.first];

				// Leading '\n' is always a newline
				if (c == '\n')
				{
					if (processed.second + newlineBytes <= outputLen)
					{
						std::memcpy(output + processed.second, newline, newlineBytes);
						++processed.first;
						processed.second += newlineBytes;
					}
					else
					{
						break;
					}
				}
				// Leading '\r\ can be either a Windows or an Apple newline
				else if (c == '\r')
				{
					// Windows newline, eat both bytes
					if (processed.first + 1 < inputLen && input[processed.first + 1] == '\n')
					{
						if (processed.second + newlineBytes <= outputLen)
						{
							std::memcpy(output + processed.second, newline, newlineBytes);
							processed.first += 2;
							processed.second += newlineBytes;
						}
						else
						{
							break;
						}
					}
					// Apple newline, eat just the '\r'
					else
					{
						if (processed.second + newlineBytes <= outputLen)
						{
							std::memcpy(output + processed.second, newline, newlineBytes);
							++processed.first;
							processed.second += newlineBytes;
						}
						else
						{
							break;
						}
					}
				}
				// All other characters are a pass through
				else
				{
					output[processed.second] = input[processed.first];
					++processed.first;
					++processed.second;
				}
			}
		}

		return processed;
	}
}