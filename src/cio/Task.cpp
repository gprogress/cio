/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Task.h"

#include "Logger.h"

namespace cio
{
	Task::Task() noexcept :
		mLogger(nullptr),
		mTimeout(std::chrono::milliseconds::max())
	{
		// nothing to do
	}
	
	Task::Task(std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mLogger(logger),
		mTimeout(timeout)
	{
		// nothing more to do
	}
	
	Task::Task(Length maxProgress, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(0u, maxProgress),
		mLogger(logger),
		mTimeout(timeout)
	{
		// nothing more to do
	}

	Task::Task(Action action, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(action),
		mLogger(logger),
		mTimeout(timeout)
	{
		// nothing more to do
	}

	Task::Task(Action action, Status status, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(action, status),
		mLogger(logger),
		mTimeout(timeout)
	{
		if (mProgress.started())
		{
			mStarted = std::chrono::steady_clock::now();
		}
	}

	Task::Task(Action action, Length maxProgress, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(action, 0u, maxProgress),
		mLogger(logger),
		mTimeout(timeout)
	{
		// nothing more to do
	}

	Task::Task(Action action, Status status, Length maxProgress, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(action, status, 0u, maxProgress),
		mLogger(logger),
		mTimeout(timeout)
	{
		if (mProgress.started())
		{
			mStarted = std::chrono::steady_clock::now();
		}
	}
	
	Task::Task(Progress<Length> progress, std::chrono::milliseconds timeout, Logger *logger) noexcept :
		mProgress(progress),
		mLogger(logger),
		mTimeout(timeout)
	{
		if (progress.started())
		{
			mStarted = std::chrono::steady_clock::now();
		}
	}
	
	Task::Task(const Task &in) :
		mLogger(nullptr)
	{
		std::lock_guard<std::mutex> lock(in.mMutex);
		this->executeTaskCopy(in);
	}
	
	Task::Task(Task &&in) noexcept :
		mLogger(nullptr)
	{
		std::lock_guard<std::mutex> lock(in.mMutex);
		this->executeTaskMove(std::move(in));
	}
	
	Task &Task::operator=(const Task &in)
	{
		if (this < &in)
		{
			std::lock_guard<std::mutex> lock1(mMutex);
			std::lock_guard<std::mutex> lock2(in.mMutex);
			this->executeTaskCopy(in);
		}
		else if (this > &in)
		{
			std::lock_guard<std::mutex> lock1(in.mMutex);
			std::lock_guard<std::mutex> lock2(mMutex);
			this->executeTaskCopy(in);
		}
		
		return *this;
	}
	
	Task &Task::operator=(Task &&in) noexcept
	{
		if (this < &in)
		{
			std::lock_guard<std::mutex> lock1(mMutex);
			std::lock_guard<std::mutex> lock2(in.mMutex);
			this->executeTaskMove(std::move(in));
		}
		else if (this > &in)
		{
			std::lock_guard<std::mutex> lock1(in.mMutex);
			std::lock_guard<std::mutex> lock2(mMutex);
			this->executeTaskMove(std::move(in));
		}
		
		return *this;
	}
	
	Task::~Task() noexcept = default;
	
	void Task::clear() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		this->executeTaskClear();
	}
	
	void Task::executeTaskCopy(const Task &in)
	{
		// @pre all locks needed are already held
		mProgress = in.mProgress;
		mException = in.mException;
		mTimeout = in.mTimeout;
		mStarted = in.mStarted;
		mInterruptor = in.mInterruptor;
		mLogger = in.mLogger;
	}
	
	void Task::executeTaskMove(Task &&in) noexcept
	{
		// @pre all locks needed are already held	
		if (mInterruptor)
		{
			mInterruptor(Reason::Canceled);
		}
	
		mProgress = in.mProgress;
		mException = std::move(in.mException);
		mTimeout = std::move(in.mTimeout);
		mStarted = std::move(in.mStarted);
		mInterruptor = std::move(in.mInterruptor);
		mLogger = in.mLogger;
		
		in.executeTaskClear();
	}
	
	void Task::executeTaskClear() noexcept
	{
		// @pre all locks needed are already held	
		if (mInterruptor)
		{
			mInterruptor(Reason::Canceled);
			mInterruptor = std::function<State (Reason)>();
		}
		
		mProgress.clear();
		mException = std::exception_ptr();
		mTimeout = std::chrono::milliseconds();
		mStarted = std::chrono::time_point<std::chrono::steady_clock>();
		mLogger = nullptr;
	}
	
	State Task::request() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (!mProgress.running())
		{
			mProgress.request(Action::Execute);
		}
		return mProgress;
	}
	
	Progress<std::chrono::milliseconds> Task::start() noexcept
	{
		auto startTime = std::chrono::steady_clock::now();
		std::lock_guard<std::mutex> lock(mMutex);
		return this->executeStart(startTime);
	}

	Progress<std::chrono::milliseconds> Task::start(Action action) noexcept
	{
		auto startTime = std::chrono::steady_clock::now();
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
		return this->executeStart(startTime);
	}

	Progress<std::chrono::milliseconds> Task::start(Action action, std::chrono::milliseconds timeout) noexcept
	{
		auto startTime = std::chrono::steady_clock::now();

		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
		mTimeout = timeout;		
		return this->executeStart(startTime);
	}

	Progress<std::chrono::milliseconds> Task::start(Action action, Length maxProgress) noexcept
	{
		auto startTime = std::chrono::steady_clock::now();

		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
		mProgress.total = maxProgress;
		return this->executeStart(startTime);
	}

	Progress<std::chrono::milliseconds> Task::start(Action action, std::chrono::time_point<std::chrono::steady_clock> startTime, std::chrono::milliseconds timeout) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
		mTimeout = timeout;
		return this->executeStart(startTime);
	}

	Progress<std::chrono::milliseconds> Task::start(Action action, Length maxProgress, std::chrono::time_point<std::chrono::steady_clock> startTime, std::chrono::milliseconds timeout) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
		mProgress.total = maxProgress;
		mTimeout = timeout;
		return this->executeStart(startTime);
	}

	void Task::reset() noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		
		if (mProgress.running())
		{
			mProgress.status = Status::Canceled;
			mProgress.reason = Reason::Canceled;
		
			if (mInterruptor)
			{
				mInterruptor(Reason::Canceled);
			}

			mCondition.notify_all();
		
			do
			{
				mCondition.wait(lock);
			}
			while (mProgress.unfinished());
		}
		
		mProgress.clear();
		mException = std::exception_ptr();
	}
	
	Progress<std::chrono::milliseconds> Task::executeStart(std::chrono::time_point<std::chrono::steady_clock> startTime) noexcept
	{
		// @pre lock is held
		Progress<std::chrono::milliseconds> timeout = this->checkRemaining();

		// If we don't have an action yet, set to generic Action::Execute
		if (mProgress.action == Action::None)
		{
			mProgress.action = Action::Execute;
		}

		// If we haven't started yet, completely clear out state			
		if (!timeout.started())
		{
			mProgress.count = 0;
			mProgress.start();
			mException = std::exception_ptr();
			mStarted = startTime;
			
			timeout.start();
			timeout.count = mTimeout; // we have full timeout remaining now
		}
		// Otherwise if we're resuming an interrupted task
		else if (timeout.running()) 
		{
			mProgress.resume();
		}
		
		return timeout;
	}

	Action Task::action() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress.action;
	}

	void Task::action(Action action) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.action = action;
	}

	Status Task::status() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress.status;
	}

	State Task::state() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress;
	}

	Progress<Length> Task::progress() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress;
	}
	
	bool Task::running() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress.running();
	}
		
	bool Task::completed() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mProgress.completed();
	}
	
	Progress<std::chrono::milliseconds> Task::update(Length achieved) noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		this->executeProgress(achieved, 0u);
		return this->executePoll(lock);	
	}
	
	Progress<std::chrono::milliseconds> Task::update(Length achieved, Length discovered) noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		this->executeProgress(achieved, discovered);
		return this->executePoll(lock);	
	}
	
	void Task::executeProgress(Length achieved, Length discovered) noexcept
	{
		// @pre mMutex is locked
		
		if (achieved > 0)
		{
			mProgress.count += achieved;
			mProgress.total += discovered;
			
			// First progress report, mark as updated
			if (mProgress.status < Status::Updated)
			{
				mProgress.status = Status::Updated;
				mCondition.notify_all();
			}

			if (mLogger)
			{
				mLogger->progress(mProgress);
			}
		}
		else if (discovered > 0)
		{
			mProgress.total += discovered;
			
			if (mLogger)
			{
				mLogger->progress(mProgress);
			}
		}
	}
	
	Logger *Task::getLogger() const noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		return mLogger;
	}
			
	void Task::setLogger(Logger *logger) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mLogger = logger;
	}
	
	void Task::unsetLogger() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mLogger = nullptr;
	}
	
	Progress<std::chrono::milliseconds> Task::checkElapsed() const noexcept
	{
		Progress<std::chrono::milliseconds> elapsed;
		
		if (mProgress.started())
		{
			auto time = std::chrono::steady_clock::now();
			elapsed.count = std::chrono::duration_cast<std::chrono::milliseconds>(time - mStarted);		
			elapsed.total = mTimeout;
			
			if (elapsed.count < elapsed.total || mProgress.completed())
			{
				elapsed.status = mProgress.status;
			}
			else 
			{
				elapsed.status = Status::Canceled;
				elapsed.reason = Reason::Timeout;
			}
		}
		
		return elapsed;	
	}
	
	Progress<std::chrono::milliseconds> Task::checkRemaining() const noexcept
	{
		Progress<std::chrono::milliseconds> elapsed = this->checkElapsed();
		Progress<std::chrono::milliseconds> remaining(elapsed);
		
		if (elapsed.count < elapsed.total)
		{
			remaining.count = elapsed.total - elapsed.count;
		}
		else
		{
			remaining.count = std::chrono::milliseconds(0);
		}
		
		return remaining;
	}
	
	Progress<std::chrono::milliseconds> Task::remaining() const noexcept
	{
		Progress<std::chrono::milliseconds> result;
		std::lock_guard<std::mutex> lock(mMutex);
		result = this->checkRemaining();		
		return result;
	}
	
	Progress<std::chrono::milliseconds> Task::poll() noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		return this->executePoll(lock);
	}
	
	Progress<std::chrono::milliseconds> Task::executePoll(std::unique_lock<std::mutex> &lock) noexcept
	{
		// @pre lock is held
		
		Progress<std::chrono::milliseconds> remaining;
				
		// Wait until task is resumed if it is paused
		while (mProgress.status == Status::Paused)
		{
			mCondition.wait(lock);
		}
		
		auto time = std::chrono::steady_clock::now();
		Progress<std::chrono::milliseconds> elapsed = this->checkElapsed();

		if (mLogger)
		{
			mLogger->time(elapsed);
		}
		
		remaining.status = elapsed.status;
		if (elapsed.count < elapsed.total)
		{
			remaining.count = elapsed.total - elapsed.count;
		}
		else
		{
			remaining.count = std::chrono::milliseconds(0);
		}

		// If we haven't completed, we need to check execution time
		if (!remaining.completed())
		{
		 	// we've exceeded the allowed execution time, force cancellation due to timeout
			if (elapsed.count > elapsed.total)
			{		
				mProgress.abandon(Reason::Timeout);
					
				if (mInterruptor)
				{
					mInterruptor(Reason::Timeout);
				}
				
				mCondition.notify_all();
			}
			else if (mProgress.status == Status::Canceled)
			{
				mProgress.abandon(Reason::Canceled);
			}
			// Otherwise we're good, pass through running status
		}
		// Otherwise pass through final result
		
		return remaining;
	}
	
	State Task::cancel(Reason reason) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);

		if (!mProgress.completed())
		{
			mProgress.status = Status::Canceled;
			mProgress.reason = reason;
		
			if (mInterruptor)
			{
				mInterruptor(reason);
			}

			mCondition.notify_all();
		}
		
		return mProgress;
	}
				
	void Task::succeed(Reason reason) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.succeed(reason);
		
		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();		
	}
	
	void Task::complete(Outcome outcome, Reason reason) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.complete(outcome, reason);
		
		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();
	}
	
	void Task::fail(Reason r) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.fail(r);

		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();
	}
	
	void Task::fail(Exception &&e) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.fail(e.reason());
		mException = std::make_exception_ptr(std::move(e));
		
		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();
	}
	
	void Task::fail(std::exception_ptr e) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.fail();
		mException = std::move(e);
		
		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();
	}
	
	void Task::fail(std::exception_ptr e, Reason reason) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mProgress.fail(reason);
		mException = std::move(e);
		
		if (mInterruptor)
		{
			mInterruptor(Reason::Interrupted);
		}
		
		mCondition.notify_all();
	}
	
	State Task::pause() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		
		if (mProgress.running())
		{
			mProgress.pause();
			
			if (mInterruptor)
			{
				mInterruptor(Reason::Interrupted);
			}
			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::resume() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		
		if (mProgress.running())
		{
			mProgress.resume();			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::pause(bool value) noexcept
	{
		return value ? this->pause() : this->resume();
	}
	
	State Task::interrupt() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		
		if (mProgress.running())
		{
			if (mInterruptor)
			{
				mInterruptor(Reason::Interrupted);
			}
			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::interrupt(Reason reason) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		
		if (mProgress.running())
		{
			if (mInterruptor)
			{
				mInterruptor(reason);
			}
			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::setInterruptor(std::function<State (Reason)> &&interruptor) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		mInterruptor = std::move(interruptor);
		
		if (mInterruptor && mProgress.interrupting())
		{
			State changed = mInterruptor(mProgress.reason);
			if (changed.status != Status::Interrupted)
			{
				mProgress = changed;
			}
			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::setSubtaskInterruptor(Task &subtask)
	{
		std::lock_guard<std::mutex> lock(mMutex);
		
		// Choose proper overload
		State (Task::*method)(Reason) = &Task::interrupt;
		mInterruptor = std::bind(method, &subtask, std::placeholders::_1);
		
		if (mProgress.interrupting())
		{
			State changed = mInterruptor(mProgress.reason);
			if (changed .status != Status::Interrupted)
			{
				mProgress = changed;
			}
			
			mCondition.notify_all();
		}
		
		return mProgress;
	}
	
	State Task::waitForUpdate() const noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		
		mCondition.wait(lock);
		
		return mProgress;	
	}
	
	State Task::waitForUpdate(std::chrono::milliseconds timeout) const noexcept
	{	
		auto mark = std::chrono::steady_clock::now();
	
		std::unique_lock<std::mutex> lock(mMutex);
		
		auto current = std::chrono::steady_clock::now();
		std::chrono::milliseconds remainder = std::chrono::duration_cast<std::chrono::milliseconds>(current - mark);
		
		if (remainder < timeout)
		{
			mCondition.wait_for(lock, remainder);			
		}
	
		return mProgress;
	}
	
	State Task::waitForCompletion() const noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);
		
		while (!mProgress.completed())
		{
			mCondition.wait(lock);
		}
		
		return mProgress;
	}
	
	State Task::waitForCompletion(std::chrono::milliseconds timeout) const noexcept
	{
		auto mark = std::chrono::steady_clock::now();
	
		std::unique_lock<std::mutex> lock(mMutex);
		
		State e;
		
		while (!mProgress.completed())
		{
			auto current = std::chrono::steady_clock::now();
			std::chrono::milliseconds remainder = std::chrono::duration_cast<std::chrono::milliseconds>(current - mark);
			if (remainder > timeout)
			{
				e.reason = Reason::Timeout;
				break;
			}
			else
			{
				mCondition.wait_for(lock, remainder);
				e = mProgress;
			}
		}
		
		return e;
	}
	
	State Task::waitForSuccess() const
	{
		std::unique_lock<std::mutex> lock(mMutex);
		
		while (!mProgress.completed())
		{
			mCondition.wait(lock);
		}
		
		if (mProgress.failed())
		{
			if (mException)
			{
				std::rethrow_exception(mException);
			}
			else
			{
				throw cio::Exception(mProgress);
			}
		}
		
		return mProgress;
	}
	
	State Task::waitForSuccess(std::chrono::milliseconds timeout) const
	{
		State e;
		auto mark = std::chrono::steady_clock::now();
	
		std::unique_lock<std::mutex> lock(mMutex);

		while (!mProgress.completed())
		{
			auto current = std::chrono::steady_clock::now();
			std::chrono::milliseconds remainder = std::chrono::duration_cast<std::chrono::milliseconds>(current - mark);
			if (remainder > timeout)
			{
				throw Exception(Reason::Timeout, "Timed out while waiting for execution to complete");
			}
			else
			{
				mCondition.wait_for(lock, remainder);
				e = mProgress;
			}
		}
		
		
		if (e.failed())
		{
			if (mException)
			{
				std::rethrow_exception(mException);
			}
			else
			{
				throw cio::Exception(mProgress);
			}
		}
		
		return e;
	}
	
	State Task::waitToResume() const noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);

		while (mProgress.status == Status::Paused)
		{
			mCondition.wait(lock);
		}
		
		return mProgress;
	}
	
	State Task::waitToResume(std::chrono::milliseconds timeout) const noexcept
	{
		State e;
		auto mark = std::chrono::steady_clock::now();
	
		std::unique_lock<std::mutex> lock(mMutex);

		while (mProgress.status == Status::Paused)
		{
			auto current = std::chrono::steady_clock::now();
			std::chrono::milliseconds remainder = std::chrono::duration_cast<std::chrono::milliseconds>(current - mark);
			if (remainder > timeout)
			{
				e.status = Status::None;
				e.reason = Reason::Timeout;
				break;
			}
			else
			{
				mCondition.wait_for(lock, remainder);
				e = mProgress;
			}
		}
		
		return e;
	}
	
	// All functions below lock() require obtaining a lock to be thread-safe
	
	std::unique_lock<std::mutex> Task::lock() const noexcept
	{
		return std::unique_lock<std::mutex>(mMutex);
	}
				
	std::unique_lock<std::mutex> Task::lockForCompletion() const noexcept
	{
		std::unique_lock<std::mutex> lock(mMutex);

		while (!mProgress.completed())
		{
			mCondition.wait(lock);
		}
		
		return lock;
	}
		
	std::unique_lock<std::mutex> Task::lockForCompletion(std::chrono::milliseconds timeout) const noexcept
	{
		auto mark = std::chrono::steady_clock::now();
	
		std::unique_lock<std::mutex> lock(mMutex);
		
		while (!mProgress.completed())
		{
			auto current = std::chrono::steady_clock::now();
			std::chrono::milliseconds remainder = std::chrono::duration_cast<std::chrono::milliseconds>(current - mark);
			if (remainder > timeout)
			{
				break;
			}
			else
			{
				mCondition.wait_for(lock, remainder);
			}
		}
		
		return lock;
	}
	
	std::chrono::time_point<std::chrono::steady_clock> Task::getStartTime() const noexcept
	{
		return mStarted;
	}

	void Task::setStartTime(std::chrono::time_point<std::chrono::steady_clock> startTime) noexcept
	{
		mStarted = startTime;
	}

	void Task::setTimeout(std::chrono::milliseconds timeout) noexcept
	{
		mTimeout = timeout;
	}
			
	void Task::unsetTimeout() noexcept
	{
		mTimeout = std::chrono::milliseconds::max();
	}
			
	std::chrono::milliseconds Task::getTimeout() const noexcept
	{
		return mTimeout;
	}
	
	void Task::notify() const noexcept
	{
		mCondition.notify_all();
	}
}

