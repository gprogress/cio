/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "AttributeClient.h"

#include "Characteristic.h"
#include "Descriptor.h"
#include "Property.h"
#include "Service.h"
#include "Subscription.h"
#include "TypeId.h"

#include <cio/Exception.h>
#include <cio/Class.h>
#include <cio/Response.h>
#include <cio/State.h>
#include <cio/Task.h>

namespace cio
{
	namespace bluetooth
	{
		Class<AttributeClient> AttributeClient::sMetaclass("cio::bluetooth::AttributeClient");
					
		const std::uint16_t AttributeClient::DEFAULT_ATT_CID(4);
		
		const std::uint16_t AttributeClient::DEFAULT_ATT_PSM(31);
		
		const std::size_t AttributeClient::DEFAULT_MAX_PACKET_SIZE(23u);
		
		const std::size_t AttributeClient::HIGHEST_MAX_PACKET_SIZE(251u);
	
		const Metaclass &AttributeClient::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		AttributeClient::AttributeClient() noexcept :
			mMaxPacketSize(DEFAULT_MAX_PACKET_SIZE),
			mMonitorConnectionState(true)
		{
			// nothing more to do
		}

		AttributeClient::AttributeClient(AttributeClient &&in) noexcept :
			mMaxPacketSize(in.mMaxPacketSize),
			mTypes(std::move(in.mTypes)),
			mMonitorConnectionState(in.mMonitorConnectionState),
			mConnectionState(in.mConnectionState)
		{
			in.mMaxPacketSize = DEFAULT_MAX_PACKET_SIZE;
		}
			
		AttributeClient &AttributeClient::operator=(AttributeClient &&in) noexcept
		{
			if (this != &in)
			{
				mMaxPacketSize = in.mMaxPacketSize;
				in.mMaxPacketSize = DEFAULT_MAX_PACKET_SIZE;
				
				mTypes = std::move(in.mTypes);
				mMonitorConnectionState = in.mMonitorConnectionState;
				mConnectionState = in.mConnectionState;
			}
			return *this;
		}
			
		AttributeClient::~AttributeClient() noexcept = default;
		
		const Metaclass &AttributeClient::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void AttributeClient::clear() noexcept
		{
			mMaxPacketSize = DEFAULT_MAX_PACKET_SIZE;
			mTypes.clear();
		}
		
		void AttributeClient::connect(const Peripheral &peripheral)
		{
			// nothing to do in base class
		}
					
		void AttributeClient::connect(const Device &adapter, const Peripheral &peripheral)
		{
			// nothing to do in base class
		}
		
		bool AttributeClient::isOpen() const noexcept
		{
			return false;
		}
				
		void AttributeClient::disconnect() noexcept
		{
			// nothing to do in base class
		}
		
		std::size_t AttributeClient::getMaxPacketSize() const noexcept
		{
			return mMaxPacketSize;
		}

		void AttributeClient::setMaxPacketSize(std::size_t mtu) noexcept
		{
			mMaxPacketSize = mtu;
		}
		
		std::string AttributeClient::getDeviceName()
		{
			std::string value;
			auto listener = [&](std::uint16_t handle, const void *data, std::size_t length)
			{
				value.assign(static_cast<const char *>(data), static_cast<const char *>(data) + length);
				return cio::succeed(Action::Read);
			};
			
			this->readValuesByType(TypeId::DeviceName, listener);
			return value;
		}
		
		std::vector<Attribute> AttributeClient::discoverAllAttributes()
		{
			return this->discoverAttributes(0x0001u, 0xFFFFu);
		}
		
		
		std::uint16_t AttributeClient::findAttributeHandle(const UniqueId &type)
		{
			return this->findAttributeHandleInRange(type, 0x0001u, 0xFFFFu);
		}
		
		std::vector<Service> AttributeClient::discoverAllPrimaryServices()
		{
			return this->discoverPrimaryServices(0x0001u, 0xFFFFu);
		}
				
		Service AttributeClient::findPrimaryService(const UniqueId &id)
		{
			return this->findPrimaryServiceInRange(id, 0x0001u, 0xFFFFu);
		}
		
		std::vector<Characteristic> AttributeClient::discoverAllCharacteristics()
		{
			return this->discoverCharacteristics(0x0001u, 0xFFFFu);
		}
		
		Characteristic AttributeClient::findCharacteristic(const UniqueId &type)
		{
			return this->findCharacteristicInRange(type, 0x0001u, 0xFFFFu);
		}
		
		std::vector<Descriptor> AttributeClient::discoverAllDescriptors()
		{
			return this->discoverDescriptors(0x0001u, 0xFFFFu);
		}
						
		Descriptor AttributeClient::findDescriptor(const UniqueId &type)
		{
			return this->findDescriptorInRange(type, 0x0001u, 0xFFFFu);
		}
		
		Progress<std::size_t> AttributeClient::getValue(std::uint16_t handle, void *buffer, std::size_t length, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Read);
			
			auto receiver = [&](std::uint16_t handle, const void *dataRead, std::size_t lengthRead)
			{
				std::size_t toCopy = std::min(length, lengthRead);
				if (toCopy > 0)
				{
					std::memcpy(buffer, dataRead, toCopy);
				}
				
				result.count = lengthRead;
				result.succeed();
				return result;
			};
			
			this->readValue(handle, receiver, offset);
			
			if (result.count < length)
			{
				std::memset(static_cast<std::uint8_t *>(buffer) + result.count, 0, length - result.count);
			}
			
			return result;
		}
		
		Progress<std::size_t> AttributeClient::getValueByType(const UniqueId &type, void *buffer, std::size_t length)
		{
			Progress<std::size_t> result(Action::Read);
			
			auto receiver = [&](std::uint16_t handle, const void *dataRead, std::size_t lengthRead)
			{
				std::size_t toCopy = std::min(length, lengthRead);
				if (toCopy > 0)
				{
					std::memcpy(buffer, dataRead, toCopy);
				}
				
				result.count = lengthRead;
				result.succeed();
				return result;
			};
			
			this->readValuesByTypeInRange(type, receiver, 0x0001u, 0xFFFFu);
			
			if (result.count < length)
			{
				std::memset(static_cast<std::uint8_t *>(buffer) + result.count, 0, length - result.count);
			}
			
			return result;
		}	
		
		Progress<std::size_t> AttributeClient::readValuesByType(const UniqueId &type, const AttributeCallback &listener)
		{
			return this->readValuesByTypeInRange(type, listener, 0x0001u, 0xFFFFu);
		}
		
		std::size_t AttributeClient::negotiateMaxPacketSize(std::size_t mtu)
		{
			// Clamp to valid range
			mMaxPacketSize = std::min(std::max(mtu, DEFAULT_MAX_PACKET_SIZE), HIGHEST_MAX_PACKET_SIZE);
			return mMaxPacketSize; 
		}
					
		UniqueId AttributeClient::getAttributeType(std::uint16_t handle)
		{
			return UniqueId();
		}
					
		std::vector<Attribute> AttributeClient::discoverAttributes(std::uint16_t start, std::uint16_t end)
		{
			return std::vector<Attribute>();
		}
		
		std::uint16_t AttributeClient::findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			return 0;
		}
				
		Service AttributeClient::getService(std::uint16_t handle)
		{
			return Service();
		}
		
		std::vector<Service> AttributeClient::discoverPrimaryServices(std::uint16_t start, std::uint16_t end)
		{	
			return std::vector<Service>();
		}
		
		Service AttributeClient::findPrimaryServiceInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			return Service();
		}
		
		Characteristic AttributeClient::getCharacteristic(std::uint16_t handle)
		{
			return Characteristic();
		}
		
		std::vector<Characteristic> AttributeClient::discoverCharacteristics(std::uint16_t start, std::uint16_t end)
		{
			return std::vector<Characteristic>();
		}

		Characteristic AttributeClient::findCharacteristicInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			return Characteristic();
		}
		
		Descriptor AttributeClient::getDescriptor(std::uint16_t handle)
		{
			return Descriptor();
		}

		std::vector<Descriptor> AttributeClient::discoverDescriptors(std::uint16_t start, std::uint16_t end)
		{
			return std::vector<Descriptor>();
		}
			
		Descriptor AttributeClient::findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			return Descriptor();	
		}
		
		Progress<std::size_t> AttributeClient::readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset)
		{
			return cio::fail(Action::Read, Reason::Unsupported);
		}
				
		Progress<std::size_t> AttributeClient::readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end)
		{
			return cio::fail(Action::Read, Reason::Unsupported);
		}
				
		Progress<std::size_t> AttributeClient::writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset)
		{
			return cio::fail(Action::Write, Reason::Unsupported);
		}
				
		Progress<std::size_t> AttributeClient::sendValue(std::uint16_t handle, const void *buffer, std::size_t requested)
		{
			return cio::fail(Action::Write, Reason::Unsupported);
		}
		
		TypeDictionary &AttributeClient::getTypes() noexcept
		{
			return mTypes;
		}
		
		const TypeDictionary &AttributeClient::getTypes() const noexcept
		{
			return mTypes;
		}
		
		const Subscription *AttributeClient::subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval)
		{
			Response<Subscription *> sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
			sub.value->setModes(Property::Read);
			sub.value->setCurrentMode(Property::Read);
			return sub.value;
		}
				
		State AttributeClient::executeSubscriptions(Task &exec) noexcept
		{
			State status;
			
			Progress<std::chrono::milliseconds> timeout = exec.poll();
			Subscription *next =  mSubscriptions.schedule(timeout.count);
			
			while (next && !timeout.completed())
			{
				this->readValue(next->getHandle(), next->getListener(), 0);
				timeout = exec.poll();
				next = mSubscriptions.schedule(timeout.count);
			} 
			
			return status;
		}
				
		State AttributeClient::unsubscribe(const Subscription *subscription)
		{
			return mSubscriptions.unsubscribe(subscription);
		}
				
		Progress<std::size_t> AttributeClient::clearSubscriptions(std::uint16_t handle)
		{
			return mSubscriptions.unsubscribe(handle);
		}
				
		Progress<std::size_t> AttributeClient::clearAllSubscriptions()
		{
			return mSubscriptions.unsubscribe();
		}
								
		Subscriptions &AttributeClient::getSubscriptions() noexcept
		{
			return mSubscriptions;
		}
				
		const Subscriptions &AttributeClient::getSubscriptions() const noexcept
		{
			return mSubscriptions;
		}
		
		std::vector<Service> &AttributeClient::organizeServices(std::vector<Service> &services, std::vector<Characteristic> &chars, std::vector<Descriptor> &decls)
		{
			if (!services.empty())
			{
				// Ensure services are in ascending start handle order
				std::sort(services.begin(), services.end());
				
				// Set service end handles accordingly
				{
					auto cur = services.begin();
					auto prev = cur++;
					while (cur != services.end())
					{
						prev->setEndHandle(cur->getHandle() - 1);
						prev = cur++;
					}
					prev->setEndHandle(0xFFFFu);
				}
					
				// Determine which services each characteristic belongs to and which characteristic each declaration belongs to
				if (!chars.empty())
				{
					// Sort characteristics into ascending order
					std::sort(chars.begin(), chars.end());
					
					// Sort declarations into ascending order
					std::sort(decls.begin(), decls.end());
					
					auto charItr = chars.begin();
					auto declItr = decls.begin();
						
					// Process services until we are out of characteristics
					for (Service &service : services)
					{
						std::uint16_t start = service.getStartHandle();
						std::uint16_t limit = service.getEndHandle();
						std::vector<Characteristic> serviceChars;
						
						// Skip characteristics before this service (shouldn't normally happen)
						while (charItr != chars.end() && charItr->getHandle() <= start)
						{
							++charItr;
						}
						
						if (charItr != chars.end())
						{
							// Does the next characteristic belong to this service?
							if (charItr->getHandle() <= limit)
							{
								// Okay, go through characteristics until we find all of them for this service
								auto charStartItr = charItr;
								while (++charItr != chars.end() && charItr->getHandle() <= limit)
								{
									// keep looping to increment
								}
								
								// We have at least one characteristic for this service, so let's set that up
								serviceChars.reserve(charItr - charStartItr);
								while (charStartItr != charItr)
								{
									serviceChars.emplace_back(std::move(*charStartItr));
									++charStartItr;
								}
							}
						}
									
						// Service will set end handles and client on characteristics
						service.setCharacteristics(std::move(serviceChars));
								
						// Now let's organize declarations into characteristics that we just set up (if we have any left)
						for (Characteristic &c : service)
						{
							std::uint16_t cstart = c.getStartHandle();
							std::uint16_t climit = c.getEndHandle();
							std::vector<Descriptor> charDecls;
							
							// Skip declarations before this characteristic (shouldn't normally happen)
							while (declItr != decls.end() && declItr->getHandle() <= cstart)
							{
								++declItr;
							}
							
							if (declItr != decls.end())
							{
								// Does the next declaration belong to this characteristic?
								if (declItr->getHandle() <= climit)
								{
									auto declStartItr = declItr;
									while (++declItr != decls.end() && declItr->getHandle() <= climit)
									{
										// keep looping to increment
									}
									
									// We have at least one declaration for this characteristic so set that up
									charDecls.reserve(declItr - declStartItr);
									while (declStartItr != declItr)
									{
										charDecls.emplace_back(std::move(*declStartItr));
										++declStartItr;
									}
								}
							}
														
							// Service will set end handles and client on characteristics
							c.setDescriptors(std::move(charDecls));
						}
					}
				}		
			}		
			
			return services;			
		}
		
		bool AttributeClient::getMonitorConnectionState() const noexcept
		{
			return mMonitorConnectionState;
		}
		
		void AttributeClient::setMonitorConnectionState(bool value)
		{
			mMonitorConnectionState = value;
		}
		
		State AttributeClient::getConnectionState() const noexcept
		{
			return mConnectionState;
		}
		
		AttributeClient::operator bool() const noexcept
		{
			return this->isOpen();
		}
	}
}
