/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ChannelType.h"

#include <cio/Case.h>

#include <iostream>

#if defined CIO_USE_UNIX_SOCKETS
#include <sys/socket.h>
#endif

#if defined CIO_USE_WINSOCK2
#include <Winsock2.h>
#endif

namespace cio
{
	namespace net
	{
		namespace
		{
			const char *sChannelTypeText[] =
			{
				"None",
				"Stream",
				"Datagram",
				"Sequential",
				"Raw",
				"Reliable",
				"Unknown"
			};

			int sNativeChannelTypes[] =
			{
				0,
				SOCK_STREAM,
				SOCK_DGRAM,
				SOCK_SEQPACKET,
				SOCK_RAW,
				SOCK_RDM,
				0
			};
		}

		const char *print(ChannelType type) noexcept
		{
			return sChannelTypeText[static_cast<unsigned>(type)];
		}

		int toNativeSocketType(ChannelType type) noexcept
		{
			return sNativeChannelTypes[static_cast<unsigned>(type)];
		}

		ChannelType fromNativeSocketType(int code) noexcept
		{
			ChannelType type = ChannelType::Unknown;

			for (std::size_t i = 0; i < sizeof(sNativeChannelTypes) / sizeof(int); ++i)
			{
				if (sNativeChannelTypes[i] == code)
				{
					type = static_cast<ChannelType>(i);
					break;
				}
			}

			return type;
		}

		Parse<ChannelType> parseChannelType(const char *text) noexcept
		{
			Parse<ChannelType> type;

			if (text)
			{
				std::size_t len = std::strlen(text);
				type = parseChannelType(text, len);
			}

			return type;
		}

		Parse<ChannelType> parseChannelType(const char *text, std::size_t len) noexcept
		{
			Parse<ChannelType> type;
			type.value = ChannelType::Unknown;

			for (unsigned i = 0; i < sizeof(sChannelTypeText) / sizeof(const char *); ++i)
			{
				if (CIO_STRNCASECMP(sChannelTypeText[i], text, len) == 0)
				{
					type.value = static_cast<ChannelType>(i);
					type.status = Validation::Exact;
					type.parsed = len;
					break;
				}
			}

			return type;
		}

		Parse<ChannelType> parseChannelType(const std::string &text) noexcept
		{
			return parseChannelType(text.c_str(), text.size());
		}

		std::ostream &operator<<(std::ostream &stream, ChannelType mode)
		{
			return (stream << print(mode));
		}

		std::istream &operator>>(std::istream &stream, ChannelType &mode)
		{
			std::string chunk;
			stream >> chunk;
			mode = parseChannelType(chunk);
			return stream;
		}
	}
}

