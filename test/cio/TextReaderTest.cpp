/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TextReaderTest.h"

#include <cio/Input.h>
#include <cio/ModeSet.h>
#include <cio/Path.h>

#include <cio/TextReader.h>
#include <cio/test/Assert.h>

#include <cmath>

namespace cio
{
	TextReaderTest::TextReaderTest() :
		cio::test::Suite("cio/TextReader")
	{		
		this->addTestCase("TextReader", &TextReaderTest::testConstruction);
		this->addTestCase("operator=", &TextReaderTest::testAssignment);
		this->addTestCase("token", &TextReaderTest::testToken);
		this->addTestCase("token", &TextReaderTest::testTokenSkipWhitespace);
		this->addTestCase("reset", &TextReaderTest::testReset);
		this->addTestCase("clear", &TextReaderTest::testClear);
	}

	TextReaderTest::~TextReaderTest() noexcept = default;

	void TextReaderTest::testConstruction()
	{
		// Default construction
		{
			TextReader tr;
			CIO_ASSERT(!tr.input());
			CIO_ASSERT(!tr.source().getFile());
			CIO_ASSERT(!tr.buffer().data());
		}

		// Copy construction
		{
			TextReader tr;
			TextReader tr2(tr);

			CIO_ASSERT(!tr.input());
			CIO_ASSERT(!tr.source().getFile());
			CIO_ASSERT(!tr.buffer().data());

			CIO_ASSERT(!tr2.input());
			CIO_ASSERT(!tr2.source().getFile());
			CIO_ASSERT(!tr2.buffer().data());

		}

		// Move construction
		{
			TextReader tr;
			TextReader tr2(std::move(tr));

			CIO_ASSERT(!tr2.input());
			CIO_ASSERT(!tr2.source().getFile());
			CIO_ASSERT(!tr2.buffer().data());
		}
	}

	void TextReaderTest::testAssignment()
	{
		// Copy assignment
		{
			TextReader tr;
			TextReader tr2;

			tr2 = tr;

			CIO_ASSERT(!tr.input());
			CIO_ASSERT(!tr.source().getFile());
			CIO_ASSERT(!tr.buffer().data());

			CIO_ASSERT(!tr2.input());
			CIO_ASSERT(!tr2.source().getFile());
			CIO_ASSERT(!tr2.buffer().data());

		}

		// Move assignment
		{
			TextReader tr;
			TextReader tr2;

			tr2 = std::move(tr);

			CIO_ASSERT(!tr2.input());
			CIO_ASSERT(!tr2.source().getFile());
			CIO_ASSERT(!tr2.buffer().data());

		}
	}

	void TextReaderTest::testTokenSkipWhitespace()
	{
		std::uint8_t bytes[] = "-32768 \r\t 65535\n-212\t 35 = ";
		Buffer buffer(sizeof(bytes));
		std::memcpy(buffer.data(), bytes, sizeof(bytes));
		TextReader tr;
		tr.setBuffer(buffer);
		std::vector<Token> tokenVector;
		Token tok = tr.nextToken(true);
		while (tok.empty() != true)
		{
			tokenVector.push_back(tok);
			tok = tr.nextToken(true);
		}

		/* Positive and Negative integer comparisons */
		CIO_ASSERT_EQUAL(-32768, tokenVector.at(0).to<short>());
		CIO_ASSERT_EQUAL(65535u, tokenVector.at(1).to<unsigned short>());
		CIO_ASSERT_EQUAL(-212, tokenVector.at(2).to<int>());
		CIO_ASSERT_EQUAL(35, tokenVector.at(3).to<unsigned int>());
		CIO_ASSERT_EQUAL("=", tokenVector.at(4).to<std::string>());
	}
		
	void TextReaderTest::testToken()
	{
		std::uint8_t bytes[] = "-32768 \r\t 65535\n-212\t 35   01.23 -.52 NaN iNF 97 abcDe . s";
		Buffer buffer(sizeof(bytes));
		std::memcpy(buffer.data(), bytes, sizeof(bytes));
		TextReader tr;
		tr.setBuffer(buffer);
		std::vector<Token> tokenVector;

		Token tok = tr.nextToken();
		while (tok)
		{
			tokenVector.push_back(tok);
			tok = tr.nextToken();
		}

		/* Positive and Negative integer comparisons */
		//At the minimum limit for short
		CIO_ASSERT_EQUAL(-32768, tokenVector.at(0).to<short>());

		//Handle a variety of whitespace characters
		CIO_ASSERT(tokenVector.at(1).isWhitespace());

		//At the maximum for unsigned short
		CIO_ASSERT_EQUAL(65535u, tokenVector.at(2).to<unsigned short>());
		CIO_ASSERT(tokenVector.at(3).isWhitespace());

		CIO_ASSERT_EQUAL(-212, tokenVector.at(4).to<int>());
		CIO_ASSERT_EQUAL(true, tokenVector.at(5).isWhitespace());
		CIO_ASSERT_EQUAL(35, tokenVector.at(6).to<unsigned int>());
		CIO_ASSERT_EQUAL(true, tokenVector.at(7).isWhitespace());

		/* Double and float comparisons */
		bool valueEqual = false;
		float precisionFactor = 0.01f;
		
		//Handle positive and negative decimals, varying formats
		if (tokenVector.at(8).to<double>() < (1.23 + precisionFactor) &&
			tokenVector.at(8).to<double>() > (1.23 - precisionFactor))
			valueEqual = true;
		CIO_ASSERT(valueEqual);
		CIO_ASSERT_EQUAL(true, tokenVector.at(9).isWhitespace());

		valueEqual = false;
		if (tokenVector.at(10).to<long double>() < (-0.52 + precisionFactor) &&
			tokenVector.at(10).to<long double>() > (-0.52 - precisionFactor))
			valueEqual = true;
		CIO_ASSERT(valueEqual);
		CIO_ASSERT(tokenVector.at(11).isWhitespace());

		//NaN
		CIO_ASSERT(std::isnan(tokenVector.at(12).to<long double>()));
		CIO_ASSERT(std::isnan(tokenVector.at(12).to<double>()));
		CIO_ASSERT(std::isnan(tokenVector.at(12).to<float>()));
		CIO_ASSERT(tokenVector.at(13).isWhitespace());

		//iNF
		CIO_ASSERT(std::isinf(tokenVector.at(14).to<long double>()));
		CIO_ASSERT(std::isinf(tokenVector.at(14).to<double>()));
		CIO_ASSERT(std::isinf(tokenVector.at(14).to<float>()));
		CIO_ASSERT(tokenVector.at(15).isWhitespace());

		/* Text Input */
		//Convert unsigned char digits (97) to single character
		CIO_ASSERT_EQUAL('a', tokenVector.at(16).to<unsigned char>());
		CIO_ASSERT(tokenVector.at(17).isWhitespace());

		//Handle strings
		CIO_ASSERT_EQUAL("abcDe", tokenVector.at(18).to<std::string>());
		CIO_ASSERT(tokenVector.at(19).isWhitespace());

		//Handle single characters, non-digit values
		CIO_ASSERT_EQUAL('.', tokenVector.at(20).to<char>());
		CIO_ASSERT(tokenVector.at(21).isWhitespace());
		CIO_ASSERT_EQUAL('s', tokenVector.at(22).to<char>());
	}

	void TextReaderTest::testReset()
	{
		TextReader tr;

		tr.reset();
		CIO_ASSERT_EQUAL(0u, tr.source().getLine());
		CIO_ASSERT_EQUAL(0u, tr.source().getColumn());

		std::uint8_t bytes[6] = { 0x61, 0x62, 0x63, 0x64, 0x65, '\n' };
		Buffer buffer(sizeof(bytes));
		std::memcpy(buffer.data(), bytes, sizeof(bytes));
		tr.setBuffer(buffer);

		Token tok = tr.nextToken();
		CIO_ASSERT_EQUAL("abcde", tok.to<std::string>());
		CIO_ASSERT_EQUAL(5u, tr.source().getColumn());
		CIO_ASSERT_EQUAL(0u, tr.source().getLine());
		tr.reset();
		CIO_ASSERT_EQUAL(0u, tr.source().getColumn());
		CIO_ASSERT_EQUAL(0u, tr.source().getLine());

		tok = tr.nextToken();
		CIO_ASSERT(tok.isWhitespace());
		CIO_ASSERT_EQUAL(0u, tr.source().getColumn());
		CIO_ASSERT_EQUAL(1u, tr.source().getLine());

		tr.reset();
		CIO_ASSERT_EQUAL(0u, tr.source().getColumn());
		CIO_ASSERT_EQUAL(0u, tr.source().getLine());
	}

	void TextReaderTest::testClear()
	{
		TextReader tr;

		CIO_ASSERT(!tr.input());
		CIO_ASSERT(!tr.source().getFile());
		CIO_ASSERT(!tr.buffer().data());

		std::uint8_t bytes[5] = { 0x61, 0x62, 0x63, 0x64, 0x65 };
		Buffer buffer(sizeof(bytes));
		std::memcpy(buffer.data(), bytes, sizeof(bytes));
		tr.setBuffer(buffer);

		CIO_ASSERT(tr.buffer().data());

		tr.clear();
		CIO_ASSERT(!tr.buffer().data());
	}
}
