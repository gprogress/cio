/************************************************************************
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DirectoryTest.h"

#include <cio/Buffer.h>
#include <cio/Exception.h>
#include <cio/Input.h>
#include <cio/Metadata.h>
#include <cio/test/Assert.h>
#include <cio/File.h>
#include <cio/Filesystem.h>

#include <set>

#include <iostream>

namespace cio
{
	DirectoryTest::DirectoryTest() :
		cio::test::Suite("cio/Directory")
	{
		CIO_TEST_METHOD(DirectoryTest, open);
		CIO_TEST_METHOD(DirectoryTest, next);
		CIO_TEST_METHOD(DirectoryTest, filename);
		CIO_TEST_METHOD(DirectoryTest, current);
		CIO_TEST_METHOD(DirectoryTest, traversable);
		CIO_TEST_METHOD(DirectoryTest, openCurrentInput);
		CIO_TEST_METHOD(DirectoryTest, openInput);
		CIO_TEST_METHOD(DirectoryTest, checkForSpecialWindowsDevices);
		CIO_TEST_METHOD(DirectoryTest, checkForSpecialFilename);
		CIO_TEST_METHOD(DirectoryTest, clear);
	}

	DirectoryTest::~DirectoryTest() noexcept = default;

	cio::Severity DirectoryTest::setUp()
	{
		mSampleDir = mProtocol.createTemporaryDirectory();
		CIO_ASSERT(!mSampleDir.empty());

		cio::Path prefix = mSampleDir;

		char quxText[] = "This is the cio::file::DirectoryTest content for QUX.txt";
		Buffer &quxContent = mExpectedContent["QUX.txt"];
		quxContent << quxText;
		quxContent.flip();

		// Expected entries are the three we create plus the two special ones
		{
			mExpectedEntries["."] = Metadata(".", ModeSet::general(), Resource::Directory);
			mExpectedEntries[".."] = Metadata("..", ModeSet::general(), Resource::Directory);
			mExpectedEntries["foobar"] = Metadata("foobar", ModeSet::general(), Resource::Directory);
			mExpectedEntries["bazdir"] = Metadata("bazdir", ModeSet::general(), Resource::Directory);
			mExpectedEntries["QUX.txt"] = Metadata("QUX.txt", ModeSet::general(), Resource::File, Length(quxContent.limit()));
		}
			
		CIO_ASSERT_EQUAL(cio::succeed(Action::Create), mProtocol.createDirectory(prefix / "foobar"));
		CIO_ASSERT_EQUAL(cio::succeed(Action::Create), mProtocol.createDirectory(prefix / "bazdir"));
		CIO_ASSERT_EQUAL(cio::succeed(Action::Create), mProtocol.createFile(prefix / "QUX.txt"));

		File quxFile(prefix / "QUX.txt");
		quxFile.write(quxContent.data(), quxContent.limit());

		return cio::Severity::None;
	}

	void DirectoryTest::tearDown() noexcept
	{
		if (!mSampleDir.empty())
		{
			cio::Path prefix = mSampleDir;

			mProtocol.erase(prefix / "foobar");
			mProtocol.erase(prefix / "bazdir");
			mProtocol.erase(prefix / "QUX.txt");
			mProtocol.erase(mSampleDir);
		}
	}

	void DirectoryTest::openTest()
	{
		Directory dir;
		CIO_ASSERT(dir.getNativeHandle() == nullptr);
		CIO_ASSERT(dir.getNativeEntry() == nullptr);

		// traversableate that trying to open the empty directory throws
		CIO_ASSERT_THROW(dir.open(""), cio::Exception);
		CIO_ASSERT(dir.getNativeHandle() == nullptr);
		CIO_ASSERT(dir.getNativeEntry() == nullptr);

		// The one directory we can be pretty sure of its existence and contents (to a degree) is the app dir
		cio::Path appdir = mProtocol.getApplicationDirectory();
		cio::ModeSet expected = cio::Mode::Read | cio::Mode::Load;
		CIO_ASSERT_EQUAL(expected, dir.open(appdir));
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		// Test the sample directory
		// @pre setUp succeeded
		CIO_ASSERT_EQUAL(expected, dir.open(mSampleDir));
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		// now test a subdirectory of that which we know for sure won't exist
		cio::Path missing = mSampleDir / "missing";
		CIO_ASSERT_THROW(dir.open(missing), cio::Exception);
		CIO_ASSERT(dir.getNativeHandle() == nullptr);
		CIO_ASSERT(dir.getNativeEntry() == nullptr);

		// test a known subdirectory
		cio::Path subdir = mSampleDir / "bazdir";
		CIO_ASSERT_EQUAL(expected, dir.open(subdir));

		// test a file that exists but is not a directory
		cio::Path qux = mSampleDir / "QUX.txt";
		CIO_ASSERT_THROW(dir.open(qux), cio::Exception);
		// @note tearDown will clean up
	}

	void DirectoryTest::filenameTest()
	{
		Directory dir;
		CIO_ASSERT_EQUAL("", dir.filename());

		// Start with the expected entries and remove them as we process
		std::map<std::string, Metadata> missing = mExpectedEntries;

		dir.open(mSampleDir);
		while (dir.traversable())
		{
			std::string entry = dir.filename();
			CIO_ASSERT(!entry.empty());

			bool match = missing.erase(entry);
			if (!match)
			{
				if (mExpectedEntries.count(entry))
				{
					throw cio::test::Fault("Duplicate directory entry found " + entry, CIO_CURRENT_SOURCE(), cio::Severity::Critical);
				}
				else
				{
					throw cio::test::Fault("Unexpected directory entry found " + entry, CIO_CURRENT_SOURCE(), cio::Severity::Critical);
				}
			}

			dir.next();
		}
			
		if (!missing.empty())
		{
			throw cio::test::Fault("Not all expected directory entries found", CIO_CURRENT_SOURCE(), cio::Severity::Critical);
		}
	}
		
	void DirectoryTest::currentTest()
	{
		Directory dir;
		CIO_ASSERT_EQUAL(Metadata(), dir.current());

		// Start with the expected entries and remove them as we process
		std::map<std::string, Metadata> missing = mExpectedEntries;

		dir.open(mSampleDir);
		while (dir.traversable())
		{
			Metadata entry = dir.current();
			CIO_ASSERT(entry);
				
			const std::string &filename = entry.getLocation().str();
				
			auto found = mExpectedEntries.find(filename);
			if (found == mExpectedEntries.end())
			{
				throw cio::test::Fault("Unexpected directory entry found " + filename, CIO_CURRENT_SOURCE(), cio::Severity::Critical);
			}
				
			CIO_ASSERT_METADATA_EQUAL(found->second, entry);
				
			bool match = missing.erase(filename);
			if (!match)
			{
				if (mExpectedEntries.count(filename))
				{
					throw cio::test::Fault("Duplicate directory entry found " + filename, CIO_CURRENT_SOURCE(), cio::Severity::Critical);
				}
			}

			dir.next();
		}
			
		if (!missing.empty())
		{
			throw cio::test::Fault("Not all expected directory entries found", CIO_CURRENT_SOURCE(), cio::Severity::Critical);
		}
	}
		
	void DirectoryTest::nextTest()
	{
		Directory dir;

		CIO_ASSERT_EQUAL(cio::fail(Action::Discover, Reason::Unopened), dir.next());

		dir.open(mSampleDir);
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		CIO_ASSERT_EQUAL(cio::succeed(Action::Discover), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		CIO_ASSERT_EQUAL(cio::succeed(Action::Discover), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		CIO_ASSERT_EQUAL(cio::succeed(Action::Discover), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		CIO_ASSERT_EQUAL(cio::succeed(Action::Discover), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		CIO_ASSERT_EQUAL(cio::fail(Action::Discover, Reason::Underflow), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		// native entry state is platform-dependent

		CIO_ASSERT_EQUAL(cio::fail(Action::Discover, Reason::Underflow), dir.next());
		CIO_ASSERT(dir.getNativeHandle() != nullptr);
		// native entry state is pltaform dependent
		//
		dir.clear();
		CIO_ASSERT_EQUAL(cio::fail(Action::Discover, Reason::Unopened), dir.next());
	}

	void DirectoryTest::traversableTest()
	{
		Directory dir;

		CIO_ASSERT_EQUAL(false, dir.traversable());

		dir.open(mSampleDir);
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.next();
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.next();
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.next();
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.next();
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.next();
		CIO_ASSERT_EQUAL(false, dir.traversable());

		dir.clear();
		CIO_ASSERT_EQUAL(false, dir.traversable());

		dir.open(mSampleDir);
		CIO_ASSERT_EQUAL(true, dir.traversable());

		dir.clear();
		CIO_ASSERT_EQUAL(false, dir.traversable());
	}
		
	void DirectoryTest::openCurrentInputTest()
	{
		Directory dir;
			
		std::unique_ptr<Input> input;
			
		CIO_ASSERT_THROW(input = dir.openCurrentInput(), cio::Exception);

		dir.open(mSampleDir);
		while (dir)
		{
			bool thrown = false;
			input.reset();

			try
			{
				input = dir.openCurrentInput();
			}
			catch (const Exception &)
			{
				thrown = true;
			}
				
			std::string filename = dir.filename();
				
			auto found = mExpectedEntries.find(filename);
			if (found == mExpectedEntries.end())
			{
				throw cio::test::Fault("Unexpected directory entry found " + filename, CIO_CURRENT_SOURCE(), Severity::Error);
			}
				
			auto data = mExpectedContent.find(filename);
			if (data == mExpectedContent.end())
			{
				if (input || !thrown)
				{
					throw cio::test::Fault("Entry unexpected allowed opening current file: " + filename, CIO_CURRENT_SOURCE(), Severity::Error);
				}
			}
			else
			{
				CIO_ASSERT(input);
				CIO_ASSERT(!thrown);
				CIO_ASSERT(dynamic_cast<File *>(input.get()));
					
				const Buffer &expected = data->second;
					
				Buffer actual(4096);
				input->requestFill(actual);
				actual.flip();
					
				CIO_ASSERT_BYTES_EQUAL(expected.current(), expected.remaining(), actual.current(), actual.remaining());
			}

			dir.next();
		}
	}
		
	void DirectoryTest::openInputTest()
	{
		Directory dir;
			
		std::unique_ptr<Input> input;
			
		// Test empty path on empty dir
		CIO_ASSERT_THROW(input = dir.openInput(Path()), cio::Exception);

		// Test traversable input on empty dir
		CIO_ASSERT_THROW(input = dir.openInput("QUX.txt"), cio::Exception);
		
		// Open sample dir
		dir.open(mSampleDir);
			
		// try all sample directories and see what we have
		for (const std::pair<std::string, Metadata> &item : mExpectedEntries)
		{
			if (item.second.getType() == Resource::Directory)
			{
				CIO_ASSERT_THROW(input = dir.openInput(item.first), cio::Exception);
			}
			else
			{
				input = dir.openInput(item.first);
				CIO_ASSERT(input);
				CIO_ASSERT(dynamic_cast<File *>(input.get()));
					
				const Buffer &expected = mExpectedContent[item.first];
				Buffer actual(4096);
				input->requestFill(actual);
				actual.flip();
				CIO_ASSERT_BYTES_EQUAL(expected.current(), expected.remaining(), actual.current(), actual.remaining());
			}
		}
			
		// Try a file we know doesn't exist
		CIO_ASSERT_THROW(input = dir.openInput("Beef.txt"), cio::Exception);
	}

	void DirectoryTest::checkForSpecialWindowsDevicesTest()
	{
		cio::Resource windowsDeviceType = cio::Resource::Device;

		// This method isn't supposed to check for '.' or '..' so traversableate that
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices(nullptr));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices(""));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("."));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices(".."));

		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("..."));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("foo"));

		// traversableate the set of windows device types for the current platform
		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("AUX"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("AUXC"));

		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("CON"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("CONP"));

		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("NUL"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("NULL"));

		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("PRN"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("PRNP"));

		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("COM0"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("COMX"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("COM10"));

		CIO_ASSERT_EQUAL(windowsDeviceType, Directory::checkForSpecialWindowsDevices("LPT5"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("LPTY"));
		CIO_ASSERT_EQUAL(cio::Resource::None, Directory::checkForSpecialWindowsDevices("LPT20"));
	}

	void DirectoryTest::checkForSpecialFilenameTest()
	{
		Directory dir;

		cio::Resource windowsDeviceType = cio::Resource::None;
#if defined _WIN32
		windowsDeviceType = cio::Resource::Device;
#endif
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename(nullptr));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename(""));
		CIO_ASSERT_EQUAL(cio::Resource::Directory, dir.checkForSpecialFilename("."));
		CIO_ASSERT_EQUAL(cio::Resource::Directory, dir.checkForSpecialFilename(".."));

		// traversableate that length parameter is honored
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("..."));

		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("foo"));

		// traversableate the set of windows device types for the current platform
		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("AUX"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("AUXC"));

		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("CON"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("CONP"));


		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("NUL"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("NULL"));

		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("PRN"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("PRNP"));

		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("COM0"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("COMX"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("COM10"));

		CIO_ASSERT_EQUAL(windowsDeviceType, dir.checkForSpecialFilename("LPT5"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("LPTY"));
		CIO_ASSERT_EQUAL(cio::Resource::None, dir.checkForSpecialFilename("LPT20"));
	}

	void DirectoryTest::clearTest()
	{
		Directory dir;

		dir.clear();
		CIO_ASSERT(dir.getNativeEntry() == nullptr);

		dir.open(mSampleDir);
		CIO_ASSERT(dir.getNativeEntry() != nullptr);

		dir.clear();
		CIO_ASSERT_EQUAL("", dir.current());
		CIO_ASSERT_EQUAL(false, dir.traversable());
		CIO_ASSERT(dir.getNativeEntry() == nullptr);
		CIO_ASSERT_EQUAL(cio::fail(Action::Discover, Reason::Unopened), dir.next());
	}
}

