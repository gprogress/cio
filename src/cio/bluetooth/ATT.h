/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_ATT_H
#define CIO_BLUETOOTH_ATT_H

#include "Types.h"

#include <iosfwd>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The ATT enumeration provides the standard operation codes for attribute requests, responses, and commands.
		 */
		enum class ATT : std::uint8_t
		{
			/** No ATT opcode is set */
			None =			0x00,
					
			/** An error occurred and the response includes the request type, attribute handle, and Error code */
			Error = 		0x01,
			
			/** Requesting the server's maximum transmission unit (MTU) and sending the client's (MTU) */
			MaxPacketSizeRequest =	0x02,
				
			/** The server responded with its maximum transmission unit (MTU) */
			MaxPacketSizeResponse  = 0x03,
			
			/** Requests to get attribute handle and types in the given handle range */
			FindInformationRequest = 0x04,
					
			/** One or more attribute handles and types were reported in the given handle range */
			FindInformationResponse = 0x05,
			
			/** Requests to get the ranges of attribute handles for all attributes that have the given type UUID and (if specified) value */
			FindByTypeRequest =	0x06,
			
			/** One or more attributes matched the given type and value (if specified) and the range of each discovered declaration is included */
			FindByTypeResponse =	0x07,
			
			/** Requests to get the value of an attribute given its type UUID */
			ReadByTypeRequest =	0x08,
			
			/** One or more values matched the given type and the attribute handle and value are included in the response */
			ReadByTypeResponse = 	0x09,
			
			/** Requests to get the value of an attribute given its handle */
			ReadRequest =		0x0A,
			
			/** The attribute had data and the first portion of the data is included in the response */
			ReadResponse =		0x0B,
					
			/** Requests to get portions of large attribute given its handle and byte offset */
			ReadBlobRequest =	0x0C,
					
			/** The attribute had data at the requested offset and the next portion of the data is included in the response */
			ReadBlobResponse =	0x0D,
			
			/** Requests to batch read values of the same size from multiple attributes given as a handle list */
			ReadMultipleRequest =	0x0E,
			
			/** One or more attribute values were read in a batch and sequences of fixed length handles and values are included */
			ReadMultipleResponse =	0x0F,
					
			/** Gets the value (type UUID) and end handle of a service group given its handle range and overall service category (primary, secondary, etc.) */
			ReadByGroupRequest = 	0x10,

			/** One or more groups existed in the requested handle range, the handle and value (type UUID) of each service group is included in response */
			ReadByGroupResponse =	0x11,
			
			/** Writes the value of an attribute given its handle, expecting a response */
			WriteRequest = 		0x12,
					
			/** The write succeeded (no data is included in the response, this is purely a confirmation) */
			WriteResponse =		0x13,

			// Gap - 0x14 and 0x15
			
			/** Prepares a write for a portion of an attribute value into a larger write transaction */
			PrepareWriteRequest =	0x16,
			
			/** The requested prepared write was included in the current write transaction */
			PrepareWriteResponse =	0x17,
			
			/** Executes the prepared write transaction */
			ExecuteWriteRequest =	0x18,
			
			/** The current write transation was successfully executed */
			ExecuteWriteResponse =	0x19,
			
			// Gap - 0x1A

			/** The server sent a notification of new attribute value data to the client */
			Notify =		0x1B,
			
			// Gap - 0x1C

			/** Server indicates that new data is available for an attribute handle, requesting client to confirm */
			Indicate =		0x1D,
			
			/** Confirmation sent form client to server that it received the updated indicated data */
			Confirm = 		0x1E,
			
			// Gap from 0x1F to 0x51

			/** The client is writing attribute value to the server without confirmation */
			Write =		0x52,
			
			/** The client is writing cryptographically signed attribute value to the server without confirmation */
			SignedWrite =	0xD2	
		};
		
		/**
		 * Gets a human readable text representation of an ATT opcode.
		 * 
		 * @param code The ATT opcode
		 * @return the text representation, or "Unknown" if the code is not a valid value
		 */
		CIO_BLUETOOTH_API const char *print(ATT code) noexcept;

		/**
		 * Prints a human readable text representation of an ATT opcode to a C++ stream.
		 *
		 * @param s The stream
		 * @param code The ATT opcode
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &s, ATT code);
	}
}

#endif

