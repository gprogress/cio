/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "TemporaryFileTest.h"

#include <cio/test/Assert.h>

#include <cio/File.h>
#include <cio/TemporaryFile.h>

#include <cio/Filesystem.h>
#include <cio/JsonWriter.h>
#include <cio/MarkupWriter.h>
#include <cio/Program.h>

#include <cio/Path.h>
#include <cio/Text.h>

#include <iostream>

namespace cio
{
	TemporaryFileTest::TemporaryFileTest() :
		cio::test::Suite("cio::TemporaryFile")
	{
		CIO_TEST_METHOD(TemporaryFileTest, createTempFile);
	}

	TemporaryFileTest::~TemporaryFileTest() noexcept = default;

	void TemporaryFileTest::createTempFileTest()
	{
		// Get a writable location
		cio::Filesystem fs;
		cio::Path destination = fs.getTemporaryDirectory() / ("TemporaryFileTest-" + std::to_string(Program::getApplicationId()));
		fs.createDirectory(destination);

		try
		{
			// classic hello world test
			{
				const cio::Path subdir = "subdir";
				const cio::Text ext = "txt";
				cio::TemporaryFile *file = new TemporaryFile(subdir, ext);
				cio::File tempFile = file->open();

				tempFile.putText("hello world");
				tempFile.clear();
				const cio::Path savePath = destination / "hello";
				file->copyTo(savePath);
				file->clear();
			}

			// hello pt2 (directory "another" did not exist at the time of running test)
			{
				const cio::Path subdir = "subdir";
				const cio::Text ext = "txt";
				cio::TemporaryFile *file = new TemporaryFile(subdir, ext);
				cio::File tempFile = file->open();

				tempFile.putText("hello world part 2");
				tempFile.clear();
				const cio::Path savePath = destination / "another" / "hello2";
				file->copyTo(savePath);
				file->clear();
			}

			// temp no ext, copy path with ext
			{
				const cio::Path subdir = "subdir";
				const cio::Text ext = "";
				cio::TemporaryFile *file = new TemporaryFile(subdir, ext);
				cio::File tempFile = file->open();

				tempFile.putText("hello world part 3");
				tempFile.clear();
				const cio::Path savePath = destination / "hello3.txt";
					file->copyTo(savePath);
				file->clear();
			}

			// set temp path, default constructor
			{
				cio::TemporaryFile *file = new TemporaryFile();
				file->setTempFilePath(destination / "delete.txt");
				cio::File tempFile = file->open();

				tempFile.putText("hello world part 4");
				tempFile.clear();
				const cio::Path savePath = destination / "hello4.txt";
				file->copyTo(savePath);
				file->clear();
			}

			// set temp path in temp dir, default constructor
			{
				cio::TemporaryFile *file = new TemporaryFile();
				Filesystem fs;
				cio::Path path = destination / UniqueId::random().print();
				path.setExtension("ext");
				file->setTempFilePath(path);
				file->createTempFile();
				cio::File tempFile = file->open();

				tempFile.putText("hello world part 5");
				tempFile.clear();
				const cio::Path savePath = destination / "hello5.txt";
				file->copyTo(savePath);
				file->clear();
			}

			// existing directory
			{
				const cio::Path subdir = "subdir";
				const cio::Text ext = "txt";
				cio::TemporaryFile *file = new TemporaryFile(subdir, ext);
				cio::File tempFile = file->open();

				tempFile.putText("hello world");
				tempFile.clear();
				const cio::Path savePath = destination;
				file->copyTo(savePath);
				file->clear();
			}

			// json writer test
			{
				const cio::Path subdir = "subdir/subdir1/subdir2/subdir3/subdir4";
				const cio::Text ext = "json";
				cio::TemporaryFile *file = new TemporaryFile(subdir, ext);
				cio::File tempFile = file->open();
				cio::JsonWriter writer;

				writer.setOutput(&tempFile);
				writer.startDocument();
				writer.writeSimpleElement("simple", "element");
				writer.startElement("element");
				writer.writeSimpleArray("write simple", "array");
				writer.endElement();
				writer.endDocument();
				writer.clear();
				tempFile.clear();
				const cio::Path savePath = destination / "testjson";

				file->copyTo(savePath);
				file->clear();
			}

			fs.erase(destination, Traversal::Recursive);
		}
		catch (...)
		{
			fs.erase(destination, Traversal::Recursive);
			throw;
		}
	}
}