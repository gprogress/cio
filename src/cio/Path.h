/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PATH_H
#define CIO_PATH_H

#include "Types.h"

#include <iosfwd>
#include <string>

#include <cio/Text.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Path class provides a representation of a URI for any protocol.
	 */
	class CIO_API Path
	{
		public:			
			/**
			 * Gets the metaclass for this clas.
			 *
			 * @return the metaclass
			 */
			static const Class<Path> &getDeclaredMetaclass() noexcept;
		
			/**
			 * Gets whether the native file path has a native root.
			 * On Microsoft Windows platforms, this is equivalent to calling hasWindowsRoot.
			 * On other platforms, this is equivalent to calling hasUnixRoot.
			 *
			 * @param path The native file path string, which may or may not be null-terminated
			 * @param length The length of the native file path string
			 * @return how many characters of the path start are the native root directory for the current platform
			 */
			static std::size_t hasNativeRoot(const char *path, std::size_t length) noexcept;

			/**
			* Gets whether the Microsoft Windows file path has a root directory.
			* This is true if the path starts with a drive letter pattern such as "C:\" or with a leading
			* separator '/' or '\' optionally followed by a drive letter pattern.
			* 
			* This method is a more permissive superset of hasUnixRoot and will also match Unix roots.
			*
			* @param path The Windows file path string, which may or may not be null-terminated
			* @param length The length of the Windows file path string
			* @return how many characters of the path starts with a Windows root directory
			*/
			static std::size_t hasWindowsRoot(const char *path, std::size_t length) noexcept;

			/**
			* Gets whether the Unix file path has a root directory.
			* This is true if the path starts with the directory separator '/'
			*
			* @param path The Unix file path string, which may or may not be null-terminated
			* @param length The length of the Unix file path string
			* @return whether the string starts with a Unix root directory
			*/
			static bool hasUnixRoot(const char *path, std::size_t length) noexcept;

			/**
			 * Constructs a URI path from a native file path text formatted for the host platform.
			 *
			 * @param path The path text
			 * @param len The number of bytes to consider in the text
			 * @return the URI representing the native path
			 */
			static Path fromNativeFile(const char *path, std::size_t len);

			/**
			 * Constructs a URI path from a native file path null-terminated text formatted for the host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromNativeFile(const char *path);

			/**
			 * Constructs a URI path from a native file path text formatted for the host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromNativeFile(const std::string &path);

			/**
			 * Constructs a URI path from a Windows-style file path text, regardless of host platform.
			 *
			 * @param path The path text
			 * @param len The number of bytes to consider in the text
			 * @return the URI representing the native path
			 */
			static Path fromWindowsFile(const char *path, std::size_t len);

			/**
			 * Constructs a URI path from a Windows-style file path null-terminated text, regardless of host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromWindowsFile(const char *path);

			/**
			 * Constructs a URI path from a Windows-style file path text, regardless of host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromWindowsFile(const std::string &path);

			/**
			 * Constructs a URI path from a Unix-style file path text, regardless of host platform.
			 *
			 * @param path The path text
			 * @param len The number of bytes to consider in the text
			 * @return the URI representing the native path
			 */
			static Path fromUnixFile(const char *path, std::size_t len);

			/**
			 * Constructs a URI path from a Unix-style file path null-terminated text, regardless of host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromUnixFile(const char *path);

			/**
			 * Constructs a URI path from a Unix-style file path text, regardless of host platform.
			 *
			 * @param path The path text
			 * @return the URI representing the native path
			 */
			static Path fromUnixFile(const std::string &path);

			/**
			 * Gets the primary native directory separator for the platform.
			 * This is '\\' on Windows and '/' on every other platform.
			 * 
			 * @return the native separator
			 */
			static char getNativeSeparator() noexcept;

			/**
			 * Checks whether the given character is a directory separator on the host operating system.
			 * 
			 * @param c The character
			 * @return whether it is a native directory separator on the current platform
			 */
			static bool isNativeSeparator(char c) noexcept;

			/**
			 * Checks whether the given character is a directory separator on Windows.
			 * This is true for both '/' and '\\'
			 *
			 * @param c The character
			 * @return whether it is a Windows directory separator
			 */
			inline static bool isWindowsSeparator(char c) noexcept;

			/**
			 * Checks whether the given character is a directory separator on Unix.
			 * This is only true for '/'.
			 *
			 * @param c The character
			 * @return whether it is a Unix directory separator
			 */
			inline static bool isUnixSeparator(char c) noexcept;
			
			/**
			 * Validates the given unsanitized input, determining the path style
			 * and transforming as needed to a conventional path.
			 * 
			 * The following normalizations are applied:
			 * <ul>
			 * <li>Windows separators are normalized to '/'</li>
			 * </ul>
			 * 
			 * If the text is unmodified, it is moved straight through to output.
			 * 
			 * @param text The text input
			 * @return the validated path
			 */
			static std::string validateNativeInputText(std::string text);

			/**
			 * Construct the empty URI path.
			 */
			Path() noexcept;

			/**
			 * Construct a URI path from the given null-terminated text.
			 *
			 * @note If this is a file path you should use fromNativeFile, fromWindowsFile, or fromUnixFile instead
			 *
			 * @param path The text
			 */
			Path(const char *path);

			/**
			 * Construct a URI path from the given text.
			 *
			 * @note If this is a file path you should use fromNativeFile, fromWindowsFile, or fromUnixFile instead
			 *
			 * @param path The text
			 * @param length The number of characters in the text
			 */
			Path(const Text &path);

			/**
			 * Construct a URI path from the given text.
			 *
			 * @note If this is a file path you should use fromNativeFile, fromWindowsFile, or fromUnixFile instead
			 *
			 * @param path The text
			 */
			Path(const std::string &path);

			/**
			 * Construct a URI path from the given text.
			 *
			 * @note If this is a file path you should use fromNativeFile, fromWindowsFile, or fromUnixFile instead
			 *
			 * @param path The text
			 */
			Path(std::string &&path) noexcept;

			/**
			 * Construct a deep copy of a URI path.
			 *
			 * @param in The path to copy
			 */
			Path(const Path &in);

			/**
			 * Construct a path by providing a sequence of arguments to form a directory hierarchy.
			 * 
			 * @tparam T The type of the initial base directory
			 * @param U The type of the first child
			 * @tparam A... The remaining child argument types
			 * @param base The initial base directory
			 * @param first The first child
			 * @param rest The remaining children
			 */
			template <typename T, typename U, typename... A>
			explicit Path(T &&base, U &&first, A &&...rest);

			/**
			 * Construct a path by transferring the contents of an existing path.
			 *
			 * @param in The path to move
			 */
			Path(Path &&in) noexcept;

			/**
			 * Copies a path.
			 *
			 * @param in The path to copy
			 * @return this path
			 */
			Path &operator=(const Path &in);

			/**
			 * Moves a path.
			 *
			 * @param in The path to move
			 * @return this path
			 */
			Path &operator=(Path &&in) noexcept;

			/**
			 * Destructor.
			 */
			~Path() noexcept;

			/**
			 * Clears the path to set the empty path.
			 */
			void clear() noexcept;

			/**
			 * Gets the text underlying the path URI.
			 *
			 * @return the underlying text
			 */
			inline const std::string &get() const noexcept;
			
			/**
			 * Gets the text underlying the path URI.
			 *
			 * @return the underlying text
			 */
			inline const std::string &str() const noexcept;
			
			/**
			 * Gets the number of bytes in the underlying path text.
			 *
			 * @return the number of bytes in the path text
			 */
			inline std::size_t size() const noexcept;

			/**
			 * Gets whether the path is empty.
			 *
			 * @return whether the path is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Interprets the path in a Boolean context.
			 * It is considered true if it is not empty.
			 *
			 * @return whether the path is not empty
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Gets the text character at the i'th byte in the path text.
			 *
			 * @param i The character byte offset
			 * @return the character in the path text at that offset
			 */
			inline char operator[](std::size_t i) const noexcept;

			/**
			 * Gets the underlying path text as a null-terminated C string.
			 *
			 * @return the underlying path text
			 */
			inline const char *c_str() const noexcept;
			
			/**
			 * Gets the underlying path text as a null-terminated C string.
			 *
			 * @return the underlying path text
			 */
			inline const char *data() const noexcept;

			/**
			 * Sets the underlying path text.
			 *
			 * @param path The path text
			 */
			void set(const std::string &path);

			/**
			 * Sets the underlying path text.
			 *
			 * @param path The path text
			 */
			void set(std::string &&path) noexcept;

			/**
			 * Sets the underlying path text.
			 *
			 * @param path The path text
			 */
			void set(const char *path);

			/**
			 * Sets the underlying path text.
			 *
			 * @param path The path text
			 */
			void set(const Text &path);

			/**
			 * Sets this path from a given parent path and a child entry.
			 * The child is treated as a single child path element and a separator '/' is inserted
			 * if not present at the end of the parent path.
			 * 
			 * If the parent has a query fragment, it is discarded since it will not apply to the child.
			 *
			 * @param parent The parent path
			 * @param child The child entry as a null-terminated C string
			 * @throw std::bad_alloc If the memory for the path text could not be allocated
			 */
			void set(const Path &parent, const Text &child);

			/**
			 * Sets this path from a given parent path and a child path.
			 * If the child is already complete, then the path is constructed to be a copy of the child.
			 * Otherwise, the child is completed using the parent path and a separator '/' is inserted
			 * if not present at the end of the parent path.
			 * 
			 * If the parent has a query fragment, it is discarded since it will not apply to the child.
			 *
			 * @param parent The parent path
			 * @param child The child path
			 * @throw std::bad_alloc If the memory for the path text could not be allocated
			 */
			void set(const Path &parent, const Path &child);

			/**
			 * Gets whether the path has a protocol prefix such as "http://".
			 *
			 * @return whether the path has a protocol prefix
			 */
			bool hasProtocol() const noexcept;

			/**
			 * Gets the prefix of the URI that represents the protocol prefix such as "http://".
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to the protocol portion of the path URI
			 */
			Text getProtocol() const noexcept;

			/**
			 * Gets the portion of the URI that represents the root portion of the path such as the hostname or the filesystem root.
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 * 
			 * As a special case, if the URI is the file:// protocol or has no protocol but starts with a native platform root,
			 * the full native platform root (such as Windows drive letter) will be returned.
			 *
			 * @return a reference to the root portion of the path URI
			 */
			Text getRoot() const noexcept;
					
			/**
			 * Sets the portion of the URI that represents the root portion of the path such as the hostname or the filesystem root.
			 *
			 * @param root The root to set
			 */
			void setRoot(const Text &root);

			/**
			 * Gets the portion of the URI that represents resource location part of the path relative to the host.
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to the path portion of the path URI
			 */
			Text getPath() const noexcept;
			
			/**
			 * Updates the portion of the URI that resource location relative to the host. 
			 *
			 * @param path the path to set
			 */
			void setPath(const Text &path);
			
			/**
			 * Copies the given path if the current path is empty.
			 *
			 * @param path The path to merge
			 * @return a reference to this path for convenience
			 */
			Path &merge(const Path &path);
			
			/**
			 * Moves the given path to the current path if the current path is empty.
			 *
			 * @param path The path to merge
			 * @return a reference to this path for convenience
			 */
			Path &merge(Path &&path);

			/**
			 * Gets whether the URI has a filename with a final extension suffix of the filename such as ".png".
			 *
			 * @return whether this path has a file extension
			 */
			bool hasExtension() const noexcept;

			/**
			 * Gets the portion of the URI that represents the final extension suffix of the filename such as ".png".
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to the path portion of the path URI
			 */
			Text getExtension() const noexcept;

			/**
			 * Gets the portion of the URI that represents the query fragment starting with '?' after the full file path.
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to query fragment of the path URI
			 */
			Text getQuery() const noexcept;

			/**
			 * Gets the prefix of the URI that represents the parent resource of the given resource URI.
			 * This includes the protocol, root, and path portion up to the parent directory of the filename.
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to parent portion of the URI
			 */
			Text getParent() const noexcept;

			/**
			 * Gets the portion of the URI that represents the leaf filename of the resource such as "image.png".
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to the leaf filename of the URI
			 */
			Text getFilename() const noexcept;
			
			/**
			 * Gets the portion of the URI that represents the leaf filename of the resource such as "image.png".
			 *
			 * @param filename The filename to set
			 */
			void setFilename(const Text &filename);

			/**
			 * Gets the portion of the URI that represents the leaf filename of the resource, but without any extension suffixes.
			 * This is a reference to the path text and cannot be used after this path is cleared or modified.
			 *
			 * @return a reference to the leaf filename without extension of the URI
			 */
			Text getFilenameNoExtension() const noexcept;
			
			/**
			 * Updates the protocol of the resource URI.
			 * This invalidates all references to the URI.
			 *
			 * @param protocol The new protocol to use
			 */
			void setProtocol(const Text &protocol);

			/**
			 * Checks whether this path is compatible with the given protocol.
			 * It is true if this path does not have a protocol set (and thus is a relative path) or if the protocols are equal.
			 *
			 * @param protocol The protocol to check
			 * @return whether this path matches the given protocol
			 */
			bool matchesProtocol(const Text &protocol) const noexcept;

			/**
			 * Checks whether this path is compatible with the "file://" protocol as a common special case.
			 *
			 * @return whether this path matches the "file" protocol
			 */
			bool isFileProtocol() const noexcept;
			
			/**
			 * Checks whether the filename on this path has the given extension.
			 * If the provided extension is empty then the current path must not have an extension.
			 * This method will also handle a single wildcard '*' in the input extension to consider all
			 * characters after that point to match
			 *
			 * @param ext The extension to consider
			 * @return whether this path is compatible with the extension
			 */
			bool matchesExtension(const char *ext) const noexcept;
			
			/**
			 * Checks whether the filename on this path has the given extension.
			 * If the provided extension is empty then the current path must not have an extension.
			 * This method will also handle a single wildcard '*' in the input extension to consider all
			 * characters after that point to match.
			 *
			 * @param ext The extension to consider
			 * @return whether this path is compatible with the extension
			 */
			bool matchesExtension(const std::string &ext) const noexcept;
			
			/**
			 * Checks whether the filename on this path has the given extension.
			 * If the provided extension is empty then the current path must not have an extension.
			 * This method will also handle a single wildcard '*' in the input extension to consider all
			 * characters after that point to match
			 *
			 * @param ext The extension to consider
			 * @return whether this path is compatible with the extension
			 */
			bool matchesExtension(const Text &ext) const noexcept;

			/**
			 * Sets the last extension on the path's filename.
			 * This invalidates all references to the path.
			 *
			 * @param ext The extension to set
			 */
			void setExtension(const Text &ext);

			/**
			 * Removes the last extension on the path's filename.
			 * This invalidates all references to the path.
			 */
			void removeExtension();

			/**
			 * Checks to see if this path is considered a relative path.
			 * This is true if it does not have a root.
			 *
			 * @return whether this is a relative path
			 */
			bool isRelative() const noexcept;

			/**
			 * Checks to see if this path is considered a complete path.
			 * This is true if it does have a root.
			 *
			 * @return whether this is a complete path
			 */
			bool isComplete() const noexcept;

			/**
			 * Checks to see if this path is actually a root path itself.
			 * This is true if the full path is equal to the root path.
			 *
			 * @return whether this is a root path
			 */
			bool isRoot() const noexcept;

			/**
			 * Updates this path to remove any unnecessary path elements.
			 * This will strip all '.' parent directories, and will also strip
			 * any '../' parent directories that can be matched to a removable parent.
			 * 
			 * @return Whether this method changed the path
			 */
			bool normalize();

			/**
			 * Gets a normalized copy of this path that removes any unnecessary path elements.
			 * This will strip all '.' parent directories, and will also strip
			 * any '../' parent directories that can be matched to a removable parent.
			 *
			 * @return The normalized path
			 */
			cio::Path makeNormalized() const;

			/**
			 * Updates this path to be a complete path using itself as the relative path and the given base path.
			 * If this path is already a complete path, nothing happens.
			 * Otherwise this path is updated in place and all references to the path are invalidated.
			 *
			 * @param base The base path to use as the completion parent
			 * @return this path after the operation
			 */
			Path &complete(const Path &base);

			/**
			 * Computes a complete path using this path as a relative path and the given base path.
			 * If this path is already a complete path, it is returned unmodified.
			 *
			 * @param base The base path to use as the completion parent
			 * @return the completed path, or this path if it is already complete
			 */
			Path makeCompletePath(const Path &base) const;

			/**
			 * Computes a relative path that represents a traversal to get from the current path to the target path.
			 * If this path and the target path are not reachable from each other, then the target path is returned unmodified.
			 *
			 * @param target The target path
			 * @return the relative path from this path to the target path, or the target path if no such relative path exists
			 */
			Path makeRelativePath(const Path &target) const;

			/**
			 * Computes a path with the same protocol, directories, and name but replacing the extension (if present) with the given extension.
			 * This can be used to determine a sibling file path in the same directory that has a desired extension.
			 *
			 * @param ext The desired extension
			 * @return the new path with the desired extension
			 */
			Path makePathWithExtension(const Text &ext) const;

			/**
			 * Appends a child path in-place to this parent path.
			 * All references to the current path are invalidated.
			 *
			 * @param child The child to append
			 * @return a reference to this path
			 */
			Path &addChild(const Path &child);

			/**
			 * Checks whether this path contains the given path element at any hierarchy, ignoring the protocol and query fragments.
			 * This will not consider partial matches within each path element.
			 * 
			 * @param element The path element to find
			 * @return whether the path element was found
			 */
			bool containsPath(const Path &element) const noexcept;

			/**
			 * Checks whether this path contains the given path element at any hierarchy using the given case match rule,
			 * ignoring the protocol and query fragments.
			 * 
			 * This will not consider partial matches within each path element.
			 *
			 * @param element The path element to find
			 * @param match The case match rule
			 * @return whether the path element was found
			 */
			bool containsPath(const Path &element, Case match) const noexcept;

			/**
			 * Appends a sequence of arguments as children to form a directory hierarchy.
			 *
			 * @tparam T The type of the first child
			 * @tparam A... The argument types
			 * @param base The initial base directory
			 * @param args The arguments
			 */
			template <typename T, typename... A>
			inline void append(T &&first, A &&...rest);

			/**
			 * Finishes appending the sequence of arguments as children.
			 */
			constexpr void append() const noexcept;

			/**
			 * Converts the URI to a native filename most appropriate for the host operating system.
			 *
			 * @return the native file path
			 * @throw std::bad_alloc If the memory for the output text could not be allocated
			 */
			Text toNativeFile() const;

			/**
			 * Converts the URI to a native filename most appropriate for the host operating system.
			 * If the buffer is smaller than the filename, the filename is truncated and the actual needed size is returned.
			 * If the buffer is larger than the filename, it will be null-terminated.
			 *
			 * @param text The text buffer to copy into
			 * @param length The maximum number of bytes in the text buffer to use
			 * @return the actual size needed for the native file path
			 */
			std::size_t toNativeFile(char *text, std::size_t length) const noexcept;

			/**
			 * Converts the URI to a Windows-style filename.
			 * Drive letters are properly converted (if set) and all directory separators are converted to '\'.
			 *
			 * @return the Windows file path
			 * @throw std::bad_alloc If the memory for the output text could not be allocated
			 */
			Text toWindowsFile() const;

			/**
			 * Converts the URI to a Windows-style filename.
			 * Drive letters are properly converted (if set) and all directory separators are converted to '\'.
			 * If the buffer is smaller than the filename, the filename is truncated and the actual needed size is returned.
			 * If the buffer is larger than the filename, it will be null-terminated.
			 *
			 * @param text The text buffer to copy into
			 * @param length The maximum number of bytes in the text buffer to use
			 * @return the actual size needed for the Unix file path
			 */
			std::size_t toWindowsFile(char *text, std::size_t length) const noexcept;

			/**
			 * Converts the URI to a Unix-style filename.
			 *
			 * @return the Unix file path
			 * @throw std::bad_alloc If the memory for the output text could not be allocated
			 */
			Text toUnixFile() const;

			/**
			 * Converts the URI to a Unix-style filename storing the text in the provided buffer.
			 * If the buffer is smaller than the filename, the filename is truncated and the actual needed size is returned.
			 * If the buffer is larger than the filename, it will be null-terminated.
			 *
			 * @param text The text buffer to copy into
			 * @param length The maximum number of bytes in the text buffer to use
			 * @return the actual size needed for the Unix file path
			 */
			std::size_t toUnixFile(char *text, std::size_t length) const noexcept;

			/**
			 * Validates the current path text as unformatted native input and corrects for any platform-specific quirks.
			 */
			void validateNativeInput();

			/**
			 * Gets the print length of the path in UTF-8 characters.
			 *
			 * @return the print length
			 */
			std::size_t strlen() const noexcept;

			/**
			 * Prints the path to the given UTF-8 text buffer.
			 *
			 * @param buffer The text buffer
			 * @param capacity The capacity of the buffer
			 * @return the actual number of characters needed
			 */
			std::size_t print(char *buffer, std::size_t capacity) const noexcept;

			/**
			 * Prints the path to a returned UTF-8 text string.
			 *
			 * @return the path
			 */
			const std::string &print() const noexcept;

			/**
			 * Replaces characters that are not legal in encoded string
			 * with their ASCII hexadecimal value, in the format %NN.
			 *
			 * @param text The text to be encoded
			 * @return True if successful, false otherwise
			 */
			static bool encode(std::string &text);

			/**
			 * Reverses Path::encode, if possible. Hexadecimal value, in the format %NN,
			 * are replaced with the ascii characters of the same value.
			 * If the input is not a valid encoding, it will not be modified.
			 *
			 * @param text The text to be decoded
			 * @return True if successful, false otherwise
			 */
			static bool decode(std::string &text);

		private:
			/** The metaclass for this class */
			static Class<Path> sMetaclass;
					
			/** The underlying text for the path */
			std::string mText;
	};

	/**
	 * Appends a child path to a parent path using the '/' directory separator to join them.
	 *
	 * @param left The parent path
	 * @param right The child path
	 * @return the appended path
	 */
	CIO_API Path operator/(const Path &left, const Path &right);

	/**
	 * Appends a child path to a parent path in-place using the '/' directory separator to join them.
	 *
	 * @param left The parent path
	 * @param right The child path
	 * @return the parent path after being appended
	 */
	CIO_API Path &operator/=(Path &left, const Path &right);

	/**
	 * Prints a path URI to a C++ stream.
	 *
	 * @param stream The stream
	 * @param path The path to print
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Path &path);

	/**
	 * Checks to see if two Path URIs are equal.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the two paths are equal
	 */
	inline bool operator==(const Path &left, const Path &right) noexcept;

	/**
	 * Checks to see if two Path URIs are not equal.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the two paths are not equal
	 */
	inline bool operator!=(const Path &left, const Path &right) noexcept;

	/**
	 * Checks to see if one path is less than another path.
	 * The order is determined by text comparison.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the first path is less than the second path
	 */
	inline bool operator<(const Path &left, const Path &right) noexcept;

	/**
	 * Checks to see if one path is less than or equal to another path.
	 * The order is determined by text comparison.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the first path is less than or equal to the second path
	 */
	inline bool operator<=(const Path &left, const Path &right) noexcept;

	/**
	 * Checks to see if one path is greater than another path.
	 * The order is determined by text comparison.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the first path is greater than the second path
	 */
	inline bool operator>(const Path &left, const Path &right) noexcept;

	/**
	 * Checks to see if one path is greater than or equal to another path.
	 * The order is determined by text comparison.
	 *
	 * @param left The first path
	 * @param right The second path
	 * @return whether the first path is greater than or equal to the second path
	 */
	inline bool operator>=(const Path &left, const Path &right) noexcept;
}

namespace cio
{
	inline bool Path::isWindowsSeparator(char c) noexcept
	{
		return c == '\\' || c == '/';
	}

	inline bool Path::isUnixSeparator(char c) noexcept
	{
		return c == '/';
	}

	template <typename T, typename U, typename... A>
	inline Path::Path(T &&base, U &&first, A &&...rest) :
		Path(std::forward<T>(base))
	{
		this->append(std::forward<U>(first), std::forward<A>(rest)...);
	}

	template <typename T, typename... A>
	inline void Path::append(T &&first, A &&...rest)
	{
		this->addChild(std::forward<T>(first));
		this->append(std::forward<A>(rest)...);
	}

	constexpr void Path::append() const noexcept
	{
		// nothing to do in base case
	}

	inline const std::string &Path::get() const noexcept
	{
		return mText;
	}
	
	inline const std::string &Path::str() const noexcept
	{
		return mText;
	}
	
	inline std::size_t Path::size() const noexcept
	{
		return mText.size();
	}

	inline bool Path::empty() const noexcept
	{
		return mText.empty();
	}

	inline Path::operator bool() const noexcept
	{
		return !mText.empty();
	}

	inline char Path::operator[](std::size_t i) const noexcept
	{
		return mText[i];
	}

	inline const char *Path::c_str() const noexcept
	{
		return mText.c_str();
	}
	
	inline const char *Path::data() const noexcept
	{
		return mText.data();
	}
	
	inline bool operator==(const Path &left, const Path &right) noexcept
	{
		return left.get() == right.get();
	}

	inline bool operator!=(const Path &left, const Path &right) noexcept
	{
		return left.get() != right.get();
	}

	inline bool operator<(const Path &left, const Path &right) noexcept
	{
		return left.get() < right.get();
	}

	inline bool operator<=(const Path &left, const Path &right) noexcept
	{
		return left.get() <= right.get();
	}

	inline bool operator>(const Path &left, const Path &right) noexcept
	{
		return left.get() > right.get();
	}

	inline bool operator>=(const Path &left, const Path &right) noexcept
	{
		return left.get() >= right.get();
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
