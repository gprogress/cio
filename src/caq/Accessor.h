/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CAQ_ACCESSOR_H
#define CAQ_ACCESSOR_H

#include "Types.h"

#include "Reader.h"

namespace caq
{
	/**
	 * The Accessor class provides a type-erasure wrapper to compose any actual Reader instance into a lightweight chunk of memory
	 * that can be easily passed around and used to work with array data.
	 * 
	 * @warning This class can only accommodate implementations where the class structure is at most 128 bytes. Subclasses must 
	 * use dynamic allocation if more data than that is necessary.
	 * 
	 * @tparam T The data component type
	 */
	template <typename T>
	class Accessor final : public Reader<T>
	{
		public:
			/**
			 * Default constructor.
			 */
			inline Accessor() = default;

			/**
			 * Construct the implementing accessor by copying the given accessor.
			 *
			 * @tparam U The type to copy
			 * @param impl The implementation to copy
			 */
			template <typename U>
			inline Accessor(const U &impl)
			{
				static_assert(sizeof(U) <= sizeof(mMemory), "Type cannot be embedded into Accessor<T>");
				mImplementation = new (mMemory) U(impl);
			}

			/**
			 * Construct the implementing accessor by moving the given accessor.
			 *
			 * @tparam U The type to move
			 * @param impl The implementation to move
			 */
			template <typename U>
			inline Accessor(U &&impl) noexcept
			{
				static_assert(sizeof(U) <= sizeof(mMemory), "Type cannot be embedded into Accessor<T>");
				mImplementation = new (mMemory) U(std::move(impl));
			}

			/**
			 * Copy constructor.
			 *
			 * @param in The accessor to copy
			 */
			inline Accessor(const Accessor<T> &in)
			{
				if (in.mImplementation)
				{
					std::memcpy(mMemory, in.mMemory, sizeof(mMemory));
					mImplementation = reinterpret_cast<Reader<T> *>(mMemory);
				}
			}

			/**
			 * Move constructor.
			 *
			 * @param in The accessor to move
			 */
			inline Accessor(Accessor<T> &&in) noexcept
			{
				if (in.mImplementation)
				{
					std::memcpy(mMemory, in.mMemory, sizeof(mMemory));
					mImplementation = reinterpret_cast<Reader<T> *>(mMemory);
				}
			}

			/**
			 * Copy assignment.
			 *
			 * @param in The accessor to copy
			 * @return This accessor
			 */			
			inline Accessor<T> &operator=(const Accessor<T> &in)
			{
				if (in.mImplementation)
				{
					std::memcpy(mMemory, in.mMemory, sizeof(mMemory));
					mImplementation = reinterpret_cast<Reader<T> *>(mMemory);				
				}
				return *this;
			} 

			/**
			 * Move assignment.
			 *
			 * @param in The accessor to copy
			 * @return This accessor
			 */
			inline Accessor<T> &operator=(Accessor<T> &&in) noexcept
			{
				if (in.mImplementation)
				{
					std::memcpy(mMemory, in.mMemory, sizeof(mMemory));
					mImplementation = reinterpret_cast<Reader<T> *>(mMemory);
				}
				return *this;
			}

			/**
			 * Destructor. Calls underlying implementation destructor if needed.
			 */
			inline virtual ~Accessor() noexcept override = default;
			
			// Override Abstract methods

			/**
			 * Gets the number of array elements associated with this accessor.
			 *
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return mImplementation->size();
			}

			/**
			 * Gets the number of components per array element associated with this accessor.
			 *
			 * @return The number of components per array element
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return mImplementation->components();
			}

			/**
			 * Interprets whether this accessor is usable.
			 * This is true if the implementation is set and reports that it is usable.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mImplementation && *mImplementation;
			}

			// Override Reader<T> methods

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The generic Accessor will delegate to the implementation if set.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				return mImplementation ? mImplementation->optimize() : Accessor<T>();
			}

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const override
			{
				return mImplementation->get(n, c);
			}

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 *
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const override
			{
				return mImplementation->retrieve(buffer, n);
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 *
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const override
			{
				return mImplementation->retrieve(buffer, noff, count);
			}

			// Methods unique to Accessor<T>

			/**
			 * Constructs the implementing accessor with the given constructor arguments.
			 * The existing implementation, if any, is destroyed.
			 * 
			 * @tparam U The type to construct
			 * @tparam A... The arguments to the constructor for U
			 * @return The pointer to the constructed implementation
			 */
			template <typename U, typename... A>
			inline U *construct(A &&...args)
			{
				static_assert(sizeof(U) <= sizeof(mMemory), "Type cannot be embedded into Accessor<T>");

				if (mImplementation)
				{
					mImplementation->~Reader<T>();
				}
				U *constructed = new (mMemory) U(std::forward<A>(args)...);
				mImplementation = constructed;
				return constructed;
			}

			/**
			 * Construct the implementing accessor by copying the given accessor.
			 * The existing implementation, if any, is destroyed.
			 * 
			 * @tparam U The type to copy
			 * @param impl The implementation to copy
			 * @return The pointer to the constructed implementation
			 */
			template <typename U>
			inline U *copy(const U &impl)
			{
				static_assert(sizeof(U) <= sizeof(mMemory), "Type cannot be embedded into Accessor<T>");
				U *constructed;

				if (mImplementation != &impl)
				{
					if (mImplementation)
					{
						mImplementation->~Reader<T>();
					}
					constructed = new (mMemory) U(impl);
					mImplementation = constructed;
				}
				else
				{
					constructed = const_cast<U *>(&impl);
				}
				return constructed;
			}

			/**
			 * Construct the implementing accessor by moving the given accessor.
			 * The existing implementation, if any, is destroyed.
			 *
			 * @tparam U The type to move
			 * @param impl The implementation to move
			 * @return The pointer to the constructed implementation
			 */
			template <typename U>
			inline U *move(U &&impl) noexcept
			{
				static_assert(sizeof(U) <= sizeof(mMemory), "Type cannot be embedded into Accessor<T>");
				U *constructed;
				if (mImplementation != &impl)
				{
					if (mImplementation)
					{
						mImplementation->~Reader<T>();
					}
					constructed = new (mMemory) U(std::move(impl));
					mImplementation = constructed;
				}
				else
				{
					constructed = const_cast<U *>(&impl);
				}
				return constructed;
			}

			/**
			 * Gets the underlying implementation.
			 * 
			 * @return The implementation
			 */
			inline Reader<T> *implementation() noexcept
			{
				return mImplementation;
			}

		private:
			/** The implementation for the accessor pointing into the memory */
			Reader<T> *mImplementation = nullptr;

			/** The memory containing the embedded accessor */
			unsigned char mMemory[256u - sizeof(void *)];
	};
}

#endif
