/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "MemoryInput.h"

#include <cio/Class.h>
#include <cio/Resize.h>
#include <cio/Seek.h>

namespace cio
{
	Class<MemoryInput> MemoryInput::sMetaclass("cio::MemoryInput");

	const Metaclass &MemoryInput::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &MemoryInput::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	MemoryInput::MemoryInput() noexcept
	{
		// nothing to do
	}

	MemoryInput::MemoryInput(const MemoryInput &in) = default;
	
	MemoryInput::MemoryInput(MemoryInput &&in) noexcept = default;
	
	MemoryInput &MemoryInput::operator=(const MemoryInput &in) = default;
	
	MemoryInput &MemoryInput::operator=(MemoryInput &&in) noexcept = default;

	MemoryInput::MemoryInput(const void *data, std::size_t length) noexcept :
		mBuffer(data, length)
	{
		// nothing more to do
	}

	MemoryInput::~MemoryInput() noexcept = default;

	void MemoryInput::bind(ReadBuffer buffer)
	{
		mBuffer = std::move(buffer);
	}

	void MemoryInput::bind(const void *data, std::size_t length)
	{
		mBuffer.bind(data, length);
	}

	ReadBuffer MemoryInput::unbind()
	{
		return std::move(mBuffer);
	}

	ReadBuffer &MemoryInput::buffer() noexcept
	{
		return mBuffer;
	}

	const ReadBuffer &MemoryInput::buffer() const noexcept
	{
		return mBuffer;
	}

	void MemoryInput::clear() noexcept
	{
		mBuffer.clear();
		Input::clear();
	}

	bool MemoryInput::isOpen() const noexcept
	{
		return mBuffer.size() > 0;
	}

	Length MemoryInput::size() const noexcept
	{
		return mBuffer.limit();
	}

	bool MemoryInput::empty() const noexcept
	{
		return mBuffer.empty();
	}

	Length MemoryInput::readable() const noexcept
	{
		return mBuffer.remaining();
	}

	Progress<std::size_t> MemoryInput::requestRead(void *data, std::size_t bytes) noexcept
	{
		return mBuffer.requestRead(data, bytes);
	}

	Progress<std::size_t> MemoryInput::requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept
	{
		Progress<std::size_t>  result(Action::Read, 0, bytes);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
		
		if (absolute < 0)
		{
			result.fail(Reason::Underflow);
		}
		else if (absolute >= mBuffer.limit())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			std::size_t start = absolute.size();
			std::size_t toRead = std::min(mBuffer.size() - start, bytes);
			std::memcpy(data, mBuffer.data() + start, toRead);
			result.count = toRead;
			result.succeed();
		}
		
		return result;
	}

	Length MemoryInput::getReadPosition() const noexcept
	{
		return mBuffer.position();
	}

	Progress<Length> MemoryInput::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
			
		if (absolute < 0)
		{
			result.fail(Reason::Underflow);
		}
		else if (absolute > mBuffer.limit())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.position(position.size());
			result.succeed();
		}
		
		return result;
	}

	Progress<Length> MemoryInput::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Read);
		
		if (bytes < 0)
		{
			result.fail(Reason::Invalid);
		}
		else if (!mBuffer.available())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.skip(bytes.size());
			result.succeed();
		}
			
		return result;
	}

	Text MemoryInput::getText(std::size_t bytes)
	{
		return mBuffer.getText(bytes).duplicate();
	}

	Text MemoryInput::getTextLine(std::size_t bytes)
	{
		return mBuffer.getTextLine(bytes).duplicate();
	}
}
