/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CHANNEL_H
#define CIO_CHANNEL_H

#include "Types.h"

#include "Input.h"
#include "Output.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The Channel interface represents the ability to use the same transport layer resource to act as both Input and Output concurrently.
	 * Depending on the underlying transport model, the Input and Output may be two sequential streams tied to the same address or resource,
	 * or may represent a single unified resource stream that can both be read and written such as a memory buffer or a file on disk.
	 * 
	 * This method overrides all methods defined in both the Input and Output base class to resolve ambiguity.
	 */
	class CIO_API Channel : public Input, public Output
	{
		public:
			/**
			 * Gets the metaclass for the Channel class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Channel() noexcept override;

			/**
			 * Clears the current state of the Channel. This should close any files, handles, or other resources
			 * and free any memory used, but should not remove the underlying storage or data if not exclusively
			 * owned by this Channel.
			 */
			virtual void clear() noexcept override;
			
			/*
			 * Checks whether the channel is currently open and usable in any way.
			 * 
			 * Overriden from Resource to resolve the ambiguity of inheriting from both Input and Output.
			 * The base Channel implementation always returns false.
			 *
			 * @return whether the channel is open
			 */
			virtual bool isOpen() const noexcept override;

			/**
			 * Gets the metaclass for the runtime type of this object.
			 *
			 * @return the metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Opens the Channel to work with the given resource with both read and write access if possible.
			 * This method will fail if the resource is not present.
			 * The default protocol factory is used when necessary.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resource could be opened with
			 */
			virtual cio::ModeSet open(const cio::Path &resource);

			/**
			 * Opens the Channel to create the given resource with both read and write access if possible.
			 * This method will fail if the resource is already present.
			 * The default protocol factory is used when necessary.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resource could be opened with
			 */
			virtual cio::ModeSet create(const cio::Path &resource);

			/**
			 * Opens the Channel to create the given resource with both read and write access if possible.
			 * This method will create the resource if it is not present.
			 * The default protocol factory is used when necessary.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the resource could be opened with
			 */
			virtual cio::ModeSet openOrCreate(const cio::Path &resource);

			/**
			 * Opens the resource using the given open modes.
			 * The given protocol factory is used to traverse parent resources and construct
			 * underlying transport and codec mechanisms if necessary.
			 *
			 * This is the fully general open method that subclasses must override.
			 * The base class simply returns the empty mode set.
			 *
			 * @param resource The resource to open
			 * @param modes The desired resource modes to use to open the resource
			 * @param factory The factory to use to construct underlying transport and codec protocols
			 * @return the actual resource modes that the resource could be opened with
			 * @throw cio::Exception If the resource could not be opened
			 */
			virtual cio::ModeSet openWithFactory(const cio::Path &resource, cio::ModeSet modes, ProtocolFactory *factory) override;

			// Overall resource metadata

			/**
			 * Requests the metadata for the currently opened resource if possible to obtain.
			 * Only the fields that are known are returned.
			 *
			 * The base implementation constructs metadata using type(), size(), location(), and modes().
			 * Subclasses may override for performance if desired.
			 *
			 * @return all known metadata for the open channel
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Metadata metadata() const override;

			/**
			 * Gets the most specific path location of the resource, if possible.
			 *
			 * The base implementation returns an empty path.
			 *
			 * @return the resource location of the open channel if known
			 * @throw std::bad_alloc If memory allocations failed for any operation
			 */
			virtual Path location() const override;

			/**
			 * Gets the overall resource type if known.
			 *
			 * The base implementation returns Resource::Unknown.
			 *
			 * @return the resource type of the open channel if known
			 */
			virtual Resource type() const noexcept override;

			/**
			 * Gets the current modes of the resource to the extent known.
			 *
			 * The base implementation returns an empty mode set.
			 *
			 * @return the modes of the open channel if known
			 */
			virtual ModeSet modes() const noexcept override;

			/**
			 * Gets the total size in bytes of the resource providing the input if known.
			 * If unknown or potentially unbounded, the constant CIO_UNKNOWN_LENGTH is returned, which is
			 * also the largest possible std::uint64_t size value.
			 *
			 * The base implementation returns CIO_UNKNOWN_LENGTH. Subclasses should always override it.
			 *
			 * @return the total resource size of the open channel in bytes
			 */
			virtual Length size() const noexcept override;

			/**
			 * Checks whether the resource is empty and contains zero bytes.
			 *
			 * @return whether the resource is emtpy
			 */
			virtual bool empty() const noexcept override;

			// Polymorphic API - virtual methods that may perform partial actions

			/**
			 * Gets whether the load and store positions are linked such that setting one also modifies the other.
			 * This is true on resources like files but false on duplex channels like sockets.
			 *
			 * Some subclasses may return a different value depending on the exact resource they open.
			 * Others, the property is inherent in the class type.
			 * 
			 * If true, this has a number of important implications:
			 * <ul>
			 * <li>getReadPosition() and getWritePosition() are always equal</li>
			 * <li>requestRead, requestReadSeek, requestReadMove, and requestDiscard also modify the write position</li>
			 * <li>requestWrite, requestWriteSeek, requestWriteMove, requestSkip, and requestSplat also modify the read position</li>
			 * </ul>
			 *
			 * The base class returns false. Subclasses should override if the positions are or might be linked.
			 */
			virtual bool linkedReadWritePosition() const noexcept;

			/**
			 * Interprets whether the channel is currently open when used in a Boolean context.
			 *
			 * @return whether the channel is currently open
			 */
			explicit operator bool() const noexcept;

		private:
			/** The metaclass for this class */
			static Class<Channel> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

