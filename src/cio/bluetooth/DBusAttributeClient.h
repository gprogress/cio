/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_DBUSATRIBUTECLIENT_H
#define CIO_BLUETOOTH_DBUSATRIBUTECLIENT_H

#include "Types.h"

#include "AttributeClient.h"
#include "DBusClient.h"
#include "Subscriptions.h"

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The CIO D-Bus Bluetooth Attribute Client implements GATT support using the BlueZ D-Bus API.
		 * This is the officially recommended implementation for Linux clients.
		 *
		 * @note This class can be used regardless of whether D-Bus is used for the controller interface and the
		 * two classes are entirely independent. However in most cases it makes sense to use both together to avoid odd system interactions.
		 */
		class CIO_BLUETOOTH_API DBusAttributeClient : public AttributeClient
		{
			public:		
				/** The text for the BlueZ D-Bus domain. Currently org.bluez. */
				static const char *BLUEZ_DOMAIN;
				
				/** The text for the D-Bus interface for BlueZ adapters. Currently org.bluez.Adapter1. */
				static const char *BLUEZ_ADAPTER_IFACE;
				
				/** The text for the D-Bus interface for BlueZ peripherals. Currently org.bluez.Device1. */
				static const char *BLUEZ_PERIPHERAL_IFACE;
				
				/** BlueZ D-Bus interface for GATT services */
				static const char *BLUEZ_SERVICE_IFACE;
				
				/** BlueZ D-Bus interface for GATT characteristics */
				static const char *BLUEZ_CHARACTERISTIC_IFACE;
				
				/** BlueZ D-Bus interface for GATT attribute descriptors */
				static const char *BLUEZ_DESCRIPTOR_IFACE;
									
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Construct an unopened Bluetooth Attribute Client.
				 */
				DBusAttributeClient() noexcept;
				
				/**
				 * Constructs a Bluetooth Attribute Client by adopting the state of a given client.
				 * 
				 * @param in The client to move
				 */
				DBusAttributeClient(DBusAttributeClient &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Attribute Client into this connection.
				 * The current state is cleared first.
				 *
				 * @param in The connection to move
				 * @return this connection
				 */
				DBusAttributeClient &operator=(DBusAttributeClient &&in) noexcept;
				
				/**
				 * Destructor. Clears all device state.
				 */
				virtual ~DBusAttributeClient() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the connection state and disconnects from any connected remote device.
				 */				
				virtual void clear() noexcept override;
				
				// Connection management
				
				/**
				 * Connects to the given peripheral using the first available local adapter.
				 *
				 * @param peripheral The peripheral to connect to
				 */
				virtual void connect(const Peripheral &peripheral) override;
					
				/**
				 * Connects to the given peripherall using the given local device as the adapter.
				 *
				 * @param adapter The local adapter to use
				 * @param peripheral The peripheral to connect to
				 */								
				virtual void connect(const Device &adapter, const Peripheral &peripheral) override;
				
				/**
				 * Disconnects from the current peripheral.
				 */
				virtual void disconnect() noexcept override;
				
				/**
				 * Checks whether the client is currently connected.
				 *
				 * @return whether the client is connected
				 */
				virtual bool isOpen() const noexcept override;
				
				// D-Bus specific methods
				
				/**
				 * Gets the connection to the D-Bus system bus.
				 *
				 * @return the D-Bus system connection
				 */
				DBusClient &getSystemBus() const noexcept;
				
				/**
				 * Gets the connection to the D-Bus system bus,
				 * creating a new connection if none exists.
				 *
				 * @return the D-Bus system connection
				 */
				DBusClient &getOrCreateSystemBus();
				
				/**
				 * Sets the connection to the D-Bus system bus.
				 * This controller will take ownership of the connection.
				 *
				 * @param connection The D-Bus system connection to use
				 */
				void setSystemBus(const DBusClient &connection) noexcept;
				
				// Discovery - polymorphic API implemented with D-Bus
				
				/**
				 * Gets an attribute's type given its handle.
				 *
				 * @param handle The attribute handle
				 * @return the attribute type
				 */
				virtual UniqueId getAttributeType(std::uint16_t handle) override;
									
				/**
				 * Gets the list of every attribute in a handle range on the remote device.
				 * It's generally more useful to work with the hierarchy of services, characteristics, and descriptors,
				 * but this method will get you a flat list of attributes.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of attributes on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Attribute> discoverAttributes(std::uint16_t start, std::uint16_t end) override;
							
				/**
				 * Finds the handle of the first attribute with the given type within the given handle range.
				 *
				 * @param type The type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the first attribute handle with the given type
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual std::uint16_t findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end) override;
								
				/**
				 * Gets a service given its handle.
				 *
				 * @param handle The service handle
				 * @return the service
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Service getService(std::uint16_t handle) override;
				
				/**
				 * Discovers all primary services on the remote device.
				 * This issues a sequence of read value by group type requests to process all possible handle ranges.
				 * 
				 * @return the list of all primary services
				 */
				virtual std::vector<Service> discoverPrimaryServices(std::uint16_t start, std::uint16_t end) override;

				/**
				 * Finds a primary service given its service type in a particular handle range.
				 *
				 * @param id The service type (value UUID of the service)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first service with the given definition
				 */
				virtual Service findPrimaryServiceInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Gets a characteristic given its handle.
				 *
				 * @param handle The characteristic handle
				 * @return the characteristic
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Characteristic getCharacteristic(std::uint16_t handle) override;
				
				/**
				 * Discovers characteristics on the remote device within a specified handle range.
				 * It's generally more useful to start with services and find associated characteristics, but if you need a particular range in one pass, this will do it.
				 * This issues a sequence of read value by type requests to process the given handle range.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of characteristics in the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the characteristics
				 */		
				virtual std::vector<Characteristic> discoverCharacteristics(std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Finds a characteristic given its value type in a particular handle range.
				 *
				 * @param id The value type (value UUID of the characteristic)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first characteristic with the given value type
				 */
				virtual Characteristic findCharacteristicInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Gets the list of every descriptor in a handle range on the remote device.
				 * It's generally more useful to start with services and characteristics, but if you need the full list in one pass, this will do it.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of descriptors on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Descriptor> discoverDescriptors(std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Gets an attribute given its handle.
				 *
				 * @param handle The attribute handle
				 * @return the attribute
				 * @throw cio::Exception If a transport or packet error prevented reading the attribute or it did not exist
				 */
				virtual Descriptor getDescriptor(std::uint16_t handle) override;

				/**
				 * Finds the first descriptor with the given type within the given handle range.
				 *
				 * @return the attribute with the given type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual Descriptor findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Requests to read the given attribute given its handle.
				 *
				 * @param handle The attribute handle
				 * @param listener The listener to receive the data
				 * @param offset The offset of the remote attribute value to start reading at
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset) override;
				
				/**
				 * Requests to read one or more values in an attribute range whose attribute matches the given type.
				 *
				 * @param type The type
				 * @param listener The listener to receive each read value
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 */
				virtual Progress<std::size_t> readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Requests to write the given attribute with confirmation given its handle.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @param offset The offset of the remote attribute value to start write at
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset) override;
				
				/**
				 * Requests to write the given attribute without confirmation given its handle.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> sendValue(std::uint16_t handle, const void *buffer, std::size_t requested) override;
				
				/**
				 * Attempts to subscribe to receive updates for the given characteristic's value.
				 * Each received update will be given to the provided listener.
				 *
				 * If the handle is a characteristic value handle, and if the characteristic supports notify/indicate mode, and if
				 * this is the first subscription to that attribute, then this will call the D-Bus "StartNotify" method on the
				 * org.bluez.GattCharacteristic1 interface so that D-Bus starts broadcasting incoming updates to this client.
				 *
				 * If the handle is not a characteristic value or does not support indicate/notify mode, then it is set up to use
				 * read polling instead.
				 *
				 * The returned Subscription pointer is stable and will not be deleted until unsubscribe is called.
				 * Users should use it as a key if they want to manage when this particular subscription is unsubscribed.
				 * 
				 * @param handle The handle of the characteristic to subscribe to
				 * @param listener The listener to receive subscriptions
				 * @param interval The polling interval to use, or 0 to receive all updates
				 * @return the status of this operation
				 */
				virtual const Subscription *subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval) override;
				
				/**
				 * Runs a loop to receive incoming subscription updates based on the given executor.
				 * The executor specifies the maximum amount of time to run and allows for asynchronous cancelation, and the client should report
				 * completion status (either success or errors) ot the executor without throwing exceptions.
				 *
				 * This class first executes a polling loop for all subscriptions that do not support indicate/notify mode.
				 * Then it processes all incoming D-Bus property updates for characteristic "Value" property that are set up for that.
				 *
				 * @param exec The executor to manage timeout and cancelation and report results to
				 * @return the final status of the execution, Success if it ran to the full timeout, Abandoned if canceled, or a failure status if
				 * an actual error prevented any subscription processing
				 */
				virtual State executeSubscriptions(Task &exec) noexcept override;
				
				/**
				 * Unsubscribes the subscription of the given characteristic.
				 * If this was the last subscription for this handle, this also calls the D-Bus "StopNotify" method on the org.bluez.GattCharacteristic1 interface for
				 * the characteristic value handle if it was set up in notify/indicate mode.
				 *
				 * @param c The characteristic of interest
				 * @param modes which types of subscription modes to unsubscribe
				 * @return the status of this operation
				 */
				virtual State unsubscribe(const Subscription *subscription) override;
				
				/**
				 * Unsubscribes all subscriptions for the given attribute handle.
				 * This also calls the D-Bus "StopNotify" method on the org.bluez.GattCharacteristic1 interface for
				 * the characteristic value handle if it was set up in notify/indicate mode.
				 *
				 * @warning This invalidates all Subscription pointers for this handle.
				 *
				 * @param handle The attribute handle
				 * @return the number of subscriptions removed and status
				 */
				virtual Progress<std::size_t>  clearSubscriptions(std::uint16_t handle) override;
				
				/**
				 * Unsubscribes all subscriptions for all attributes.
				 * This also calls the D-Bus "StopNotify" method on the org.bluez.GattCharacteristic1 interface for
				 * all characteristic value handles that were set up for notify/indicate mode.
				 *
				 * @warning This invalidates all Subscription pointers.
				 *
				 * @return the number of subscriptions removed and status
				 */
				virtual Progress<std::size_t> clearAllSubscriptions() override;
				
				/**
				 * Gets direct access to the known services for this client.
				 * This will conduct service discovery if it has not yet been done.
				 *
				 * @return all services available over this client's connection
				 */
				std::vector<Service> &getServices();
				
				/**
				 * Gets direct access to the known services for this client.
				 * This will conduct service discovery if it has not yet been done.
				 *
				 * @return all services available over this client's connection
				 */
				const std::vector<Service> &getServices() const;
				
				/**
				 * Gets direct access to the known services for this client.
				 * This will return the currently cached results without conducting discovery.
				 *
				 * @return all known services available over this client's connection
				 */
				std::vector<Service> &getCachedServices() noexcept;
				
				/**
				 * Gets direct access to the known services for this client.
				 * This will return the currently cached results without conducting discovery.
				 *
				 * @return all known services available over this client's connection
				 */
				const std::vector<Service> &getCachedServices() const noexcept;
				
				/**
				 * Directly sets the known services for this client to a copy of the given list of services.
				 * This can be used to restore the cached state from persistent storage.
				 *
				 * @warning This method assumes that the services are in ascending order of handle range and do not overlap
				 *
				 * @param services The list of services to copy
				 */
				void setCachedServices(const std::vector<Service> &services);
				
				/**
				 * Directly sets the known services for this client by moving the given list of services.
				 * This can be used to restore the cached state from persistent storage.
				 *
				 * @warning This method assumes that the services are in ascending order of handle range and do not overlap
				 *
				 * @param services The list of services to move
				 */
				void setCachedServices(std::vector<Service> &&services) noexcept;
			
				/**
				 * Clears the cached service list to force service discovery next time services are needed.
				 */
				void clearCachedServices() noexcept;
				
				/**
				 * Uses the D-Bus connection and current adapter and peripheral settings to discover the list of known
				 * services provided by BlueZ. This does not consult the existing list of cached services and is primarily
				 * intended to help build that list.
				 *
				 * @return the list of available GATT services
				 */
				std::vector<Service> discoverRemoteServices() const;
				
				/**
				 * Synchronizes the current service state of this client.
				 * If services have not yet been discovered, then discovery will be conducted.
				 *
				 * @return the result of service discovery
				 */
				State synchronizeServices() const noexcept;
				
				/**
				 * Prints the adapter D-Bus object path to the given text buffer.
				 * This is a prefix of the peripheral path and all GATT object paths.
				 *
				 * @param adapter The adapter
				 * @param text The text buffer
				 * @param length The maximum text length
				 * @return the actual text length needed
				 */
				std::size_t printAdapterPath(const Device &adapter, char *text, std::size_t length) const noexcept;
				
				/**
				 * Prints the peripheral D-Bus object path suffix (without the adapter prefix) to the given text buffer.
				 * This is a prefix of all GATT object paths.
				 *
				 * @param peripheral The MAC address of the peripheral
				 * @param text The text buffer
				 * @param length The maximum text length
				 * @return the actual text length needed
				 */
				std::size_t printPeripheralPathSuffix(const DeviceAddress &peripheral, char *text, std::size_t length) const noexcept;
				
				/**
				 * Prints the full peripheral D-Bus object path including adapter prefix and MAC suffix to the given text buffer.
				 * This is a prefix of all GATT object paths.
				 *
				 * @param adapter The adapter
				 * @param peripheral The MAC address of the peripheral
				 * @param text The text buffer
				 * @param length The maximum text length
				 * @return the actual text length needed
				 */
				std::size_t printPeripheralPath(const Device &adapter, const DeviceAddress &peripheral, char *text, std::size_t length) const noexcept;
				
				/**
				 * Parses the given D-Bus object path to verify that it is a GATT Service and if so determine the handle of the service itself.
				 *
				 * @param object The D-Bus object path
				 * @return the service handle or 0x0000 if the object is not a service
				 */
				std::uint16_t parseServiceHandle(const char *object) const;
				
				/**
				 * Parses the given D-Bus object path to verify that it is a GATT Characteristic and if so determine the handle of the characteristic itself.
				 *
				 * @param object The D-Bus object path
				 * @return the characteristic handle or 0x0000 if the object is not a characteristic
				 */
				std::uint16_t parseCharacteristicHandle(const char *object) const;
				
				/**
				 * Parses the given D-Bus object path to verify that it is a GATT Descriptor and if so determine its handle.
				 *
				 * @param object The D-Bus object path
				 * @return the descriptor handle or 0x0000 if the object is not a service
				 */
				std::uint16_t parseDescriptorHandle(const char *object) const;
				
				/**
				 * Parses the four text bytes that represents a single GATT handle.
				 *
				 * @param object The object path at the start of the four handle digits
				 * @return the handle value
				 */
				std::uint16_t parseHandleValue(const char *object) const;
				
				/**
				 * Prints the D-Bus object path that represents the given attribute.
				 * This determines if the handle is a service, characteristic, or descriptor and then prints the appropriate path.
				 * Note that a characteristic value has the same D-Bus object handle as its containing characteristic.
				 *
				 * @param handle The attribute handle
				 * @param text The text buffer to print to
				 * @param length The number of bytes available in the text buffer
				 * @return the actual number of bytes needed for the full path
				 */
				std::size_t printAttributePath(std::uint16_t handle, char *text, std::size_t length) const noexcept;
				
				/**
				 * Prints the D-Bus object path that represents the given service.
				 * This method just prints the expected path and does not validate whether the service exists.
				 *
				 * @param service The service handle
				 * @param text The text buffer to print to
				 * @param length The number of bytes available in the text buffer
				 * @return the actual number of bytes needed for the full path
				 */
				std::size_t printServicePath(std::uint16_t service, char *text, std::size_t length) const noexcept;
				
				/**
				 * Prints the D-Bus object path that represents the given characteristic inside the given service.
				 * This method just prints the expected path and does not validate whether the service exists.
				 * Note that a characteristic value has the same D-Bus object handle as its containing characteristic.
				 *
				 * @param service The service handle
				 * @param c The characteristic handle
				 * @param text The text buffer to print to
				 * @param length The number of bytes available in the text buffer
				 * @return the actual number of bytes needed for the full path
				 */
				std::size_t printCharacteristicPath(std::uint16_t service, std::uint16_t c, char *text, std::size_t length) const noexcept;
				
				/**
				 * Prints the D-Bus object path that represents the given descriptor inside the given characteristic inside the given service.
				 * This method just prints the expected path and does not validate whether the descriptor exists.
				 * Note that the characteristic value cannot be used as a descriptor for this purpose and is the same D-Bus object as its containing characteristic.
				 *
				 * @param service The service handle
				 * @param c The characteristic handle
				 * @param desc The descriptor handle
				 * @param text The text buffer to print to
				 * @param length The number of bytes available in the text buffer
				 * @return the actual number of bytes needed for the full path
				 */
				std::size_t printDescriptorPath(std::uint16_t service, std::uint16_t c, std::uint16_t desc, char *text, std::size_t length) const noexcept;
				
				/**
				 * Reads the properties of a D-Bus object to build a GATT service.
				 * This does not build out the contained characteristics, just the properties of the service itself.
				 *
				 * @param object The D-Bus object path to a service
				 * @return the GATT service read from the object
				 */
				Service readServiceInformation(const char *object) const;
				
				/**
				 * Reads the properties of a D-Bus object to build a GATT characteristic.
				 * This does not build out the contained declarations, just the properties of the characteristic and its value.
				 *
				 * @param object The D-Bus object path to a characteristic
				 * @return the GATT service read from the object
				 */
				Characteristic readCharacteristicInformation(const char *object) const;
				
				/**
				 * Reads the properties of a D-Bus object to build a GATT descriptor.
				 *
				 * @param object The D-Bus object path to a descriptor
				 * @return the GATT descriptor read from the object
				 */
				Descriptor readDescriptorInformation(const char *object) const;
				
				/**
				 * Locates the internal index offset of the service whose handle range contains the given handle.
				 * This requires service synchronization to have already been performed.
				 * If no such service exists, the size of the service array is returned.
				 *
				 * @param handle The handle
				 * @return the index of the service or the end index if not found
				 */
				std::size_t locateServiceIndex(std::uint16_t handle) const noexcept;
				
				/**
				 * Locates the internal index offset range of the services whose handle ranges overlap the given handle range.
				 * This requires service synchronization to have already been performed.
				 * The handle range is inclusive of both start and end.
				 * The returned index range is exclusive of the end.
				 * If no such services exist, an empty range is returned.
				 *
				 * @param start The lowest handle of the handle range to find
				 * @param end The highest handle (inclusive) of the handle range to find
				 * @return the range of index values (exclusive of end) overlapping the given handle range
				 */
				std::pair<std::size_t, std::size_t> locateServiceRange(std::uint16_t start, std::uint16_t end) const noexcept;
					
				/**
				 * Sets whether to monitor the BLE connection state.
				 * If enabled, this will subscribe to updates to the 'Connected' property on the associated peripheral.
				 * Disconnect events will be reported by executeSubscriptions as Status::Disconnected.
				 *
				 * @param value Whether to monitor connection state
				 */
				virtual void setMonitorConnectionState(bool value) override;
				
			private:
				/**
				 * Common method for all unsubscribe approaches to notify D-Bus to stop notification of characteristic values in notify or indicate mode.
				 *
				 * @param handle The handle of the characteristic value handle to stop notification for
				 */
				void stopNotify(std::uint16_t handle);
				
				/**
				 * Subscribes to events from the peripheral such as connection, disconnection, and so on.
				 */
				void applyConnectionMonitor();
				
				void clearConnectionMonitor();
			
				/** The metaclass for this class */
				static Class<DBusAttributeClient> sMetaclass;
				
				/** The D-Bus system bus */
				mutable DBusClient mSystemBus;
				
				/** D-Bus object path to the connected peripheral that is the base of all GATT paths */
				cio::Path mPeripheralPath;
				
				/** Services cached from D-Bus queries in sorted order */
				mutable std::vector<Service> mServices;
		};
	}
}

#endif

