/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_SERVICE_H
#define CIO_NET_SERVICE_H

#include "Types.h"

#include "Address.h"
#include "AddressFamily.h"
#include "ChannelType.h"
#include "Flow.h"
#include "ProtocolFamily.h"

#include <cio/Path.h>

#include <iosfwd>

namespace cio
{
	namespace net
	{
		/**
		 * The CIO Net Service class provides a complete description of how to connect to or listen for a particular network service.
		 * It includes the network address, protocol type, channel type, and port.
		 */
		class CIO_NET_API Service
		{
			public:
				/**
				 * Construct an empty service.
				 */
				Service() noexcept;

				/**
				 * Constructs a service from a protocol family and port.
				 * The default channel type for the protocol will be selected.
				 * The address will be unspecified.
				 *
				 * @param pf The protocol family
				 * @param port The port
				 * @param flow The flow type, or None to autodetect
				 */
				explicit Service(const ProtocolFamily &pf, std::uint16_t port = 0, Flow flow = Flow::None) noexcept;

				/**
				 * Constructs a service from an IPv4 address, protocol family, and port.
				 * The default channel type for the protocol will be selected.
				 * The address will be unspecified.
				 *
				 * @param ip The IPv4 address
				 * @param pf The protocol family
				 * @param port The port
				 * @param flow The flow type, or None to autodetect
				 */
				Service(const IPv4 &ip, const ProtocolFamily &pf, std::uint16_t port = 0, Flow flow = Flow::None) noexcept;
				
				/**
				 * Constructs a service from a IPv6 address, protocol family, and port.
				 * The default channel type for the protocol will be selected.
				 *
				 * @param ip The IPv6 address
				 * @param pf The protocol family
				 * @param port The port
				 * @param flow The flow type, or None to autodetect
				 */
				Service(const IPv6 &ip, const ProtocolFamily &pf, std::uint16_t port = 0, Flow flow = Flow::None) noexcept;

				/**
				 * Constructs a service from an address and protocol family.
				 * The default channel type for the protocol will be selected.
				 * The port is detected from the address if present.
				 *
				 * @param address The address
				 * @param pf The protocol family
				 * @param flow The flow type, or None to autodetect
				 */
				Service(Address address, const ProtocolFamily &pf, Flow flow = Flow::None) noexcept;

				/**
				 * Construct a deep copy of a service.
				 *
				 * @param in The service to copy
				 */
				Service(const Service &in);

				/**
				 * Construct a service by transferring the contents of an existing service.
				 *
				 * @param in The service to move
				 */
				Service(Service &&in) noexcept;

				/**
				 * Copy a service.
				 *
				 * @param in The service to copy
				 * @return this service
				 */
				Service &operator=(const Service &in);

				/**
				 * Move a service.
				 *
				 * @param in The service to move
				 * @return this service
				 */
				Service &operator=(Service &&in) noexcept;

				/**
				 * Destructors.
				 */
				~Service() noexcept;

				/**
				 * Clears all state on the service.
				 */
				void clear() noexcept;

				/**
				 * Interprets the service as true or false in a Boolean context.
				 * Services are considered true if all parameters - address, channel type, protocol type, and port - are set.
				 *
				 * @return whether the service has all parameters set
				 */
				explicit operator bool() const noexcept;

				/**
				 * Gets the host path as used to determine the service.
				 * 
				 * @return the host path
				 */
				const cio::Path &getHost() const noexcept;

				/**
				 * Sets the host path to use to determine the service.
				 * This does not automatically resolve the path into other service state.
				 * 
				 * @param path The new host path
				 */
				void setHost(cio::Path path) noexcept;

				/**
				 * Performs service resolution to determine the address, protocol family, and port.
				 */
				void resolve();

				/**
				 * Performs minimal service resolution to determine the protocol family and port.
				 * The address is not resolved.
				 */
				void resolveProtocolAndPort();

				/**
				 * Gets the channel type for the service.
				 *
				 * @return the channel type
				 */
				ChannelType getChannelType() const noexcept;

				/**
				 * Sets the channel type for the service.
				 * This overrides any default type assumed from the protocol type.
				 *
				 * @param type The channel type
				 */
				void setChannelType(ChannelType type) noexcept;

				/**
				 * Gets the protocol type for the service.
				 *
				 * @return the protocol type
				 */
				ProtocolFamily getProtocolFamily() const noexcept;

				/**
				 * Sets the protocol type for the service.
				 * If the channel type is not set, the default channel type for the protocol type will also be set.
				 *
				 * @param type The protocol type
				 */
				void setProtocolFamily(ProtocolFamily type) noexcept;

				/**
				 * Gets the address for the service.
				 *
				 * @return the address
				 */
				const Address &getAddress() const noexcept;

				/**
				 * Sets the address for the service to a copy of the given address.
				 * If the port is not set on the service, it is taken from the address.
				 *
				 * @param address The address to copy
				 * @throw std::bad_alloc If a memory allocation was necessary but failed
				 */
				void setAddress(const Address &address);

				/**
				 * Sets the address for the service by adopting the contents of the given address.
				 * If the port is not set on the service, it is taken from the address.
				 *
				 * @param address The address to move
				 */
				void setAddress(Address &&address) noexcept;

				/**
				 * Gets the native OS constant for the address family.
				 *
				 * @return the address family
				 */
				AddressFamily getAddressFamily() const noexcept;

				/**
				 * Gets the port for the service.
				 *
				 * @return the port
				 */
				std::uint16_t getPort() const noexcept;

				/**
				 * Sets the port for the service.
				 * This will also update the port on the address if it set.
				 *
				 * @param port The port to use
				 */
				void setPort(std::uint16_t port) noexcept;
				
				/**
				 * Gets the flow type. Defaults to None which means to autodetect.
				 * Not meaningful for Stream channel types since those are inherently Unicast.
				 * Primarily affects socket options when creating sockets.
				 *
				 * @return the flow type
				 */
				Flow getFlow() const noexcept;
				
				/**
				 * Sets the flow type. Defaults to None which means to autodetect.
				 * Not meaningful for Stream channel types since those are inherently Unicast.
				 * Primarily affects socket options when creating sockets.
				 *
				 * @param flow the flow type
				 */
				void setFlow(Flow flow) noexcept;

			private:
				/** Host path */
				cio::Path mHost;

				/** The address */
				Address mAddress;

				/** The protocol type */
				ProtocolFamily mProtocolFamily;

				/** The port */
				std::uint16_t mPort;
				
				/** The channel type */
				ChannelType mChannelType;
				
				/** The flow type */
				Flow mFlow;
		};

		/**
		 * Prints the service to a C++ stream.
		 *
		 * @param stream The stream
		 * @param service The service
		 * @return the stream
		 */
		CIO_NET_API std::ostream &operator<<(std::ostream &stream, const Service &service);
	}
}

#endif


