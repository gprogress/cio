/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ENUMERATION_H
#define CIO_ENUMERATION_H

#include "Types.h"

#include "Enumerants.h"

namespace cio
{
	/**
	 * The Enumeration<E> is the metaclass for the a C++ enum. In addition to baseline metaclass features provided by Class<E>
	 * it provides a container interface for actually exploring the enumerant codes, values, descriptions, and so on.
	 *
	 * By design, the generic template does not exist as there is no generic way to define such an enumeration.
	 * Users must explicitly specialize for their enumeration type.
	 * 
	 * Specializations must define the following members:
	 * const Enumeration<E> Enumeration<E>::instance - global instance
	 * const Enumerant<E> Enumeration<E>::values[] - array of enum values in sequence
	 * 
	 * Typically the specializations should be a final subclass Enumerants<E> to get all of the expected capabilities.
	 * However, nothing stops you from implementing your own enumeration in a different way as long as
	 * all of the methods prescribed by Enumerants<E> are present.
	 * 
	 * Note that since operator<< and operator>> are found via argument-dependent lookup, these methods must be individually
	 * overloaded in the same namespace as the enum itself, and cannot be autogenerated by this template.
	 * They can be trivially implemented by calling print and parse.
	 *
	 * @tparam E The enum class type
	 */
	template <typename E>
	class Enumeration;

	/**
	 * Obtains the Enumeration<E> metaclass for C++ enum E. If the type E is not actually an enumeration, this will fail to specialize due
	 * to the use of std::enable_if, which can be used by other methods to detect if the Enumeration<E> is available for use.
	 * 
	 * @tparam E The C++ enum type
	 * @param value The enum value of interest
	 * @return the enumeration for E
	 */
	template <typename E>
	auto enumeration(E value) noexcept -> decltype(Enumeration<E>::instance)
	{
		return Enumeration<E>::instance;
	}

	/**
	 * Obtains the Enumerant<E> metaclass for C++ enum value of type E. If the type E is not actually an enumeration, this will fail to specialize,
	 * which can be used by other methods to detect whether the value is part of a valid enumeration.
	 *
	 * @tparam E The C++ enum type
	 * @param value The enum value of interest
	 * @return the enumerant for the value
	 */
	template <typename E>
	auto enumerant(E value) noexcept -> decltype(enumeration(value).find(value))
	{
		return enumeration(value).find(value);
	}

	/**
	 * Calculates the exact printing length of the specified C++ enum value for UTF-8 text without any trailing null-terminator.
	 * This will fail to specialize if E is not a valid enum type allowing for another overload to be chosen.
	 * 
	 * @tparam E The enum type
	 * @param value The enum value
	 * @return the UTF-8 text length of the enum printed text
	 */
	template <typename E>
	inline auto strlen(const E &value) noexcept -> decltype(enumeration(value).strlen(value))
	{
		return enumeration(value).strlen(value);
	}

	/**
	 * Prints the specified C++ enum value to a UTF-8 text buffer with a maximum capacity.
	 * This will fail to specialize if E is not a valid enum type allowing for another overload to be chosen.
	 * 
	 * @tparam E The enum type
	 * @param value The enum value
	 * @param buffer The UTF-8 text buffer
	 * @param capacity The capacity of the text buffer
	 * @return the UTF-8 text length needed for the enum printed text
	 */
	template <typename E>
	inline auto print(const E &value, char *buffer, std::size_t capacity) noexcept -> decltype(enumeration(value).print(value, buffer, capacity))
	{
		return enumeration(value).print(value, buffer, capacity);
	}

	/**
	 * Prints the specified C++ enum value to be returned as UTF-8 text. This returns the enum's static text directly.
	 * This will fail to specialize if E is not a valid enum type allowing for another overload to be chosen.
	 * 
	 * @tparam E The enum type
	 * @param value The enum value
	 * @return the UTF-8 text for the enum
	 */
	template <typename E>
	inline auto print(const E &value) -> decltype(enumeration(value).print(value))
	{
		return enumeration(value).print(value);
	}
}

#endif
