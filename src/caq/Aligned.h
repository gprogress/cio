/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef GAQ_ALIGNED_H
#define GAQ_ALIGNED_H

#include "Types.h"

#include "Accessor.h"
#include "Native.h"
#include "Reader.h"

namespace caq
{
	/**
	 * The Aligned accessor provides direct access to an aligned native datatype via interleaved arrays with an offset and a stride
	 * with a fixed number of components pulled from an underlying data array of a compatible type.
	 * 
	 * @warning This class cannot be used if the data components are not aligned to the native memory unit of the underlying type.
	 * 
	 * @tparam T The logical component type
	 * @tparam U The component type in the underlying data array
	 * @tparam C The number of components
	 */
	template <typename T, typename U, std::size_t C>
	class Aligned : public Reader<T>
	{
		public:
			/**
			 * Destructor.
			 */
			inline virtual ~Aligned() noexcept override = default;

			// Override Abstract accessor methods

			/**
			 * Gets the number of array elements in this accessor.
			 * 
			 * @return The number of array elements
			 */
			inline virtual unsigned long long size() const noexcept override
			{
				return mSize;
			}

			/**
			 * Gets the fixed number of element components in this accessor.
			 * 
			 * @return The number of components
			 */
			inline virtual std::size_t components() const noexcept override
			{
				return C;
			}

			/**
			 * Interprets whether this accessor is usable.
			 * The Aligned accessor returns true if the data pointer is set and the size is nonzero.
			 *
			 * @return Whether the accessor is usable
			 */
			inline virtual explicit operator bool() const noexcept override
			{
				return mData != nullptr && mSize > 0;
			}

			// Override Reader<T> accessor methods

			/**
			 * Analyzes the accessor configuration and return the most optimized accessor that will give the same answers.
			 * The Aligned accessor can be optimized into the Native accessor if the components are contiguous.
			 *
			 * @return The optimized accessor achieving the same effect
			 */
			inline virtual Accessor<T> optimize() const override
			{
				Accessor<T> acc;
				if (this->contiguous())
				{
					acc.copy(Native<T, U, C>(mData + mOffsets[0], mSize));
				}
				else
				{
					acc.copy(*this);
				}

				return Accessor<T>(*this);
			}

			/**
			 * Gets a single component value at a given array index.
			 *
			 * @param n The array index
			 * @param c The component index
			 * @return The value of the given component at the given index
			 */
			inline virtual std::remove_cv_t<T> get(unsigned long long n, std::size_t c) const noexcept override
			{
				return static_cast<std::remove_cv_t<T>>(mData[mOffsets[c] + n * mStrides[c]]);
			}

			/**
			 * Reads all components of a single data element into a sequential data buffer starting at the given array index.
			 * The Aligned implementation directly accesses the given array index element using the offset and stride.
			 *
			 * @param buffer The buffer to read into
			 * @param n The array index to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long n) const noexcept override
			{
				std::remove_const_t<T> *output = buffer;
				for (unsigned c = 0; c < C; ++c)
				{
					*output++ = static_cast<std::remove_const_t<T>>(mData[mOffsets[c] + n * mStrides[c]]);
				}
				return output;
			}

			/**
			 * Reads all components of a given count of data elements into a sequential data buffer starting at the given array index.
			 * The Aligned implementation directly accesses the each array index element using the offset and stride.
			 *
			 * @param buffer The buffer to read into
			 * @param noff The first array index to read
			 * @param count The number of array elements to read
			 * @return The pointer to the next byte in the data buffer after all modified bytes
			 */
			inline virtual std::remove_const_t<T> *retrieve(std::remove_const_t<T> *buffer, unsigned long long noff, unsigned long long count) const noexcept override
			{
				std::remove_const_t<T> *output = reinterpret_cast<std::remove_const_t<T> *>(buffer);
				for (unsigned long long n = 0; n < count; ++n)
				{
					for (unsigned c = 0; c < C; ++c)
					{
						*output++ = static_cast<std::remove_const_t<T>>(mData[mOffsets[c] + (n + noff) * mStrides[c]]);
					}
				}
				return output;
			}

			// Methods unique to Aligned<T, C>

			/**
			 * Sets the offset and stride for a given component.
			 * 
			 * @param c The component index
			 * @param offset The initial element offset in units of T
			 * @param stride The number of elements between adjacent array index values of this component
			 */
			inline void setup(std::size_t c, std::size_t offset, std::ptrdiff_t stride) noexcept
			{
				mOffsets[c] = offset;
				mStrides[c] = stride;
			}

			/**
			 * Gets the offset for the component.
			 * 
			 * @param c The component index
			 * @return The initial element offset in units of T
			 */
			inline std::size_t offset(std::size_t c) const noexcept
			{
				return mOffsets[c];
			}

			/**
			 * Gets the stride for the component.
			 *
			 * @param c The component index
			 * @return The number of elements between adjacent array index values of this component
			 */
			inline std::ptrdiff_t stride(std::size_t c) const noexcept
			{
				return mOffsets[c];
			}

			/**
			 * Sets the data array and array index count.
			 * 
			 * @param data The data array
			 * @param size The number of array elements
			 */
			inline void bind(U *data, std::size_t size) noexcept
			{
				mData = data;
				mSize = size;
			}

			/**
			 * Checks whether all components are packed such that they are adjacent to each other.
			 * This is true if the offsets are sequential, and each component has the same stride.
			 * There may still be gaps between array elements.
			 *
			 * @return Whether the components are adjacent
			 */
			inline bool adjacent() const noexcept
			{
				bool matched = true;

				for (std::size_t i = 1; i < C; ++i)
				{
					if (mOffsets[i] - mOffsets[i - 1] != 1u)
					{
						matched = false;
						break;
					}

					if (mStrides[i] != mStrides[i - 1])
					{
						matched = false;
						break;
					}
				}

				return matched;
			}

			/**
			 * Checks whether this accessor describes a contiguous accessor that could be implemented using Native accessor.
			 * This is true if the offsets are sequential and the strides all match C so there are no gaps between array elements.
			 * 
			 * @return Whether this accessor is a contiguous accessor
			 */
			inline bool contiguous() const noexcept
			{
				return this->adjacent() && mStrides[0] == C;
			}

			/**
			 * Gets the underlying data array.
			 * 
			 * @return The data array
			 */
			inline U *data() noexcept
			{
				return mData;
			}

			/**
			 * Gets the underlying data array.
			 *
			 * @return The data array
			 */
			inline const U *data() const noexcept
			{
				return mData;
			}

		private:
			/** The initial offset (in units of T) for each component */
			std::size_t mOffsets[C] = { };

			/** The spacing (in units of T) between the adjacent array elements of each component */
			std::ptrdiff_t mStrides[C] = { };

			/** The number of array index elements */
			std::size_t mSize = 0;

			/** The array data pointer */
			U *mData = nullptr;
	};

	/** 
	 * Convenience subclass for a native aligned accessor that doesn't need casting.
	 *
	 * @tparam T The component type
	 * @tparam C The component count
	 */
	template <typename T, std::size_t C>
	using AlignedT = Aligned<T, T, C>;
}

#endif
