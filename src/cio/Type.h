/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TYPE_H
#define CIO_TYPE_H

#include "Types.h"

#include <complex>
#include <iosfwd>

namespace cio
{
	/**
	 * The CIO Type enumeration describes the primitive logical value types available to use for binary representation and text parsing.
	 * The logical types do not mandate a particular physical encoding since that can be data and format dependent.
	 *
	 * The first set of values describe baseline logical types of Boolean, Integer, Unsigned, Real, Scalar, Text, and Blob.
	 * Type also describes commonly used composite types Vec2, Vec3, Vec4, Mat2, Mat3, Mat4, and generalized Vector, Matrix, and Tensor types.
	 * Finally, the type of "Any" implies a variant or polymorphic type which can be any type or composition of types.
	 */
	enum class Type : std::uint8_t
	{
		/** No value type has been assigned */
		None = 0,

		/** The value must logically be either true or false */
		Boolean,

		/** The value must be a integer */
		Integer,
		
		/** The value must be an unsigned non-negative integer */
		Unsigned,

		/** The value must be a real number, typically represented by floating point or fixed point values */
		Real,
		
		/** The value is an enumeration reference of unspecified type */
		Enum,

		/** The value must be natural language text, typically represented by UTF-8 strings */
		Text,

		/** The value is a general binary sequence that represents some encoded data format */
		Blob,

		/** The value is a structured objects with key-value subfields */
		Object,

		/** The value may be any value or composition of primitive values via a Variant */
		Any
	};

	/**
	 * Gets the text label of the Type enumeration.
	 *
	 * @param prim The primitive enumeration value
	 * @return the text representation
	 */
	CIO_API const char *print(Type prim) noexcept;

	/**
	 * Prints the text label of the Type enumeration.
	 *
	 * @param prim The primitive enumeration value
	 * @param buffer The buffer to print to
	 * @param capacity The number of characters available in the buffer
	 * @return the number of bytes actually needed for the label
	 */
	CIO_API std::size_t print(Type prim, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Parses the given text to identify the type from its text label.
	 * 
	 * @param text The text
	 * @param prim The type to update
	 * @return the parse status
	 */
	CIO_API TextParseStatus parse(const cio::Text &text, Type &prim) noexcept;

	/**
	 * Prints a Type enumeration value to a C++ stream.
	 *
	 * @param s The stream
	 * @param prim The primitive
	 * @return the stream
	 * @throw std::exception If the C++ stream threw an exception to indicate failure
	 */
	CIO_API std::ostream &operator<<(std::ostream &s, Type prim);
	
	/**
	 * Gets the default component type for the given primitive type.
	 *
	 * @param prim The primitive type
	 * @return the default component type
	 */
	CIO_API Primitive primitive(Type prim) noexcept;

	/**
	 * The Type Value template selector is used to map any C++ type T to the best matching primitive type.
	 * The template declares an inner "type" field with the proper std::integral_constant.
	 * @tparam <T> The C++ type to map to a Type enumeration
	 */
	template <typename T>
	struct TypeValue;

	/**
	 * The DefaultPrimitiveType template selector is used to map compile-time Type enumeration values to the C++ type
	 * most useful for representing element values of that primitve.
	 * The template declares an inner "type" field with the proper C++ type.
	 * For example, for Type::Boolean the type field will be "bool".
	 *
	 * @tparam <E> The compile-time enumeration value
	 */
	template <Type E>
	struct DefaultPrimitiveType;

	/**
	 * Gets the compile-time std::integral_constant value of the Type enumeration that best represents the C++ type T.
	 *
	 * @tparam <T> The C++ type of interest
	 * @return the Type enumeration value best matching that type
	 */
	template <typename T>
	inline typename TypeValue<T>::type primitive();

	/**
	 * The DefaultPrimitive template alias is used to get the default C++ type for implementing the given Type type.
	 *
	 * @tparam <T> The Type enumeration value
	 */
	template <Type E>
	using DefaultPrimitive = typename DefaultPrimitiveType<E>::type;
}

/* Inline implementation */

namespace cio
{
	template <typename T>
	inline typename TypeValue<T>::type primitive()
	{
		return typename TypeValue<T>::type();
	}

	/* General template */

	template <typename T>
	struct TypeValue
	{
		using type = std::integral_constant<Type, Type::Blob>;
	};

	template <>
	struct TypeValue<bool>
	{
		using type = std::integral_constant<Type, Type::Boolean>;
	};

	template <>
	struct TypeValue<char>
	{
		using type = std::integral_constant<Type, Type::Text>;
	};

	template <>
	struct TypeValue<const char *>
	{
		using type = std::integral_constant<Type, Type::Text>;
	};

	template <>
	struct TypeValue<wchar_t>
	{
		using type = std::integral_constant<Type, Type::Text>;
	};

	template <>
	struct TypeValue<unsigned char>
	{
		using type = std::integral_constant<Type, Type::Unsigned>;
	};

	template <>
	struct TypeValue<signed char>
	{
		using type = std::integral_constant<Type, Type::Integer>;
	};

	template <>
	struct TypeValue<short>
	{
		using type = std::integral_constant<Type, Type::Integer>;
	};

	template <>
	struct TypeValue<unsigned short>
	{
		using type = std::integral_constant<Type, Type::Unsigned>;
	};

	template <>
	struct TypeValue<int>
	{
		using type = std::integral_constant<Type, Type::Integer>;
	};

	template <>
	struct TypeValue<unsigned>
	{
		using type = std::integral_constant<Type, Type::Unsigned>;
	};

	template <>
	struct TypeValue<long>
	{
		using type = std::integral_constant<Type, Type::Integer>;
	};

	template <>
	struct TypeValue<unsigned long>
	{
		using type = std::integral_constant<Type, Type::Unsigned>;
	};

	template <>
	struct TypeValue<long long>
	{
		using type = std::integral_constant<Type, Type::Integer>;
	};

	template <>
	struct TypeValue<unsigned long long>
	{
		using type = std::integral_constant<Type, Type::Unsigned>;
	};

	template <>
	struct TypeValue<float>
	{
		using type = std::integral_constant<Type, Type::Real>;
	};

	template <>
	struct TypeValue<double>
	{
		using type = std::integral_constant<Type, Type::Real>;
	};

	template <>
	struct TypeValue<long double>
	{
		using type = std::integral_constant<Type, Type::Real>;
	};

	template <typename T>
	struct TypeValue<std::complex<T>>
	{
		using type = typename TypeValue<T>::type;
	};

	template <>
	struct DefaultPrimitiveType<Type::None>
	{
		using type = void;
	};

	template <>
	struct DefaultPrimitiveType<Type::Boolean>
	{
		using type = bool;
	};

	template <>
	struct DefaultPrimitiveType<Type::Text>
	{
		using type = char;
	};

	template <>
	struct DefaultPrimitiveType<Type::Integer>
	{
		using type = std::int32_t;
	};
	
	template <>
	struct DefaultPrimitiveType<Type::Unsigned>
	{
		using type = std::uint32_t;
	};
	
	template <>
	struct DefaultPrimitiveType<Type::Real>
	{
		using type = float;
	};

	template <>
	struct DefaultPrimitiveType<Type::Blob>
	{
		using type = std::uint8_t;
	};
	
	template <>
	struct DefaultPrimitiveType<Type::Any>
	{
		using type = Variant;
	};
}

#endif
