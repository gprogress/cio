/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Control.h"

#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		struct ControlDefinition {
			Control code;
			const char *text;
		};

		const ControlDefinition sControlDefs[] = {
			{ Control::None, "None" },
			{ Control::Disconnect, "Disconnect"},
			{ Control::Reset, "Reset" },
			{ Control::SetScanParameters,  "Set Scan Parameters"},
			{ Control::Scan, "Scan" },
			{ Control::LowEnergyCreateConnection, "Low Energy Create Connection"},
			{ Control::LowEnergyCancelConnection, "Low Energy Cancel Connection"},
			{ Control::LowEnergyUpdateConnection, "Low Energy Update Connection"},
			{ Control::Encrypt, "Encrypt"},
			{ Control::Rand, "Random Number"},
			{ Control::StartEncryption, "Start Encryption"},
			{ Control::ProvideLongTermKey, "Provide Long Term Key"},
			{ Control::DenyLongTermKey, "Deny Long Term Key"},
		};

		const char *print(Control code) noexcept
		{
			const char *text = "Unknown";

			// Binary search - defs are sorted in order of opcode
			std::size_t low = 0;
			std::size_t high = sizeof(sControlDefs) / sizeof(ControlDefinition);
			std::size_t current = (low + high) / 2;
			while (low < high)
			{
				if (sControlDefs[current].code == code)
				{
					text = sControlDefs[current].text;
					break;
				}
				else if (sControlDefs[current].code < code)
				{
					low = current + 1;
				}
				else
				{
					high = current;
				}
			}

			return text;
		}

		std::ostream &operator<<(std::ostream &s, Control code)
		{
			return s << print(code);
		}
	}
}
