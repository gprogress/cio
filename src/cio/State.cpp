/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "State.h"

#include <cio/Print.h>

#include <algorithm>
#include <cstdio>
#include <cstring>
#include <ostream>

namespace cio
{
	std::size_t State::print(char *buffer, std::size_t length) const noexcept
	{
		char temp[128] = { };
		char *text = (length < 128) ? temp : buffer;

		auto &type = enumeration(this->reason);
		
		const char *actionText = cio::print(this->action);
		const char *statusText = cio::print(this->status);
		const char *outcomeText = cio::print(this->outcome);
		const char *reasonText = cio::print(this->reason);
		
		std::snprintf(text, 128, "Action: %s, Status: %s, Outcome: %s, Reason: %s", actionText, statusText, outcomeText, reasonText);
		std::size_t actual = std::strlen(text);
		
		if (text != buffer)
		{
			std::size_t toCopy = std::min(length, actual);
			std::memcpy(buffer, text, toCopy);
		}
		
		if (actual < length)
		{
			std::memset(buffer + actual, 0, length - actual);
		}
		
		return actual;
	}
			
	std::string State::print() const
	{
		char buffer[128];
		std::size_t actual = this->print(buffer, 128);
		return std::string(buffer, buffer + actual);
	}
	
	std::ostream &operator<<(std::ostream &stream, const State &state)
	{
		char buffer[128];
		std::size_t actual = state.print(buffer, 128);
		stream.write(buffer, actual);
		return stream;
	}
}

