/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_TEST_SUITE_H
#define CIO_TEST_SUITE_H

#include "Types.h"

#include "Test.h"

#include <memory>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace test
	{
		/**
		 * The Suite class provides an implementation of Suite which organizes test cases into a hierarchy and execution sequence.
		 * The suite succeeds if all test cases on it succeeds.
		 *
		 * Test cases are organized into sequence buckets where every test in a lower sequence ID is executed before any test in a higher sequence.
		 * Test cases in the same sequence bucket may be executed in any order, including in parallel.
		 * This suite's setUp() and tearDown() are called once per sequence bucket rather than once per test case.
		 */
		class CIO_TEST_API Suite : public Test
		{
			public:
				/**
				 * Gets the metaclass for Suite.
				 *
				 * @return the metaclass for Suite
				 */
				static const Metaclass &getDeclaredMetaclass();

				/**
				 * Construct the empty test.
				 */
				Suite() noexcept;

				/**
				 * Construct a test labeled with the given resource path.
				 * The test short name is taken from the leaf filename of the path.
				 *
				 * @param path The resource path for the test
				 */
				explicit Suite(Text path);

				/**
				 * Construct a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Suite(const Suite &in);

				/**
				 * Construct a test by transferring the input of a given test.
				 *
				 * @param in The test to move
				 */
				Suite(Suite &&in) noexcept;

				/**
				 * Assign a copy of a test.
				 *
				 * @param in The test to copy
				 */
				Suite &operator=(const Suite &in);

				/**
				 * Transfer the contents of a test into this one.
				 *
				 * @param in The test to move
				 */
				Suite &operator=(Suite &&in) noexcept;

				/**
				 * Destructor.
				 */
				virtual ~Suite() noexcept  override;

				/**
				 * Gets the metaclass for the runtime type of this Suite class or subclass.
				 * This must be overridden by subclasses to enable polymorphic cloning and transfers.
				 *
				 * @return the metaclass for the actual runtime type of this object
				 */
				virtual const Metaclass &getMetaclass() const noexcept  override;

				/**
				 * Clears all state on this object and restores it to as if it had been default constructed.
				 */
				virtual void clear() noexcept  override;

				/**
				 * Executes the given test, reporting faults to the given logger.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed as a standalone test, so subclasses that need context will need to override this method to set it up.
				 *
				 * The Suite implementation executes each test case sequence (if any) in order, waiting for all tests in a lower sequence bucket to finish
				 * before starting the next sequence bucket. setUp() and tearDown() are called once per sequence bucket at the start and end respectively.
				 * Tests within the same sequence bucket may be executed in any order, including in parallel.
				 * All test cases are executed using this Suite as the context.
				 * If a particular sequence fails, all sequences after it will report all tests as skipped.
				 *
				 * Subclasses may override this further to either provide tests for the Suite itself, or to change the behavior entirely.
				 *
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity execute(Reporter &reporter) override;

				/**
				 * Executes the given test, reporting faults to the given logger.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed as a standalone test, so subclasses that need context will need to override this method to set it up.
				 *
				 * The Suite implementation executes each test case sequence (if any) in order, waiting for all tests in a lower sequence bucket to finish
				 * before starting the next sequence bucket. setUp() and tearDown() are called once per sequence bucket at the start and end respectively.
				 * Tests within the same sequence bucket may be executed in any order, including in parallel.
				 * All test cases are executed using a context stack of the given context followed by this Suite as the full context.
				 * If a particular sequence fails, all sequences after it will report all tests as skipped.
				 *
				 * Subclasses may override this further to either provide tests for the Suite itself, or to change the behavior entirely.
				 *
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity executeInContext(Test *context, Reporter &reporter) override;

				/**
				 * Executes the given test, reporting faults to the given logger.
				 * It is up to the overriding subclass as to whether faults are thrown as exceptions or logged iteratively during the test.
				 * The test is executed in the full context of the entire stack of tests being executed.
				 * Most tests will not need the full context and can just execute the test in isolation.
				 *
				 * The Suite implementation executes each test case sequence (if any) in order, waiting for all tests in a lower sequence bucket to finish
				 * before starting the next sequence bucket. setUp() and tearDown() are called once per sequence bucket at the start and end respectively.
				 * Tests within the same sequence bucket may be executed in any order, including in parallel.
				 * All test cases are executed using a context stack of the given context stack followed by this Suite as the full context.
				 * If a particular sequence fails, all sequences after it will report all tests as skipped.
				 *
				 * @param context The array of test contexts to run this test in
				 * @param depth The test context depth
				 * @param reporter The logger to report faults to
				 * @return the highest severity of any faults encountered
				 */
				virtual Severity executeInFullContext(Test **context, std::size_t depth, Reporter &reporter) override;

				/**
				 * Finds the test case for the given URI. If empty, returns this test case.
				 * This override first checks to see if the URI matches the suite as a whole, and then if not checks
				 * each test case until it finds one.
				 *
				 * @param uri The URI of the test to find
				 * @return the found test case, or nullptr if it did not exist
				 */
				virtual Test *find(const Text &uri) noexcept override;

				/**
				 * Adds a test case temporary or local value to this test suite. Memory is allocated for the test and then it is moved into the new memory via cio::moveIntoNewInstance.
				 * Whenever this suite is copied, the referenced test is cloned using cio::clone (the Test subclass must implement getMetaclass() if derived state should be copied).
				 * The test sequence ID controls the order of execution. All tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @param test The test to reference
				 * @param sequenceId the sequence ID for the test
				 */
				Test &addTestCase(Test &&test, unsigned sequenceId = 0);

				/**
				 * Adds a preallocated test case to this test suite and take ownership of it.
				 * Whenever this suite is copied, the referenced test is cloned using cio::clone (the Test subclass must implement getMetaclass() if derived state should be copied).
				 * The test sequence ID controls the order of execution. All tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @param test The test to reference
				 * @param sequenceId the sequence ID for the test
				 */
				Test &addTestCase(std::unique_ptr<Test> test, unsigned sequenceId = 0);

				/**
				 * Adds a test case to execute a test method as a test case.
				 * Internally, a new Method test is created and added to this suite. When run, it will execute the method on an object of type T, and catch and report all Faults and exceptions.
				 * If T is a subclass of Test and this class is an instance of T, it will be used to supply the "this" argument to the Method test.
				 * Otherwise, the Method test will always create a new instance of T to use for its test.
				 *
				 * The test sequence ID controls the order of execution. All tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @param method The pointer to test class member method to run as a test
				 * @param name The name of the method, which generally should be the name of the class method being tested
				 * @param sequenceId The sequence ID for the test
				 * @return the added test case
				 */
				template <typename T, typename R>
				Test &addTestCase(const char *name, R(T::*method)(), unsigned sequenceId = 0);

				/**
				 * Adds a test case to execute a test method as a test case.
				 * Internally, a new Method test is created and added to this suite. When run, it will execute the method on an object of type T, and catch and report all Faults and exceptions.
				 * If T is a subclass of Test and this class is an instance of T, it will be used to supply the "this" argument to the Method test.
				 * Otherwise, the Method test will always create a new instance of T to use for its test.
				 *
				 * The test sequence ID controls the order of execution. All tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @param method The pointer to test class member method to run as a test
				 * @param name The name of the method, which generally should be the name of the class method being tested
				 * @param sequenceId The sequence ID for the test
				 * @return the added test case
				 */
				template <typename T, typename R>
				Test &addTestCase(std::string name, R(T:: *method)(), unsigned sequenceId = 0);

				/**
				 * Adds a test case that is already a subclass of cio::Test, returned the instance of it moved into this suite.
				 * 
				 * @tparam T The subclass type
				 * @param test The test case to move into this suite
				 * @param sequenceId The sequence ID for the test
				 * @return the moved test case added to this suite
				 */
				template <typename T>
				T &addTestCase(T &&test, unsigned sequenceId = 0);

				/**
				 * Adds a test case that is already a subclass of cio::Test, returned the instance of it moved into this suite.
				 *
				 * @tparam T The subclass type
				 * @param test The test case to move into this suite
				 * @param sequenceId The sequence ID for the test
				 * @return the moved test case added to this suite
				 */
				template <typename T>
				T &addTestCase(std::unique_ptr<T> &&test, unsigned sequenceId = 0);

				/**
				 * Adds a test case to this test suite without taking ownership of it.
				 * It is the responsibility of the caller to ensure the test is not deallocated while this suite references it.
				 * Whenever this suite is copied, the referenced test is not copied; the new test will also reference it.
				 * The test sequence ID controls the order of execution. All tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @param test The test to reference
				 * @param sequenceId the sequence ID for the test
				 * @return the reference to the test case added to this suite
				 */
				Test &referenceTestCase(Test *test, unsigned sequenceId = 0);

				/**
				 * Gets the number of test case sequences defined for this test.
				 * Test cases are sorted by sequence ID, where all tests of a lower sequence ID must be executed before any tests in a higher sequence ID.
				 * Test cases with the same sequence ID may be executed in any order, including in parallel.
				 *
				 * @return the number of test case sequences
				 */
				std::size_t getTestCaseSequenceCount() const;

				/**
				 * Gets all of the test cases for this suite, ordered into buckets by sequence ID.
				 * All tests in each bucket should finish before starting any tests in the next bucket.
				 * Tests within a bucket can be executed in any order, including in parallel.
				 *
				 * @return all test cases bucketed by sequence ID
				 */
				std::vector<std::vector<Test *>> &getTestCases();

				/**
				 * Gets the test cases for a particular sequence ID.
				 * Tests within this bucket may be executed in any order, including in parallel.
				 *
				 * @param sequenceId The sequence ID
				 * @return the test cases for that sequence ID
				 * @throw std::out_of_range If the sequence ID is higher than any valid test case sequence
				 */
				std::vector<Test *> &getTestCases(unsigned sequenceId);

				/**
				 * Gets all of the test cases for this suite read-only, ordered into buckets by sequence ID.
				 * All tests in each bucket should finish before starting any tests in the next bucket.
				 * Tests within a bucket can be executed in any order, including in parallel.
				 * *
				 * @return all test cases bucketed by sequence ID
				*/
				const std::vector<std::vector<Test *>> &getTestCases() const;

				/**
				 * Gets the test cases for a particular sequence ID read-only.
				 * Tests within this bucket may be executed in any order, including in parallel.
				 *
				 * @param sequenceId The sequence ID
				 * @return the test cases for that sequence ID
				 * @throw std::out_of_range If the sequence ID is higher than any valid test case sequence
				 */
				const std::vector<Test *> &getTestCases(unsigned sequenceId) const;

				/**
				 * Removes all test cases from this suite.
				 * Any owned test cases will also be deallocated and destroyed.
				 */
				void clearTestCases();

				/**
				 * Removes all test cases from the given test case sequence bucket.
				 * Any owned test cases used in this sequence bucket will also be deallocated and destroyed.
				 * If the given sequence ID is not a valid sequence ID, then nothing happens.
				 * After removal, any empty sequence buckets at the end of the test sequence will also be removed.
				 *
				 * 	@param sequenceId The sequence ID
				 */
				void clearTestCases(unsigned sequenceId);

				/**
				 * Performs the setUp() for this Suite, capturing any failures or exceptions and reporting them as test issues.
				 *
				 * @param reporter The reporter to use to log failures in setUp()
				 * @return the severity returned by setup, or Severity::Critical if setUp() threw an exception
				 */
				Severity setUpTestCaseSequence(Reporter &reporter) noexcept;

				/**
				 * Executes a single test case as a standalone tests in the calling thread.
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 *
				 * @pre this->setUp() must have been called and succeeded before calling this method
				 *
				 * @note While it's intended that the test case set is one of this suite's test cases, it will still work if it isn't
				 *
				 * @param reporter The test result reporter
				 * @param test The test case
				 * @return the test severity result
				 */
				Severity executeTestCase(Reporter &reporter, Test &test) noexcept;

				/**
				 * Executes the given test case set as standalone tests.
				 * Before the start of the sequence, this suite's setUp() method is called and if it throws, the sequence will stop and all tests will be logged as "skipped".
				 * Each of the tests is then executed using this suite's execution strategy (currently only sequential in current thread).
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 * After the entire sequence executes or is skipped, this suite's tearDown() method is called.
				 *
				 * @note While it's intended that the test case set is one of this suite's test case sequence buckets, it will still work if it isn't
				 *
				 * @param reporter The test result reporter
				 * @param tests The set of test cases, which may be executed in any order or even in parallel
				 * @return the worst severity result of the setUp() method and any test results
				 */
				Severity executeTestCaseSet(Reporter &reporter, const std::vector<Test *> &tests) noexcept;

				/**
				 * Executes a single test case set with the given context in the calling thread.
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 *
				 * @pre this->setUp() must have been called and succeeded before calling this method
				 *
				 * @note While it's intended that the test case set is one of this suite's test cases, it will still work if it isn't
				 *
				 * @param context The context
				 * @param reporter The test result reporter
				 * @param test The test case
				 * @return the test severity result
				 */
				Severity executeTestCaseInContext(Test *context, Reporter &reporter, Test &test) noexcept;

				/**
				 * Executes the given test case set in the given parent test context (typically this suite but it doesn't have to be).
				 * Before the start of the sequence, this suite's setUp() method is called and if it throws, the sequence will stop and all tests will be logged as "skipped".
				 * Each of the tests is then executed using this suite's execution strategy (currently only sequential in current thread).
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 * After the entire sequence executes or is skipped, this suite's tearDown() method is called.
				 *
				 * @note While it's intended that the test case set is one of this suite's test case sequence buckets, it will still work if it isn't
				 *
				 * @param context The context to supply to all of the test cases
				 * @param reporter The test result reporter
				 * @param tests The set of test cases, which may be executed in any order or even in parallel
				 * @return the worst severity result of the setUp() method and any test results
				 */
				Severity executeTestCaseSetInContext(Test *context, Reporter &reporter, const std::vector<Test *> &tests) noexcept;

				/**
				 * Executes a single test case set with the given full context stack in the calling thread.
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 *
				 * @pre this->setUp() must have been called and succeeded before calling this method
				 *
				 * @note While it's intended that the test case set is one of this suite's test cases, it will still work if it isn't
				 *
				 * @param context The array of contexts to supply to all of the test cases
				 * @param depth The number of parent test contexts
				 * @param reporter The test result reporter
				 * @param test The test case
				 * @return the test severity result
				 */
				Severity executeTestCaseInFullContext(Test **context, std::size_t depth, Reporter &reporter, Test &test) noexcept;

				/**
				 * Executes the given test case set in the given full test context stack of parent tests (typically the context ending in this suite but it doesn't have to be).
				 * Before the start of the sequence, this suite's setUp() method is called and if it throws, the sequence will stop and all tests will be logged as "skipped".
				 * Each of the tests is then executed using this suite's execution strategy (currently only sequential in current thread).
				 * Any test case that throws a Fault or exception will be marked as a failure, in addition to any failures logged by the test case itself.
				 * After the entire sequence executes or is skipped, this suite's tearDown() method is called.
				 *
				 * @note While it's intended that the test case set is one of this suite's test case sequence buckets, it will still work if it isn't
				 *
				 * @param context The array of contexts to supply to all of the test cases
				 * @param depth The number of parent test contexts
				 * @param reporter The test result reporter
				 * @param tests The set of test cases, which may be executed in any order or even in parallel
				 * @return the worst severity result of the setUp() method and any test results
				 */
				Severity executeTestCaseSetInFullContext(Test **context, std::size_t depth, Reporter &reporter, const std::vector<Test *> &tests) noexcept;

			private:
				/** Metaclass for this class */
				static const Class<Suite> sMetaclass;
			
				/**
				 * Helper method to copy the test case sequence and to deep copy any owned tests and update the pointers for them on this test.
				 *
				 * @param in The test case to copy
				 */
				void copyTestCases(const Suite &in);

				/** The list of test cases uniquely owned by this Suite */
				std::vector<std::unique_ptr<Test>> mOwnedTestCases;

				/** The list of test case sequence buckets, with the tests being referenced in each bucket */
				std::vector<std::vector<Test *>> mTestCases;
		};
	}
}

#include "Method.h"

/* Inline implementation */
namespace cio
{
	namespace test
	{
		template <typename T, typename R>
		Test &Suite::addTestCase(const char *name, R(T:: *method)(), unsigned sequenceId)
		{
			Method<T, R(T::*)()> testCase(this->getName(), name, method);
			return this->addTestCase(std::move(testCase), sequenceId);
		}

		template <typename T, typename R>
		Test &Suite::addTestCase(std::string name, R(T:: *method)(), unsigned sequenceId)
		{
			Method<T, R(T::*)()> testCase(this->getName(), std::move(name), method);
			return this->addTestCase(std::move(testCase), sequenceId);
		}

		template <typename T>
		T &Suite::addTestCase(T &&test, unsigned sequenceId)
		{
			std::unique_ptr<T> allocated(cio::moveIntoNewInstance(std::move(test)));
			T *value = allocated.get();
			this->addTestCase(std::move(allocated), sequenceId);
			return *value;
		}

		template <typename T>
		T &Suite::addTestCase(std::unique_ptr<T> &&test, unsigned sequenceId)
		{
			T *value = test.get();
			std::unique_ptr<Test> upcast(std::move(test));
			this->addTestCase(std::move(upcast), sequenceId);
			return *value;
		}
	}
}

/**
 * The CIO_TEST_METHOD macro is a convenience macro for configurating test cases on test suites
 * implemented as structured methods on the test suite.
 * 
 * This macro presupposes you are testing a method on a target unit test class whose name is METHOD,
 * and the actual suite method implementing the test case is on a test suite KLASS and is named $(METHOD)Test.
 * 
 * For example, if you are writing a unit test for the class cio::Directory, the suite itself (KLASS) would be cio::DirectoryTest.
 * If you were testing the cio::Directory::open() method, then the test case name (METHOD) would be open and the test itself
 * would be implemented as cio::DirectoryTest::openTest() method.
 * 
 * It's not necessary to use this macro since cio::test::Suite::addTestCase fully supports all possible cases, but
 * it can greatly simplify writing test suites and fixtures when used properly.
 * 
 * @param KLASS The suite with the method to run as a test case
 * @param METHOD The name of the targeted method being tested, which is assumed to have the suffix "Test" on the suite
 */
#define CIO_TEST_METHOD(KLASS, METHOD) this->addTestCase(#METHOD, &KLASS::METHOD##Test)

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
