/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/#
#include "Property.h"

#include <algorithm>
#include <ostream>

namespace cio
{
	namespace bluetooth
	{
		const char *sPropertyText[] = {
			"Broadcast",
			"Read",
			"CommandWrite",
			"Write",
			"Notify",
			"Indicate",
			"CommandSignedWrite",
			"Extended",
			"Unknown"
		};
		
		const char *print(Property value) noexcept
		{
			std::size_t idx = std::min<std::size_t>(static_cast<std::size_t>(value), 8u);
			return sPropertyText[idx];
		}
		
		std::ostream &stream(std::ostream &stream, Property value)
		{
			std::size_t idx = std::min<std::size_t>(static_cast<std::size_t>(value), 8u);
			return stream << sPropertyText[idx];
		}
	}
}
