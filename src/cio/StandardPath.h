/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_FILE_STANDARDPATH_H
#define CIO_FILE_STANDARDPATH_H

#include "Types.h"

#include <iosfwd>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	* The CIO File Standard Path enumeration identifies standard native system file paths with particular logical meanings
	* whose location may vary by operating system or configuration.
	*/
	enum class StandardPath : std::uint8_t
	{
		/** Indicate no standard path is defined */
		None,
				
		/** Application file path */
		Application,
			
		/** Application library path */
		Library,

		/** Current working directory */
		Current,
			
		/** System temporary directory */
		Temporary,
			
		/** Null device path */
		Null,

		/** User home directory */
		Home,
			
		/** User documents directory */
		Documents,
			
		/** User desktop directory */
		Desktop
	};
		
	/**
		* Gets a text representation of the StandardPath enumeration.
		* This is the label of the type itself, not any actual path.
		*
		* @param type The standard path type
		* @return the text for the type
		*/
	CIO_API const char *print(StandardPath type) noexcept;
		
	/**
		* Prints the text for a StandardPath type to a C++ stream.
		*
		* @param stream The C++ stream
		* @param type The type to print
		* @return the C++ stream
		* @throw std::exception If the C++ stream threw an exception to indicate an error
		*/
	CIO_API std::ostream &operator<<(std::ostream &stream, StandardPath type);
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

