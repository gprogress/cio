/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "JsonWriterTest.h"

#include <cio/JsonWriter.h>
#include <cio/Newline.h>

#include <cio/test/Assert.h>

namespace cio
{
	JsonWriterTest::JsonWriterTest() :
		cio::test::Suite("cio/JsonWriter")
	{
		this->addTestCase("JsonWriter", &JsonWriterTest::testDefaultConstruction);
		this->addTestCase("startElement", &JsonWriterTest::testStartElement);
		this->addTestCase("endElement", &JsonWriterTest::testEndElement);
		this->addTestCase("startArray", &JsonWriterTest::testStartArray);
		this->addTestCase("endArray", &JsonWriterTest::testEndArray);
		this->addTestCase(makeWriteValueSubtest());
	}

	JsonWriterTest::~JsonWriterTest() noexcept = default;

	cio::test::Suite JsonWriterTest::makeWriteValueSubtest()
	{
		cio::test::Suite suite("cio/JsonWriter/writeValue");
		suite.addTestCase("bool", &JsonWriterTest::testWriteValueBool);
		suite.addTestCase("Text", &JsonWriterTest::testWriteValueText);
		suite.addTestCase("int", &JsonWriterTest::testWriteValueInt);
		suite.addTestCase("float", &JsonWriterTest::testWriteValueFloat);
		suite.addTestCase("double", &JsonWriterTest::testWriteValueDouble);
		return suite;
	}

	void JsonWriterTest::testDefaultConstruction()
	{
		JsonWriter writer;
		CIO_ASSERT(writer.getWriteBuffer().empty());
		CIO_ASSERT_EQUAL(MarkupEvent(), writer.getCurrentLevel());
		CIO_ASSERT(!writer.getOutput());
		CIO_ASSERT_EQUAL(Newline::None, writer.getNewlineStyle());
		CIO_ASSERT_EQUAL('\t', writer.getIndentChar());
		CIO_ASSERT_EQUAL(1u, writer.getIndentAmount());
		CIO_ASSERT_EQUAL(0u, writer.getCurrentIndentAmount());
	}

	void JsonWriterTest::testStartElement()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test writing a top level value
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(R"J("test": )J", buffer.viewProcessedText());

		// Test writing a subvalue to it
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("subtest"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": )J", buffer.viewProcessedText());

		// Test writing a third level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("third"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": )J", buffer.viewProcessedText());

		// End current level and then write a different subvalue
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null)J", buffer.viewProcessedText());

		// Test writing a second element at third level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("alt"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": )J", buffer.viewProcessedText());

		// End current level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null)J", buffer.viewProcessedText());

		// Start another item at third level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("alt2"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": )J", buffer.viewProcessedText());

		// End third level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null)J", buffer.viewProcessedText());

		// End second level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null })J", buffer.viewProcessedText());

		// Start another item at second level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("sub2"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null }, "sub2": )J", buffer.viewProcessedText());

		// End second level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null }, "sub2": null)J", buffer.viewProcessedText());

		// End top level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null }, "sub2": null })J", buffer.viewProcessedText());

		// Start another item at top level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("top2"));
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null }, "sub2": null }, "top2": )J", buffer.viewProcessedText());

		// End top level
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": { "subtest": { "third": null, "alt": null, "alt2": null }, "sub2": null }, "top2": null)J", buffer.viewProcessedText());

		// Now show we can do this under a document
		writer.clear();
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("doctest"));
		CIO_ASSERT_EQUAL(R"J({ "doctest": )J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testEndElement()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test at top level first
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endElement());
		CIO_ASSERT_EQUAL(R"J()J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J("test": null)J", buffer.viewProcessedText());

		// Test in other contexts
		writer.clear();
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endElement());
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("sub"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue("value"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J({ "test": { "sub": "value")J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(R"J({ "test": { "sub": "value" })J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endElement());
		CIO_ASSERT_EQUAL(R"J({ "test": { "sub": "value" })J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testWriteValueBool()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test writing at top level - this validates formatting
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(true));
		CIO_ASSERT_EQUAL(R"J(true)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(false));
		CIO_ASSERT_EQUAL(R"J(true, false)J", buffer.viewProcessedText());

		writer.clear();

		// Test writing to a document - not allowed
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(false));
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing to an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(true));
		CIO_ASSERT_EQUAL(R"J({ "test": true)J", buffer.viewProcessedText());

		// Can't do it again, element only has one value
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(true));
		CIO_ASSERT_EQUAL(R"J({ "test": true)J", buffer.viewProcessedText());

		// Test writing to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("arr"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(true));
		CIO_ASSERT_EQUAL(R"J({ "test": true, "arr": [ true)J", buffer.viewProcessedText());

		// Write again
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(false));
		CIO_ASSERT_EQUAL(R"J({ "test": true, "arr": [ true, false)J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testWriteValueText()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test writing at top level - this validates formatting
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("value1")));
		CIO_ASSERT_EQUAL(R"J("value1")J", buffer.viewProcessedText());

		// Write null string
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text()));
		CIO_ASSERT_EQUAL(R"J("value1", null)J", buffer.viewProcessedText());

		// Write empty string
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("")));
		CIO_ASSERT_EQUAL(R"J("value1", null, "")J", buffer.viewProcessedText());

		// Validate that append with common character escapes are done properly
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("abc\"\tbah\n'\\/whee")));
		CIO_ASSERT_EQUAL(R"J("value1", null, "", "abc\"\tbah\n'\\/whee")J", buffer.viewProcessedText());

		writer.clear();

		// Test writing to a document - not allowed
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(Text("value1")));
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing to an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("value1")));
		CIO_ASSERT_EQUAL(R"J({ "test": "value1")J", buffer.viewProcessedText());

		// Do it again, should fail since we've already written the element's value
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(Text("suffix")));
		CIO_ASSERT_EQUAL(R"J({ "test": "value1")J", buffer.viewProcessedText());

		// End value
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endValue());

		// Test writing to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("arr"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("e1")));
		CIO_ASSERT_EQUAL(R"J({ "test": "value1", "arr": [ "e1")J", buffer.viewProcessedText());

		// Write another element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text("e2")));
		CIO_ASSERT_EQUAL(R"J({ "test": "value1", "arr": [ "e1", "e2")J", buffer.viewProcessedText());

		// Write null as another array element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(Text()));
		CIO_ASSERT_EQUAL(R"J({ "test": "value1", "arr": [ "e1", "e2", null)J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testWriteValueInt()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test writing at top level - this validates formatting
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(42));
		CIO_ASSERT_EQUAL(R"J(42)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-37337));
		CIO_ASSERT_EQUAL(R"J(42, -37337)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(0));
		CIO_ASSERT_EQUAL(R"J(42, -37337, 0)J", buffer.viewProcessedText());

		writer.clear();

		// Test writing to a document - not allowed
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(7));
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing to an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(7));
		CIO_ASSERT_EQUAL(R"J({ "test": 7)J", buffer.viewProcessedText());

		// Can't do it again, element only has one value
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(8));
		CIO_ASSERT_EQUAL(R"J({ "test": 7)J", buffer.viewProcessedText());

		// Test writing to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("arr"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(9));
		CIO_ASSERT_EQUAL(R"J({ "test": 7, "arr": [ 9)J", buffer.viewProcessedText());

		// Write again to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-10));
		CIO_ASSERT_EQUAL(R"J({ "test": 7, "arr": [ 9, -10)J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testWriteValueFloat()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Note: we carefully select values that do not have roundoff issues
		// Test writing at top level - this validates formatting
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(42.0f));
		CIO_ASSERT_EQUAL(R"J(42.0)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-0.125f));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125)J", buffer.viewProcessedText());

		// Special values should result in null being written

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(float(INFINITY)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(float(-INFINITY)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(float(NAN)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(0.0f));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null, null, 0.0)J", buffer.viewProcessedText());

		writer.clear();

		// Test writing to a document - not allowed
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(42.0f));
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing to an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(42.0f));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0)J", buffer.viewProcessedText());

		// Can't do it again, element only has one value
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(-10.5f));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0)J", buffer.viewProcessedText());

		// Test writing to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("arr"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(0.5f));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0, "arr": [ 0.5)J", buffer.viewProcessedText());

		// Write again to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-0.375f));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0, "arr": [ 0.5, -0.375)J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testWriteValueDouble()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Note: we carefully select values that do not have roundoff issues
		// Test writing at top level - this validates formatting
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(42.0));
		CIO_ASSERT_EQUAL(R"J(42.0)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-0.125));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125)J", buffer.viewProcessedText());

		// Special values should result in null being written

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(double(INFINITY)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(double(-INFINITY)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::fail(Reason::Unsupported), writer.writeValue(double(NAN)));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null, null)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(0.0));
		CIO_ASSERT_EQUAL(R"J(42.0, -0.125, null, null, null, 0.0)J", buffer.viewProcessedText());

		writer.clear();

		// Test writing to a document - not allowed
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(42.0));
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing to an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(42.0));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0)J", buffer.viewProcessedText());

		// Can't do it again, element only has one value
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.writeValue(-10.5));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0)J", buffer.viewProcessedText());

		// Test writing to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("arr"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(0.5));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0, "arr": [ 0.5)J", buffer.viewProcessedText());

		// Write again to an array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-0.375));
		CIO_ASSERT_EQUAL(R"J({ "test": 42.0, "arr": [ 0.5, -0.375)J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testStartArray()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test writing a top level array
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(R"J([)J", buffer.viewProcessedText());

		writer.clear();

		// Test writing an array under a document. This should not be allowed.
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.startArray());
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		// Test writing an array under an element
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(R"J({ "test": [)J", buffer.viewProcessedText());

		// Test multi-level arrays.
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(R"J({ "test": [[)J", buffer.viewProcessedText());

		// Testing writing an array after starting a value, this should fail
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startValue());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Unexpected), writer.startArray());
		CIO_ASSERT_EQUAL(R"J({ "test": [[ )J", buffer.viewProcessedText());
	}

	void JsonWriterTest::testEndArray()
	{
		JsonWriter writer;
		Buffer &buffer = writer.getWriteBuffer();

		// Test ending an array without starting one
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J()J", buffer.viewProcessedText());

		// Start an array then end it
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endArray());
		CIO_ASSERT_EQUAL(R"J([ ])J", buffer.viewProcessedText());

		// Start an array, write one value, then end it
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue("test"));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endArray());
		CIO_ASSERT_EQUAL(R"J([ ], [ "test" ])J", buffer.viewProcessedText());

		// Start an array, write two values, then end it
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startArray());
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(4));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(-5));
		CIO_ASSERT_EQUAL(cio::succeed(), writer.endArray());
		CIO_ASSERT_EQUAL(R"J([ ], [ "test" ], [ 4, -5 ])J", buffer.viewProcessedText());

		// Test ending arrays in various other contexts without starting one

		writer.clear();
		CIO_ASSERT_EQUAL(cio::succeed(), writer.startDocument());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J({)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.startElement("test"));
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J({ "test": )J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.startValue());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J({ "test": )J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.writeValue(true));
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J({ "test": true)J", buffer.viewProcessedText());

		CIO_ASSERT_EQUAL(cio::succeed(), writer.endElement());
		CIO_ASSERT_EQUAL(cio::fail(Reason::Missing), writer.endArray());
		CIO_ASSERT_EQUAL(R"J({ "test": true)J", buffer.viewProcessedText());
	}
}