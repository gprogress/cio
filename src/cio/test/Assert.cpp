/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Assert.h"

#include <cio/Metadata.h>

#include <algorithm>
#include <sstream>

namespace cio
{
	namespace test
	{
		void assertBytesEqual(const void *expected, std::size_t expectedLength, const void *actual, std::size_t actualLength, const Source &source)
		{
			const std::uint8_t *exbytes = reinterpret_cast<const std::uint8_t *>(expected);
			const std::uint8_t *acbytes = reinterpret_cast<const std::uint8_t *>(actual);
			std::size_t toReview = std::min(expectedLength, actualLength);
			std::ostringstream stream;
			bool mismatched = false;
			
			for (std::size_t i = 0; i < toReview; ++i)
			{
				if (exbytes[i] != acbytes[i])
				{
					stream << "First mismatch at byte offset " << i << ": expected " << static_cast<unsigned>(exbytes[i]) << ", actual " << static_cast<unsigned>(acbytes[i]);
					mismatched = true;
					break;
				}
			}
							
			if (expectedLength != actualLength)
			{
				if (mismatched)
				{
					stream << ", ";
				}
				
				stream << "Length mismatch: expected length " << expectedLength << ", actual length " << actualLength;
				mismatched = true;
			}
			
			if (mismatched)
			{
				throw cio::test::Fault(stream.str(), source, Severity::Error);
			}
		}
		
		void assertMetadataEqual(const Metadata &expected, const Metadata &actual, const Source &source)
		{
			if (expected.getLocation() != actual.getLocation() ||
				expected.getMode() != actual.getMode() ||
				expected.getType() != actual.getType() ||
				expected.getLength() != actual.getLength())
			{
				std::ostringstream stream;
				stream << "Metadata mismatch: expected " << expected << ", actual " << actual;
				throw cio::test::Fault(stream.str(), source, Severity::Error);
			}
		}
	}
}

