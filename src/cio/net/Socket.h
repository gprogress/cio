/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_SOCKET_H
#define CIO_NET_SOCKET_H

#include "Types.h"

#include "Datagram.h"

#include <chrono>

#include <cio/Channel.h>
#include <cio/Exception.h>
#include <cio/Progress.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace net
	{
		/**
		 * The Socket provides an abstraction of low-level network sockets provided by operating system facilities.
		 * This encapsulates the main OS-dependent differences for managing sockets so that the maining CIO Network classes can focus
		 * more on semantic value-add operations.
		 *
		 * @note Most users will want to use higher level interfaces such as DatagramChannel, StreamChannel, and StreamServer to implement
		 * structured interfaces for managing addresses and operations.
		 */
		class CIO_NET_API Socket
		{
			public:
				/**
				 * Gets the native OS representation of the invalid socket handle.
				 *
				 * @return the invalid socket
				 */
				static SocketId invalid() noexcept;

				/**
				 * Gets the most recent native error code in the current thread
				 * from the underlying native socket library.
				 *
				 * This is a different OS-level function on different platforms.
				 * The possible error codes and values also vary by platform.
				 *
				 * @return the most recent native error in the calling thread, or 0 if none
				 */
				static int getLastNativeError() noexcept;

				/**
				 * Maps native error codes to CIO State types.
				 * The possible error codes and values vary by platform so this attempts
				 * to normalize them. Multiple native codes may map to the same State.
				 *
				 * @param error The native error code
				 * @return the CIO State that best represents the error, or State::Failure generically
				 */
				static Reason fromNativeError(int error) noexcept;

				/**
				 * Maps the most recent native error in the calling thread (as per getLastNativeError())
				 * to a CIO State (as per fromNativeError(int)).
				 *
				 * @return the CIO State that best represents the most recent native error in the calling thread
				 */
				static Reason mapLastNativeError() noexcept;

				/**
				 * On some platforms, the native socket library requires an initialization call.
				 * This is done automatically by the Socket class when needed.
				 *
				 * However, for performance you can call this before any use of the socket API
				 * to ensure the real setup happens prior to any actual socket use.
				 * It may be called multiple times and only the first call does any real work.
				 * You must call deinitialize() the same number of times as you call initialize().
				 *
				 * On platforms that don't need it, this call has no effect.
				 *
				 * @warning On platforms that need this, it CANNOT be called from global, static, or
				 * shared library initialization routines.
				 *
				 * @return a State indicating the result: Success normally since failures result in an exception being thrown
				 * @throw cio::Exception if library startup failed
				 */
				static State initialize();

				/**
				 * On some platforms, the native socket library requires an initialization call
				 * and must be deinitialized after use is complete.
				 * This is done automatically by the Socket class when needed, but if you manually
				 * called initialize(), then you must call deinitialize() the same number of times.
				 *
				 * On platforms that don't need it, this call has no effect.
				 *
				 * @warning On platforms that need this, it CANNOT be called from global, static, or
				 * shared library initialization routines.
				 */
				static void deinitialize() noexcept;

				/**
				 * Construct the empty socket.
				 */
				Socket() noexcept;

				/**
				 * Construct a socket and adopt the given native OS socket handle.
				 *
				 * @param socketId The socket handle to adopt
				 */
				explicit Socket(SocketId socketId) noexcept;

				/**
				 * Construct a socket allocated with the given parameters and the default protocol for the given address type and channel type.
				 * This calls Socket::initialize() first.
				 * This can be used to create native OS sockets for address families not explicitly supported by CIO.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @throw cio::Exception If the socket allocation failed
				 */
				Socket(AddressFamily af, ChannelType type);

				/**
				 * Construct a socket allocated with the given parameters.
				 * This calls Socket::initialize() first.
				 * This can be used to create native OS sockets for address families or protocol types not explicitly supported by CIO.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @param pf The protocol family (i.e. TCP/IP, UDP/IP) which may be defaulted
				 * @throw cio::Exception If the socket allocation failed
				 */
				Socket(AddressFamily af, ChannelType type, ProtocolFamily pf);
				
				/**
				 * Construct a socket allocated with the given parameters.
				 * This calls Socket::initialize() first.
				 * This can be used to create native OS sockets for address families or protocol types not explicitly supported by CIO.
				 * If additional configuration is needed to support the provided Flow type, that will also be performed.
				 * For example, if Broadcast is specified, the appropriate socket option will be enabled.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @param pf The protocol family (i.e. TCP/IP, UDP/IP) which may be defaulted
				 * @param flow The flow type
				 * @throw cio::Exception If the socket allocation failed
				 */
				Socket(AddressFamily af, ChannelType type, ProtocolFamily pf, Flow flow);

				/**
				 * Construct a socket allocated using the given service parameters.
				 * This calls Socket::initialize() first, then if there is an existing initialized socket, calls clear().
				 * The address family, socket type, and protocol family are taken from the given service description.
				 * If the service specifies a flow type, any necessary setup for that will be done too.
				 * For example, if Broadcast is specified, the appropriate socket option will be enabled.
				 *
				 * @param service The service description
				 * @throw cio::Exception If the socket allocation failed
				 */				
				explicit Socket(const Service &service);
				
				/**
				 * Construct a moved socket.
				 *
				 * @param in The socket to move
				 */
				Socket(Socket &&in) noexcept;

				/**
				 * Assign a moved socket.
				 * If an existing socket is initialized, it will be cleared.
				 *
				 * @param in The socket to move
				 * @return this socket
				 */
				Socket &operator=(Socket &&in) noexcept;

				/**
				 * Destructor. If the socket was initialized, this closes the socket then calls Socket::deinitialize().
				 */
				~Socket() noexcept;

				/**
				 * If the socket was initialized, this closes the socket then calls Socket::deinitialize().
				 */
				void clear() noexcept;

				/**
				 * Gets the native OS socket handle.
				 *
				 * @return the native OS socket handle
				 */
				inline SocketId get() const noexcept;

				/**
				 * Releases the native OS socket handle from this socket.
				 * The caller will own the returned socket and is responsible for closing it.
				 * This socket will no longer reference a native OS socket handle.
				 *
				 * @return the released native OS socket handle
				 */
				SocketId take() noexcept;

				/**
				 * Adopts the given native OS socket handle.
				 * If not the same as the current handle, the current handle will be closed.
				 *
				 * @param socketId The socket handle to adopt
				 */
				void adopt(SocketId socketId) noexcept;

				/**
				 * Performs the low level socket allocation call with the default protocol for the given address type and channel type.
				 * This calls Socket::initialize() first, then if there is an existing initialized socket, calls clear().
				 * This can be used to create native OS sockets for address families not explicitly supported by CIO.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @param pf The protocol family (i.e. TCP/IP, UDP/IP) which may be defaulted
				 * @return the State result of this operation
				 * @throw cio::Exception If the socket allocation failed
				 */
				State allocate(AddressFamily af, ChannelType type);

				/**
				 * Performs the low level socket allocation call.
				 * This calls Socket::initialize() first, then if there is an existing initialized socket, calls clear().
				 * This can be used to create native OS sockets for address families or protocol types not explicitly supported by CIO.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @param pf The protocol family (i.e. TCP/IP, UDP/IP) which may be defaulted
				 * @return the State result of this operation
				 * @throw cio::Exception If the socket allocation failed
				 */
				State allocate(AddressFamily af, ChannelType type, ProtocolFamily pf);
				
				/**
				 * Performs the low level socket allocation call.
				 * This calls Socket::initialize() first, then if there is an existing initialized socket, calls clear().
				 * This can be used to create native OS sockets for address families or protocol types not explicitly supported by CIO.
				 * If additional configuration is needed to support the provided Flow type, that will also be performed.
				 * For example, if Broadcast is specified, the appropriate socket option will be enabled.
				 *
				 * @param af The address family
				 * @param type The socket type (i.e. stream, datagram, sequence)
				 * @param pf The protocol family (i.e. TCP/IP, UDP/IP) which may be defaulted
				 * @param flow The flow type
				 * @return the State result of this operation
				 * @throw cio::Exception If the socket allocation failed
				 */
				State allocate(AddressFamily af, ChannelType type, ProtocolFamily pf, Flow flow);
				
				/**
				 * Performs the low level socket allocation call using the given service parameters.
				 * This calls Socket::initialize() first, then if there is an existing initialized socket, calls clear().
				 * The address family, socket type, and protocol family are taken from the given service description.
				 * If the service specifies a flow type, any necessary setup for that will be done too.
				 * For example, if Broadcast is specified, the appropriate socket option will be enabled.
				 *
				 * @param service The service description
				 * @throw cio::Exception If the socket allocation failed
				 */
				State allocate(const Service &service);

				/**
				 * Binds the socket to a local address, native OS version.
				 * This is necessary for a socket to act as a server to remote clients but can also be done to specify the local interface and port
				 * for outgoing connections too.
				 *
				 * @param address The address to bind to in native OS representation
				 * @param addrlen The number of bytes in the address
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in the socket not being configured to listen on the given interface
				 */
				State bind(const void *address, std::size_t addrlen);

				/**
				 * Binds the socket to a local address specified using the Address class.
				 * This is necessary for a socket to act as a server to remote clients but can also be done to specify the local interface and port
				 * for outgoing connections too.
				 *
				 * @note If both IPv6 and IPv4 addresses are specified, the IPv6 address will be tried first,
				 * then the IPv4 if that does not succeed.
				 *
				 * @param address The address to bind to
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in the socket not being configured to listen on the given interface
				 */
				State bind(const Address &address);

				/**
				 * Binds the socket to a local address specified by the given Service.
				 * This is necessary for a socket to act as a server to remote clients but can also be done to specify the local interface and port
				 * for outgoing connections too.
				 *
				 * @note If both IPv6 and IPv4 addresses are specified, the IPv6 address will be tried first,
				 * then the IPv4 if that does not succeed.
				 *
				 * @param service The service describing the local address
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in the socket not being configured to listen on the given interface
				 */
				State bind(const Service &service);

				/**
				 * Configures the socket to be a passive listener in server mode.
				 * This is necessary for a socket to act as a server to remote clients after the bind() step is performed.
				 * The default connection backlog is used.
				 *
				 * @throw cio::Exception If a failure occurred that resulted in the socket not listening to the bound interface
				 */
				State listen();

				/**
				 * Configures the socket to be a passive listener in server mode.
				 * This is necessary for a socket to act as a server to remote clients after the bind() step is performed.
				 * The specified connection backlog is used, possibly capped to the highest that the OS actually supported.
				 *
				 * @param backlog The connection backlog to use
				 * @throw cio::Exception If a failure occurred that resulted in the socket not listening to the bound interface
				 */
				State listen(std::size_t backlog);

				/**
				 * For a bound stream socket, wait for the next available incoming connection and return it.
				 * This version is for situations where the caller does not care about the remote client address.
				 *
				 * @return the socket that was accepted as a remote client
				 * @throw cio::Exception If a failure occurred that resulted in no connection being accepted
				 */
				Socket accept();

				/**
				 * For a bound stream socket, wait for the next available incoming connection and return it.
				 *
				 * @param address The address to capture from the incoming connection
				 * @param addrlen The address length, which will be updated to reflect the actual length
				 * @return the socket that was accepted as a remote client
				 * @throw cio::Exception If a failure occurred that resulted in no connection being accepted
				 */
				Socket accept(void *address, std::size_t &addrlen);

				/**
				 * For a bound stream socket, wait for the next available incoming connection and return it.
				 *
				 * @param address The address to capture from the incoming connection
				 * @return the socket that was accepted as a remote client
				 * @throw cio::Exception If a failure occurred that resulted in no connection being accepted
				 */
				Socket accept(Address &address);

				/**
				 * Connects the socket to a remote address, native OS version.
				 * For stream-based sockets, this is necessary to connect to a remote server and send or receive data.
				 * For other sockets, this sets the default send address and limits the socket to only receive from that address.
				 *
				 * @param address The address to connect to in native OS representation
				 * @param addrlen The number of bytes in the address
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in no connection being established
				 */
				State connect(const void *address, std::size_t addrlen);

				/**
				 * Connects the socket to a remote address using the Address class.
				 * For stream-based sockets, this is necessary to connect to a remote server and send or receive data.
				 * For other sockets, this sets the default send address and limits the socket to only receive from that address
				 *
				 * @param address The address to connect to
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in no connection being established
				 */
				State connect(const Address &address);

				/**
				 * Connects the socket to a remote address using the Service description.
				 * For stream-based sockets, this is necessary to connect to a remote server and send or receive data.
				 * For other sockets, this sets the default send address and limits the socket to only receive from that address
				 *
				 * @param service The service to connect to
				 * @return the State result of this operation
				 * @throw cio::Exception If a failure occurred that resulted in no connection being established
				 */
				State connect(const Service &service);

				/**
				 * Performs the low-level send operation using the native OS interface.
				 * The socket must be connected to a particular address for this to work.
				 *
				 * @param buffer The data to send to the connected remote recipient
				 * @param length The number of bytes to send from the buffer
				 * @param flags Any flags modifying the send operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually sent
				 */
				Progress<std::size_t> send(const void *buffer, std::size_t length, int flags = 0) noexcept;

				/**
				 * Performs the low-level send operation using the native OS interface.
				 * The socket must be connected to a particular address for this to work.
				 * A buffer is used to provide the input.
				 *
				 * @param buffer The data to send to the connected remote recipient
				 * @param flags Any flags modifying the send operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually sent
				 */
				Progress<std::size_t> send(Buffer &buffer, int flags = 0) noexcept;

				/**
				 * Performs the low-level send operation using the native OS interface to a particular address.
				 *
				 * @param buffer The data to send to the connected remote recipient
				 * @param length The number of bytes to send from the buffer
				 * @param address The address to send to in native OS representation
				 * @param addrlen The number of bytes in the address
				 * @param flags Any flags modifying the send operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually sent
				 */
				Progress<std::size_t> sendTo(const void *buffer, std::size_t length, const void *address, std::size_t addrlen, int flags = 0) noexcept;

				/**
				 * Performs the low-level send operation using the native OS interface to a particular address.
				 * A Datagram is used to provide the data buffer input (based on current remaining bytes of limit minus position) and to
				 * specify the send address. If no send address is specified, the default connected address will be used instead.
				 *
				 * @param datagram The datagram to send
				 * @param flags Any flags modifying the send operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually sent
				 */
				Progress<std::size_t> sendTo(Datagram &datagram, int flags = 0) noexcept;

				/**
				 * Performs the low-level receive operation using the native OS interface.
				 *
				 * @warning For non-stream sockets, this will lose the address of the original sender, and can lose packet data
				 * if the buffer length was not enough to fully capture the datagram. In the latter case, depending on the flags, the result
				 * may actually state more bytes than were available in the buffer with State::Overflow.
				 *
				 * @param buffer The data buffer to receive into from any remote sender
				 * @param length The number of bytes to receive int the buffer
				 * @param flags Any flags modifying the receive operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually received, or possibly more if
				 * the flags were set to report the full datagram size and the buffer wasn't large enough
				 */
				Progress<std::size_t> receive(void *buffer, std::size_t length, int flags = 0) noexcept;

				/**
				 * Performs the low-level receive operation using the native OS interface.
				 * A buffer is used to capture the results.
				 *
				 * @warning For non-stream sockets, this will lose the address of the orginal sender, and can lose packet data
				 * if the buffer length was not enough to fully capture the datagram. In the latter case, depending on the flags, the result
				 * may actually state more bytes than were available in the buffer with State::Overflow.
				 *
				 * @param buffer The data buffer to receive into from any remote sender
				 * @param flags Any flags modifying the receive operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually received, or possibly more if
				 * the flags were set to report the full datagram size and the buffer wasn't large enough
				 */
				Progress<std::size_t> receive(Buffer &buffer, int flags = 0) noexcept;

				/**
				 * Performs the low-level receive operation using the native OS interface, capturing the address of the original sender.
				 *
				 * @warning For non-stream sockets, this can lose packet data if the buffer length was not enough to fully capture the datagram.
				 * In the latter case, depending on the flags, the result may actually state more bytes than were available in the buffer with State::Overflow.
				 * Also, if the address was not long enough for the native address format, the address will be truncated.
				 *
				 * @param buffer The data buffer to receive into from any remote sender
				 * @param length The number of bytes to receive int the buffer
				 * @param address The address to receive the sender into in native OS representation
				 * @param addrlen The number of bytes in the address
				 * @param flags Any flags modifying the receive operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually received, or possibly more if
				 * the flags were set to report the full datagram size and the buffer wasn't large enough
				 */
				Progress<std::size_t> receiveFrom(void *buffer, std::size_t length, void *address, std::size_t &addrlen, int flags = 0) noexcept;

				/**
				 * Performs the low-level receive operation using the native OS interface, capturing the address of the original sender.
				 * This version uses the Datagram class to provide the storage for the buffer and the address.
				 * The buffer's remaining bytes (limit minus position) will be used to receive the data.
				 * The Address on the Datagram will capture the remote sender's address directly if it is IPv4 or IPv6.
				 *
				 * @warning For non-stream sockets, this can lose packet data if the buffer length was not enough to fully capture the datagram.
				 * In the latter case, depending on the flags, the result may actually state more bytes than were available in the buffer with State::Overflow.
				 * Also, if the address was not long enough for the native address format, the address will be truncated.
				 *
				 * @param datagram The datagram to receive
				 * @param flags Any flags modifying the receive operation, as supported by the native OS, or 0 to use the default flags
				 * @return a result describing the success of the send operation and the number of bytes actually received, or possibly more if
				 * the flags were set to report the full datagram size and the buffer wasn't large enough
				 */
				Progress<std::size_t> receiveFrom(Datagram &datagram, int flags = 0) noexcept;

				/**
				 * Sets the receive timeout option on the socket if possible.
				 *
				 * @param timeout The receive timeout in milliseconds
				 * @throw cio::Exception If the timeout could not be set
				 */
				void setReceiveTimeout(std::chrono::milliseconds timeout);
				
				/**
				 * Gets the receive timeout option currently set on the socket.
				 * If this returns 0, no timeout was set.
				 *
				 * @return the receive timeout
				 */
				std::chrono::milliseconds getReceiveTimeout() const;
				
				/**
				 * Sets the send timeout option on the socket if possible.
				 *
				 * @param timeout The receive timeout in milliseconds
				 * @throw cio::Exception If the timeout could not be set
				 */
				void setSendTimeout(std::chrono::milliseconds timeout);
				
				/**
				 * Gets the send timeout option currently set on the socket.
				 * If this returns 0, no timeout was set.
				 *
				 * @return the send timeout
				 */
				std::chrono::milliseconds getSendTimeout() const;

				/**
				 * Sets whether this socket can be used for IPv4 broadcast or similar functionality.
				 * The default value depends on the host operating system.
				 * To be certain, callers should use this to enable broadcast when needed.
				 * 
				 * @param value Whether to enable broadcast
				 */
				void setBroadcastEnabled(bool value);

				/**
				 * Gets whether this socket can be used for IPv4 broadcast or similar functionality.
				 * The default value depends on the host operating system.
				 *
				 * @return Whether broadcast is enabled
				 */
				bool getBroadcastEnabled() const;
				
				/**
				 * Sets a socket option not specifically handled by this class API.
				 * This is basically a passthrough to the low-level setsockopt method.
				 *
				 * Higher-level cio::net::Channel classes and subclasses are encouraged to use this
				 * to implement domain-specific or protocol-specific options.
				 *
				 * @param layer The socket layer code
				 * @param option The option code
				 * @param data The data to use to set the option
				 * @param length The length of the data
				 * @return the State of the operation
				 */
				State setGeneralOption(int layer, int option, const void *data, std::size_t length) noexcept;
				
				/**
				 * Gets a socket option not specifically handled by this class API.
				 * This is basically a passthrough to the low-level getsockopt method.
				 *
				 * Higher-level cio::net::Channel classes and subclasses are encouraged to use this
				 * to implement domain-specific or protocol-specific options.
				 *
				 * @param layer The socket layer code
				 * @param option The option code
				 * @param data The data buffer to receive the option value
				 * @param maxLength The maximum buffer length
				 * @return the actual buffer length of the option value and the State of the operation
				 */
				Progress<std::size_t> getGeneralOption(int layer, int option, void *data, std::size_t maxLength) const noexcept;

				/**
				 * Checks whether this socket is connected.
				 *
				 * @return whether this socket is connected
				 */
				explicit operator bool() const noexcept;

			private:
				/** Class logger */
				static Logger sLogger;
			
				/** The underlying socket ID for the operating system */
				SocketId mSocketId;
		};
	}
}

/* Inline implementation */

namespace cio
{
	namespace net
	{
		inline SocketId Socket::get() const noexcept
		{
			return mSocketId;
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
