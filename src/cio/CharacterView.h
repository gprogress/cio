/*==============================================================================
 * Copyright 2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CHARACTERVIEW_H
#define CIO_CHARACTERVIEW_H

#include "Types.h"

#include "CharacterIterator.h"

#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Character View class provides a non-owning read-only view of a sequence of character encoded
	 * in some representation such as UTF-8 (char), UTF-16 (char16_t) or UCS-4 (char32_t).
	 * 
	 * Unlike std::string, this presents a logical view of the number of Unicode Character instances rather
	 * than a physical view of the number of encoded data values. As a consequence, it is also not
	 * random access and only supports forward iteration.
	 */
	template <typename C>
	class CharacterView
	{
		public:
			/**
			 * Construct an empty character view.
			 */
			CharacterView() noexcept :
				mElements(nullptr),
				mCapacity(0)
			{
				// nothing more to do
			}

			/**
			 * Constructs a text that views the given null-terminated buffer.
			 *
			 * @param cstr The text to view
			 */
			inline explicit CharacterView(C *cstr, std::size_t limit = SIZE_MAX) noexcept :
				mElements(cstr),
				mCapacity(cio::terminator(cstr, limit))
			{
				// include null terminator
				if (mCapacity < limit)
				{
					++mCapacity;
				}
			}

			/**
			 * Constructs a text that views the given null-terminated C string literal.
			 *
			 * @tparam N The static buffer length
			 * @param cstr The C string to view
			 */
			template <std::size_t N>
			CharacterView(C (&cstr)[N]) noexcept :
				mElements(cstr),
				mCapacity(N + 1)
			{
				// nothing more to do
			}			

			/**
			 * Copy constructor.
			 *
			 * @param in The character view to copy
			 */
			CharacterView(const CharacterView<C> &in) noexcept = default;

			/**
			 * Copy assignment.
			 *
			 * @param in The character view to copy
			 * @return This character view
			 */
			CharacterView<C> &operator=(const CharacterView<C> &in) noexcept = default;

			/**
			 * Destructor.
			 */
			~CharacterView() noexcept = default;

			/**
			 * Clears the text. Deallocates the text memory if owned.
			 */
			inline void clear() noexcept
			{
				mElements = nullptr;
				mCapacity = 0;
			}

			/**
			 * Checks to see whether the text is currently null-terminated.
			 * If true, the text data pointer can be used as a C-style string.
			 *
			 * @return Whether the text is null-terminated
			 */
			inline bool isNullTerminated() const noexcept
			{
				return mElements && mCapacity && mElements[mCapacity - 1] == 0;
			}

			/**
			 * Gets the number of data values in the encoded text.
			 * For UTF-8 text this is also the number of bytes.
			 * If the text is null terminated, the null terminator is included in this count.
			 *
			 * @return The number of data values in the encoded text
			 */
			inline std::size_t values() const noexcept
			{
				return mCapacity;
			}

			/**
			 * Checks whether the text is the empty string.
			 * This is true both if it is null (no bytes) or if the first byte is the C-style null terminator.
			 * 
			 * @return Whether the text is null or empty by C-style string rules
			 */
			inline bool empty() const noexcept
			{
				return !mElements || !mCapacity || !mElements[0];
			}

			/**
			 * Checks whether the text has zero bytes and is just the null pointer.
			 * 
			 * @return Whether the text is null 
			*/
			inline bool null() const noexcept
			{
				return !mElements || !mCapacity;
			}

			/**
			 * Gets the size of the text.
			 * This is the number of characters, not necessarily of data values.
			 * 
			 * @warning This requires scanning the entire text.
			 *
			 * @return The number of characters
			 */
			inline std::size_t size() const noexcept
			{
				std::size_t count = 0;
				for (Character c : *this)
				{
					++count;
				}
				return count;
			}

			/**
			 * Gets the character at the given character offset.
			 * If no such character exists, the NUL (0) Character is returned.
			 * 
			 * @warning This requires scanning the text up to the given character.
			 *
			 * @param offset The offset
			 * @return The character
			 */
			inline Character operator[](std::size_t offset) const noexcept
			{
				std::size_t current = 0;
				CharacterIterator<C> ii = this->begin();
				while (ii && current < offset)
				{
					++ii;
				}
				return *ii;
			}

			/**
			 * Gets the data pointer to the text.
			 * Note that the text might not actually be null-terminated so use nullTerminate() first if you want to ensure the data pointer
			 * is usable with C-style text APIs. This method returns the data pointer regardless of null termination.
			 *
			 * @return The text data pointer
			 */
			inline C *data() noexcept
			{
				return mElements;
			}

			/**
			 * Gets the data pointer to the text.
			 * Note that the text might not actually be null-terminated so use nullTerminate() first if you want to ensure the data pointer
			 * is usable with C-style text APIs. This method returns the data pointer regardless of null termination.
			 *
			 * @return The text data pointer
			 */
			inline const C *data() const noexcept
			{
				return mElements;
			}

			/**
			 * Gets the iterator to the beginning of the text, which also happens to be the data pointer.
			 *
			 * @return The begin iterator
			 */
			inline CharacterIterator<C> begin() noexcept
			{
				return CharacterIterator<C>(mElements, mCapacity);
			}

			/**
			 * Gets the iterator to the beginning of the text, which also happens to be the data pointer.
			 *
			 * @return The begin iterator
			 */
			inline CharacterIterator<const C> begin() const noexcept
			{
				return CharacterIterator<const C>(mElements, mCapacity);
			}

			/**
			 * Gets the iterator to the end of the text.
			 *
			 * @return The end iterator
			 */
			inline CharacterIterator<C> end() noexcept
			{
				return CharacterIterator<C>(mElements + mCapacity, 0);
			}

			/**
			 * Gets the iterator to the end of the text.
			 *
			 * @return The end iterator
			 */
			inline CharacterIterator<const C> end() const noexcept
			{
				return CharacterIterator<const C>(mElements + mCapacity, 0);
			}

			/**
			 * Sets up this Text object to reference the given text without copying.
			 *
			 * @param text The text to reference
			 * @param length The number of character bytes in the text
			 */
			void bind(C *text, std::size_t length = SIZE_MAX) noexcept
			{
				mElements = text;
				mCapacity = cio::terminator(text, length);
			}
		
			/**
			 * Creates a std::string copy of this text transcoded to UTF-8.
			 *
			 * @return The string copy
			 * @throw std::bad_alloc If memory for the string could not be allocated
			 */
			inline std::string str() const
			{
				std::string output;
				output.resize(this->strlen());
				std::size_t offset = 0;
				for (Character c : *this)
				{
					offset += c.print(&output[0] + offset, output.size() - offset);
				}
				return output;
			}

			/**
			 * Gets the printed length of the text as UTF-8.
			 * 
			 * @return The number of UTF-8 characters in the text up to the first null terminator if present
			 */
			inline std::size_t strlen() const noexcept
			{
				std::size_t length = 0;
				for (Character c : *this)
				{
					length += c.strlen();
				}
				return length;
			}

			/**
			 * Prints to a UTF-8 string buffer.
			 *
			 * @param buffer The buffer to print to
			 * @param capacity The capacity of the buffer
			 * @return The length of this text to be printed
			 */
			inline std::size_t print(char *buffer, std::size_t count) const noexcept
			{
				std::size_t needed = 0;
				std::size_t printed = 0;
				for (Character c : *this)
				{
					needed += c.print(buffer + printed, count - printed);
					printed = std::min(needed, count);
				}
				return needed;
			}

			CharacterView<C> trim() const noexcept
			{
				std::size_t start = 0;
				std::size_t end = SIZE_MAX;
				std::size_t current = 0;

				// Skip all non-glyphs at beginning
				Parse<Character, std::remove_const_t<C>> decoded = Character::decode(this->data(), this->values());
				while (decoded && !decoded.value.isGlyph())
				{
					start += decoded.parsed;
					current = start;
					decoded = Character::decode(this->data() + current, this->values() - current);
				}

				// Iterate until end and track first non-glyph character after all glyphs
				while (decoded)
				{
					if (decoded.value.isGlyph())
					{
						end = SIZE_MAX;
					}
					else if (end == SIZE_MAX)
					{
						end = current;
					}

					current += decoded.parsed;
					decoded = Character::decode(this->data() + current, this->values() - current);
				}

				return CharacterView<C>(this->data() + start, std::min(this->values(), end) - start);
			}

			/**
			 * Casts to a std::string copy of this text transcoded to UTF-8.
			 *
			 * @return The string copy
			 * @throw std::bad_alloc If memory for the string could not be allocated
			 */
			inline operator std::string() const
			{
				return this->str();
			}

			/**
			 * Interprets the text in a Boolean context.
			 * It is considered true if it is not empty.
			 *
			 * @return Whether the text is not empty
			 */
			inline explicit operator bool() const noexcept
			{
				return mCapacity > 0;
			}

		private:
			/** The character bytes **/
			C *mElements = nullptr;

			/** The number of character byte elements **/
			std::size_t mCapacity = 0;
	};

	/**
	 * Gets a text for a static character array.
	 * 
	 * @tparam C The character value type
	 * @tparam N The number of values
	 * @return The character view
	 */
	template <typename C, std::size_t N>
	inline CharacterView<C> text(C (&buffer)[N]) noexcept
	{
		return CharacterView<C>(buffer);
	}

	template <std::size_t N>
	inline CharacterView<const char> text(const FixedText<N> &text) noexcept
	{
		return CharacterView<const char>(text.data(), N + 1);
	}

	/**
	 * Gets a text view for a character array.
	 * If the array is not null terminated, the limit must be provided.
	 *
	 * @tparam C The character value type
	 * @param buffer The buffer The character array
	 * @param limit The maximum number of values to consider
	 * @return The character view
	 */
	template <typename C>
	inline CharacterView<C> text(C *buffer, std::size_t limit = SIZE_MAX) noexcept
	{
		return CharacterView<C>(buffer, limit);
	}

	/**
	 * Gets a text view for a std::string.
	 *
	 * @param s The string
	 * @return The character view
	 */
	inline CharacterView<char> text(std::string &s) noexcept
	{
		return CharacterView<char>(s.empty() ? nullptr : &s[0], s.empty() ? 0 : s.size() + 1);
	}

	/**
	 * Gets a text view for a std::string.
	 *
	 * @param s The string
	 * @return The character view
	 */
	inline CharacterView<const char> text(const std::string &s) noexcept
	{
		return CharacterView<const char>(s.data(), s.empty() ? 0 : s.size() + 1);
	}

	/**
	 * Forwards a character view.
	 *
	 * @param s The character view
	 * @return The character view
	 */
	template <typename C>
	inline CharacterView<C> &text(CharacterView<C> &s) noexcept
	{
		return s;
	}

	/**
	 * Forwards a character view.
	 *
	 * @param s The character view
	 * @return The character view
	 */
	template <typename C>
	inline const CharacterView<C> &text(const CharacterView<C> &s) noexcept
	{
		return s;
	}

	/**
	 * Prints a CharacterView object to a C++ stream by transcoding it to UTF-8.
	 * 
	 * @param s The stream
	 * @param text The text to print
	 * @return The stream
	 * @throw std::exception if the stream threw any exceptions on failure
	 */
	template <typename C>
	std::ostream &operator<<(std::ostream &s, const CharacterView<C> &text)
	{
		for (Character c : text)
		{
			s << c;
		}
		return s;
	}

	/**
	 * Compare two character views for equality.
	 *
	 * @tparam C The first character type
	 * @tparam D The second character type
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are equal
	 */
	template <typename C, typename D>
	inline bool operator==(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return std::equal(left.begin(), left.end(), right.begin(), right.end());
	}

	/**
	 * Compare two character views for inequality.
	 *
	 * @tparam C The first character type
	 * @tparam D The second character type
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the text objects are not equal
	 */
	template <typename C, typename D>
	inline bool operator!=(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return !(left == right);
	}

	/**
	 * Compare two text strings for sort order to see if the first string is before the second.
	 * Sorting is done by Unicode codepoint, which is often but not exactly alphabetical.
	 *
	 * @tparam C The first character type
	 * @tparam D The second character type
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts before the second text
	 */
	template <typename C, typename D>
	inline bool operator<(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end(), std::less<>());
	}

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is before or equal to the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically before or equal to the second text
	 */
	template <typename C, typename D>
	inline bool operator<=(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end(), std::less_equal<>());
	}

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is after the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically after the second text
	 */
	template <typename C, typename D>
	inline bool operator>(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end(), std::greater<>());
	}

	/**
	 * Compare two text strings for alphabetic sort order to see if the first string is after or equal to the second.
	 *
	 * @param left The first text
	 * @param right The second text
	 * @return Whether the first text sorts alphabetically after or equal to the second text
	 */
	template <typename C, typename D>
	inline bool operator>=(const CharacterView<C> &left, const CharacterView<D> &right) noexcept
	{
		return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end(), std::greater_equal<>());
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
