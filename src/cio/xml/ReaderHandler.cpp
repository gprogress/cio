/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ReaderHandler.h"

namespace cio
{
	namespace xml
	{
		ReaderHandler::ReaderHandler() :
			mEventCapacity(0),
			mCancelParse(false),
			mIsDone(false)
		{
			// nothing more to do
		}

		ReaderHandler::~ReaderHandler() = default;

		void ReaderHandler::startDocument()
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mEventCapacity == 0)
			{
				mEventCapacity = 1024;
			}

			mCancelParse = false;
			mIsDone = false;
			mEvents.emplace_back(Type::Document, EventType::Start);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::endDocument()
		{
			std::lock_guard<std::mutex> lock(mMutex);
			mIsDone = true;
			mEvents.emplace_back(Type::Document, EventType::End);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::startElement(Text element)
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mCancelParse)
			{
				throw std::underflow_error("Parse canceled");
			}

			mEvents.emplace_back(element, Type::Element, EventType::Start);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::endElement(Text element)
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mCancelParse)
			{
				throw std::underflow_error("Parse canceled");
			}

			mEvents.emplace_back(element, Type::Element, EventType::End);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::processAttribute(Text key, Text value)
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mCancelParse)
			{
				throw std::underflow_error("Parse canceled");
			}

			mEvents.emplace_back(key, Type::Attribute, EventType::Key);
			mEvents.emplace_back(value, Type::Attribute, EventType::Value);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::processComment(Text comment)
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mCancelParse)
			{
				throw std::underflow_error("Parse canceled");
			}

			mEvents.emplace_back(comment, Type::Comment, EventType::None);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::processTextChunk(Text text)
		{
			std::lock_guard<std::mutex> lock(mMutex);

			if (mCancelParse)
			{
				throw std::underflow_error("Parse canceled");
			}

			mEvents.emplace_back(text, Type::Text, EventType::None);
			mWaitCondition.notify_all();
		}

		void ReaderHandler::handleError(Text message)
		{
			// todo figure out what this is and how to communicate it
		}

		void ReaderHandler::setEventQueueSize(std::size_t capacity)
		{
			std::lock_guard<std::mutex> lock(mMutex);
			mEventCapacity = capacity;
		}

		std::size_t ReaderHandler::getEventQueueSize() const
		{
			std::lock_guard<std::mutex> lock(mMutex);
			std::size_t capacity = mEventCapacity;
			return capacity;
		}

		Event ReaderHandler::peekNextEvent() const
		{
			Event peeked;
			std::lock_guard<std::mutex> lock(mMutex);

			if (!mEvents.empty())
			{
				peeked = mEvents.front();
			}

			return peeked;
		}

		Event ReaderHandler::peekNextEventType() const
		{
			Event peeked;
			std::lock_guard<std::mutex> lock(mMutex);

			if (!mEvents.empty())
			{
				peeked.setDataType(mEvents.front().getDataType());
				peeked.setEventType(mEvents.front().getEventType());
			}

			return peeked;
		}

		Event ReaderHandler::takeNextEvent()
		{
			std::unique_lock<std::mutex> lock(mMutex);

			while (mEvents.empty())
			{
				mWaitCondition.wait(lock);
			}

			Event result = std::move(mEvents.front());
			mEvents.pop_front();
			mWaitCondition.notify_all();
			return result;
		}

		Event ReaderHandler::takeNextEvent(Type type)
		{
			std::unique_lock<std::mutex> lock(mMutex);

			while (mEvents.empty())
			{
				mWaitCondition.wait(lock);
			}

			Event result;

			if (mEvents.front().getDataType() == type)
			{
				result = std::move(mEvents.front());
				mEvents.pop_front();
				mWaitCondition.notify_all();
			}

			return result;
		}

		Event ReaderHandler::takeNextEvent(Type type, EventType subtype)
		{
			std::unique_lock<std::mutex> lock(mMutex);

			while (mEvents.empty())
			{
				mWaitCondition.wait(lock);
			}

			Event result;

			if (mEvents.front().matches(type, subtype))
			{
				result = std::move(mEvents.front());
				mEvents.pop_front();
				mWaitCondition.notify_all();
			}

			return result;
		}

		void ReaderHandler::skipCurrentAttributes()
		{
			std::unique_lock<std::mutex> lock(mMutex);
			bool done = false;

			while (!done && !mCancelParse)
			{
				if (mEvents.empty())
				{
					mWaitCondition.wait(lock);
				}

				while (!mEvents.empty() && mEvents.front().getDataType() == Type::Attribute)
				{
					mEvents.pop_front();
				}

				done = !mEvents.empty() || mIsDone;
				mWaitCondition.notify_all();
			}
		}

		void ReaderHandler::skipCurrentElement()
		{
			std::unique_lock<std::mutex> lock(mMutex);
			unsigned depth = 0;
			bool done = false;

			while (!done && !mCancelParse)
			{
				if (mEvents.empty())
				{
					mWaitCondition.wait(lock);
				}

				while (!mEvents.empty())
				{
					if (depth == 0 && mEvents.front().matches(Type::Element, EventType::End))
					{
						done = true;
						break;
					}
					else if (mEvents.front().matches(Type::Document, EventType::End))
					{
						done = true;
						break;
					}

					if (mEvents.front().matches(Type::Element, EventType::Start))
					{
						++depth;
					}
					else if (mEvents.front().matches(Type::Element, EventType::End))
					{
						--depth;
					}

					mEvents.pop_front();
				}

				if (mEvents.empty())
				{
					done |= mIsDone;
				}

				mWaitCondition.notify_all();
			}
		}

		Event ReaderHandler::skipToTakeNextElementStart()
		{
			Event nextElement;
			std::unique_lock<std::mutex> lock(mMutex);
			unsigned depth = 1;
			bool done = false;

			while (!done && !mCancelParse)
			{
				if (mEvents.empty() && !mIsDone)
				{
					mWaitCondition.wait(lock);
				}

				while (!mEvents.empty())
				{
					// Document end is end of line regardless of where we are
					if (mEvents.front().matches(Type::Document, EventType::End))
					{
						done = true;
						break;
					}
					// If we've hit depth 0, then we've escaped into the parent element
					else if (depth == 0)
					{
						// If we've escaped up one level and found a new element start, we're golden
						if (mEvents.front().matches(Type::Element, EventType::Start))
						{
							nextElement = mEvents.front();
							mEvents.pop_front();
							done = true;
							break;
						}
						// If we've escaped up one level but found the parent element end too, we're done, there's no "next" element to process
						else if (mEvents.front().matches(Type::Element, EventType::End))
						{
							done = true;
							break;
						}

						// else Skip text chunks and other non-element stuff in the parent element
					}
					// Inside current element, found a child element, increase depth and ignore
					else if (mEvents.front().matches(Type::Element, EventType::Start))
					{
						++depth;
					}
					// Inside current element, ended a child element, decrease depth and ignore
					else if (mEvents.front().matches(Type::Element, EventType::End))
					{
						--depth;
					}

					mEvents.pop_front();
				}

				if (mEvents.empty())
				{
					done |= mIsDone;
				}

				mWaitCondition.notify_all();
			}

			return nextElement;
		}

		Event ReaderHandler::skipToNextChild()
		{
			Event nextElement;
			std::unique_lock<std::mutex> lock(mMutex);
			bool done = false;

			while (!done && !mCancelParse)
			{
				if (mEvents.empty() && !mIsDone)
				{
					mWaitCondition.wait(lock);
				}

				if (!mEvents.empty() && !mEvents.front().matches(Type::Document, EventType::End))
				{
					if (!mEvents.front().matches(Type::Element, EventType::End))
					{
						nextElement = mEvents.front();
						mEvents.pop_front();
					}

					done = true;
				}
				else
				{
					done |= mIsDone;
				}

				mWaitCondition.notify_all();
			}

			return nextElement;
		}

		Event ReaderHandler::skipToNextChildElement()
		{
			Event nextElement;
			std::unique_lock<std::mutex> lock(mMutex);
			bool done = false;

			while (!done && !mCancelParse)
			{
				if (mEvents.empty() && !mIsDone)
				{
					mWaitCondition.wait(lock);
				}

				while (!mEvents.empty())
				{
					if (mEvents.front().matches(Type::Element, EventType::Start))
					{
						nextElement = mEvents.front();
						mEvents.pop_front();
						done = true;
						break;
					}
					else if (mEvents.front().matches(Type::Element, EventType::End))
					{
						done = true;
						break;
					}
					else if (mEvents.front().matches(Type::Document, EventType::End))
					{
						done = true;
						break;
					}
					else
					{
						mEvents.pop_front();
					}
				}

				if (mEvents.empty())
				{
					done |= mIsDone;
				}

				mWaitCondition.notify_all();
			}

			return nextElement;
		}

		void ReaderHandler::cancel()
		{
			std::lock_guard<std::mutex> lock(mMutex);
			mCancelParse = true;
			mIsDone = true;
			mEvents.clear();
			mWaitCondition.notify_all();
		}

		ReaderHandler::operator bool() const
		{
			bool running = false;
			std::lock_guard<std::mutex> lock(mMutex);
			running = mIsDone;
			return running;
		}
	}
}
