/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_EXCEPTION_H
#define CIO_EXCEPTION_H

#include "Types.h"

#include "Progress.h"

#include <iosfwd>
#include <stdexcept>
#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Exception class extends the base C++ exception to provide additional information on the I/O operation status
	 * such as bytes processed and overall Status code. Additionally it also provides mechanisms to either set a static text message for
	 * the C++ what() method or to set a dynamically-allocated std::string message.
	 *
	 * Finally, it provides printing functionality to get a more detailed error message that includes the bytes processed and status if set.
	 *
	 * This flexibility is intended to allow the Exception to be used in performance-critical code where dynamic string allocation is not desired,
	 * while still preserving full general flexibility of the base C++ exception features.
	 */
	class CIO_API Exception : public std::exception
	{
		public:
			/**
			 * Construct an empty Exception.
			 * The result is set to the default empty result with zero bytes processed and Status None.
			 * The what() message will return the default static text for Status::None.
			 */
			Exception() noexcept;
			
			/**
			 * Construct an exception representing the failure of an action for a particular reason.
			 *
			 * @param action The action
			 * @param reason The reason for failure
			 */	
			Exception(Action action, Reason reason) noexcept;
			
			/**
			 * Construct an exception from a state (typically a failure).
			 *
			 * @param e The state
			 */
			explicit Exception(State e) noexcept;

			/**
			 * Construct an Exception using the given Status code.
			 * The result will be set to the given result.
			 * The what() message returns the static text string for the Status on the Progress.
			 *
			 * @param result The result to use
			 */
			explicit Exception(Progress<std::uint64_t> result) noexcept;
			
			/**
			 * Construct an Exception using the given Reason given static message.
			 * The what() message returns the given static message.
			 *
			 * @param reason The reason for failure
			 * @param message The static message
			 */
			Exception(Reason reason, const char *message) noexcept;
			
			/**
			 * Construct an Exception using the given Status code and the given static message.
			 * The result will be set to the given Status with zero bytes processed.
			 * The what() message returns the given static message.
			 *
			 * @param status The status to use
			 * @param message The static message
			 */
			Exception(State status, const char *message) noexcept;

			/**
			 * Construct an Exception using the given Action and failure Reason and the given static message.
			 * The result will be set to the given Action and Reason with zero bytes processed.
			 * The what() message returns the given static message.
			 *
			 * @param action The action
			 * @param reason The reason for failure
			 * @param message The static message
			 */
			Exception(Action action, Reason reason, const char *message) noexcept;

			/**
			 * Construct an Exception using the given Status code and result bytes.
			 * The what() message returns the static text string for the Status on the Progress.
			 *
			 * @param status The status to use
			 * @param bytes The number of bytes processed
			 * @param message The static message
			 */
			Exception(State status, std::uint64_t bytes) noexcept;
			
			/**
			 * Construct an Exception using the given result and the given static message.
			 * The result will be set to the given result.
			 * The what() message returns the given static message.
			 *
			 * @param result the result to use
			 * @param message The static message
			 */
			Exception(Progress<std::uint64_t> result, const char *message) noexcept;

			/**
			 * Construct a copy of an Exception.
			 * If it has a dynamic message, that will be duplicated.
			 * All static fields will be shallow copied.
			 *
			 * @param e The exception to copy
			 * @throw std::bad_alloc If the input had a dynamic message and allocating a copy failed
			 */
			Exception(const Exception &e);

			/**
			 * Construct an exception by moving the contents of an existing Exception.
			 *
			 * @param e The exception to move
			 */
			Exception(Exception &&e) noexcept;

			/**
			 * Copies an Exception.
			 * If it has a dynamic message, that will be duplicated.
			 * All static fields will be shallow copied.
			 *
			 * @param e The exception to copy
			 * @return this exception
			 * @throw std::bad_alloc If the input had a dynamic message and allocating a copy failed
			 */
			Exception &operator=(const Exception &e);

			/**
			 * Moves an Exception.
			 *
			 * @param e The exception to move
			 * @return this exception
			 */
			Exception &operator=(Exception &&e) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Exception() noexcept override;

			/**
			 * Clears the exception state.
			 * The dynamic message, if any, will be cleared.
			 * The result will be reset to zero bytes processed with a status of None.
			 * The what() message will return the static text for Status::None.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the most specific message for this exception.
			 * If a dynamic message is set that will be returned.
			 * Otherwise, if a static message is set that will be returned.
			 * If neither are set, then the static text for the result status is returned.
			 *
			 * @return the exception what message
			 */
			virtual const char *what() const noexcept override;

			/**
			 * Gets the failure reason on the exception.
			 *
			 * @return the failure reason
			 */
			Reason reason() const noexcept;
			
			/**
			 * Sets the failure reason on the exception.
			 *
			 * @param r The failure reason
			 */
			void reason(Reason r) noexcept;

			/**
			 * Gets the result status of the operation that generated the exception.
			 *
			 * @return the result status
			 */
			Status status() const noexcept;

			/**
			 * Sets the result status of the operation that generated the exception.
			 * If no other message has been set, this will update what() to return the static text for this status.
			 *
			 * @param status The status to set
			 */
			void status(Status status) noexcept;
			
			/**
			 * Gets the state of the operation that generated the exception.
			 *
			 * @return the result status
			 */
			State state() const noexcept;

			/**
			 * Sets the result of the operation that generated the exception.
			 * If no other message has been set, this will update what() to return the static text for this result's status.
			 *
			 * @param state The state to set
			 */
			void state(State state) noexcept;
			
			/**
			 * Gets the result of the operation that generated the exception.
			 *
			 * @return the result status
			 */
			const Progress<std::uint64_t> &progress() const noexcept;

			/**
			 * Sets the result of the operation that generated the exception.
			 * If no other message has been set, this will update what() to return the static text for this result's status.
			 *
			 * @param result The result to set
			 */
			void progress(const Progress<std::uint64_t> &result) noexcept;
			
			/**
			 * Gets the dynamic message set on this exception.
			 * This will be empty if no such message was set.
			 *
			 * @return the dynamic message
			 */
			const std::string &getMessage() const noexcept;

			/**
			 * Sets the dynamic message on this exception to the given message.
			 *
			 * @param message The message to move
			 */
			void setMessage(std::string message) noexcept;

			/**
			 * Gets the static message set on this exception.
			 * This will be nullptr if no such message was set.
			 *
			 * @return the static message
			 */
			const char *getStaticMessage() const noexcept;

			/**
			 * Sets the static message for this exception.
			 * The caller must ensure the message is not destroyed while this exception is using it.
			 * Typically it should be a string literal or other static text.
			 * Setting to nullptr will disable the static message.
			 *
			 * @param message The static message to set
			 */
			void setStaticMessage(const char *message) noexcept;

			/**
			 * Prints the exception to the given buffer, up to the given length.
			 * This will include the static and/or dynamic message (if present) and the result.
			 * If all items are present, the exception will have the form: "<static message> (<status>): <bytes> processed, <dynamic message>"
			 *
			 * The returned number of bytes is how many characters would have actually been needed to print the message,
			 * which may be less or more than the given length.
			 * If the given length is larger than necessary to print the message, the unused bytes will be set to null.
			 *
			 * @param buffer The buffer to print to
			 * @param len The maximum number of characters to print
			 * @return the number of characters actually needed to print this exception, not counting any trailing null terminators
			 */
			std::size_t print(char *buffer, std::size_t len) const noexcept;

			/**
			 * Prints the exception and returns it as a string.
			 * This will include the static and/or dynamic message (if present) and the result.
			 * If all items are present, the exception will have the form: "<static message> (<status>): <bytes> processed, <dynamic message>"
			 *
			 * @return the printed exception
			 */
			std::string print() const;

			/**
			 * Marks this exception as indicating a successful operation (and therefore not actually an exception).
			 */
			void succeed() noexcept;

			/**
			 * Marks this exception as indicating a failed outcome with the given reason and dynamic error message.
			 * 
			 * @param reason The reason for failure
			 * @param message The detailed dynamic error message
			 */
			void fail(Reason reason, std::string message) noexcept;

			/**
			 * Interprets this exception in a Boolean context.
			 * Since this is intended to represent an error, it is true if an error actually does exist, and false if the state is marked as successful.
			 * 
			 * @return whether this exception indicates an error occurred
			 */
			operator bool() const noexcept;

		private:
			/** The static message for this exception or null */
			const char *mStaticMessage;

			/** The dynamic message for this exception */
			std::string mDynamicMessage;

			/** The result for this exception */
			Progress<std::uint64_t> mProgress;
	};

	/**
	 * Prints a fully detailed Exception to the given C++ stream.
	 * This will include the static and/or dynamic message (if present) and the result.
	 * If all items are present, the exception will have the form: "<static message> (<status>): <bytes> processed, <dynamic message>"
	 *
	 * @param stream The stream to print to
	 * @param e The exception
	 * @return the stream to print to
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, const Exception &e);
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
