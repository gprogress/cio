/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Action.h"

#include "Enumeration.h"

namespace cio
{
	const Enumerant<Action> Enumeration<Action>::values[] =
	{
		{ Action::None, "None", "No action performed" },
		{ Action::Prepare, "Prepare", "Prepare for an execution step" },
		{ Action::Execute, "Execute", "Execute a procedure" },
		{ Action::Complete, "Complete", "Complete an execution step" },
		{ Action::Read, "Read", "Read data from a resource" },
		{ Action::Write, "Write", "Write data to a resource" },
		{ Action::Edit, "Edit", "Read and write data to a resource" },
		{ Action::Notify, "Notify", "Receive an asynchronous update from a resource" },
		{ Action::Acknowledge, "Acknowledge", "Acknowledge receipt of an asynchronous update from a resource" },
		{ Action::Grow, "Grow", "Increase the size of a resource" },
		{ Action::Shrink, "Shrink", "Decrease the size of a resource" },
		{ Action::Resize, "Resize", "Changes the size of a resource" },
		{ Action::Seek, "Seek", "Change the current position within a resource" },
		{ Action::Inquire, "Inquire", "Request metadata from a resource" },
		{ Action::Metadata, "Metadata", "Change metadata on a resource" },
		{ Action::Subscribe, "Subscribe", "Subscribe to asynchronous notifications" },
		{ Action::Unsubscribe, "Unsubscribe", "Unsubscribe from asynchronous notifications" },
		{ Action::Create, "Create", "Create a new resource" },
		{ Action::Open, "Open", "Open an existing resource" },
		{ Action::Close, "Close", "Close an open resource" },
		{ Action::Remove, "Remove", "Remove a resource" },
		{ Action::Rename, "Rename", "Rename a resource" },
		{ Action::Connect, "Connect", "Connect to a remote resource" },
		{ Action::Disconnect, "Disconnect", "Disconnect from a remote resource" },
		{ Action::Scan, "Scan", "Scan to find resources" },
		{ Action::Discover, "Discover", "Discover the existence of a resource" },
		{ Action::Copy, "Copy", "Copy one resource to another" },
		{ Action::Move, "Move", "Move one resource to another" }
	};

	const Enumeration<Action> Enumeration<Action>::instance;

	Enumeration<Action>::Enumeration() :
		Enumerants<Action>("cio::Action", values, Action::None)
	{
		// nothing more to do
	}

	Enumeration<Action>::~Enumeration() noexcept = default;
}
