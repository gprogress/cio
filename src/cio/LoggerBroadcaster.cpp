/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "LoggerBroadcaster.h"

#include "Class.h"

namespace cio
{
	const Class<LoggerBroadcaster> LoggerBroadcaster::sMetaclass("cio::LoggerBroadcaster");

	const Metaclass &LoggerBroadcaster::getDeclaredMetaclass()
	{
		return sMetaclass;
	}

	LoggerBroadcaster::LoggerBroadcaster()
	{
		// nothing more to do
	}

	LoggerBroadcaster::LoggerBroadcaster(const char *context) :
		Logger(context)
	{
		// nothing more to do
	}

	LoggerBroadcaster::LoggerBroadcaster(const char *context, Severity severity) :
		Logger(context, severity)
	{
		// nothing more to do
	}

	LoggerBroadcaster::~LoggerBroadcaster() noexcept
	{
		// nothing more to do
	}

	void LoggerBroadcaster::clear() noexcept
	{
		mOutputs.clear();
		Logger::clear();
	}

	const Metaclass &LoggerBroadcaster::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	bool LoggerBroadcaster::log(const char *context, Severity severity, const char *message)
	{
		bool success = false;

		if (!mOutputs.empty())
		{
			for (Logger *output : mOutputs)
			{
				success |= output->logIfEnabled(context, severity, message);
			}
		}
		else
		{
			success = Logger::log(context, severity, message);
		}

		return success;
	}

	void LoggerBroadcaster::addOutput(Logger *output)
	{
		mOutputs.emplace_back(output);
	}

	void LoggerBroadcaster::removeOutput(Logger *output)
	{
		auto ii = std::find(mOutputs.begin(), mOutputs.end(), output);

		if (ii != mOutputs.end())
		{
			mOutputs.erase(ii);
		}
	}

	std::vector<Logger *> &LoggerBroadcaster::getOutputs()
	{
		return mOutputs;
	}

	const std::vector<Logger *> &LoggerBroadcaster::getOutputs() const
	{
		return mOutputs;
	}

	void LoggerBroadcaster::clearOutputs()
	{
		mOutputs.clear();
	}
}
