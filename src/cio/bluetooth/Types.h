/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_TYPES_H
#define CIO_BLUETOOTH_TYPES_H

#if defined _WIN32
#if defined CIO_BLUETOOTH_BUILD
#if defined CIO_BLUETOOTH_BUILD_SHARED
#define CIO_BLUETOOTH_API __declspec(dllexport)
#else
#define CIO_BLUETOOTH_API
#endif
#else
#define CIO_BLUETOOTH_API __declspec(dllimport)
#endif
#elif defined __ELF__
#define CIO_BLUETOOTH_API __attribute__ ((visibility ("default")))
#else
#define CIO_BLUETOOTH_API
#endif

#include <cstddef>
#include <cstdint>
#include <functional>

// D-Bus forward structure declarations
typedef struct DBusConnection DBusConnection;

namespace cio
{
	// Classes used from other packages
	class Buffer;
	class DeviceAddress;
	class Exception;
	class Executor;
	class Metaclass;
	template <typename> class Class;
	template <typename...> class Listener;
	class Logger;	
	class Path;
	template <typename T> class Progress;
	enum class Reason : std::uint8_t;
	template <typename> class Response;
	class State;
	enum class Status : std::uint8_t;
	class Task;
	class Text;
	class UniqueId;
	
	
	template <typename... T>
	using Callback = std::function<void (T...)>;
	
	namespace net
	{
		class Address;
		class AddressFamily;
		enum class ChannelType : std::uint8_t;
		class Socket;
		class ProtocolFamily;
	}

	/**
	 * The CIO bluetooth namespace provides classes useful for Bluetooth device discovery and communication.
	 */
	namespace bluetooth
	{
		enum class ATT : std::uint8_t;
		enum class ATTError : std::uint8_t;
		class Adapter;
		enum class AddressType : std::uint8_t;
		class Attribute;
		class AttributeClient;
		class AttributeCodec;
		class Characteristic;
		class Connection;
		enum class Control : std::uint16_t;
		class Controller;
		class DBusAttributeClient;
		class DBusController;
		class Descriptor;
		class Device;
		enum class Event : std::uint8_t;
		class HCICodec;
		class HCIController;
		class L2CAPAttributeClient;
		class L2CAPChannel;
		enum class LowEnergyEvent : std::uint8_t;
		enum class Packet : std::uint8_t;
		class Peripheral;
		enum class Property : std::uint8_t;
		class Properties;
		class Protocol;
		class Service;
		class Subscription;
		class Subscriptions;
		class TypeDictionary;
		enum class TypeId : std::uint16_t;
		class UniqueId;

		using AdvertisementListener = Listener<const Peripheral &>;

		/** The CIO Bluetooth Listener is a callback function to receive data regarding a particular attribute value */
		using AttributeCallback = Callback<std::uint16_t, const void *, std::size_t>;
		
		using AttributeListener = Listener<std::uint16_t, const void *, std::size_t>;
	}
}

#endif



