/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ALLOCATOR_H
#define CIO_ALLOCATOR_H

#include "Types.h"

#include <atomic>
#include <memory>
#include <type_traits>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Allocator is a polymorphic interface to perform type-safe allocation, deallocation, duplication,
	 * and movement that can be implemented by multiple backends.
	 * 
	 * Most users will not want to use this directly, and will instead want to use Factory<T> for working
	 * with higher level types.
	 * 
	 * The base class implements the no-op allocator which cannot allocate memory and will not destroy it.
	 * Derived classes implement specific allocators based on new/delete, malloc/free,
	 * pool allocators from a buffer, and so on. Allocators may be global singletons like new and malloc or they may have
	 * individual state for tracking local pools for improving performance.
	 */
	class CIO_API Allocator
	{
		public:
			/**
			 * Gets an allocator that does nothing, to be used for
			 * scenarios where the data is not actually allocated. Avoids a null check.
			 * 
			 * @return the global lack of allocator
			 */
			static Allocator *none() noexcept { return &sNone; }

			/**
			 * Gets the allocator for global C++ new/delete.
			 * Under normal circumstances this is also the default allocator.
			 *
			 * @return the global C++ new/delete allocator
			 */
			static New *getGlobalNew() noexcept { return &sNew; }

			/**
			 * Gets the allocator for global C malloc/realloc/free.
			 *
			 * @return the global C malloc/realloc/free allocator
			 */
			static Malloc *getGlobalMalloc() noexcept { return &sMalloc; }

			/**
			 * Gets the global default allocator in use.
			 * This is normally the C++ New/Delete Allocator.
			 * This is done atomically so it is thread safe.
			 * 
			 * @return the global default allocator
			 */
			static Allocator *getDefault() noexcept;

			/**
			 * Changes the global default allocator in use.
			 * This is done atomically so it is thread safe.
			 * 
			 * @warning This applies to everyone using the metaclass framework.
			 *
			 * @param allocator The new global allocator
			 * @return the previous global default allocator
			 */
			static Allocator *setDefault(Allocator *allocator) noexcept;

			/** 
			 * Construct an allocator.
			 */
			inline Allocator() noexcept = default;
			
			/**
			 * Copies an allocator.
			 * 
			 * @param in The allocator to copy
			 * @return this allocator
			 */
			inline Allocator &operator=(const Allocator &in) = default;

			/**
			 * Destructor.
			 */
			virtual ~Allocator() noexcept;

			/**
			 * Clears the allocator state.
			 * If the allocator is not global, this may also deallocate all of its managed objects.
			 */
			virtual void clear() noexcept;

			// Raw byte methods that bypass the metaclass system

			/**
			 * Allocates bytes.
			 * If count is 0, this returns nullptr.
			 * This will not perform construction for objects but will zero-fill the bytes.
			 * 
			 * @param bytes The number of bytes to allocate
			 * @return the allocated array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *allocate(std::size_t bytes);

			/**
			 * Allocates bytes that is a direct copy of the give bytes.
			 * If count is 0, this returns nullptr.
			 * This will not perform object construction and destruction for objects so
			 * it will only work properly for types where std::memcpy is a sufficient copy method.
			 *
			 * @param data The data to copy
			 * @param bytes The number of bytes desired
			 * @return the allocated array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *duplicate(const void *data, std::size_t bytes);

			/**
			 * Allocates bytes that is a direct copy of the give bytes, copying a different number of bytes.
			 * If allocated is greater than copied, the additional bytes are zero-filled.
			 *
			 * @param data The data to copy
			 * @param copied The number of bytes to copy
			 * @param allocated The desired number of bytes in the output
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *duplicate(const void *data, std::size_t count, std::size_t desired);

			/**
			 * Deallocates the given bytes.
			 * Since this is often used in destructors and move operations, it cannot throw.
			 * 
			 * @param data The existing data previously allocated by the same metaclass
			 * @param bytes The number of bytes to deallocate as a hint, or SIZE_MAX if not known
			 * @return nullptr for convenience
			 */
			virtual std::nullptr_t deallocate(void *data, std::size_t bytes) noexcept;

			/**
			 * Reallocates bytes
			 * If the desired count is 0, this deallocates the input and returns nullptr.
			 * 
			 * @warning This will NOT handle memory with constructed objects correctly
			 * and should only be used for primitive types that are suitable for std::memcpy.
			 *
			 * @param data The existing data previously allocated by the same metaclass
			 * @param current The current number of bytes
			 * @param desired The desired number of bytes
			 * @return the reallocated array, or nullptr if desired is 0
			 * @throw std::bad_alloc If new memory needed to be allocated and doing so failed
			 */
			virtual void *reallocate(void *data, std::size_t current, std::size_t desired);

			// Class methods that use Metaclass to reflect construction, destruction, copying, and moving

			/**
			 * Allocates memory for objects and constructs them.
			 * If count is 0, this returns nullptr.
			 * This uses the given metaclass object construction and destruction for objects.
			 *
			 * @param type The metaclass describing the type of object to allocate
			 * @param count The number of objects desired
			 * @return the allocated object array
			 * @throw std::bad_alloc If memory allocation fails
			 */
			virtual void *create(const Metaclass *type, std::size_t count = 1);

			/**
			 * Destroys objects and deallocates the memory for them.
			 * Since this is often used in destructors and move operations, it cannot throw.
			 *
			 * @param type The metaclass describing the runtime type of the objects to destroy
			 * @param data The existing objects previously created by the same metaclass and allocator
			 * @param count The number of objects to destroy
			 * @return nullptr for convenience
			 */
			virtual std::nullptr_t destroy(const Metaclass *type, void *data, std::size_t count = 1) noexcept;

			/**
			 * Allocates a copy of an array of count elements using the given metaclass.
			 * If the metaclass is null, this returns null.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param count The number of data elements to copy
			 * @return the duplicated array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *copy(const Metaclass *type, const void *data, std::size_t count = 1);

			/**
			 * Creates a copy of an array of count elements using the metaclass, copying a different number of elements.
			 * If desired is greater than count, the additional elements not present in the input are default constructed.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param copied The number of objects to copy
			 * @param allocated The desired number of objects in the output
			 * @return the copied array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *copy(const Metaclass *type, const void *data, std::size_t copied, std::size_t allocated);

			/**
			 * Creates a copy of an array of count elements using the metaclass, moving a different number of elements.
			 * If desired is greater than count, the additional elements not present in the input are default constructed.
			 * If the object is not movable, this falls back to copy construction instead.
			 *
			 * @param type The metaclass to use
			 * @param data The data to copy
			 * @param moved The number of data elements to move
			 * @param allocated The desired number of elements in the output
			 * @return the moved array
			 * @throw std::bad_alloc If allocating memory for the duplicate failed
			 */
			virtual void *move(const Metaclass *type, void *data, std::size_t moved, std::size_t allocated);

			/**
			 * Reallocates an object array to have a new size.
			 * If the desired count is 0, this deallocates the input and returns nullptr.
			 * If the current size and new size are the same, this returns the data pointer without
			 * doing anything.
			 *
			 * @param data The existing data previously allocated by the same metaclass
			 * @param current The current number of elements
			 * @param desired The desired number of elements
			 * @return the reallocated array, or nullptr if desired is 0
			 * @throw std::bad_alloc If new memory needed to be allocated and doing so failed
			 */
			virtual void *resize(const Metaclass *type, void *data, std::size_t current, std::size_t desired);

			/**
			 * Interprets the allocator in a Boolean context.
			 * This should be true if the allocator is in a state where it can function.
			 * The base class always returns true.
			 * 
			 * @return whether the allocator is usable
			 */
			virtual explicit operator bool() const noexcept { return true; }

		protected:
			/** Allocator indicate no allocation is being done */
			static Allocator sNone;

			/** Allocator for global C++ new/delete */
			static New sNew;

			/** Allocator for global C malloc/realloc/free */
			static Malloc sMalloc;

			/** Global default allocator as currently set */
			static std::atomic<Allocator *> sGlobalDefault;
	};

	/**
	 * Wraps a raw pointer in a C++-11 std::shared_ptr without actually deallocating it when the reference count drops to 0.
	 * This is primarily useful for giving pointers to stack or static memory to APIs expecting std::shared_ptr.
	 * It must still allocate the std::shared_ptr control block but sets the deleter to a no-op lambda.
	 * 
	 * @tparam T The type of object being pointed to
	 * @param pointer The pointer to wrap
	 * @return the pointer wrapped in a non-deleting std::shared_ptr
	 */
	template <typename T>
	inline std::shared_ptr<T> not_shared_ptr(T *pointer)
	{
		return std::shared_ptr<T>(pointer, [](T *data) {});
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
