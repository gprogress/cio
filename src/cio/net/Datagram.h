/*==============================================================================
 * Copyright 2021-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_DATAGRAM_H
#define CIO_NET_DATAGRAM_H

#include "Types.h"

#include "Address.h"

#include <cio/Buffer.h>
#include <cio/Progress.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace net
	{
		/**
		 * The Datagram class represents a single message send to or received from a particular network address.
		 * It consists of a sender address, a recipient address, a packet buffer representing the contents, and a result.
		 */
		class CIO_NET_API Datagram
		{
			public:
				/**
				 * Construct an empty datagram.
				 */
				Datagram() noexcept;

				/**
				 * Construct a deep copy of a datagram.
				 *
				 * @param in The datagram to copy
				 */
				Datagram(const Datagram &in);

				/**
				 * Construct a datagram by transferring the contents of an existing one.
				 *
				 * @param in The datagram to move
				 */
				Datagram(Datagram &&in) noexcept;

				/**
				 * Copy a datagram.
				 *
				 * @param in The datagram to copy
				 * @return this datagram
				 */
				Datagram &operator=(const Datagram &in);

				/**
				 * Move a datagram.
				 *
				 * @param in The datagram to move
				 * @return this datagram
				 */
				Datagram &operator=(Datagram &&in) noexcept;

				/**
				 * Destructor.
				 */
				~Datagram() noexcept;

				/**
				 * Gets the address of the sender if known.
				 * For outgoing datagrams this may be optional.
				 * For incoming datagrams it captures the remote sender's address.
				 *
				 * @return the address of the sender
				 */
				inline const Address &getSender() const noexcept;

				/**
				 * Sets the address of the sender if known.
				 * For outgoing datagrams this may be optional.
				 * For incoming datagrams it captures the remote sender's address.
				 *
				 * @param address The address of the sender
				 */
				void setSender(Address address) noexcept;

				/**
				 * Gets the address of the recipient if known.
				 * For outgoing datagrams this must be specified unless the socket is connected to a particular recipient.
				 * For incoming datagrams this may be optional.
				 *
				 * @return the address of the recipient
				 */
				inline const Address &getRecipient() const noexcept;

				/**
				 * Sets the address of the recipient if known.
				 * For outgoing datagrams this must be specified unless the socket is connected to a particular recipient.
				 * For incoming datagrams this may be optional.
				 *
				 * @param address the address of the recipient
				 */
				void setRecipient(Address address) noexcept;

				/**
				 * Gets the packet buffer for the datagram's contents.
				 *
				 * @return the packet buffer
				 */
				inline Buffer &getPacket() noexcept;

				/**
				 * Gets the packet buffer for the datagram's contents.
				 *
				 * @return the packet buffer
				 */
				inline const Buffer &getPacket() const noexcept;

				/**
				 * Transfers the given packet buffer into this datagram for its use.
				 *
				 * @param packet the packet buffer to use
				 */
				void setPacket(Buffer &&packet) noexcept;

				/**
				 * Clears the datagram. This clears the sender, recipient, packet buffer, and result.
				 */
				void clear() noexcept;

				/**
				 * Gets the current size of the datagram packet.
				 * This is the underlying packet buffer's size.
				 * 
				 * @return the datagram size
				 */
				std::size_t size() const noexcept;

				/**
				 * Gets whether any bytes are allocated in the datagram packet.
				 * 
				 * @return whether the packet has any bytes allocated
				 */
				bool empty() const noexcept;

				/**
				 * Sets the current size of the datagram packet.
				 * This modifies the underlying packet buffer's size.
				 *
				 * @param bytes The desired datagram size
				 * @return the new datagram size
				 */
				std::size_t resize(std::size_t bytes);

				/**
				 * Reserves the current size of the datagram packet to be at least the given size.
				 * This modifies the underlying packet buffer's size.
				 *
				 * @param bytes The desired datagram size
				 * @return the new datagram size
				 */
				std::size_t reserve(std::size_t bytes);

				/**
				 * Compacts the packet buffer to remove bytes prior to the current position.
				 * 
				 * @return the number of bytes remaining in the packet buffer
				 */
				std::size_t compact() noexcept;

				/**
				 * Flips the packet buffer to process content from the start to the current position.
				 * This sets the position to 0 and the limit to the previous position.
				 *
				 * @return the number of bytes remaining in the packet buffer after flipping
				 */
				std::size_t flip() noexcept;

				/**
				 * Unflips the packet buffer to process unused bytes after the current limit.
				 * This sets the position to the current limit and the limit to the size.
				 *
				 * @return the number of bytes remaining in the packet buffer after unflipping
				 */
				std::size_t unflip() noexcept;

				/**
				 * Resets the packet buffer to enable processing all allocated bytes.
				 * This sets the position to 0 and the limit to the size.
				 * 
				 * @return the size
				 */
				std::size_t reset() noexcept;

				/**
				 * Skips up to the given number of bytes in the packet buffer.
				 * 
				 * @param bytes The number of bytes to skip
				 * @return the number of bytes actually skipped
				 */
				std::size_t skip(std::size_t bytes) noexcept;

				/**
				 * Gets the number of unprocessed bytes remaining in the packet buffer.
				 * 
				 * @return the number of bytes remaining
				 */
				std::size_t remaining() const noexcept;

				/**
				 * Ensures that the packet has at least the given numbr of unprocessed bytes remaining in the packet buffer.
				 * 
				 * @param requested the number of bytes requested
				 * @return the actual number of bytes remaining
				 */
				std::size_t remaining(std::size_t requested);

				/**
				 * Gets the most recent status result for the datagram.
				 *
				 * @return the result
				 */
				inline Progress<std::size_t> getProgress() const noexcept;

				/**
				 * Sets the most recent status result for the datagram.
				 *
				 * @param result the result
				 */
				inline void setProgress(Progress<std::size_t> result) noexcept;

				/**
				 * Gets the most recent status for the datagram.
				 *
				 * @return the status
				 */
				inline Status getStatus() const noexcept;

				/**
				 * Sets the most recent status for the datagram.
				 *
				 * @param status The status
				 */
				inline void setStatus(Status status) noexcept;

				/**
				 * Configures the datagram to reply to the original sender.
				 * This swaps the sender and recipient and ensures the packet buffer is large enough to prepare a response.
				 */
				void reply();

				/**
				 * Interprets the datagram in a Boolean context.
				 * This is true if the packet buffer has data remaining.
				 * 
				 * @return whether the packet buffer has data remaining
				 */
				explicit operator bool() const noexcept;

			private:
				/** The sender address */
				Address mSender;

				/** The recipient address */
				Address mRecipient;

				/** The packet buffer */
				Buffer mPacket;

				/** The most recent result */
				Progress<std::size_t> mProgress;
		};
	}
}

/* Inline implementation */

namespace cio
{
	namespace net
	{
		inline const Address &Datagram::getSender() const noexcept
		{
			return mSender;
		}

		inline const Address &Datagram::getRecipient() const noexcept
		{
			return mRecipient;
		}

		inline Buffer &Datagram::getPacket() noexcept
		{
			return mPacket;
		}

		inline const Buffer &Datagram::getPacket() const noexcept
		{
			return mPacket;
		}

		inline Progress<std::size_t> Datagram::getProgress() const noexcept
		{
			return mProgress;
		}

		inline void Datagram::setProgress(Progress<std::size_t> result) noexcept
		{
			mProgress = result;
		}

		inline Status Datagram::getStatus() const noexcept
		{
			return mProgress.status;
		}

		inline void Datagram::setStatus(Status status) noexcept
		{
			mProgress.status = status;
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

