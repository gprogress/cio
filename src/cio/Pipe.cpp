/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/

#include "Pipe.h"

#include "Class.h"
#include "Exception.h"

#if defined CIO_HAVE_WINDOWS_H
#include <windows.h>

#elif defined CIO_HAVE_UNISTD_H
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "Filesystem.h"

#else

#error "No platform implementation for acsl::Pipe"

#endif

#include <cerrno>
#include <system_error>

namespace cio
{
	Class<Pipe> Pipe::sMetaclass("cio::Pipe");

	const Metaclass &Pipe::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Pipe::Pipe()
	{
		// nothing to do
	}

	Pipe::Pipe(std::uintptr_t handle) :
		Device(handle)
	{
		// nothing more to do
	}

	Pipe::Pipe(Pipe &&in) noexcept :
		Device(std::move(in))
	{
		// nothing more to do
	}

	Pipe &Pipe::operator=(Pipe &&in) noexcept
	{
		Device::operator=(std::move(in));
		return *this;
	}

	Pipe::~Pipe() noexcept
	{
		// nothing more to do
	}

	const Metaclass &Pipe::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	// Platform-specific code
#if defined CIO_HAVE_WINDOWS_H

	Pipe Pipe::createAnonymousPipe(std::size_t bufferSize)
	{
		this->clear();

		HANDLE readable = INVALID_HANDLE_VALUE;
		HANDLE writable = INVALID_HANDLE_VALUE; 
		BOOL status = ::CreatePipe(&readable, &writable, nullptr, static_cast<DWORD>(bufferSize));
		if (!status)
		{
			Reason r = cio::errorWin32();
			throw Exception(Action::Create, r);
		}

		mHandle = reinterpret_cast<std::uintptr_t>(writable);
		return Pipe(reinterpret_cast<std::uintptr_t>(readable));
	}

	void Pipe::createPipe(const char *name, std::size_t bufferSize)
	{
		this->clear();
		std::string fullName("\\\\.\\pipe\\");
		fullName += name;
		HANDLE handle = ::CreateNamedPipe(
							fullName.c_str(),             // pipe name
							PIPE_ACCESS_DUPLEX,       // read/write access
							PIPE_TYPE_BYTE |       // message type pipe
							PIPE_READMODE_BYTE |   // message-read mode
							PIPE_WAIT,                // blocking mode
							PIPE_UNLIMITED_INSTANCES, // max. instances
							static_cast<DWORD>(bufferSize),                  // output buffer size
							static_cast<DWORD>(bufferSize),                  // input buffer size
							0,                        // client time-out
							nullptr);                    // default security attribute

		if (handle == INVALID_HANDLE_VALUE)
		{
			throw std::system_error(::GetLastError(), std::system_category());
		}

		mHandle = reinterpret_cast<std::uintptr_t>(handle);
		mWriteBuffered = true; // named pipe servers are always buffered on Windows
	}

	void Pipe::connectPipe(const char *name)
	{
		this->clear();
		std::string fullName("\\\\.\\pipe\\");
		fullName += name;
		HANDLE hComm;
		hComm = ::CreateFile(fullName.c_str(),
								GENERIC_READ | GENERIC_WRITE,
								0,
								0,
								OPEN_EXISTING,
								FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH,
								0);

		if (hComm == INVALID_HANDLE_VALUE)
		{
			throw std::system_error(::GetLastError(), std::system_category());
		}

		mHandle = reinterpret_cast<std::uintptr_t>(hComm);
		mWriteBuffered = false;
	}

#elif defined CIO_HAVE_UNISTD_H

	Pipe Pipe::createAnonymousPipe(std::size_t bufferSize)
	{
		this->clear();

		int fds[2] = { };
		int status = ::pipe(fds);

		if (status != 0)
		{
			Reason r = cio::error();
			throw Exception(Action::Create, r);
		}

		mHandle = static_cast<std::uintptr_t>(fds[1]);
		return Pipe(static_cast<std::uintptr_t>(fds[0]));
	}

	void Pipe::createPipe(const char *name, std::size_t bufferSize)
	{
		this->clear();
		
		cio::Filesystem fs;
		cio::Path fullPath = fs.getTemporaryDirectory() / name;
		std::string nativePath = fullPath.toNativeFile();
			
		int fd = ::mkfifo(nativePath.c_str(), 0666);

		if (fd == -1)
		{
			throw std::system_error(errno, std::system_category());
		}

		int handle = ::open(nativePath.c_str(), O_RDWR);

		if (handle == -1)
		{
			throw std::system_error(errno, std::system_category());
		}

		mHandle = static_cast<std::uintptr_t>(handle);
		mRemoveOnClose.internalize(nativePath);
		mWriteBuffered = false;
	}

	void Pipe::connectPipe(const char *name)
	{
		this->clear();
		
		cio::Filesystem fs;
		cio::Path fullPath = fs.getTemporaryDirectory() / name;
		std::string nativePath = fullPath.toNativeFile();
		int handle = ::open(nativePath.c_str(), O_RDWR);

		if (handle == -1)
		{
			throw std::system_error(errno, std::system_category());
		}

		mHandle = static_cast<std::uintptr_t>(handle);
		mWriteBuffered = false;
	}


#else
#error "No cio::file::Pipe backend available on your platform, please implement one"
#endif
}

