/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "New.h"

#include "Metaclass.h"

#include <cstring>
#include <new>

namespace cio
{
	void *New::allocate(std::size_t bytes)
	{
		void *data = nullptr;
		if (bytes)
		{
			data = ::operator new(bytes);
			std::memset(data, 0, bytes);
		}
		return data;
	}

	std::nullptr_t New::deallocate(void *data, std::size_t bytes) noexcept
	{
		::operator delete(data);
		return nullptr;
	}

	void *New::reallocate(void *data, std::size_t copied, std::size_t allocated)
	{
		// C++ doesn't have realloc so we have to do this the hard way
		void *result = data;
		if (copied != allocated)
		{
			if (!allocated)
			{
				::operator delete(data);
				result = nullptr;
			}
			else if (!data || !copied)
			{
				result = ::operator new(allocated);
				std::memset(result, 0, allocated);
			}
			else
			{
				result = ::operator new(allocated);
				std::memcpy(result, data, std::min(copied, allocated));
				if (copied < allocated)
				{
					std::memset(static_cast<char *>(result) + copied, 0, allocated - copied);
				}
				::operator delete(data);
			}
		}
		return result;
	}

	void *New::duplicate(const void *data, std::size_t bytes)
	{
		void *output = nullptr;
		if (bytes)
		{
			output = ::operator new(bytes);
			if (data)
			{
				std::memcpy(output, data, bytes);
			}
			else
			{
				std::memset(output, 0, bytes);
			}
		}
		return output;
	}

	void *New::duplicate(const void *data, std::size_t copied, std::size_t allocated)
	{
		void *output = nullptr;
		if (allocated)
		{
			output = ::operator new(allocated);
			if (data)
			{
				if (copied)
				{
					std::memcpy(output, data, copied);
				}

				if (copied < allocated)
				{
					std::memset(static_cast<char *>(output) + copied, 0, allocated - copied);
				}
			}
			else
			{
				std::memset(output, 0, allocated);
			}
		}
		return output;
	}

	void *New::create(const Metaclass *type, std::size_t count)
	{
		void *result = nullptr;
		if (type && count)
		{
			result = ::operator new(type->size() * count);
			type->construct(result, count);
		}
		return result;
	}

	std::nullptr_t New::destroy(const Metaclass *type, void *data, std::size_t count) noexcept
	{
		if (type && data && count)
		{
			type->destroy(data, count);
			::operator delete(data);
		}
		return nullptr;
	}
	
	void *New::copy(const Metaclass *type, const void *data, std::size_t count)
	{
		void *result = nullptr;

		if (type && count)
		{
			result = ::operator new(type->size() * count);
			type->initialize(data, count, result, count);
		}

		return result;
	}

	void *New::copy(const Metaclass *type, const void *data, std::size_t copied, std::size_t allocated)
	{
		void *result = nullptr;

		if (type && allocated)
		{
			result = ::operator new(type->size() * allocated);
			type->initialize(data, copied, result, allocated);
		}

		return result;
	}

	void *New::move(const Metaclass *type, void *data, std::size_t moved, std::size_t allocated)
	{
		void *result = nullptr;

		if (type && allocated)
		{
			result = ::operator new(type->size() * allocated);
			type->initialize(data, moved, result, allocated);
		}

		return result;
	}

	void *New::resize(const Metaclass *type, void *data, std::size_t current, std::size_t desired)
	{
		void *result = nullptr;
		if (type)
		{
			if (current != desired)
			{
				if (!desired)
				{
					type->destroy(data, current);
					::operator delete(data);
					result = nullptr;
				}
				else if (!data || !current)
				{
					result = ::operator new(type->size() * desired);
					type->construct(result, desired);
				}
				else
				{
					result = ::operator new(type->size() * desired);
					type->initialize(data, current, result, desired);
					::operator delete(data);
				}
			}
			else
			{
				result = data;
			}
		}

		return result;
	}
}