/*==============================================================================
 * Copyright 2023-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_JSONWRITER_H
#define CIO_JSONWRITER_H

#include "Types.h"

#include <cio/MarkupWriter.h>

#include <vector>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The JSON Writer simplifies writing JSON documents by implementing the general Markup Writer interface.
	 * It makes it possible to write the logical content of the document without worrying about the JSON syntax
	 * and escaping special characters.
	 */
	class CIO_API JsonWriter : public MarkupWriter
	{
		public:
			/**
			 * Checks whether the given JSON character needs an escape sequence.
			 *
			 * @param c The character under consideration
			 * @return the number of characters in the escape sequence, or 1 to indicate the character is directly printable
			 */
			static std::size_t needsEscape(char c) noexcept;

			/**
			 * Prints the given character to a buffer with JSON escaping if needed.
			 *
			 * @param c The character to print
			 * @param buffer The buffer to print to
			 * @param capacity The capacity of the buffer
			 * @return the number of bytes needed to print the character
			 */
			static std::size_t printEscape(char c, char *buffer, std::size_t capacity) noexcept;

			/**
			 * Construct a JSON writer.
			 *
			 * @param stream The underlying output stream if any
			 */
			explicit JsonWriter(Pointer<Output> stream = nullptr);

			/**
			 * Construct a JSON writer with a preconfigured write buffer.
			 *
			 * @param buffer The write buffer
			 * @param stream The underlying output stream if any
			 */
			explicit JsonWriter(Buffer buffer, Pointer<Output> stream = nullptr);

			/**
			 * Construct a JSON writer with a preconfigured statically allocated write buffer.
			 *
			 * @tparam N The number of characters in the static storage array
			 * @param storage The storage for the write buffer
			 * @param stream The underlying output stream if any
			 */
			template <std::size_t N>
			inline explicit JsonWriter(char(&storage)[N], Pointer<Output> stream = nullptr) :
				JsonWriter(Buffer(storage, N), std::move(stream))
			{
				// nothing more to do
			}

			/**
			 * Construct a JSON writer with a preconfigured fixed-length write buffer.
			 * 
			 * @param storage The storage for the write buffer
			 * @param capacity The capacity of the write buffer
			 */
			JsonWriter(char *storage, std::size_t capacity);

			/**
			 * Destructor. Attempts to flush all uncommitted writes, but will not throw if that fails.
			 */
			virtual ~JsonWriter() noexcept override;

			/**
			 * Starts a new document.
			 * For JSON, this writes a leading '{' to start the anonymous top level element.
			 *
			 * @return the status of this operation
			 */
			virtual State startDocument() override;

			/**
			 * Ends the current document.
			 * For JSON, this writes a leading '}' to end the anonymous top level element.
			 *
			 * @return the status of this operation
			 */
			virtual State endDocument() override;

			/**
			 * Start a new element in the document.
			 *
			 * If this is the first child of the parent element, the initial '{' for the parent will be written.
			 * Otherwise, it will be separated from the previous child by a ','.
			 *
			 * @param element The element to start
			 * @return the status of this operation
			 */
			virtual State startElement(Text element) override;

			/**
			 * End the current element.  Only valid after calling start element.
			 * This will write the terminating '}' for the element if any children were written.
			 * Otherwise, the element will be given the "null" value.
			 *
			 * @return the status of this operation
			 */
			virtual State endElement() override;

			/**
			 * Start a new attribute in the document.
			 *
			 * While JSON treats attributes and elements the same, per the logical model attributes can only
			 * be direct children of elements.
			 *
			 * If this is the first child of the parent element, the initial '{' for the parent will be written.
			 * Otherwise, it will be separated from the previous child by a ','.
			 *
			 * @param key The element to start
			 * @return the status of this operation
			 */
			virtual State startAttribute(Text key) override;

			/**
			 * Ends the current attribute in the document.
			 *
			 * @return the status of this operation
			 */
			virtual State endAttribute() override;

			/**
			 * Pull in base class writeValue overloads if not otherwise overridden.
			 */
			using MarkupWriter::writeValue;	
			
			/**
			 * Writes a text value for the current item.
			 *
			 * This method handles escaping the text characters that cannot be in a JSON text string
			 * and adds initial quote if not already done.
			 *
			 * @param value The value
			 * @return the status of this operation
			 */
			virtual State writeValue(const Text &value) override;

			/**
			 * Writes a real value for the current item.
			 *
			 * Special values like infinity and NaN will result in null being written since JSON
			 * does not support those.
			 *
			 * @param value The value
			 * @return the status of this operation
			 */
			virtual State writeValue(float value) override;

			/**
			 * Writes a real value for the current item.
			 *
			 * Special values like infinity and NaN will result in null being written since JSON
			 * does not support those.
			 *
			 * @param value The value
			 * @return the status of this operation
			 */
			virtual State writeValue(double value) override;

			/**
			 * Writes raw byte data for the currently active item.
			 * This is encoded as a text string containing hexadecimal byte digits.
			 * If the bytes array is null, the text "null" is written.
			 * If the length is zero, the empty string is written.
			 *
			 * @param value The byte array
			 * @param length The number of bytes to write
			 * @return the status of this operation
			 */
			virtual State writeValue(const void *bytes, std::size_t length) override;

			/**
			 * Writes an empty value for the current item.
			 * If the current item is not otherwise specified or written, this writes out "null".
			 * 
			 * @return the status of this operation
			 */
			virtual State writeEmptyValue() override;

			/**
			 * Attempts to start a value for the given value type.
			 * If the current level is already compatible with this value type, it is reused.
			 * Otherwise, a new level is created with the given value type.
			 *
			 * If the current level is a text type and nothing has yet been written to it, the initial quote
			 * will be written.
			 *
			 * JSON does not support comments or processing instructions. The appropriate markup levels will
			 * still be created, but this method will return a failure state with Reason::Unsupported so the
			 * text writes will be filtered out.
			 *
			 * @param type The value type
			 * @return the status of this operation
			 */
			virtual State startValue(Type type = Type::None) override;

			/**
			 * Attempts to end the current value.
			 * If the current value is a text type, the ending quote will be added.
			 *
			 * @return the status of this operation
			 */
			virtual State endValue() override;

			/**
			 * Writes a text value for the current item.  Note:  This does not start or end the value,
			 * it only writes the value.
			 *
			 * This method handles escaping the text characters that cannot be in a JSON text string
			 * and adds initial quote if not already done.
			 *
			 * @param value The value
			 * @return the status of this operation
			 */
			virtual void writeQuotedString(Text value);

			/**
			 * Starts a comment. JSON does not support comments, so this turns on write filtering to ensure
			 * the writer still works fine with the user's code.
			 *
			 * @return the state of the operation
			 */
			virtual State startComment() override;

			/**
			 * Ends a comment. JSON does not support comments, so this turns off write filtering to restore
			 * writing out content for supported elements.
			 *
			 * @return the state of the operation
			 */
			virtual State endComment() override;

			/**
			 * Starts a processing instruction. JSON does not support istructions, so this turns on write filtering to ensure
			 * the writer still works fine with the user's code.
			 *
			 * @param key The processing instruction key
			 * @return the state of the operation
			 */
			virtual State startInstruction(Text key) override;

			/**
			 * Ends a comment. JSON does not support instructions, so this turns off write filtering to restore
			 * writing out content for supported elements.
			 *
			 * @return the state of the operation
			 */
			virtual State endInstruction() override;

			/**
			 * Starts processing an array as a JSON value. The size and type is purely advisory for JSON.
			 * If the current document state is valid for creating an array, the leading '[' will be encoded.
			 * 
			 * @param count The maximum number of array elements
			 * @param type The array element type
			 * @return the status of this operation
			 */
			virtual State startArray(std::size_t count = SIZE_MAX, Type type = Type::None) override;

			/**
			 * Ends processing the current array JSON value.
			 * If the document is currently in an array value, that value will be ended first.
			 * If the current document state is inside an array, the leading ']' will be encoded.
			 *
			 * @return the status of this operation
			 */
			virtual State endArray() override;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
