/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PROTOCOL_H
#define CIO_PROTOCOL_H

#include "Types.h"

#include "Chunk.h"
#include "ModeSet.h"
#include "Progress.h"
#include "ProtocolFilter.h"
#include "Traversal.h"

#include <functional>
#include <memory>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Protocol interface describes the methodology for obtaining inputs, outputs, and channel for resources, traversing resource hierarchies,
	 * and obtaining metadata about resources. As such, it also provides the primary interface for directories.
	 *
	 * The primary inputs to the CIO methods are either URI paths or resource Metadata.
	 * Examples of protocol implementations include the local filesystem via Filesystem and the Internet via net::Protocol.
	 */
	class CIO_API Protocol
	{
		public:
			/**
			 * Gets the metaclass for the base Protocol class.
			 *
			 * @return the declared metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			 * Checks whether the given filename entry is one of the two generic special directories '.' for current
			 * directory and '..' for parent directory.
			 *
			 * If the filename is one of these two entries, Resource::Directory is returned.
			 * Otherwise Resource::None is returned.
			 *
			 * @param filename the filename
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			static Resource checkForSpecialDirectories(const Text &filename) noexcept;

			/**
			 * Implementation of cross-protocol copy.
			 *
			 * @param input The input path
			 * @param inputProto The input protocol
			 * @param output The output path
			 * @param outputProto The output protocol
			 * @param depth The copy depth
			 * @param createParents Whether to create missing parents
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 * @throw Exception If copying failed
			 */
			static Progress<std::uint64_t> crossCopy(const Path &input, const Protocol &inputProto, const Path &output, Protocol &outputProto, Traversal depth = Traversal::Recursive, Traversal createParents = Traversal::Recursive);
			
			/**
			 * Implementation of cross-protocol move.
			 *
			 * @param input The input path
			 * @param inputProto The input protocol
			 * @param output The output path
			 * @param outputProto The output protocol
			 * @param createParents Whether to create missing parents
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 * @throw Exception If copying failed
			 */
			static Progress<std::uint64_t> crossMove(const Path &input, Protocol &inputProto, const Path &output, Protocol &outputProto, Traversal createParents = Traversal::Recursive);

			/**
			 * Construct an empty protocol.
			 */
			Protocol() noexcept;

			/**
			 * Construct a protocol initialized with the given default protocol prefix.
			 *
			 * @param prefix The default protocol prefix for relative paths
			 */
			explicit Protocol(Text prefix) noexcept;

			/**
			 * Construct a protocol initialized with the given filters.
			 * Typically these filters are specified as static members on relevant subclasses.
			 * 
			 * @param prefix The default protocol prefix for relative paths
			 * @param filters The protocol filters to use
			 */
			Protocol(Text prefix, Chunk<ProtocolFilter> filters) noexcept;

			/**
			 * Copy constructor.
			 * 
			 * @param in The protocol to copy
			 */
			Protocol(const Protocol &in);

			/**
			 * Move constructor.
			 * 
			 * @param in The protocol to move
			 */
			Protocol(Protocol &&in) noexcept;

			/**
			 * Copy assignment.
			 * 
			 * @param in The protocol to copy
			 * @return this protocol
			 */
			Protocol &operator=(const Protocol &in);

			/**
			 * Move assignment.
			 * 
			 * @param in The protocol to move
			 * @return this protocol
			 */
			Protocol &operator=(Protocol &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Protocol() noexcept;

			/**
			 * Gets the runtime metaclass for this object.
			 *
			 * @return the runtime metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept;

			/**
			 * Clears any state on the Protocol itself.
			 */
			virtual void clear() noexcept;

			/**
			 * Opens the Protocol to read entries from the given resource and obtains the first entry.
			 *
			 * The base implementation acts as a permanently empty directory and
			 * will always succeed but returns empty modes and has no entries.
			 *
			 * @param resource The resource to open
			 * @return the actual resource modes that the directory could be opened with
			 * @throw Exception If the directory could not be opened for any reason
			 */
			ModeSet open(const Path &resource);

			/**
			 * Opens the Protocol to read entries from the given resource and obtains the first entry.
			 *
			 * The base implementation acts as a permanently empty directory and
			 * will always succeed but returns empty modes and has no entries.
			 *
			 * @param resource The resource to open
			 * @param modes The open modes
			 * @param factory The protocol factory to use
			 * @return the actual resource modes that the resource could be opened with
			 * @throw Exception If the directory could not be opened for any reason
			 */
			virtual ModeSet openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory);

			// Protocol state

			/**
			 * Gets the default protocol prefix to assume for relative paths.
			 * 
			 * @return the default prefix
			 */
			Text getDefaultPrefix() const noexcept;

			/**
			 * Sets the default protocol prefix to assume for relative paths.
			 * Setting to the empty string will disable matching of relative paths.
			 *
			 * @param text the default prefix
			 */
			void setDefaultPrefix(Text text);

			/**
			 * Gets the primary file extension supported by this protocol class.
			 * This is the first entry in the extension list.
			 * The wildcard '*' means that all extensions are supported and none are primary.
			 * The empty string means that the primary extension is to not have an extension.
			 *
			 * @return the primary protocol prefix
			 */
			Text getPrimaryExtension() const noexcept;

			/**
			 * Gets the full list of protocol filters in use for this protocol.
			 * This describes for each type of resource which protocol prefixes and extensions may be used.
			 * 
			 * @return the filter list
			 */
			const Chunk<ProtocolFilter> &getFilterList() const noexcept;

			// Protocol filter API

			/**
			 * Sets the full list of protocol filters in use for this protocol.
			 * This describes for each type of resource which protocol prefixes and extensions may be used.
			 * 
			 * Typically a default static list is set up by each protocol class and this should not be changed.
			 * However, this can be used to cover gaps in the default assumptions and this is also used specifically
			 * by the Protocol Factory.
			 * 
			 * Subclasses can override to do more with this information such as set up indexing to optimize searches.
			 *
			 * @param filters the filters to set
			 */
			virtual void addFilterList(Chunk<ProtocolFilter> filters);

			/**
			 * CLears the current filter list.
			 * 
			 * Subclasses can override to also update any related search indexing.
			 */
			virtual void clearFilterList() noexcept;

			/**
			 * Finds the filter that most closely matches the given path for the given resource type.
			 * A default "empty" filter is returned if no match occurred.
			 * Subclasses can override for more specific check procedures or to optimize performance.
			 * 
			 * @param path The path of interest
			 * @param type the resource type
			 * @return the best matching filter
			 */
			virtual const ProtocolFilter &findBestFilter(const Path &path, Resource type = Resource::Unknown) const noexcept;

			/**
			 * Checks whether this protocol can handle the given path URI for the given intended resource type.
			 * 
			 * The base implementation checks to see if any filter matches the given request.
			 * Subclasses can override for more specific check procedures or to optimize performance.
			 * 
			 * If the type is specified as anything other than Resource::Unknown, this may match specific filters for the type.
			 * Otherwise, only generic matches will be used.
			 *
			 * @param path The path
			 * @param type The desired type, or Resource::Unknown to check in general
			 * @return whether this protocol can handle this input
			 */
			virtual bool compatible(const Path &path, Resource type = Resource::Unknown) const noexcept;

			/**
			 * Opens the most specific protocol that can handle this path.
			 * 
			 * The base implementation calls readProtocol.
			 * 
			 * @param path The path to the resource of interest
			 * @param type The desired type, or Resource::Unknown to check in general
			 * @return the best protocol to use with the given resource
			 * @throw Exception If no protocol could handle the path or creating it failed
			 */
			virtual std::unique_ptr<Protocol> openProtocol(const Path &path, Resource type = Resource::Unknown);

			/**
			 * Opens the most specific protocol that can handle this path in read-only mode.
			 *
			 * The base implementation returns null.
			 *
			 * @param path The path to the resource of interest
			 * @param type The desired type, or Resource::Unknown to check in general
			 * @return the best protocol to use with the given resource
			 * @throw Exception If no protocol could handle the path or creating it failed
			 */
			virtual std::unique_ptr<Protocol> readProtocol(const Path &path, Resource type = Resource::Unknown) const;

			// Path Metadata API

			/**
			 * Gets the metadata about the given resource.
			 * The metadata includes the modes and permissions, overall category, and content length.
			 * It also includes the canonical URI for the resource, following any links or redirects.
			 * If the resource does not actually exist, the returned metadata will reflect that.
			 *
			 * @param input the URI to the resource
			 * @return the metadata about the resource
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Metadata readMetadata(const Path &input) const;

			/**
			 * Creates an empty resource using metadata as a template.
			 *
			 * The base implementation will check to see if the resource is a file or a directory and call the appropriate
			 * createFile or createDirectory method with the resource path and modes.
			 *
			 * Subclasses may override to support creating other kinds of resources.
			 *
			 * @param resource The resource metadata describing the URI, permissions, and other parameters for the resource
			 * @param createParents Whether to create missing parent resources and if so whether to do it recursively
			 * @return the metadata describing the actually created resource
			 * @throw Exception If any error (other than the resource already existing) prevented creating the resource
			 */
			virtual Metadata create(const Metadata &resource, Traversal createParents = Traversal::Recursive);

			/**
			 * Gets the overall resource type for a given resource.
			 * This is a subset of the metadata.
			 *
			 * @note
			 * This method may be implemented separately for performance.
			 * The base implementation on Protocol gets the metadata and returns the appropriate field.
			 *
			 * @param path The resource path
			 * @return the overall resource type for the resource, or None if it does not exist
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Resource readType(const Path &path) const;

			/**
			 * Gets the resource available access modes for the given resource, as a subset of the desired modes.
			 * This is a subset of the metadata.
			 *
			 * @note
			 * This method may be implemented separately for performance.
			 * The base implementation on Protocol gets the metadata and returns the appropriate field.
			 *
			 * @param path The resource path
			 * @param desired The desired access modes
			 * @return the resource modes and permissions
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual ModeSet readModes(const Path &path, ModeSet desired = ModeSet::general()) const;

			/**
			 * Gets the total logical content length for a given resource.
			 * For inputs and outputs, this is the logical content size in bytes.
			 * For directories this is the number of directory entries.
			 *
			 * @note
			 * This method may be implemented separately for performance.
			 * The base implementation on Protocol gets the metadata and returns the appropriate field.
			 *
			 * @param path The resource path
			 * @return the overall content length for the resource, or CIO_UNKNOWN_LENGTH if not known
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented getting the metadata
			 */
			virtual Length readSize(const Path &path) const;

			/**
			 * Checks whether the given filename entry is considered a "special" entry for this protocol.
			 * This should work even if the protocol is not currently opened, although it may change behavior
			 * if opened on a particular device or resource.
			 *
			 * The polymorphic version here is the fully general version. Protocol defines other overloads with
			 * the special() method for convenience.
			 *
			 * If the entry is somehow special, the general category of item it represents should be returned.
			 *
			 * The base implementation always considers "." and ".." to be special with Resource::Directory.
			 * Subclasses may override to add additional types of special entries. For example, on a Windows filesystem
			 * a number of special filename entries represent legacy devices like printers and serial ports.
			 *
			 * @param entry The filename entry, which is not necessarily null terminated
			 * @param length The maximum number of characters to consider in the entry
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			virtual Resource checkForSpecialFilename(const Text &filename) const noexcept;

			/**
			 * Checks whether the current filename entry is considered a "regular" entry for this protocol.
			 *
			 * This method just calls checkForSpecialFilename and verifies it is not special.
			 *
			 * @return whether te current entry is a regular entry
			 */
			inline bool regular() const noexcept;

			/**
			 * Checks whether the current filename entry is considered a "special" entry for this protocol.
			 *
			 * This method just calls checkForSpecialFilename.
			 *
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			inline Resource special() const noexcept;

			/**
			 * Checks whether the given filename entry is considered a "special" entry for this protocol.
			 * This should work even if the protocol is not currently opened, although it may change behavior
			 * if opened on a particular device or resource.
			 *
			 * This method just calls checkForSpecialFilename.
			 *
			 * @param entry The null-terminated text entry
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			inline Resource special(const char *entry) const noexcept;

			/**
			 * Checks whether the given filename entry is considered a "special" entry for this protocol.
			 * This should work even if the protocol is not currently opened, although it may change behavior
			 * if opened on a particular device or resource.
			 *
			 * This method just calls checkForSpecial.
			 *
			 * @param entry The text entry
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			inline Resource special(const std::string &text) const noexcept;

			/**
			 * Checks whether the given filename entry is considered a "special" entry for this protocol.
			 * This should work even if the protocol is not currently opened, although it may change behavior
			 * if opened on a particular device or resource.
			 *
			 * This method just calls checkForSpecial.
			 *
			 * @param entry The text entry
			 * @return the type of special entry, or Resource::None if the entry has no special significance
			 */
			inline Resource special(const Text &text) const noexcept;

			// Current directory management
			
			/**
			* Gets the path for the current working directory.
			* This may be a global state (as is the case for Filesystem) or specific to this object instance.
			* It is primarily used to resolve relative paths for this object.
			* 
			* @return the path for the current working directory at the time of this call
			* @throw std::bad_alloc If the path was known but memory could not be allocated for it
			* @throw std::runtime_error If the path itself could not be determined
			*/
			virtual Path getCurrentDirectory() const;

			/**
			* Sets the current working directory to the given path.
			* This may be a global state (as is the case for Filesystem) or specific to this object instance.
			* It is primarily used to resolve relative paths for this object.
			*
			* @param path The native file path for the new current working directory
			* @param createMissing If the directory does not currently exist, describe whether to create the immediate or all missing parent directories if possible
			* @throw Exception If setting the current directory failed
			*/
			virtual void setCurrentDirectory(const Path &path, Traversal createMissing = Traversal::None);

			/**
			* Gets the path for the given standard path type, if possible to determine.
			* This can be used to obtain the location of any StandardPath enumerated value
			* including the user home directory, documents directory, and so on.
			* 
			* In some cases, standard paths may be available in multiple resource types - for example StandardPath::Application
			* has different values for both Resource::File and Resource::Directory. In most cases only Resource::Directory is available however.
			* To get the default type for a particular path, use Resource::Unknown.
			* 
			* The base implementation always throws an exception indicating the path is missing, since this is very protocol-specific.
			*
			* @param path the type of standard path to obtain
			* @param type The type of resource desired for the standard path
			* @return the standard path if determined
			* @throw Exception if the path was missing or otherwise could not be determined
			*/
			virtual Path getStandardPath(StandardPath path, Resource type = Resource::Unknown) const;

			/**
			 * Gets the maximum depth of entries directly accessible by this Protocol listing.
			 * If the depth is 0, this protocol cannot be iterated or listed.
			 * If the depth is 1, only direct children of this protocol may be listed (typical of filesystem directories).
			 * If the depth is higher, entire subtrees may be listed via traversal.
			 *
			 * The base implementation returns 0.
			 *
			 * @return the traversal depth
			 */
			virtual Traversal depth() const noexcept;

			/**
			 * Checks whether the Protocol is currently open and usable for directory listing.
			 *
			 * @return whether the Protocol has an directory entry available
			 */
			virtual bool traversable() const noexcept;

			/**
			 * Gets the total number of entries in the Protocol listing if known.
			 * If unknown or potentially unbounded, the constant CIO_UNKNOWN_LENGTH is returned, which is
			 * also the largest possible std::int64_t size value.
			 *
			 * The base class returns CIO_UNKNOWN_LENGTH. Subclasses should override.
			 *
			 * @warning Many directory implementations don't know this a priori and must scan the entire directory.
			 *
			 * @return the total number of directory entries
			 */
			virtual Length size() const noexcept;

			/**
			 * Gets the theoretical maximum number of entries that could be in this Protocol listing.
			 *
			 * The base class returns CIO_UNKNOWN_LENGTH. Subclasses should override.
			 *
			 * @return the maximum possible number of directory entries
			 */
			virtual Length capacity() const noexcept;

			/**
			 * Gets the metadata of the current entry as reported by the underlying physical representation.
			 * Typically the path in the metadata will be a relative path to this directory.
			 *
			 * @return the metadata of the current entry
			 * @throw std::bad_alloc If the memory for the metadata could not be allocated
			 */
			virtual Metadata current() const;

			/**
			 * Gets the current raw filename entry exactly as reported by the underlying physical representation.
			 * The entry is not necessarily null-terminated, probably will be relative, and may only be valid until the next call to next() or clear().
			 * Typically this is the most efficient way to get the filename.
			 *
			 * The base Protocol implementation acts as a permanently empty directory and will always return the empty string.
			 *
			 * The filename entry may contain multiple levels of hierarchy such as internal subdirectories if
			 * the underlying directory implementation allows that. For example, Zip archives may contain internal sudirectories,
			 * but traditional filesystem directories do not.
			 *
			 * @return the current filename entry
			 */
			virtual Text filename() const;

			/**
			 * Advances to the next directory entry if possible.
			 * The returned State will either succeed if an entry is available or fail with Reason::Underflow if not.
			 * If a transport error or other failure to read the directory itself occurs, an Exception is thrown with the information
			 * about the error.
			 *
			 * The base implementation acts as a permanently empty directory and will always underflow.
			 *
			 * @return the outcome of advancing to the next directory entry
			 * @throw Exception If a transport error occurred that prevented reading the directory
			 */
			virtual State next();

			/**
			 * Seeks back to the first directory entry.
			 *
			 * @return the oucome of rewinding to the first entry, which is either successful or fails with Reason::Underflow if no entries are present
			 * @throw Exception If a transport error occurred that prevented reading the directory
			 */
			virtual State rewind();

			/**
			 * Seeks the directory to the given entry described as a relative path.
			 *
			 * @return the outcome of this operation, which is either successful or fails with Reason::Missing if the entry is not present
			 * @throw Exception If seeking failed for any reason other than the entry not existing
			 * */
			virtual State seekToEntry(const Path &relative);

			/**
			 * Examines the given resource as a directory to enumerate all of its children.
			 * This may consider only immediate children or recursively scan all children.
			 *
			 * @note
			 * This method may be implemented separately for performance.
			 * The base implementation on Protocol creates a lambda receiver to build the returned vector using enumerateDirectory.
			 *
			 * @param path The path to the directory resource
			 * @param depth The mode describing whether to only capture immediate children or capture all children recursively
			 * @return the list of all discovered children
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented enumerating the children
			 * @throw std::bad_alloc If not enough memory was available to allocate the returned vector
			 */
			virtual std::vector<Path> listDirectory(const Path &path, Traversal depth = Traversal::Immediate) const;

			/**
			 * Examines the given resource as a directory to enumerate all of its children.
			 * This may consider only immediate children or recursively scan all children.
			 * The given receiver callback is used to receive each child as it is discovered.
			 *
			 * The receiver can end traversal early by returning false or by throwing an exception.
			 *
			 * The base implementation uses openDirectory as needed to scan the passed path and, for recursive traversal,
			 * for the child paths.
			 *
			 * If the traversal mode is Traversal::None, no action is taken regardless of implementation.
			 * For Traversal::Immediate, only the immediate children of the directory are enumerated.
			 * For Traversal::Recursive, all child directories are recursively enumerated.
			 *
			 * @warning This method will not properly deal with infinite redirects or link loops
			 * if symbolic links are involved and will either run endlessly or crash due to memory use.
			 *
			 * @param path The path to the directory resource
			 * @param depth The depth describing whether to only capture immediate children or capture all children recursively
			 * @param receiver The function callback to receive each discovered child and return whether to keep enumerating
			 * @return the total number of discovered children
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented enumerating the children
			 * @throw std::exception The receiver may throw any exception to indicate an error
			 */
			virtual std::size_t enumerateDirectory(const Path &path, Traversal depth, const std::function<bool(const Path &)> &receiver) const;

			// Relative methods for current entry

			/**
			 * Create an Input subclass instantiation that can read the content for the current directory entry.
			 *
			 * If the current directory entry is not readable, was asynchronously deleted, or is a subdirectory, this method should throw
			 * a Exception indicating that.
			 *
			 * The base implementation using the current filename to construct a relative path, then calls openInput.
			 * Subclasses may override to implement this more directly.
			 *
			 * @param modes The desired open modes
			 * @return the opened Input
			 * @throw Exception If the entry did not exist, was not readable, was a subdirectory, or otherwise could not be opened
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Input> openCurrentInput(ModeSet modes = ModeSet::readonly()) const;

			/**
			 * Create an Output subclass instantiation that can read the content for the current directory entry.
			 *
			 * If the current directory entry is not writable, was asynchronously deleted, or is a subdirectory, this method should throw
			 * a Exception indicating that.
			 *
			 * The base implementation using the current filename to construct a relative path, then calls openOutput.
			 * Subclasses may override to implement this more directly.
			 *
			 * @param modes The desired open modes
			 * @return the opened Output
			 * @throw Exception If the entry did not exist, was not readable, was a subdirectory, or otherwise could not be opened
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Output> openCurrentOutput(ModeSet modes = ModeSet::writeonly());

			/**
			 * Create a Channel subclass instantiation that can read and write the content for the current directory entry.
			 *
			 * If the current directory entry is not accessible, was asynchronously deleted, or is a subdirectory, this method should throw
			 * a Exception indicating that.
			 *
			 * The base implementation using the current filename to construct a relative path, then calls openChannel.
			 * Subclasses may override to implement this more directly.
			 *
			 * @param modes The desired open modes
			 * @return the opened Channel
			 * @throw Exception If the entry did not exist, was not readable, was a subdirectory, or otherwise could not be opened
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Channel> openCurrentChannel(ModeSet modes = ModeSet::general());

			/**
			 * Create a Protocol subclass instantiation that is opened to seek through the current subdirectory read-only.
			 *
			 * If the current directory entry is not accessible, was asynchronously deleted, or is not a subdirectory, this method should throw
			 * a Exception indicating that.
			 *
			 * The base implementation uses the current filename to construct a relative path, then calls openDirectory.
			 * Subclasses may override to implement this more directly.
			 *
			 * @return the opened directory as a protocol
			 * @throw Exception If the entry did not exist, was not readable, was not a subdirectory, or otherwise could not be opened
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Protocol> readCurrentSubdirectory(ModeSet modes = ModeSet::readonly()) const;

			/**
			 * Create a Protocol subclass instantiation that is opened to seek through the current subdirectory.
			 *
			 * If the current directory entry is not accessible, was asynchronously deleted, or is not a subdirectory, this method should throw
			 * a Exception indicating that.
			 *
			 * The base implementation uses the current filename to construct a relative path, then calls openDirectory.
			 * Subclasses may override to implement this more directly.
			 *
			 * @return the opened directory as a protocol
			 * @throw Exception If the entry did not exist, was not readable, was not a subdirectory, or otherwise could not be opened
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Protocol> openCurrentSubdirectory(ModeSet modes = ModeSet::general());

			// Absolute methods for any path

			/**
			 * Removes and permanently erases (if possible) the resource at the given path.
			 * The Traversal parameter controls what happens if the resource is a directory with children.
			 * Traversal::None will only erase the resource if it has no children.
			 * Traversal::Immediate will erase direct children of the resource (but not their children if non-empty).
			 * Traversal::Recursive will recursively erase every child resource.
			 *
			 * @param path The path to the resource to erase
			 * @param depth The depth controlling whether to erase non-empty directories and if so whether to erase only immediate children
			 * or all children recursively
			 * @return the total number of resources that were erased
			 * @throw Exception If transport failures or other problems (other than the resource not existing) prevented erasure
			 */
			virtual Progress<std::uint64_t> erase(const Path &path, Traversal depth = Traversal::None);

			/**
			 * Creates the CIO Protocol implementation most suitable for the given path as a directory, and then returns
			 * an opened directory suitable for scanning directory entries read-only.
			 *
			 * The base implementation returns null.
			 * 
			 * Modes that can edit the underlying content will be discarded.
			 *
			 * @param path The path to the directory
			 * @param modes The modes to use to open the resource
			 * @return the opened directory
			 * @throw Exception If the directory could not be opened for any reason
			 * @throw std::bad_alloc If memory allocation failed
			 */
			virtual std::unique_ptr<Protocol> readDirectory(const Path &path, ModeSet modes = ModeSet::readonly()) const;

			/**
			 * Creates the CIO Protocol implementation most suitable for the given path, and then returns
			 * an opened directory suitable for scanning directory entries and modifying its contents.
			 * 
			 * The base implementation returns readDirectory.
			 *
			 * If the modes include ModeSet::Create, the directory may be created if it is not present.
			 * 
			 * @param path The path to the directory
			 * @param modes The modes to use to open the resource
			 * @return the opened directory
			 * @throw Exception If the directory could not be opened for any reason
			 * @throw std::bad_alloc If memory allocation failed
			 */
			virtual std::unique_ptr<Protocol> openDirectory(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::None);
			
			/**
			 * Creates an empty directory using this protocol at the given path with the given modes, optionally also creating parent resources.
			 *
			 * The base implementation validates that parent resources exist (if requested) then calls open directory with the
			 * specified modes plus ModeSet::Create, afterward closing the directory.
			 *
			 * @param path The path of the directory to create
			 * @param modes The desired modes of the directory
			 * @param createParents Whether to create missing parent diretories and if so recursively
			 * @return the outcome of the operation
			 * @throw Exception If directory creation failed for any reason other than the directory already existing
			 */
			virtual State createDirectory(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive);
			
			/**
			 * Creates any missing parent resources for the given path, up to the given traversal depth.
			 *
			 * @param path The path of the resource
			 * @param depth The number of parents to create
			 * @return the outcome of this operation
			 */
			virtual State createParents(const Path &path, Traversal depth = Traversal::Recursive);
			
			/**
			 * Create an Input subclass instantiation that can read the content for the resource at the given path.
			 * 
			 * The base implementation delegates to openChannel to use only the Input portion of the returned Channel.
			 *
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open the resource
			 * @return the opened input
			 * @throw Exception If transport problems or lack of existence of the resource prevented creating an Input
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Input> openInput(const Path &path, ModeSet modes = ModeSet::readonly()) const;

			/**
			 * Create an Output subclass instantiation that can write the content for the resource at the given path.
			 * The resource will be created if it does not already exist, but the existing content will be preserved if it is
			 * a persistent resource.
			 *
			 * Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			 * 
			 * The base implementation delegates to createContentChannel to use only the Output portion of the returned Channel.
			 * 
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open or create the resource
			 * @param createParentResources Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the opened output
			 * @throw Exception If transport problems or inability to open or create the resource prevented creating an Output
			 * @throw std::bad_alloc If allocating the new Input failed
			 */
			virtual std::unique_ptr<Output> openOutput(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive);

			/**
			 * Create an Channel subclass instantiation that can both read and write the content for the resource at the given path.
			 * The resource will be created if it does not already exist, but the existing content will be preserved if it is
			 * a persistent resource.
			 *
			 * Optionally, the direct parent resource directory (or all parents recursively) may be created if missing.
			 * 
			 * The base implementation returns the null pointer. This method, and/or createContentInput and createContentOutput,
			 * must be overridden by subclasses to provide content I/O support.
			 *
			 * @param path The path to the resource to read
			 * @param modes The modes to use to open or create the resource
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the opened channel
			 * @throw Exception If transport problems or inability to open or create the resource prevented creating a Channel
			 * @throw std::bad_alloc If allocating the new Channel failed
			 */
			virtual std::unique_ptr<Channel> openChannel(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive);

			/**
			 * Creates an empty file using this protocol at the given path with the given modes, optionally also creating parent resources.
			 *
			 * The base implementation validates that parent resources exist (if requested) then calls openOutput() with the
			 * specified modes plus ModeSet::Create, afterward closing the output.
			 *
			 * @param path The path to the file to create
			 * @param modes The desired modes for the file
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome of this operation, which should either be a success or a failure with Reason::Exists
			 * @throw Exception If creating the file failed for any reason other than it already existing
			 */
			virtual State createFile(const Path &path, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive);

			/**
			 * Reads the target URI specified by the given path which is a link.
			 *
			 * @param path The path to the link
			 * @param depth How many links to traverse
			 * @return the path to the resource the link references
			 * @throw Exception If the given path didn't exist or wasn't a link
			 */
			virtual Path readLink(const Path &path, Traversal depth = Traversal::Immediate) const;
			
			/**
			 * Creates a link at the given link path that references the given target path.
			 *
			 * @param link The path of the link to create
			 * @param target The path the link should point to
			 * @param modes The modes for the created link
			 * @param createParents Whether to create missing parent directories
			 * @return the outcome of the operation
			 * @throw Exception If the given link path already existed or the link couldn't be created
			 */
			virtual State createLink(const Path &link, const Path &target, ModeSet modes = ModeSet::general(), Traversal createParents = Traversal::Recursive);

			/**
			 * Performs a direct copy from input to output path up to the specified traversal depth.
			 *
			 * The base implementation provides a generic algorithm that should work on any two inputs using the same protocol,
			 * although subclasses may override to provide more efficient implementations.
			 * 
			 * If it's a directory, then it will attempt to create the output as a directory, and then (if depth allows)
			 * enumerate its children and copy them recursively.
			 *
			 * When a file is encountered, an input and output will be opened and the input will be copied to the output using the
			 * recommended block size (or 4 KiB if unknown) buffers.
			 *
			 * @param input The input path
			 * @param output the output path
			 * @param depth The copy depth
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 */
			virtual Progress<std::uint64_t> copy(const Path &input, const Path &output, Traversal depth = Traversal::Recursive, Traversal createParents = Traversal::Recursive);
			
			/**
			 * Performs a direct move from input to output path.
			 *
			 * The base implementation provides a generic copy + erase algorithm that should work on any two inputs using the same protocol,
			 * although subclasses may override to provide more efficient implementations for direct moves.
			 *
			 * By definition a move must be fully recursive, unlike a copy.
			 *
			 * @param input The input path
			 * @param output the output path
			 * @param createParents Whether to create missing parent resources and, if so, whether to do it recursively
			 * @return the outcome and how many total resources were copied out of
			 * the examined resources
			 */
			virtual Progress<std::uint64_t> move(const Path &input, const Path &output, Traversal createParents = Traversal::Recursive);

			// Convenience methods
			
			/**
			 * Check to see if a path exists and is a regular file.
			 *
			 * @param path The path
			 * @return whether the path exists and is a regular file
			 */
			inline bool isFile(const Path &path) const noexcept;
			
			/**
			 * Check to see if a path exists and is a directory file.
			 *
			 * @param path The path
			 * @return whether the path exists and is a directory file
			 */
			inline bool isDirectory(const Path &path) const noexcept;
			
			/**
			 * Check to see if a path exists and is a link.
			 *
			 * @param path The path
			 * @return whether the path exists and is a link
			 */
			inline bool isLink(const Path &path) const noexcept;
			
			/**
			 * Check to see if a path exists regardless of type.
			 *
			 * @param path The path
			 * @return whether the path exists
			 */
			inline bool exists(const Path &path) const noexcept;

			/**
			 * Interprets whether the protocol is currently open when used in a Boolean context.
			 *
			 * @return whether the protocol is currently open
			 */
			explicit operator bool() const noexcept;

		protected:
			/** Text entry suitable for prefix or extensions consisting of just a wildcard */
			static const Text sWildcard;
			
			/** Empty protocol filter to return if nothing matches */
			static const ProtocolFilter sEmptyFilter;

			/** The default assumed protocol prefix for relative paths */
			Text mDefaultPrefix;

			/** List of protocol filters in use. */
			Chunk<ProtocolFilter> mFilters;

		private:
			/** The metaclass for this class */
			static Class<Protocol> sMetaclass;
	};
}

/* Inline implementation */

namespace cio
{
	inline bool Protocol::regular() const noexcept
	{
		return this->checkForSpecialFilename(this->filename()) == Resource::None;
	}

	inline Resource Protocol::special() const noexcept
	{
		return this->checkForSpecialFilename(this->filename());
	}

	inline Resource Protocol::special(const char *entry) const noexcept
	{
		Text text(entry);
		return this->checkForSpecialFilename(text);
	}

	inline Resource Protocol::special(const std::string &entry) const noexcept
	{
		return this->checkForSpecialFilename(Text::view(entry));
	}

	inline Resource Protocol::special(const Text &text) const noexcept
	{
		return this->checkForSpecialFilename(text);
	}

	inline bool Protocol::isFile(const Path &path) const noexcept
	{
		return this->readType(path) == Resource::File;
	}

	inline bool Protocol::isDirectory(const Path &path) const noexcept
	{
		return this->readType(path) == Resource::Directory;
	}

	inline bool Protocol::isLink(const Path &path) const noexcept
	{
		return this->readType(path) == Resource::Link;
	}

	inline bool Protocol::exists(const Path &path) const noexcept
	{
		return this->readType(path) != Resource::None;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

