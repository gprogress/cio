/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "DBusAttributeClient.h"

#include "DBusController.h"
#include "Characteristic.h"
#include "Peripheral.h"
#include "Property.h"
#include "Service.h"
#include "Subscription.h"
#include "TypeId.h"

#include <cio/DeviceAddress.h>
#include <cio/Exception.h>
#include <cio/Class.h>
#include <cio/Parse.h>
#include <cio/Print.h>
#include <cio/Response.h>
#include <cio/Status.h>
#include <cio/Variant.h>

#include <thread>

namespace cio
{
	namespace bluetooth
	{
		Class<DBusAttributeClient> DBusAttributeClient::sMetaclass("cio::bluetooth::DBusAttributeClient");

		const char *DBusAttributeClient::BLUEZ_DOMAIN = "org.bluez";
				
		const char *DBusAttributeClient::BLUEZ_ADAPTER_IFACE = "org.bluez.Adapter1";
		
		const char *DBusAttributeClient::BLUEZ_PERIPHERAL_IFACE = "org.bluez.Device1";
				
		const char *DBusAttributeClient::BLUEZ_SERVICE_IFACE = "org.bluez.GattService1";
				
		const char *DBusAttributeClient::BLUEZ_CHARACTERISTIC_IFACE = "org.bluez.GattCharacteristic1";
				
		const char *DBusAttributeClient::BLUEZ_DESCRIPTOR_IFACE = "org.bluez.GattDescriptor1";

		const Metaclass &DBusAttributeClient::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		DBusAttributeClient::DBusAttributeClient() noexcept
		{
			// nothing more to do
		}

		DBusAttributeClient::DBusAttributeClient(DBusAttributeClient &&in) noexcept :
			AttributeClient(std::move(in)),
			mSystemBus(std::move(in.mSystemBus)),
			mPeripheralPath(std::move(in.mPeripheralPath)),
			mServices(std::move(in.mServices))
		{
			// nothing more to do
		}
			
		DBusAttributeClient &DBusAttributeClient::operator=(DBusAttributeClient &&in) noexcept
		{
			AttributeClient::operator=(std::move(in));
			mSystemBus = std::move(in.mSystemBus);
			mPeripheralPath = std::move(in.mPeripheralPath);
			mServices = std::move(in.mServices);
			
			return *this;
		}
			
		DBusAttributeClient::~DBusAttributeClient() noexcept = default;
		
		const Metaclass &DBusAttributeClient::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void DBusAttributeClient::clear() noexcept
		{
			this->clearConnectionMonitor();
		
			mServices.clear();
			mPeripheralPath.clear();
			mSystemBus.clear();
		
			AttributeClient::clear();
		}
		
		void DBusAttributeClient::connect(const Peripheral &peripheral)
		{
			mSystemBus.open(BLUEZ_DOMAIN);
		
			char tmp[128];
			std::size_t actual = 0;
			
			// simplified query to just get some random device
			auto capture = [&](const char *object, const char *iface)
			{
				if (actual == 0)
				{
					actual = std::strlen(object);
					std::memcpy(tmp, object, actual);
				}
			};
			
			mSystemBus.findMatchingObjects(&BLUEZ_ADAPTER_IFACE, 1, capture);
			
			if (actual == 0)
			{
				throw cio::Exception(Reason::Missing, "No local adapter to connect with");
			}
			
			tmp[actual++] = '/';
			this->printPeripheralPathSuffix(peripheral.getAddress(), tmp + actual, 128);
			mPeripheralPath = tmp;
			
			this->applyConnectionMonitor();
		}
					
		void DBusAttributeClient::connect(const Device &adapter, const Peripheral &peripheral)
		{
			sMetaclass.info() << "Connecting to " << peripheral;
		
			mSystemBus.open(BLUEZ_DOMAIN);
		
			char tmp[128];
			this->printPeripheralPath(adapter, peripheral.getAddress(), tmp, 128);
			mPeripheralPath = tmp;
			
			this->applyConnectionMonitor();
		}
		
		bool DBusAttributeClient::isOpen() const noexcept
		{
			return mSystemBus.isOpen() && !mPeripheralPath.empty();
		}
				
		void DBusAttributeClient::disconnect() noexcept
		{
			this->clearConnectionMonitor();
		
			mServices.clear();
			mPeripheralPath.clear();			
			mSystemBus.clear();
			
			AttributeClient::disconnect();
		}
		
		UniqueId DBusAttributeClient::getAttributeType(std::uint16_t handle) 
		{
		 	UniqueId type;
		 	
		 	this->synchronizeServices();
		 	
		 	std::size_t i = this->locateServiceIndex(handle);
		 	if (i < mServices.size())
		 	{
		 		Service &service = mServices[i];
		 		if (service.getHandle() == handle)
		 		{
		 			type = service.getType();
		 		}
		 		else
		 		{
		 			std::size_t j = service.locateCharacteristicIndex(handle);
		 			if (j < service.size())
		 			{
		 				Characteristic &c = service[j];
		 				if (c.getHandle() == handle)
		 				{
		 					type = c.getType();
		 				}
		 				else if (c.getValueHandle() == handle)
		 				{
		 					type = c.getValueType();
						}
						else
						{
							std::size_t k = c.locateDescriptorIndex(handle);
							if (k < c.size())
							{
								Descriptor &d = c[k];
								type = d.getType();
							}
						}
		 			}
		 		}
		 	}
		 	
		 	return type;
		}
		 						
		std::vector<Attribute> DBusAttributeClient::discoverAttributes(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Attribute> attrs;
			
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> range = this->locateServiceRange(start, end);
			for (std::size_t i = range.first; i < range.second; ++i)
			{
				Service &service = mServices[i];
				if (service.isHandleContained(start, end))
				{
					attrs.emplace_back(service);
				}
				
				std::pair<std::size_t, std::size_t> crange = service.locateCharacteristicRange(start, end);
				for (std::size_t j = crange.first; j < crange.second; ++j)
				{
					Characteristic &c = service[j];
					if (c.isHandleContained(start, end))
					{
						attrs.emplace_back(c);
					}
					
					if (c.getValueDescriptor().isHandleContained(start, end))
					{
						attrs.emplace_back(c.getValueDescriptor());
					}
					
					std::pair<std::size_t, std::size_t> drange = c.locateDescriptorRange(start, end);
					for (std::size_t k = drange.first; k < drange.second; ++k)
					{
						attrs.emplace_back(c[k]);
					}
				}
			}
			
			return attrs;
		}
								
		std::uint16_t DBusAttributeClient::findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			std::uint16_t handle = 0;
			
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> range = this->locateServiceRange(start, end);
			
			if (type.getBluetoothId() == TypeId::PrimaryService)
			{
				for (std::size_t i = range.first; i < range.second; ++i)
				{
					Service &service = mServices[i];
					if (service.getType() == type)
					{
						handle = service.getHandle();
						break;
					}
				}
			}
			else
			{
				for (std::size_t i = range.first; i < range.second; ++i)
				{
					Service &service = mServices[i];
					std::pair<std::size_t, std::size_t> crange = service.locateCharacteristicRange(start, end);
					if (type.getBluetoothId() == TypeId::Characteristic)
					{
						if (crange.first < crange.second)
						{
							handle = service[crange.first].getHandle();
							break;
						}
					}
					else
					{
						for (std::size_t j = crange.first; j < crange.second; ++j)
						{
							Characteristic &c = service[j];
							
							if (c.getValueType() == type)
							{	
								handle = c.getValueHandle();
								range.second = i;
								break;
							}
							else
							{
								std::pair<std::size_t, std::size_t> drange = c.locateDescriptorRange(start, end);
								for (std::size_t k = drange.first; k < drange.second; ++k)
								{
									Descriptor &d = c[k];
									if (d.getType() == type)
									{
										handle = d.getHandle();
										crange.second = j;
										range.second = i;
										break;
									}
								}
							}
						}
					}
				}
			}
			
			return handle;
		}
	
		Service DBusAttributeClient::getService(std::uint16_t handle)
		{
			Service service;
			this->synchronizeServices();
			
			std::size_t idx = this->locateServiceIndex(handle);
			
			if (idx < mServices.size() && mServices[idx].getHandle() == handle)
			{
				service = mServices[idx];
			}
			
			return service;
		}

		std::vector<Service> DBusAttributeClient::discoverPrimaryServices(std::uint16_t start, std::uint16_t end)
		{	
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			return std::vector<Service>(mServices.begin() + idx.first, mServices.begin() + idx.second);
		}
		
		Service DBusAttributeClient::findPrimaryServiceInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			Service service;
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			
			for (std::size_t i = idx.first; i < idx.second; ++i)
			{
				const Service &candidate = mServices[i];
				if (candidate.getServiceType() == type)
				{
					service = candidate;
					break;
				}
			}

			return service;
		}

		std::vector<Characteristic> DBusAttributeClient::discoverCharacteristics(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Characteristic> chars;
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			for (std::size_t i = idx.first; i < idx.second; ++i)
			{
				const Service &service = mServices[i];
				std::pair<std::size_t, std::size_t> cidx = service.locateCharacteristicRange(start, end);
				for (std::size_t j = cidx.first; j < cidx.second; ++j)
				{
					chars.emplace_back(service[j]);
				}
			}
			
			return chars;
		}
								
		Characteristic DBusAttributeClient::getCharacteristic(std::uint16_t handle)
		{
			Characteristic c;
			this->synchronizeServices();
			
			std::size_t idx = this->locateServiceIndex(handle);
			if (idx < mServices.size())
			{
				const Service &service = mServices[idx];
				std::size_t cidx = service.locateCharacteristicIndex(handle);
				if (cidx < service.size())
				{
					const Characteristic &candidate = service[cidx];
					if (candidate.getHandle() == handle)
					{
						c = candidate;
					}
				}
			}
			
			return c;
		}
		
		Characteristic DBusAttributeClient::findCharacteristicInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			Characteristic c;
			
			this->synchronizeServices();
			
			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			for (std::size_t i = idx.first; i < idx.second; ++i)
			{
				Service &service = mServices[i];
				std::pair<std::size_t, std::size_t> cidx = service.locateCharacteristicRange(start, end);
				for (std::size_t j = cidx.first; j < cidx.second; ++j)
				{
					Characteristic &candidate = service[j];
					if (candidate.getValueType() == type)
					{
						c = candidate;
						idx.second = i;
						break;
					}
				}
			}
			
			return c;
		}

		std::vector<Descriptor> DBusAttributeClient::discoverDescriptors(std::uint16_t start, std::uint16_t end)
		{
			std::vector<Descriptor> attributes;
			
			this->synchronizeServices();

			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			for (std::size_t i = idx.first; i < idx.second; ++i)
			{
				Service &service = mServices[i];
				std::pair<std::size_t, std::size_t> chars = service.locateCharacteristicRange(start, end);
				for (std::size_t j = chars.first; j < chars.second; ++j)
				{
					Characteristic &c = service[j];
					
					if (c.getValueDescriptor().intersectsHandleRange(start, end))
					{
						attributes.emplace_back(c.getValueDescriptor());
						break;
					}
					else
					{
						std::pair<std::size_t, std::size_t> descs = c.locateDescriptorRange(start, end);
						attributes.reserve(attributes.size() + descs.second - descs.first);
						
						for (std::size_t k = descs.first; k < descs.second; ++k)
						{
							attributes.emplace_back(c[k]);
						} 
					}
				}
			}

			return attributes;
		}
			
		Descriptor DBusAttributeClient::getDescriptor(std::uint16_t handle)
		{
			Descriptor attr;
			
			this->synchronizeServices();
			
			std::size_t idx = this->locateServiceIndex(handle);
			
			if (idx < mServices.size())
			{
				Service &service = mServices[idx];
				std::size_t cidx = service.locateCharacteristicIndex(handle);
				
				if (cidx < service.size())
				{
					Characteristic &c = service[cidx];
					
					if (handle == c.getValueHandle())
					{
						attr = c.getValueDescriptor();
					}
					else
					{
						std::size_t didx = c.locateDescriptorIndex(handle);
						
						if (didx < c.size())
						{
							attr = c[didx];
						}
						else
						{
							throw cio::Exception(Reason::Missing, "No descriptor with specified handle in containing characteristic");
						}
					}
				}
				else
				{
					throw cio::Exception(Reason::Missing, "No characteristic exists that contains the specified descriptor handle");
				}
			}
			else
			{
				throw cio::Exception(Reason::Missing, "No service exists that contains the specified descriptor handle");
			}
			
			return attr;
		}

		Descriptor DBusAttributeClient::findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end)
		{
			Descriptor attribute;
			
			this->synchronizeServices();
	
			std::pair<std::size_t, std::size_t> idx = this->locateServiceRange(start, end);
			for (std::size_t i = idx.first; i < idx.second; ++i)
			{
				Service &service = mServices[i];
				std::pair<std::size_t, std::size_t> chars = service.locateCharacteristicRange(start, end);
				for (std::size_t j = chars.first; j < chars.second; ++j)
				{
					Characteristic &c = service[j];
					
					if (c.getValueDescriptor().intersectsHandleRange(start, end) && c.getValueType() == type)
					{
						attribute = c.getValueDescriptor();
						chars.second = j;
						idx.second = i;
						break;
					}
					else
					{
						std::pair<std::size_t, std::size_t> descs = c.locateDescriptorRange(start, end);
						for (std::size_t k = descs.first; k < descs.second; ++k)
						{
							Descriptor &desc = c[k];
							if (desc.getType() == type)
							{
								attribute = desc;
								chars.second = j;
								idx.second = i;
								break;
							}
						} 
					}
				}
			}

			return attribute;
		}
		
		Progress<std::size_t> DBusAttributeClient::readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Read);
		
			this->synchronizeServices();
		
			// TODO figure out how to handle when requested is larger than MTU or offset is nonzero
		
			// BlueZ D-Bus has different interfaces and properties depending on the exact type of handle
			// Step 1: locate service
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				Service &service = mServices[serviceIdx];
				
				// If the service handle itself is requested, the value is the UUID
				if (service.getHandle() == handle)
				{
					const UniqueId &uuid = service.getServiceType();
					TypeId shortId = uuid.getBluetoothId();
					
					// If it's a short UUID, we have 2 bytes available
					if (shortId != TypeId::None)
					{
						// BLE wire format is little endian so clients using this expect that
						TypeId little = Little::order(shortId);
						if (offset >= 2)
						{
							result.fail(Reason::Underflow);
						}
						else
						{
							std::size_t toCopy = 2 - offset;
							listener(handle, reinterpret_cast<const std::uint8_t *>(&little) + offset, toCopy);
							result.count = 2 - offset;
						}
					}
					// If it's a long UUID, we have 16 bytes available
					else
					{
						// BLE wire format is little endian so clients using this expect that
						UniqueId little =  Little::order(uuid);
						if (offset >= 16)
						{
							result.fail(Reason::Underflow);
						}
						else
						{
							std::size_t toCopy = 16 - offset;
							listener(handle, little.data() + offset, toCopy);
							result.count = 16 - offset;
						}
					}
				}
				// Otherwise find the relevant characteristic
				else
				{
					std::size_t cidx = service.locateCharacteristicIndex(handle);
					if (cidx < service.size())
					{
						Characteristic &c = service[cidx];
						if (c.getHandle() == handle)
						{
							// Characteristic value itself is special - type ID followed by properties
							std::uint16_t value[17];
							std::size_t length;
							const UniqueId &uuid = c.getValueType();
							TypeId shortId = uuid.getBluetoothId();
							if (shortId != TypeId::None)
							{
								length = 3;
								TypeId little = Little::order(shortId);
								std::memcpy(value, &little, 2);
							}
							else
							{
								length = 17;
								UniqueId little = Little::order(uuid);
								std::memcpy(value, &little, 16);
							}
							
							Properties properties = c.getProperties();
							value[length - 1] = properties.getValue();
							
							if (offset >= length)
							{
								result.fail(Reason::Underflow);
							}
							else
							{
								std::size_t toCopy = length - offset;
								listener(handle, value + offset, toCopy);
								result.count = toCopy;
							}
						}
						else if (c.getValueHandle() == handle)
						{
							// Value handle is available via D-Bus through characteristic ReadValue
											
							if (c.readable())
							{
								char cobject[128] = { };
								this->printCharacteristicPath(service.getHandle(), handle, cobject, 128);
			
								result = mSystemBus.executeGattRead(cobject, BLUEZ_CHARACTERISTIC_IFACE, handle, listener, offset);
							}
							else
							{
								result.fail(Reason::Unreadable);	
							}
						}
						else
						{
							std::size_t aidx = c.locateDescriptorIndex(handle);
							if (aidx < c.size())
							{
								const Attribute &decl = c[aidx];
								char cobject[128] = { };
								this->printDescriptorPath(service.getHandle(), c.getHandle(), handle, cobject, 128);
								result = mSystemBus.executeGattRead(cobject, BLUEZ_DESCRIPTOR_IFACE, handle, listener, offset);
							}
						}
					}
				}
			}
		
			return result;
		}
				
		Progress<std::size_t> DBusAttributeClient::readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end)
		{
			return cio::fail(Action::Read, Reason::Unsupported);
		}
				
		Progress<std::size_t> DBusAttributeClient::writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset)
		{
			Progress<std::size_t> result(Action::Write);
			
			this->synchronizeServices();
			
			// TODO figure out how to handle when requested is larger than MTU or offset is nonzero
		
			// BlueZ D-Bus has different interfaces and properties depending on the exact type of handle
			// Step 1: locate service
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				Service &service = mServices[serviceIdx];
				
				// If the service handle itself is requested, writes are not supported
				if (service.getHandle() == handle)
				{
					result.fail(Reason::Unwritable);
				}
				// Otherwise find the relevant characteristic
				else
				{
					std::size_t cidx = service.locateCharacteristicIndex(handle);
					if (cidx < service.size())
					{
						Characteristic &c = service[cidx];
						// Characteristic handle special value is not writable
						if (c.getHandle() == handle)
						{
							result.fail(Reason::Unwritable);
						}
						// Value handle might be writable through characteristic
						else if (c.getValueHandle() == handle)
						{
							if (c.writable(true, false))
							{
								// Value handle is available via D-Bus through characteristic WriteValue
								char cobject[128] = { };
								this->printCharacteristicPath(service.getHandle(), c.getHandle(), cobject, 128);
								
								result = mSystemBus.executeGattWrite(cobject, BLUEZ_CHARACTERISTIC_IFACE, buffer, requested, offset);
							}
							else
							{
								result.fail(Reason::Unwritable);
							}
						}
						// Search for declaration that might be writable
						else
						{
							std::size_t aidx = c.locateDescriptorIndex(handle);
							if (aidx < c.size())
							{
								const Attribute &decl = c[aidx];
								char cobject[128] = { };
								this->printDescriptorPath(service.getHandle(), c.getHandle(), handle, cobject, 128);
								result = mSystemBus.executeGattWrite(cobject, BLUEZ_DESCRIPTOR_IFACE, buffer, requested, offset);
							}
						}
					}
				}
			}
			else
			{
				result.fail(Reason::Missing);
			}
		
			return result;
		}
				
		Progress<std::size_t> DBusAttributeClient::sendValue(std::uint16_t handle, const void *buffer, std::size_t requested)
		{
			Progress<std::size_t> result(Action::Write);
			
			this->synchronizeServices();
		
			// BlueZ D-Bus has different interfaces and properties depending on the exact type of handle
			// Step 1: locate service
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				Service &service = mServices[serviceIdx];
				
				// If the service handle itself is requested, writes are not supported
				if (service.getHandle() == handle)
				{
					result.fail(Reason::Unwritable);
				}
				// Otherwise find the relevant characteristic
				else
				{
					std::size_t cidx = service.locateCharacteristicIndex(handle);
					if (cidx < service.size())
					{
						Characteristic &c = service[cidx];
						// Characteristic handle special value is not writable
						if (c.getHandle() == handle)
						{
							result.fail(Reason::Unwritable);
						}
						// Value handle might be writable through characteristic
						else if (c.getValueHandle() == handle)
						{
							if (c.writable(false, false))
							{
								// Value handle is available via D-Bus through characteristic WriteValue
								char cobject[128] = { };
								this->printCharacteristicPath(service.getHandle(), c.getHandle(), cobject, 128);
								
								result = mSystemBus.executeGattSend(cobject, BLUEZ_CHARACTERISTIC_IFACE, buffer, requested);
							}
							else
							{
								result.fail(Reason::Unwritable);
							}
						}
						// Search for declaration that might be writable
						else
						{
							std::size_t aidx = c.locateDescriptorIndex(handle);
							if (aidx < c.size())
							{
								const Attribute &decl = c[aidx];
								char cobject[128] = { };
								this->printDescriptorPath(service.getHandle(), c.getHandle(), handle, cobject, 128);
								result = mSystemBus.executeGattSend(cobject, BLUEZ_DESCRIPTOR_IFACE, buffer, requested);
							}
						}
					}
				}
			}
			else
			{
				result.fail(Reason::Missing);
			}
		
			return result;
		}
		
		const Subscription *DBusAttributeClient::subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval)
		{			
			Response<Subscription *> sub;
			
			this->synchronizeServices();
		
			// Determine characteristic containing subscription
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				Service &service = mServices[serviceIdx];	
				if (service.getHandle() == handle)
				{
					// okay, subscribing to a service itself is odd, but we'll honor it as a polling read
					sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
					sub.value->setModes(Property::Read);
					sub.value->setCurrentMode(Property::Read);
				}
				else
				{
					std::size_t charIdx = service.locateCharacteristicIndex(handle);
					if (charIdx < service.size())
					{
						Characteristic &ch = service[charIdx];
						
						if (ch.getHandle() == handle)
						{
							// okay, subscribing to a characteristic itself is odd, but we'll honor it as a polling read
							sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
							sub.value->setModes(Property::Read);
							sub.value->setCurrentMode(Property::Read);
						}
						else if (ch.getValueHandle() == handle)
						{
							// typical case, subscribing to a characteristic value handle
							
							Properties props = ch.getProperties();
							if (props.get(Property::Notify))
							{
								sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
								sub.value->setModes(props);
								sub.value->setCurrentMode(Property::Notify);
								
								// New subscription, we need to tell D-Bus
								if (sub.succeeded())
								{
									// Step 1 - get object path
									char objectPath[64];
									this->printCharacteristicPath(service.getHandle(), ch.getHandle(), objectPath, 64);
									
									// Step 2 - set up property listener for Value
									mSystemBus.subscribePropertyUpdates(objectPath);
									
									// Step 3 - start notification process
									mSystemBus.invokeCommand(objectPath, BLUEZ_CHARACTERISTIC_IFACE, "StartNotify");
								}
							}
							else if (props.get(Property::Indicate))
							{
								sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
								sub.value->setModes(props);
								sub.value->setCurrentMode(Property::Notify);
								
								// New subscription, we need to tell D-Bus
								if (sub.succeeded())
								{
									// Step 1 - get object path
									char objectPath[64];
									this->printCharacteristicPath(service.getHandle(), ch.getHandle(), objectPath, 64);
																		
									// Step 2 - set up property listener for Value
									mSystemBus.subscribePropertyUpdates(objectPath);
									
									// Step 3 - start notification process
									mSystemBus.invokeCommand(objectPath, BLUEZ_CHARACTERISTIC_IFACE, "StartNotify");
								}
							}
							else if (props.get(Property::Read))
							{
								sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
								sub.value->setModes(props);
								sub.value->setCurrentMode(Property::Read);
							}
							else
							{
								throw cio::Exception(Reason::Unreadable, "Could not subscribe to attribute because no receipt mode was available");
							}
						}
						else
						{
							// might be a non-value descriptor
							std::size_t descIdx = ch.locateDescriptorIndex(handle);
							if (descIdx < ch.size())
							{
								// These guys can only be polled
								sub = mSubscriptions.subscribe(handle, std::move(listener), interval);
								sub.value->setModes(Property::Read);
								sub.value->setCurrentMode(Property::Read);
							}
							else
							{
								throw cio::Exception(Reason::Missing, "Could not subscribe to invalid characteristic descriptor");
							}
						}
					}
					else
					{
						throw cio::Exception(Reason::Missing, "Could not subscribe to attribute in nonexistent characteristic");
					}
				}
			}
			else
			{
				throw cio::Exception(Reason::Missing, "Could not subscribe to attribute in nonexistent service");
			}
			
			return sub.value;
		}
				
		State DBusAttributeClient::executeSubscriptions(Task &exec) noexcept
		{
			State status = this->synchronizeServices();
			if (status.succeeded())
			{
				bool connected = true;
				
				auto capture = [&](const char *object, const char *iface, const char *property, const void *value, std::size_t length)
				{
					if (std::strcmp(iface, BLUEZ_CHARACTERISTIC_IFACE) == 0)
					{
						if (std::strcmp(property, "Value") == 0)
						{
							std::uint16_t handle = this->parseCharacteristicHandle(object);
							
							std::size_t serviceIdx = this->locateServiceIndex(handle);
							if (serviceIdx < mServices.size())
							{
								const Service &service = mServices[serviceIdx];
								
								std::size_t charIdx = service.locateCharacteristicIndex(handle);
								if (charIdx < service.size())
								{
									const Characteristic &ch = service[charIdx];
									mSubscriptions.notify(ch.getValueHandle(), value, length);
								}
							}
						}
					}
					else if (std::strcmp(iface, BLUEZ_PERIPHERAL_IFACE) == 0)
					{
						if (std::strcmp(property, "Connected") == 0)
						{
							cio::Text text(static_cast<const char *>(value), length);
							sMetaclass.info() << "Received connection update " << text;	
							connected = (text == "true");
							mConnectionState.reason = connected ? cio::Reason::Connected : cio::Reason::Disconnected;
							sMetaclass.info() << "Connection state changed: " << mConnectionState;
						}
					}
				};
				
				mSystemBus.listenForProperties(capture, exec);
				
				if (connected)
				{
					status = AttributeClient::executeSubscriptions(exec); // Progress polled attributes
				}
				else
				{
					status.fail(Reason::Disconnected);
				}
			}
			else
			{
				sMetaclass.error() << "Failed to synchronize service list" << status;
			}	

			return status;
		}
				
		State DBusAttributeClient::unsubscribe(const Subscription *subscription)
		{
			// Capture handle before we invalidate the sub
			std::uint16_t handle = subscription->getHandle();
			Property mode = subscription->getCurrentMode();
		
			// Remove in memory sub
			State status = mSubscriptions.unsubscribe(subscription);
			
			// If this was the last sub and this was an indicate/notify, tell D-Bus
			if (status.succeeded() && (mode == Property::Notify || mode == Property::Indicate))
			{
				this->stopNotify(handle);	
			}
			
			return status;
		}
				
		Progress<std::size_t> DBusAttributeClient::clearSubscriptions(std::uint16_t handle)
		{
			Progress<std::size_t> removed(Action::Unsubscribe);
			
			Subscription *subscription = mSubscriptions.top(handle);
			if (subscription)
			{
				std::uint16_t handle = subscription->getHandle();
				Property mode = subscription->getCurrentMode();
				
				// If this was an indicate/notify, tell D-Bus
				if (mode == Property::Notify || mode == Property::Indicate)
				{	
					this->stopNotify(handle);
				}
				
				// Remove any more subscriptions that may have been present
				removed += mSubscriptions.unsubscribe(handle);
				removed.succeed();
			}
			else
			{
				removed.fail(Reason::Missing);
			}
			
			return removed;
		}
				
		Progress<std::size_t> DBusAttributeClient::clearAllSubscriptions()
		{
			Progress<std::size_t> removed(Action::Unsubscribe);
			
			Subscription *next = mSubscriptions.top();
			
			while (next)
			{
				Property mode = next->getCurrentMode();

				// If this was an indicate/notify, tell D-Bus
				if (mode == Property::Notify || mode == Property::Indicate)
				{	
					this->stopNotify(next->getHandle());
				}
				
				// Clear all subscriptions on the same handle
				removed += mSubscriptions.unsubscribe(next->getHandle());
				next = mSubscriptions.top();
			}
			
			removed.succeed();
			
			return removed;
		}
	
		void DBusAttributeClient::stopNotify(std::uint16_t handle)
		{
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				Service &service = mServices[serviceIdx];
				
				std::size_t charIdx = service.locateCharacteristicIndex(handle);
				if (charIdx < service.size())
				{
					Characteristic &ch = service[charIdx];
					if (ch.getValueHandle() == handle)
					{
						// Step 1 - get object path
						char objectPath[64];
						this->printCharacteristicPath(service.getHandle(), ch.getHandle(), objectPath, 64);
						
						// Stop 2 - unsubscribe property listener for Value
						mSystemBus.unsubscribePropertyUpdates(objectPath);
						
						// Step 3 - stop notification				
						mSystemBus.invokeCommand(objectPath, BLUEZ_CHARACTERISTIC_IFACE, "StopNotify");
					}
				}
			}
		}
				
		std::vector<Service> &DBusAttributeClient::getServices()
		{
			this->synchronizeServices();
			return mServices;
		}
		
		const std::vector<Service> &DBusAttributeClient::getServices() const
		{
			this->synchronizeServices();
			return mServices;
		}
	
		std::vector<Service> &DBusAttributeClient::getCachedServices() noexcept
		{
			return mServices;
		}
		
		const std::vector<Service> &DBusAttributeClient::getCachedServices() const noexcept
		{
			return mServices;
		}
	
		void DBusAttributeClient::clearCachedServices() noexcept
		{
			mServices.clear();
		}
		
		State DBusAttributeClient::synchronizeServices() const noexcept
		{
			State state;

			if (mServices.empty())
			{
				try
				{
					mServices = this->discoverRemoteServices();
					state.succeed();
				}
				catch (const cio::Exception &e)
				{
					sMetaclass.error() << "Service synchronization unexpectedly failed: " << e;
					state.fail(Action::Discover, e.reason());
				}
				catch (...)
				{
					state.fail(Action::Discover, Reason::Unknown);
				}
			}
			else
			{
				state.succeed(Reason::Exists);
			}

			return state;
		}
		
		std::vector<Service> DBusAttributeClient::discoverRemoteServices() const
		{
			std::vector<Service> services;
			
			if (mSystemBus && mPeripheralPath)
			{
				// First make sure the services are resolved
				{					
					// This might take a while...
					std::chrono::milliseconds timeout(200);
					std::size_t attempts = 25;
					bool connected = false;
					do
					{
						std::this_thread::sleep_for(timeout);
						connected = mSystemBus.getProperty(mPeripheralPath.c_str(), BLUEZ_PERIPHERAL_IFACE, "ServicesResolved").getBoolean();
					}
					while (!connected && --attempts > 0);
				}
				
			
				std::vector<Characteristic> chars;
				std::vector<Descriptor> descriptors;
			
				const char *ifaces[3] = { BLUEZ_SERVICE_IFACE, BLUEZ_CHARACTERISTIC_IFACE, BLUEZ_DESCRIPTOR_IFACE };
				auto capture = [&](const char *object, const char *iface)
				{
					// Verify this is a service associated with current peripheral
					if (std::strncmp(mPeripheralPath.c_str(), object, mPeripheralPath.size()) == 0)
					{
						if (std::strcmp(iface, BLUEZ_SERVICE_IFACE) == 0)
						{
							services.emplace_back(this->readServiceInformation(object));
						}
						else if (std::strcmp(iface, BLUEZ_CHARACTERISTIC_IFACE) == 0)
						{
							chars.emplace_back(this->readCharacteristicInformation(object));
						}
						else if (std::strcmp(iface, BLUEZ_DESCRIPTOR_IFACE) == 0)
						{
							descriptors.emplace_back(this->readDescriptorInformation(object));
						}
					}
				};
					
				mSystemBus.findMatchingObjects(ifaces, 3, capture);
				
				AttributeClient::organizeServices(services, chars, descriptors);
				
				for (Service &s : services)
				{				
					s.setClient(const_cast<DBusAttributeClient *>(this));
				}
			}
			
			return services;
		}

		
		std::size_t DBusAttributeClient::printAdapterPath(const Device &adapter, char *text, std::size_t length) const noexcept
		{
			char printed[64] = "/org/bluez/hci";
			std::size_t actual = 14 + cio::print(adapter.getIndex(), printed + 14, 40);
			std::size_t toCopy = std::min(actual, length);
			
			if (toCopy > 0)
			{
				std::memcpy(text, printed, toCopy);
			}
			
			if (toCopy < length)
			{
				std::memset(text + toCopy, 0, length - toCopy);
			}
			
			return actual;
		}
		
		std::size_t DBusAttributeClient::printPeripheralPathSuffix(const DeviceAddress &peripheral, char *text, std::size_t length) const noexcept
		{
			std::size_t actual = 0;
			std::size_t printed = 0;

			char field[22] = "dev_";
			peripheral.printMac(field + 4, 18);
			
			field[6] = '_'; // replace ':' with '_"
			field[9] = '_';
			field[12] = '_';
			field[15] = '_';
			field[18] = '_';
		
			
			std::size_t toCopy = std::min<std::size_t>(21u, length - printed);
			if (toCopy > 0)
			{
				std::memcpy(text + printed, field, toCopy);
			}
			printed += toCopy;
			
			actual += 22; // "/dev_" plus MAC (12 chars, 5 separators)	
			
			if (printed < length)
			{
				std::memset(text + printed, 0, length - printed);
			}
			
			return actual;
		}
		
		std::size_t DBusAttributeClient::printPeripheralPath(const Device &adapter, const DeviceAddress &peripheral, char *text, std::size_t length) const noexcept
		{
			std::size_t actual = this->printAdapterPath(adapter, text, length);
			std::size_t printed = std::min(actual, length);
			
			if (printed < length)
			{
				text[printed++] = '/';
			}
			++actual;
			
			actual += this->printPeripheralPathSuffix(peripheral, text + printed, length - printed);
			
			return actual;
		}
		
		std::uint16_t DBusAttributeClient::parseServiceHandle(const char *object) const
		{
			std::uint16_t handle = 0;
			
			// Matches the prefix
			if (std::strncmp(mPeripheralPath.c_str(), object, mPeripheralPath.size()) == 0)
			{
				const char *peripheralSuffix = object + mPeripheralPath.size();
				if (std::strncmp(peripheralSuffix, "/service", 8) == 0)				
				{
					const char *handleText = peripheralSuffix + 8;
					handle = this->parseHandleValue(handleText);
				}
			}
			
			return handle;
		}

		std::uint16_t DBusAttributeClient::parseCharacteristicHandle(const char *object) const
		{
			std::uint16_t handle = 0;
			
			// Matches the prefix
			if (std::strncmp(mPeripheralPath.c_str(), object, mPeripheralPath.size()) == 0)
			{
				const char *peripheralSuffix = object + mPeripheralPath.size();
				if (std::strncmp(peripheralSuffix, "/service", 8) == 0)				
				{
					const char *serviceText = peripheralSuffix + 8;
					
					// Skip to end of service parent object path
					while (*serviceText != 0 && *serviceText != '/')
					{
						++serviceText;
					}
					
					// Make sure we have the /char path entry
					if (std::strncmp(serviceText, "/char", 5) == 0)
					{
						const char *handleText = serviceText + 5;
						handle = this->parseHandleValue(handleText);
					}
				}
			}
			
			return handle;
		}
	
		std::uint16_t DBusAttributeClient::parseDescriptorHandle(const char *object) const
		{
			std::uint16_t handle = 0;
			
			// Matches the prefix
			if (std::strncmp(mPeripheralPath.c_str(), object, mPeripheralPath.size()) == 0)
			{
				const char *peripheralSuffix = object + mPeripheralPath.size();
				
				if (std::strncmp(peripheralSuffix, "/service", 8) == 0)				
				{
					const char *serviceText = peripheralSuffix + 8;
					
					// Skip to end of service parent object path
					while (*serviceText != 0 && *serviceText != '/')
					{
						++serviceText;
					}
	
					// Make sure we have the /char path entry
					if (std::strncmp(serviceText, "/char", 5) == 0)
					{
						serviceText += 5;
					
						// Skip to end of char parent object path
						while (*serviceText != 0 && *serviceText != '/')
						{
							++serviceText;
						}

						if (std::strncmp(serviceText, "/desc", 5) == 0)
						{
							const char *handleText = serviceText + 5;
							handle = this->parseHandleValue(handleText);
						}
					}
				}
			}
			
			return handle;
		}

		std::uint16_t DBusAttributeClient::parseHandleValue(const char *object) const
		{
			std::uint16_t handle = 0;
			
			// Make sure we have four chars availalbe
			const char *handleEnd = object;
			while (*handleEnd != 0 && *handleEnd != '/')
			{
				++handleEnd;
			}
	
			if (handleEnd - object == 4)
			{
				handle = (cio::parseByte(object[0], object[1]) << 8) | cio::parseByte(object[2], object[3]);
			}
			
			return handle;		
		}
	
		std::size_t DBusAttributeClient::printAttributePath(std::uint16_t handle, char *text, std::size_t length) const noexcept
		{
			std::size_t actual = 0;
			
			std::size_t serviceIdx = this->locateServiceIndex(handle);
			if (serviceIdx < mServices.size())
			{
				const Service &service = mServices[serviceIdx];
				std::uint16_t serviceHandle = service.getHandle();
				if (serviceHandle == handle)
				{
					actual = this->printServicePath(serviceHandle, text, length);
				}
				else
				{
					std::size_t cidx = service.locateCharacteristicIndex(handle);
					if (cidx < service.size())
					{
						const Characteristic &c = service[cidx];
						std::uint16_t chandle = c.getHandle();
						
						if (chandle == handle || c.getValueHandle() == handle)
						{
							actual = this->printCharacteristicPath(serviceHandle, chandle, text, length);
						}
						else
						{
							std::size_t aidx = c.locateDescriptorIndex(handle);
							if (aidx < c.size())
							{
								const Attribute &decl = c[aidx];
								this->printDescriptorPath(serviceHandle, chandle, handle, text, length);
							}
						}
					}
				}
			}
			
			return actual;
		}
				
		std::size_t DBusAttributeClient::printServicePath(std::uint16_t service, char *text, std::size_t length) const noexcept
		{
			char tmp[13] = "/serviceXXXX"; 
			
			std::size_t actual = mPeripheralPath.size() + 12; // omit trailing null
			std::size_t printed = std::min(length, mPeripheralPath.size());
			if (printed > 0)
			{
				std::memcpy(text, mPeripheralPath.c_str(), mPeripheralPath.size());
			}
			
			if (printed < length)
			{
				// go ahead and fill in the handle
				Lowercase c;
				cio::printByte(service >> 8, tmp + 8, c);
				cio::printByte(service, tmp + 10, c);

				std::size_t toCopy = std::min<std::size_t>(12u, length - printed);
				std::memcpy(text + printed, tmp, toCopy);
				printed += toCopy;
			}
			
			if (printed < length)
			{
				std::memset(text + printed, 0, length - printed);
			}
			
			return actual;	
		}
				
		std::size_t DBusAttributeClient::printCharacteristicPath(std::uint16_t service, std::uint16_t ch, char *text, std::size_t length) const noexcept
		{
			char tmp[22] = "/serviceXXXX/charXXXX"; 
			
			std::size_t actual = mPeripheralPath.size() + 21; // omit trailing null
			std::size_t printed = std::min(length, mPeripheralPath.size());
			if (printed > 0)
			{
				std::memcpy(text, mPeripheralPath.c_str(), mPeripheralPath.size());
			}
			
			if (printed < length)
			{
				// go ahead and fill in both handles
				Lowercase c;
				cio::printByte(service >> 8, tmp + 8, c);
				cio::printByte(service, tmp + 10, c);
				
				cio::printByte(ch >> 8, tmp + 17, c);
				cio::printByte(ch, tmp + 19, c);
				
				std::size_t toCopy = std::min<std::size_t>(21u, length - printed);
				std::memcpy(text + printed, tmp, toCopy);
				printed += toCopy;
			}
			
			if (printed < length)
			{
				std::memset(text + printed, 0, length - printed);
			}
			
			return actual;	
		}
				
		std::size_t DBusAttributeClient::printDescriptorPath(std::uint16_t service, std::uint16_t ch, std::uint16_t desc, char *text, std::size_t length) const noexcept
		{
			char tmp[31] = "/serviceXXXX/charXXXX/descXXXX"; 
			
			std::size_t actual = mPeripheralPath.size() + 30; // omit trailing null
			std::size_t printed = std::min(length, mPeripheralPath.size());
			if (printed > 0)
			{
				std::memcpy(text, mPeripheralPath.c_str(), mPeripheralPath.size());
			}
			
			if (printed < length)
			{
				// go ahead and fill in all three handles
				Lowercase c;
				cio::printByte(service >> 8, tmp + 8, c);
				cio::printByte(service, tmp + 10, c);
				
				cio::printByte(ch >> 8, tmp + 17, c);
				cio::printByte(ch, tmp + 19, c);
				
				cio::printByte(desc >> 8, tmp + 26, c);
				cio::printByte(desc, tmp + 28, c);
				
				std::size_t toCopy = std::min<std::size_t>(30u, length - printed);
				std::memcpy(text + printed, tmp, toCopy);
				printed += toCopy;
			}
			
			if (printed < length)
			{
				std::memset(text + printed, 0, length - printed);
			}
			
			return actual;
		}
		
		Service DBusAttributeClient::readServiceInformation(const char *object) const
		{
			Service service(this->parseServiceHandle(object), 0xFFFFu);
			service.setServiceType(mSystemBus.getProperty(object, BLUEZ_SERVICE_IFACE, "UUID").getString());
			return service;
		}
				
		Characteristic DBusAttributeClient::readCharacteristicInformation(const char *object) const
		{
			Characteristic c(this->parseCharacteristicHandle(object));
			
			c.setValueType(mSystemBus.getProperty(object, BLUEZ_CHARACTERISTIC_IFACE, "UUID").getString());
			c.setValueHandle(c.getHandle() + 1);

			Properties properties;

			auto capture = [&](const char *property, const Variant &value)
			{
				if (value.matches("read"))
				{
					properties.set(Property::Read);
				}
				else if (value.matches("write"))
				{
					properties.set(Property::Write);
				}
				else if(value.matches("notify"))
				{
					properties.set(Property::Notify);
				}
				else if (value.matches("indicate"))
				{
					properties.set(Property::Indicate);
				}
			};

			mSystemBus.getProperty(object, BLUEZ_CHARACTERISTIC_IFACE, "Flags", capture);
			c.setProperties(properties);
			
			return c;
		}
				
		Descriptor DBusAttributeClient::readDescriptorInformation(const char *object) const
		{
			std::uint16_t handle = this->parseDescriptorHandle(object);
			UniqueId type = mSystemBus.getProperty(object, BLUEZ_DESCRIPTOR_IFACE, "UUID").getString();
			return Descriptor(type, handle);
		}
				
		std::size_t DBusAttributeClient::locateServiceIndex(std::uint16_t handle) const noexcept
		{
			std::size_t low = 0;
			std::size_t high = mServices.size();
			std::size_t current = (low + high) / 2;
			
			while (current < high)
			{
				const Service &service = mServices[current];
				std::uint16_t start = service.getHandle();
				std::uint16_t end = service.getEndHandle();
				
				if (start <= handle)
				{
					// Service contains handle, end search here
					if (handle <= end)
					{
						break;
					}
					
					low = current + 1;
				}
				else
				{
					high = current;
				}
				
				current = (low + high) / 2;
			}
			
			return current;
		}
		
		std::pair<std::size_t, std::size_t> DBusAttributeClient::locateServiceRange(std::uint16_t start, std::uint16_t end) const noexcept
		{
			std::pair<std::size_t, std::size_t> range(0, 0);
		
			std::size_t low = 0;
			std::size_t high = mServices.size();
			std::size_t current = (low + high) / 2;
			
			while (current < high)
			{
				const Service &service = mServices[current];
				std::uint16_t sstart = service.getHandle();
				std::uint16_t send = service.getEndHandle();
				
				if (sstart <= start)
				{
					// Service contains start handle, end lower bound search here
					if (start <= send)
					{
						break;
					}
					
					low = current + 1;
				}
				else
				{
					high = current;
				}
				
				current = (low + high) / 2;
			}
			
			range.first = current;
			
			low = high;
			high = mServices.size();
			current = (low + high) / 2;
			
			while (current < high)
			{
				const Service &service = mServices[current];
				std::uint16_t sstart = service.getHandle();
				std::uint16_t send = service.getEndHandle();
				
				if (sstart <= end)
				{
					// Service contains end handle, end upper bound search here and adjust current up 1
					if (end <= send)
					{
						++current;
						break;
					}
					
					low = current + 1;
				}
				else
				{
					high = current;
				}
				
				current = (low + high) / 2;
			}
			
			range.second = current;
					
			return range;
		}
		
		void DBusAttributeClient::setMonitorConnectionState(bool value)
		{
			if (mMonitorConnectionState != value)
			{
				mMonitorConnectionState = value;
				if (value)
				{
					this->applyConnectionMonitor();
				}
				else
				{
					this->clearConnectionMonitor();
				}
			}
		}
		
		void DBusAttributeClient::applyConnectionMonitor()
		{
			if (mMonitorConnectionState && mSystemBus && !mPeripheralPath.empty())
			{
				mSystemBus.subscribePropertyUpdates(mPeripheralPath.c_str());
			}
		}
		
		void DBusAttributeClient::clearConnectionMonitor()
		{
			if (!mMonitorConnectionState && mSystemBus && !mPeripheralPath.empty())
			{
				mSystemBus.unsubscribePropertyUpdates(mPeripheralPath.c_str());
			}
		}
	}
}
