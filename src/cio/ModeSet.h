/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MODESET_H
#define CIO_MODESET_H

#include "Types.h"

#include "Mode.h"

#include <iosfwd>
#include <string>

namespace cio
{
	/**
	 * The CIO ModeSet describes the possible modes and permissions for a resource.
	 * This is essentially a bitset of the Mode enumeration.
	 */
	class CIO_API ModeSet
	{
		public:
			/** The mask of valid bits in the underlying integer representing this mode set */
			static const std::uint32_t MASK = (1u << (static_cast<unsigned>(Mode::Invalid) + 1)) - 1u;

			/**
			 * Gets the ModeSet with all modes enabled.
			 *
			 * @return the mode set with all modes enabled
			 */
			inline static ModeSet all();

			/**
			 * Gets the ModeSet with only read modes enabled.
			 * This includes the following modes: Load and Read
			 *
			 * @return a mode set representing read-only access
			 */
			inline static ModeSet readonly();

			/**
			 * Gets the ModeSet with only write modes enabled.
			 * This includes the following modes: Load, Create, Replace, and Write
			 *
			 * @return a mode set representing read-only access
			 */
			inline static ModeSet writeonly();

			/**
			 * Gets the ModeSet representing the conventional general access for a read/write channel, without specialty options.
			 * This includes the following modes: Load, Create, Replace, Read, and Write
			 */
			inline static ModeSet general();

			/**
			 * Creates an empty mode set
			 */
			inline ModeSet();

			/**
			 * Creates a ModeSet with the given mode initially enabled.
			 *
			 * @param mode The mode to enable
			 */
			inline ModeSet(Mode mode);

			/**
			 * Creates a ModeSet from the underlying integer representation used to represent it.
			 *
			 * @param bits The mode set bits
			 */
			inline explicit ModeSet(std::uint32_t bits);

			/**
			 * Interprets the ModeSet in a Boolean context.
			 * It is considered true if any modes are set.
			 *
			 * @return whether any modes are set
			 */
			inline explicit operator bool() const;

			/**
			 * Clears the mode set, turning all modes off.
			 */
			inline void clear();

			/**
			 * Checks whether the mode set is empty.
			 *
			 * @return whether the mode set is empty
			 */
			inline bool empty() const;

			/**
			 * Sets whether the given mode(s) are enabled or disabled.
			 *
			 * @param modes The mode(s) to change
			 * @param value Whether to enable or disable the given modes
			 */
			inline void set(ModeSet modes, bool value);

			/**
			 * Enables the given mode.
			 *
			 * @param mode The mode to enable
			 */
			inline void insert(Mode mode);

			/**
			 * Enables the given modes.
			 *
			 * @param modes The modes to enable
			 */
			inline void insert(ModeSet modes);

			/**
			 * Disables the given mode.
			 *
			 * @param mode The mode to disable
			 */
			inline void erase(Mode mode);

			/**
			 * Disables the given modes.
			 *
			 * @param modes The modes to disable
			 */
			inline void erase(ModeSet modes);

			/**
			 * Gets whether the given mode is enabled.
			 *
			 * @param mode The mode of interest
			 * @return whether the mode is enabled
			 */
			inline bool count(Mode mode) const;

			/**
			 * Gets whether the given mode is enabled.
			 *
			 * @param mode The mode of interest
			 * @return whether the mode is enabled
			 */
			inline bool contains(Mode mode) const;

			/**
			 * Gets the underlying bits used to represent the mode set.
			 *
			 * @return the underlying bits
			 */
			inline std::uint32_t getBits() const;

			/**
			 * Sets the underlying bits used to represent the mode set.
			 *
			 * @param bits the underlying bits
			 */
			inline void setBits(std::uint32_t bits);

			/**
			 * Prints the mode set into a human-readable string.
			 * This puts a comma ',' between each mode and uses the text form of each mode.
			 *
			 * @return the text string of the mode set
			 */
			std::string print() const;

		private:
			/** The underlying bits representing the mode set */
			std::uint32_t mModes;
	};

	/**
	 * Inverts a mode to get the mode set representing all modes except that one being enabled.
	 *
	 * @param mode The mode
	 * @return the mode set representing the inverse of that mode
	 */
	inline ModeSet operator~(Mode mode);

	/**
	 * Inverts a mode set to get the mode set representing all modes not enabled in it.
	 *
	 * @param modes The mode set
	 * @return the mode set representing the inverse of that mode
	 */
	inline ModeSet operator~(ModeSet modes);

	/**
	 * Creates the intersection of two mode sets to get a mode set of the modes that are enabled in both of them.
	 *
	 * @param left the first mode set
	 * @param right The second mode set
	 * @return the mode set representing the modes enabled in both inputs
	 */
	inline ModeSet operator&(ModeSet left, ModeSet right);

	/**
	 * Intersects a mode set with a given mode set to only enable the modes that are enabled in both.
	 *
	 * @param left the first mode set
	 * @param right The second mode set
	 * @return the first mode set after being updated to be the intersection
	 */
	inline ModeSet &operator&=(ModeSet &left, ModeSet right);

	/**
	 * Creates the union of two mode sets to get a mode set of the modes that are enabled in either one of them.
	 *
	 * @param left the first mode set
	 * @param right The second mode set
	 * @return the mode set representing the union of modes enabled in at least one input
	 */
	inline ModeSet operator|(ModeSet left, ModeSet right);

	/**
	 * Creates a mode set representing both given modes being enabled.
	 *
	 * @param left The first mode
	 * @param right The second mode
	 * @return a mode set representing both input modes being enabled
	 */
	inline ModeSet operator|(Mode left, Mode right);

	/**
	 * Adds all enabled modes in a new mode set into a given mode set to enable all modes present in both of them
	 *
	 * @param left the first mode set
	 * @param right The second mode set
	 * @return the first mode set after being updated
	 */
	inline ModeSet &operator|=(ModeSet &left, ModeSet right);

	/**
	 * Check to see if two mode sets are equal.
	 *
	 * @param left The first mode set
	 * @param right The second mode set
	 * @return whether the mode sets are equal
	 */
	inline bool operator==(ModeSet left, ModeSet right);

	/**
	 * Check to see if two mode sets are not equal.
	 *
	 * @param left The first mode set
	 * @param right The second mode set
	 * @return whether the mode sets are not equal
	 */
	inline bool operator!=(ModeSet left, ModeSet right);

	/**
	 * Prints the mode set to a C++ stream.
	 * This puts a comma ',' between each mode and uses the text form of each mode.
	 *
	 * @param stream The stream to print to
	 * @param modes The mode set to print
	 * @return the stream
	 */
	CIO_API std::ostream &operator<<(std::ostream &stream, ModeSet modes);
}

/* Inline implementation */

namespace cio
{
	inline ModeSet ModeSet::all()
	{
		return ModeSet(ModeSet::MASK);
	}

	inline ModeSet ModeSet::general()
	{
		return ModeSet(Mode::Load | Mode::Create | Mode::Replace | Mode::Read | Mode::Write);
	}

	inline ModeSet ModeSet::readonly()
	{
		return ModeSet(Mode::Load | Mode::Read);
	}

	inline ModeSet ModeSet::writeonly()
	{
		return ModeSet(Mode::Load | Mode::Create | Mode::Replace | Mode::Write);
	}

	inline ModeSet::ModeSet() :
		mModes(0)
	{
		// nothing more to do
	}

	inline ModeSet::ModeSet(Mode mode) :
		mModes(1u << static_cast<unsigned>(mode))
	{
		// nothing more to do
	}

	inline ModeSet::ModeSet(std::uint32_t bits) :
		mModes(bits & ModeSet::MASK)
	{
		// nothing more to do
	}

	inline ModeSet::operator bool() const
	{
		return mModes != 0;
	}

	inline void ModeSet::clear()
	{
		mModes = 0;
	}

	inline bool ModeSet::empty() const
	{
		return mModes == 0;
	}

	inline void ModeSet::set(ModeSet modes, bool value)
	{
		if (value)
		{
			mModes |= modes.mModes;
		}
		else
		{
			mModes &= (~modes.mModes & ModeSet::MASK);
		}
	}

	inline void ModeSet::insert(Mode mode)
	{
		mModes |= (1u << static_cast<unsigned>(mode));
	}

	inline void ModeSet::insert(ModeSet modes)
	{
		mModes |= modes.mModes;
	}

	inline void ModeSet::erase(Mode mode)
	{
		mModes &= ~(1u << static_cast<unsigned>(mode));
	}

	inline void ModeSet::erase(ModeSet modes)
	{
		mModes &= (~modes.mModes & ModeSet::MASK);
	}

	inline bool ModeSet::count(Mode mode) const
	{
		return (mModes & (1u << static_cast<unsigned>(mode))) != 0;
	}
	
	inline bool ModeSet::contains(Mode mode) const
	{
		return (mModes & (1u << static_cast<unsigned>(mode))) != 0;
	}
	
	inline std::uint32_t ModeSet::getBits() const
	{
		return mModes;
	}

	inline void ModeSet::setBits(std::uint32_t bits)
	{
		mModes = bits & ModeSet::MASK;
	}

	inline ModeSet operator~(Mode mode)
	{
		return ModeSet(~(1u << static_cast<unsigned>(mode)));
	}

	inline ModeSet operator~(ModeSet modes)
	{
		return ModeSet(~modes.getBits());
	}

	inline ModeSet operator&(ModeSet left, ModeSet right)
	{
		return ModeSet(left.getBits() & right.getBits());
	}

	inline ModeSet operator|(ModeSet left, ModeSet right)
	{
		return ModeSet(left.getBits() | right.getBits());
	}

	inline ModeSet operator|(Mode left, Mode right)
	{
		return ModeSet((1u << static_cast<unsigned>(left)) | (1u << static_cast<unsigned>(right)));
	}

	inline ModeSet &operator&=(ModeSet &left, ModeSet right)
	{
		left.setBits(left.getBits() & right.getBits());
		return left;
	}

	inline ModeSet &operator|=(ModeSet &left, ModeSet right)
	{
		left.setBits(left.getBits() | right.getBits());
		return left;
	}

	inline bool operator==(ModeSet left, ModeSet right)
	{
		return left.getBits() == right.getBits();
	}

	inline bool operator!=(ModeSet left, ModeSet right)
	{
		return left.getBits() != right.getBits();
	}
}

#endif
