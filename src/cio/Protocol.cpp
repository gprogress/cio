/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Protocol.h"

#include "Class.h"
#include "Channel.h"
#include "Directory.h"
#include "Exception.h"
#include "Metadata.h"
#include "Path.h"
#include "ProtocolFactory.h"
#include "ProtocolFilter.h"

namespace cio
{
	Class<Protocol> Protocol::sMetaclass("cio::Protocol");

	/* In Static.cpp
	const Text Protocol::sWildcard("*");

	const ProtocolFilter Protocol::sEmptyFilter = { nullptr, Protocol::sWildcard, Protocol::sWildcard, Resource::None };
	*/

	const Metaclass &Protocol::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &Protocol::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Resource Protocol::checkForSpecialDirectories(const Text &filename) noexcept
	{
		std::size_t length = filename.strlen();
		bool special = (length == 1 && filename[0] == '.') || (length == 2 && filename[0] == '.' && filename[1] == '.');
		return special ? Resource::Directory: Resource::None;
	}

	Protocol::Protocol() noexcept
	{
		// nothing more to do
	}

	Protocol::Protocol(Text prefix) noexcept :
		mDefaultPrefix(std::move(prefix))
	{
		// nothing more to do
	}

	Protocol::Protocol(Text prefix, Chunk<ProtocolFilter> filters) noexcept :
		mDefaultPrefix(std::move(prefix)),
		mFilters(std::move(filters))
	{
		// nothing more to do
	}

	Protocol::Protocol(const Protocol &in) = default;

	Protocol::Protocol(Protocol &&in) noexcept = default;

	Protocol &Protocol::operator=(const Protocol &in) = default;

	Protocol &Protocol::operator=(Protocol&&in) noexcept = default;

	Protocol::~Protocol() noexcept = default;

	void Protocol::clear() noexcept
	{
		this->clearFilterList();
		mDefaultPrefix.clear();
	}

	ModeSet Protocol::open(const Path &resource)
	{
		return this->openWithFactory(resource, ModeSet::general(), cio::ProtocolFactory::getDefault());
	}

	ModeSet Protocol::openWithFactory(const Path &resource, ModeSet modes, ProtocolFactory *factory)
	{
		return ModeSet();
	}

	const Chunk<ProtocolFilter> &Protocol::getFilterList() const noexcept
	{
		return mFilters;
	}

	void Protocol::addFilterList(Chunk<ProtocolFilter> filters)
	{
		if (mFilters.empty())
		{
			mFilters = std::move(filters);
		}
		else if (!filters.empty())
		{
			std::size_t oldSize = mFilters.size();
			mFilters.resize(oldSize + filters.size()); // allow reallocation

			auto ii = mFilters.begin() + oldSize;
			for (ProtocolFilter &filter : filters)
			{
				*ii++ = std::move(filter);
			}
		}
	}

	void Protocol::clearFilterList() noexcept
	{
		mFilters.clear();
	}

	const ProtocolFilter &Protocol::findBestFilter(const Path &path, Resource type) const noexcept
	{
		const ProtocolFilter *result = &sEmptyFilter;

		int score = -1;

		Text prefix = path.getProtocol();
		if (prefix.empty())
		{
			prefix = mDefaultPrefix.view();
		}

		Text ext = path.getExtension();

		for (const ProtocolFilter &candidate : mFilters)
		{
			int candidateScore = candidate.matches(type, prefix, ext);

			// If we have a better score than before, run with it
			if (candidateScore > score)
			{
				result = &candidate;
				score = candidateScore;

				// highest possible score, we're done
				if (candidateScore >= 7)
				{
					break;
				}
			}
		}

		return *result;
	}

	Text Protocol::getDefaultPrefix() const noexcept
	{
		return mDefaultPrefix.view();
	}

	void Protocol::setDefaultPrefix(Text text)
	{
		mDefaultPrefix = std::move(text);
	}

	Text Protocol::getPrimaryExtension() const noexcept
	{
		return mFilters.empty() ? sWildcard : mFilters[0].extension.splitBefore(';');
	}

	bool Protocol::compatible(const Path &path, Resource type) const noexcept
	{
		const ProtocolFilter &filter = this->findBestFilter(path, type);
		return filter.metaclass != nullptr;
	}

	std::unique_ptr<Protocol> Protocol::openProtocol(const Path &path, Resource type)
	{
		return this->readProtocol(path, type);
	}

	std::unique_ptr<Protocol> Protocol::readProtocol(const Path &path, Resource type) const
	{
		return nullptr;
	}

	Metadata Protocol::readMetadata(const Path &input) const
	{
		return Metadata();
	}

	Metadata Protocol::create(const Metadata &input, Traversal createParentResource) 
	{
		return Metadata();
	}

	Resource Protocol::readType(const Path &path) const
	{
		Metadata md = this->readMetadata(path);
		return md.getType();
	}

	ModeSet Protocol::readModes(const Path &path, ModeSet desired) const
	{
		Metadata md = this->readMetadata(path);
		return md.getMode();
	}

	Length Protocol::readSize(const Path &path) const
	{
		Metadata md = this->readMetadata(path);
		return md.getLength();
	}

	Resource Protocol::checkForSpecialFilename(const Text &filename) const noexcept
	{
		return Protocol::checkForSpecialDirectories(filename);
	}

	Path Protocol::getCurrentDirectory() const
	{
		return Path();
	}

	void Protocol::setCurrentDirectory(const Path &path, Traversal createMissing)
	{
		throw Exception(Action::Metadata, Reason::Unsupported);
	}

	Path Protocol::getStandardPath(StandardPath path, Resource type) const
	{
		throw Exception(Action::Inquire, Reason::Missing);
	}

	Traversal Protocol::depth() const noexcept
	{
		return Traversal::None;
	}

	bool Protocol::traversable() const noexcept
	{
		return false;
	}

	Length Protocol::size() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	Length Protocol::capacity() const noexcept
	{
		return CIO_UNKNOWN_LENGTH;
	}

	Metadata Protocol::current() const
	{
		return Metadata();
	}

	Text Protocol::filename() const
	{
		return Text();
	}

	State Protocol::next()
	{
		return cio::fail(Action::Discover, Reason::Underflow);
	}

	State Protocol::rewind()
	{
		return cio::fail(Action::Discover, Reason::Underflow);
	}

	State Protocol::seekToEntry(const Path &relative)
	{
		return cio::fail(Action::Seek, Reason::Missing);
	}

	std::unique_ptr<Input> Protocol::openCurrentInput(ModeSet modes) const
	{
		std::unique_ptr<Input> input;
		Text filename = this->filename();
		if (!filename.empty())
		{
			input = this->openInput(filename, modes);
		}
		return input;
	}

	std::unique_ptr<Output> Protocol::openCurrentOutput(ModeSet modes)
	{
		std::unique_ptr<Output> output;
		Text filename = this->filename();
		if (!filename.empty())
		{
			output = this->openOutput(filename, modes, Traversal::None);
		}
		return output;
	}

	std::unique_ptr<Channel> Protocol::openCurrentChannel(ModeSet modes)
	{
		std::unique_ptr<Channel> output;
		Text filename = this->filename();
		if (!filename.empty())
		{
			output = this->openChannel(filename, modes, Traversal::None);
		}
		return output;
	}

	std::unique_ptr<Protocol> Protocol::readCurrentSubdirectory(ModeSet modes) const
	{
		std::unique_ptr<Protocol> output;
		Text filename = this->filename();
		if (!filename.empty())
		{
			output = this->readDirectory(filename, modes);
		}
		return output;
	}

	std::unique_ptr<Protocol> Protocol::openCurrentSubdirectory(ModeSet modes)
	{
		std::unique_ptr<Protocol> output;
		Text filename = this->filename();
		if (!filename.empty())
		{
			output = this->openDirectory(filename, modes, Traversal::None);
		}
		return output;
	}

	std::unique_ptr<Protocol> Protocol::openDirectory(const Path &path, ModeSet modes, Traversal createParents)
	{
		return this->readDirectory(path, modes);
	}

	std::unique_ptr<Protocol> Protocol::readDirectory(const Path &path, ModeSet modes) const
	{
		return nullptr;
	}

	std::vector<Path> Protocol::listDirectory(const Path &path, Traversal depth) const
	{
		std::vector<Path> result;
		auto receiver = [&](const Path & path)
		{
			result.emplace_back(path);
			return true;
		};
		this->enumerateDirectory(path, depth, receiver);
		return result;
	}

	std::size_t Protocol::enumerateDirectory(const Path &path, Traversal depth, const std::function<bool(const Path &)> &receiver) const
	{
		std::size_t count = 0;
		bool proceed = true;
		if (depth)
		{
			std::unique_ptr<Protocol> dir = this->readDirectory(path, ModeSet::readonly());
			if (dir)
			{
				// Different implementations for Immediate vs. Recursive
				// immediate just feeds paths straight to the receiver
				if (depth.value == 1)
				{
					while (proceed && dir->traversable())
					{
						Text filename = dir->filename();
						if (dir->special(filename) == cio::Resource::None)
						{
							Path child(path, filename);
							proceed = receiver(child);
							++count;
						}

						dir->next();
					}
				}
				// recursive captures all child paths in a temporary list then closes the directory
				// this is necessary to avoid exhausting the system with too many directory handles for large trees
				else
				{
					std::vector<Path> children;
					while (proceed && dir->traversable())
					{
						Text filename = dir->filename();
						if (dir->special(filename) == cio::Resource::None)
						{
							children.emplace_back(path, filename);
						}
					}

					// Close the directory to release resources
					dir.reset();

					for (const Path &child : children)
					{
						proceed = receiver(child);
						++count;
						if (!proceed)
						{
							break;
						}

						count += this->enumerateDirectory(child, Traversal::Recursive, receiver);
					}
				}
			}
		}
		return count;
	}
	
 	State Protocol::createParents(const Path &path, Traversal depth)
 	{
 		State state;
 	
 		if (depth)
 		{
 			if (path.isRoot())
 			{
 				Resource type = this->readType(path);
 				if (type != Resource::Directory)
 				{
 					throw Exception(Reason::Unusable, "Root parent is not a valid directory");
 				}
 				else
 				{
 					state.succeed(Reason::Exists);
 				}
 			}
 			else
 			{
 				Path parent = path.getParent();
 				state = this->createDirectory(parent, ModeSet::general(), depth - 1);	
 			}
 		}
 		
 		return state;
 	}

	State Protocol::createDirectory(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
		
		Resource type = this->readType(path);
		if (type == Resource::None)
		{
			state = this->createParents(path, createParents);
		
			std::unique_ptr<Protocol> dir = this->openDirectory(path, modes | Mode::Create);
			if (dir)
			{
				state.succeed();
				dir.reset();
			}
			else
			{
				state.fail();
			}
		}
		else if (type == Resource::Directory)
		{
			state.succeed(Reason::Exists);	
		}
		else
		{
			state.fail(Reason::Unusable);
			throw Exception(state, "Could not create directory as incompatible resource already exists at path");
		}
		
		return state;
	}

	Progress<std::uint64_t> Protocol::erase(const Path &path, Traversal depth)
	{
		return cio::fail(Action::Remove, Reason::Unsupported);
	}

	std::unique_ptr<Input> Protocol::openInput(const Path &path, ModeSet modes) const
	{
		return const_cast<Protocol *>(this)->openChannel(path, modes, Traversal::None);
	}

	std::unique_ptr<Output> Protocol::openOutput(const Path &path, ModeSet modes, Traversal createParents)
	{
		return this->openChannel(path, modes, createParents);
	}

	std::unique_ptr<Channel> Protocol::openChannel(const Path &path, ModeSet modes, Traversal createParents)
	{
		return nullptr;
	}
	
	State Protocol::createFile(const Path &path, ModeSet modes, Traversal createParents)
	{
		State state(Action::Create);
		
		Resource type = this->readType(path);
		if (type == Resource::None)
		{
			std::unique_ptr<Output> output = this->openOutput(path, modes | Mode::Create, createParents);
			if (output)
			{
				state.succeed();
				output.reset();
			}
			else
			{
				state.fail();
			}
		}
		else if (type == Resource::File)
		{
			state.succeed(Reason::Exists);	
		}
		else
		{
			state.fail(Reason::Unusable);
			throw Exception(state, "Could not create file as incompatible resource already exists at path");
		}
		
		return state;
	}
	
	Path Protocol::readLink(const Path &path, Traversal depth) const
	{
		return Path();
	}
			
	State Protocol::createLink(const Path &link, const Path &target, ModeSet modes, Traversal createParents)
	{
		return cio::fail(Reason::Unsupported);
	}
		
	Progress<std::uint64_t> Protocol::crossCopy(const Path &input, const Protocol &inputProto, const Path &output, Protocol &outputProto, Traversal depth, Traversal createParents)
	{
		Progress<std::uint64_t> progress(Action::Copy);		
		Resource inputType = inputProto.readType(input);
		Resource outputType = outputProto.readType(output);
		
		if (inputType == Resource::Directory)
		{
			progress.update(0, 1);
		
			if (outputType == Resource::None)
			{
				outputProto.createDirectory(output, ModeSet::general(), createParents);
				progress.update(1);
			}
			else if (outputType != Resource::Directory)
			{
				throw Exception(Reason::Unusable, "Cannot copy input directory to non-directory output");
			}
			
			if (depth)
			{
				std::vector<Path> entries = inputProto.listDirectory(input, Traversal::Immediate);
				progress.update(0, entries.size());
				
				for (const Path &entry : entries)
				{
					Progress<std::uint64_t> subprogress = Protocol::crossCopy(entry, inputProto, output / entry.getFilename(), outputProto, depth - 1, createParents);
					if (subprogress.total >= entries.size())
					{
						progress.update(0, subprogress.total - entries.size());
					}
					progress.update(subprogress.count);
				}
			}
		}
		else if (inputType != Resource::None)
		{
			progress.update(0, 1);
		
			std::unique_ptr<Input> inputReader = inputProto.openInput(input, ModeSet::readonly());
			std::unique_ptr<Output> outputWriter = outputProto.openOutput(output, ModeSet::general(), createParents);
			
			if (input && output)
			{
				inputReader->copyTo(*outputWriter, CIO_UNKNOWN_LENGTH);
				progress.update(1);
			}
		}
		
		return progress;
	}
	
	Progress<std::uint64_t> Protocol::copy(const Path &input, const Path &output, Traversal depth, Traversal createParents)
	{
		return Protocol::crossCopy(input, *this, output, *this, depth, createParents);
	}
	
	Progress<std::uint64_t> Protocol::crossMove(const Path &input, Protocol &inputProto, const Path &output, Protocol &outputProto, Traversal createParents)
	{
		Progress<std::uint64_t> progress = Protocol::crossCopy(input, inputProto, output, outputProto, Traversal::Recursive, createParents);
		if (progress.succeeded())
		{
			inputProto.erase(input, Traversal::Recursive);
		}
		return progress;
	}
	
	Progress<std::uint64_t> Protocol::move(const Path &input, const Path &output, Traversal createParents)
	{
		Progress<std::uint64_t> progress = this->copy(input, output, Traversal::Recursive, createParents);
		if (progress.succeeded())
		{
			this->erase(input, Traversal::Recursive);
		}
		return progress;
	}

	Protocol::operator bool() const noexcept
	{
		return this->traversable();
	}
}
