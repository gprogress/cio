/*=====================================================================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
=====================================================================================================================*/
#ifndef CIO_FIXEDTEXTTEST_H
#define CIO_FIXEDTEXTTEST_H

#include <cio/test/Suite.h>

namespace cio
{
	/**
	 * Conducts a unit test of the FixedText<N> class.
	 */
	class FixedTextTest : public cio::test::Suite
	{
		public:
			/**
			 * Constructor.
			 */
			FixedTextTest();

			/**
			 * Destructor.
			 */
			virtual ~FixedTextTest() noexcept override;

			/**
			 * Tests FixedText<N>::split.
			 */
			void splitTest();

			/**
			 * Tests operator== with FixedText.
			 */
			void equalTest();
	};
}

#endif

