/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "TemporaryFile.h"

#include "File.h"
#include "Filesystem.h"
#include "ProtocolFactory.h"

namespace cio
{
	TemporaryFile::TemporaryFile()
	{
		// nothing more to do
	}

	TemporaryFile::TemporaryFile(const Path &subdir, const Text &ext)
	{
		this->createTempFile(subdir, ext);
	}

	TemporaryFile::~TemporaryFile() noexcept
	{
		this->clear();
	}

	TemporaryFile::TemporaryFile(TemporaryFile &&in) noexcept :
		mTempFilePath(std::move(in.mTempFilePath))
	{
		// mothing more to do
	}

	TemporaryFile &TemporaryFile::operator=(TemporaryFile &&in) noexcept
	{
		mTempFilePath = std::move(in.mTempFilePath);

		return *this;
	}

	void TemporaryFile::clear() noexcept
	{
		// remove the temp file
		Filesystem fs;
		fs.removeNative(mTempFilePath.toNativeFile().c_str());

		// remove directories created
		fs.pruneEmptyDirectories(mTempFilePath.getParent());
	}

	void TemporaryFile::createTempFile()
	{
		Filesystem fs;
		fs.createFile(mTempFilePath);
	}

	void TemporaryFile::createTempFile(const Path &subdir, const Text &ext)
	{
		Filesystem fs;
		UniqueId tempName = UniqueId::random();
		mTempFilePath = fs.getTemporaryDirectory() / subdir / tempName.print();
		mTempFilePath.setExtension(ext);

		this->createTempFile();
	}

	File TemporaryFile::open()
	{
		return File(mTempFilePath);
	}

	void TemporaryFile::copyTo(const Path &path)
	{
		ProtocolFactory *pf = ProtocolFactory::getDefault();
		// if no file is given
		if (pf->isDirectory(path))
		{
			Path copyPath = path;
			copyPath.addChild(mTempFilePath.getFilename());
			pf->copy(mTempFilePath, copyPath);
		}
		else
		{
			pf->copy(mTempFilePath, path);
		}
	}

	const Path &TemporaryFile::getTempFilePath() noexcept
	{
		return mTempFilePath;
	}

	void TemporaryFile::setTempFilePath(const Path &path)
	{
		this->clear();
		mTempFilePath = path;
	}
}