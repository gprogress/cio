/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "MemoryChannel.h"

#include "Class.h"
#include "Resize.h"
#include "Seek.h"
#include "Traversal.h"

namespace cio
{
	Class<MemoryChannel> MemoryChannel::sMetaclass("cio::MemoryChannel");

	const Metaclass &MemoryChannel::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	const Metaclass &MemoryChannel::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	MemoryChannel::MemoryChannel() noexcept
	{
		// nothing to do
	}

	MemoryChannel::MemoryChannel(void *data, std::size_t length) noexcept :
		mBuffer(data, length)
	{
		// nothing more to do
	}
	
	MemoryChannel::MemoryChannel(const MemoryChannel &in) = default;
	
	MemoryChannel::MemoryChannel(MemoryChannel &&in) noexcept = default;
	
	MemoryChannel &MemoryChannel::operator=(const MemoryChannel &in) = default;
	
	MemoryChannel &MemoryChannel::operator=(MemoryChannel &&in) noexcept = default;

	MemoryChannel::~MemoryChannel() noexcept = default;

	void MemoryChannel::bind(Buffer buffer)
	{
		mBuffer = std::move(buffer);
	}

	void MemoryChannel::bind(void *data, std::size_t length)
	{
		mBuffer.bind(data, length);
	}

	Buffer MemoryChannel::unbind()
	{
		return std::move(mBuffer);
	}

	Buffer &MemoryChannel::buffer() noexcept
	{
		return mBuffer;
	}

	const Buffer &MemoryChannel::buffer() const noexcept
	{
		return mBuffer;
	}

	void MemoryChannel::clear() noexcept
	{
		mBuffer.clear();
		Channel::clear();
	}

	bool MemoryChannel::isOpen() const noexcept
	{
		return mBuffer.size() > 0;
	}

	bool MemoryChannel::linkedReadWritePosition() const noexcept
	{
		return true;
	}
		
	Resize MemoryChannel::resizable() const noexcept
	{
		return mBuffer.getResizePolicy();
	}
		
	bool MemoryChannel::appendable() const noexcept
	{
		return cio::growable(mBuffer.getResizePolicy());
	}

	Length MemoryChannel::size() const noexcept
	{
		return mBuffer.size();
	}

	bool MemoryChannel::empty() const noexcept
	{
		return mBuffer.empty();
	}

	Length MemoryChannel::capacity() const noexcept
	{
		return mBuffer.capacity();
	}
		
	Progress<Length> MemoryChannel::requestResize(Length desired) noexcept
	{
		Progress<Length> result(Action::Resize, desired);
			
		if (desired.sizeable())
		{
			mBuffer.resize(desired.size());
			result.succeed();
		}
		else if (desired >= SIZE_MAX)
		{
			result.fail(Reason::Overflow);
		}
		else 
		{
			result.fail(Reason::Underflow);
		}
			
		return result;
	}

	Length MemoryChannel::readable() const noexcept
	{
		return mBuffer.remaining();
	}

	Progress<std::size_t> MemoryChannel::requestRead(void *data, std::size_t bytes) noexcept
	{
		return mBuffer.requestRead(data, bytes);
	}

	Progress<std::size_t> MemoryChannel::requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Read, bytes);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
		
		if (absolute < 0)
		{
			result.fail(Reason::Invalid);
		}
		else if (absolute >= mBuffer.limit())
		{
			result.fail(Reason::Underflow);
		}
		else
		{
			std::size_t start = absolute.size();
			std::size_t toRead = std::min(mBuffer.size() - start, bytes);
			if (toRead > 0)
			{
				std::memcpy(data, mBuffer.data() + start, toRead);
			}
			result.count = toRead;
			result.succeed();
		}
		
		return result;
	}

	Length MemoryChannel::getReadPosition() const noexcept
	{
		return mBuffer.position();
	}

	Progress<Length> MemoryChannel::requestSeekToRead(Length position, Seek mode) noexcept
	{
		Progress<Length> result(Action::Seek);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
			
		if (absolute < 0)
		{
			result.fail(Reason::Underflow);
		}
		else if (absolute > mBuffer.limit())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.position(position.size());
			result.succeed();
		}				
		
		return result;
	}

	Progress<Length> MemoryChannel::requestDiscard(Length bytes) noexcept
	{
		Progress<Length> result(Action::Read);
		
		if (bytes < 0)
		{
			result.fail(Reason::Invalid);
		}
		else if (bytes > mBuffer.remaining())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.skip(bytes.size());
			result.succeed();
		}
			
		return result;
	}

	Length MemoryChannel::writable() const noexcept
	{
		return mBuffer.remaining();
	}

	Progress<std::size_t> MemoryChannel::requestWrite(const void *data, std::size_t bytes) noexcept
	{
		mBuffer.writable(bytes);
		return mBuffer.requestWrite(data, bytes);
	}

	Progress<std::size_t> MemoryChannel::requestWriteToPosition(const void *data, std::size_t bytes, Length position, Seek mode) noexcept
	{
		Progress<std::size_t> result(Action::Write, 0, bytes);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
		
		if (absolute < 0)
		{
			result.fail(Reason::Underflow);
		}
		else if (absolute > mBuffer.limit())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			std::size_t start = absolute.size();
			std::size_t toWrite = std::min(mBuffer.size() - start, bytes);
			std::memcpy(mBuffer.data() + start, data, toWrite);
			result.count = toWrite;
			result.succeed();
		}
		
		return result;
	}

	Length MemoryChannel::getWritePosition() const noexcept
	{
		return mBuffer.position();
	}

	Progress<Length> MemoryChannel::requestSeekToWrite(Length position, Seek mode) noexcept
	{
		Progress<Length>  result(Action::Seek);
		Length absolute = cio::position(position, mode, mBuffer.position(), mBuffer.limit());
			
		if (absolute < 0)
		{
			result.fail(Reason::Underflow);
		}
		else if (absolute > mBuffer.limit())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.position(position.size());
			result.succeed();
		}
		
		return result;
	}

	Progress<Length> MemoryChannel::requestSkip(Length bytes) noexcept
	{
		Progress<Length> result(Action::Write, 0, bytes);
			
		if (bytes < 0)
		{
			result.fail(Reason::Invalid);
		}
		else if (!mBuffer.available())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			mBuffer.skip(bytes.size());
			result.succeed();
		}
			
		return result;
	}

	Progress<Length> MemoryChannel::requestSplat(std::uint8_t value, Length count) noexcept
	{
		Progress<Length> result(Action::Write, 0, count);
		
		if (count < 0)
		{
			result.fail(Reason::Invalid);
		}
		else if (!mBuffer.available())
		{
			result.fail(Reason::Overflow);
		}
		else
		{
			result.count = mBuffer.requestSplat(value, count.size());
			result.succeed();
		}
	
		return result;
	}

	Progress<Traversal> MemoryChannel::flush(Traversal /*traversal*/) noexcept
	{
		return State(Action::Write, Status::Completed, Outcome::Completed);
	}

	Text MemoryChannel::getText(std::size_t bytes)
	{
		return mBuffer.getText(bytes).duplicate();
	}

	Text MemoryChannel::getTextLine(std::size_t bytes)
	{
		return mBuffer.getTextLine(bytes).duplicate();
	}
}
