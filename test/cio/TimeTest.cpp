/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TimeTest.h"

#include <cio/test/Assert.h>

#include <cio/Time.h>

#include <sstream>

namespace cio
{
	TimeTest::TimeTest() :
		cio::test::Suite("cio::Time")
	{
		this->addTestCase("setDate", &TimeTest::testSetDate);
		this->addTestCase("operator<<", &TimeTest::testPrintStream);
	}

	TimeTest::~TimeTest() noexcept = default;

	void TimeTest::testSetDate()
	{
		Time time;

		time.setDate(2005, 2, 15);
		CIO_ASSERT_EQUAL(std::time_t(1108425600ll), time.getPosixTime());

		// <cmath> functions can't handle times before 1970, revisit when we need this 
#if 0
		time.setDate(1885, 1, 31);
		CIO_ASSERT_EQUAL(std::time_t(0), time.getPosixTime());

		time.setDate(-1001, 11, 1);
		CIO_ASSERT_EQUAL(std::time_t(0), time.getPosixTime());
#endif
	}

	void TimeTest::testPrintStream()
	{
		Time time;
		{
			std::ostringstream stream;
			stream << time;
			CIO_ASSERT_EQUAL("1970-01-01 00:00:00", stream.str());
		}

		{
			time.setDate(2005, 2, 15);
			std::ostringstream stream;
			stream << time;
			CIO_ASSERT_EQUAL("2005-02-15 00:00:00", stream.str());
		}

		// <cmath> functions can't handle times before 1970, revisit when we need this 
#if 0
		{
			time.setDate(1885, 1, 31);
			std::ostringstream stream;
			stream << time;
			CIO_ASSERT_EQUAL("1885-01-31 00:00:00", stream.str());
		}

		{
			time.setDate(-1001, 11, 1);
			std::ostringstream stream;
			stream << time;
			CIO_ASSERT_EQUAL("-1001-01-31 00:00:00", stream.str());
		}
#endif
	}
}

