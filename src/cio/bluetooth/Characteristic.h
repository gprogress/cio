/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_CHARACTERISTIC_H
#define CIO_BLUETOOTH_CHARACTERISTIC_H

#include "Types.h"

#include "Attribute.h"
#include "Descriptor.h"
#include "Properties.h"

#include <chrono>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Characteristic describes the schema for a particular logical attribute value in Bluetooth GATT.
		 * It consists of a primary attribute defining the characteristic itself plus a sequence of auxiliary descriptor attributes
		 * that describe information about the characteristic or state that can be controlled such as subscriptions and broadcasting.
		 * The actual data value is typically the final descriptor and can be directly accessed.
		 */
		class CIO_BLUETOOTH_API Characteristic : public Attribute
		{
			public:
				/** Descriptor iterator type */
				using iterator = Attribute *;
				
				/** Const descriptor iterator type */
				using const_iterator = const Attribute *;
			
				/**
				 * Construct an empty characteristic.
				 * The base attribute will be initialized with TypeId::Characteristic
				 * and the start and end handle will be set to 0x0000 to indicate unknown.
				 */
				Characteristic() noexcept;
				
				/**
				 * Construct a characteristic with the given start handle.
				 * The Attribute type is set to TypeId::Characteristic and the end handle is set to 0x0000 to indicate unknown.
				 *
				 * @param handle The characteristic start handle
				 */
				explicit Characteristic(std::uint16_t handle) noexcept;
				
				/**
				 * Construct a characteristic with the given start handle and attribute client.
				 * The Attribute type is set to TypeId::Characteristic and the end handle is set to 0x0000 to indicate unknown.
				 *
				 * @param handle The start handle
				 * @param client The attribute client
				 */
				Characteristic(std::uint16_t handle, AttributeClient *client) noexcept;
				
				/**
				 * Construct a characteristic with the given start and end handle.
				 * The Attribute type is set to TypeId::Characteristic.
				 *
				 * @param start The characteristic start handle
				 * @param end The characteristic end handle
				 */
				Characteristic(std::uint16_t start, std::uint16_t end) noexcept;
				
				/**
				 * Construct a characteristic with the given start and end handle and attribute client
				 * The Attribute type is set to TypeId::Characteristic.
				 *
				 * @param start The characteristic start handle
				 * @param end The characteristic end handle
				 */
				Characteristic(std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept;
				
				/**
				 * Construct a copy of a characteristic.
				 *
				 * @param in The characteristic to copy
				 */
				Characteristic(const Characteristic &in);
				
				/**
				 * Construct a characteristic by transferring the state of an existing characteristic.
				 *
				 * @param in The characteristic to copy
				 */
				Characteristic(Characteristic &&in) noexcept;
				
				/**
				 * Copy a characteristic.
				 *
				 * @param in The characteristic to copy
				 * @return this characteristic
				 */
				Characteristic &operator=(const Characteristic &in);
				
				/**
				 * Move a characteristic.
				 *
				 * @param in The characteristic to move
				 * @return this characteristic
				 */
				Characteristic &operator=(Characteristic &&in) noexcept;
			
				/**
				 * Destructor.
				 */
				virtual ~Characteristic() noexcept override;
				
				/**
				 * Clears the state of the characteristic and all associated descriptors.
				 */
				virtual void clear() noexcept override;
				
				/**
				 * Sets the BLE client for the characteristic.
				 * This also sets the client on on all associated descriptor attributes.
				 *
				 * @param client The client to set
				 */
				virtual void setClient(AttributeClient *client) noexcept override;
			
				/**
				 * Synchronizes this characteristic with the attribute client.
				 * In addition to base Attribute synchronization, this also discovers
				 * descriptors and the characteristic end handle.
				 *
				 * @return the status of the synchronization (Success for updates, Exists if discovery was already complete, Unconnected if there is no client)
				 */
				virtual State synchronize() override;
				
				/**
				 * Validates the characteristic.
				 * In addition to base Attribute validation, this also ensures that the end handle is known, properly
				 * calculating it if not, and then checking to see if all descriptors are within the handle range (discarding ones that are not).
				 */
				virtual void validate() override;
				
				/**
				 * Gets the value handle for the characteristic's actual value attribute.
				 * This should be the next handle after the characteristic handle itself,
				 * but we don't assume that.
				 * 
				 * @return The value handle
				 */
				std::uint16_t getValueHandle() const noexcept;
				
				/**
				 * Sets the value handle for the characteristic handle range.
				 * This should typically be one after the characteristic handle itself,
				 * although we do not require it.
				 * 
				 * @param value The value handle
				 */
				void setValueHandle(std::uint16_t value) noexcept;
				
				/**
				 * Gets the value type for the characteristic's value.
				 * This is what is normally considered the characteristic type, but it's actually part of the characteristic attribute's value.
				 * 
				 * @note This just gets the value type of the value attribute
				 *
				 * @return the value type of the characteristic
				 */
				const UniqueId &getValueType() const noexcept;
				
				/**
				 * Sets the value type for the characteristic's value.
				 * This is what is normally considered the characteristic type, but it's actually part of the characteristic attribute's value.
				 * 
				 * @note This just sets the value type of the value attribute
				 *
				 * @param valueType the value type of the characteristic
				 */
				void setValueType(const UniqueId &valueType) noexcept;
				
				/**
				 * Gets the name of the attribute's value type.
				 * This requires a BLE Client to be set and will consult its type dictionary.
				 *
				 * @return the type name
				 */
				Text getValueName() const noexcept;
				
				/**
				 * Sets the attribute value type given its name.
				 * This requires a BLE Client to be set and will consult its type dictionary.
				 *
				 * @param name The type name
				 * @return the discovered type ID, or the empty ID if none matched
				 * @throw cio::Exception If no BLE Client exists, or the given name did not match any type
				 */
				const UniqueId &setValueTypeByName(const Text &name);
	
				// Value Read/Write API - convenience for accessing value attribute
				// Read API - uses LE Client to encode request and decode response;
				
				/**
				  * Uses the associated BLE Client to read the value of the attribute.
				  * The value handle must be set and the characteristic must be readable.
				  *
				  * @param listener The listener to receive the read data
				  * @return the actual number of bytes read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				std::size_t readValue(const AttributeCallback &listener) const;
				
				/**
				  * Uses the associated BLE Client to read the value of the attribute.
				  * The value handle must be set and the characteristic must be readable.
				  *
				  * @param data The data buffer to read the attribute value into
				  * @param length The maximum length to read into the data buffer
				  * @return the actual number of bytes read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				std::size_t readValue(void *data, std::size_t length) const;
				
				/**
				  * Uses the associated BLE Client to read a single value of the attribute as the given type T.
				  * The attribute handle must be set and the attribute must be readable.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to read a text value.
				  *
				  * @tparam <T> The data type of the value
				  * @return the value that was read
				  * @throw cio::Exception If there was no BLE Client or if the read request failed for any reason
				  */
				template <typename T>
				T readValue() const;
				
				// Write API - uses LE Client to encode request and decode response
				
				/**
				  * Uses the associated BLE Client to write the value of the attribute.
				  * The value handle must be set and the characteristic must be writable with response.
				  *
				  * @param data The data buffer containing the data to write to the attribute
				  * @param length The length to write from the data buffer
				  * @return the actual number of bytes written
				  * @throw cio::Exception If there was no BLE Client or if the write request failed for any reason
				  */
				std::size_t writeValue(const void *data, std::size_t length);
				
				/**
				  * Uses the associated BLE Client to write the value of the attribute without waiting for confirmation.
				  * The value handle must be set and the attribute must be writable without response.
				  *
				  * @param data The data buffer containing the data to write to the attribute
				  * @param length The length to write from the data buffer
				  * @return the actual number of bytes submitted to be written
				  * @throw cio::Exception If there was no BLE Client or if the write command failed to be submitted
				  */
				std::size_t sendValue(const void *data, std::size_t length);
				
				/**
				  * Uses the associated BLE Client to write the value of the attribute from an input of type T.
				  * The value handle must be set and the attribute must be writable with response.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to write a text value.
				  *
				  * @tparam <T> The data type of the value
				  * @param value The value to write
				  * @throw cio::Exception If there was no BLE Client or if the write request failed for any reason
				  */
				template <typename T>
				void writeValue(const T &value);
				
				/**
				  * Uses the associated BLE Client to write the value of the attribute from an input of type T without waiting for confirmation.
				  * The value handle must be set and the attribute must be writable without response.
				  * T can be any C++ primitive type, or it can be std::string or cio::Text to write a text value.
				  *
				  * @tparam <T> The data type of the value
				  * @param value The value to write
				  * @throw cio::Exception If there was no BLE Client or if the write command failed to be submitted
				  */
				template <typename T>
				void sendValue(const T &value);	
				
				// High-level API for state
				
				/**
				 * Checks whether the characteristic value is reported as readable.
				 * This consults the property bitset for the appropriate bit.
				 *
				 * @return whether the characteristic value is readable
				 */
				bool readable() const noexcept;
				
				/**
				 * Checks whether the characteristic value is reported as writable with the given parameters.
				 * If responseNeeded is true, this checks to see if write request with response is supported.
				 * If responseNeeded is false and signatureNeeded is true, this checks to see if a signed write command is supported.
				 * If responseNeeded is false and signatureNeeded is false, this checks to see if a (non-signed) write command is supported.
				 * If responseNeeded is true and signatureNeeded is true, this always returns false since Bluetooth does not support this combination.
				 *
				 * This consults the property bitset for the appropriate bit.
				 *
				 * @param responseNeeded Whether the write must return a confirmation response
				 * @param signatureNeeded Whether you want to cryptographically sign the write
				 * @return whether the characteristic value is writable with these parameters
				 */
				bool writable(bool responseNeeded, bool signatureNeeded) const noexcept;
				
				/**
				 * Subscribes to updates for the value of the characteristic.
				 * This uses the attribute client to set up a subscription.
				 *
				 * The best possible subscription mode for the value will be used if supported by the attribute client.
				 * Notify is preferred first, then Indicate, and if neither are available but the value is readable,
				 * a polling read loop will be used.
				 *
				 * @param listener The listener for the notifications
				 * @param interval The minimum interval between notifications
				 * @return the subscription
				 */
				const Subscription *subscribe(AttributeCallback &&listener, std::chrono::milliseconds interval);
				
				/**
				 * Unsubscribes a particular subscription for this characteristic's value.
				 * This uses the attribute client to remove the subscription.
				 *
				 * @param subscription The subscription to remove
				 * @return the status of the request
				 */
				State unsubscribe(const Subscription *subscription);
				
				/**
				 * Unsubscribes all subscriptions for this characteristic's value using the attribute client.
				 *
				 * @return the number of subscriptions removed and the status
				 */
				Progress<std::size_t> unsubscribe();
				
				/**
				 * Checks whether the characteristic value is reported to be subscribable for asynchronous notifications.
				 *
				 * This consults the property bitset for the appropriate bit.
				 * If true, then the client configuration descriptor should exist and can be used to control subscription.
				 * 
				 * @return whether the characteric value can be subscribed to for notification
				 */
				bool notifiable() const noexcept;
				
				/**
				 * Checks whether the characteristic value is reported to be currently subscribed for asynchronous notification.
				 *
				 * This checks to see if the client configuration descriptor attribute is found. If so, it will attempt to read the current
				 * value of the client configuration descriptor and check the appropriate bit to verify the current state.
				 * 
				 * @return whether the characteristic value currently is set up to send notifications
				 */
				bool notifying() const;
				
				/**
				 * Requests that the server sends out asynchronous notifications for changes to the characteristic value.
				 *
				 * This checks to see if the client configuration descriptor attribute is found. If so, it will attempt to write the current
				 * value of the client configuration with the bit set to enable or disable notification.
				 *
				 * @warning This bypasses the higher-level Attribute Client subscription mechanism, it is recommended to use subscribe() and unsubscribe() instead.
				 *
				 * If the request is to turn off notification and the characteristic does not support notification, the method will just do nothing.
				 *
				 * @param value Whether to enable or disable notification
				 */
				void requestNotification(bool value = true);
				
				/**
				 * Checks whether the characteristic value is reported to be subscribable for asynchronous indication,
				 * which is a notification in which the server expects a confirmation response.
				 *
				 * This consults the property bitset for the appropriate bit.
				 * If true, then the client configuration descriptor should exist and can be used to control subscription.
				 * 
				 * @return whether the characteric value can be subscribed to for indication
				 */
				bool indicatable() const noexcept;
				
				/**
				 * Checks whether the characteristic value is reported to be currently subscribed for asynchronous indication,
				 * which is a notification in which the server expects a confirmation response.
				 *
				 * This checks to see if the client configuration descriptor attribute is found. If so, it will attempt to read the current
				 * value of the client configuration and check the appropriate bit to verify the current state.
				 * 
				 * @return whether the characteristic value currently is set up to send indications
				 */
				bool indicating() const;
				
				/**
				 * Requests that the server sends out asynchronous indications for changes to the characteristic value,
				 * which is a notification in which the server expects a confirmation response.
				 *
				 * This checks to see if the client configuration descriptor attribute is found. If so, it will attempt to write the current
				 * value of the client configuration with the bit set to enable or disable notification.
				 *
				 * If the request is to turn off indication and the characteristic does not support indication, the method will just do nothing.
				 *
				 * @warning This bypasses the higher-level Attribute Client subscription mechanism, it is recommended to use subscribe() and unsubscribe() instead.
				 *
				 * @param value Whether to enable or disable indication
				 */
				void requestIndication(bool value = true);
				
				/**
				 * Checks whether the characteristic value is reported to be broadcastable in advertising packets.
				 *
				 * This consults the property bitset for the appropriate bit.
				 * If true, then the server configuration descriptor should exist and can be used to control broadcasting.
				 * 
				 * @return whether the characteric value can be broadcast in advertising packets
				 */
				bool broadcastable() const noexcept;
				
				/**
				 * Checks whether the characteristic value is reported to be currently set up for broadcasting in advertising packets.
				 *
				 * This checks to see if the server configuration descriptor is found. If so, it will attempt to read the current
				 * value of the server configuration and check the appropriate bit to verify the current state.
				 * 
				 * @return whether the characteristic value currently is set up to broadcast in advertising packets
				 */
				bool broadcasting() const;
				
				/**
				 * Requests that the server include this characteristic value in broadcast advertising packets.
				 *
				 * This checks to see if the server configuration descriptor is found. If so, it will attempt to write the current
				 * value of the server configuration with the bit set to enable or disable broadcasting.
				 *
				 * If the request is to turn off broadcasting and the characteristic does not support broadcasting, the method will just do nothing.
				 *
				 * @param value Whether to enable or disable broadcasting
				 */
				void requestBroadcast(bool value = true);
				
				/**
				 * Checks whether the characteristic is marked as having an extended properties value.
				 * This consults the property bitset for the appropriate bit.
				 * If true, then the extended properties descriptor should exist and can be used to read extended properties.
				 *
				 * @return whether the characteristic is marked as having extended properties
				 */
				bool extended() const noexcept;
				
				// Low level property handling
				
				/**
				 * Gets the property bitset exactly as discovered from the remote device.
				 *
				 * @return the property bitset
				 */
				Properties getProperties() const noexcept;
				
				/**
				 * Sets the property bitset. This should generally only be done by characteristic discovery.
				 *
				 * @param properties The property bitset
				 */
				void setProperties(Properties properties) noexcept;
				
				/**
				 * Gets the extended properties bitset.
				 *
				 * This checks to see if the extended properties descriptor is found. If so, it will attempt to read the current
				 * value of the extended properties.
				 * 
				 * If no extended properties exists, this will just return the empty bitset 0.
				 *
				 * @return the extended properties bitset
				 */
				std::uint8_t getExtendedProperties() const;
				
				/**
				 * Gets the client configuration bitset.
				 *
				 * This checks to see if the client configuration descriptor is found. If so, it will attempt to read the current
				 * value of the client configuration.
				 * 
				 * If no client configuration exists, this will just return the empty bitset 0.
				 *
				 * @return the client configuration bitset
				 */
				std::uint8_t getClientConfiguration() const;
				
				/**
				 * Sets the client configuration bitset.
				 *
				 * This checks to see if the client configuration descriptor is found. If so, it will attempt to write the current
				 * value of the client configuration.
				 * 
				 * If no client configuration exists but the value is just the empty bitset 0, then this method will just do nothing.
				 * Otherwise the method will fail since the expected command to the server won't be sent.
				 *
				 * @param value The new client configuration bitset
				 * @throw cio::Exception If the client configuration was non-zero but the attribute did not actually exist
				 */
				void setClientConfiguration(std::uint8_t value);
				
				/**
				 * Gets the server configuration bitset.
				 *
				 * This checks to see if the server configuration descriptor is found. If so, it will attempt to read the current
				 * value of the client configuration.
				 * 
				 * If no server configuration exists, this will just return the empty bitset 0.
				 *
				 * @return the server configuration bitset
				 */
				std::uint8_t getServerConfiguration() const;
				
				/**
				 * Sets the server configuration bitset.
				 *
				 * This checks to see if the server configuration descriptor attribute is found. If so, it will attempt to write the current
				 * value of the server configuration.
				 * 
				 * If no server configuration exists but the value is just the empty bitset 0, then this method will just do nothing.
				 * Otherwise the method will fail since the expected command to the server won't be sent.
				 *
				 * @param value The new server configuration bitset
				 * @throw cio::Exception If the server configuration was non-zero but the attribute did not actually exist
				 */
				void setServerConfiguration(std::uint8_t value);
				
				/**
				 * Gets the user description text.
				 *
				 * This checks to see if the user description descriptor is found. If so, it will attempt to read the current text value of the user description.
				 *
				 * If no user description exists, this returns the empty string.
				 *
				 * @return the user description
				 */			 
				Text getUserDescription() const;
				
				/**
				 * Sets the user description text.
				 *
				 * This checks to see if the user description descriptor is found. If so, it will attempt to write the current text value of the user description.
				 *
				 * @return the user description
				 * @throw cio::Exception If the user description attribute did not exist and the value to write was non-empty
				 */
				void setUserDescription(const Text &text);
				
				// Descriptors API - uses cached descriptors
				
				/**
				 * Checks whether there are descriptors associated with this characteristic.
				 * The value handle is not counted when considering this since it is always present and stored separately.
				 *
				 * @return whether there are descriptors
				 */
				bool empty() const noexcept;
				
				/**
				 * Gets the number of descriptors associated with this characteristic.
				 * The value handle is not counted when considering this since it is always present and stored separately.
				 *
				 * @return the number of descriptors
				 */
				std::size_t size() const noexcept;
				
				/**
				 * Gets a start iterator to the list of descriptors associated with this characteristic.
				 * The value handle is not part of this iterator traversal since it is stored separately, use getValueAttribute() for that.
				 *
				 * @return the descriptor start iterator
				 */
				Characteristic::iterator begin() noexcept;
				
				/**
				 * Gets an end iterator to the list of descriptors associated with this characteristic.
				 *
				 * @return the descriptor start iterator
				 */
				Characteristic::iterator end() noexcept;
				
				/**
				 * Gets a start iterator to the list of descriptors associated with this characteristic.
				 * The value handle is not part of this iterator traversal since it is stored separately, use getValueAttribute() for that.
				 *
				 * @return the descriptor start iterator
				 */
				Characteristic::const_iterator begin() const noexcept;
								
				/**
				 * Gets an end iterator to the list of descriptors associated with this characteristic.
				 *
				 * @return the descriptor start iterator
				 */
				Characteristic::const_iterator end() const noexcept;
				
				/**
				 * Gets the descriptor at the given sequence index in the characteristic.
				 * This performs discovery at most once then returns the cached results after that.
				 * The index is NOT the handle, use getDescriptor for that.
				 *
				 * @param idx The index
				 * @return the descriptor
				 */
				Descriptor &operator[](std::size_t idx);
				
				/**
				 * Gets the descriptor at the given sequence index in the characteristic.
				 * This performs discovery at most once then returns the cached results after that.
				 * The index is NOT the handle, use getDescriptor for that.
				 *
				 * @param idx The index
				 * @return the descriptor
				 */
				const Descriptor &operator[](std::size_t idx) const;
				
				// Iterator based searches
				
				/**
				 * Finds the iterator to the descriptor attribute with the given handle.
				 * If no such attribute is found, the end iterator is returned.
				 *
				 * @return the iterator to the descriptor, or end iterator if not found
				 */ 
				Characteristic::iterator find(std::uint16_t handle) noexcept;
				
				/**
				 * Finds the iterator to the descriptor attribute with the given handle.
				 * If no such attribute is found, the end iterator is returned.
				 *
				 * @return the iterator to the descriptor, or end iterator if not found
				 */ 
				Characteristic::const_iterator find(std::uint16_t handle) const noexcept;
				
				/**
				 * Finds the iterator to thedescriptor attribute with the given type.
				 * If no such attribute is found, the end iterator is returned.
				 *
				 * @return the iterator to the descriptor, or end iterator if not found
				 */ 
				Characteristic::iterator find(const UniqueId &type) noexcept;
				
				/**
				 * Finds the iterator to the first descriptor attribute with the given type.
				 * If no such attribute is found, the end iterator is returned.
				 *
				 * @return the iterator to the descriptor, or end iterator if not found
				 */ 			
				Characteristic::const_iterator find(const UniqueId &type) const noexcept;
				
				// Descriptor management API
				
				/**
				 * Gets the list of descriptor attributes associated with this characteristic.
				 * This includes all attributes associated with the characteristic except the chacteristic itself and its value attribute.
				 *
				 * @return the descriptor attributes
				 */
				std::vector<Descriptor> &getDescriptors() noexcept;
				
				/**
				 * Gets the list of descriptor attributes associated with this characteristic.
				 * This includes all attributes associated with the characteristic except the chacteristic itself and its value attribute.
				 *
				 * @return the descriptor attributes
				 */
				const std::vector<Descriptor> &getDescriptors() const noexcept;

				/**
				 * Manually copies the list of cached descriptor attributes and mark discovery as completed.
				 * This can be used by more general discovery mechanisms (depending on the attribute client)
				 * or to restore persisted descriptor information that was saved offline.
				 *
				 * @param attrs The attributes
				 * @throw std::bad_alloc If memory could not be allocated for the copy
				 */
				void setDescriptors(const std::vector<Descriptor> &attrs);
				
				/**
				 * Manually sets the list of cached descriptor attributes and mark discovery as completed.
				 * This can be used by more general discovery mechanisms (depending on the attribute client)
				 * or to restore persisted descriptor information that was saved offline.
				 *
				 * @param attrs The attributes
				 */
				void setDescriptors(std::vector<Descriptor> &&attrs) noexcept;
				
				/**
				 * Validates the list of currently cached descriptors.
				 * This will filter out all descriptors whose handles are outside of the characteristic range, filter out duplicates,
				 * and sort the remainder in ascending handle order.
				 */
				void validateDescriptors();

				/**
				 * Clears the cached list of descriptors previously discovered and mark discovery as not completed.
				 * This will force discovery to be redone the next time any descriptor is requested.
				 */
				void clearDescriptors() noexcept;
				
				/**
				 * Gets the descriptor attribute with the given handle.
				 * This method will return the value if that handle is specified, otherwise it will search the remaining descriptors.
				 *
				 * @parma handle The attribute handle
				 * @return the attribute 
				 * @throw std::out_of_range If the handle is not part of the characteristic range
				 * @throw cio::Exception If discovery failed
				 * @throw std::bad_alloc If allocating memory for characteristics failed
				 */
				Descriptor &getDescriptor(std::uint16_t handle);
				
				/**
				 * Gets the descriptor attribute with the given handle.
				 * This method will return the value if that handle is specified, otherwise it will search the remaining descriptors.
				 *
				 * @parma handle The attribute handle
				 * @return the attribute 
				 * @throw std::out_of_range If the handle is not part of the characteristic range
				 * @throw cio::Exception If discovery failed
				 * @throw std::bad_alloc If allocating memory for descriptors failed
				 */
				const Descriptor &getDescriptor(std::uint16_t handle) const;
				
				/**
				 * Gets the first descriptor whose type is the given UUID.
				 *
				 * @param type The v type
				 * @return the attribute
				 * @throw std::invalid_argument If the type is the invalid UUID
				 * @throw cio::Exception If discovery failed or no such descriptor c exists
				 * @throw std::bad_alloc If allocating memory for descriptors failed
				 */
				Descriptor &getDescriptor(const UniqueId &type);
				
				/**
				 * Gets the first descriptor whose type is the given UUID.
				 *
				 * @param type The v type
				 * @return the attribute
				 * @throw std::invalid_argument If the type is the invalid UUID
				 * @throw cio::Exception If discovery failed or no such descriptor c exists
				 * @throw std::bad_alloc If allocating memory for descriptors failed
				 */
				const Descriptor &getDescriptor(const UniqueId &type) const;				
				
				// Discovery API
				
				/**
				 * Uses the BLE Client to discover the end handle of the characteristic.
				 * This will start with one after the value handle (which is guaranteed to be set)
				 * and determine where the next service or characteristic attribute is to set the cutoff boundary.
				 *
				 * @return the end handle
				 */
				std::uint16_t discoverEndHandle() const;
				
				/**
				 * Discovers all attributes that belong to the characteristic using the attribute client.
				 * This does not consult existing cached descriptors.
				 * If the end handle is known, it is used. Otherwise it is discovered and set during this call.
				 *
				 * @return the discovered descriptors
				 */
				std::vector<Descriptor> discoverDescriptors() const;
				
				/**
				 * Synchronizes the cached list of descriptors, performing discovery if needed.
				 *
				 * @return the status of the synchronization (Success for updates, Exists if discovery was already complete, Unconnected if there is no client)
				 */
				State synchronizeDescriptors();

				/**
				 * Gets the Descriptor representing the actual data value of the characteristic.
				 * This should be located on characteristic discovery and its handle should be the same as the characteristic end handle.
				 *
				 * @return the value attribute
				 */
				Descriptor &getValueDescriptor() noexcept;
				
				/**
				 * Gets the Descriptor representing the actual data value of the characteristic.
				 * This should be located on characteristic discovery and its handle should be the same as the characteristic end handle.
				 *
				 * @return the value attribute
				 */
				const Descriptor &getValueDescriptor() const noexcept;
				
				/**
				 * Sets the Descriptor representing the actual data value of the characteristic.
				 * This method should generally only be called by characteristic discovery and the attribute handle should be the same as the characteristic end handle.
				 *
				 * @param handle the value attribute
				 */				
				void setValueDescriptor(const Descriptor &handle);
				
				// Index based searches, used to implement iterators
				
				/**
				 * Locates the internal index offset of the first descriptor attribute with the given type.
				 * If no such attribute is found, the index to the end of the descriptor array is returned.
				 *
				 * @return the index of the located descriptor
				 */ 
				std::size_t locateDescriptorIndex(const UniqueId &type) const noexcept;
				
				/**
				 * Locates the internal index offset of the descriptor attribute with the given handle.
				 * If no such attribute is found, the index to the end of the descriptor array is returned.
				 *
				 * @return the index of the located descriptor
				 */ 
				std::size_t locateDescriptorIndex(std::uint16_t handle) const noexcept;
				
				/**
				 * Locates the range of internal index offsets of the descriptor attributes inside the given handle range.
				 * The handle range is inclusive of start and end.
				 * The returned index range is exclusive of the end.
				 * If no matching attributes are found, an empty range (start and end the same) are returned.
				 *
				 * @param start The lowest handle of the matching descriptor attributes to find
				 * @param end The highest handle (inclusive) of the matching descriptor attributes to find
				 * @return the index range (exclusive of end) of the located descriptors
				 */ 
				std::pair<std::size_t, std::size_t> locateDescriptorRange(std::uint16_t start, std::uint16_t end) const noexcept;
				
			private:			
				/** Metaclass for this class */		
				static Class<Characteristic> sMetaclass;
				
				/** The list of all known descriptors */
				std::vector<Descriptor> mDescriptors;
				
				/** The value attribute */
				Descriptor mValue;
			
				/** The characteristic properties bitset */
				Properties mProperties;
		};
	}
}

/* Inline implementation */
namespace cio
{
	namespace bluetooth
	{
		template <typename T>
		inline T Characteristic::readValue() const
		{
			return mValue.read<T>();
		}
		
		template <typename T>
		inline void Characteristic::writeValue(const T &value)
		{
			mValue.write(value);
		}

			
		template <typename T>
		inline void Characteristic::sendValue(const T &value)
		{
			mValue.send(value);
		}
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

