/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_UNIQUEID_H
#define CIO_BLUETOOTH_UNIQUEID_H

#include "Types.h"

#include <cio/UniqueId.h>

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The Bluetooth Unique ID extends the baseline CIO Unique ID to also support 16-bit Bluetooth short IDs.
		 * It is otherwise functionally and bytewise equivalent to a baseline 128-bit Unique ID.
		 */
		class CIO_BLUETOOTH_API UniqueId : public cio::UniqueId
		{
			public:
				/** The high 8 bytes of the Bluetooth UUID base used for 16-bit short UUIDs. */
				static const std::uint64_t BLUETOOTH_UUID_HIGH;
				
				/** The low 8 bytes of the Bluetooth UUID base used for 16-bit short UUIDs. */
				static const std::uint64_t BLUETOOTH_UUID_LOW;
			
				/** The Bluetooth UUID base used for 16-bit short UUIDs. */
				static const UniqueId BLUETOOTH_BASE_UUID;
			
				/**
				 * Creates the empty Unique ID of all zero bytes.
				 */
				UniqueId() noexcept;
				
				/**
				 * Construct a Unique ID from the high 8 bytes and low 8 bytes.
				 * Note that on a little endian system, the low bytes will actually be first in memory.
				 *
				 * @param high The high 8 bytes
				 * @param low The low 8 bytes
				 */
				UniqueId(std::uint64_t high, std::uint64_t low);	
				
				/**
				 * Construct a copy of a base CIO unique ID.
				 *
				 * @param in The unique ID to copy
				 */
				UniqueId(const cio::UniqueId &id) noexcept;
				
				/**
				 * Construct a Unique ID from the given 16-bit Bluetooth short UUID.
				 *
				 * @param shortId The short ID
				 */				
				UniqueId(std::uint16_t shortId) noexcept;
				
				/**
				 * Construct a Unique ID from the given standard 16-bit Bluetooth Type ID enumeration.
				 *
				 * @param shortId The short ID enumeration
				 */				
				UniqueId(TypeId shortId) noexcept;
				
				/**
				 * Construct a Unique ID by parsing C-style null delimited text.
				 * If parsing fails, the unique ID will be set to the empty ID.
				 *
				 * @param text The text to parse
				 */
				UniqueId(const char *text) noexcept;
				
				/**
				 * Construct a Unique ID by parsing text.
				 * If parsing fails, the unique ID will be set to the empty ID.
				 *
				 * @param text The text to parse
				 * @param length The number of bytes in the text
				 */
				UniqueId(const char *text, std::size_t length) noexcept;
				
				/**
				 * Construct a Unique ID by parsing text.
				 * If parsing fails, the unique ID will be set to the empty ID.
				 *
				 * @param text The text to parse
				 */
				UniqueId(const std::string &text) noexcept;
							
				/**
				 * Construct a copy of a unique ID.
				 *
				 * @param in The unique ID to copy
				 */
				UniqueId(const UniqueId &in) noexcept;
				
				/**
				 * Copy a unique ID.
				 *
				 * @param in The unique ID to copy
				 * @return this ID
				 */
				UniqueId &operator=(const UniqueId &in) noexcept;
				
				/**
				 * Copy a base CIO unique ID.
				 *
				 * @param in The unique ID to copy
				 * @return this ID
				 */
				UniqueId &operator=(const cio::UniqueId &in) noexcept;
				
				/**
				 * Sets this Unique ID to represent the given 16-bit Bluetooth short UUID.
				 *
				 * @param shortId the 16-bit short ID
				 * @return this ID
				 */				
				UniqueId &operator=(std::uint16_t shortId) noexcept;
				
				/**
				 * Sets this Unique ID to represent the given 16-bit Bluetooth standard UUID enumeration.
				 *
				 * @param shortId the 16-bit short ID enumeration
				 * @return this ID
				 */					
				UniqueId &operator=(TypeId shortId) noexcept;
				
				/**
				 * Destructor.
				 */
				~UniqueId() noexcept;
				
				/**
				 * Checks whether this Unique ID represents a Bluetooth 16-bit short UUID.
				 *
				 * @return this is a Bluetooth short UUID
				 */
				bool isBluetoothId() const noexcept;
				
				/**
				 * Gets the 16-bit Bluetooth short UUID from this full 128-bit Unique ID.
				 * If this unique ID is not actually a 16-bit short ID, this method will return 0.
				 *
				 * @return the 16-bit short UUID, or 0x0000 if this Unique ID isn't a short ID
				 */
				std::uint16_t getShortId() const noexcept;
				
				/**
				 * Gets the 16-bit Bluetooth short UUID enumeration from this full 128-bit Unique ID.
				 * If this unique ID is not actually a 16-bit short ID, this method will return Type::None.
				 *
				 * @return the 16-bit short UUID, or Type::None if this Unique ID isn't a short ID
				 */
				TypeId getBluetoothId() const noexcept;
				
				/**
				 * Sets this UUID to the given 16-bit Bluetooth short UUID.
				 *
				 * @param id The short ID
				 */
				void setShortId(std::uint16_t id) noexcept;
				
				/**
				 * Sets this UUID to the given 16-bit Bluetooth short UUID enumeration.
				 *
				 * @param type the 16-bit short UUID type
				 */
				void setBluetoothId(TypeId type) noexcept;
				
				/**
				 * Checks to see if this ID represents a predefined Bluetooth group type.
				 * This is true if the type is a Bluetooth short ID and is either the value for TypeId::PrimaryService, TypeId::SecondaryService,
				 * or TypeId::Characteristic.
				 *
				 * @return whether this ID represents a group type
				 */
				bool isGroupType() const noexcept;
				
				/**
				 * Prints the Bluetooth UUID to the given text buffer.
				 * If the ID is a valid 16-bit Bluetooth short ID, then the short ID will be printed as four hexadecimal digits.
				 * Otherwise the conventional format for 128-bit unique IDs is used.
				 *
				 * @param buffer The buffer to print to
				 * @param length The maximum number of bytes to print
				 * @return the actual number of bytes that would be needed to fully print this UUID
				 */
				std::size_t print(char *buffer, std::size_t length) const noexcept;
				
				/**
				 * Prints the Bluetooth UUID into a returned string.
				 * If the ID is a valid 16-bit Bluetooth short ID, then the short ID will be printed as four hexadecimal digits.
				 * Otherwise the conventional format for 128-bit unique IDs is used.
				 *
				 * @return the printed UUID
				 * @throw std::bad_alloc If the memory could not be allocated for the string
				 */
				std::string print() const;
							
				/**
				 * Parses the given text to extract out a Bluetooth UUID.
				 * This will first check for the 16-bit Bluetooth short ID formatted either as a decimal or hexadecimal integer in range,
				 * then fall back to the conventional format for 128-bit unique IDs.
				 *
				 * If parsing fails, the unique ID will be reset to the empty UUID.
				 *
				 * @param buffer The text to parse
				 * @return the status of the parse attempt
				 */
				TextParseStatus parse(const Text &text) noexcept;
		};
				
		/**
		 * Performs a byte swap of the Bluetooth UUID.
		 * 
		 * @param value The type ID
		 * @return the byte swapped type ID
		 */ 
		CIO_BLUETOOTH_API UniqueId swapBytes(const UniqueId &value) noexcept;
		
		/**
		 * Prints the Bluetooth UUID to a C++ stream.
		 * If it matches a short 16-bit UUID, that will be printed as a hexadecimal number.
		 * Otherwise the full 128-bit UUID will be printed in conventional UUID format.
		 *
		 * @param stream The stream to print to
		 * @param value The value to print
		 * @return the stream
		 */
		CIO_BLUETOOTH_API std::ostream &operator<<(std::ostream &stream, const UniqueId &value);
	}
}

#endif

