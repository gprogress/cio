/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Metaclass.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_POINTER_H
#define CIO_POINTER_H

#include "Factory.h"

#include <memory>

namespace cio
{
	/**
	 * The CIO Pointer templates implements an optionally-unique smart pointer using the CIO Factory.
	 * 
	 * If the underlying factory provides an allocator, this class is equivalent to std::unique_ptr in that
	 * it uniquely owns the object and will deallocate it upon destruction or reassignment of a different pointer.
	 * Unlike std::unique_ptr, this class also supports copy construction and copy assignment to duplicate the
	 * allocated object via the factory and metaclass.
	 * 
	 * If the underlying factory does not provide an allocator, this class is equivalent to a non-owning raw pointer
	 * and (unlike std::unique_ptr or std::shared_ptr) can be used to safely wrap pointers managed externally.
	 * 
	 * For trivial types, the factory is optimized to omit the metaclass since that is determined by the type T.
	 * Otherwise, the metaclass is propagated through so even if you move or copy into a pointer of different type
	 * such as a base class, the real underlying type is preserved via the metaclass.
	 * 
	 * @tparam <T> The pointee type
	 */
	template <typename T>
	class Pointer
	{
		public:
			/**
			 * Grant direct access to different instantiations of Pointer so that
			 * move semantics can be easily implemented.
			 * 
			 * @tparam U The other pointee type
			 */
			template <typename U>
			friend class Pointer;

			/**
			 * Default constructor.
			 */
			inline Pointer() noexcept :
				mPointee(nullptr),
				mFactory(nullptr)
			{
				// nothing more to do
			}

			/**
			 * Construct using the null pointer.
			 * Equivalent to default construction.
			 * 
			 * @param ptr The null pointer
			 */
			inline Pointer(std::nullptr_t ptr) noexcept :
				mPointee(nullptr),
				mFactory(nullptr)
			{
				// nothing more to do
			}

			/**
			 * Construct a non-owning view of the given pointer.
			 * 
			 * @param pointee The pointer to view
			 */
			inline Pointer(T *pointee) noexcept :
				mPointee(pointee),
				mFactory(nullptr)
			{
				// nothing more to do
			}

			/**
			 * Construct using the given pointer and factory.
			 * 
			 * @param pointee The pointer to adopt
			 * @param factory The factory to adopt
			 */
			inline Pointer(T *pointee, Factory<T> factory) noexcept :
				mPointee(pointee),
				mFactory(factory)
			{
				// nothing more to do
			}

			/**
			 * Constructor that adopts a moved std::unique_ptr.
			 * 
			 * The metaclass is inferred from the pointee if set or the declared metaclass for U if null.
			 * The owning allocator is the default allocator for the metaclass.
			 *
			 * @tparam U The input pointer type, which must implicitly convert to T
			 * @param in The pointer to adopt
			 */
			template <typename U>
			inline Pointer(std::unique_ptr<U> pointee) noexcept :
				mPointee(pointee.release()),
				mFactory(&cio::getPointerMetaclass(mPointee))
			{
				// nothing more to do
			}

			/**
			 * Copy constructor.
			 *
			 * This will use the factory from the input pointer to allocate and
			 * construct a copy of the input pointer if it is non-null.
			 * If the input does not have an allocator, the default allocator for the pointee's
			 * metaclass is used.
			 *
			 * @param in The pointer to copy
			 * @throw std::bad_alloc If allocating the memory for the copy failed
			 */
			inline Pointer(const Pointer<T> &in) :
				mPointee(in.mFactory ? in.mFactory.copy(in.mPointee) : in.mPointee),
				mFactory(in.mFactory)
			{
				// nothing more to do
			}

			/**
			 * Copy constructor from a pointer of a different type U.
			 *
			 * This will use the factory from the input pointer to allocate and
			 * construct a copy of the input pointer if it is non-null.
			 * If the input does not have an allocator, the default allocator for the pointee's
			 * metaclass is used.
			 *
			 * @tparam U The input pointer type, which must implicitly convert to T
			 * @param in The pointer to copy
			 * @throw std::bad_alloc If allocating the memory for the copy failed
			 */
			template <typename U>
			inline Pointer(const Pointer<U> &in) :
				mPointee(in.mFactory ? in.mFactory.copy(in.mPointee) : in.mPointee),
				mFactory(in.mFactory)
			{
				// nothing more to do
			}

			/**
			 * Move constructor.
			 *
			 * @param in The pointer to move
			 */
			inline Pointer(Pointer<T> &&in) noexcept :
				mPointee(in.mPointee),
				mFactory(in.mFactory)
			{
				in.mPointee = nullptr;
			}

			/**
			 * Move constructor from a pointer of a different type U.
			 * 
			 * @tparam U The input pointer type, which must implicitly convert to T
			 * @param in The pointer to move
			 */
			template <typename U>
			inline Pointer(Pointer<U> &&in) noexcept :
				mPointee(in.mPointee),
				mFactory(in.mFactory)
			{
				in.mPointee = nullptr;
			}

			/**
			 * Assign the null pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 *
			 * @tparam ptr The null pointer
			 * @return this pointer
			 */
			inline Pointer<T> &operator=(std::nullptr_t ptr) noexcept
			{
				mPointee = mFactory.destroy(mPointee);
				mFactory = nullptr;
				return *this;
			}

			/**
			 * Copy assignment from a pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 * 
			 * This will use the factory from the input pointer to allocate and
			 * construct a copy of the input pointer if it is non-null.
			 * If the input does not have an allocator, the default allocator for the pointee's
			 * metaclass is used.
			 *
			 * @param in The pointer to copy
			 * @return this pointer
			 * @throw std::bad_alloc If allocating the memory for the copy failed
			 */
			inline Pointer<T> &operator=(const Pointer<T> &in)
			{
				if (&in != this)
				{
					mFactory.destroy(mPointee);
					mPointee = in.mFactory ? in.mFactory.copy(in.mPointee) : in.mPointee;
					mFactory = in.mFactory;
				}
				return *this;
			}

			/**
			 * Copy assignment from a pointer of a different type U.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 * 
			 * This will use the factory from the input pointer to allocate and
			 * construct a copy of the input pointer if it is non-null.
			 * If the input does not have an allocator, the default allocator for the pointee's
			 * metaclass is used.
			 *
			 * @tparam U The input pointer type, which must implicitly convert to T
			 * @param in The pointer to copy
			 * @return this pointer
			 * @throw std::bad_alloc If allocating the memory for the copy failed
			 */
			template <typename U>
			inline Pointer<T> &operator=(const Pointer<U> &in)
			{
				if (&in != this)
				{
					mFactory.destroy(mPointee);
					mPointee = in.mFactory ? in.mFactory.copy(in.mPointee) : in.mPointee;
					mFactory = in.mFactory;
				}
				return *this;
			}

			/**
			 * Move assignment.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 *
			 * @param in The pointer to move
			 * @return this pointer
			 */
			inline Pointer<T> &operator=(Pointer<T> &&in) noexcept
			{
				if (&in != this)
				{
					mFactory.destroy(mPointee);
					mPointee = in.mPointee;
					mFactory = in.mFactory;
					in.mPointee = nullptr;
				}
				return *this;
			}

			/**
			 * Move assignment from a pointer of a different type U.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 * 
			 * @tparam U The input pointer type, which must implicitly convert to T
			 * @param in The pointer to move
			 * @return this pointer
			 */
			template <typename U>
			inline Pointer<T> &operator=(Pointer<U> &&in) noexcept
			{
				if (&in != this)
				{
					mFactory.destroy(mPointee);
					mPointee = in.mPointee;
					mFactory = in.mFactory;
					in.mPointee = nullptr;
				}
				return *this;
			}

			/**
			 * Assigns a std::unique_ptr by moving it.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 * The metaclass is inferred from the pointee if set or the declared metaclass for U if null.
			 * The owning allocator is the default allocator for the metaclass.
			 * 
			 * @tparam <U> The pointee type
			 * @param pointer The unique pointer to adopt
			 * @return this pointer
			 */
			template <typename U>
			inline Pointer<T> &operator=(std::unique_ptr<U> pointer) noexcept
			{
				mFactory.destroy(mPointee);
				mPointee = pointer.release();
				mFactory = &cio::getPointerMetaclass(mPointee);
				return *this;
			}

			/**
			 * Destructor.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 */
			inline ~Pointer() noexcept
			{
				mFactory.destroy(mPointee);
			}

			/**
			 * Gets the underlying native pointer.
			 * 
			 * @return the underlying pointer
			 */
			inline T *get() const noexcept
			{
				return mPointee;
			}

			/**
			 * Dereferences the pointer.
			 * This results in undefined behavior of the pointee is null.
			 * 
			 * @return the deferenced pointee
			 */
			inline T &operator*() const noexcept
			{
				return *mPointee;
			}

			/**
			 * Delegates method invocation to the underlying pointee.
			 * 
			 * @return the underlying pointee
			 */
			inline T *operator->() const noexcept
			{
				return mPointee;
			}

			/**
			 * Resets the pointer to the null pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 */
			inline void clear() noexcept
			{
				mPointee = mFactory.destroy(mPointee);
				mFactory = nullptr;
			}

			/**
			 * Resets the pointer to the null pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 */
			inline void reset() noexcept
			{
				mPointee = mFactory.destroy(mPointee);
				mFactory = nullptr;
			}

			/**
			 * Resets the pointer to the null pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 *
			 * @param ptr The null pointer
			 */
			inline void reset(std::nullptr_t ptr)
			{
				mPointee = mFactory.destroy(mPointee);
				mFactory = nullptr;
			}

			/**
			 * Resets the pointer to point to a non-owning view of the given pointer.
			 * If the existing pointee is owned by this pointer, it is destroyed using the factory.
			 * 
			 * @param data The new pointee
			 */
			inline void reset(T *data)
			{
				if (data != mPointee)
				{
					mFactory.destroy(mPointee);
					mPointee = data;
					mFactory = nullptr;
				}
			}

			/**
			 * Gets a non-owning view of the pointee.
			 * 
			 * @return the non-owning view
			 */
			inline Pointer<T> view() const noexcept
			{
				return Pointer<T>(mPointee);
			}

			/**
			 * Allocates a new owned copy of the current value, and move
			 * the current value into it. If the current factory does not have
			 * an allocator, the default allocator is used instead.
			 *
			 * @return the moved copy of the current value
			 */
			inline Pointer<T> copy() const
			{
				Factory<T> factory = mFactory ? mFactory : Factory<T>();
				return Pointer<T>(factory.copy(mPointee), factory);
			}

			/**
			 * Allocates a new owned copy of the current value, and move
			 * the current value into it. If the current factory does not have
			 * an allocator, the default allocator is used instead.
			 * 
			 * @return the moved copy of the current value
			 */
			inline Pointer<T> move()
			{
				Factory<T> factory = mFactory ? mFactory : Factory<T>();
				return Pointer<T>(factory.move(mPointee), factory);
			}

			/**
			 * If the pointer currently does not own its pointee, allocate
			 * an owned copy of it.
			 * 
			 * @throw std::bad_alloc If allocating memory for the copy failed
			 */
			inline void internalize()
			{
				if (!mFactory)
				{
					mFactory.setAllocator();
					mPointee = mFactory.copy(mPointee);
				}
			}

			/**
			 * Allocates a new owned object of type T.
			 * The existing pointee is destroyed if it is owned by the pointer.
			 * 
			 * @tparam A The arugment types to use to construct the new object
			 * @param args The arguments to use to contruct the new object
			 * @return this pointer 
			 */
			template <typename U, typename... A>
			inline void create(A &&...args)
			{
				this->reset();
				mFactory.setCreatedMetaclass(cio::getDeclaredMetaclass<U>());
				mFactory.setAllocator();

				void *memory = mFactory.allocate();
				mPointee = new (memory) U(std::forward<A>(args)...);
			}

			/**
			 * Implicitly cast to the underlying pointer when needed.
			 * 
			 * @return the underlying pointer
			 */
			inline operator T *() const noexcept
			{
				return mPointee;
			}

			/**
			 * Checks to see if the pointer is set.
			 * 
			 * @return whether the pointer is non-null
			 */
			inline explicit operator bool() const noexcept
			{
				return mPointee != nullptr;
			}

		private:
			/** Object being pointed to */
			T *mPointee;

			/** Factory used to manage the pointer */
			Factory<T> mFactory;
	};
}

#endif
