/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "AttributeCodec.h"

#include "ATT.h"
#include "ATTError.h"
#include "AttributeClient.h"
#include "Characteristic.h"
#include "Service.h"
#include "TypeId.h"
#include "UniqueId.h"

#include <cio/Buffer.h>
#include <cio/Class.h>

namespace cio
{
	namespace bluetooth
	{
		Class<AttributeCodec> AttributeCodec::sMetaclass("cio::bluetooth::AttributeCodec");

		cio::Exception AttributeCodec::decodeErrorResponse(Buffer &packet) const
		{
			cio::Exception error;
			
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
			
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::Error:
					packet.skip(1);
					break;
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not an error response when it was expected to be");
			}
			
			if (packet.remaining() != 4)
			{
				throw cio::Exception(Reason::Unparsable, "Error response packet is not the expected length of 4");
			}
			
			ATT opcode = packet.get<ATT>();
			std::uint16_t handle = packet.getLittle<std::uint16_t>();
			ATTError ecode = packet.get<ATTError>();
	
			// TODO create bluetooth specific exception with these values?
				
			error.reason(status(ecode));
			error.setStaticMessage(print(ecode));
			
			return error;
		}
		
		void AttributeCodec::encodeMaxPacketSizeRequest(Buffer &packet, std::size_t clientMax) const
		{
			std::uint16_t clientMtu = static_cast<std::uint16_t>(std::min(std::max(AttributeClient::DEFAULT_MAX_PACKET_SIZE, clientMax), AttributeClient::HIGHEST_MAX_PACKET_SIZE));
		
			packet.put(ATT::MaxPacketSizeRequest);
			packet.putLittle(clientMtu);
		}
				
		std::size_t AttributeCodec::decodeMaxPacketSizeResponse(Buffer &packet) const
		{
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
			
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::MaxPacketSizeResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
					throw this->decodeErrorResponse(packet);
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a max packet size response");
			}
			
			std::uint16_t serverMtu = packet.getLittle<std::uint16_t>();
			return serverMtu;
		}
		
		void AttributeCodec::encodeDiscoverServicesRequest(Buffer &packet, std::uint16_t start, std::uint16_t end) const
		{
			packet.put(ATT::ReadByGroupRequest); // opcode for request by group - skip byte swap due to 1 byte
			packet.putLittle(start); // start service range
			packet.putLittle(end); // end service range
			packet.putLittle(TypeId::PrimaryService); // primary service short UUID
		}

		Progress<std::uint16_t> AttributeCodec::decodeDiscoverServicesResponse(Buffer &packet, std::vector<Service> &services) const
		{	
			Progress<std::uint8_t> preamble = this->decodeDiscoverServicesPreamble(packet);
			Progress<std::uint16_t> result(preamble.status);

			if (preamble.status == Status::Started)
			{
				std::size_t entryCount = packet.remaining() / preamble.count;
				services.reserve(services.size() + entryCount);
				
				if (preamble.count == 6)
				{
					for (std::size_t i = 0; i < entryCount; ++i)
					{
						services.emplace_back(this->decodeNextShortServiceDiscovery(packet));
					}
				}
				else if (preamble.count == 20)
				{
					for (std::size_t i = 0; i < entryCount; ++i)
					{
						services.emplace_back(this->decodeNextServiceDiscovery(packet));
					}
				}
				else
				{
					throw cio::Exception(Reason::Unparsable, "Discover services response item length was unexpected value");
				}
				
				result.count = static_cast<std::uint16_t>(entryCount);
				
				// If we happen to find the last service mark this operation as done
				// Servers will generally have the last service end handle be marked this way
				if (entryCount == 0 || services.back().getEndHandle() == 0xFFFFu)
				{
					result.status = Status::Completed;
				}
			}
			
			return result;
		}
		
		Progress<std::uint8_t> AttributeCodec::decodeDiscoverServicesPreamble(Buffer &packet) const
		{
			Progress<std::uint8_t> preamble;
		
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
			
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::ReadByGroupResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
				{
					cio::Exception e = this->decodeErrorResponse(packet);
					if (e.reason() == Reason::Missing)
					{
						preamble.status = Status::Completed;
					}
					else
					{
						throw e;
					}
					break;
				}
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a read by group response for discovering services");
			}
			
			if (preamble.status == Status::None)
			{
				// Read 1 byte entry length
				std::uint8_t entryLength = packet.get();
				if (entryLength != 6 && entryLength != 20)
				{
					throw cio::Exception(Reason::Unparsable, "Packet is malformed because service discovery length is not 6 (short UUIDs) or 20 (full UUIDs)");
				}
				
				if (packet.remaining() % entryLength != 0)
				{
					throw cio::Exception(Reason::Unparsable, "Packet is malformed because the total length is not a multiple of the entry length");
				}
				
				preamble.count = entryLength;
				preamble.status = Status::Started;
			}
			
			return preamble;
		}

		Service AttributeCodec::decodeNextShortServiceDiscovery(Buffer &packet) const
		{
			Service service;
			
			service.setType(TypeId::PrimaryService);
			service.setHandle(packet.getLittle<std::uint16_t>());
			service.setEndHandle(packet.getLittle<std::uint16_t>());
			service.setServiceType(packet.getLittle<TypeId>());
			
			return service;
		}
				
		Service AttributeCodec::decodeNextServiceDiscovery(Buffer &packet) const
		{
			Service service;
			
			service.setType(TypeId::PrimaryService);
			service.setHandle(packet.getLittle<std::uint16_t>());
			service.setEndHandle(packet.getLittle<std::uint16_t>());
			service.setServiceType(packet.getLittle<UniqueId>());
			
			return service;
		}
		
		void AttributeCodec::encodeFindServiceRequest(Buffer &packet, const UniqueId &serviceId, std::uint16_t low /* = 0x0001u */, std::uint16_t high /* = 0xFFFFu */) const
		{
			packet.put(ATT::FindByTypeRequest);
			packet.putLittle(low);
			packet.putLittle(high);
			packet.putLittle(TypeId::PrimaryService);
			
			if (serviceId.isBluetoothId())
			{
				packet.putLittle(serviceId.getShortId());
			}
			else
			{
				packet.putLittle(serviceId);
			}
		}
				
		Service AttributeCodec::decodeFindServiceResponse(Buffer &packet) const
		{
			Service service;
			Logger logger("cio::bluetooth::AttributeCodec");
			
			logger.info() << "Attempting to process find by type response for locating a service range";
			
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
			
			
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::FindByTypeResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
				{
					logger.info() << "Opcode is for an error response, packet size = " << packet.remaining();
					throw this->decodeErrorResponse(packet);
					break;
				}

					
				default:
					logger.info() << "Opcode is unexpected value = " << static_cast<unsigned>(*packet.current());
					throw cio::Exception(Reason::Unexpected, "Packet is not a find by type response for locating a specific service");
			}
			
			// Packet is sequenced as pairs of uint16_t ranges, so remainder of packet must be a multiple of four bytes
			if (packet.remaining() % 4 != 0)
			{
				throw cio::Exception(Reason::Unparsable, "Packet for find by type value response does not have a length that evenly divides pairs of handle ranges");
			}
			
			// Not sure if an empty range list can happen, but just in case it does
			if (packet.remaining() == 0)
			{
				throw cio::Exception(Reason::Missing, "Requested service was not found");
			}
			
			// Strictly speaking you can have more than one result, however this call we assume you're searching for a unique primary service
			// So we just return the first one
			
			logger.info() << "Service response found";
			
			service.setType(TypeId::PrimaryService);
			service.setHandle(packet.getLittle<std::uint16_t>());
			service.setEndHandle(packet.getLittle<std::uint16_t>());
			
			return service;
		}
				
		void AttributeCodec::encodeDiscoverCharacteristicsRequest(Buffer &packet, std::uint16_t start, std::uint16_t end) const
		{
			packet.put(ATT::ReadByTypeRequest); // opcode for request by type - skip byte swap due to 1 byte
			packet.putLittle(start); // start service range
			packet.putLittle(end); // end service range
			packet.putLittle(TypeId::Characteristic); // characteristics short UUID
		}
		
		Progress<std::uint16_t> AttributeCodec::decodeDiscoverCharacteristicsResponse(Buffer &packet, std::vector<Characteristic> &characteristics) const
		{
			Progress<std::uint8_t> preamble = this->decodeDiscoverCharacteristicsPreamble(packet);
			Progress<std::uint16_t> result(preamble.status);		

			if (preamble.count > 0)
			{
				std::size_t entryCount = packet.remaining() / preamble.count;
				characteristics.reserve(characteristics.size() + entryCount);
				
				if (preamble.count == 7)
				{
					for (std::size_t i = 0; i < entryCount; ++i)
					{
						characteristics.emplace_back(this->decodeNextShortCharacteristic(packet));
					}
				}
				else if (preamble.count == 21)
				{
					for (std::size_t i = 0; i < entryCount; ++i)
					{
						characteristics.emplace_back(this->decodeNextCharacteristic(packet));
					}
				}
				else
				{
					throw cio::Exception(Reason::Unparsable, "Discover characteristics response item length was unexpected value");
				}
				
				// WARNING: in general we do not know the characteristic end handles here, because you need to know service boundaries
				// and adjacent characteristics to discover that
				// So we leave the end handle 0x0000 to indicate it needs to be found
				
				result.count = static_cast<std::uint16_t>(entryCount);
								
				// In the unlikely event that we read the last possible characteristic, mark this operation as complete
				if (!characteristics.empty() && characteristics.back().getEndHandle() == 0xFFFFu)
				{
					result.status = Status::Completed;
				}
			}
			
			return result;
		}
		
		Progress<std::uint8_t> AttributeCodec::decodeDiscoverCharacteristicsPreamble(Buffer &packet) const
		{
			Progress<std::uint8_t> preamble;
			
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
			
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::ReadByTypeResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
				{
					cio::Exception e = this->decodeErrorResponse(packet);
					if (e.reason() == Reason::Missing) // this is how we are notified that we're done
					{
						preamble.status = Status::Completed;
					}
					else
					{
						throw e;
					}
					
					break;
				}

					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a read by group response for discovering services");
			}
			
			if (preamble.status == Status::None)
			{
				// Read 1 byte entry length
				std::uint8_t entryLength = packet.get<std::uint8_t>();
				if (entryLength != 7 && entryLength != 21)
				{
					throw cio::Exception(Reason::Unparsable, "Packet is malformed because characteristic length is not 7 (short UUIDs) or 21 (full UUIDs)");
				}
				
				if (packet.remaining() % entryLength != 0)
				{
					throw cio::Exception(Reason::Unparsable, "Packet is malformed because the total length is not a multiple of the entry length");
				}
				
				preamble.status = Status::Started;
				preamble.count = entryLength;
			}
			
			return preamble;	
		}
		
		Characteristic AttributeCodec::decodeNextShortCharacteristic(Buffer &packet) const
		{
			Characteristic characteristic;
			
			characteristic.setType(TypeId::Characteristic);
			characteristic.setHandle(packet.getLittle<std::uint16_t>());
			characteristic.setProperties(packet.get<Properties>());
			
			std::uint16_t valueHandle = packet.getLittle<std::uint16_t>();
			characteristic.setValueHandle(valueHandle);			
			characteristic.setValueType(packet.getLittle<TypeId>());
			
			// WARNING: in general we do not know the characteristic end handles here, because you need to know service boundaries
			// and adjacent characteristics to discover that
			// So we leave the end handle 0x0000 to indicate it needs to be found
			
			return characteristic;
		}
				
		Characteristic AttributeCodec::decodeNextCharacteristic(Buffer &packet) const
		{
			Characteristic characteristic;
			
			characteristic.setType(TypeId::Characteristic);
			characteristic.setHandle(packet.getLittle<std::uint16_t>());
			characteristic.setProperties(packet.get<Properties>());
			
			std::uint16_t valueHandle = packet.getLittle<std::uint16_t>();
			characteristic.setValueHandle(valueHandle);			
			characteristic.setValueType(packet.getLittle<UniqueId>());
			characteristic.setEndHandle(valueHandle);
			
			// WARNING: in general we do not know the characteristic end handles here, because you need to know service boundaries
			// and adjacent characteristics to discover that
			// So we leave the end handle 0x0000 to indicate it needs to be found
			
			return characteristic;		
		}
		
		void AttributeCodec::encodeFindInformationRequest(Buffer &packet, std::uint16_t start /* = 0x0001u */, std::uint16_t end /* = 0xFFFFu */) const
		{
			packet.put(ATT::FindInformationRequest);
			packet.putLittle(start);
			packet.putLittle(end);
		}
		
		Progress<std::uint8_t> AttributeCodec::decodeFindInformationPreamble(Buffer &packet) const
		{
			Progress<std::uint8_t> preamble;
		
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::FindInformationResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
				{
					cio::Exception e = this->decodeErrorResponse(packet);
					if (e.reason() == Reason::Missing)
					{
						preamble.status = Status::Completed;	
					}
					else
					{
						throw e;
					}
					break;
				}
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a find information response");
			}
			
			if (preamble.status == Status::None)
			{
				std::uint8_t itemFormat = packet.get<std::uint8_t>();
				if (itemFormat == 0x01)
				{
					preamble.count = 4;
				}
				else if (itemFormat == 0x02)
				{
					preamble.count = 18;			
				}
				else
				{
					throw cio::Exception(Reason::Unparsable, "Find information response item format is not allowed values 0x01 or 0x02");
				}
				
				if (packet.remaining() % preamble.count != 0)
				{
					throw cio::Exception(Reason::Unparsable, "Find information response length is not a multiple of the item length");
				}
				
				preamble.status = Status::Started;	
			}
			
			return preamble;
		}
		
		Progress<std::uint16_t> AttributeCodec::decodeFindInformationResponse(Buffer &packet, std::vector<Attribute> &attrs) const
		{
			Progress<std::uint8_t> preamble = this->decodeFindInformationPreamble(packet);
			Progress<std::uint16_t> result(preamble.status);
			
			if (preamble.status == Status::Started)
			{
				result.count = static_cast<std::uint16_t>(packet.remaining() / preamble.count);
				attrs.reserve(attrs.size() + result.count);
				
				if (preamble.count == 4)
				{
					for (std::size_t i = 0; i < result.count; ++i)
					{
						attrs.emplace_back(this->decodeNextShortAttributeResponse(packet));
					}
				}
				else if (preamble.count == 18)
				{
					for (std::size_t i = 0; i < result.count; ++i)
					{
						attrs.emplace_back(this->decodeNextAttributeResponse(packet));
					}
				}
				else
				{
					throw cio::Exception(Reason::Unparsable, "Find information response item length was unexpected value");
				}			
				
				// In the rare case where we find the last possible attribute handle, mark as done
				if (!attrs.empty() && attrs.back().getHandle() == 0xFFFFu)
				{
					result.status = Status::Completed;
				}
			}
			
			return result;
		}
				
		Attribute AttributeCodec::decodeNextShortAttributeResponse(Buffer &packet) const
		{
			Attribute attribute;
			
			attribute.setHandle(packet.getLittle<std::uint16_t>());
			attribute.setType(packet.getLittle<std::uint16_t>());
			
			return attribute;
		}
				
		Attribute AttributeCodec::decodeNextAttributeResponse(Buffer &packet) const
		{
			Attribute attribute;
			
			attribute.setHandle(packet.getLittle<std::uint16_t>());
			attribute.setType(packet.getLittle<UniqueId>());
			
			return attribute;		
		}
				
		void AttributeCodec::encodeReadRequest(Buffer &packet, std::uint16_t handle) const
		{
			packet.put(ATT::ReadRequest);
			packet.putLittle(handle);
		}
				
		Buffer &AttributeCodec::decodeReadResponse(Buffer &packet) const
		{
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::ReadResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
					throw this->decodeErrorResponse(packet);
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a read response");
			}
			
			// Remainder of packet data after opcode should be the actual data (and it can be 0 length)
			
			return packet;
		}
		
		void AttributeCodec::encodeReadByTypeRequest(Buffer &packet, const UniqueId &type, std::uint16_t low /* = 0x0001u */, std::uint16_t high /* = 0xFFFFu */) const
		{
			packet.put(ATT::ReadByTypeRequest);
			packet.putLittle(low);
			packet.putLittle(high);
			
			if (type.isBluetoothId())
			{
				packet.putLittle(type.getShortId());
			}
			else
			{
				packet.putLittle(type);
			}
		}
	
		std::uint16_t AttributeCodec::decodeReadByTypeResponse(Buffer &packet) const
		{
			Progress<std::uint8_t> preamble = this->decodeReadByTypePreamble(packet);
			std::uint16_t handle = 0;

			if (preamble.count > 0)
			{
				// Read the handle then reduce the limit to the first item end
				// if there's more than one item, that's not the problem of this method
				handle = packet.getLittle<std::uint16_t>();
				packet.limit(packet.position() + preamble.count - 2);
			}
			
			return handle;
		}
		
		Progress<std::uint16_t> AttributeCodec::decodeReadByTypeResponse(Buffer &packet, const AttributeCallback &listener) const
		{
			Progress<std::uint8_t> preamble = this->decodeReadByTypePreamble(packet);
			Progress<std::uint16_t> result(preamble.status);

			if (preamble.count > 0)
			{
				std::size_t itemLength = preamble.count;
				std::size_t itemCount = packet.remaining() / itemLength;
				
				std::uint16_t handle = 0;
				
				for (std::size_t i = 0; i < itemCount; ++i)
				{
					handle = packet.get<std::uint16_t>();
					listener(handle, packet.current(), itemLength);
					packet.skip(itemLength);
					++result.count;
				}
				
				// in the rare case where the last Progress handle is also the last possible handle
				// mark this operation as complete						
				if (handle == 0xFFFFu)
				{
					result.status = Status::Completed;
				}
			}
			
			return result;
		}
		
		Progress<std::uint8_t> AttributeCodec::decodeReadByTypePreamble(Buffer &packet) const
		{
			Progress<std::uint8_t> preamble;
			
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::ReadByTypeResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
				{
					cio::Exception e = this->decodeErrorResponse(packet);
					if (e.reason() == Reason::Missing)
					{
						// this is how completion is indicated
						preamble.status = Status::Completed;
					}
					else
					{
						throw e;
					}
					break;
				}
				
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a read by type response");
			}
			
			if (preamble.status == Status::None)
			{
				std::uint8_t itemLength = packet.get<std::uint8_t>();
				if (itemLength < 3)
				{
					throw cio::Exception(Reason::Unparsable, "Read by type response item length is below minimum size of 3 bytes");
				}
				
				if (packet.remaining() % itemLength != 0)
				{
					throw cio::Exception(Reason::Unparsable, "Read by type response packet length is not a multiple of the item length");
				}
				
				preamble.count = itemLength;
				preamble.status = Status::Started;
			}
			
			return preamble;
		}
		
		Buffer &AttributeCodec::initiateWriteRequest(Buffer &packet, std::uint16_t handle) const
		{
			packet.put(ATT::WriteRequest);
			packet.putLittle(handle);
			return packet;
		}
				
		void AttributeCodec::encodeWriteRequest(Buffer &packet, std::uint16_t handle, const void *data, std::size_t length) const
		{
			packet.put(ATT::WriteRequest);
			packet.putLittle(handle);
			packet.write(data, length);
		}
				
		void AttributeCodec::decodeWriteResponse(Buffer &packet) const
		{
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::WriteResponse:
					packet.skip(1);
					break;
					
				case ATT::Error:
					throw this->decodeErrorResponse(packet);
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a write response");
			}
			
			// Write response is just a notification
			if (packet.remaining())
			{
				throw cio::Exception(Reason::Unexpected, "Write response has unexpected data in packet");
			}
		}
		
		Buffer &AttributeCodec::initiateWriteCommand(Buffer &packet, std::uint16_t handle) const
		{
			packet.put(ATT::Write);
			packet.putLittle(handle);
			return packet;
		}
				
		void AttributeCodec::encodeWriteCommand(Buffer &packet, std::uint16_t handle, const void *data, std::size_t length) const
		{
			packet.put(ATT::Write);
			packet.putLittle(handle);
			packet.write(data, length);
		}
				
		std::uint16_t AttributeCodec::decodeNotify(Buffer &packet) const
		{
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::Notify:
					packet.skip(1);
					break;
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not a notify command");
			}
			
			std::uint16_t handle = packet.getLittle<std::uint16_t>();
			
			// Remainder of packet data after opcode should be the actual data (and it can be 0 length)
			
			return handle;
		}
				
				
		std::uint16_t AttributeCodec::decodeIndicateRequest(Buffer &packet) const
		{
			if (!packet.remaining())
			{
				throw cio::Exception(Reason::Underflow, "No packet data available to decode");
			}
						
			// Peek at first byte, see if it's the right opcode
			switch (static_cast<ATT>(*packet.current()))
			{
				case ATT::Indicate:
					packet.skip(1);
					break;
					
				default:
					throw cio::Exception(Reason::Unexpected, "Packet is not an indicate request");
			}
			
			std::uint16_t handle = packet.getLittle<std::uint16_t>();
			
			// Remainder of packet data after opcode should be the actual data (and it can be 0 length)
			
			return handle;
		}
		
		void AttributeCodec::encodeIndicateResponse(Buffer &packet) const
		{
			packet.put(ATT::Confirm);
		}
	}
}

