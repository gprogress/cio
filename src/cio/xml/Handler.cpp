/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Handler.h"

namespace cio
{
	namespace xml
	{
		Handler::Handler() = default;

		Handler::~Handler() = default;

		void Handler::startDocument()
		{
			// do nothing
		}

		void Handler::endDocument()
		{
			// do nothing
		}

		void Handler::startElement(Text element)
		{
			// do nothing
		}

		void Handler::endElement(Text element)
		{
			// do nothing
		}

		void Handler::processAttribute(Text key, Text value)
		{
			// do nothing
		}

		void Handler::processTextChunk(Text text)
		{
			// do nothing
		}

		void Handler::processComment(Text text)
		{
			// do nothing
		}

		void Handler::handleError(Text message)
		{
			// do nothing
		}
	}
}
