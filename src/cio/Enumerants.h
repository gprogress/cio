/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_ENUMERANTS_H
#define CIO_ENUMERANTS_H

#include "Types.h"

#include "Class.h"
#include "Enumerant.h"

#include <cstring>
#include <ostream>

namespace cio
{
	/**
	 * The Enumerants<E> is a base metaclass for the a C++ enum. In addition to baseline metaclass features provided by Class<E>
	 * it provides a C++-compatible container interface for actually exploring the enumerant codes, values, descriptions, and so on
	 * via iterators and standard methdods like size() and find().
	 * 
	 * The intended use for it is to specialize the Enuemration<E> subclass to construct the base class with the proper settings
	 * and to determine the final code storage location for it.
	 *
	 * @tparam E The enum class type
	 */
	template <typename E>
	class Enumerants : public Class<E>
	{
		public:
			/** The underlying integer type of the enumeration */
			using Underlying = typename std::underlying_type<E>::type;

			/**
			 * Constructor.
			 * 
			 * @tparam N The number of enumerants in the fixed array
			 *
			 * @param name The C++ enum name
			 * @param values The list of enumerant values
			 * @param def The default value to use when the value is missing or unusuable
			 * @param gaps Whether there are gaps in the enum integer values such that the sequence is noncontiguous
			 */
			template <std::size_t N>
			Enumerants(const char *name, const Enumerant<E>(&values)[N], E def = E(), bool gaps = false) :
				Class<E>(name),
				mValues(values),
				mSize(N),
				mDefault(def),
				mGaps(gaps)
			{
				// nothing more to do
			}

			/**
			 * Destructor.
			 */
			virtual ~Enumerants() noexcept override = default;

			/**
			 * Gets the start iterator to the enumerant list.
			 *
			 * @return the start iterator
			 */
			inline const Enumerant<E> *begin() const noexcept
			{
				return this->mValues;
			}

			/**
			 * Gets the start iterator to the enumerant list.
			 *
			 * @return the start iterator
			 */
			inline const Enumerant<E> *end() const noexcept
			{
				return this->mValues + this->mSize;
			}

			/**
			 * Gets the start iterator to the enumerant list.
			 *
			 * @return the start iterator
			 */
			inline const Enumerant<E> *cbegin() const noexcept
			{
				return this->mValues;
			}

			/**
			 * Gets the start iterator to the enumerant list.
			 *
			 * @return the start iterator
			 */
			inline const Enumerant<E> *cend() const noexcept
			{
				return this->mValues + this->mSize;
			}

			/**
			 * Gets the enumerant at the given position in the enumeration list.
			 *
			 * @param idx The index
			 * @return the enumerant definition
			 */
			inline const Enumerant<E> &operator[](std::size_t idx) const noexcept
			{
				return this->mValues[idx];
			}

			/**
			 * Gets the offset of an enumerant from the first enumerant.
			 * For contiguous enumerations, this is also the index.
			 *
			 * @param value The enum value
			 * @return the enumerant offset
			 */
			constexpr auto offset(E value) const noexcept
			{
				return static_cast<Underlying>(value) - static_cast<Underlying>(this->mValues[0].value);
			}

			/**
			 * Finds the enumerant index by conducting a binary search assuming enumerants are sorted in ascending order.
			 * This will always work even if enumerations are mGaps, but is slower than a direct lookup for contiguous enumerations.
			 *
			 * @param value The enum value
			 * @return the enumerant index, or SIZE_MAX if not found
			 */
			inline std::size_t indexBinarySearch(E value) const noexcept
			{
				std::size_t found = SIZE_MAX;
				std::size_t low = 0;
				std::size_t high = this->mSize;
				std::size_t current = (low + high) / 2;
				while (low < high)
				{
					if (this->mValues[current].value < value)
					{
						low = current + 1;
					}
					else if (this->mValues[current].value > value)
					{
						high = current;
					}
					else
					{
						found = current;
						break;
					}

					current = (low + high) / 2;
				}

				return found;
			}

			/**
			 * Finds the enumerant index of the given value.
			 *
			 * @param value The enum value
			 * @return the enumerant index
			 */
			inline std::size_t index(E value) const noexcept
			{
				return this->mGaps ? this->indexBinarySearch(value) : this->offset(value);
			}

			/**
			 * Finds the enumerant iterator for the given value.
			 * If not found, it will return the end iterator.
			 * 
			 * @param value The enum value
			 * @return the iterator to the enumerant
			 */
			inline const Enumerant<E> *find(E value) const noexcept
			{
				std::size_t offset = this->index(value);
				return this->mValues + std::min(offset, this->mSize);
			}

			/**
			 * Finds the enumerant iterator for the given label.
			 * If not found, it will return the end iterator.
			 * Note that this will match based on the label not the print text if they are different.
			 *
			 * @param label the label
			 * @return the iterator to the enumerant
			 */
			inline const Enumerant<E> *find(const char *label) const noexcept
			{
				std::size_t offset = this->mSize;
				for (std::size_t i = 0; i < offset; ++i)
				{
					if (std::strcmp(this->mValues[i].label, label) == 0)
					{
						offset = i;
						break;
					}
				}
				return this->mValues + offset;
			}

			/**
			 * Determines whether the enum value is present.
			 * Normally this should always be true, but if you decode a binary enum values that doesn't actually exist,
			 * it can be false.
			 * 
			 * @param value The enum value
			 * @return whether the enum value is valid
			 */
			inline bool count(E value) const noexcept
			{
				return this->index(value) < this->mSize;
			}

			/**
			 * Checks whether this enumeration is empty.
			 * Should always be false, but implemented for container compatibility.
			 * 
			 * @return whether the container is empty
			 */
			inline bool empty() const noexcept
			{
				return this->mSize > 0;
			}

			/**
			 * Gets the number of enumeration values.
			 * 
			 * @return the number of values
			 */
			inline std::size_t size() const noexcept
			{
				return this->mSize;
			}

			/**
			 * Gets the enum label.
			 *
			 * @param value The enum value
			 * @return the enum label or nullptr if it doesn't exist
			 */
			inline const char *label(E value) const noexcept
			{
				std::size_t offset = this->index(value);
				return (offset < this->mSize) ? this->mValues[offset].label : nullptr;
			}

			/**
			 *
			 * Gets the enum description.
			 *
			 * @param value The enum value
			 * @return the enum description, or null if it doesn't exist
			 */
			inline const char *description(E value) const noexcept
			{
				std::size_t offset = this->index(value);
				return (offset < this->mSize) ? this->mValues[offset].description : nullptr;
			}

			/**
			 *
			 * Gets the enum text for printing if different from the label.
			 *
			 * @param value The enum value
			 * @return the enum print text
			 */
			inline const char *text(E value) const noexcept
			{
				std::size_t offset = this->index(value);
				return (offset < this->mSize) ? this->mValues[offset].text : nullptr;
			}

			/**
			 * Gets the length of the enum when printed to UTF-8 text.
			 *
			 * @param value The enum value
			 * @return the UTF-8 text length of the printed enum
			 */
			inline std::size_t strlen(E value) const noexcept
			{
				return std::strlen(this->print(value));
			}

			/**
			 * Prints the enumeration to UTF-8 text.
			 *
			 * @param value The enum value
			 * @return the UTF-8 text
			 */
			inline const char *print(E value) const noexcept
			{
				std::size_t offset = this->index(value);
				return (offset < this->mSize) ? this->mValues[offset].print() : this->find(mDefault)->print();
			}

			/**
			 * Prints the enumeration to the given UTF-8 text buffer.
			 *
			 * @param value The enum value
			 * @param buffer The UTF-8 text buffer
			 * @param capacity The capacity of the text buffer
			 * @return the number of characters actually needed
			 */
			inline std::size_t print(E value, char *buffer, std::size_t capacity) const noexcept
			{
				const char *text = this->print(value);
				std::size_t length = std::strlen(text);
				cio::reprint(text, length, buffer, capacity);
				return length;
			}

			/**
			 * Prints the value to a C++ stream.
			 * 
			 * @param value The value to print
			 * @param stream The stream to print to
			 * @return the stream
			 */
			inline std::ostream &stream(E value, std::ostream &stream) const
			{
				return stream << this->print(value);
			}

			/**
			 * Gets the enum value that best matches the given text.
			 *
			 * @param text The text value
			 * @return the enum value
			 */
			inline E parse(const char *text) const
			{
				E value = this->mDefault;
				for (const Enumerant<E> &entry : *this)
				{
					if (std::strcmp(entry.print(), text) == 0)
					{
						value = entry.value;
						break;
					}
				}
				return value;
			}

		private:
			/** The list of enumerant values in ascending order */
			const Enumerant<E> *mValues;

			/** The number of enumerant values */
			std::size_t mSize;

			/** The default enum value to use if the value is missing or unusable. */
			E mDefault;

			/** Whether the enumerant values have gaps between their integer codes */
			bool mGaps;
	};
}

#endif
