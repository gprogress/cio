/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TypeDictionary.h"

#include "TypeId.h"

namespace cio
{
	namespace bluetooth
	{
		TypeDictionary::TypeDictionary() noexcept :
			mUsePredefinedTypes(true)
		{
			// nothing more to do
		}
		
		TypeDictionary::TypeDictionary(const TypeDictionary &in) = default;
		
		TypeDictionary::TypeDictionary(TypeDictionary &&in) noexcept :
			mEntries(std::move(in.mEntries)),
			mUsePredefinedTypes(in.mUsePredefinedTypes)
		{
			// nothing more to do
		}
		
		TypeDictionary &TypeDictionary::operator=(const TypeDictionary &in) = default;
		
		TypeDictionary &TypeDictionary::operator=(TypeDictionary &&in) noexcept
		{
			mEntries = std::move(in.mEntries);
			mUsePredefinedTypes = in.mUsePredefinedTypes;
			return *this;
		}
		
		TypeDictionary::~TypeDictionary() noexcept = default;
		
		void TypeDictionary::clear() noexcept
		{
			mEntries.clear();
			mUsePredefinedTypes = true;
		}
		
		bool TypeDictionary::getUsePredefinedTypes() noexcept
		{
			return mUsePredefinedTypes;
		}
		
		void TypeDictionary::setUsePredefinedTypes(bool value) noexcept
		{
			mUsePredefinedTypes = value;
		}
				
		bool TypeDictionary::insert(const UniqueId &id, Text name)
		{
			mEntries.emplace_back();
			mEntries.back().id = id;
			mEntries.back().name = std::move(name);
			return true;
		}
		
		UniqueId TypeDictionary::id(const Text &name) const noexcept
		{
			UniqueId found;
			
			for (const Entry &entry : mEntries)
			{
				if (entry.name == name)
				{
					found = entry.id;
					break;
				}
			}
			
			if (!found && mUsePredefinedTypes)
			{
				TypeId shortId = TypeId::None;
				if (cio::bluetooth::parse(name, shortId))
				{
					found = shortId;
				} 
			}
			
			return found;
		}
		
		Text TypeDictionary::name(const UniqueId &id) const noexcept
		{
			Text found;
			
						
			for (const Entry &entry : mEntries)
			{
				if (entry.id == id)
				{
					found = entry.name;
					break;
				}
			}
			
			if (!found)
			{
				std::uint16_t shortId = id.getShortId();
				if (mUsePredefinedTypes && shortId != 0)
				{
					found.internalize(cio::bluetooth::print(static_cast<TypeId>(shortId)));
				}
				else
				{
					found.bind("Unknown");
				}
			}
			
			return found;
		}	
	}
}
