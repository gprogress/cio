/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Ownership.h"

#include <ostream>

namespace cio
{
	namespace
	{
		static const char *sOwnershipText[] = {
			"None",
			"Embed",
			"Reference",
			"Allocate",
			"Share"
		};
	}

	const char *print(Ownership value) noexcept
	{
		const char *text = "Unknown";
		std::size_t idx = static_cast<std::size_t>(value);
		if (idx < 4)
		{
			text = sOwnershipText[idx];
		}
		return text;
	}

	std::ostream &operator<<(std::ostream &s, Ownership value)
	{
		return s << print(value);
	}
}
