/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_NET_IPV4_H
#define CIO_NET_IPV4_H

#include "Types.h"

#include <string>

#include <cio/Text.h>
#include <cio/Validation.h>

namespace cio
{
	namespace net
	{
		/**
		 * The IPv4 class provides a runtime representation of IPv4 addresses in native runtime byte order.
		 *
		 * @note The byte order is native for ease of runtime operations using integer math. On little-endian host machines, it will
		 * need to be converted to "network" (big-endian) byte order to be used in native Address buffers using a byte swap.
		 * Using this class with the Address class will swap automatically as needed.
		 */
		class CIO_NET_API IPv4
		{
			public:
				/**
				 * Gets the IPv4 address that represents the "any available address" for servers.
				 *
				 * @return the IPv4 "any" address
				 */
				static IPv4 any() noexcept;

				/**
				 * Gets the IPv4 address for localhost.
				 *
				 * @return the IPv4 "localhost" address
				 */
				static IPv4 localhost() noexcept;

				/**
				 * Gets the IPv4 address for network broadcasts.
				 *
				 * @return the IPv4 "broadcast" address
				 */
				static IPv4 broadcast() noexcept;
				
				/**
				 * Checks whether IPv4 supports the given flow type.
				 * This is true for all types except Anycast.
				 *
				 * @param flow The flow type
				 * @return whether the flow type is supported by IPv4
				 */
				static bool supports(Flow flow) noexcept;

				/**
				 * Construct the empty IPv4 address of all zero bytes.
				 * This also happens to be the "any" address.
				 */
				IPv4() noexcept;

				/**
				 * Construct the IPv4 address with the given bytes.
				 *
				 * @param bytes The bytes
				 */
				explicit IPv4(std::uint32_t bytes) noexcept;

				/**
				 * Construct an IPv4 address by parsing the given null-terminated text.
				 * If parsing fails, the "any" address will be set.
				 *
				 * @param text The text to parse
				 */
				explicit IPv4(const char *text) noexcept;

				/**
				 * Construct an IPv4 address by parsing the given text.
				 * If parsing fails, the "any" address will be set.
				 *
				 * @param text The text to parse
				 * @param length The number of bytes in the text
				 */
				IPv4(const char *text, std::size_t length) noexcept;

				/**
				 * Construct an IPv4 address by parsing the given text.
				 * If parsing fails, the "any" address will be set.
				 *
				 * @param text The text to parse
				 */
				explicit IPv4(const std::string &text) noexcept;

				/**
				 * Gets the bytes of the IP address in native host byte order.
				 *
				 * @return the bytes
				 */
				std::uint32_t get() const noexcept;

				/**
				 * Sets the bytes in the IPv4 address in native host byte order.
				 *
				 * @param bytes The bytes to set
				 */
				void set(std::uint32_t bytes) noexcept;

				/**
				 * Clears the bytes to all zero values for the IP address.
				 * This also happens to be the "any" address.
				 */
				void clear() noexcept;

				/**
				 * Sets the bytes of an IPv4 address from the values of its four constituent parts.
				 * Each of the parts must be in the range [0,255] or the validation result will indicate a failure.
				 *
				 * @param b1 The first part
				 * @param b2 The second part
				 * @param b3 The third part
				 * @param b4 The fourth part
				 * @return the validation result of the inputs
				 */
				Validation set(int b1, int b2, int b3, int b4) noexcept;

				/**
				 * Parses the given null-terminated text to get an IPv4 address.
				 *
				 * @param text The text to parse
				 * @return the validation result for the parsing
				 */
				TextParseStatus parse(const char *text) noexcept;

				/**
				 * Parses the given text to get an IPv4 address.
				 *
				 * @param text The text to parse
				 * @param len The number of bytes in the text to parse
				 * @return the validation result for the parsing
				 */
				TextParseStatus parse(const char *text, std::size_t len) noexcept;

				/**
				 * Parses the given text to get an IPv4 address.
				 *
				 * @param text The text to parse
				 * @return the validation result for the parsing
				 */
				TextParseStatus parse(const std::string &text) noexcept;

				/**
				 * Prints the IP address to a text buffer in the conventional form of four numbers  "a.b.c.d".
				 * If the buffer is not large enough, the text will be truncated but the return value will provide how many bytes would have been needed.
				 * If the buffer is larger than needed, it will be null terminated.
				 *
				 * @param text The buffer to print to
				 * @param len The number of bytes in the buffer
				 * @return the actual number of bytes needed to print the IP
				 */
				std::size_t print(char *text, std::size_t len) const noexcept;

				/**
				 * Prints the IP address to a returned text string in the conventional form of four numbers  "a.b.c.d".
				 *
				 * @return the printed IP address
				 * @throw std::bad_alloc If not enough memory could be allocated for the string
				 */
				std::string print() const;

				/**
				 * Interprets the IP address in a Boolean context.
				 * IPs are considered true if they are not all zero bytes (the "any" address).
				 *
				 * @return whether the IP was considered true
				 */
				explicit operator bool() const noexcept;

			private:
				/** The 4 bytes of the IP address in native host byte order */
				std::uint32_t mBytes;
		};

		/**
		 * Prints the IPv4 address to a C++ stream in the conventional form of four numbers "a.b.c.d".
		 *
		 * @param stream The stream to print to
		 * @param ip The IP to print
		 * @return this stream
		 */
		CIO_NET_API std::ostream &operator<<(std::ostream &stream, const IPv4 &ip);

		/**
		 * Checks to see if two IPv4 addresses are equal.
		 * This is true if they have exactly the same byte sequence.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether they are equal
		 */
		bool operator==(const IPv4 &left, const IPv4 &right);

		/**
		 * Checks to see if two IPv4 addresses are not equal.
		 * This is true if they have different byte sequences.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether they are not equal
		 */
		bool operator!=(const IPv4 &left, const IPv4 &right);

		/**
		 * Checks to see if an IPv4 address is less than another IPv4 address.
		 * This does an ordered comparison on the bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is less than the second address
		 */
		bool operator<(const IPv4 &left, const IPv4 &right);

		/**
		 * Checks to see if an IPv4 address is less than or equal to another IPv4 address.
		 * This does an ordered comparison on the bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is less than or equal to the second address
		 */
		bool operator<=(const IPv4 &left, const IPv4 &right);

		/**
		 * Checks to see if an IPv4 address is greater than another IPv4 address.
		 * This does an ordered comparison on the bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is greater than the second address
		 */
		bool operator>(const IPv4 &left, const IPv4 &right);

		/**
		 * Checks to see if an IPv4 address is greater than or equal to another IPv4 address.
		 * This does an ordered comparison on the bytes.
		 *
		 * @param left The first address
		 * @param second The second address
		 * @return whether the first address is greater than or equal to the second address
		 */
		bool operator>=(const IPv4 &left, const IPv4 &right);
	}
}

#endif
