/*==============================================================================
 * Copyright 2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_RELATIVEINPUT_H
#define CIO_RELATIVEINPUT_H

#include "Types.h"

#include <cio/Input.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The Relative Input presents a subset of another Input as a readable range using requestReadFromPosition to apply an offset
	 * without modifying the underlying input's seek position.
	 *
	 * It is primarily used as a utility class to implement archives and other resources where the underlying input is seekable
	 * and consists of multiple independent logical resources.
	 */
	class CIO_API RelativeInput : public Input
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const Metaclass &getDeclaredMetaclass() noexcept;

			/**
			* Construct a stream.
			*/
			RelativeInput() noexcept;
			
			/**
			 * Opens the given input to read from the given start offset up to the given length.
			 *
			 * @param input The input to use
			 * @param start The start offset
			 * @param length The length
			 */
			explicit RelativeInput(std::shared_ptr<Input> input, Length start = 0, Length length = CIO_UNKNOWN_LENGTH) noexcept;
			
			/**
			 * Copy constructor.
			 * The underlying input will be shared with the input.
			 * The relative start, position, and length will be copied and will
			 * update independently of the input.
			 *
			 * @param in The input to copy
			 */
			RelativeInput(const RelativeInput &in);

			/**
			 * Move constructor.
			 *
			 * @param in The input to move
			 */
			RelativeInput(RelativeInput &&in) noexcept;
			
			/**
			 * Copy assignment.
			 * The underlying input will be shared with the input.
			 * The relative start, position, and length will be copied and will
			 * update independently of the input.
			 *
			 * @param in The input to copy
			 * @return this input
			 */
			RelativeInput &operator=(const RelativeInput &in);
			
			/**
			 * Move assignment.
			 *
			 * @param in The input to move
			 * @return this input
			 */
			RelativeInput &operator=(RelativeInput &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~RelativeInput() noexcept override;

			/**
			 * Gets the metaclass for this object.
			 *
			 * @return the metaclass
			 */
			virtual const Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears this relative input.
			 * The underlying input pointer is reset, but the actual underlying input is not modified.
			 */
			virtual void clear() noexcept override;
			
			/**
			 * Checks whether the input is open.
			 * This is true if there is an underlying input and it is open.
			 *
			 * @return whether the channel is open
			 */
			virtual bool isOpen() const noexcept override;
			
			/**
			 * Gets the size of the relative input.
			 * This is the size set for the relative input range, regardless of underlying input.
			 *
			 * @return the size of the underlying input
			 */
			virtual Length size() const noexcept override;
			
			/**
			 * Gets the number of sequentially readable bytes remaining in the relative input.
			 * This is is the difference between the size and the read position.
			 *
			 * @return the number of sequentially readable bytes
			 */
			virtual Length readable() const noexcept override;
			
			/**
			 * Reads bytes sequentially from the underlying input, using its requestReadFromPosition to apply the relative offset.
			 * This will never read more bytes than are defined for the relative input.
			 *
			 * @param data The buffer to read into
			 * @param bytes The number of bytes to read
			 * @return the status and number of bytes actually read
			 */
			virtual Progress<std::size_t> requestRead(void *data, std::size_t bytes) noexcept override;
			
			/**
			 * Reads bytes at the given offset from this relative input, using the underlying input's requestReadFromPosition
			 * to apply the relative offset after all adjustements are made.
			 * This will never read more bytes than are defined for the relative input.
			 *
			 * @param data The buffer to read into
			 * @param bytes The number of bytes to read
			 * @return the status and number of bytes actually read
			 */
			virtual Progress<std::size_t> requestReadFromPosition(void *data, std::size_t bytes, Length position, Seek mode) noexcept override;

			/**
			 * Gets the current sequential read position of this relative input.
			 * This is the difference between the current absolute position and the start position.
			 *
			 * @return the current read position
			 */
			virtual Length getReadPosition() const noexcept override;

			/**
			 * Seeks this relative input to the given read position.
			 * This does not modify the read position of the underlying input.
			 *
			 * @param position The desired position
			 * @param mode The seek mode to use to apply the position
			 * @return the status and bytes processed to seek
			 */
			virtual Progress<Length> requestSeekToRead(Length position, Seek mode) noexcept override;

			/**
			 * Discards the given number of the bytes from this relative input.
			 * This will adjust the sequential position without actually reading any data from the underlying input.
			 *
			 * @param bytes The number of bytes to discard
			 * @return the status and actual bytes discarded
			 */
			virtual Progress<Length> requestDiscard(Length bytes) noexcept override;

			/**
			 * Opens the given input to read from the given start offset up to the given length.
			 *
			 * @param input The input to use
			 * @param start The start offset
			 * @param length The length
			 */
			void open(std::shared_ptr<Input> input, Length start = 0, Length length = CIO_UNKNOWN_LENGTH);

			/**
			 * Gets the underlying input used by this object.
			 *
			 * @return the underlying input
			 */
			std::shared_ptr<Input> getInput() noexcept;
			
			/**
			 * Gets the underlying input used by this object.
			 *
			 * @return the underlying input
			 */
			std::shared_ptr<const Input> getInput() const noexcept; 
			
			/**
			 * Sets the underlying input used by this object without changing any of the
			 * position parameters.
			 *
			 * @param input The input to set
			 */
			void setInput(std::shared_ptr<Input> input) noexcept;

			/**
			 * Gets the byte offset in the underlying input to consider the relative start of this input.
			 *
			 * @return the relative start byte offset
			 */
			Length start() const noexcept;
			
			/**
			 * Sets the byte offset in the underlying input to consider the relative start of this input.
			 *
			 * @param start the relative start byte offset
			 */
			void start(Length start) noexcept;
			
			/**
			 * Gets the current relative position of this input.
			 *
			 * @return the current position
			 */
			Length position() const noexcept;
			
			/**
			 * Sets the current relative position of this input.
			 *
			 * @param position the new current position
			 */
			void position(Length position) noexcept;
			
			/**
			 * Gets the current relative length of this input.
			 *
			 * @return the relative length
			 */
			Length limit() const noexcept;
			
			/**
			 * Sets the current relative length of this input.
			 *
			 * @param limit the relative length
			 */	
			void limit(Length limit) noexcept;

		private:
			/** The metaclass for this class */
			static Class<RelativeInput> sMetaclass;
			
			/** The underlying input */
			std::shared_ptr<Input> mInput;
			
			/** The absolute byte offset in the underlying input of the start position of this input */
			Length mStart;
			
			/** The total number of bytes to use from the underlying input relative to the start */
			Length mLength;
			
			/** The relative byte offset from start for sequential reads */
			Length mCurrent;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
