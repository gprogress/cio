/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Print.h"

#include <cctype>
#include <cmath>

namespace cio
{
	template <typename T>
	inline std::size_t strlenUnsigned(T value) noexcept
	{
		std::size_t count = 1;
		while (value >= 10)
		{
			++count;
			value /= 10;
		}
		return count;
	}

	template <typename T>
	inline std::size_t strlenSigned(T value) noexcept
	{
		std::size_t count = 1 + (value < 0);
		MatchingInt<T> absolute = std::abs(value);
		while (absolute >= 10)
		{
			++count;
			absolute /= 10;
		}
		return count;
	}

	std::size_t strlen(unsigned char value) noexcept
	{
		return strlenUnsigned(value);
	}

	std::size_t strlen(signed char value) noexcept
	{
		return strlenSigned(value);
	}

	std::size_t strlen(unsigned short value) noexcept
	{
		return strlenUnsigned(value);
	}

	std::size_t strlen(short value) noexcept
	{
		return strlenSigned(value);
	}

	std::size_t strlen(unsigned value) noexcept
	{
		return strlenUnsigned(value);
	}

	std::size_t strlen(int value) noexcept
	{
		return strlenSigned(value);
	}

	std::size_t strlen(unsigned long value) noexcept
	{
		return strlenUnsigned(value);
	}

	std::size_t strlen(long value) noexcept
	{
		return strlenSigned(value);
	}

	std::size_t strlen(unsigned long long value) noexcept
	{
		return strlenUnsigned(value);
	}

	std::size_t strlen(long long value) noexcept
	{
		return strlenSigned(value);
	}

	std::size_t strlen(float value) noexcept
	{
		// There isn't an easy way to determine float length except by actually printing it.
		return print(value, nullptr, 0);
	}

	std::size_t strlen(double value) noexcept
	{
		// There isn't an easy way to determine double length except by actually printing it.
		return print(value, nullptr, 0);
	}

	// Helper template printing for general case printing

	template <typename T>
	inline std::size_t printUnsigned(T value, PrintBuffer<T> &buffer) noexcept
	{
		std::size_t count = strlenUnsigned(value);
		std::size_t divisor = 10;

		buffer[count - 1] = static_cast<char>(value % 10u) + '0';

		for (std::size_t i = 1; i < count; ++i)
		{
			unsigned digit = (value / divisor) % 10u;
			buffer[count - i - 1] = static_cast<char>(digit) + '0';
			divisor *= 10;
		}

		return count;
	}

	template <typename T>
	inline std::size_t printUnsigned(T value, char *buffer, std::size_t capacity) noexcept
	{
		PrintBuffer<T> temp;
		std::size_t needed = printUnsigned(value, temp);
		reprint(temp.data(), needed, buffer, capacity);
		return needed;
	}

	template <typename T>
	inline PrintBuffer<T> printUnsigned(T value) noexcept
	{
		PrintBuffer<T> buffer;
		printUnsigned(value, buffer);
		return buffer;
	}

	template <typename T>
	inline std::size_t printSigned(T value, PrintBuffer<T> &buffer) noexcept
	{
		std::size_t count = strlen(value);
		std::size_t divisor = 10;
		std::size_t offset = (value < 0);

		if (offset)
		{
			buffer[0] = '-';
		}

		MatchingInt<T> absolute = std::abs(value);
		buffer[count - 1] = static_cast<char>(absolute % 10u) + '0';

		for (std::size_t i = 1; i < count - offset; ++i)
		{
			MatchingInt<T> digit = (absolute / divisor) % 10u;
			buffer[count - i - 1] = static_cast<char>(digit) + '0';
			divisor *= 10;
		}

		return count;
	}

	template <typename T>
	inline std::size_t printSigned(T value, char *buffer, std::size_t capacity) noexcept
	{
		PrintBuffer<T> temp;
		std::size_t needed = printSigned(value, temp);
		reprint(temp.data(), needed, buffer, capacity);
		return needed;
	}

	template <typename T>
	inline PrintBuffer<T> printSigned(T value) noexcept
	{
		PrintBuffer<T> buffer;
		printSigned(value, buffer);
		return buffer;
	}

	std::size_t print(unsigned char value, char *buffer, std::size_t capacity) noexcept
	{
		return printUnsigned(value, buffer, capacity);
	}

	std::size_t print(signed char value, char *buffer, std::size_t capacity) noexcept
	{
		return printSigned(value, buffer, capacity);
	}

	std::size_t print(unsigned short value, char *buffer, std::size_t capacity) noexcept
	{
		return printUnsigned(value, buffer, capacity);
	}

	std::size_t print(short value, char *buffer, std::size_t capacity) noexcept
	{
		return printSigned(value, buffer, capacity);
	}

	std::size_t print(unsigned value, char *buffer, std::size_t capacity) noexcept
	{
		return printUnsigned(value, buffer, capacity);
	}

	std::size_t print(int value, char *buffer, std::size_t capacity) noexcept
	{
		return printSigned(value, buffer, capacity);
	}

	std::size_t print(unsigned long value, char *buffer, std::size_t capacity) noexcept
	{
		return printUnsigned(value, buffer, capacity);
	}

	std::size_t print(long value, char *buffer, std::size_t capacity) noexcept
	{
		return printSigned(value, buffer, capacity);
	}

	std::size_t print(unsigned long long value, char *buffer, std::size_t capacity) noexcept
	{
		return printUnsigned(value, buffer, capacity);
	}

	std::size_t print(long long value, char *buffer, std::size_t capacity) noexcept
	{
		return printSigned(value, buffer, capacity);
	}
	
	// Standard C++ float printing adds unnecessary trailing zeros, with no good way to suppress it
	// This method will detect and strip them off
	std::size_t trimFloatZeros(char *buffer, std::size_t length) noexcept
	{
		std::size_t results = length;
		char *current = buffer;
		char *end = current + length;
		while (current < end && *current)
		{
			// Find decimal point
			if (*current == '.')
			{
				// Now check for exponent (e or E) and stop either there or at end of text
				char *limit = current + 1;
				while (limit < end && *limit && std::tolower(*limit) != 'e')
				{
					++limit;
				}

				// Back up one and count trailing zeros
				char *zeros = limit;
				while (zeros > current + 2 && *(zeros - 1) == '0')
				{
					--zeros;
				}

				// Move content after extra zeros up to replace trailing zeros
				if (zeros < limit)
				{
					std::memmove(zeros, limit, end - limit);
					results -= (limit - zeros);
					buffer[results] = 0;
				}

				break;
			}

			++current;
		}

		return results;
	}

	template <typename T>
	std::size_t printSpecialFloat(T value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = 0;

		if (std::isnan(value))
		{
			needed = 3;
			reprint("NaN", 3, buffer, capacity);
		}
		else if (std::isinf(value))
		{
			if (value > 0)
			{
				needed = 8;
				reprint("Infinity", 8, buffer, capacity);
			}
			else
			{
				needed = 9;
				reprint("-Infinity", 9, buffer, capacity);
			}
		}

		return needed;
	}

	std::size_t printSpecialFloat(float value, char *buffer, std::size_t capacity) noexcept
	{
		return printSpecialFloat<float>(value, buffer, capacity);
	}

	std::size_t printSpecialFloat(double value, char *buffer, std::size_t capacity) noexcept
	{
		return printSpecialFloat<double>(value, buffer, capacity);
	}

	std::size_t print(float value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = printSpecialFloat(value, buffer, capacity);
		if (needed == 0) // not a special float
		{
			PrintBuffer<float> temp;
			std::size_t printed = std::snprintf(temp.data(), temp.capacity(), "%lf", value);
			needed = trimFloatZeros(temp.data(), printed);
			reprint(temp.data(), needed, buffer, capacity);
		}

		return needed;
	}

	std::size_t print(double value, char *buffer, std::size_t capacity) noexcept
	{
		std::size_t needed = printSpecialFloat(value, buffer, capacity);
		if (needed == 0) // not a special float
		{
			PrintBuffer<double> temp;
			std::size_t printed = std::snprintf(temp.data(), temp.capacity(), "%lf", value);
			needed = trimFloatZeros(temp.data(), printed);
			reprint(temp.data(), needed, buffer, capacity);
		}

		return needed;
	}

	// Print to returned UTF-8 text methods

	PrintBuffer<unsigned char> print(unsigned char value) noexcept
	{
		return printUnsigned(value);
	}

	PrintBuffer<unsigned short> print(unsigned short value) noexcept
	{
		return printUnsigned(value);
	}

	PrintBuffer<unsigned> print(unsigned value) noexcept
	{
		return printUnsigned(value);
	}

	PrintBuffer<unsigned long> print(unsigned long value) noexcept
	{
		return printUnsigned(value);
	}

	PrintBuffer<unsigned long long> print(unsigned long long value) noexcept
	{
		return printUnsigned(value);
	}

	PrintBuffer<signed char> print(signed char value) noexcept
	{
		return printSigned(value);
	}

	PrintBuffer<short> print(short value) noexcept
	{
		return printSigned(value);
	}

	PrintBuffer<int> print(int value) noexcept
	{
		return printSigned(value);
	}

	PrintBuffer<long> print(long value) noexcept
	{
		return printSigned(value);
	}

	PrintBuffer<long long> print(long long value) noexcept
	{
		return printSigned(value);
	}

	PrintBuffer<float> print(float value) noexcept
	{
		PrintBuffer<float> buffer;
		std::size_t needed = printSpecialFloat(value, buffer.data(), buffer.capacity());
		if (needed == 0) // not a special float
		{
			std::size_t printed = std::snprintf(buffer.data(), buffer.capacity(), "%f", value);
			needed = trimFloatZeros(buffer.data(), printed);
		}
		return buffer;
	}

	PrintBuffer<double> print(double value) noexcept
	{
		PrintBuffer<double> buffer;
		std::size_t needed = printSpecialFloat(value, buffer.data(), buffer.capacity());
		if (needed == 0) // not a special float
		{
			std::size_t printed = std::snprintf(buffer.data(), buffer.capacity(), "%lf", value);
			needed = trimFloatZeros(buffer.data(), printed);
		}
		return buffer;
	}

	// Print bytes methods

	std::string printBytes(const void *bytes, std::size_t len)
	{
		std::string text;
		text.resize(len * 2);
		const std::uint8_t *input = reinterpret_cast<const std::uint8_t *>(bytes);
		char *chars = &(text[0]);

		for (std::size_t i = 0; i < len; ++i)
		{
			chars = printByte(input[i], chars);
		}

		return text;
	}

	std::size_t printBytes(const void *bytes, std::size_t len, char *text, std::size_t textlen) noexcept
	{
		const std::uint8_t *input = reinterpret_cast<const std::uint8_t *>(bytes);
		std::size_t toWrite = std::min(len, textlen / 2);
		char *chars = text;

		for (std::size_t i = 0; i < toWrite; ++i)
		{
			chars = printByte(input[i], chars);
		}

		return len * 2;
	}

	CIO_API std::string printSwappedBytes(const void *bytes, std::size_t len)
	{
		std::string text;
		text.resize(len * 2);
		const std::uint8_t *input = reinterpret_cast<const std::uint8_t *>(bytes);
		char *chars = &(text[0]);

		for (std::size_t i = len; i > 0; --i)
		{
			chars = printByte(input[i - 1], chars);
		}

		return text;
	}

	std::size_t printSwappedBytes(const void *bytes, std::size_t len, char *text, std::size_t textlen) noexcept
	{
		const std::uint8_t *input = reinterpret_cast<const std::uint8_t *>(bytes);
		std::size_t toWrite = std::min(len, textlen / 2);
		char *chars = text;

		for (std::size_t i = 0; i < toWrite; ++i)
		{
			chars = printByte(input[len - i - 1], chars);
		}

		return len * 2;
	}
}
