/*==============================================================================
 * Copyright 2020-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_CHUNK_H
#define CIO_CHUNK_H

#include "Types.h"

#include "Factory.h"
#include "Metatypes.h"
#include "Status.h"

#include <algorithm>
#include <cstring>

namespace cio
{
	/**
	* The Chunk class provides a way to manage a dynamically sized array with an optional factory allocator.
	* It can either take ownership of the array or simply reference an externally managed array.
	*
	* @tparam <T> The type of data element for the array
	*/
	template <typename T>
	class Chunk
	{
		public:
			/**
			* Construct a new chunk buffer with initial size of 0 and no allocator.
			*/
			inline Chunk() noexcept;

			/**
			 * Construct a buffer with the given size using the default Factory<T>.
			 *
			 * @param capacity The number of T elements to allocate
			 * @throw std::bad_alloc If memory allocation was needed and failed
			 */
			inline explicit Chunk(std::size_t capacity);

			/**
			 * Construct a buffer with the given size using the given allocator.
			 * If the null allocator (nullptr) is provided, the default Factory<T> is used instead.
			 *
			 * @param capacity The number of T elements to allocate
			 * @param allocator The allocator to use
			 * @throw std::bad_alloc If memory allocation was needed and failed
			 */
			inline explicit Chunk(std::size_t capacity, Factory<T> allocator);

			/**
			 * Binds an existing array with the given size to be used by this chunk.
			 * The provided allocator will be used to manage the array.
			 * 
			 * If the null allocator (nullptr) is provided,
			 * the array will merely be referenced and not actually owned or deallocated by this object.
			 *
			 * @param data The array pointer to bind
			 * @param capacity the length of the array
			 * @param allocator The allocator to use to manage the data array, or nullptr to disable resizing and management
			 */
			inline Chunk(T *data, std::size_t capacity, Factory<T> allocator = nullptr) noexcept;

			/**
			 * Construct a copy of a chunk.
			 *
			 * @param in The chunk to copy
			 * @throw std::bad_alloc If memory allocation was needed and failed
			 */
			inline Chunk(const Chunk<T> &in);

			/**
			* Move chunk underlying data into a new instance of this class.
			*
			* @param in The chunk to move
			*/
			inline Chunk(Chunk<T> &&in) noexcept;

			/**
			 * Copy a buffer.
			 *
			 * @param in The buffer to copy
			 * @return this buffer
			 */
			inline Chunk<T> &operator=(const Chunk<T> &in);

			/**
			* Move buffer underlying data into this object.
			*
			* @param in The buffer to move
			* @return this buffer
			*/
			inline Chunk<T> &operator=(Chunk<T> &&in) noexcept;

			/**
			* Destructor.
			*/
			inline ~Chunk() noexcept;

			/**
			 * Sets up this buffer to reference an existing data array.
			 * If an allocator is specified, it will be used to resize and destroy the data array as necessary.
			 * The size will be set to the given capacity.
			 *
			 * If the null allocator (nullptr) is provided,
			 * the array will merely be referenced and not actually owned or deallocated by this object.
			 * 
			 * @param data The data array bytes
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to manage the data array, or nullptr to disable resizing and management
			 */
			inline void bind(T *data, std::size_t capacity, Factory<T> allocator = nullptr) noexcept;

			/**
			 * Sets up this buffer to allocate a new data array with the given capacity.
			 * The data array will be destroyed when this Chunk is destroyed or cleared.
			 * This uses the default allocator based on C++ operator new.
			 *
			 * @param capacity The size of the data array in bytes
			 * @return the allocated data array
			 */
			inline T *allocate(std::size_t capacity);
			/**
			 * Sets up this buffer to allocate a new data array with the given capacity.
			 * The data array will be destroyed when this Chunk is destroyed or cleared.
			 * This uses the provided allocator to the job, which will then be stored on this Chunk.
			 *
			 * @param capacity The size of the data array in bytes
			 * @param allocator The allocator to use to allocate the array
			 * @return the allocated data array
			 */
			inline T *allocate(std::size_t capacity, Factory<T> allocator);

			/**
			 * Gets the allocator used by this chunk.
			 *
			 * @return the allocator
			 */
			inline Factory<T> allocator() const noexcept;

			/**
			 * Gets the raw data array pointer.
			 *
			 * @return the raw data array
			 */
			inline const T *data() const noexcept;

			/**
			* Gets the raw data array poitner.
			*
			* @return the raw data array
			*/
			inline T *data() noexcept;

			/**
			 * Creates a Chunk that references, without owning, the same data as this Chunk.
			 *
			 * @return a Chunk that references the same content as this Chunk
			 */
			inline Chunk<T> reference() noexcept;

			/**
			 * Creates a read-only Chunk that references, without owning, the same data as this Chunk.
			 *
			 * @return a read-only Chunk that references the same content as this Chunk
			 */
			inline Chunk<const T> reference() const noexcept;

			/**
			 * Creates a Chunk that references, without owning, a subset of the data as this Chunk.
			 *
			 * @param off The element offset from the array start to reference in the subset
			 * @param length The number of elements to reference in the subset
			 * @return a Chunk that references the specified subset of data of this Chunk
			 */
			inline Chunk<T> slice(std::size_t off, std::size_t length) noexcept;

			/**
			 * Creates a read-only Chunk that references, without owning, a subset of the data as this Chunk.
			 *
			 * @param off The element offset from the array start to reference in the subset
			 * @param length The number of elements to reference in the subset
			 * @return a read-only Chunk that references the specified subset of data of this Chunk
			 */
			inline Chunk<const T> slice(std::size_t off, std::size_t length) const noexcept;

			/**
			 * Sets the size to zero and the element pointer to nullptr, which will also deallocate the array if an allocator is specified.
			 */
			inline void clear() noexcept;

			/**
			 * Gets the current number of T elements in the chunk.
			 * If T = void, this is the number of bytes.
			 *
			 * @return the number of T elements in the chunk
			 */
			inline std::size_t size() const noexcept;

			/**
			 * Attempts to resize the chunk to the given number of elements and gets the actual number of elements afterward.
			 * If no allocator is specified, this action fails and the existing size of the chunk is returned.
			 * If the allocator is specified, it is called with the current data array and the requested size, and the results are then captured back into this Chunk.
			 * Note that it is possible for the allocator to either fail or to return a different size than requested due to alignment restrictions.
			 * If the requested size is the same as the current size, the chunk is not modified.
			 *
			 * @param desired the desired number of elements in the chunk
			 * @return the actual number of elements in the chunk after attempting to resize to the desired size
			 */
			inline std::size_t resize(std::size_t desired, Factory<T> reallocator = Factory<T>());

			/**
			 * Attempts to ensure that the chunk has at least the given number of elements.
			 * This is equivalent to checking the current size and then requesting a resize if the current size is less than the desired size.
			 *
			 * @param desired the desired number of elements in the chunk
			 * @param reallocator the reallocator to use if the current allocator is null
			 * @return the actual number of elements in the chunk after attempting to reserve the desired size
			 */
			inline std::size_t reserve(std::size_t desired, Factory<T> reallocator = Factory<T>());

			/**
			 * Checks to see if the Chunk is empty. This is true if the size is 0.
			 *
			 * @return whether the Chunk is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Checks to see if the Chunk is valid in a Boolean context. This is true if the size is nonzero.
			 *
			 * @return whether the Chunk is valid
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Gets the start iterator for this memory chunk.
			 * This is presented as a T pointer to the start of the array, or a byte pointer if T = void.
			 * 
			 * @return the start iterator
			 */
			inline ValuePointer<T> begin() noexcept;

			/**
			 * Gets the read-only start iterator for this memory chunk.
			 * This is presented as a T pointer to the start of the array, or a byte pointer if T = void.
			 *
			 * @return the start iterator
			 */
			inline ValuePointer<const T> begin() const noexcept;

			/**
			 * Gets the end iterator for this memory chunk.
			 * This is presented as a T pointer to the start of the array, or a byte pointer if T = void.
			 *
			 * @return the end iterator
			 */
			inline ValuePointer<T> end() noexcept;

			/**
			 * Gets the read-only end iterator for this memory chunk.
			 * This is presented as a T pointer to the start of the array, or a byte pointer if T = void.
			 *
			 * @return the end iterator
			 */
			inline ValuePointer<const T> end() const noexcept;

			/**
			 * Gets the array element at the given array index in this Chunk.
			 * This does not perform any bounds checking.
			 *
			 * @param idx The array element index
			 * @return the value at the given index
			 */
			inline Element<T *> operator[](std::size_t idx) noexcept;

			/**
			 * Gets the array element at the given array index in this Chunk.
			 * This does not perform any bounds checking.
			 *
			 * @param idx The array element index
			 * @return the value at the given index
			 */
			inline Element<const T *> operator[](std::size_t idx) const noexcept;

			/**
			 * Gets the first array element.
			 * This does not verify that the chunk is not empty.
			 * 
			 * @return the first value
			 */
			inline Element<T *> front() noexcept;

			/**
			 * Gets the first array element.
			 * This does not verify that the chunk is not empty.
			 *
			 * @return the first value
			 */
			inline Element<const T *> front() const noexcept;

			/**
			 * Gets the last array element.
			 * This does not verify that the chunk is not empty.
			 *
			 * @return the first value
			 */
			inline Element<T *> back() noexcept;

			/**
			 * Gets the last array element.
			 * This does not verify that the chunk is not empty.
			 *
			 * @return the first value
			 */
			inline Element<const T*> back() const noexcept;

			/**
			 * Gets the array element at the given array index in this Chunk.
			 * This does not perform any bounds checking.
			 *
			 * @param idx The array element index
			 * @return the value at the given index
			 */
			inline Element<T *> operator()(std::size_t idx) noexcept;

			/**
			 * Gets the array element at the given array index in this Chunk.
			 * This does not perform any bounds checking.
			 *
			 * @param idx The array element index
			 * @return the value at the given index
			 */
			inline Element<const T *> operator()(std::size_t idx) const noexcept;

		protected:
			/** The allocator, if any */
			Factory<std::remove_cv_t<T>> mAllocator = nullptr;

			/** The underlying array data pointer */
			T *mElements = nullptr;

			/** The number of data elements */
			std::size_t mSize = 0;
	};
}

namespace cio
{
	template <typename T>
	inline Chunk<T>::Chunk() noexcept :
		mAllocator(nullptr),
		mElements(nullptr),
		mSize(0)
	{
		// nothing to do
	}

	template <typename T>
	inline Chunk<T>::Chunk(std::size_t capacity) :
		mElements(capacity > 0 ? mAllocator.create(capacity) : nullptr),
		mSize(capacity)
	{
		// nothing more to do
	}

	template <typename T>
	inline Chunk<T>::Chunk(std::size_t capacity, Factory<T> allocator) :
		mAllocator(allocator ? allocator : Factory<T>()),
		mElements(capacity > 0 ? mAllocator.create(capacity) : nullptr),
		mSize(capacity)
	{
		// nothing more to do
	}

	template <typename T>
	inline Chunk<T>::Chunk(T *data, std::size_t capacity, Factory<T> allocator) noexcept :
		mAllocator(allocator),
		mElements(data),
		mSize(capacity)
	{
		// nothing more to do
	}

	template <typename T>
	inline Chunk<T>::Chunk(const Chunk<T> &in) :
		mAllocator(in.mAllocator),
		mElements(mAllocator ? mAllocator.copy(in.mElements, in.mSize) : in.mElements),
		mSize(in.mSize)
	{
		// nothing more to do
	}

	template <typename T>
	inline Chunk<T> &Chunk<T>::operator=(const Chunk<T> &in)
	{
		if (this != &in)
		{
			mElements = mAllocator.destroy(mElements, mSize);
			mSize = 0;

			mAllocator = in.mAllocator;
			mElements = mAllocator ? mAllocator.copy(in.mElements, in.mSize) : in.mElements;
			mSize = in.mSize;
		}

		return *this;
	}

	template <typename T>
	inline Chunk<T>::Chunk(Chunk<T> &&in) noexcept :
		mAllocator(in.mAllocator),
		mElements(in.mElements),
		mSize(in.mSize)
	{
		in.mAllocator = nullptr;
		in.mElements = nullptr;
		in.mSize = 0;
	}

	template <typename T>
	inline Chunk<T> &Chunk<T>::operator=(Chunk<T> &&in) noexcept
	{
		if (this != &in)
		{
			mAllocator.destroy(const_cast<std::remove_const_t<T> *>(mElements), mSize);

			mAllocator = in.mAllocator;
			mElements = in.mElements;
			mSize = in.mSize;

			in.mAllocator = nullptr;
			in.mElements = nullptr;
			in.mSize = 0;
		}

		return *this;
	}

	template <typename T>
	inline Chunk<T>::~Chunk() noexcept
	{
		mAllocator.destroy(const_cast<std::remove_const_t<T> *>(mElements), mSize);
	}

	template <typename T>
	inline void Chunk<T>::clear() noexcept
	{
		mAllocator.destroy(const_cast<std::remove_const_t<T> *>(mElements), mSize);
		mAllocator = nullptr;

		mElements = nullptr;
		mSize = 0;
	}

	template <typename T>
	inline void Chunk<T>::bind(T *data, std::size_t capacity, Factory<T> allocator) noexcept
	{
		this->clear();

		mAllocator = allocator;
		mElements = data;
		mSize = capacity;
	}

	template <typename T>
	inline Factory<T> Chunk<T>::allocator() const noexcept
	{
		return mAllocator;
	}

	template <typename T>
	inline T *Chunk<T>::allocate(std::size_t capacity)
	{
		this->clear();

		if (capacity > 0)
		{
			mAllocator.clear();
			mElements = mAllocator.create(capacity);
			mSize = capacity;
		}

		return mElements;
	}

	template <typename T>
	inline T *Chunk<T>::allocate(std::size_t capacity, Factory<T> allocator)
	{
		this->clear();
		mAllocator = allocator;
		mElements = mAllocator.create(capacity);
		mSize = capacity;
		return mElements;
	}

	template <typename T>
	inline const T *Chunk<T>::data() const noexcept
	{
		return mElements;
	}

	template <typename T>
	inline T *Chunk<T>::data() noexcept
	{
		return mElements;
	}

	template <typename T>
	inline std::size_t Chunk<T>::size() const noexcept
	{
		return mSize;
	}

	template <typename T>
	inline bool Chunk<T>::empty() const noexcept
	{
		return mSize == 0;
	}

	template <typename T>
	inline Chunk<T>::operator bool() const noexcept
	{
		return mSize > 0;
	}

	template <typename T>
	inline std::size_t Chunk<T>::resize(std::size_t desired, Factory<T> reallocator)
	{
		if (mAllocator)
		{
			mElements = mAllocator.resize(mElements, mSize, desired);
			mSize = desired;
		}
		else if (reallocator)
		{
			mAllocator = reallocator;
			mElements = mAllocator.copy(mElements, mSize, desired);
			mSize = desired;
		}
		else if (!mElements && desired > 0)
		{
			mAllocator.setDefaultAllocator();
			mElements = mAllocator.create(desired);
			mSize = desired;
		}

		return mSize;
	}

	template <typename T>
	inline std::size_t Chunk<T>::reserve(std::size_t desired, Factory<T> reallocator)
	{
		if (mAllocator && mSize < desired)
		{
			mElements = mAllocator.resize(mElements, mSize, desired);
			mSize = desired;
		}
		else if (reallocator && mSize < desired)
		{
			mAllocator = reallocator;
			mElements = mAllocator.move(mElements, mSize, desired);
			mSize = desired;
		}
		else if (!mElements && desired > 0)
		{
			mAllocator.clear();
			mElements = mAllocator.create(desired);
			mSize = desired;
		}

		return mSize;
	}

	template <typename T>
	inline Chunk<T> Chunk<T>::reference() noexcept
	{
		return Chunk<T>(mElements, mSize);
	}

	template <typename T>
	inline Chunk<const T> Chunk<T>::reference() const noexcept
	{
		return Chunk<const T>(mElements, mSize);
	}

	template <typename T>
	inline Chunk<T> Chunk<T>::slice(std::size_t off, std::size_t length) noexcept
	{
		return Chunk<T>(static_cast<ValuePointer<T>>(mElements) + off, std::min(mSize - off, length), nullptr);
	}

	template <typename T>
	inline Chunk<const T> Chunk<T>::slice(std::size_t off, std::size_t length) const noexcept
	{
		return Chunk<const T>(static_cast<ValuePointer<T>>(mElements) + off, std::min(mSize - off, length), nullptr);
	}

	template <typename T>
	inline ValuePointer<T> Chunk<T>::begin() noexcept
	{
		return static_cast<ValuePointer<T>>(mElements);
	}


	template <typename T>
	inline ValuePointer<const T> Chunk<T>::begin() const noexcept
	{
		return static_cast<ValuePointer<const T>>(mElements);
	}

	template <typename T>
	inline ValuePointer<T> Chunk<T>::end() noexcept
	{
		return static_cast<ValuePointer<T>>(mElements) + mSize;
	}


	template <typename T>
	inline ValuePointer<const T> Chunk<T>::end() const noexcept
	{
		return static_cast<ValuePointer<const T>>(mElements) + mSize;
	}

	template <typename T>
	inline Element<T *> Chunk<T>::operator[](std::size_t idx) noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[idx];
	}

	template <typename T>
	inline Element<const T *> Chunk<T>::operator[](std::size_t idx) const noexcept
	{
		return static_cast<ValuePointer<const T>>(mElements)[idx];
	}

	template <typename T>
	inline Element<T *> Chunk<T>::front() noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[0];
	}

	template <typename T>
	inline Element<const T *> Chunk<T>::front() const noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[0];
	}

	template <typename T>
	inline Element<T *> Chunk<T>::back() noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[mSize - 1];
	}

	template <typename T>
	inline Element<const T *> Chunk<T>::back() const noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[mSize - 1];
	}

	template <typename T>
	inline Element<T *> Chunk<T>::operator()(std::size_t idx) noexcept
	{
		return static_cast<ValuePointer<T>>(mElements)[idx];
	}

	template <typename T>
	inline Element<const T *> Chunk<T>::operator()(std::size_t idx) const noexcept
	{
		return static_cast<ValuePointer<const T>>(mElements)[idx];
	}
}

#endif
