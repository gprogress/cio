/*==============================================================================
 * Copyright 2021-2024, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_FIXEDTEXT_H
#define CIO_FIXEDTEXT_H

#include "Types.h"

#include "Metatypes.h"

#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>

#include <iosfwd>

namespace cio
{
	/**
	 * The FixedText template implements short bounded-length text.
	 * For overall code interoperability, fixed texts are still null-termined C-style strings so an extra byte is added
	 * above the bounded number of characters defined.
	 * 
	 * Fixed Text is an optimization, so it does not automatically convert from regular text strings. Conversely, it
	 * does automatically convert to C-style character strings so it can be used as an intermediate
	 * result given to a final string.
	 * 
	 * @tparam N The maximum number of characters that may be used in text not counting the null terminator
	 */
	template <std::size_t N>
	class FixedText
	{
		public:
			/**
			 * Construct the empty fixed text with all bytes null.
			 */
			inline FixedText() noexcept;

			/**
			 * Construct fixed text with up to N bytes from the given txt.
			 *
			 * @param text The text to copy
			 */
			inline explicit FixedText(const cio::Text &text) noexcept;

			/**
			 * Construct fixed text with up to N bytes from the given string.
			 *
			 * @param text The text to copy
			 */
			inline explicit FixedText(const std::string &text) noexcept;

			/**
			 * Construct fixed text with up to N bytes from the given string.
			 *
			 * @param text The text to copy
			 */
			inline explicit FixedText(const char *text) noexcept;

			/**
			 * Construct fixed text with up to N bytes from the given string.
			 *
			 * @param text The text to copy
			 * @param count The number of characters to copy
			 */
			inline FixedText(const char *text, std::size_t count) noexcept;

			/**
			 * Construct fixed text with up to N bytes from the given string range.
			 *
			 * @param start The start of the text range
			 * @param end The end of the text range
			 */
			inline FixedText(const char *start, const char *end) noexcept;

			/**
			 * Construct fixed text consisting of the single character provided.
			 *
			 * @param c The character to become the text
			 */
			inline explicit FixedText(char c) noexcept;

			/**
			 * Copy constructor.
			 * 
			 * @param in The text to copy
			 */
			inline FixedText(const FixedText<N> &in) noexcept = default;

			/**
			 * Construct fixed text by copying up to the lesser of N or M bytes from the given fixed text.
			 *
			 * @tparam M The length of the other fixed text
			 * @param code The fixed text to copy
			 */
			template <std::size_t M>
			inline FixedText(const FixedText<M> &code) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The text to copy
			 * @return this text
			 */
			inline FixedText<N> &operator=(const FixedText<N> &in) noexcept = default;

			/**
			 * Assign from lesser of N or M bytes from the given fixed text.
			 *
			 * @tparam M The length of the other fixed text
			 * @param code The fixed text to copy
			 */
			template <std::size_t M>
			inline FixedText<N> &operator=(const FixedText<M> &code) noexcept;

			/**
			 * Assign from text.
			 * 
			 * @param text The text to copy
			 * @return this text
			 */
			inline FixedText<N> &operator=(const Text &text) noexcept;

			/**
			 * Assign from a single character.
			 *
			 * @param in The text to cgrooveopy
			 * @return this text
			 */
			inline FixedText<N> &operator=(char in) noexcept;

			/**
			 * Assign from C-style null terminated string.
			 *
			 * @param text The text to copy
			 * @return this text
			 */
			inline FixedText<N> &operator=(const char *text) noexcept;

			/**
			 * Assign from C++ std::string.
			 *
			 * @param text The text to copy
			 * @return this text
			 */
			inline FixedText<N> &operator=(const std::string &text) noexcept;

			/**
			 * Destructor.
			 */
			~FixedText() noexcept = default;

			/**
			 * Clears the fixed text. This sets all bytes to null.
			 */
			inline void clear() noexcept;

			/**
			 * Copies up to N bytes from the given text.
			 *
			 * @param text The text to copy
			 */
			inline void set(const char *text) noexcept;
			
			/**
			 * Copies up to N bytes from the given text.
			 *
			 * @param text The text to copy
			 */
			inline void set(const std::string &text) noexcept;
			
			/**
			 * Copies up to N bytes from the given text range.
			 *
			 * @param start The start of the text range
			 * @param end The end of the text range
			 */
			inline void assign(const char *start, const char *end) noexcept;

			/**
			 * Appends the given text to the end of the current text up to the N byte limit.
			 *
			 * @param text The text to append
			 */
			inline void append(const char *text) noexcept;
			
			/**
			 * Appends the given text to the end of the current text up to the N byte limit.
			 *
			 * @param text The text to append
			 */
			inline void append(const std::string &text) noexcept;
			
			/**
			 * Copies the given text to the current text if the current text is empty.
			 *
			 * @param in The text to merge
			 * @return a reference to this text for convenience
			 */
			inline FixedText<N> &merge(const FixedText<N> &in) noexcept;

			/**
			 * Gets direct access to the underlying text characters.
			 *
			 * @return the text data
			 */
			inline char *data() noexcept;
			
			/**
			 * Gets direct access to the underlying text characters.
			 *
			 * @return the text data
			 */
			inline const char *data() const noexcept;
			
			/**
			 * Gets direct access to the underlying text characters.
			 *
			 * @return the text data
			 */
			inline const char *c_str() const noexcept;

			/**
			 * Constructs a C++ string from the text.
			 *
			 * @return the string
			 * @throw std::bad_alloc If allocating the memory failed
			 */
			inline std::string str() const;

			/**
			 * Gets the length of the text.
			 * This finds the null byte and then returns the length not including it.
			 *
			 * @return the length of the text
			 */
			inline std::size_t size() const noexcept;
			
			/**
			 * Gets the maximum length of the text.
			 * This is a compile-time constant N.
			 *
			 * @return the capacity
			 */
			static constexpr std::integral_constant<std::size_t, N> capacity() noexcept;

			/**
			 * Resizes the text. This can only truncate the text if the new length
			 * is lower than the current length. Setting a higher length does nothing.
			 *
			 * @param length The desired text length
			 */
			inline void resize(std::size_t length) noexcept;

			/**
			 * Checks whether the text is empty.
			 *
			 * @return whether the text is empty
			 */
			inline bool empty() const noexcept;

			/**
			 * Gets the i'th character of the text.
			 * 
			 * @param i The index
			 * @return The character
			 */
			inline char &operator[](std::size_t i) noexcept
			{
				return mText[i];
			}

			/**
			 * Gets the i'th character of the text.
			 *
			 * @param i The index
			 * @return The character
			 */
			inline const char &operator[](std::size_t i) const noexcept
			{
				return mText[i];
			}

			/**
			 * Finds the position index of the given character relative
			 * to the beginning of the text.
			 *
			 * @param c The character to find
			 * @return the first position of the found character,
			 * or std::string::npos if not found
			 */
			inline std::size_t find(char c) const noexcept;

			/**
			 * Gets a referenced view of the prefix of this string up to the first instance of the given character.
			 * If the character was not found, the entire string is considered the prefix.
			 *
			 * @param c The character to find
			 * @return a reference to the text portion before the found character, or the whole string if it was not found
			 */
			inline FixedText<N> splitPrefix(char c) const noexcept;

			/**
			 * Gets a referenced view of the suffix of this string after the first instance of the given character.
			 * If the character was not found, the suffix is considered empty.
			 *
			 * @param c The character to find
			 * @return a reference to the text portion before the found character, or the empty string if it was not found
			 */
			inline FixedText<N> splitSuffix(char c) const noexcept;

			/**
			 * Gets two referenced views of the string before and after the first instance of the given character.
			 * If the character is not found, the entire string is considered before and after is empty.
			 *
			 * @param c The character to find
			 * @return a reference to the text portion before the found character, or the whole string if it was not found
			 */
			inline std::pair<FixedText<N>, FixedText<N>> split(char c) const noexcept;

			/**
			 * Interprets the text in a Boolean context.
			 * It is considered true if it is not empty.
			 *
			 * @return whether the text is considered true
			 */
			inline explicit operator bool() const noexcept;

			/**
			 * Gets the number of UTF-8 characters needed to print this text.
			 * 
			 * @return the length of this text
			 */
			inline std::size_t strlen() const noexcept;

			/**
			 * Prints to a returned UTF-8 text buffer.
			 * This just returns the internal text.
			 * 
			 * @return the printed text
			 */
			inline const char *print() const noexcept;

			/**
			 * Prints to a UTF-8 text buffer.
			 * 
			 * @param buffer The buffer
			 * @param capacity The capacity of the buffer
			 * @return the actual number of characters available to print
			 */
			inline std::size_t print(char *buffer, std::size_t capacity) const noexcept;

			/**
			 * Performs byte swapping on the fixed text.
			 * Since this is UTF-8 text, this does nothing and returns itself.
			 * 
			 * @return the swapped text
			 */
			inline const FixedText<N> &swapBytes() const noexcept;

			/**
			 * Performs byte swapping on the fixed text.
			 * Since this is UTF-8 text, this does nothing.
			 *
			 * @return this text
			 */
			inline FixedText<N> &applyByteSwap() noexcept;

			/**
			 * Gets the text without any leading or trailing whitespace or unprintable characters.
			 * 
			 * @return the trimmed string
			 */
			inline FixedText<N> trim() const noexcept;

			/**
			 * Implicitly cast to a C-style string.
			 * 
			 * @return The C-style string pointer
			 */
			inline operator const char *() const noexcept;

		private:
			/** Text character bytes with trailing null */
			char mText[N + 1];
	};

	/**
	 * Checks to see if two fixed texts are equal.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are equal
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator==(const FixedText<N> &left, const FixedText<M> &right) noexcept;

	/**
	 * Checks to see if a fixed text is equal to a C-style string.
	 *
	 * @tparam <N> The capacity of the first text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are equal
	 */
	template <std::size_t N>
	inline bool operator==(const FixedText<N> &left, const char *right) noexcept;

	/**
	 * Checks to see if a fixed text is equal to a C-style string.
	 *
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are equal
	 */
	template <std::size_t M>
	inline bool operator!=(const char *left, const FixedText<M> &right) noexcept;

	/**
	 * Checks to see if two fixed texts are not equal.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are not equal
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator!=(const FixedText<N> &left, const FixedText<M> &right) noexcept;

	/**
 	 * Checks to see if a fixed text is not equal to a C-style string.
	 *
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are not equal
	 */
	template <std::size_t M>
	inline bool operator!=(const char *left, const FixedText<M> &right) noexcept;

	/**
	 * Checks to see if a fixed text is not equal to a C-style string.
	 *
	 * @tparam <N> The capacity of the first text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the two texts are not equal
	 */
	template <std::size_t N>
	inline bool operator!=(const FixedText<N> &left, const char *right) noexcept;

	/**
	 * Checks to see if one fixed text sorts before another.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the first text is before the second
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator<(const FixedText<N> &left, const FixedText<M> &right) noexcept;
	
	/**
	 * Checks to see if one fixed text sorts before or equal to another.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the first text is before or equal to the second
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator<=(const FixedText<N> &left, const FixedText<M> &right) noexcept;
	
	/**
	 * Checks to see if one fixed text sorts after another.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the first text is after the second
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator>(const FixedText<N> &left, const FixedText<M> &right) noexcept;
	
	/**
	 * Checks to see if one fixed text sorts after or equal to another.
	 *
	 * @tparam <N> The capacity of the first text
	 * @tparam <M> The capacity of the second text
	 * @param left The first text
	 * @param right The second text
	 * @return whether the first text is after or equal to the second
	 */
	template <std::size_t N, std::size_t M>
	inline bool operator>=(const FixedText<N> &left, const FixedText<M> &right) noexcept;

	/**
	 * Prints fixed text to a C++ stream.
	 *
	 * @tparam <N> The capacity of the text
	 * @param s The stream
	 * @param code The text
	 * @return the stream
	 * @throw std::exception If the stream throws an exception
	 */
	template <std::size_t N>
	inline std::ostream &operator<<(std::ostream &s, const FixedText<N> &code);
}

/* Inline implementation */

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4996) 
#endif 

// Can't include until here to avoid circular include dependencies
#include "Text.h"

namespace cio
{
	template <std::size_t N>
	inline FixedText<N>::FixedText() noexcept
	{
		std::memset(mText, 0, N + 1);
	}

	template <std::size_t N>
	template <std::size_t M>
	FixedText<N>::FixedText(const FixedText<M> &code) noexcept
	{
		std::size_t copied = std::min(N, M);
		std::memcpy(mText, code.data(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(const cio::Text &text) noexcept
	{
		std::size_t copied = std::min(N, text.size());
		std::memcpy(mText, text.data(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(const std::string &text) noexcept
	{
		std::size_t copied = std::min(N, text.size());
		std::memcpy(mText, text.c_str(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(const char *text) noexcept
	{
		std::size_t copied = 0;
		if (text)
		{
			copied = std::min(N, std::strlen(text));
			std::memcpy(mText, text, copied);
		}
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(const char *text, std::size_t count) noexcept
	{
		std::size_t copied = std::min(N, count);
		std::memcpy(mText, text, copied);
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(const char *start, const char *end) noexcept
	{
		std::size_t copied = 0;
		if (start < end)
		{
			copied = std::min<std::size_t>(N, end - start);
			std::memcpy(mText, start, copied);
		}
		std::memset(mText + copied, 0, N + 1 - copied);
	}

	template <std::size_t N>
	inline FixedText<N>::FixedText(char c) noexcept
	{
		mText[0] = c;
		std::memset(mText + 1, 0, N);
	}

	// Specialize to ignore when N = 0

	template <>
	inline FixedText<0>::FixedText(char c) noexcept
	{
		mText[0] = 0;
	}

	template <std::size_t N>
	template <std::size_t M>
	inline FixedText<N> &FixedText<N>::operator=(const FixedText<M> &code) noexcept
	{
		std::size_t copied = std::min(N, M);
		std::memcpy(mText, code.data(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
		return *this;
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::operator=(const cio::Text &text) noexcept
	{
		std::size_t copied = std::min(N, text.size());
		std::memcpy(mText, text.data(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
		return *this;
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::operator=(char in) noexcept
	{
		mText[0] = in;
		std::memset(mText + 1, 0, N);
		return *this;
	}

	template <>
	inline FixedText<0> &FixedText<0>::operator=(char in) noexcept
	{
		mText[0] = 0;
		return *this;
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::operator=(const char *text) noexcept
	{
		std::size_t copied = 0;
		if (text)
		{
			copied = std::min(N, std::strlen(text));
			std::memcpy(mText, text, copied);
		}
		std::memset(mText + copied, 0, N + 1 - copied);
		return *this;
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::operator=(const std::string &text) noexcept
	{
		std::size_t copied = std::min(N, text.size());
		std::memcpy(mText, text.c_str(), copied);
		std::memset(mText + copied, 0, N + 1 - copied);
		return *this;
	}

	template <std::size_t N>
	inline void FixedText<N>::clear() noexcept
	{
		std::memset(mText, 0, N + 1);
	}

	template <std::size_t N>
	inline void FixedText<N>::set(const char *text) noexcept
	{
		std::size_t copied = 0;
		if (text)
		{
			copied = std::min(N, std::strlen(text));
			std::memcpy(mText, text, copied);
		}
		std::memset(mText + copied, 0, N - copied);
	}

	template <std::size_t N>
	inline void FixedText<N>::set(const std::string &text) noexcept
	{
		std::size_t copied = std::min(N, text.size());
		std::memcpy(mText, text.c_str(), copied);
		std::memset(mText + copied, 0, N - copied);
	}

	template <std::size_t N>
	inline void FixedText<N>::assign(const char *start, const char *end) noexcept
	{
		std::size_t copied = 0;
		if (start < end)
		{
			copied = std::min<std::size_t>(N, end - start);
			std::memcpy(mText, start, copied);
		}
		std::memset(mText + copied, 0, N - copied);
	}

	template <std::size_t N>
	inline void FixedText<N>::append(const char *text) noexcept
	{
		if (text && *text)
		{
			std::size_t copied = 0;
			std::size_t len = this->strlen();
			if (len < N)
			{
				copied = std::min(N - len, std::strlen(text));
				std::memcpy(mText + len, text, copied);
			}
		}
	}

	template <std::size_t N>
	inline void FixedText<N>::append(const std::string &text) noexcept
	{
		std::size_t copied = 0;
		if (!text.empty())
		{
			std::size_t copied = 0;
			std::size_t len = this->strlen();
			if (len < N)
			{
				copied = std::min(N - len, text.size());
				std::memcpy(mText + len, text.c_str(), copied);
			}
		}
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::merge(const FixedText<N> &in) noexcept
	{
		if (mText[0] == 0)
		{
			std::memcpy(mText, in.mText, N);
		}

		return *this;
	}

	template <std::size_t N>
	inline char *FixedText<N>::data() noexcept
	{
		return mText;
	}

	template <std::size_t N>
	inline const char *FixedText<N>::data() const noexcept
	{
		return mText;
	}

	template <std::size_t N>
	inline const char *FixedText<N>::c_str() const noexcept
	{
		return mText;
	}

	template <std::size_t N>
	inline std::string FixedText<N>::str() const
	{
		return mText;
	}

	template <std::size_t N>
	inline std::size_t FixedText<N>::size() const noexcept
	{
		return std::strlen(mText);
	}

	template <std::size_t N>
	inline std::size_t FixedText<N>::strlen() const noexcept
	{
		return std::strlen(mText);
	}

	template <std::size_t N>
	inline const char *FixedText<N>::print() const noexcept
	{
		return *this;
	}

	template <std::size_t N>
	inline std::size_t FixedText<N>::print(char *buffer, std::size_t capacity) const noexcept
	{
		std::size_t needed = this->strlen();
		cio::reprint(mText, needed, buffer, capacity);
		return needed;
	}

	template <std::size_t N>
	inline const FixedText<N> &FixedText<N>::swapBytes() const noexcept
	{
		return *this;
	}

	template <std::size_t N>
	inline FixedText<N> &FixedText<N>::applyByteSwap() noexcept
	{
		return *this;
	}

	template <std::size_t N>
	constexpr std::integral_constant<std::size_t, N> FixedText<N>::capacity() noexcept
	{
		return std::integral_constant<std::size_t, N>();
	}
	
	template <std::size_t N>
	inline void FixedText<N>::resize(std::size_t length) noexcept
	{
		if (length < N)
		{
			std::memset(mText + length, 0, N - length);
		}
	}

	template <std::size_t N>
	inline bool FixedText<N>::empty() const noexcept
	{
		return mText[0] == 0;
	}

	template <std::size_t N>
	inline std::size_t FixedText<N>::find(char c) const noexcept
	{
		std::size_t found = std::string::npos;
		for (std::size_t i = 0; i < N && mText[i]; ++i)
		{
			if (mText[i] == c)
			{
				found = i;
				break;
			}
		}
		return found;
	}

	template <std::size_t N>
	inline FixedText<N> FixedText<N>::splitPrefix(char c) const noexcept
	{
		std::size_t found = this->find(c);
		return (found < N) ? FixedText<N>(mText, found) : *this;
	}

	template <std::size_t N>
	inline FixedText<N> FixedText<N>::splitSuffix(char c) const noexcept
	{
		std::size_t found = this->find(c);
		return (found < N) ? FixedText<N>(mText + found + 1, N - found - 1) : FixedText<N>();
	}

	template <std::size_t N>
	inline std::pair<FixedText<N>, FixedText<N>> FixedText<N>::split(char c) const noexcept
	{
		std::size_t found = this->find(c);
		return (found < N) ? std::make_pair(FixedText<N>(mText, found), FixedText<N>(mText + found + 1, N - found - 1)) :
			std::make_pair(*this, FixedText<N>());
	}

	template <std::size_t N>
	inline FixedText<N> FixedText<N>::trim() const noexcept
	{
		std::size_t start = 0;
		while (start < N && mText[start] && !std::isgraph(mText[start]))
		{
			++start;
		}

		std::size_t end = start;
		while (end < N && mText[end])
		{
			++end;
		}

		while (end > start && !std::isgraph(mText[end - 1]))
		{
			--end;
		}

		return FixedText<N>(mText + start, mText + end);
	}

	template <std::size_t N>
	inline FixedText<N>::operator const char *() const noexcept
	{
		return mText;
	}

	template <std::size_t N>
	inline FixedText<N>::operator bool() const noexcept
	{
		return mText[0] != 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator==(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) == 0;
	}

	template <std::size_t M>
	inline bool operator==(const char *left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left, right.data()) == 0;
	}

	template <std::size_t N>
	inline bool operator==(const FixedText<N> &left, const char *right) noexcept
	{
		return std::strcmp(left.data(), right) == 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator!=(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) != 0;
	}

	template <std::size_t M>
	inline bool operator!=(const char *left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left, right.data()) != 0;
	}

	template <std::size_t N>
	inline bool operator!=(const FixedText<N> &left, const char *right) noexcept
	{
		return std::strcmp(left.data(), right) != 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator<(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) < 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator<=(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) <= 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator>(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) > 0;
	}

	template <std::size_t N, std::size_t M>
	inline bool operator>=(const FixedText<N> &left, const FixedText<M> &right) noexcept
	{
		return std::strcmp(left.data(), right.data()) >= 0;
	}

	template <std::size_t N>
	inline std::ostream &operator<<(std::ostream &s, const FixedText<N> &code)
	{
		return s << code.c_str();
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
