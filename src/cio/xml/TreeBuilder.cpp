/************************************************************************
 * Copyright 2020, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "TreeBuilder.h"

#include <cctype>

namespace cio
{
	namespace xml
	{
		std::string TreeBuilder::trimWhitespace(Text text)
		{
			std::string value;
			// Skip any leading text
			auto ws = text.begin();
			auto ii = ws;

			while (ii != text.end() && std::isspace(*ii))
			{
				++ii;
			}

			// If the entire chunk isn't whitespace, process it
			if (ii != text.end())
			{
				value.push_back(*ws);

				// Skip rest of whitespace (if any) and process until we hit end of chunk
				while (ii != text.end())
				{
					// Process until we hit whitespace
					auto jj = ii;

					while (++jj != text.end() && !std::isspace(*jj))
					{
						// keep looping until we hit end or space
					}

					// insert all non-space content
					value.insert(value.end(), ii, jj);
					// If we hit a space before end of chunk, add it
					ws = jj;

					if (ws != text.end())
					{
						value.push_back(*ws);
						// and then skip all remaining spaces
						ii = jj;

						while (ii != text.end() && std::isspace(*ii))
						{
							++ii;
						}
					}
				}
			}

			return value;
		}

		TreeBuilder::TreeBuilder() :
			mRemoveWhiteSpace(true),
			mMergeText(true)
		{
			// nothing to do
		}

		TreeBuilder::~TreeBuilder() = default;

		void TreeBuilder::startDocument()
		{
			mRoot.clear();
			mRoot.setType(Type::Document);
			mStack.push_back(&mRoot);
		}

		void TreeBuilder::startElement(Text element)
		{
			Node &node = mStack.back()->addChild(Type::Element, element.str());
			mStack.push_back(&node);
		}

		void TreeBuilder::processAttribute(Text key, Text value)
		{
			Node &node = *mStack.back();
			node.addAttribute(key, value);
		}

		void TreeBuilder::processTextChunk(Text text)
		{
			std::string value;

			if (mRemoveWhiteSpace)
			{
				value = trimWhitespace(text);
			}
			else
			{
				value = text.str();
			}

			if (mMergeText && mStack.back()->getChildCount() > 0 && mStack.back()->getLastChild().isText())
			{
				mStack.back()->getLastChild().appendText(value);
			}
			else
			{
				mStack.back()->addChild(Type::Text, std::move(value));
			}
		}

		void TreeBuilder::endElement(Text element)
		{
			mStack.pop_back();
		}

		void TreeBuilder::endDocument()
		{
			mStack.clear();
		}

		Node &TreeBuilder::data()
		{
			return mRoot;
		}

		const Node &TreeBuilder::data() const
		{
			return mRoot;
		}

		Node TreeBuilder::take()
		{
			mStack.clear();
			return std::move(mRoot);
		}
	}
}
