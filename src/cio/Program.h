/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_PROGRAM_H
#define CIO_PROGRAM_H

#include "Types.h"

#include "Path.h"
#include "Pipe.h"
#include "Text.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	/**
	 * The CIO Program class implements the ability to execute a native file as a running program, incrementally
	 * provide it input, and incrementally monitor the output.
	 */
	class CIO_API Program
	{
		public:
			/** The value for indicating no native handle is set */
			static const std::uintptr_t InvalidHandle;

			/** The value for indicating no process ID is set */
			static const int InvalidId;

			/**
			 * The primary filename suffix for executables on this platform.
			 * Currently this is "exe" for Windows platforms and the empty string for others.
			 */
			static const char *FileExtension;

			/**
			 * The name of the environment variable that specifies executable search path list.
			 * Currently this is "PATH" for all platforms.
			 */
			static const char *PathVariable;

			/**
			 * The delimiter character that separates entries in an executable search path on this platform.
			 * Currently this is ';' for Windows platforms and ':' for others.
			 */
			static const char PathDelimiter;

			/**
			 * Gets the process ID of the currently running application.
			 * 
			 * @return the application process ID 
			 */
			static int getApplicationId() noexcept;

			/**
			 * Gets the list of all executable search paths as a single text string delimited by the native path
			 * separator. This is typically the native system executable search path.
			 *
			 * @return the list of all executable search paths delimited by the native path separator
			 */
			static Text getSearchPaths();

			/**
			 * Gets the list of all executable search paths as a list of paths.
			 *
			 * @return the list of all executable search paths
			 */
			static std::vector<Path> getSearchPathList();

			/**
			 * Checks whether the path matches the expected conventions for an executable file on
			 * the current platform. This verifies the filename matches the executable file extension.
			 *
			 * @param path The file path
			 * @return whether the path follows the convention for an executable
			 */
			static bool matchesPath(const Path &path) noexcept;

			/**
			 * Finds the full path to an executable with the given name.
			 * If an absolute path is provided, this method will validate that it exists.
			 * If a relative path is provided with an extension, the executable search path will be searched to find it.
			 * If a relative path is provided without an extension, the executable search path will be searched to find
			 * the file with the default executable extension for the platform.
			 * 
			 * @param name The executable name or path
			 * @return the found executable if any, or the empty path if none was found
			 */
			static Path find(const Path &name);

			/**
			 * Finds the full path to an executable with the given name.
			 * If an absolute path is provided, this method will validate that it exists.
			 * If a relative path is provided with an extension, the given directory will be searched to find it.
			 * If a relative path is provided without an extension, the given directory will be searched to find
			 * the file with the default executable extension for the platform.
			 *
			 * @param name The executable name or path
			 * @param dir The directory to search
			 * @return the found executable if any, or the empty path if none was found
			 */
			static Path find(const Path &name, const Path &dir);

			/**
			 * Finds the full path to an executable with the given name.
			 * If an absolute path is provided, this method will validate that it exists.
			 * If a relative path is provided with an extension, the executable search path will be searched to find it.
			 * If a relative path is provided without an extension, the executable search path will be searched to find
			 * the file with the default executable extension for the platform.
			 *
			 * @param name The executable name or path
			 * @param search The list of paths to search
			 * @return the found executable if any, or the empty path if none was found
			 */
			static Path find(const Path &name, const std::vector<Path> &search);

			/**
			 * Finds the full path to an executable with the given name.
			 * If an absolute path is provided, this method will validate that it exists.
			 * If a relative path is provided with an extension, the executable search path will be searched to find it.
			 * If a relative path is provided without an extension, the executable search path will be searched to find
			 * the file with the default executable extension for the platform.
			 * 
			 * @param name The executable name or path
			 * @param delimitedList The list of paths to search delimited by the native platform separator
			 * @return the found executable if any, or the empty path if none was found
			 */
			static Path find(const Path &name, Text delimitedList);

			/**
			 * Determines whether the given path represents an executable that can be found.
			 * This succeeds if the name is a relative path.
			 * 
			 * It fails with reason Exists if it is an absolute path to an excutable program that does exist on disk,
			 * with reason Missing if it is an absolute path that does not exist on disk,
			 * with reason Unexecutable if it is an absolute path to something other than an executable program on disk,
			 * and with reason Refused if it is not a local file protocol.
			 * 
			 * @return whether the name is a findable executable and if not why not
			 */
			static State findable(const Path &name);

			/**
			 * Creates and launches a program to run the given executable.
			 * 
			 * @param executable The executable path 
			 * @return the launched program
			 */
			static Program launch(cio::Path executable);

			/**
			 * Launches a program with the given path and arguments.
			 * 
			 * @tparam ...T The argument types
			 * @param executable The name or path to the executable program file
			 * @param ...args The program arguments
			 * @return the launched program
			 */
			template <typename... T>
			inline static Program launch(cio::Path executable, const T &...args)
			{
				Program program;
				program.setExecutable(std::move(executable));
				program.setWorkingDirectory();
				program.setArguments(args...);
				program.start();
				return program;
			}

			/**
			 * Construct an empty program.
			 */
			Program() noexcept;

			/**
			 * Creates a program with the given executable.
			 * It does not launch the executable.
			 * 
			 * @param exe The executable
			 */
			explicit Program(Path exe);

			/**
			 * Creates a program with the given executable and arguments.
			 * It does not launch the executable.
			 *
			 * @tparam ...A The argument types
			 * @param exe The executable
			 * @param ...args The arguments
			 */
			template <typename... A>
			inline explicit Program(Path exe, const A &...args) :
				Program(std::move(exe))
			{
				this->setArguments(args...);
			}

			/**
			 * Copy constructor.
			 * This copies the program launch configuration but does not launch a copy of a running program.
			 * 
			 * @param in The program to copy 
			 */
			Program(const Program &in);

			/**
			 * Move constructor.
			 * 
			 * @param in The program to move
			 */
			Program(Program &&in) noexcept;

			/**
			 * Copy assignment.
			 * This copies the program launch configuration but does not launch a copy of a running program.
			 * 
			 * @warning If the current program is running, this will forcibly terminate it.
			 *
			 * @param in The program to copy
			 * @return this program
			 */
			Program &operator=(const Program &in);

			/**
			 * Move assignment.
			 * This copies the program launch configuration but does not launch a copy of a running program.
			 *
			 * @warning If the current program is running, this will forcibly terminate it.
			 *
			 * @param in The program to move
			 * @return this program
			 */
			Program &operator=(Program &&in) noexcept;

			/**
			 * Destructor.
			 * 
			 * @warning If the current program is running, this will forcibly terminate it.
			 */
			~Program() noexcept;

			/**
			 * Clears the program state.
			 * 
			 * @warning If the current program is running, this will forcibly terminate it.
			 */
			void clear() noexcept;

			/**
			 * Detaches the currently running program from this object.
			 * The returned handle is the native handle for the running program, which becomes the caller's responsibility.
			 * The program launch configuration is not modified.
			 * 
			 * @return the detched program handle
			*/
			std::uintptr_t detach() noexcept;

			/**
			 * Starts the program with the current launch configuration.
			 * 
			* @throw Exception If the process could not start.
			 */
			void start();

			/**
			 * Starts the program with the given executable and arguments.
			 * The working directory will be set to the directory containing the executable.
			 * 
			 * @tparam ...T The argument types
			 * @param exe The name or path of the executable program file
			 * @param args... The program arguments
			 */
			template <typename... T>
			inline void start(Path exe, const T &...args)
			{
				this->setExecutable(std::move(exe));
				this->setWorkingDirectory();
				this->setArguments(args...);
			}

			/**
			 * Waits for the running program to exit.
			 * 
			 * @param timeout The amount of time to wait, max for infinite 
			 * @return the program completion state
			 */
			State wait(std::chrono::milliseconds timeout = std::chrono::milliseconds::max()) const;

			/**
			 * Gets the current status of the program
			 * 
			 * @return the program current status
			 */
			State status() const noexcept;

			/**
			 * Gets the program exit code if it is terminated.
			 * The value is OS-dependent if the program is still running.
			 *
			 * @return the program exit code if terminated
			 */
			int getExitCode() const;

			/**
			 * Gets the program exit code, waiting for the program to terminate first.
			 *
			 * @return the program exit code if terminated, or an incomplete state if not
			 */
			Response<int> waitForExitCode(std::chrono::milliseconds timeout = std::chrono::milliseconds::max());

			/**
			 * Forcibly terminates the running program.
			 * 
			 * @warning This typically does not allow the program to gracefully shut down or save state.
			 */
			void terminate();

			// Control program settings prior to launch

			/**
			 * Gets the executable path to launch.
			 * 
			 * @return the executable path 
			 */
			const Path &getExecutable() const noexcept;

			/**
			 * Sets the executable path to launch.
			 * If this is a relative path, it will be resolved on start().
			 *
			 * @param path The executable path
			 */
			void setExecutable(Path path);

			/**
			 * Gets the working directory to launch the program in.
			 * If empty, will use the current working directory on launch.
			 * 
			 * @return the program working directory 
			 */
			const Path &getWorkingDirectory() const noexcept;

			/**
			 * Sets the working directory to launch the program in to be the
			 * directory containing the program.
			 */
			void setWorkingDirectory();

			/**
			 * Sets the working directory to launch the program in.
			 * If empty, will use the current working directory on launch.
			 *
			 * @param path The program working directory to use
			 */
			void setWorkingDirectory(Path path);

			/**
			 * Gets the list of command line arguments to provide to the program.
			 * This does not include the implicit first argument which is the program path.
			 * 
			 * @return the list of program arguments
			*/
			const std::vector<Text> &getArguments() const noexcept;

			/**
			 * Sets the list of command line arguments to provide to the program.
			 * This should not include the implicit first argument which is the program path.
			 *
			 * @param args the list of program arguments
			*/
			void setArguments(std::vector<Text> args);

			/**
			 * Sets the list of command line arguments to provide to the program.
			 * This should not include the implicit first argument which is the program path.
			 *
			 * @param argc The number of program arguments
			 * @param argv the list of program arguments
			*/
			void setArguments(int argc, const char **argv);

			/**
			 * Adds arguments to the current argument list.
			 * This overload handles the base case where no more arguments exist.
			*/
			inline void addArguments() noexcept
			{
				// nothing more to do
			}

			/**
			 * Adds arguments to the current argument list.
			 * This overload prints the next argument to UTF-8 text, then recursively delegates the remaining arguments.
			 * 
			 * @tparam T The next argument type
			 * @tparam U The remaining argument types
			 * @param next The next argument
			 * @param remainder The remaining arguments
			*/
			template <typename T, typename... U>
			inline void addArguments(const T &next, const U &...remainder)
			{
				cio::Text text(cio::print(next));
				text.internalize();
				mArguments.emplace_back(std::move(text));
				this->addArguments(remainder...);
			}

			/**
			 * Replaces the current arguments with the given arguments.
			 * The arguments are printed to UTF-8 text as needed.
			 *
			 * @tparam U The argument types
			 * @param args The arguments
			*/
			template <typename... U>
			inline void setArguments(const U &...args)
			{
				mArguments.clear();
				this->addArguments(args...);
			}

			/**
			 * Create a command line from the current launch configuration that is suitable for running in Windows cmd.exe
			 * or providing to the Win32 CreateProcess method.
			 * 
			 * @return the Windows command line for this launch configuration 
			*/
			Text makeWindowsCommandLine() const;

			/**
			 * Create a command line from the current launch configuration that is suitable for running in a POSIX shell
			 * such as Bash.
			 * 
			 * @return the shell command line for this launch configuration
			*/
			Text makeShellCommandLine() const;

			/**
			 * Create a command line from the current launch configuration most suitable for the current native host platform.
			 * 
			 * @return the native command line for this launch configuration
			 */
			Text makeNativeCommandLine() const;

			/**
			 * Gets the native operating system handle for the currently running process.
			 * On some platforms this may be different than the process ID.
			 * 
			 * @return the native process handle for the running program
			 */
			std::uintptr_t getProcessHandle() const noexcept;

			/**
			 * Gets the process ID for the currently running process.
			 * 
			 * @return the process ID
			 */
			int getProcessId() const noexcept;

			/**
			 * Gets a readable input stream that contains the program console output.
			 * 
			 * @return The readable input containing the program's output
			 */
			Input &getProgramOutput() noexcept;

			/**
			 * Gets a writable output stream that provides the console input to the program.
			 * 
			 * @return The writable output providing the program's console input 
			 */
			Output &getProgramInput() noexcept;

			/**
			 * Interprets this program in a Boolean context.
			 * It is considered true if it has been started.
			 * 
			 * @return whether the program has been started
			 */
			explicit operator bool() const noexcept;

		private:	
			/**
			 * Checks the given directory to determine if the given relative path name is present as an executable.
			 * If the platform expects a file extension for executables and the given name does not have it, it will
			 * be added.
			 *
			 * @param name The relative path name
			 * @param directory The directory to search
			 * @return the found executable, or the empty path if none
			 */
			static Path findInDirectory(const Path &name, const Path &directory);

			/** Path to the program executable */
			Path mExecutable;

			/** Path to the program working directory */
			Path mWorkingDir;

			/** Program arguments */
			std::vector<Text> mArguments;

			/** Current process handle if running */
			std::uintptr_t mProcessHandle;

			/** Process ID */
			int mProcessId;

			/** Input provided to program */
			std::unique_ptr<Output> mProgramInput;

			/** Output received from program */
			std::unique_ptr<Input> mProgramOutput;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
