/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Service.h"

#include <cio/Buffer.h>
#include <cio/Logger.h>

#include "AttributeClient.h"
#include "Characteristic.h"
#include "TypeDictionary.h"
#include "TypeId.h"

namespace cio
{
	namespace bluetooth
	{
		Logger Service::sLogger("cio::bluetooth::Service");
	
		Service::Service() noexcept :
			Attribute(TypeId::PrimaryService)
		{
			// nothing more to do
		}
		
		Service::Service(const UniqueId &serviceType) noexcept :
			Attribute(TypeId::PrimaryService),
			mServiceType(serviceType)
		{
			// nothing more to do
		}
			
		Service::Service(std::uint16_t start, std::uint16_t end) noexcept :
			Attribute(TypeId::PrimaryService, start, end)
		{
			// nothing more to do
		}
		
		Service::Service(std::uint16_t start, std::uint16_t end, const UniqueId &serviceType) noexcept :
			Attribute(TypeId::PrimaryService, start, end),
			mServiceType(serviceType)
		{
			// nothing more to do
		}	
		
		Service::Service(std::uint16_t start, std::uint16_t end, AttributeClient *client) noexcept :
			Attribute(TypeId::PrimaryService, start, end, client)
		{
			// nothing more to do
		}
				
		Service::Service(std::uint16_t start, std::uint16_t end, const UniqueId &serviceType, AttributeClient *client) noexcept :
			Attribute(TypeId::PrimaryService, start, end, client),
			mServiceType(serviceType)
		{
			// nothing more to do
		}
		
		Service::Service(Service &&in) noexcept :
			Attribute(std::move(in)),
			mCharacteristics(std::move(in.mCharacteristics)),
			mServiceType(in.mServiceType)
		{
			// nothing more to do
		}
				
		Service::Service(const Service &in) = default;
	
		Service &Service::operator=(Service &&in) noexcept
		{
			Attribute::operator=(std::move(in));
			mCharacteristics = std::move(in.mCharacteristics);
			mServiceType = in.mServiceType;
			return *this;
		}
	
		Service &Service::operator=(const Service &in) = default;
	
		Service::~Service() noexcept = default;
		
		void Service::clear() noexcept
		{
			mCharacteristics.clear();
			mServiceType.clear();
			Attribute::clear();
		}
		
		void Service::validate()
		{
			Attribute::validate();
			this->validateCharacteristics();
		}
		
		State Service::synchronize()
		{
			State typeStatus = this->synchronizeTypeHandle();
			State contentStatus = this->synchronizeCharacteristics();
			return typeStatus;
		}

		const UniqueId &Service::getServiceType() const noexcept
		{
			return mServiceType;
		}		
		
		void Service::setServiceType(const UniqueId &id) noexcept
		{
			mServiceType = id;
		}
		
		Text Service::getServiceName() const noexcept
		{
			Text name("Unknown");
			if (mClient)
			{
				name = mClient->getTypes().name(mServiceType);
			}
			return name;
		}
		
		const UniqueId &Service::setServiceTypeByName(const Text &name)
		{
			if (!mClient)
			{
				throw Exception(Reason::Unopened, "Can't set service type by name: no BLE client");
			}
			
			mServiceType = mClient->getTypes().id(name);
			if (!mServiceType)
			{
				throw Exception(Reason::Invalid, "Can't set service type by name: no type matched the given name");
			}
			
			return mServiceType;
		}
	
		void Service::setClient(AttributeClient *client) noexcept
		{
			Attribute::setClient(client);
			
			for (Characteristic &c : mCharacteristics)
			{
				c.setClient(client);
			}
		}

		std::pair<std::uint16_t, std::size_t>  Service::readValueByType(const UniqueId &type, void *buffer, std::size_t length) const
		{
			std::pair<std::uint16_t, std::size_t> result;
			
			if (!mClient)
			{
				throw cio::Exception(Reason::Unopened, "No Bluetooth Low Energy client to read value by type");
			}
			
			auto receiver = [&](std::uint16_t handle, const void *dataRead, std::size_t lengthRead)
			{
				result.first = handle;
				result.second = lengthRead;
				
				std::size_t toCopy = std::min(length, lengthRead);
				if (toCopy > 0)
				{
					std::memcpy(buffer, dataRead, toCopy);
				}
				
				return cio::succeed(Action::Read);
			};
			
			mClient->readValuesByTypeInRange(type, receiver, mStartHandle, mEndHandle);
			
			if (result.second < length)
			{
				std::memset(static_cast<std::uint8_t *>(buffer) + result.second, 0, length - result.second);
			}

			return result;	
		}
		
		template <>
		std::string Service::readValueByType<std::string>(const UniqueId &type) const
		{
			char tmp[512];
			std::pair<std::uint16_t, std::size_t> result = this->readValueByType(type, tmp, sizeof(tmp));
			return std::string(tmp, tmp + result.second);
		}
		
		template <>
		cio::Text Service::readValueByType<cio::Text>(const UniqueId &type) const
		{
			char tmp[512];
			std::pair<std::uint16_t, std::size_t> result = this->readValueByType(type, tmp, sizeof(tmp));
			
			cio::Text text;
			text.internalize(tmp, result.second);
			return text;
		}
		
		// Characteristic management
		
		bool Service::empty() const noexcept
		{
			return mCharacteristics.empty();
		}
				
		std::size_t Service::size() const noexcept
		{
			return mCharacteristics.size();
		}
				
		Service::iterator Service::begin() noexcept
		{
			return mCharacteristics.data();
		}
				
		Service::iterator Service::end() noexcept
		{
			return mCharacteristics.data() + mCharacteristics.size();
		}
				
		Service::const_iterator Service::begin() const noexcept
		{
			return mCharacteristics.data();
		}
				
		Service::const_iterator Service::end() const noexcept
		{
			return mCharacteristics.data() + mCharacteristics.size();
		}	
		
		Characteristic &Service::operator[](std::size_t idx)
		{
			return mCharacteristics[idx];
		}
				
		const Characteristic &Service::operator[](std::size_t idx) const
		{
			return mCharacteristics[idx];
		}
		
		std::vector<Characteristic> &Service::getCharacteristics() noexcept
		{
			return mCharacteristics;
		}
				
		const std::vector<Characteristic> &Service::getCharacteristics() const noexcept
		{
			return mCharacteristics;
		}
				
		Characteristic &Service::getCharacteristic(std::uint16_t handle)
		{
			std::size_t idx = this->locateCharacteristicIndex(handle);
			
			if (idx >= mCharacteristics.size())
			{
				throw std::invalid_argument("No such characteristic in service");
			}
			
			return mCharacteristics[idx];
		}
				
		const Characteristic &Service::getCharacteristic(std::uint16_t handle) const
		{
			std::size_t idx = this->locateCharacteristicIndex(handle);
			
			if (idx >= mCharacteristics.size())
			{
				throw std::invalid_argument("No such characteristic in service");
			}
			
			return mCharacteristics[idx];
		}
				
		Characteristic &Service::getCharacteristic(const UniqueId &type)
		{
			std::size_t idx = this->locateCharacteristicIndex(type);
			
			if (idx >= mCharacteristics.size())
			{
				throw std::invalid_argument("No such characteristic in service");
			}
			
			return mCharacteristics[idx];		
		}
				
		const Characteristic &Service::getCharacteristic(const UniqueId &type) const
		{
			std::size_t idx = this->locateCharacteristicIndex(type);
			
			if (idx >= mCharacteristics.size())
			{
				throw std::invalid_argument("No such characteristic in service");
			}
			
			return mCharacteristics[idx];
		}
		
		void Service::setCharacteristics(const std::vector<Characteristic> &chars)
		{
			mCharacteristics = chars;
			this->validateCharacteristics();
			mContentDiscovered = true;		
		}
			
		void Service::setCharacteristics(std::vector<Characteristic> &&chars) noexcept
		{
			mCharacteristics = std::move(chars);
			this->validateCharacteristics();
			mContentDiscovered = true;
		}
		
		void Service::validateCharacteristics()
		{
			// Sort characteristics in ascending order by handle
			std::sort(mCharacteristics.begin(), mCharacteristics.end());

			// Remove characteristics outside of valid range, set client, and set end handles
			this->validateCharacteristicBoundaries();
			
			// Vaildate surviving characteristics
			for (Characteristic &c : mCharacteristics)
			{
				c.validate();
			}
		}
		
		void Service::validateCharacteristicBoundaries()
		{
			if (!mCharacteristics.empty())
			{
				auto cur = mCharacteristics.begin();
				while (cur != mCharacteristics.end() && cur->getHandle() < mStartHandle)
				{
					++cur;
				}
				
				// Erase any characteristics before the start of the service handle range
				cur = mCharacteristics.erase(mCharacteristics.begin(), cur);
				
				if (!mCharacteristics.empty())
				{
					auto prev = cur++;
					
					// Set each characteristic end handle to the next characteristic handle minus 1
					while (cur != mCharacteristics.end() && cur->getHandle() <= mEndHandle)
					{
						prev->setEndHandle(cur->getHandle() - 1);
						prev->setClient(mClient);
						prev = cur++;
					}
					
					// Set final characteristic to service end handle
					prev->setEndHandle(mEndHandle);
					prev->setClient(mClient);
					
					// Erase any characteristics after the end of the service handle range
					mCharacteristics.erase(cur, mCharacteristics.end());
				}
			}
		}
		
		void Service::clearCharacteristics() noexcept
		{
			mCharacteristics.clear();
			mContentDiscovered = false;
		}	

		std::vector<Characteristic> Service::discoverCharacteristics() const
		{
			std::vector<Characteristic> chars;
			
			if (mClient)
			{
				chars = mClient->discoverCharacteristics(mStartHandle, mEndHandle);
				
				for (std::size_t i = 1; i < chars.size(); ++i)
				{
					chars[i - 1].setEndHandle(chars[i].getHandle() - 1);
				}
				
				if (!chars.empty())
				{
					chars.back().setEndHandle(mEndHandle);
				}
			}
			
			return chars;
		}
		
		State Service::synchronizeCharacteristics()
		{
			State status;
			
			if (!mContentDiscovered)
			{
				if (mClient)
				{
					this->synchronizeTypeHandle();
				
					mCharacteristics = this->discoverCharacteristics();
					
					mContentDiscovered = true;
					status.succeed();
				}
				else
				{
					status.fail(Reason::Unopened);
				}
			}
			else
			{
				status.succeed(Reason::Exists);
			}
			
			return status;
		}

		std::size_t Service::locateCharacteristicIndex(std::uint16_t handle) const noexcept
		{
			std::size_t idx = mCharacteristics.size();
			
			if (handle > mStartHandle && handle <= mEndHandle)
			{
				// Binary search of char ranges
				std::size_t low = 0;
				std::size_t high = mCharacteristics.size();
				std::size_t current = (low + high) / 2;
				
				while (current < high)
				{
					const Characteristic &candidate = mCharacteristics[current];
					std::uint16_t start = candidate.getHandle();
					std::uint16_t end = candidate.getEndHandle();
					
					if (handle >= start && handle <= end)
					{
						idx = current;
						break;
					}
					else if (start < handle)
					{
						low = current + 1;
					}
					else
					{
						high = current;
					}
					
					current = (low + high) / 2;
				}
			}
			
			return idx;
		}
				
		std::size_t Service::locateCharacteristicIndex(const UniqueId &type) const noexcept
		{
			std::size_t i = 0;
			
			while (i < mCharacteristics.size() && mCharacteristics[i].getValueType() != type)
			{
				++i;
			}
			
			return i;
		}
		
		std::pair<std::size_t, std::size_t> Service::locateCharacteristicRange(std::uint16_t start, std::uint16_t end) const noexcept
		{
			std::pair<std::size_t, std::size_t> range(0, 0);
		
			std::size_t low = 0;
			std::size_t high = mCharacteristics.size();
			std::size_t current = (low + high) / 2;
			
			while (current < high)
			{
				const Characteristic &c = mCharacteristics[current];
				std::uint16_t cstart = c.getHandle();
				std::uint16_t cend = c.getEndHandle();
				
				if (cstart <= start)
				{
					// Characteristic contains start handle, end lower bound search here
					if (start <= cend)
					{
						break;
					}
					
					low = current + 1;
				}
				else
				{
					high = current;
				}
				
				current = (low + high) / 2;
			}
			
			range.first = current;
			
			low = high;
			high = mCharacteristics.size();
			current = (low + high) / 2;
			
			while (current < high)
			{
				const Characteristic &c = mCharacteristics[current];
				std::uint16_t cstart = c.getHandle();
				std::uint16_t cend = c.getEndHandle();
				
				if (cstart <= end)
				{
					// Service contains end handle, end upper bound search here and adjust current up 1
					if (end <= cend)
					{
						++current;
						break;
					}
					
					low = current + 1;
				}
				else
				{
					high = current;
				}
				
				current = (low + high) / 2;
			}
			
			range.second = current;
			
			return range;
		}
	}
}

