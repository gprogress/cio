/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_BLUETOOTH_L2CAPATTRIBUTECLIENT_H
#define CIO_BLUETOOTH_L2CAPATTRIBUTECLIENT_H

#include "Types.h"

#include "AttributeClient.h"

#include "L2CAPChannel.h"

#include <cio/Buffer.h>

#include <chrono>
#include <functional>
#include <memory>
#include <mutex>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace cio
{
	namespace bluetooth
	{
		/**
		 * The L2CAP Attribute Client provides a full GATT implementation using direct L2CAP network communications and manually encoding and decoding
		 * the requests and responses.
		 *
		 * @warning This class works in principle but unresolved security and pairing issues mean that it typically can only be used with public GAP queries.
		 */
		class CIO_BLUETOOTH_API L2CAPAttributeClient : public AttributeClient
		{
			public:						
				/**
				 * Gets the metaclass for the cio::bluetooth::L2CAPAttributeClient class.
				 *
				 * @return the metaclass for this class
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;
				
				/**
				 * Manually opens the L2CAP socket to connect the given local adapter to the given peripheral.
				 * This is primarily a support method for implementing other methods.
				 * This creates an L2CAP socket with the default ATT channel ID and a PSM of 0.
				 *
				 * @param adapter The local adapter to use
				 * @param peripheral The peripheral to connect to\
				 * @return the established L2CAP socket
				 * @throw cio::Exception If the connection could not be established
				 */
				static L2CAPChannel createTransportLink(const Device &adapter, const Peripheral &peripheral);

				/**
				 * Construct an unopened Bluetooth Low Energy Client.
				 */
				L2CAPAttributeClient() noexcept;
				
				/**
				 * Constructs a Bluetooth Low Energy Client by adopting the state of a given client.
				 * 
				 * @param in The client to move
				 */
				L2CAPAttributeClient(L2CAPAttributeClient &&in) noexcept;
				
				/**
				 * Constructs a Bluetooth Low Energy Client by adopting the given preconfigured L2CAP channel.
				 * 
				 * @param in The L2CAP channel to adopt
				 */
				L2CAPAttributeClient(L2CAPChannel &&in) noexcept;
				
				/**
				 * Moves a Bluetooth Low Energy Client into this connection.
				 * The current state is cleared first.
				 *
				 * @param in The connection to move
				 * @return this connection
				 */
				L2CAPAttributeClient &operator=(L2CAPAttributeClient &&in) noexcept;
				
				/**
				 * Destructor. Clears all device state.
				 */
				virtual ~L2CAPAttributeClient() noexcept override;
				
				/**
				 * Gets the metaclass for this class.
				 *
				 * @return this object's runtime metaclass
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;
				
				/**
				 * Clears the connection state and disconnects from any connected remote device.
				 */				
				virtual void clear() noexcept override;
				
				// Connection management
				
				/**
				 * Connects to the given peripheral using the first available local adapter.
				 * This opens an L2CAP socket to the peripheral using the default ATT channel ID.
				 *
				 * @note The LowEnergyConnection subclass overrides this method to also establishing a pairing connection first.
				 *
				 * @param peripheral The address of the peripheral
				 * @param type The address type of the peripheral
				 */
				virtual void connect(const Peripheral &peripheral) override;
					
				/**
				 * Establishes a low energy pairing connection to the given peripheral using the first available local adapter.
				 * This will also open an L2CAP connection to the peripheral using the default ATT channel ID.
				 *
				 * @note The LowEnergyConnection subclass overrides this method to also establishing a pairing connection first.
				 *
				 * @param adapter The local adapter to use
				 * @param peripheral The peripheral Peripheral to connect to
				 */								
				virtual void connect(const Device &adapter, const Peripheral &peripheral) override;
				
				virtual bool isOpen() const noexcept override;
				
				/**
				 * Disconnects from the current peripheral.
				 * This closes the L2CAP socket.
				 */
				virtual void disconnect() noexcept override;
				
				// Transport link management
	
				/**
				 * Gets the L2CAP channel configured as the underlying transport link.
				 * 
				 * @return the transport link
				 */
				L2CAPChannel &getTransportLink() noexcept;
				
				/**
				 * Gets the L2CAP channel configured as the underlying transport link.
				 * 
				 * @return the transport link
				 */
				const L2CAPChannel &getTransportLink() const noexcept;
				
				/**
				 * Adopts the given preconfigured transport link.
				 * This bypasses the connection step to allow this client to be used directly with the socket.
				 * The receive timeout is set to the client's response timeout and the transmit and receive buffers
				 * are allocated with the default max packet size.
				 *
				 * @param in The L2CAP channel
				 * @throw std::bad_alloc If the receive and transmit buffers could not be allocated
				 */
				void adoptTransportLink(L2CAPChannel &&in);
				
				/**
				 * Manually opens the L2CAP socket to connect the given local adapter to the given peripheral.
				 * This is primarily a support method for implementing other methods.
				 * This creates an L2CAP socket with the default ATT channel ID and a PSM of 0.
				 * The receive timeout is set to the client's response timeout and the transmit and receive buffers
				 * are allocated with the default max packet size.
				 *
				 * @param adapter The local adapter to use
				 * @param peripheral The peripheral to connect to
				 * @return the connected L2CAP channel
				 * @throw cio::Exception If connection failed
				 * @throw std::bad_alloc If the receive and transmit buffers could not be allocated
				 */
				L2CAPChannel &openTransportLink(const Device &adapter, const Peripheral &peripheral);

				// High-level discovery API - most users will want to use this rather than lower-level functionality
				
				// Packet size negotiation
								
				/**
				 * Gets the most recently requested client max packet size.
				 * If no requests have been made, this is the default BLE MTU which is currently 23 bytes.
				 * 
				 * @return the client's requested max packet size
				 */
				std::size_t getClientMaxPacketSize() const noexcept;
				
				/**
				 * Gets the most recently received server max packet size.
				 * If no responses have been received, this is the default BLE MTU which is currently 23 bytes.
				 * 
				 * @return the server's requested max packet size
				 */
				std::size_t getServerMaxPacketSize() const noexcept;
				
				/**
				 * Performs an exchange of max packet size limits (MTU) with the remote device.
				 * The smaller of the provided value and the server's response value will be used for both the transmit
				 * and receive buffers.
				 *
				 * @param requested The client's requested max packet size
				 * @return the negotiated max packet size
				 */
				virtual std::size_t negotiateMaxPacketSize(std::size_t requested) override;

				// Attribute discovery
				
				/**
				 * Gets an attribute's type given its handle.
				 * This uses a GATT Find Information request.
				 *
				 * @param handle The attribute handle
				 * @return the attribute type
				 */
				virtual UniqueId getAttributeType(std::uint16_t handle) override;
				
				/**
				 * Gets the list of every attribute in a handle range on the remote device.
				 * It's generally more useful to work with the hierarchy of services, characteristics, and descriptors,
				 * but this method will get you a flat list of attributes.
				 *
				 * This uses a sequence of GATT Find Information requests.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of attributes on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Attribute> discoverAttributes(std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Finds the handle of the first attribute with the given type within the given handle range.
				 *
				 * @param type The type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the first attribute handle with the given type
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual std::uint16_t findAttributeHandleInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end) override;				
				
				// Service discovery 
								
				/**
				 * Gets a service given its handle.
				 *
				 * @param handle The service handle
				 * @return the service
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Service getService(std::uint16_t handle) override;

				/**
				 * Discovers all primary services on the remote device.
				 * This issues a sequence of read value by group type requests to process all possible handle ranges.
				 * 
				 * @return the list of all primary services
				 */
				virtual std::vector<Service> discoverPrimaryServices(std::uint16_t start, std::uint16_t end) override;

				/**
				 * Finds a primary service given its service type in a particular handle range.
				 *
				 * @param id The service type (value UUID of the service)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first service with the given definition
				 */
				virtual Service findPrimaryServiceInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end) override;
				
				// Characteristic discovery - high level API without using services
							
				/**
				 * Gets a characteristic given its handle.
				 *
				 * @param handle The characteristic handle
				 * @return the characteristic
				 * @throw cio::Exception If a transport or packet error prevented getting the service or it did not exist
				 */
				virtual Characteristic getCharacteristic(std::uint16_t handle) override;
				
				/**
				 * Discovers characteristics on the remote device within a specified handle range.
				 * It's generally more useful to start with services and find associated characteristics, but if you need a particular range in one pass, this will do it.
				 * This issues a sequence of read value by type requests to process the given handle range.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of characteristics in the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the characteristics
				 */		
				virtual std::vector<Characteristic> discoverCharacteristics(std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Finds a characteristic given its value type in a particular handle range.
				 * This is not natively supported by GATT, but is performed by a sequence of Read Value By Type queries then matching the UUID.
				 *
				 * @param id The value type (value UUID of the characteristic)
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider				 
				 * @return the first characteristic with the given value type
				 */
				virtual Characteristic findCharacteristicInRange(const UniqueId &id, std::uint16_t start, std::uint16_t end) override;
				
				// Descriptor discovery - high level API
								
				/**
				 * Gets a descriptor given its handle.
				 * This is implemented using a GATT Find Information requests.
				 *
				 * @param handle The attribute handle
				 * @return the attribute
				 * @throw cio::Exception If a transport or packet error prevented reading the attribute or it did not exist
				 */
				virtual Descriptor getDescriptor(std::uint16_t handle) override;
				
				/**
				 * Gets the list of every descriptor in a handle range on the remote device.
				 * It's generally more useful to start with services and characteristics, but if you need the full list in one pass, this will do it.
				 * This is performed with a sequence of GATT Find Information requests, filtering out group types.
				 *
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @return the list of descriptors on the remote device within the handle range (which may be empty if there are none)
				 * @throw cio::Exception If a transport or packet error prevented discovering the attributes
				 */				
				virtual std::vector<Descriptor> discoverDescriptors(std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Finds the first descriptor with the given type within the given handle range.
				 *
				 * @return the attribute with the given type
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 * @throw cio::Exception If no BLE connection exists or if the underlying protocol queries failed
				 */
				virtual Descriptor findDescriptorInRange(const UniqueId &type, std::uint16_t start, std::uint16_t end) override;

				/**
				 * Requests to read the given attribute given its handle.
				 * This uses a GATT Read Request regardless of attribute type and will always give the native ATT value.
				 *
				 * @param handle The attribute handle
				 * @param listener The listener to receive the read data
				 * @param offset The offset of the remote attribute value to start reading at
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> readValue(std::uint16_t handle, const AttributeCallback &listener, std::size_t offset) override;
				
				/**
				 * Requests to read one or more values in an attribute range whose attribute matches the given type.
				 *
				 * @param type The type
				 * @param listener The listener to receive each read value
				 * @param start The lowest handle ID to consider
				 * @param end The highest handle ID to consider
				 */
				virtual Progress<std::size_t> readValuesByTypeInRange(const UniqueId &type, const AttributeCallback &listener, std::uint16_t start, std::uint16_t end) override;
				
				/**
				 * Requests to write the given attribute with confirmation given its handle.
				 * This uses a GATT Write Request.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @param offset The offset of the remote attribute value to start write at
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> writeValue(std::uint16_t handle, const void *buffer, std::size_t requested, std::size_t offset) override;
				
				/**
				 * Requests to write the given attribute without confirmation given its handle.
				 * This uses a GATT Write Command.
				 *
				 * @param handle The attribute handle
				 * @param buffer The buffer to write the bytes from
				 * @param requested The requested number of bytes to write
				 * @return the actual number of bytes Progress and the status
				 */
				virtual Progress<std::size_t> sendValue(std::uint16_t handle, const void *buffer, std::size_t requested) override;
				
				/**
				 * Attempts to subscribe to receive updates for the given characteristic's value.
				 * Each received update will be given to the provided listener.
				 *
				 * If the handle is a characteristic value handle, and if the characteristic supports notify/indicate mode, and if
				 * this is the first subscription to that attribute, then this will write an update to the characteristic's client configuration descriptor
				 * to enable notify or indicate. If both notify and indicate are supported, notify is preferred.
				 *
				 * If the handle is not a characteristic value or does not support indicate/notify mode, then it is set up to use
				 * read polling instead.
				 *
				 * The returned Subscription pointer is stable and will not be deleted until unsubscribe is called.
				 * Users should use it as a key if they want to manage when this particular subscription is unsubscribed.
				 * 
				 * @param handle The handle of the characteristic to subscribe to
				 * @param listener The listener to receive subscriptions
				 * @param interval The polling interval to use, or 0 to receive all updates
				 * @return the status of this operation
				 */
				virtual const Subscription *subscribe(std::uint16_t handle, AttributeCallback &&listener, std::chrono::milliseconds interval) override;
				
				/**
				 * Runs a loop to receive incoming subscription updates based on the given executor.
				 * The executor specifies the maximum amount of time to run and allows for asynchronous cancelation, and the client should report
				 * completion status (either success or errors) ot the executor without throwing exceptions.
				 *
				 * This class will acquire a lock on the L2CAP channel and then process incoming packets to decode notify and update packets.
				 * It will also do a polling loop on any attributes that require read polling.
				 *
				 * @param exec The executor to manage timeout and cancelation and report results to
				 * @return the final status of the execution, Success if it ran to the full timeout, Abandoned if canceled, or a failure status if
				 * an actual error prevented any subscription processing
				 */
				virtual State executeSubscriptions(Task &exec) noexcept override;
				
				/**
				 * Unsubscribes the subscription of the given characteristic.
				 * If this was the last subscription for this handle, and it was a characteristic value handle that supported notify or indicate mode,
				 * then this will write an update to the characteristic's client configuration descriptor
				 * to disable notify and indicate modes.
				 *
				 * @param c The characteristic of interest
				 * @param modes which types of subscription modes to unsubscribe
				 * @return the status of this operation
				 */
				virtual State unsubscribe(const Subscription *subscription) override;
				
				/**
				 * Unsubscribes all subscriptions for the given attribute handle.
				 * This also writes to the characteric's client configuration descriptoer to disable notify/indicate mode where applicable.
				 *
				 * @warning This invalidates all Subscription pointers for this handle.
				 *
				 * @param handle The attribute handle
				 * @return the number of subscriptions removed and status
				 */
				virtual Progress<std::size_t> clearSubscriptions(std::uint16_t handle) override;
				
				/**
				 * Unsubscribes all subscriptions for all attributes.
				 * This also writes to the characteric's client configuration descriptoer to disable notify/indicate mode where applicable.
				 *
				 * @warning This invalidates all Subscription pointers.
				 *
				 * @return the number of subscriptions removed and status
				 */
				virtual Progress<std::size_t> clearAllSubscriptions() override;
				
				// Buffer management - used to implement requests and responses
				
				/**
				 * Locks the Low Energy Connection for exclusive use by the calling thread.
				 * If you are using multiple threads to communicate over the same connection, each thread
				 * will need to hold this lock for the entire duration of transmitting the request and (if applicable)
				 * receiving the response.
				 *
				 * This can be skipped if only one thread will ever use the connection.
				 *
				 * @warning Do NOT get this lock from a notify or indicate handler since they may be called while
				 * the lock is already held and it will deadlock if so.
				 *
				 * @return a lock protecting the low energy connection from multi-threaded access
				 */
				std::unique_lock<std::mutex> lock();
				
				/**
				 * Gets the maximum time to wait on incoming packets for a response to a request.
				 * The default is 100ms if not otherwise set.
				 *
				 * @return the maximum response time
				 */
				std::chrono::milliseconds getResponseTimeout() const noexcept;
					
				/**
				 * Sets the maximum time to wait on incoming packets for a response to a request.
				 *
				 * @param timeout the maximum response time
				 */
				void setResponseTimeout(std::chrono::milliseconds timeout);
				
				/**
				 * Gets the maximum number of retry attempts to make a request without receiving a timely response
				 * before giving up. The default is 5 if not otherwise set.
				 *
				 * @return the maximum request retry count
				 */
				std::size_t getRequestRetryCount() const noexcept;
				
				/**
				 * Gets the maximum number of retry attempts to make a request without receiving a timely response
				 * before giving up. This can be set to 0 to disable retries to only make one attempt.
				 *
				 * @param count the maximum request retry count
				 */
				void setRequestRetryCount(std::size_t count) noexcept;
				
				/**
				 * Requests a transmit buffer from the connection.
				 * This should generally be done to reuse the same transmit buffer for efficiency.
				 * The buffer will be set up to start at position 0 and have a limit of the negotiated max packet size.
				 *
				 * @return the connection's transmit buffer
				 */
				Buffer &requestTransmitBuffer();
				
				/** 
				 * Transmits the given request and returns a buffer containing the response to the request.
				 * This will send the request, then wait up to the current response timeout to receive a response.
				 * If no response is received in time, it will retry the request up to request retry count.
				 * If all retries are exhausted, an exception is thrown to indicate failure.
				 *
				 * If any notify or indicate packets are received, it will reset the timeout, not affect the retry count,
				 * and will be forwarded to the subscriptions map to be Progress by subscription listeners.
				 * 
				 * Most users that are working with request and response packets will want to use this method.
				 * 
				 * @param request The packet with the encoded request
				 * @return the buffer with the encoded response, or empty if no response is received
				 * @throw cio::Exception If no response was received, a BLE ATT error is received, or an unexpected response is received
				 */
				Buffer &requestResponse(Buffer &request);
				
				/** 
				 * Transmits the given request and captures the resopnse into the given buffer.
				 * This will send the request, then wait up to the current response timeout to receive a response.
				 * If no response is received in time, it will retry the request up to request retry count.
				 * If all retries are exhausted, an exception is thrown to indicate failure.
				 *
				 * If any notify or indicate packets are received, it will reset the timeout, not affect the retry count,
				 * and will be forwarded to the subscriptions map to be Progress by subscription listeners.
				 * 
				 * @param request The packet with the encoded request
				 * @param response The buffer to store the encoded response packet
				 * @return the provided response buffer
				 * @throw cio::Exception If no response was received, a BLE ATT error is received, or an unexpected response is received
				 */
				Buffer &requestResponse(Buffer &request, Buffer &response);
								
				/**
				 * Transmits the given request, command, or response packet over the link layer to the remote device.
				 * The request packet will be considered to be the portion of the buffer from the current position to the limit.
				 * You will typically need to flip the buffer after encoding the packet to make that work.
				 *
				 * Most users should only use this for command packets where no response is expected.
				 * 
				 * @param packet The packet to transmit
				 */
				void transmitPacket(Buffer &packet);
				
				/**
				 * Requests a receiver buffer from the connection.
				 * This should generally be done to reuse the same receive buffer for efficiency.
				 * The buffer will be set up to start at position 0 and have a limit of the negotiated max packet size.
				 *
				 * @return the connection's transmit buffer
				 */
				Buffer &requestReceiveBuffer();
				
				/**
				 * Receives the next response from the server in the connection's provided receive buffer.
				 * This method will wait at most the response timeout to recieve the response.
				 * The buffer will be configured such that the current position to the limit contain the current ATT packet.
				 * Since this method is not aware of which request generated it, it will not retry if no response is received.
				 *
				 * If any notify or indicate packets are received, it will reset the timeout, not affect the retry count,
				 * and will be forwarded to the subscriptions map to be Progress by subscription listeners.
				 *
				 * @param the receive buffer with the response packet
				 */
				Buffer &receiveResponse();
				
				/**
				 * Receives the next response from the server in your provided buffer.
				 * This method will wait at most the response timeout to recieve the response.
				 * The buffer will be configured such that the current position to the limit contain the current ATT packet.
				 * Since this method is not aware of which request generated it, it will not retry if no response is received.
				 *
				 * If any notify or indicate packets are received, it will reset the timeout, not affect the retry count,
				 * and will be forwarded to the subscriptions map to be Progress by subscription listeners.
				 *
				 * @param packet the packet to receive into
				 * @return the provided packet, returned for convenience
				 */
				Buffer &receiveResponse(Buffer &packet);
							
				/**
				 * Receives the next packet from the server in the connection's provided receive buffer.
				 * This method will wait at most the response timeout to recieve any incoming packet.				 
				 * This is the same as receiveResponse, except it will not filter out notify and indicate packets.
				 *
				 * @param packet the packet to recieve into
				 * @return the provided packet, returned for convenience
				 */
				Buffer &receiveRawPacket();
				
				/**
				 * Receives the next packet from the server in your provided buffer.
				 * This method will wait at most the response timeout to recieve any incoming packet.	
				 * This is the same as receiveResponse, except it will not filter out notify and indicate packets.
				 *
				 * @param packet the packet to recieve into
				 * @return the provided packet, returned for convenience
				 */
				Buffer &receiveRawPacket(Buffer &packet);
				
				/**
				 * Processes the received packet if it is a notify or indicate packet.
				 *
				 * If the packet is an indicate packet and the subscriptions map returns normally (without throwing an exception)
				 * then an indicate confirmation response is sent back to the server.
				 *
				 * @param packet The packet to process
				 * @return true if the packet was a notify or indicate packet, false if it was any other packet (including an error packet)
				 * @throw cio::Exception If the transport link or the notify listener threw an exception
				 */
				bool processNotification(Buffer &packet);
							
				/**
				 * Gets the packet codec currently in use.
				 *
				 * @return the packet codec
				 */
				AttributeCodec &getCodec() noexcept;
				
				/**
				 * Gets the packet codec currently in use.
				 *
				 * @return the packet codec
				 */
				const AttributeCodec &getCodec() const noexcept;
				
				/**
				 * Sets the packet codec currently in use.
				 *
				 * @param codec The packet codec
				 */
				void setCodec(AttributeCodec &codec) noexcept;

			private:
				/** The metaclass for this class */
				static Class<L2CAPAttributeClient> sMetaclass;

				/** Default attribute codec for this class */
				static AttributeCodec sDefaultCodec;
				
				/** Mutex to guard multi-threaded access to this class */
				std::mutex mMutex;

				/** L2CAP network transport link */
				L2CAPChannel mTransportLink;
				
				/** Most recently requested client max packet size */
				std::size_t mClientPacketSize;
				
				/** Most recently received server max packet size */
				std::size_t mServerPacketSize;
				
				/** Maximum timeout to wait for a response to a transmitted request */
				std::chrono::milliseconds mResponseTimeout;
				
				/** How many times to retry requests that did not receive a response */
				std::size_t mRequestRetryCount;
				
				/** Allocated transmit buffer for use by this class and users */
				Buffer mTransmitBuffer;
			
				/** Allocated receiver buffer for use by this class and users */
				Buffer mReceiveBuffer;
				
				/** Mutex guarding subscription members */
				std::mutex mSubscriberMutex;
				
				/** Codec for this class */
				AttributeCodec *mCodec;

		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

