/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_MARKUP_H
#define CIO_MARKUP_H

#include "Types.h"

#include <iosfwd>

#if defined _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace cio
{
	/**
	 * The CIO Markup enumeration describes the core type of markup elements supported by the generic markup framework.
	 */
	enum class Markup : std::uint8_t
	{
		/** No markup element type */
		None = 0,

		/** Entire document as a whole */
		Document,

		/** Hierarchical element within a document that may contain a value */
		Element,

		/** Key-value attribute pair within an element */
		Attribute,

		/** Documentation comment */
		Comment,

		/** Markup processing instruction for a particular encoding */
		Instruction,

		/** Type value such as text, Boolean, integer, or real being written for any type of parent element */
		Value,

		/** An array of values, either primitive or complex */
		Array
	};

	/**
	 * Gets the markup type label.
	 * 
	 * @param type The markup type
	 * @return the label for the markup type
	 */
	const char *print(Markup type) noexcept;

	/**
	 * Prints the markup type label.
	 *
	 * @param type The markup type
	 * @param buffer The buffer to print to
	 * @param capacity The number of characters available in the buffer
	 * @return the number of bytes needed to print the markup type
	 */
	std::size_t print(Markup type, char *buffer, std::size_t capacity) noexcept;

	/**
	 * Prints the markup type label to a C++ stream.
	 * 
	 * @param stream The stream to print to
	 * @param type The markup type
	 * @return the stream after printing
	 * @throw std::exception If the stream threw an exception
	 */
	std::ostream &operator<<(std::ostream &stream, Markup type);
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
