/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "StreamServer.h"

#include "Resolver.h"

#include <cio/Class.h>

#if defined CIO_USE_UNIX_SOCKETS
#if defined CIO_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#include <sys/types.h>
#endif

#if defined CIO_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#endif

#if defined CIO_USE_WINSOCK2

#if defined CIO_HAVE_WINSOCK2_H
#include <WinSock2.h>
#endif

#if defined CIO_HAVE_WS2StreamIP_H
#include <WS2tcpip.h>
#endif
#endif

namespace cio
{
	namespace net
	{
		namespace
		{
			Class<StreamServer> sMetaclass("cio::net::StreamServer");
		}

		const Metaclass &StreamServer::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}

		StreamServer::StreamServer() noexcept
		{
			// nothing more to do
		}

		StreamServer::StreamServer(Socket &&socket) noexcept :
			mSocket(std::move(socket))
		{
			// nothing more to do
		}

		StreamServer::StreamServer(StreamServer &&in) noexcept :
			mSocket(std::move(in.mSocket))
		{
			// nothing more to do
		}

		StreamServer &StreamServer::operator=(StreamServer &&in) noexcept
		{
			mSocket = std::move(in.mSocket);
			return *this;
		}

		StreamServer::~StreamServer() noexcept
		{
			// nothing more to do
		}

		const Metaclass &StreamServer::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void StreamServer::clear() noexcept
		{
			mSocket.clear();
		}

		StreamChannel StreamServer::waitForConnection()
		{
			return StreamChannel(mSocket.accept());
		}

		State StreamServer::listenToAllInterfaces(std::int16_t port)
		{
			return this->listenToInterface(IPv4(), port);
		}

		State StreamServer::listenToInterface(const cio::Path &iface)
		{
			Resolver resolver;
			Service address = resolver.lookupService(iface);
			return this->listenToInterface(address);
		}

		State StreamServer::listenToInterface(const Service &service)
		{
			this->clear();
			mSocket.allocate(service);
			mSocket.bind(service.getAddress());
			return mSocket.listen();
		}

		State StreamServer::listenToInterface(const Address &address)
		{
			this->clear();
			mSocket.allocate(address.getAddressFamily(), ChannelType::Stream);
			mSocket.bind(address);
			return mSocket.listen();
		}

		State StreamServer::listenToInterface(const IPv4 &ip, std::int16_t port)
		{
			this->clear();
			mSocket.allocate(AddressFamily::ipv4(), ChannelType::Stream);
			mSocket.bind(Address(ip, port));
			return mSocket.listen();
		}

		State StreamServer::listenToInterface(const IPv6 &ip, std::int16_t port)
		{
			mSocket.allocate(AddressFamily::ipv6(), ChannelType::Stream);
			mSocket.bind(Address(ip, port));
			return mSocket.listen();
		}
	}
}


