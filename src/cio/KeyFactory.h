/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#ifndef CIO_KEYFACTORY_H
#define CIO_KEYFACTORY_H

#include "Types.h"

#include "Factory.h"

#include <map>

namespace cio
{
	/**
	 * The Key Factory provides a map-based factory for allocating objects with different types or allocators
	 * based on some identifier key.
	 * 
	 * @tparam K The key type, which must support operator<
	 * @tparam V The formally returned value type, which must be a base class of all allocated types or void
	 * @tparam F The factory type if not using the default Factory<V>
	 */
	template <typename K, typename V, typename F = Factory<V>>
	class KeyFactory
	{
		public:
			/** The iterator type for this factory */
			using iterator = typename::std::map<K, F>::const_iterator;

			/** The const iterator type for this factory */
			using const_iterator = typename::std::map<K, F>::const_iterator;

			/**
			 * Construct the empty key factory with no default factory.
			 */
			KeyFactory() :
				mDefaultFactory(nullptr)
			{
				// nothing more to do
			}

			/**
			 * Copy constructor.
			 * 
			 * @param in The key factory to copy
			 */
			KeyFactory(const KeyFactory &in) = default;

			/**
			 * Move constructor.
			 * 
			 * @param in The key factory to move
			 */
			KeyFactory(KeyFactory<K, V, F> &&in) noexcept :
				mDefaultFactory(std::move(in.mDefaultFactory)),
				mFactories(std::move(in.mFactories))
			{
				// nothing more to do
			}

			/**
			 * Copy assignment.
			 * 
			 * @param in The key factory to copy
			 * @return this key factory
			 */
			KeyFactory<K, V, F> &operator=(const KeyFactory<K, V, F> &in) = default;
			
			/**
			 * Move assignment.
			 * 
			 * @param in The key factory to move
			 * @return this key factory
			 */
			KeyFactory<K, V, F> &operator=(KeyFactory<K, V, F> &&in) noexcept
			{
				mDefaultFactory = std::move(in.mDefaultFactory);
				mFactories = std::move(in.mFactories);
				return *this;
			}

			/**
			 * Destructor.
			 */
			~KeyFactory() noexcept = default;

			/**
			 * Clears the key factory.
			 */
			void clear() noexcept
			{
				mDefaultFactory = nullptr;
				mFactories.clear();
			}

			/**
			 * Gets the factory assigned to the given key, or the default factory is none matched.
			 * 
			 * @param key The key to find
			 * @return the assigned factory or the default factory
			 */
			const F &operator[](const K &key) const noexcept
			{
				auto ii = mFactories.find(key);
				return ii != mFactories.end() ? ii->second : mDefaultFactory;
			}

			/**
			 * Sets the factory for the given key if it does not already exist.
			 * 
			 * @param key The key
			 * @param factory The factory to use
			 */
			void add(K key, F factory)
			{
				mFactories.insert(std::make_pair(std::move(key), std::move(factory)));
			}

			/**
			 * Sets the factory for the given key to the default factory for R if it does not already exist.
			 * 
			 * @tparam R The type to use for the factory
			 * @param key The key
			 */
			template <typename R>
			void add(K key)
			{
				F factory(&getDeclaredMetaclass<R>());
				mFactories.insert(std::make_pair(std::move(key), std::move(factory)));
			}

			/**
			 * Gets the default factory.
			 * 
			 * @return the default
			 */
			const F &getDefault() const noexcept
			{
				return mDefaultFactory;
			}

			/**
			 * Sets the default factory.
			 * 
			 * @param factory The default factory
			 */
			void setDefault(F factory)
			{
				mDefaultFactory = std::move(factory);
			}

			/**
			 * Disables the default factory.
			 */
			void clearDefault() noexcept
			{
				mDefaultFactory = nullptr;
			}

			/**
			 * Creates a new object using the factory for the given key, or the default factory if none matches.
			 * If no factory is assigned and there is no default factory, or if the null factory is explicity assigned to the key,
			 * the returned pointer will be null.
			 * 
			 * @param key The key
			 * @return the new object allocated for the key
			 */
			std::unique_ptr<V> create(const K &key) const
			{
				auto ii = mFactories.find(key);
				return std::unique_ptr<V>(ii != mFactories.end() ? ii->second.create() : mDefaultFactory.create());
			}

			/**
			 * Finds the factory associated with the given key if one exists.
			 * This will not consider the default factory as a valid option and will return null if no key-specific factory is found.
			 * If the key-specific factory is found (pointer is non-null) but the factory does not support allocation, then that particular
			 * key has been disabled for this factory.
			 * 
			 * @param key The key of interest
			 * @return the factory associated with the key, or null if none exists
			 */
			const F *find(const K &key) const noexcept
			{
				auto ii = mFactories.find(key);
				return ii != mFactories.end() ? &(ii->second) : nullptr;
			}

			/**
			 * Gets the start iterator to key factories.
			 *
			 * @return the start iterator
			 */
			inline typename KeyFactory<K, V, F>::const_iterator begin() const noexcept
			{
				return mFactories.begin();
			}

			/**
			 * Gets the end iterator to key factories.
			 * 
			 * @return the end iterator
			 */
			inline typename KeyFactory<K, V, F>::const_iterator end() const noexcept
			{
				return mFactories.end();
			}

			/**
			 * Gets the number of keys registered with factories.
			 * 
			 * @return the number of registered keys
			 */
			inline std::size_t size() const noexcept
			{
				return mFactories.size();
			}

			/**
			 * Gets whether no keys are registered with factories.
			 *
			 * @return whether no keys are registered
			 */
			inline bool empty() const noexcept
			{
				return mFactories.empty();
			}

			/**
			 * Interprets the key factory in a Boolean context.
			 * This should be true if either the default factory is valid or a key factory is registered.
			 *
			 * @return whether the key factory is usable
			 */
			inline explicit operator bool() const noexcept
			{
				return mDefaultFactory || !mFactories.empty();
			}

		private:
			/** The default factory to use if no key is matched */
			F mDefaultFactory;

			/** The map of keys to factories */
			std::map<K, F> mFactories;
	};
}

#endif
