/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "Device.h"

#include "AddressType.h"

#include <cio/Class.h>
#include <cio/Parse.h>
#include <cio/Print.h>

#include <ostream>

#define CIO_BLUETOOTH_INVALID_DEVICE static_cast<std::uintptr_t>(-1)

namespace cio
{
	namespace bluetooth
	{	
		Class<Device> Device::sMetaclass("cio::bluetooth::Device");
	
		const Metaclass &Device::getDeclaredMetaclass() noexcept
		{
			return sMetaclass;
		}
	
		std::uintptr_t Device::getInvalidIndex() noexcept
		{
			return CIO_BLUETOOTH_INVALID_DEVICE;
		}	
		
		Device::Device() noexcept :
			mDevice(CIO_BLUETOOTH_INVALID_DEVICE),
			mAddressType(AddressType::None)
		{
			// nothing more to do
		}
		Device::Device(std::uintptr_t index) noexcept :
			mDevice(index),
			mAddressType(index != CIO_BLUETOOTH_INVALID_DEVICE ? AddressType::Public : AddressType::None)
		{
			// nothing more to do
		}
		
		Device::Device(std::uintptr_t index, AddressType type) noexcept :
			mDevice(index),
			mAddressType(type)
		{
			// nothing more to do
		}
		
		Device::Device(const Text &text) noexcept :
			mDevice(CIO_BLUETOOTH_INVALID_DEVICE),
			mAddressType(AddressType::None)
		{
			this->parse(text);		
		}
		
		Device::Device(const Device &in) = default;
			
		Device::Device(Device &&in) noexcept :
			mDevice(in.mDevice),
			mAddressType(in.mAddressType)
		{
			in.mDevice = CIO_BLUETOOTH_INVALID_DEVICE;
			in.mAddressType = AddressType::None;
		}
		
		Device &Device::operator=(const Device &in) = default;
		
		Device &Device::operator=(Device &&in) noexcept
		{
			if (&in != this)
			{
				this->clear();
				
				mDevice = in.mDevice;
				in.mDevice = CIO_BLUETOOTH_INVALID_DEVICE;
				
				mAddressType = in.mAddressType;
				in.mAddressType = AddressType::None;			
			}
			
			return *this;
		}
			
		Device::~Device() noexcept = default;
		
		const Metaclass &Device::getMetaclass() const noexcept
		{
			return sMetaclass;
		}
		
		void Device::clear() noexcept
		{
			mDevice = CIO_BLUETOOTH_INVALID_DEVICE;
			mAddressType = AddressType::None;
		}
					
		void Device::setIndex(std::uintptr_t index)
		{
			this->clear();
			
			if (index != CIO_BLUETOOTH_INVALID_DEVICE)
			{
				mDevice = index;
				mAddressType = AddressType::Public;
			}
		}
		
		std::uintptr_t Device::getIndex() const noexcept
		{
			return mDevice;
		}
								
		AddressType Device::getAddressType() const noexcept
		{
			return mAddressType;
		}
				
		void Device::setAddressType(AddressType value) noexcept
		{
			mAddressType = value;
		}
				
		bool Device::isLowEnergy() const noexcept
		{
			return mAddressType == AddressType::Public || mAddressType == AddressType::Random;
		}
		
		std::size_t Device::print(char *buffer, std::size_t length) const noexcept
		{
			char *target = buffer;
			char tmp[16];
			std::size_t actual = 0;
			
			if (length < 10)
			{
				target = tmp;
			}
			
			if (mDevice == CIO_BLUETOOTH_INVALID_DEVICE)
			{
				std::memcpy(target, "None", 4);
				actual = 4;
			}
			else
			{
				std::memcpy(target, "HCI #", 5);
				actual = 5 + cio::print(mDevice, target + 5, 5);
			}
			
			if (buffer != target)
			{
				if (length > 0)
				{
					std::memcpy(buffer, tmp, std::min(length, actual));
				}
			}
			
			if (actual < length)
			{
				std::memset(buffer + actual, 0, length - actual);
			}
			
			return actual;
		}

		TextParseStatus Device::parse(const Text &text) noexcept
		{
			TextParseStatus parse;
			
			this->clear();
			
			if (!text)
			{
				parse.status = Validation::Missing;
			}
			else
			{
				std::size_t length = text.strlen();

				if (length >= 4 && text.equalInsensitive("None"))
				{
					if (length == 4)
					{
						parse.status = Validation::Exact;
					}
					else
					{
						parse.status = Validation::TooLong;
					}
					
					parse.parsed = length;
					parse.terminator = text[4];
				}
				else if (length > 5 && cio::equalTextInsensitive(text.data(), "HCI #", 5))
				{
					parse.parsed += 5;
					
					TextParse<std::uintptr_t> indexParse = cio::parseUnsigned(text.suffix(5));
					
					parse.reason = indexParse.reason;
					parse.status = indexParse.status;
					parse.terminator = indexParse.terminator;
					parse.parsed += indexParse.parsed;
					
					if (parse.exact())
					{
						mDevice = indexParse.value;
					}
				}
				else
				{
					parse.status = Validation::InvalidValue;
					parse.terminator = text[0];
				}
			}
			
			return parse;
		}			
		
		Device::operator bool() const noexcept
		{
			return mDevice != CIO_BLUETOOTH_INVALID_DEVICE;
		}
		
		bool operator==(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() == right.getIndex();
		}
		
		bool operator!=(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() != right.getIndex();
		}
		
		bool operator<(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() < right.getIndex();
		}
		
		bool operator<=(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() <= right.getIndex();		
		}
		
		bool operator>(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() > right.getIndex();		
		}
		
		bool operator>=(const Device &left, const Device &right) noexcept
		{
			return left.getIndex() >= right.getIndex();		
		}
		
		std::ostream &operator<<(std::ostream &stream, const Device &dev)
		{
			char buffer[16];
			std::size_t out = dev.print(buffer, 16);
			stream.write(buffer, out);
			return stream;
		}
	}
}
