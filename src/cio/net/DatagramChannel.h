/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#ifndef CIO_NET_DATAGRAMCHANNEL_H
#define CIO_NET_DATAGRAMCHANNEL_H

#include "Types.h"

#include "Channel.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace cio
{
	namespace net
	{
		/**
		 * The Datagram Channel is a refinement of the general CIO Network Channel interface to explicitly support datagram-based send and receive
		 * operations such as those for UDP/IP.
		 */
		class CIO_NET_API DatagramChannel : public Channel
		{
			public:
				/**
				 * Gets the declared metaclass for this class.
				 *
				 * @return the declared metaclass
				 */
				static const Metaclass &getDeclaredMetaclass() noexcept;

				/**
				 * Constructs an unconnected datagram channel.
				 */
				DatagramChannel() noexcept;

				/**
				 * Constructs a datagram channel and adopts the given socket to use with it.
				 *
				 * @param socket The socket to adopt
				 */
				DatagramChannel(Socket &&socket) noexcept;

				/**
				 * Constructs a datagram channel by transferring the state of an existing one.
				 *
				 * @param in The datagram channel to move
				 */
				DatagramChannel(DatagramChannel &&in) noexcept;

				/**
				 * Moves a datagram channel.
				 *
				 * @param in The datagram channel to move
				 * @return this datagram channel
				 */
				DatagramChannel &operator=(DatagramChannel &&in) noexcept;

				/**
				 * Destructor. Closes the underlying socket.
				 */
				virtual ~DatagramChannel() noexcept override;

				/**
				 * Gets the metaclass for this object.
				 *
				 * @return the metaclass for the object
				 */
				virtual const Metaclass &getMetaclass() const noexcept override;

				/**
				 * Clears the datagram channel and closes the underlying socket.
				 */
				virtual void clear() noexcept override;

				/**
				 * Opens this Datagram Channel to work with the given resource location.
				 * The resource format is protocol://interface:port@recipicent:port where
				 * <ul>
				 * <li>protocol is the protocol, which may be omitted if the port is specified and is the default port for the protocol</li>
				 * <li>interface:port is the hostname, IPv4, or IPv6 address of the interface to send through with port</li>
				 * <li>recipient:port is the hostname, IPv4, or IPv6 address of the recipient with port</li>
				 * </ul>
				 *
				 * The Resolver is used to translate the URI into Service descriptions that can actually be used to connect.
				 * The port may be omitted if it is the default port for the protocol.
				 *
				 * Note that either or both interface and recipient may be omitted to set up a Datagram Channel that can send or receive anywhere,
				 * connected through the first working interface.
				 *
				 * Datagram sockets by nature are full duplex read/write and are always newly created, so the returned ModeSet will always indicate that.
				 *
				 * @param path The resource path to connect to
				 * @param modes The desired modes
				 * @return the actual modes
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				virtual cio::ModeSet openWithFactory(const cio::Path &path, cio::ModeSet modes, cio::ProtocolFactory *factory) override;

				/**
				 * Sets up this Datagram Channel to send datagrams through any working UDP/IP interface.
				 *
				 * Once this step is done, datagrams may be sent to any address reachable through the automatically chosen interface.
				 *
				 * @return the State of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State connectThroughAnyInterface();

				/**
				 * Sets up this Datagram Channel to send datagrams through the specified interface.
				 * This will always allocate a socket of ChannelType::Datagram.
				 * Once this step is done, datagrams may be sent to any address reachable through the automatically chosen interface.
				 *
				 * @param address The address of the local interface to send through
				 * @return the State of this operation\
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State connectThroughInterface(const Address &address);

				/**
				 * Sets up this Datagram Channel to send datagrams through the specified interface described by a Service.
				 * The service is used to determine the channel type which may not necessarily be ChannelType::Datagram as well as the address and protocol.
				 * Once this step is done, datagrams may be sent to any address reachable through the automatically chosen interface.
				 *
				 * @param service The service describing the local interface to send through
				 * @return the State of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State connectThroughInterface(const Service &service);

				/**
				 * Connects this Datagram Channel to a remote address described by the given service.
				 *
				 * This sets up the Datagram Channel to send to the provided address by default (including by the generic Channel::store method)
				 * and to only receive datagrams from the given address.
				 *
				 * If the datagram channel was not already set up to connect through an interface, it is configured to use the interface that is best for the given Service.
				 *
				 * @param service The service describing the remote address
				 * @return the State of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State connectToRecipient(const Service &service);

				/**
				 * Connects this Datagram Channel to the given remote address.
				 *
				 * This sets up the Datagram Channel to send to the provided address by default (including by the generic Channel::store method)
				 * and to only receive datagrams from the given address.
				 *
				 * If the datagram channel was not already set up to connect through an interface, it is configured to use the interface that can reach the given address
				 * using ChannelType::Datagram.
				 *
				 * @param address The remote address
				 * @return the State of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State connectToRecipient(const Address &address);

				/**
				 * Binds this datagram channel to listen on all local IP addresses and interfaces on the given port for incoming data.
				 *
				 * @param port the port to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State listenToAllInterfaces(std::uint16_t port);

				/**
				 * Binds this datagram channel to listen to the interface most appropriate for receiving data for the given Service.
				 *
				 * @param service The service describing the local interface
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State listenToInterface(const Service &service);

				/**
				 * Binds this datagram channel to listen on the given local address and port.
				 *
				 * Typically this should set either an IPv4 or IPv6 address and port.
				 *
				 * @param address The address to listen to
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State listenToInterface(const Address &address);

				/**
				 * Binds this datagram channel to listen on the given local IPv4 address and port.
				 *
				 * @param ip The IP address
				 * @param port The port
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State listenToInterface(const IPv4 &ip, std::uint16_t port);

				/**
				 * Binds this datagram channel to listen on the given local IPv6 address and port.
				 *
				 * @param ip The IP address
				 * @param port The port
				 * @return the result of this operation
				 * @throw cio::Exception If any part of the socket set up failed
				 */
				State listenToInterface(const IPv6 &ip, std::uint16_t port);
				
			private:
				/** The metaclass for this class */
				static Class<DatagramChannel> sMetaclass;
		};
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif

