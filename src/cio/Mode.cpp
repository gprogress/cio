/*==============================================================================
 * Copyright 2020-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Mode.h"

#include "Case.h"
#include "Text.h"

#include <istream>
#include <ostream>

namespace cio
{
	namespace
	{
		const char *sModeText[] =
		{
			"Read",
			"Write",
			"Execute",
			"Load",
			"Create",
			"Replace",
			"Temporary",
			"Unbuffered",
			"Synchronous",
			"Nonblocking",
			"Invalid"
		};
	}

	const char *print(Mode mode)
	{
		return sModeText[static_cast<unsigned>(mode)];
	}

	Mode parseMode(const char *text)
	{
		Mode mode = Mode::Invalid;

		for (unsigned i = 0; i < sizeof(sModeText) / sizeof(const char *); ++i)
		{
			if (CIO_STRCASECMP(sModeText[i], text) == 0)
			{
				mode = static_cast<Mode>(i);
				break;
			}
		}

		return mode;
	}

	Mode parseMode(const std::string &text)
	{
		return parseMode(text.c_str());
	}

	std::ostream &operator<<(std::ostream &stream, Mode mode)
	{
		return (stream << print(mode));
	}

	std::istream &operator>>(std::istream &stream, Mode &mode)
	{
		std::string chunk;
		stream >> chunk;
		mode = parseMode(chunk);
		return stream;
	}
}
