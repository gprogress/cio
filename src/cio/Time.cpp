/*==============================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Time.h"

#include "ParseStatus.h"

#include <cio/Buffer.h>
#include <cio/Print.h>
#include <cio/Text.h>

#include <ostream>

namespace cio
{
	std::tm Time::getDate() const noexcept
	{
		std::time_t t = std::chrono::system_clock::to_time_t(this->value);
		std::tm date = { };
#if defined _WIN32
		gmtime_s(&date, &t);
#else
		gmtime_r(&t, &date);
#endif
		return date;
	}

	bool Time::setDate(std::tm date) noexcept
	{
#if defined _WIN32
		std::time_t t = _mkgmtime(&date);
#else
		std::time_t t = timegm(&date);
#endif
		bool valid = t >= 0;
		if (valid)
		{
			this->value = std::chrono::system_clock::from_time_t(t);
		}
		else
		{
			this->clear();
		}
		return valid;
	}

	bool Time::setDate(int year, unsigned month, unsigned day) noexcept
	{
		std::tm date = { };
		date.tm_year = year - 1900; // year is relative to 1900
		date.tm_mon = month - 1; // tm month is 0-based
		date.tm_mday = day;
		return this->setDate(date);
	}

	bool Time::setDate(int year, unsigned month, unsigned day, unsigned hours, unsigned minutes, unsigned seconds) noexcept
	{
		std::tm date = { };
		date.tm_year = year - 1900; // year is relative to 1900
		date.tm_mon = month - 1; // tm month is 0-based
		date.tm_mday = day;
		date.tm_hour = hours;
		date.tm_min = minutes;
		date.tm_sec = seconds;
		return this->setDate(date);
	}

	std::time_t Time::getPosixTime() const noexcept
	{
		return std::chrono::system_clock::to_time_t(this->value);
	}

	void Time::setPosixTime(std::time_t incoming) noexcept
	{
		this->value = std::chrono::system_clock::from_time_t(incoming);
	}

	std::size_t Time::strlen() const noexcept
	{
		return 19;
	}

	std::size_t Time::print(char *buffer, std::size_t length) const noexcept
	{
		cio::Buffer stream(buffer, length);

		std::tm date = this->getDate();
		int year = 1900 + date.tm_year;
		int month = date.tm_mon + 1;
		int day = date.tm_mday;

		stream << year << '-';

		if (month < 10)
		{
			stream << '0';
		}
		stream << month << '-';

		if (day < 10)
		{
			stream << '0';
		}
		stream << day << ' ';

		if (date.tm_hour < 10)
		{
			stream << '0';
		}
		stream << date.tm_hour << ':';

		if (date.tm_min < 10)
		{
			stream << '0';
		}
		stream << date.tm_min << ':';

		if (date.tm_sec < 10)
		{
			stream << '0';
		}
		stream << date.tm_sec;
		stream.put(char(0));

		return 19;
	}

	FixedText<19> Time::print() const noexcept
	{
		FixedText<19> text;
		this->print(text.data(), 20);
		return text;
	}

	std::ostream &operator<<(std::ostream &stream, const Time &time)
	{
		FixedText<19> text;
		time.print(text.data(), 20);
		return stream << text;
	}
}
