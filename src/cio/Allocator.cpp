/*==============================================================================
 * Copyright 2023, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Allocator.h"

#include "Malloc.h"
#include "New.h"

namespace cio
{
	Allocator *Allocator::getDefault() noexcept
	{
		return sGlobalDefault.load();
	}

	Allocator *Allocator::setDefault(Allocator *allocator) noexcept
	{
		return sGlobalDefault.exchange(allocator);
	}

	Allocator::~Allocator() noexcept = default;

	void Allocator::clear() noexcept
	{
		// nothing to do
	}

	void *Allocator::allocate(std::size_t bytes)
	{
		return nullptr;
	}

	std::nullptr_t Allocator::deallocate(void *data, std::size_t bytes) noexcept
	{
		return nullptr;
	}

	void *Allocator::reallocate(void *data, std::size_t current, std::size_t desired)
	{
		return nullptr;
	}

	void *Allocator::create(const Metaclass *type, std::size_t count)
	{
		return nullptr;
	}

	void *Allocator::duplicate(const void *data, std::size_t count)
	{
		return nullptr;
	}

	void *Allocator::duplicate(const void *data, std::size_t copied, std::size_t allocated)
	{
		return nullptr;
	}

	std::nullptr_t Allocator::destroy(const Metaclass *type, void *data, std::size_t count) noexcept
	{
		return nullptr;
	}

	void *Allocator::copy(const Metaclass *type, const void *data, std::size_t count)
	{
		return nullptr;
	}

	void *Allocator::copy(const Metaclass *type, const void *data, std::size_t copied, std::size_t allocated)
	{
		return nullptr;
	}

	void *Allocator::move(const Metaclass *type, void *data, std::size_t moved, std::size_t allocated)
	{
		return nullptr;
	}

	void *Allocator::resize(const Metaclass *type, void *data, std::size_t current, std::size_t desired)
	{
		return nullptr;
	}
}