/*==============================================================================
 * Copyright 2021-2022, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
==============================================================================*/
#include "Service.h"

#include "Flow.h"
#include "Resolver.h"

#include <ostream>

namespace cio
{
	namespace net
	{	
		Service::Service() noexcept :
			mPort(0),
			mChannelType(ChannelType::None),
			mFlow(Flow::None)
		{
			// nothing more to do
		}
		
		Service::Service(const ProtocolFamily &pf, std::uint16_t port, Flow flow) noexcept :
			mAddress(IPv4::any(), port),
			mProtocolFamily(pf),
			mPort(port),
			mChannelType(pf.getChannelType()),
			mFlow(flow)
		{
			// nothing more to do
		}
				
		Service::Service(const IPv4 &ip, const ProtocolFamily &pf, std::uint16_t port, Flow flow) noexcept :
			mAddress(ip, port),
			mProtocolFamily(pf),
			mPort(port),
			mChannelType(pf.getChannelType()),
			mFlow(flow)
		{
			// nothing more to do
		}
		
		Service::Service(const IPv6 &ip, const ProtocolFamily &pf, std::uint16_t port, Flow flow) noexcept :
			mAddress(ip, port),
			mProtocolFamily(pf),
			mPort(port),
			mChannelType(pf.getChannelType()),
			mFlow(flow)
		{
			// nothing more to do
		}

		Service::Service(Address address, const ProtocolFamily &pf, Flow flow) noexcept :
			mAddress(std::move(address)),
			mProtocolFamily(pf),
			mPort(mAddress.getPort()),
			mChannelType(pf.getChannelType()),
			mFlow(flow)
		{
			// nothing more to do
		}

		Service::Service(const Service &in) = default;
		
		Service::Service(Service &&in) noexcept :
			mHost(std::move(in.mHost)),
			mAddress(std::move(in.mAddress)),
			mProtocolFamily(in.mProtocolFamily),
			mPort(in.mPort),
			mChannelType(in.mChannelType),
			mFlow(in.mFlow)
		{
			// nothing more to do
		}

		Service &Service::operator=(const Service &in) = default;

		Service &Service::operator=(Service &&in) noexcept
		{
			if (this != &in)
			{
				mHost = std::move(in.mHost);
				mAddress = std::move(in.mAddress);
				mProtocolFamily = in.mProtocolFamily;
				mPort = in.mPort;
				mChannelType = in.mChannelType;
				mFlow = in.mFlow;
			}
			return *this;
		}

		Service::~Service() noexcept
		{
			// nothing more to do
		}

		void Service::clear() noexcept
		{
			mHost.clear();
			mAddress.clear();
			mChannelType = ChannelType::None;
			mProtocolFamily.clear();
			mPort = 0;
			mFlow = Flow::None;
		}

		Service::operator bool() const noexcept
		{
			return (!mHost.empty() || mAddress) && mChannelType != ChannelType::None && mProtocolFamily && mPort > 0;
		}

		const cio::Path &Service::getHost() const noexcept
		{
			return mHost;
		}

		void Service::setHost(cio::Path path) noexcept
		{
			mHost = std::move(path);
		}

		void Service::resolve()
		{
			if (!mHost.empty() && (!mAddress || !mProtocolFamily || !mPort))
			{
				Resolver resolver;

				if (!mProtocolFamily || !mPort)
				{
					std::pair<ProtocolFamily, std::uint16_t> settings = resolver.findServicePort(mHost.str());
					if (!mProtocolFamily)
					{
						mProtocolFamily = settings.first;
						mChannelType = mProtocolFamily.getChannelType();
					}

					if (!mPort)
					{
						mPort = settings.second;
					}
				}
				
				if (!mAddress)
				{
					mAddress = resolver.lookupAddress(mHost.get());
				}
			}
			
			if (mAddress.isBroadcast())
			{
				mFlow = Flow::Broadcast;
			}
		}

		void Service::resolveProtocolAndPort()
		{
			if (!mHost.empty() && (!mProtocolFamily || !mPort))
			{
				Resolver resolver;

				std::pair<ProtocolFamily, std::uint16_t> settings = resolver.findServicePort(mHost.get());
				if (!mProtocolFamily)
				{
					mProtocolFamily = settings.first;
					mChannelType = mProtocolFamily.getChannelType();
				}

				if (!mPort)
				{
					mPort = settings.second;
				}
			}
		}

		std::uint16_t Service::getPort() const noexcept
		{
			return mPort;
		}

		ChannelType Service::getChannelType() const noexcept
		{
			return mChannelType;
		}

		void Service::setChannelType(ChannelType type) noexcept
		{
			mChannelType = type;
		}

		ProtocolFamily Service::getProtocolFamily() const noexcept
		{
			return mProtocolFamily;
		}

		const Address &Service::getAddress() const noexcept
		{
			return mAddress;
		}

		AddressFamily Service::getAddressFamily() const noexcept
		{
			return mAddress.getAddressFamily();
		}
		
		void Service::setPort(std::uint16_t port) noexcept
		{
			mPort = port;
			mAddress.setPort(port); // only has an effect if address is already IP-based
		}

		void Service::setAddress(const Address &address)
		{
			mAddress = address;
			std::uint16_t port = mAddress.getPort();

			if (port > 0)
			{
				mPort = port;
			}
		}

		void Service::setAddress(Address &&address) noexcept
		{
			mAddress = std::move(address);

			std::uint16_t port = mAddress.getPort();

			if (port > 0)
			{
				mPort = port;
			}
		}

		void Service::setProtocolFamily(ProtocolFamily type) noexcept
		{
			mProtocolFamily = type;

			if (mChannelType == ChannelType::None || mChannelType == ChannelType::Unknown)
			{
				mChannelType = type.getChannelType();
			}
		}
		
		Flow Service::getFlow() const noexcept
		{
			return mFlow;
		}
		
		void Service::setFlow(Flow flow) noexcept
		{
			mFlow = flow;
		}

		std::ostream &operator<<(std::ostream &stream, const Service &service)
		{
			return stream << service.getProtocolFamily() << "://" << service.getAddress();
		}
	}
}

